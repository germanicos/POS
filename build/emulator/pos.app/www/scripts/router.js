define(['jquery', 'knockout'], function($, ko) {
    var router = new $.mobile.Router([
        			
    //Components
			
    {
            "#selectFabric": {
                handler: function() {
                    ko.applyBindings(fabricsVM, $('#selectFabric').get(0));
                }, 
                events: "c"
            }
        },

        {
        "#selectFabric": {
            handler: function() {
                fabricsVM.initIscroll();
                fabricsVM.retrieveContext();
            }, 
            events: "s"
        }
    },
{
    "#selectFabric": {
        handler: function() {
			
        }, 
        events: "bh"
    }
},

{
    "#editCustomer": {
        handler: function() {
            ko.applyBindings(customersVM, $('#editCustomer').get(0));
        }, 
        events: "c"
    }
},


// CUSTOMERS //
{
    "#listCustomer": {
        handler: function() {
        //	localStorage.removeItem('calendarDS');
        //	localStorage.removeItem('shippingDS');
        	
            customersVM.systemMode = "customer";
        }, 
        events: "s"
    }
},
{
    "#listCustomer": {
        handler: function() {
            customersVM.systemMode = "customer";
        }, 
        events: "h"
    }
},
{
    "#listCustomer": {
        handler: function() {
            //ko.applyBindings(customersVM, $('#listCustomer').get(0));
            ko.applyBindings(orderItem, $('#listCustomer').get(0) );
        }, 
        events: "c"
    }
},
{
    "#listCustomer": {
        handler: function() {
            //if ((customersVM.selectedCustomer != {}) && (customersVM.selectedCustomer != undefined))
            //    customersVM.customerAutocomplete("");
            if ((orderItem.custSelectVM.selectedCustomer() != {}) && (orderItem.custSelectVM.selectedCustomer() != undefined))
                orderItem.custSelectVM.customerAutocomplete("");
        }, 
        events: "bs"
    }
},
{
    "#addCustomer": {
        handler: function() {
console.log('ROUTER 1');
			customersVM.systemMode = "customer";
			try{
				customersVM.setObservables('');
			}catch(e){;}
            ko.applyBindings(customersVM, $('#addCustomer').get(0));
        }, 
        events: "c"
    }
},
{
    "#updateCustomer": {
        handler: function() {
            ko.applyBindings(customersVM, $('#updateCustomer').get(0));
        }, 
        events: "c"
    }
},

{
    "#customerInfo": {
        handler: function() {
            customersVM.systemMode = "customer";
        }, 
        events: "s"
    }
},
{
    "#customerInfo": {
        handler: function() {
            customersVM.systemMode = "customer";
        }, 
        events: "h"
    }
},
{
    "#customerInfo": {
        handler: function() {
        	/*
        	localStorage.removeItem('OrdersGarmentsForFittingDS');
            OrdersGarmentsForFittingDS = new SyncableDatasource('OrdersGarmentsForFittingDS','OrdersGarmentsForFittingDS', dsRegistry, 'customer_garments_fittings_endpoint');
            orderItem.OrdersGarmentsForFittingVM.subscribeTo('OrdersGarmentsForFittingDS');
            */
            /*
             localStorage.removeItem('FittingsDS');
			FittingsDS = new SyncableDatasource('FittingsDS', 'FittingsDS', dsRegistry, 'order_fittings_endpoint');
			orderItem.OrderFittingsVM.subscribeTo('FittingsDS');
             */
            
            //ko.applyBindings(customersVM, $('#customerInfo').get(0));
            ko.applyBindings(orderItem, $('#customerInfo').get(0));
        }, 
        events: "c"
    }
},
{
    "#customerNotes": {
        handler: function() {
            //ko.applyBindings(customersVM, $('#customerInfo').get(0));
            ko.applyBindings(orderItem, $('#customerNotes').get(0));
        }, 
        events: "c"
    }
},
{
    "#customerOrders": {
        handler: function() {
        	
            //ko.applyBindings(customersVM, $('#customerInfo').get(0));
            ko.applyBindings(orderItem, $('#customerOrders').get(0));
            
            localStorage.removeItem('customerOrdersDS');
       //     customerOrdersVM = new OrdersClass(dsRegistry);
            customerOrdersDS = new SyncableDatasource('customerOrdersDS', 'customerOrdersDS', dsRegistry, 'customer_orders_endpoint');
			//orderItem.customerOrdersVM.subscribeTo('customerOrdersDS');
			orderItem.custOrdersVM.subscribeTo('customerOrdersDS');			
			/*
			customerOrderDS = new SyncableDatasource('customerOrderDS', 'customerOrderDS', dsRegistry, 'customer_order_endpoint');
			customerOrderImagesDS = new SyncableDatasource('customerOrderImagesDS', 'customerOrderImagesDS', dsRegistry, 'customer_order_images_endpoint');
			customerOrderMeasurementsAndFitlineDS = new SyncableDatasource('customerOrderMeasurementsAndFitlineDS', 'customerOrderMeasurementsAndFitlineDS', dsRegistry, 'customer_order_fitnmeas_endpoint');
			customerOrderBodyshapeDS = new SyncableDatasource('customerOrderBodyshapeDS', 'customerOrderBodyshapeDS', dsRegistry, 'customer_order_bodyshape_endpoint');
			customerOrderPaymentsDS = new SyncableDatasource('customerOrderPaymentsDS', 'customerOrderPaymentsDS', dsRegistry, 'customer_order_payments_endpoint');
			customerNotesDS = new SyncableDatasource('customerNotesDS', 'customerNotesDS', dsRegistry, 'customer_notes_endpoint');
            */
            //customerOrdersDS.sync();
            /*
            customerOrderDS.sync();
            customerOrderImagesDS.sync();
            customerOrderMeasurementsAndFitlineDS.sync();
            customerOrderBodyshapeDS.sync();
            customerOrderPaymentsDS.sync();
            customerNotesDS.sync();
            */
            
        }, 
        events: "c"
    }
},

{
    "#customerGarmentsFittingsList": {
        handler: function() {
        	ko.applyBindings(orderItem, $('#customerGarmentsFittingsList').get(0));
        	
        	localStorage.removeItem('OrdersGarmentsForFittingDS');
            OrdersGarmentsForFittingDS = new SyncableDatasource('OrdersGarmentsForFittingDS','OrdersGarmentsForFittingDS', dsRegistry, 'customer_garments_fittings_endpoint');
            orderItem.OrdersGarmentsForFittingVM.subscribeTo('OrdersGarmentsForFittingDS');
        	
        	for(var x = 0; x < 100; x++){
        		localStorage.removeItem('FittingFrontImagePhoto' + x);
        		localStorage.removeItem('FittingBackImagePhoto' + x);
        		localStorage.removeItem('FittingSideImagePhoto' + x);
        		localStorage.removeItem('FittingCustomImagePhoto' + x);
        		localStorage.removeItem('FittingFrontImageTags' + x);
        		localStorage.removeItem('FittingBackImageTags' + x);
        		localStorage.removeItem('FittingSideImageTags' + x);
        		localStorage.removeItem('FittingCustomImageTags' + x);
        		
        		localStorage.removeItem('CompletionFrontImagePhoto' + x);
        		localStorage.removeItem('CompletionBackImagePhoto' + x);
        		localStorage.removeItem('CompletionSideImagePhoto' + x);
        	}
        }, 
        events: "c"
    }
},

{
    '#fittings': {
        handler: function() {
            ko.applyBindings(orderItem, $('#fittings').get(0));
            
            $('#FittingFrontImageMediaBtn').on('click', function() {
                $('#FittingFrontImageFiles').trigger('click'); 
            });
            $('#FittingSideImageMediaBtn').on('click', function() {
                $('#FittingSideImageFiles').trigger('click'); 
            });
            $('#FittingBackImageMediaBtn').on('click', function() {
                $('#FittingBackImageFiles').trigger('click'); 
            });
            $('#FittingCustomImageMediaBtn').on('click', function() {
                $('#FittingCustomImageFiles').trigger('click'); 
            });
        },  
        events: "c"
    }
},

{
    '#completions': {
        handler: function() {
            ko.applyBindings(orderItem, $('#completions').get(0));
            
               
            $('#CompletionFrontImageMediaBtn').on('click', function() {
                $('#CompletionFrontImageFiles').trigger('click'); 
            });
            $('#CompletionSideImageMediaBtn').on('click', function() {
                $('#CompletionSideImageFiles').trigger('click'); 
            });
            $('#CompletionBackImageMediaBtn').on('click', function() {
                $('#CompletionBackImageFiles').trigger('click'); 
            });
        },  
        events: "c"
    }
},
       
                 
{
    "#fittings": {
        handler: function() {
			var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},400);
        }, 
        events: "s"
    }
},

{
    "#AlterationsList": {
        handler: function() {
        	
        	var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";
        	
        	ko.applyBindings(orderItem, $('#AlterationsList').get(0));
        	
        	localStorage.removeItem('AlterationsDS');
            AlterationsDS = new SyncableDatasource('AlterationsDS','AlterationsDS', dsRegistry, 'alterations_endpoint');
            orderItem.AlterationsVM.subscribeTo('AlterationsDS');
        	
        	for(var x = 0; x < 100; x++){
        		localStorage.removeItem('AlterationReportImage' + x);
        		localStorage.removeItem('AlterationReportCostImage' + x);
        	}
        	
        	var myVar=setTimeout(function(){spinner.style.display = "none";},1500);
        }, 
        events: "c"
    }
},


{
    '#alterationReport': {
        handler: function() {
            ko.applyBindings(orderItem, $('#alterationReport').get(0));
            
            $('#AlterationReportImageMediaBtn').on('click', function() {
                $('#AlterationReportImageFiles').trigger('click'); 
            });
            $('#AlterationReportCostImageMediaBtn').on('click', function() {
                $('#AlterationReportCostImageFiles').trigger('click'); 
            });
        },  
        events: "c"
    }
},
    

{
    "#orderInfo": {
        handler: function() {
            ko.applyBindings(orderItem, $('#orderInfo').get(0));
            
            localStorage.removeItem('customerOrderDS');
            customerOrderDS = new SyncableDatasource('customerOrderDS', 'customerOrderDS', dsRegistry, 'customer_order_endpoint');
			//customerOrdersVM.subscribeTo('customerOrdersDS');
			orderItem.custOrderVM.subscribeTo('customerOrderDS');
		//	customerOrderDS.sync();
			localStorage.removeItem('fittingphotos');
			localStorage.removeItem('fittingtags');
			
			localStorage.removeItem('FittingsDS');
			FittingsDS = new SyncableDatasource('FittingsDS', 'FittingsDS', dsRegistry, 'order_fittings_endpoint');
			orderItem.OrderFittingsVM.subscribeTo('FittingsDS');
			
			tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');
			
			orderItem.FittingVM = new FittingClass(dsRegistry);
			
			//localStorage.removeItem('tailorsDS');
			//tailorsDS.sync();
			
			/*
			customerOrderImagesDS = new SyncableDatasource('customerOrderImagesDS', 'customerOrderImagesDS', dsRegistry, 'customer_order_images_endpoint');
			customerOrderMeasurementsAndFitlineDS = new SyncableDatasource('customerOrderMeasurementsAndFitlineDS', 'customerOrderMeasurementsAndFitlineDS', dsRegistry, 'customer_order_fitnmeas_endpoint');
			customerOrderBodyshapeDS = new SyncableDatasource('customerOrderBodyshapeDS', 'customerOrderBodyshapeDS', dsRegistry, 'customer_order_bodyshape_endpoint');
			customerOrderPaymentsDS = new SyncableDatasource('customerOrderPaymentsDS', 'customerOrderPaymentsDS', dsRegistry, 'customer_order_payments_endpoint');
			customerNotesDS = new SyncableDatasource('customerNotesDS', 'customerNotesDS', dsRegistry, 'customer_notes_endpoint');
            */
            /*
            customerOrderImagesDS.sync();
            customerOrderMeasurementsAndFitlineDS.sync();
            customerOrderBodyshapeDS.sync();
            customerOrderPaymentsDS.sync();
            customerNotesDS.sync();
            */
            
        }, 
        events: "c"
    }
},

{
    "#fittingImages": {
        handler: function() {
            ko.applyBindings(orderItem, $('#fittingImages').get(0));
            $('#custMediaBtn').on('click', function() {
                $('#files').trigger('click'); 
            });
        }, 
        events: "c"
    }
},


{
    "#fittingVideo": {
        handler: function() {
            ko.applyBindings(orderItem, $('#fittingVideo').get(0));
         /*   $('#custMediaBtn').on('click', function() {
                $('#files').trigger('click'); 
            });*/
        }, 
        events: "c"
    }
},

{
    "#assignTailor": {
        handler: function() {
            ko.applyBindings(orderItem, $('#assignTailor').get(0));
         /*   $('#custMediaBtn').on('click', function() {
                $('#files').trigger('click'); 
            });*/
        }, 
        events: "c"
    }
},

{
    "#viewMeasurements": {
        handler: function() {
            ko.applyBindings(orderItem, $('#viewMeasurements').get(0));
         /*   $('#custMediaBtn').on('click', function() {
                $('#files').trigger('click'); 
            });*/
        }, 
        events: "c"
    }
},


{
    "#paymentsHistory": {
        handler: function() {
            ko.applyBindings(orderItem, $('#paymentsHistory').get(0));
            
            localStorage.removeItem('customerOrderPaymentsDS');
            customerOrderPaymentsDS = new SyncableDatasource('customerOrderPaymentsDS', 'customerOrderPaymentsDS', dsRegistry, 'customer_order_payments_endpoint');
			orderItem.custOrderPaymentsVM.subscribeTo('customerOrderPaymentsDS');
			
			orderItem.OrderPaymentVM = new OrderPaymentClass(dsRegistry);
			
			localStorage.removeItem('paymentphoto');
			localStorage.removeItem('PaymentCreditCardPhoto');
            
        }, 
        events: "c"
    }
},

{
    "#makePayment": {
        handler: function() {
            ko.applyBindings(orderItem, $('#makePayment').get(0));
            
            $('#creditCardImageMediaBtn').on('click', function() {
                $('#creditCardImageFiles').trigger('click'); 
            });
        }, 
        events: "c"
    }
},

{
    "#errorSubmit": {
        handler: function() {
            ko.applyBindings(orderItem, $('#errorSubmit').get(0));
        }, 
        events: "c"
    }
},



{
    "#main": {
        handler: function() {
        	ko.applyBindings(orderItem, $('#main').get(0));
			
        //	localStorage.removeItem('calendarDS');
        //	localStorage.removeItem('shippingDS');
        	localStorage.removeItem('customerOrdersDS');
        	localStorage.removeItem('customerOrderDS');
        	
        	localStorage.removeItem('photos');
			localStorage.removeItem('tags');
        	localStorage.removeItem('fitlinephotos');
			localStorage.removeItem('fitlinetags');
			localStorage.removeItem('PaymentCreditCardPhoto');
			for(var x = 0; x < 20; x++){
        		localStorage.removeItem('FittingFrontImagePhoto' + x);
        		localStorage.removeItem('FittingBackImagePhoto' + x);
        		localStorage.removeItem('FittingSideImagePhoto' + x);
        		localStorage.removeItem('FittingCustomImagePhoto' + x);
        		localStorage.removeItem('FittingFrontImageTags' + x);
        		localStorage.removeItem('FittingBackImageTags' + x);
        		localStorage.removeItem('FittingSideImageTags' + x);
        		localStorage.removeItem('FittingCustomImageTags' + x);
        		localStorage.removeItem('CompletionFrontImagePhoto' + x);
        		localStorage.removeItem('CompletionBackImagePhoto' + x);
        		localStorage.removeItem('CompletionSideImagePhoto' + x);
        		localStorage.removeItem('AlterationReportImage' + x);
        		localStorage.removeItem('AlterationReportCostImage' + x);
        		
        		localStorage.removeItem('ShirtCustomImages' + x);
        		localStorage.removeItem('ShirtCustomImagesTags' + x);
        		localStorage.removeItem('JacketCustomImages' + x);
        		localStorage.removeItem('JacketCustomImagesTags' + x);
        		localStorage.removeItem('SuitCustomImages' + x);
        		localStorage.removeItem('SuitCustomImagesTags' + x);
        		localStorage.removeItem('PantCustomImages' + x);
        		localStorage.removeItem('PantCustomImagesTags' + x);
        		localStorage.removeItem('VestCustomImages' + x);
        		localStorage.removeItem('VestCustomImagesTags' + x);
        	}

			localStorage.removeItem('JacketCustomStructurePhoto');
			localStorage.removeItem('JacketCustomBottomPhoto');
			localStorage.removeItem('JacketCustomVentPhoto');
			localStorage.removeItem('JacketCustomLapelPhoto');
			localStorage.removeItem('JacketEmbroideryPhoto');
			localStorage.removeItem('JacketCustomMonogramPhoto');
			localStorage.removeItem('pantsCustomPocketsPhoto');
			localStorage.removeItem('suitJacketCustomStructurePhoto');
			localStorage.removeItem('suitJacketCustomBottomPhoto');
			localStorage.removeItem('suitJacketCustomVentPhoto');
			localStorage.removeItem('suitJacketCustomLapelPhoto');
			localStorage.removeItem('suitJacketCustomMonogramPhoto');
			localStorage.removeItem('suitJacketEmbroideryPhoto');
			localStorage.removeItem('pantsCustomPocketsPhoto');
			localStorage.removeItem('vestCustomLapelPhoto');
			localStorage.removeItem('vestCustomButtonsPhoto');        	
			localStorage.removeItem('ShippingFabricPhoto');
			localStorage.removeItem('ShippingCopyGarmentPhotos');
			localStorage.removeItem('ShippingCopyGarmentTags');
			localStorage.removeItem('ShippingFaultyRemakeGarmentsPhotos');
			localStorage.removeItem('ShippingFaultyRemakeGarmentsTags');
			localStorage.removeItem('ShippingOtherPhotos');
			localStorage.removeItem('ShippingOtherTags');
        	
        	var loginpage = document.getElementById("loginPage");
//console.log("test: " + loginpage);
        	if(loginpage == null){	// if coming from the login page, don't sync again
        		custData.sync();
        		fitlinesJacketDS.sync();
        //  	fitlinesPantsDS.sync();
        	}
            
            try{
	            var st = JSON.parse(localStorage.getItem('countriesDS'));
				timestamp = st.timestamp; 
				currenttimestamp = new Date().getTime();
				currenttimestamp = Math.floor(currenttimestamp/1000)
				difference = currenttimestamp - timestamp;
				minutesdifference = difference/60;
				hoursdifference = minutesdifference/60;
				
	            if( hoursdifference > 3 && loginpage == null){	// sync if not coming from the login page and more than 3 hours have passed since the last sync
        			countriesDS.sync(); 
	            	statesData.sync(); 
	            	citiesData.sync();
	            	//tailorsDS.sync();
	            	tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');
	            	currenciesDS.sync();
	            	pricerangeDS.sync();
        		}
				orderItem  = new OrderItem(dsRegistry);
			}catch(e){
				customAlert("Unrecoverable data error. Resetting system."); 
				orderItem.clearStorageAndExit();
			}
        }, 
        events: "c"
    }
},

        	
{
    "#calendar": {
        handler: function() {
        //	calendarDS = new SyncableDatasource('calendarDS', 'calendarDS', dsRegistry, 'salesman_calendar_endpoint');
        //	shippingDS = new SyncableDatasource('shippingDS', 'shippingDS', dsRegistry, 'shipping_days_endpoint');
            //calendarDS.sync();
       //     var indexcalendar = document.getElementById('indexcalendar');
		//	var myVar=setTimeout(function(){indexcalendar.style.display = "block";},5000);
			
			//var thecalendar = document.getElementById('thecalendar');
			//thecalendar.fullCalendar( 'rerenderEvents' );
			//reRenderEvents();
			
        }, 
        events: "c"
    }
}, 

{
    "#settings": {
        handler: function() {
			ko.applyBindings(orderItem, $('#customerEditMeasurement').get(0));			
        }, 
        events: "c"
    }
}, 


// order customers create
{
    "#orderItemAddCustomer": {
        handler: function() {
console.log('ROUTER 2');        	
            $("input[type='radio']").on('change',function() {
                $("input[type='radio']").checkboxradio("refresh");
            });
            ko.applyBindings(orderItem, $('#orderItemAddCustomer').get(0));
        }, 
        events: "c"
    }
},
{
    "#orderItemUpdateCustomer": {
        handler: function() {
            $("input[type='radio']").on('change',function() {
                $("input[type='radio']").checkboxradio("refresh");
            });
            ko.applyBindings(orderItem, $('#orderItemUpdateCustomer').get(0));
        }, 
        events: "c"
    }
},
{
    "#orderItemCustomerInfo": {
        handler: function() {
            customersVM.systemMode = "order";
        }, 
        events: "s"
    }
},
{
    "#orderItemCustomerInfo": {
        handler: function() {
            customersVM.systemMode = "order";
        }, 
        events: "h"
    }
},
{
    "#orderItemCustomerInfo": {
        handler: function() {
        	//custData.sync();
            ko.applyBindings(orderItem, $('#orderItemCustomerInfo').get(0));
        }, 
        events: "c"
    }
},

// order customers before show
{
    "#orderItemAddCustomer": {	
        handler: function() {
console.log('ROUTER 3');            	
            customersVM.setObservables('');
            orderItem.customersVMmirror = customersVM;
            orderItem.customersVMmirror.setObservables('');
            $("#subscribe").attr("checked",true).checkboxradio("refresh");
        }, 
        events: "bs"
    }
},


{
    "#orderItemAddCustomer": {	
        handler: function() {     	
            customersVM.systemMode = "order";
        }, 
        events: "s"
    }
},
{
    "#orderItemUpdateCustomer": {
        handler: function() {
            customersVM.setObservables(customersVM.selected_item());
        }, 
        events: "bs"
    }
},
{
    "#orderItemUpdateCustomer": {
        handler: function() {
            customersVM.systemMode = "order";
        }, 
        events: "s"
    }
},
{
    "#orderItemUpdateCustomer": {
        handler: function() {
            customersVM.systemMode = "order";
        }, 
        events: "h"
    }
},

{
    "#customerEditCustomer": {
        handler: function() {
            customersVM.setObservables(customersVM.selected_item());
        }, 
        events: "bs"
    }
},
{
    "#customerEditCustomer": {
        handler: function() {
            customersVM.systemMode = "customer";
        }, 
        events: "s"
    }
},
{
    "#customerEditCustomer": {
        handler: function() {
            customersVM.systemMode = "customer";
        }, 
        events: "h"
    }
},
{
    "#customerEditCustomer": {
        handler: function() {
            //ko.applyBindings(customersVM, $('#customerEditCustomer').get(0));
            ko.applyBindings(orderItem, $('#customerEditCustomer').get(0));
        }, 
        events: "c"
    }
},



{
    "#orderItemAddCustomer": {  	
        handler: function() {
console.log('ROUTER 5');          	
            customersVM.systemMode = "order";
        }, 
        events: "h"
    }
},


{
    "#firstPage": {
        handler: function() {
			authCtrl.username("");
			authCtrl.password("");    
			authCtrl.userInfo = {};			      	
            $.mobile.changePage('#loginPage', {
                transition: "none"
            });
        }, 
        events: "s"
    }
},


{
    "#loginPage": {
        handler: function() {
            ko.applyBindings(authCtrl, $('#loginPage').get(0));
        }, 
        events: "c"
    }
},
{
    "#loginPage": {
        handler: function() {
        	/*
        	countriesDS.sync();
            statesData.sync();
            citiesData.sync();
            */
        	
            $('#iBespoken img').hide();
            $('#iIntroLogo img').hide();
            $('#cname').hide();
            $('#cpass').hide();
				
            $('#iIntroLogo img').fadeIn('slow');
            $('#cname').fadeIn('slow');
            $('#cpass').fadeIn('slow', function() {
                $('#iBespoken img').fadeIn('slow');
            });
        //$.mobile.changePage("#orderItemSelectCustomer");
				
        }, 
        events: "s"
    }
},
			
{
    "#orderItemReview": {
        handler: function() {
            ko.applyBindings(orderItem, $('#orderItemReview').get(0));
            
			$('#creditCardImageMediaBtn').on('click', function() {
                $('#creditCardImageFiles').trigger('click'); 
            });            
        }, 
        events: "c"
    }
},
			
{
    "#orderItemPopulateGarments": {
        handler: function() {
            ko.applyBindings(orderItem, $('#orderItemPopulateGarments').get(0) );
        }, 
        events: "c"
    }
},
{
    "#orderItemPopulateGarments": {
        handler: function() {
            orderItem.populateGarments();
                              
        }, 
        events: "bh"
    }
},			


{
    "#orderItemSelectGarment": {
        handler: function() {
            //OrderItemSelectGarmentVM OrderItemSelectGarmentload
            ko.applyBindings(orderItem, $('#orderItemSelectGarment').get(0) );
            
            if(orderItem.lockselects == true){
				orderItem.initializeHtmlFromExtraPants();
			}
                            
            var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},1500);
        }, 
        events: "c"
    }
},
{
    "#orderItemSelectGarment": {
        handler: function() {		
            //  this.numberOfPant = ko.observable(true);
            numberOfPant = ko.observable("");
            numberOfJacket = ko.observable("");                               
            numberOfSuit = ko.observable(""); 
            numberOfShirt = ko.observable("");
            numberOfVest = ko.observable(""); 
            //if((typeof orderItem.garmentsJacketVM.JacketData[0].JacketBottomStyle.id!='undefined')){
            //  }else{
            // if (typeof(window[orderItem.garmentsJacketVM]) == 'undefined') {
            if(typeof orderItem.garmentsJacketVM === "undefined" || orderItem.garmentsJacketVM == undefined || orderItem.garmentsJacketVM.jacketData == undefined || orderItem.garmentsJacketVM.jacketData[0] == undefined){
				;
            } else {
                if(	orderItem.garmentsJacketVM.JacketData[0].JacketFabric.id!=0 || orderItem.garmentsJacketVM.JacketData[0].JacketJacketStructure.id!=0 || orderItem.garmentsJacketVM.JacketData[0].JacketBottomStyle.id!=0
                    || orderItem.garmentsJacketVM.JacketData[0].JacketVentStyle.id!=0 || orderItem.garmentsJacketVM.JacketData[0].JacketLapelStyle.id!=0 || orderItem.garmentsJacketVM.JacketData[0].JacketPocketStyle.id!=0	){                  
                    // numberOfJacket = ko.observable(1);  
                    if(orderItem.garmentsJacketVM.JacketDataAID!=0){
                        var jacketclone = orderItem.garmentsJacketVM.JacketDataAID + 1;
                        numberOfJacket = ko.observable(jacketclone);   
                    }else{
                        numberOfJacket = ko.observable(1);
                    }                    
                }else{
                    numberOfJacket = ko.observable("");
                }
            }                 
            if(typeof orderItem.garmentsPantVM === "undefined" || orderItem.garmentsPantVM == undefined || orderItem.garmentsPantVM.pantData == undefined || orderItem.garmentsPantVM.pantData[0] == undefined){
            	; 
            }else{                        
                if(orderItem.garmentsPantVM.PantData[0].pantPleats.id!=0 || orderItem.garmentsPantVM.PantData[0].PantFabric.id!=0  /* || orderItem.garmentsPantVM.PantData[0].pantPockets.id!=0 */ || orderItem.garmentsPantVM.PantData[0].beltLoopStyle.id!=0
                   || orderItem.garmentsPantVM.PantData[0].pantFit.id!=0   || orderItem.garmentsPantVM.PantData[0].pantPleats.id!=0 || orderItem.garmentsPantVM.PantData[0].bpantPockets.id!=0 || orderItem.garmentsPantVM.PantData[0].beltLoopStyle.id!=0 ){
                    //  numberOfPant = ko.observable(1);     
                    //  this.numberOfPant = ko.observable(1);     
                    if(orderItem.garmentsPantVM.PantDataAID!=0){
                        var pantclone = orderItem.garmentsPantVM.PantDataAID + 1;
                        numberOfPant = ko.observable(pantclone);
                    }else{
                        numberOfPant = ko.observable(1);
                    }
                }else{
                    numberOfPant = ko.observable("");  
                } 
            }
            //  orderItem.OrderItemSelectGarmentVM.OrderItemSelectGarmentload(orderItem);
            if(typeof orderItem.garmentsSuitVM === "undefined" || orderItem.garmentsSuitVM == undefined || orderItem.garmentsSuitVM.suitData == undefined || orderItem.garmentsSuitVM.suitData[0] == undefined){
				;
            }else{
                if(orderItem.garmentsSuitVM.suitData[0].suitFabric.id!=0 || orderItem.garmentsSuitVM.suitData[0].suitJacketStructure.id!=0 || orderItem.garmentsSuitVM.suitData[0].BottomStyle.id!=0 || orderItem.garmentsSuitVM.suitData[0].VentStyle.id!=0
                   || orderItem.garmentsSuitVM.suitData[0].LapelStyle.id!=0 || orderItem.garmentsSuitVM.suitData[0].PocketStyle.id!=0 || orderItem.garmentsSuitVM.suitData[0].ButtonStyle.id!=0 || orderItem.garmentsSuitVM.suitData[0].pantFit.id!=0
                   || orderItem.garmentsSuitVM.suitData[0].pantPleats.id!=0 /*|| orderItem.garmentsSuitVM.suitData[0].pantPockets.id!=0*/|| orderItem.garmentsSuitVM.suitData[0].bpantPockets.id!=0 || orderItem.garmentsSuitVM.suitData[0].beltLoopStyle.id!=0 ){   
                    // numberOfSuit = ko.observable(1);      
                    if(orderItem.garmentsSuitVM.suitDataAID!=0){
                        var Suitclone = orderItem.garmentsSuitVM.suitDataAID + 1;
                        numberOfSuit = ko.observable(Suitclone);
                    }else{
                        numberOfSuit = ko.observable(1);
                    }     
                }else{
                    numberOfSuit = ko.observable("");
                }           
            }                             
            if(typeof orderItem.garmentsVestVM === "undefined" || orderItem.garmentsVestVM == undefined || orderItem.garmentsVestVM.vestData == undefined || orderItem.garmentsVestVM.vestData[0] == undefined ){
            	;
            }else{           
                if(orderItem.garmentsVestVM.vestData[0].vestFabric.id!=0 || orderItem.garmentsVestVM.vestData[0].vestNecklineStyle.id!=0 || orderItem.garmentsVestVM.vestData[0].vestBottomStyle.id!=0 
                	|| orderItem.garmentsVestVM.vestData[0].vestPocketStyle.id!=0 ){   
                    // numberOfVest = ko.observable(1); 
                    if(orderItem.garmentsVestVM.vestDataAID!=0){
                        var Vestclone = orderItem.garmentsVestVM.vestDataAID + 1;
                        numberOfVest = ko.observable(Vestclone);
                    }else{
                        numberOfVest = ko.observable(1);
                    }    
                }else{
                    numberOfVest = ko.observable("");
                }
            }
                 
            if(typeof orderItem.garmentsShirtVM === "undefined" || orderItem.garmentsShirtVM == undefined || orderItem.garmentsShirtVM.shirtData == undefined || orderItem.garmentsShirtVM.shirtData[0] == undefined ){
                 ;
            }else{
                //if(orderItem.garmentsShirtVM.shirtData[0] == undefined){             
                if(orderItem.garmentsShirtVM.shirtData[0].shirtFabric.id!=0 || orderItem.garmentsShirtVM.shirtData[0].shirtCollarStyle.id!=0 || orderItem.garmentsShirtVM.shirtData[0].shirtBottomStyle.id!=0 
                	|| orderItem.garmentsShirtVM.shirtData[0].shirtPocketStyle.id!=0 ){
                    	// numberOfVest = ko.observable(1); 
	                    if(orderItem.garmentsShirtVM.shirtDataAID!=0){
	                        var Shirtclone = orderItem.garmentsShirtVM.shirtDataAID + 1;
	                        numberOfShirt = ko.observable(Shirtclone);
	                   	 }else{
	                        numberOfShirt = ko.observable(1);
	                     } 
               	}else{
                   	numberOfShirt = ko.observable("");
                }
            }            


			var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";}, 1500);        
        
        }, 
        events: "bs"
    }
},
	
/*	
{
    "#orderItemSelectCustomer": {
        handler: function() {
            customersVM.systemMode = "order";
        }, 
        events: "s"
    }
},
{
    "#orderItemSelectCustomer": {
        handler: function() {
            customersVM.systemMode = "order";
        }, 
        events: "h"
    }
},*/	
{
    "#orderItemSelectCustomer": {
        handler: function() {
        //	localStorage.removeItem('calendarDS');
        //	localStorage.removeItem('shippingDS');
        	customersVM.systemMode = "order";
            ko.applyBindings(orderItem, $('#orderItemSelectCustomer').get(0) );
        }, 
        events: "c"
    }
},
{
    "#orderItemSelectCustomer": {
        handler: function() {
        //	localStorage.removeItem('calendarDS');
        //	localStorage.removeItem('shippingDS');
        	customersVM.systemMode = "order";
            if ((orderItem.custSelectVM.selectedCustomer() != {}) && (orderItem.custSelectVM.selectedCustomer() != undefined))
                orderItem.custSelectVM.customerAutocomplete("");
        }, 
        events: "bs"
    }
},

/////////////////////////////////////////

{
    '#garmentsJacket': {
        handler: function() {
            ko.applyBindings(orderItem, $('#garmentsJacket').get(0));
            
            $('#JacketCustomImagesMediaBtn').on('click', function() {
				$('#JacketCustomImagesMediaFiles').trigger('click');
			}); 
			
			// old way 
            $('#JacketCustomStructureMediaBtn').on('click', function() {
                $('#JacketCustomStructureFiles').trigger('click'); 
            });
            $('#JacketCustomBottomMediaBtn').on('click', function() {
                $('#JacketCustomBottomFiles').trigger('click'); 
            });
            $('#JacketCustomLapelMediaBtn').on('click', function() {
                $('#JacketCustomLapelFiles').trigger('click'); 
            });
            $('#JacketCustomMonogramMediaBtn').on('click', function() {
                $('#JacketCustomMonogramFiles').trigger('click'); 
            });
            $('#JacketEmbroideryMediaBtn').on('click', function() {
                $('#JacketEmbroideryFiles').trigger('click'); 
            });
            $('#JacketCustomVentMediaBtn').on('click', function() {
                $('#JacketCustomVentFiles').trigger('click'); 
            });   			
        },  
        events: "c"
    }
},
                 
{
    "#garmentsJacket": {
        handler: function() {
				
            orderItem.garmentsVM.attachCloneJacketAttribute();
            orderItem.garmentsVM.JacketFabricSelect();            
			var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},400);
        }, 
        events: "s"
    }
},
           


////////////////////////////////////////////////////////////////////////////////
{
    '#garmentsPant': {
        handler: function() {
            ko.applyBindings(orderItem, $('#garmentsPant').get(0));
            
            $('#PantCustomImagesMediaBtn').on('click', function() {
                $('#PantCustomImagesMediaFiles').trigger('click'); 
            });
            
            // old way
            $('#pantsCustomPocketsMediaBtn').on('click', function() {
                $('#pantsCustomPocketsFiles').trigger('click'); 
            }); 
        }, 
        events: "c"
    }
},
{
    "#garmentsPant": {
        handler: function() {
				
            orderItem.garmentsVM.attachClonePantsAttribute();
            orderItem.garmentsVM.PantFabricSelect();
			var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},300);
        }, 
        events: "s"
    }
},



////////////////////////////////////////////////////////////////////////////////

{
    '#garmentsVest': {
        handler: function() {
            ko.applyBindings(orderItem, $('#garmentsVestNew').get(0));
            
                $('#VestCustomImagesMediaBtn').on('click', function() {
                	$('#VestCustomImagesMediaFiles').trigger('click');         	
            	});
            	
            	//old way
            	$('#vestCustomButtonsMediaBtn').on('click', function() {
                	$('#vestCustomButtonsFiles').trigger('click');         	
            	});
            	$('#vestCustomDesignMediaBtn').on('click', function() {
					$('#vestCustomDesignFiles').trigger('click');         	
				});
				$('#vestCustomLapelMediaBtn').on('click', function() {
					$('#vestCustomLapelFiles').trigger('click');         	
				});
        }, 
        events: "c"
    }
},
{
    '#garmentsVest': {
        handler: function() {
            ko.applyBindings(orderItem, $('#garmentsVest').get(0));
            
			$('#VestCustomImagesMediaBtn').on('click', function() {
				$('#VestCustomImagesMediaFiles').trigger('click');
			});
			
			//old way
			$('#vestCustomButtonsMediaBtn').on('click', function() {
				$('#vestCustomButtonsFiles').trigger('click');
			});
			$('#vestCustomDesignMediaBtn').on('click', function() {
				$('#vestCustomDesignFiles').trigger('click');
			});
			$('#vestCustomLapelMediaBtn').on('click', function() {
				$('#vestCustomLapelFiles').trigger('click');         	
			});
        }, 
        events: "c"
    }
},
{
    "#garmentsVest": {
        handler: function() {
				
            orderItem.garmentsVM.attachCloneVestAttribute();
            orderItem.garmentsVM.vestFabricSelect();
        // $('.garmentsDropdown').trigger( "expand" );
        //orderItem.garmentsVM.vestContrastFabricSelect();
            var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},100);
        }, 
        events: "s"
    }
},

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
{
    '#garmentsSuit': {
        handler: function() {
            ko.applyBindings(orderItem, $('#garmentsSuit').get(0));
            
			$('#SuitCustomImagesMediaBtn').on('click', function() {
				$('#SuitCustomImagesMediaFiles').trigger('click');
			});         
                       
            //old way
            $('#suitJacketCustomStructureMediaBtn').on('click', function() {
                $('#suitJacketCustomStructureFiles').trigger('click'); 
            });
            $('#suitJacketCustomBottomMediaBtn').on('click', function() {
                $('#suitJacketCustomBottomFiles').trigger('click'); 
            });
            $('#suitJacketCustomLapelMediaBtn').on('click', function() {
                $('#suitJacketCustomLapelFiles').trigger('click'); 
            });
            $('#suitJacketCustomMonogramMediaBtn').on('click', function() {
                $('#suitJacketCustomMonogramFiles').trigger('click'); 
            });
            $('#suitJacketEmbroideryMediaBtn').on('click', function() {
                $('#suitJacketEmbroideryFiles').trigger('click'); 
            });   
            $('#suitJacketCustomVentMediaBtn').on('click', function() {
                $('#suitJacketCustomVentFiles').trigger('click'); 
            });
            $('#pantsCustomPocketsMediaBtn').on('click', function() {
                $('#pantsCustomPocketsFiles').trigger('click'); 
            });
        }, 
        events: "c"
    }
},
{
    "#garmentsSuit": {
        handler: function() {
            orderItem.garmentsVM.attachCloneSuitAttribute();
            orderItem.garmentsVM.suitFabricSelect();
			var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},500);
        }, 
        events: "s"
    }
},

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

{
    '#garmentsShirt': { 
        handler: function() {
            ko.applyBindings(orderItem, $('#garmentsShirt').get(0));
            
            $('#ShirtCustomImagesMediaBtn').on('click', function() {
				$('#ShirtCustomImagesMediaFiles').trigger('click');
			}); 
        }, 
        events: "c"
    }
},
{
    "#garmentsShirt": {
        handler: function() {
				
            orderItem.garmentsVM.attachCloneShirtAttribute();
            orderItem.garmentsVM.shirtFabricSelect();
            
			var spinner = document.getElementById('loading_jp');
			var myVar=setTimeout(function(){spinner.style.display = "none";},300);
        }, 
        events: "s"
    }
},





{
    "#measurements": {
        handler: function() {
            ko.applyBindings(orderItem, $('#measurements').get(0));
	         $('#fitlineMediaBtn').on('click', function() {
                $('#fitlinefiles').trigger('click'); 
            });
        }, 
        events: "c"
    }
},

{
    "#customerEditMeasurement": {
        handler: function() {        	
            ko.applyBindings(orderItem, $('#customerEditMeasurement').get(0));
console.log("router customerEditMeasurement");
	         //$('#fitlineMediaBtn').on('click', function() {
             //   $('#fitlinefiles').trigger('click'); 
            //});
        }, 
        events: "c"
    }
},

			
{
    "#bodyshape": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshape').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeShoulders": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeShoulders').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeShoulders": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},
			
{
    "#bodyshapeNeckStance": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeNeckStance').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeNeckStance": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeArms": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeArms').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeArms": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeBack": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeBack').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeBack": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeBeltAngle": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeBeltAngle').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeBeltAngle": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeBeltHeight": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeBeltHeight').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeBeltHeight": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeFront": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeFront').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeFront": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeLeg": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeLeg').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeLeg": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeNeckHeight": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeNeckHeight').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeNeckHeight": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeSeat": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeSeat').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeSeat": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeSleeve": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeSleeve').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeSleeve": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeStanding": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeStanding').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeStanding": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeStomach": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeStomach').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeStomach": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},

{
    "#bodyshapeThigh": {
        handler: function() {
            ko.applyBindings(orderItem, $('#bodyshapeThigh').get(0));
        }, 
        events: "c"
    }
},
{
    "#bodyshapeThigh": {
        handler: function() {
            $('.bodyshapeDropdown').trigger( "expand" );
        }, 
        events: "s"
    }
},



{
    "#customerEditBodyshape": {
        handler: function() {
            ko.applyBindings(orderItem, $('#customerEditBodyshape').get(0));
        }, 
        events: "c"
    }
},

{
    "#customerEditMeasurment": {
        handler: function() {
            ko.applyBindings(orderItem, $('#customerEditMeasurment').get(0));
        }, 
        events: "c"
    }
},
			
{
    "#orderItemMedia": {
        handler: function() {
            ko.applyBindings(orderItem, $('#orderItemMedia').get(0));
            $('#custMediaBtn').on('click', function() {
                $('#files').trigger('click'); 
            });
        }, 
        events: "c"
    }
},
////////////////////////////////////////////////////////////////////////////////////
				//		SHIPPING
////////////////////////////////////////////////////////////////////////////////////
{
    "#shipping_main": {
        handler: function() {
            customersVM.systemMode = "shipping";
            ko.applyBindings(orderItem, $('#shipping_main').get(0) );
            localStorage.removeItem('ShippingFabricPhoto');
            localStorage.removeItem('ShippingCopyGarmentPhotos');
			localStorage.removeItem('ShippingCopyGarmentTags');
			localStorage.removeItem('ShippingFaultyRemakeGarmentsPhotos');
			localStorage.removeItem('ShippingFaultyRemakeGarmentsTags');
			localStorage.removeItem('ShippingOtherPhotos');
			localStorage.removeItem('ShippingOtherTags');
			
			orderItem  = new OrderItem(dsRegistry);
        }, 
        events: "c"
    }
},

{
    "#shipping_select_customer": {
        handler: function() {
            ko.applyBindings(orderItem, $('#shipping_alterations_select_customer').get(0) );
            localStorage.removeItem('ShippingFabricPhoto');
            localStorage.removeItem('ShippingCopyGarmentPhotos');
			localStorage.removeItem('ShippingCopyGarmentTags');
			localStorage.removeItem('ShippingFaultyRemakeGarmentsPhotos');
			localStorage.removeItem('ShippingFaultyRemakeGarmentsTags');
			localStorage.removeItem('ShippingOtherPhotos');
			localStorage.removeItem('ShippingOtherTags');
        }, 
        events: "c"
    }
},

{
    "#shipping_alterations_list": {
        handler: function() {
            
            ko.applyBindings(orderItem, $('#shipping_alterations_list').get(0) );
            
            var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";
		   	
		   	localStorage.removeItem('AlterationsDS');
            AlterationsDS = new SyncableDatasource('AlterationsDS','AlterationsDS', dsRegistry, 'alterations_endpoint');
            orderItem.AlterationsVM.subscribeTo('AlterationsDS');
		   	
		   	var myVar=setTimeout(function(){spinner.style.display = "none";},1500);
        }, 
        events: "c"
    }
},

{
    "#shipping_fabrics": {
        handler: function() {
            ko.applyBindings(orderItem, $('#shipping_fabrics').get(0) );
            //orderItem.ShippingFabricVM = new ShippingFabric(dsRegistry);
            $('#ShippingFabricMediaBtn').on('click', function() {
                $('#ShippingFabricImageFiles').trigger('click'); 
            });
            
            orderItem.ShippingFabricVM = new ShippingFabric(orderItem.dsRegistry);
			orderItem.ShippingFabricDS = new defShippingFabric('ShippingFabricDS', orderItem.dsRegistry );
			orderItem.ShippingFabricVM.subscribeTo('ShippingFabricDS');
console.log("orderItem.shippingFabricDS.getStore(): " + JSON.stringify( orderItem.ShippingFabricDS.getStore() ) );
//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
 			orderItem.ShippingFabricVM.addVariantsShippingFabrics();
            
        }, 
        events: "c"
    }
},

{
    "#shipping_copy_garment": {
        handler: function() {
            ko.applyBindings(orderItem, $('#shipping_copy_garment').get(0) );
            //orderItem.ShippingCopyGarmentVM = new ShippingCopyGarment(dsRegistry);
            $('#ShippingCopyGarmentMediaBtn').on('click', function() {
                $('#ShippingCopyGarmentImageFiles').trigger('click'); 
            });
            
            orderItem.ShippingCopyGarmentVM = new ShippingCopyGarment(orderItem.dsRegistry);
			orderItem.ShippingCopyGarmentDS = new defShippingCopyGarment('ShippingCopyGarmentDS', orderItem.dsRegistry );
			orderItem.ShippingCopyGarmentVM.subscribeTo('ShippingCopyGarmentDS');
console.log("orderItem.ShippingCopyGarmentDS.getStore(): " + JSON.stringify( orderItem.ShippingCopyGarmentDS.getStore() ) );
//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
 			orderItem.ShippingCopyGarmentVM.addVariantsShippingCopyGarments();
            
        }, 
        events: "c"
    }
},

{
    "#shipping_faulty_remake_garments": {
        handler: function() {
            ko.applyBindings(orderItem, $('#shipping_faulty_garments').get(0) );
            //orderItem.ShippingFaultyRemakeGarmentsVM = new ShippingFaultyRemakeGarments(dsRegistry);
            $('#ShippingFaultyRemakeGarmentsMediaBtn').on('click', function() {
                $('#ShippingFaultyRemakeGarmentsImageFiles').trigger('click'); 
            });
            
            // USING THE ALTERATIONS FOR CREATION AND TESTING
            localStorage.removeItem('AlterationsDS');
            AlterationsDS = new SyncableDatasource('AlterationsDS','AlterationsDS', dsRegistry, 'alterations_endpoint');
            orderItem.AlterationsVM.subscribeTo('AlterationsDS');
            
            
            orderItem.ShippingFaultyRemakeGarmentsVM = new ShippingFaultyRemakeGarments(orderItem.dsRegistry);
			orderItem.ShippingFaultyRemakeGarmentsDS = new defShippingFaultyRemakeGarments('ShippingFaultyRemakeGarmentsDS', orderItem.dsRegistry );
			orderItem.ShippingFaultyRemakeGarmentsVM.subscribeTo('ShippingFaultyRemakeGarmentsDS');
console.log("orderItem.ShippingFaultyRemakeGarmentsDS.getStore(): " + JSON.stringify( orderItem.ShippingFaultyRemakeGarmentsDS.getStore() ) );
//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
 			orderItem.ShippingFaultyRemakeGarmentsVM.addVariantsShippingFaultyRemakeGarments();
            
        }, 
        events: "c"
    }
},

{
    "#shipping_other": {
        handler: function() {
            ko.applyBindings(orderItem, $('#shipping_other').get(0) );
            //orderItem.ShippingOtherVM = new ShippingOther(dsRegistry);
            $('#ShippingOtherMediaBtn').on('click', function() {
                $('#ShippingOtherImageFiles').trigger('click'); 
            });
                        
            orderItem.ShippingOtherVM = new ShippingOther(orderItem.dsRegistry);
			orderItem.ShippingOtherDS = new defShippingOther('ShippingOtherDS', orderItem.dsRegistry );
			orderItem.ShippingOtherVM.subscribeTo('ShippingOtherDS');
console.log("orderItem.ShippingOtherDS.getStore(): " + JSON.stringify( orderItem.ShippingOtherDS.getStore() ) );
//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
 			orderItem.ShippingOtherVM.addVariantsShippingOther();
            
        }, 
        events: "c"
    }
},



////////////////////////////////////////////////////////////////////////////////////
//						ERROR REPORT
////////////////////////////////////////////////////////////////////////////////////


{
	"#errorList":{
		handler: function() {
			
			errorReportListVM.loadErrorsList();
			
        }, 
        events: "bl"
	}
	
},

{
	"#errorList":{
		handler: function() {
			
			ko.applyBindings(errorReportListVM);
        }, 
        events: "s"
	}
	
},


{
	"#errorReport":{
		handler: function() {
			errorReportVM.setSelectedError(errorReportListVM.selectedError);
			errorReportVM.loadInfo();
        }, 
        events: "bl"
	}
	
},

{
	"#errorReport":{
		handler: function() {
			ko.applyBindings(errorReportVM);
        }, 
        events: "s"
	}
	
},



{
	"#errorReply":{
		handler: function() {
			ko.applyBindings(errorReportVM.ErrorReplyVM);
        }, 
        events: "s"
	}
	
},

{
	"#errorAdd":{
		handler: function() {
			errorReportVM.cleanAll();
			errorReportVM.prepareToInsert();
        }, 
        events: "bl"
	}
	
},

{
	"#errorAdd":{
		handler: function() {
			ko.applyBindings(errorReportVM);
        }, 
        events: "s"
	}
	
},

{
	"#errorEdit":{
		handler: function() {
			errorReportVM.prepareToUpdate();
        }, 
        events: "bl"
	}
	
},

{
	"#errorEdit":{
		handler: function() {
			ko.applyBindings(errorReportVM);
        }, 
        events: "s"
	}
	
},

{
	"#uploadMediaTest":{
		handler: function() {
        }, 
        events: "c"
	}
	
},

/*
{
    "#orderItemReview": {
        handler: function() {
        	ko.applyBindings(orderItem, $('#orderItemReview').get(0));
           // dd_buildStructure();
        //   $('#creditCardImageMediaBtn').on('click', function() {
        //        $('#creditCardImageFiles').trigger('click'); 
        //    });
        }, 
        events: "s"
    }
},
*/

			
//Route template
/*
			{ "#pLogin": { handler: function() {//onCreate page event handler}, events: "c" } },
            { "#pLogin": { handler: function() {//onShow   page event handler}, events: "s" } },
			{ "#pLogin": { handler: function() {//onHide   page event handler}, events: "c" } },
            */
			
			
//All page route, create/destroy iscroll, menu handling
{
    "(?:[?/](.*))?": {
        handler: function(type, matchObj, ui, page) {
        	
	//		var indexcalendar = document.getElementById('indexcalendar');
	//		//indexcalendar.style.display = "block";
	//		indexcalendar.style.display = "none";        	
        	
            //$.backstretch('gfx/template/body_bg.jpg');
            if ( $('#scrollWrapper').length != 0 ) {
                iSGarments =  new iScroll('scrollWrapper', {});
                iSGarments.options.onBeforeScrollStart = function(e) {                
        					
                    if ( $('.cloneDialog').length != 0 ) {
                        $('.cloneDialog').remove();
                    //$('.cloneDialog').fadeOut('fast', function(){$('.cloneDialog').remove(); }); 
                    }
							
                    var target = e.target;

                    while (target.nodeType != 1) target = target.parentNode;
                    if (target.tagName != 'SELECT' && target.tagName != 'INPUT' && target.tagName != 'TEXTAREA'){
                        e.preventDefault();
                    }
                }
            }
        }, 
        events: "s"
    }
},

{
    "(?:[?/](.*))?": {
        handler: function() {
        	try{
			//	document.getElementById("canvas_container").innerHTML = [''].join('');
 				document.getElementById("canvas_container").innerHTML = ['<canvas id="canvas-0"></canvas><canvas id="canvas-1"></canvas><canvas id="canvas-2"></canvas><canvas id="canvas-squash"></canvas>'].join('');
 			}catch(e){ ; }	
        }, 
        events: "h"
    }
}
],
{
    defaultHandler: function(type, ui, page) {
        console.log("Default handler called due to unknown route (" + type + ", " + ui + ", " + page + ")");
    },
    defaultHandlerEvents: "s"
});
});
