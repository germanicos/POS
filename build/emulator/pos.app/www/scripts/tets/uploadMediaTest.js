/*
Script to deal with the Native Camera

Linkedto: uploadMediaTest.htm

by:Igor Leal
*/

/* Flow of the script:

    1 - functions to be called are: chooseGalleryCamera or chooseGalleryVideo
    2 - follow the callbacks
*/
//Will store the full path of the image to be written on the json
imageURI = "";
//will store the json to be written
imageName = "";
//will keep the root of the app directory
rootAppDirectory = "";

function chooseGalleryCamera() 
{
    navigator.notification.confirm(
        'Source', // message
         onConfirmCamera,            // callback to invoke with index of button pressed
        '',           // title
        ['Camera','Gallery']         // buttonLabels
    );
	/*picSource = Camera.PictureSourceType.CAMERA;
	navigator.camera.getPicture(onPhotoDataSuccess, onFail,
    {   quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: picSource 
    });*/
	alert("passei");
}

function chooseGalleryVideo() 
{
    navigator.notification.confirm(
        'Source', // message
         onConfirmVideo,            // callback to invoke with index of button pressed
        '',           // title
        ['Camera','Gallery']         // buttonLabels
    );
}


/****************************Camera Photo Functions*************************************/
function onConfirmCamera(buttonIndex) 
{
    if( buttonIndex === 1 )
    {
        capturePhoto("camera");
    }
    else if( buttonIndex === 2)
    {
        capturePhoto("gallery");
    }
}

function capturePhoto(source) 
{
    var picSource;
    if(source === "camera")
    {
        picSource = Camera.PictureSourceType.CAMERA;
    }
    else
    {
        picSource = Camera.PictureSourceType.PHOTOLIBRARY;
    }

    navigator.camera.getPicture(onPhotoDataSuccess, onFail,
    {   quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: picSource 
    });
}

//moving Picture
function onPhotoDataSuccess(imageURI)
{   
    window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
}

//Callback function when the file system uri has been resolved
function resolveOnSuccess(entry)
{ 
    console.log("resolve on success");

    var d = new Date();
    var n = d.getTime();
    //new file name
    var newFileName = n + ".jpg";
    var myFolderApp = "ClientMediaStorage";

    imageName = n;
    
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {      
    //The folder is created if doesn't exist
    fileSys.root.getDirectory( myFolderApp,
                    {create:true, exclusive: false},
                    function(directory) {
                        entry.moveTo(directory, newFileName,  successMove, resOnError);
                    },
                    resOnError);
                    },
    resOnError);
}



/************************************************************************************************/

/***********************************Video Camera functions***************************************/
function onConfirmVideo (buttonIndex) 
{
    if( buttonIndex === 1 )
    {
        captureVideo("camera");
    }
    else if( buttonIndex === 2)
    {
        captureVideo("gallery");
    }
}

function captureVideo(source)
{
    if( source === "camera" )
    {
        navigator.device.capture.captureVideo(onVideoDataSuccess, onFail);      
    }
    //take video from gallery
    else
    {
        console.log("Entering video Gallery");

        navigator.camera.getPicture(onGalleryDataSuccess, onFail, { quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: Camera.MediaType.VIDEO  });      
    }
}

function onVideoDataSuccess(mediaFiles) 
{ 
    console.log("onVideoDataSuccess");
    var i, path, len;
    for (i = 0, len = mediaFiles.length; i < len; i += 1) 
    {
        path = mediaFiles[i].fullPath;
        console.log("Moving...");
        moveVideo(path);
    }
}
function onGalleryDataSuccess (videoURI) {
    console.log("onGalleryDataSuccess")
    window.resolveLocalFileSystemURL(videoURI, resolveVideoOnSuccess, resOnError);
}

function moveVideo(file)
{
    //Fixing the path for some devices
    if( file.indexOf("/private") > -1 )
    {
        file = file.replace("/private","file://");
        console.log("/private replaced by file://");
    }
    
    window.resolveLocalFileSystemURL(file, resolveVideoOnSuccess, resOnError); 
}

function resolveVideoOnSuccess(entry) { 
    console.log("resolveVideoOnSuccess");
    var d = new Date();
    var n = d.getTime();
    
    //Video formats for each platform
    var iosVideoFormat = ".MOV";
    var othersVideoFormat = ".mp4";
    
    var newFileName = n;

    var devicePlatform = device.platform;
    
    if( devicePlatform == "iOS" )
    {
        newFileName += iosVideoFormat;
    }
    else
    {
        newFileName  += othersVideoFormat;
    }

    var myFolderApp = "ClientMediaStorage";
    imageName = n;
    
    console.log("Getting File Sys...");
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {      
    //The folder is created if doesn't exist
    fileSys.root.getDirectory( myFolderApp,
                    {create:true, exclusive: false},
                    function(directory) {
                        entry.moveTo(directory, newFileName, successMove, resOnError);
                    },
                    resOnError);
                    },
    resOnError);
}


/*******************************************************************************************/

function successMove(entry) {
    imageURI = entry.fullPath;
    console.log("Successfully moved to: " + entry.fullPath);

    createJSON();
}


function gotFileS(fileSystem) { 
    rootAppDirectory = fileSystem.root;
    console.log("Root: " + rootAppDirectory); 
} 

/***************************** Creating and writting on JSON File **********************************/
function createJSON()
{
    console.log("creating JSON");
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, onFail);
}
function gotFS(fileSystem) 
{   
    console.log("gotFS");
    var fileName = "MediaData.json";
    //var myFolderApp = "ClientMediaStorage";
    fileSystem.root.getFile(fileName, {create: true, exclusive: false}, gotFileEntry, onFail);

}
function gotFileEntry(fileEntry)
{
    fileEntry.createWriter(gotFileWriter, onFail);
}
function gotFileWriter(writer) 
{
    var jsonString = '{"Path":"'+imageURI+'","Synched":"false","Timestamp":"'+imageName+'"}';   
    var jsonToWrite = JSON.parse(jsonString);
    //goes to the end of the file
    
    console.log("Writting: " + jsonString);

    writer.seek(writer.length); 
    writer.write(jsonString+",");

    writer.onwrite = function(evt) {
        console.log("writed successfully");
    };
}
/**************************************************************************************************/



/****************Error logging******************/
function onFail(message) 
{
    alert('Failed because: ' + message);
}
         
function resOnError(error) {
    alert("Failed... check console" );
    alert(error);
}
/*********************************************/

function uploadImage(imagePath)
{
	var options = new FileUploadOptions();
	options.fileKey="file";
	options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
	options.mimeType="image/jpeg";
	
	var params = new Object();
	params.value1 = "test";
	params.value2 = "param";

	options.params = params;
	options.chunkedMode = false;

	var ft = new FileTransfer();
	ft.upload(imageURI, "http://yourdomain.com/upload.php", win, fail, options);
        
}

function win(r) {
	console.log("Code = " + r.responseCode);
	console.log("Response = " + r.response);
	console.log("Sent = " + r.bytesSent);
	alert(r.response);
}

function fail(error) {
	alert("An error has occurred: Code = " = error.code);
}