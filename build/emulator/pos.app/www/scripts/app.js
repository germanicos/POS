

define(['jquery', 'knockout', 'base'], function($, ko) {



    OrderItem = SimpleControl.extend({
        init: function(DsRegistry) {
console.log('OrderItem init');			
console.log("authCtrl.userInfo.user_id: " + authCtrl.userInfo.user_id);
            this._super( DsRegistry );
            this.customersVMmirror = customersVM;
        //    this.customerOrdersVMmirror = customerOrdersVM;
            // review stuff
            
            this.companyinvoice = ko.observable(false);
            this.invoicecompanyname = ko.observable('');
            this.invoiceproductdescription = ko.observable('');
            
            this.urgent     = ko.observable(false);
            this.future     = ko.observable(false);
            this.acceptTerms= ko.observable(false);
            this.sample 	= ko.observable(false);
            this.sampletext = ko.observable('');
            this.reviewNotes= ko.observable('');
            this.deposit    = ko.observable(0);
            this.discountCode = ko.observable('');
            this.extracost = ko.observable(0);
            this.extracosttext = ko.observable('');
            this.cities     = ko.observableArray([]);//dsRegistry.getDatasource('citiesDS').getStore());
            this.states  	= ko.observableArray([]);//(dsRegistry.getDatasource('statesDS').getStore());
            this.fitting_city = ko.observable('');
            this.order_city = ko.observable('');
            this.paymentMethodList  = ko.observableArray(["Credit card","Bank transfer","Eftpos", "Cheque","Cash"]);
            this.paymentMethod = ko.observable('');
            this.referer    = ko.observable('');
            this.creditCardImage = ko.observable('');
        	this.jacketfirsttime = 0;
        	this.pantsfirsttime = 0;
        	this.suitfirsttime = 0;
        	this.vestfirsttime = 0;
        	this.shirtfirsttime = 0;
       // 	this.fitlinesJacket	= ko.observableArray(dsRegistry.getDatasource('fitlinesJacketDS').getStore());
       // 	this.fitlinesPants	= ko.observableArray(dsRegistry.getDatasource('fitlinesPantsDS').getStore());
        	this.newcustomerorder = false;
        	this.signature = "";
        	this.costanalysis = ko.observable('');
        	this.fitlinevideo = ko.observable('');
        	//this.fitlinevideonotes = ko.observable('');
        	this.garmentvideo = ko.observable('');
        	//this.garmentvideonotes = ko.observable('');
        	this.customervideo = ko.observable('');
        	//this.customervideonotes = ko.observable('');
        	this.videonotes = ko.observable('');
        	
        	this.measurementserrorcheck = 0;
        	this.mediaerrorcheck = 0;
        	this.lockselects = true;	// THIS IS USED TO "LOCK" THE SELECTS OF THE EXTRA PANTS DURING INITIALISATION 
        	
        	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        	//	FITTING VARIABLES & FUNCTIONS
        	/*	MOVED TO Fitting Class of CustomerCntClass
			this.FittingVideoViewModel = function(){
    			var self = this;
    			this.fittingvideolink = ko.observable('');
            	this.fittingvideotext = ko.observable('');
			};
        	this.fittingvideos = ko.observableArray();// ko.observableArray([new this.ExtraCostViewModel()]);        	
			
			this.addFittingVideo = function(){
        		var checkfailed = false;
        		for(var pos = 0; pos < this.fittingvideos().length; pos++){        		
        			if( this.fittingvideos()[pos].fittingvideolink().length == 0 || this.fittingvideos()[pos].fittingvideotext().length == 0 ){
       					checkfailed = true;
       					break;
       				}	
       			}
       			if(checkfailed == false){
       				this.fittingvideos.push(new this.FittingVideoViewModel());
       			}	
    		};
    		
			this.removeFittingVideo = function(pos){
       			this.fittingvideos.remove(that.fittingvideos()[pos]);
    		}; */       	 
        	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        	
        	this.ExtraCostViewModel = function(){
    			var self = this;
    			self.extracosttext = ko.observable('');
    			self.extracost = ko.observable();
			};
        	this.extracosts = ko.observableArray();// ko.observableArray([new this.ExtraCostViewModel()]); 
        	
        	this.extracoststotal = function(){
        		var total = 0;
    			for(var pos = 0; pos < this.extracosts().length; pos++){        		
        			if(this.extracosts()[pos].extracosttext().length > 0 && (this.extracosts()[pos].extracost()*1.0) >= 0){
       					total += this.extracosts()[pos].extracost()*1.0;
       				}	
       			}
       			return total;
			};
        	
        	this.addExtraCost = function(){
        		var checkfailed = false;
        		for(var pos = 0; pos < this.extracosts().length; pos++){        		
        			if(this.extracosts()[pos].extracosttext().length == 0 || (this.extracosts()[pos].extracost()*1.0) <= 0){
       					checkfailed = true;
       					break;
       				}	
       			}
       			if(checkfailed == false){
       				this.extracosts.push(new this.ExtraCostViewModel());
       			}	
    		};
    		
			this.removeExtraCost = function(pos){
       			this.extracosts.remove(that.extracosts()[pos]);
    		};    		
    		
        	
        	this.acceptTermsUpdate= function() {
        		
       			text = "<div><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Terms and Conditions </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">&ldquo;</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Customer&rdquo; </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">means any person who is purchasing items from the Supplier. </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">&ldquo;</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Supplier&rdquo; </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">means </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Germanicos Bespoke Tailors</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">, Australia. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Germanicos may at any time modify any relevant terms and conditions, policies or notices. You acknowledge that by visiting the website from time to time, you shall become bound to the current version of the relevant terms and conditions (the \"current version\") and, unless stated in the current version, all previous versions shall be superseded by the current version. You shall be responsible for reviewing the then current version each time you visit the website. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Lead Time &amp; Delivery of Product </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">It is understood that turnaround and dispatch of the orders placed at the Supplier may vary from time to time depending on current order volumes, work schedules, fabric availability or a particular characteristic of the order placed. The Supplier will try its best to comply with the average turnaround of between 4 and 6 weeks. This is an estimated time frame of delivery, the supplier is not liable for any delays. For guaranteed delivery time lines, we have the </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Express Service</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Express Service - </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Supplier guarantees delivery if a customer opts for this service. The Express Service has an additional fee of $300 AUD. This service will guarantee delivery between 2.5 to 4 weeks. If a customer chooses to pay the express fee, supplier guarantees delivery in the said time frame. We try our best to get the products in time while maintaining the quality, however if for whatever reason we are unable to deliver the product in time, a full refund can be applied upon request. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Express Service </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">has a single fitting policy due to time constrain</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">ts from the customer&rsquo;s side. If </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">there is any alteration to be done to the garments, the customer can always bring the garments at a later stage for any alteration. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Payment Terms </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">As we have to order fabrics for the orders placed with us, we will take a 50% deposit of the total amount and the balance to be paid by the customer on completion or when he/she picks up the garments and/or before we deliver/ship the garments to the customer. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Modes of Payment </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">We accept the following credit cards (Visa, MasterCard &amp; AMEX), EFTPOS and Debit card payments. Customers can also do Bank Transfers (Applicable only for deposits and not for final payments) For all Bank Transfer payments the lead time commences from the time we have clear funds in our account and not from the time of order or when a customer sees us or transfers the money from their account. </span></p></div><div title=\"Page 2\"><div><div><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Design and Style </span></p></div></div><div><div><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">By accepting these terms and conditions on the POS system or otherwise, the customer agrees that he/she is happy with all the design options chosen while placing the order with us. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Once Design &amp; Style options are chosen they cannot be changed after production commences - Approximately 7 days after initial order. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Adjustments </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">100% fit and satisfaction policy </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">After your first appointment with Germanicos, a fitting will be organised once the garment is constructed. During this fitting the tailor will determine with you, whether any adjustments are required. Any adjustments that need to be made are conducted in-house at no cost to the client. We will do as many fittings as needed to get the garments 100% fit. In the rare case that a garment cannot be adjusted, Germanicos will organise for the client to be re-measured and the garment will be re-made. Once again, there is no cost to the client undergoing this process. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Change in Body Measurement </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">As all our products are bespoke, we take various body measurements at the time of new order and make the garments accordingly. However, if the customer fluctuates more than 2 inches (from the time he has placed the order to 1st fitting); Germanicos will try its best to accommodate such changes and offer the best fit for the customer. Germanicos cannot be held liable for garments not fitting perfectly if severe body fluctuations occur. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">GST &amp; Taxes </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">The Australian GST tax rate applies to all orders delivered within Australia. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">International orders are exempt from Australian GST (10%); however they may incur taxes and duties applied by customs in the country where the order is delivered. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Any duties and taxes incurred in the country of destination are the responsibility of the customer. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">If you require fur</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">ther information about your country&rsquo;s taxes and duties you will need to contact </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">your local customs office directly. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">To comply with Australian export regulations, we are required to declare the exact value of all </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">items ordered and to mark them as dutiable &lsquo;merchandise&rsquo;. We are also prohibited by law from marking the order as a &lsquo;gift&rsquo;, even if order is placed with the intention of sending to a gift </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">recipient. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Cancellation </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">As all our products are bespoke, we do not refund or replace for Change of Mind. However, if the order is cancelled within 24 hours from the time order placed and before the supplier has ordered fabric / started the process of making the product, a cancellation fee of $ 100 AUD is applicable which will be charged / deducted from the deposit paid by the customer. </span></p></div></div></div><div title=\"Page 3\"><div><div><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Terms and Conditions of Gift Voucher Purchase &amp; Use </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">1. Germanicos gift vouchers can be exchanged for goods in all Germanicos studios. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">2. Gift vouchers may not be exchanged for cash. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">3. Gift vouchers are valid for 12 months from date of purchase. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">4. If goods purchased value is less than the value of the gift voucher, any balance will be left as a credit for you with Germanicos, and will be redeemed against subsequent future orders. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Trade Marks </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">The trade marks, names, logos and service marks (collectively \"trademarks\") displayed on this website are registered and unregistered trademarks of the Website Owner. Nothing contained on this website should be construed as granting any licence or right to use any trade mark without the prior written permission of the Website Owner. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Legal Jurisdiction </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">All our customer contracts are governed by courts of Victoria, Australia and shall fall under the exclusive jurisdiction of Victorian Courts in Australia. English is the official language offered for the conclusion of the contract. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Disclaimer </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Germanicos strives to give you accurate content, including product information, policies, pricing and visual displays. Policies, pricing, and item availability are subject to change without prior notice. Prices displayed are subject to change and final approval at the time of order fulfilment. Images are shown for representational purposes only. In the event Germanicos is unable to provide an actual image of a product, they reserve the right to substitute a similar image for the one described in the product information. While Germanicos tries in good faith to make sure that the information displayed is accurate, they are not responsible for typographical errors or technical inaccuracies.&nbsp;</span></p></div></div></div>";				
				document.getElementById("termstext").innerHTML = text;
				
				terms = document.getElementById('terms');
				terms.style.display = "block";
				/*
        		if (!confirm(text)) {
            		that.acceptTerms(false);
            		return false;
        		}
        		that.acceptTerms(true);
        		*/
        		return true;
    		};
    		
    		this.buttonNoAcceptTerms = function(){
    			that.acceptTerms(false);
    			terms = document.getElementById('terms');
				terms.style.display = "none";
    		}
    		this.buttonYesAcceptTerms = function(){
    			that.acceptTerms(true);
    			terms = document.getElementById('terms');
				terms.style.display = "none";
    		}
        	
        	
        	 this.syncmeasurementsVM = function() {		
console.log("syncmeasurementsVM " + customersVM.systemMode);
             	this.measurementsVM = new Measurements(dsRegistry, this);
             	
             	if(customersVM.systemMode == "shipping_copy_garment" || customersVM.systemMode == "shipping_remake_garments"){	// AFTER MEASUREMENTS HAVE BEEN LOADED AND systemMode == "shipping_copy_garment", CHANGE TO shipping_copy_garment
             		document.getElementById('loading_jp').style.display = "block";
             		if(customersVM.systemMode == "shipping_copy_garment"){
						$.mobile.changePage('#shipping_copy_garment');
					}else{
						$.mobile.changePage('#shipping_faulty_remake_garments');
					}	
					setTimeout(function() {
		                document.getElementById('loading_jp').style.display = "none";
		            }, 750);
						 
				}	
            };
            
        	 this.syncbodyshapeVM = function() {		
             	this.bodyshapeVM = new Bodyshape(dsRegistry);	 
            };    
            
            this.syncfitlinesJacket = function() {		
             	this.fitlinesJacket = new FitlinesJacket(dsRegistry, this);	 
            };
            
            this.syncfitlinesPants = function() {		
             	this.fitlinesPants = new FitlinesPants(dsRegistry, this);	 
            };              
        
        
            this.months = ko.observableArray([{
                "name":"January",
                "id":"1"
            },{
                "name":"February",
                "id":"2"
            },{
                "name":"March",
                "id":"3"
            },{
                "name":"April",
                "id":"4"
            },{
                "name":"May",
                "id":"5"
            },{
                "name":"June",
                "id":"6"
            },{
                "name":"July",
                "id":"7"
            },{
                "name":"August",
                "id":"8"
            },{
                "name":"September",
                "id":"9"
            },{
                "name":"October",
                "id":"10"
            },{
                "name":"November",
                "id":"11"
            },{
                "name":"December",
                "id":"12"
            }]);
            this.days = ko.observableArray([]);
            for (i=1;i<=31;i++) this.days.push(i.toString());
            this.DOP_day                     = ko.observable('');
            this.DOP_month                   = ko.observable('');
            this.customer_DOP                = ko.computed({                // date of payment
                read: function() {
                    return this.DOP_month() + "-" + this.DOP_day();
                },
                write: function(DOP) {
                    if (DOP) {
                        DOP = DOP.split('-');
                        this.DOP_month(DOP[1]);
                        $('select[name="DOP_month"]').selectmenu('refresh');
                        this.DOP_day(DOP[2]);
                        $('select[name="DOP_day"]').selectmenu('refresh');
                    }
                },
                owner: this
            });
            ////////////////////
this.depositinput = ko.computed({
        read: function () {
            return this.deposit();
        },
        write: function (value) {
        		//if(this.deposit() < value){
                	this.deposit(value);
                //}	 
        },
        owner: this
    });
	/*
    this.depositinput = ko.computed({
        read: function () {
            return this.deposit();
        },
        write: function (value) {
console.log("depositinput value: " + value );
console.log("depositinput deposit: " + this.deposit() );
				if(value == 0){
					this.deposit(0);
        		}else if(this.deposit()*1.0 < value*1.0 || this.deposit()*1.0 > value*2.0 ){
                	this.deposit(value);
                }	 
        },
        owner: this
    });*/

            this.custSelectVM = new CustomerSelectClass(DsRegistry);
            this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);            
            this.custSelectVM.setParentVM(this);
            
            this.custOrdersVM = new CustomerOrdersClass(DsRegistry);
            this.custOrdersVM.subscribeTo('customerOrdersDS');            
            this.custOrdersVM.setParentVM(this);
            
            this.custOrderVM = new CustomerOrderClass(DsRegistry);
            this.custOrderVM.subscribeTo('customerOrderDS');            
            this.custOrderVM.setParentVM(this);
            
            this.custOrderPaymentsVM = new CustomerOrderPaymentsClass(DsRegistry);
            this.custOrderPaymentsVM.subscribeTo('customerOrderPaymentsDS');            
            this.custOrderPaymentsVM.setParentVM(this);
            
            this.OrderPaymentVM = new OrderPaymentClass(DsRegistry);
            this.OrderPaymentVM.subscribeTo('OrderPaymentDS');            
            this.OrderPaymentVM.setParentVM(this);
            
            // old way of fitting
            this.OrderFittingsVM = new OrderFittingsClass(DsRegistry);
            this.OrderFittingsVM.subscribeTo('FittingsDS');            
            this.OrderFittingsVM.setParentVM(this);
            
            this.FittingVM = new FittingClass(DsRegistry);
            this.FittingVM.subscribeTo('FittingDS');            
            this.FittingVM.setParentVM(this);
            
            // new way of fitting
            this.OrdersGarmentsForFittingVM = new OrdersGarmentsForFittingClass(DsRegistry);
            this.OrdersGarmentsForFittingVM.subscribeTo('OrdersGarmentsForFittingDS');            
            this.OrdersGarmentsForFittingVM.setParentVM(this);
            ////////
            this.AlterationsVM = new AlterationsClass(DsRegistry);
            this.AlterationsVM.subscribeTo('AlterationsDS');            
            this.AlterationsVM.setParentVM(this);
            
            this.ShippingFabricVM = new ShippingFabric(DsRegistry);
			//this.ShippingFabricDS = new defShippingFabric('ShippingFabricDS', DsRegistry );
			this.ShippingFabricVM.subscribeTo('ShippingFabricDS');
           	this.ShippingFabricVM.setParentVM(this);
           	
           	this.ShippingCopyGarmentVM = new ShippingCopyGarment(DsRegistry);
			//this.ShippingCopyGarmentDS = new defShippingCopyGarment('ShippingCopyGarmentDS', DsRegistry );
			this.ShippingCopyGarmentVM.subscribeTo('ShippingCopyGarmentDS');
           	this.ShippingCopyGarmentVM.setParentVM(this);
           	
           	this.ShippingFaultyRemakeGarmentsVM = new ShippingFaultyRemakeGarments(DsRegistry);
			//this.ShippingFaultyRemakeGarmentsDS = new defShippingFaultyRemakeGarments('ShippingFaultyRemakeGarmentsDS', DsRegistry );
			this.ShippingFaultyRemakeGarmentsVM.subscribeTo('ShippingFaultyRemakeGarmentsDS');
           	this.ShippingFaultyRemakeGarmentsVM.setParentVM(this);
           	
           	this.ShippingOtherVM = new ShippingOther(DsRegistry);
			//this.ShippingOtherDS = new defShippingOther('ShippingOtherDS', DsRegistry );
			this.ShippingOtherVM.subscribeTo('ShippingOtherDS');
           	this.ShippingOtherVM.setParentVM(this);           	
           
           /* 
            this.fittingsVM = new Fitting(DsRegistry);
            this.fittingsVM.subscribeTo('fittingsDS');            
            this.fittingsVM.setParentVM(this);
            */
            
            /*
            this.customerOrdersVM = new OrdersClass(dsRegistry);
            //this.customerOrdersVM.subscribeTo('customerOrdersDS');
            this.customerOrdersVM.setParentVM(this);
			this.selectedId = 0;
			this.selected_item = ko.observable({});
//			this.selected_item.subscribe( function(data) {
//
//			});            
            */
            
            this.cities_display = ko.computed(function() {
            	this.cities     = ko.observableArray(dsRegistry.getDatasource('citiesDS').getStore());
            	this.states  	= ko.observableArray(dsRegistry.getDatasource('statesDS').getStore());
            	var stateslist = [];			
				var newlist = [];
				if(this.custSelectVM.selectedCustomer().customer_country == undefined){
					return this.cities();
				}else{
					for (i in this.states()){
						if (this.states()[i].parent_location_id == this.custSelectVM.selectedCustomer().customer_country ) {					
							stateslist.push(this.states()[i]);
						}
					}
					for (i in this.cities()){
						for(j in stateslist){
							if (this.cities()[i].parent_location_id == stateslist[j].locations_id ) {					
								newlist.push(this.cities()[i]);
							}
						}	
					}
					if(newlist.length == 0){
						return this.cities();	
					}else{
						return newlist;	
					}							
				}	
	    	}, this);	
            
            
            this.orderMediaNotes = ko.observable('');
           
            this.currentBigStep = ko.observable();

            this.bigSteps		= ko.computed(function() {
            	
                var sC = this.custSelectVM.selectedCustomer();
                console.log('bigsteps');

                customerTitle = ko.computed(function() {
                	
                	if( ($.isEmptyObject(sC) ) ){
                		return "Customer info";
                	}else{
                		if( customersVM.systemMode == "shipping_alterations" || customersVM.systemMode == "shipping_fabrics" ){ 
                			;
                		}else if( customersVM.systemMode == "shipping_faulty_remake_garments" ){
                			; // SYNC A NEW ENDPOINT FOR THE ORDERS' GARMENTS
                		}else if( customersVM.systemMode == "shipping_copy_garment" || customersVM.systemMode == "shipping_remake_garments" ){
                			measurementsDS = new SyncableDatasource('measurementsDS','measurementsDS', dsRegistry, 'customer_measurements_endpoint');
                			measurementsDS.sync();
                		}else{	
                			measurementsDS = new SyncableDatasource('measurementsDS','measurementsDS', dsRegistry, 'customer_measurements_endpoint');
                			measurementsDS.sync();
                		
                			bodyshapeDS = new SyncableDatasource('bodyshapeDS','bodyshapeDS', dsRegistry, 'customer_bodyshape_endpoint');
                			bodyshapeDS.sync();
                		}	
                //		fitlinesJacketDS.sync();
                //		fitlinesPantsDS.sync();
                		return sC.customer_first_name + ' ' + sC.customer_last_name;
                	}
                   // return ($.isEmptyObject(sC))?"Customer info":sC.customer_first_name + ' ' + sC.customer_last_name;
                }, this);
		                     
                return [
	                {
	                    title: customerTitle(),	   
	                    id: 0
	                },
	
	                {
	                    title: "Select Garments",   
	                    id: 1
	                },
	
	                {
	                    title: "Measurements",        
	                    id: 2
	                },
	
	                {
	                    title: "Media",               
	                    id: 3
	                },
	
	                {
	                    title: "Body Shape",          
	                    id: 4
	                },
	
	                {
	                    title: "DESIGN",   
	                    id: 5
	                },	
	
	                {
	                    title: "Payment",              
	                    id: 6
	                }
                ];
            }, this);

            var that = this;

            this.garmentsList  = new ko.observableArray([
	            {
	                id: 0, 
	                title: "Jacket", 
	                image: "http://shirt-tailor.net/thepos/appimg/measurements/jacket.png", 
	                active: false , 
	                count: ko.observable(0)
	            },
	            {
	                id: 1, 
	                title: "Pant"  , 
	                image: "http://shirt-tailor.net/thepos/appimg//measurements/pants.png", 
	                active: false, 
	                count: ko.observable(0)
	            } , 
				{
	                id: 2, 
	                title: "Suit"  , 
	                image: "http://shirt-tailor.net/thepos/appimg/measurements/suit.png", 
	                active: false, 
	                count: ko.observable(0)
	            },
	
	            {
	                id: 3, 
	                title: "Vest"  , 
	                image: "http://shirt-tailor.net/thepos/appimg/measurements/vest.png", 
	                active: false, 
	                count: ko.observable(0)
	            },
	
	            {
	                id: 4, 
	                title: "Shirt" , 
	                image: "http://shirt-tailor.net/thepos/appimg/measurements/shirt.png", 
	                active: false, 
	                count: ko.observable(0)
	            }
	                        
                
                        
                        
            ]);
                
            /*            
var editGarment_tt = 0;
          function editGarment_tlt() {
             
          
             customAlert("call onlu once");
           
           var garmentsPantDS = new defPant('garmentsPantDS', DsRegistry);
	   var garmentsPantVM = new GarmentsPant(DsRegistry);
                //   if (typeof(garmentsPantVM.PantData[0].pantPleats.id) != 'undefined'){
    //  customAlert($("#orderItemSelectGarment").is(".ui-page-active"));
            // if($.mobile.activePage.attr("orderItemSelectGarment") == "page"){
             if($("#orderItemSelectGarment").is(".ui-page-active")){
            customAlert("we are on");
                 editGarment_tt = garmentsPantVM.PantData[0].pantPleats.id; 
             }
                      //          }
            // }
          //  editGarment_tt 
        customAlert(editGarment_tt);
     };
           
         editGarment_tlt();  
           
           */


            this.garments_choice  = new ko.observableArray([
	            {
	                id: 0,
	                pant:ko.observable()
	            },
	
	            {
	                id: 1,
	                pant:"Jacket"
	            }, 
	            {
	                id: 2,
	                pant:"Jacket"
	            },
	            {
	                id: 3,
	                pant:"Jacket"
	            },
	            {
	                id: 4,
	                pant:"Jacket"
	            }
	                             
            ]);


            this.addGarment = function(data) {		
            	
//console.log('addGarment ' + JSON.stringify(data));			
                var ti = that.garmentsList.indexOf(data);                
//console.log('old count: ' + that.garmentsList()[ti].count());
                that.garmentsList()[ti].count( data.count() + 1);
//console.log('new count: ' + that.garmentsList()[ti].count());
                        
            };

            this.removeGarment = function(data) {
//console.log('removeGarment');						
//console.log("data: " + data);
//console.log("data.count():" + data.count());

                var ti = that.garmentsList.indexOf(data);
//console.log(that.garmentsList()[ti].count());
                if (that.garmentsList()[ti].count() != 0) {
                    that.garmentsList()[ti].count( data.count() - 1);
                }
//console.log(that.garmentsList()[ti].count());
            };



            this.atLeastOneGarment = ko.computed(function() {
//console.log('atLeastOneGarment');									
                for (var ind in that.garmentsList() ) {
                    if (that.garmentsList()[ind].count() != 0) {
                        return true; 
                    }
                }
                return false; 
            }, this);
			
			/*
            this.populateGarments = function() {
console.log('populateGarments');									
                for (var ind in that.garmentsList() ) {
                    if (that.garmentsList()[ind].count() != 0) {
                        that.enableGarment({
                            id: that.garmentsList()[ind].id
                        });
                        for (var cc = 0; cc < that.garmentsList()[ind].count() - 1; cc++ ) {
                            that.addVariantToGarment({
                                id: that.garmentsList()[ind].id 
                            });
                        }
                    }
                } 
            };
           */     
 
            this.nextButtonMeasurements = function() {			
                that.currentBigStep(that.bigSteps()[3]);
            };

            this.nextButtonBodyshape = function() {
                that.currentBigStep(that.bigSteps()[4]);
            }
            this.nextButtonMedia = function() {
                that.currentBigStep(that.bigSteps()[6]);
            };
            this.nextButtonCustomer = function() {
                that.currentBigStep(that.bigSteps()[1]);
            }

            this.nextBigStep = function() {
                var cur = jQuery.extend({}, that.currentBigStep());
console.log("cur.id: " + cur.id);
				
				var check = 0;
				 
				if(cur.id == 1){
				
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";
				
				}else if(cur.id == 2){
					if( that.measurementserror().length > 0 && that.measurementserrorcheck == 0 ){ 
						check = 1;	
						that.measurementserrorcheck = 1;
						customAlert( "Measurements missing: \n" +  that.measurementserror() );
					}else{
						check = 0;
						that.updateMeasurements();
					}
				}else if(cur.id == 3){
					if(that.mediaerrorcheck == 0){ 
						var counter = 0;
						thephotos = localStorage.getItem('photos');
						thephotos = (thephotos === null) ? [] : JSON.parse(thephotos);
		                for (var a=thephotos.length - 1; a >= 0; a--){	                    		
		                	if(thephotos[a].customer_id == that.custSelectVM.selectedCustomer().server_id){
		                    	counter++;
		              		}	
		               	}
						if(counter < 3){
							check = 1;
							that.mediaerrorcheck = 1;
							customAlert("You must upload at least 3 images!");
						}
					}else{
						check = 0;
					}	
				}
				
				

                
                if(check == 0){
                	cur.id = cur.id+1;
                	that.currentBigStep(cur);
                }	
            }


			this.openpopup = function(index, element) {  		        	      	
						var popups = document.getElementsByName(element);
						for(var x = 0; x < popups.length; x++){
							popups[x].style.display = "none";
						}
						popups[index].style.display = "block";
			} 
	
			this.closepopup = function(index, element) {  		        	      	
						var popups = document.getElementsByName(element);
						for(var x = 0; x < popups.length; x++){
							popups[x].style.display = "none";
						}
			} 



			
			this.getSellerCurrency = function(){

					var cityid = authCtrl.userInfo.city;
//console.log("citycode: " + cityid);
					var stateid = '';
					var countryid = '';
					var currencyid = '';
					var currency;


					cities = JSON.parse(localStorage.getItem('citiesDS'));
					if(localStorage.getItem('citiesDS') != null){
					    if(localStorage.getItem('citiesDS').length > 0){
						    for(var i=0; i < cities.store.length; i++){
						    	if(cities.store[i].locations_id == cityid){
						    		stateid = cities.store[i].parent_location_id;				    		
						    		break;
						    	}	
						 	}
						}  
				    }

				    states = JSON.parse(localStorage.getItem('statesDS'));
					if(localStorage.getItem('statesDS') != null){
					    if(localStorage.getItem('statesDS').length > 0){
						    for(var i=0; i < states.store.length; i++){
						    	if(states.store[i].locations_id == stateid){
						    		countryid = states.store[i].parent_location_id;
						    		break;
						    	}	
						 	}
						}  
				    }									

				    countries = JSON.parse(localStorage.getItem('countriesDS'));
					if(localStorage.getItem('countriesDS') != null){
					    if(localStorage.getItem('countriesDS').length > 0){
						    for(var i=0; i < countries.store.length; i++){
						    	if(countries.store[i].locations_id == countryid){
						    		currencyid = countries.store[i].currency_id;
						    		break;
						    	}	
						 	}
						}  
				    }	
				    
				    currencies = JSON.parse(localStorage.getItem('currenciesDS'));
					if(localStorage.getItem('currenciesDS') != null){
					    if(localStorage.getItem('currenciesDS').length > 0){
						    for(var i=0; i< currencies.store.length; i++){
						    	if(currencies.store[i].currency_id == currencyid){
						    		currency = currencies.store[i];
						    		break;
						    	}	
						 	}
						}  
				    }				    
				    
					return currency;	// its a json {"currency_id":"5","title":"Australian Dollar","code":"AUD","symbol":"Α$","id":3}
			}


			Number.prototype.round = function(places){
			    places = Math.pow(10, places); 
			    return Math.round(this * places)/places;
			}

			
			this.mobileInputChange = function(mobile){
				var phone = mobile.value.replace(/[^0-9]/g,'');
				if(phone.length > 7){
					phone = phone.slice(0,7) + '-' + phone.slice(7);
				}
				if(phone.length > 4){
					phone = phone.slice(0,4) + '-' + phone.slice(4);
				}
				mobile.value = phone;
			};
			
			this.checkGarmentsForFabrics = function(){
				if (that.garmentsList()[0].count() > 0) {
                    for (i in that.dumpOrder().Jacket.Jacket) {                    	
                        if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.title == "none"){
                        	return false;
                        }	    
					}
				}	
				if (that.garmentsList()[1].count() > 0) {
                    for (i in that.dumpOrder().Pant.Pant) {                    	
                        if (that.dumpOrder().Pant.Pant[i].PantFabric.title == "none"){
							return false;
						}
					}
				}		
				if (that.garmentsList()[2].count() > 0) {
					for (i in that.dumpOrder().Suit.suit) {						
                        if (that.dumpOrder().Suit.suit[i].suitFabric.title == "none"){
                        	return false;
                        }
					}
				}			                        	
				if (that.garmentsList()[3].count() > 0) {
                    for (i in that.dumpOrder().Vest.vest) {	                    	
                        if (that.dumpOrder().Vest.vest[i].vestFabric.title == "none"){
                        	return false;
                        }
					}
				}	
				if (that.garmentsList()[4].count() > 0) {
                    for (i in that.dumpOrder().Shirt.Shirt) {                    	
                        if (that.dumpOrder().Shirt.Shirt[i].shirtFabric.title == "none"){
							return false;
						}
					}
				}
				return true;
			}

	        
	        this.kissingButtonsExtraCost = function(){
	        	currency = that.getSellerCurrency();
	        	var pricesarray = [];
				pricerange = JSON.parse(localStorage.getItem('pricerangeDS'));
				if(localStorage.getItem('pricerangeDS') != null){
				    if(localStorage.getItem('pricerangeDS').length > 0){
					    for(var i=0; i< pricerange.store.length; i++){
					    	if(pricerange.store[i].price_range_currency == currency.code){
					    		pricesarray.push(pricerange.store[i]);
					    	}
					 	}
					}
				}
				for(x = 0; x < pricesarray.length; x++){
					if(	pricesarray[x].price_range_garment_name == 'Kissing Buttons' ){
		            	return pricesarray[x].price_range_price * 1.0;
		        	}
				}
	        }
	        
            
			this.computePrice = function() {
				
				currency = that.getSellerCurrency();				
				var pricesarray = [];
				pricerange = JSON.parse(localStorage.getItem('pricerangeDS'));
				if(localStorage.getItem('pricerangeDS') != null){
				    if(localStorage.getItem('pricerangeDS').length > 0){
					    for(var i=0; i< pricerange.store.length; i++){
					    	if(pricerange.store[i].price_range_currency == currency.code){
					    		pricesarray.push(pricerange.store[i]);
					    	}
					 	}
					}
				}
				var analysis = [];
                var price = 0;
                var text = "";
                if (that.garmentsList()[0].count() > 0) {
                    for (i in that.dumpOrder().Jacket.Jacket) {
                    	jacketprice = 0;                    	
                        if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.price){                       	
                        //   price = price +  that.dumpOrder().Jacket.Jacket[i].JacketFabric.price * 1.0;	
                        	for(x = 0; x < pricesarray.length; x++){
                        		
                        		if(	that.dumpOrder().Jacket.Jacket[i].JacketFabric.price_range ==  pricesarray[x].price_range_name	&& pricesarray[x].price_range_garment_name == 'Jacket' ){
                        			jacketprice = jacketprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
                        			analysis.push('<h4  class="grid_12"><div class="price_type grid_8">1x ' + pricesarray[x].price_range_name + ' Jacket (' +  that.dumpOrder().Jacket.Jacket[i].JacketFabric.title + ') </div><div class="price_itself grid_3"> '  + pricesarray[x].price_range_price  +'&nbsp;'+ currency.symbol + '</div></h4>');
									break;
                        		}
                        	}
                            analysis.push('<ul class="grid_12">');
	                        if(that.dumpOrder().Jacket.Jacket[i].JacketVentStyle.id == '123'){
	                        	if( that.dumpOrder().Jacket.Jacket[i].JacketTail == true){
	                        		//price = price * 1.30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Tail Suits' ){
		                        	 		extracost = jacketprice * (pricesarray[x].price_range_price * 1.0 )/100.0;
		                        			jacketprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x Jacket Tail </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price  +'&nbsp;'+ currency.symbol + '</div></li>');
											break;
		                        		}
		                        	}	                        		
	                        		
	                        	}
	                        }	                            
                            
                      //      if (that.dumpOrder().Jacket.Jacket[i].JacketDinner == true){
	                  //      	price = price + 200;
	                  //      }
	                        if (that.dumpOrder().Jacket.Jacket[i].JacketKissingButtons == true){
	                        	//price = price + 30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Kissing Buttons' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        			jacketprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price  +'&nbsp;'+ currency.symbol + '</div></li>');
											break;
		                        		}
		                        	}		                        	
	                        	
	                        }
	                      //  if (that.dumpOrder().Jacket.Jacket[i].JacketContrastTrimming == true){
	                        	if( that.dumpOrder().Jacket.Jacket[i].JacketCollarTrimming == true || that.dumpOrder().Jacket.Jacket[i].JacketLapelTrimming == true || 
	                        		that.dumpOrder().Jacket.Jacket[i].JacketCoverButtons == true || that.dumpOrder().Jacket.Jacket[i].JacketPocketTrimming == true){
										//price = price + 200;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Satin Trimming' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        			jacketprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li>');
											break;
		                        		}
		                        	}
	                        	}
	                     //   }	
	                        	    
							if(that.dumpOrder().Jacket.Jacket[i].JacketLining.price == 'yes'){
								//price = price + 100;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Silk Lining' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        			jacketprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li>');
											break;
		                        		}
		                        	}
							}	                        	    
	                        	                        
	                        if( /*that.dumpOrder().Jacket.Jacket[i].JacketTopStitch == true &&*/ (that.dumpOrder().Jacket.Jacket[i].JacketTopStitchPocket == true || that.dumpOrder().Jacket.Jacket[i].JacketTopStitchLapel == true) ){
	                        	//price = price + 90;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Top Stitch' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        			jacketprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li>');
											break;
		                        		}
		                        	}	                        	
	                        }	                        
	                        
	                        if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogram == true){
	                        	if(that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage != ""){
//console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image == that.dumpOrder().Jacket.Jacket[j].JacketCustomMonogramImage.image){
											image_already_exists = true;
											break;
										}
									}
									if(image_already_exists == false){
									//	price = price + 100;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        				jacketprice += extracost;
		                        				analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> '  + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol + '</div></li>');
												break;
			                        		}
			                        	}										
										
									}
	                        	}
	                        }
	                        
	                        if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroidery == true){
	                        	if(that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage != ""){
//console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image == that.dumpOrder().Jacket.Jacket[j].JacketEmbroideryImage.image){
											image_already_exists = true;
											break;
										}
									}
									if(image_already_exists == false){
										//price = price + 100;	
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        				jacketprice += extracost;
		                        				analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> '  + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol + '</div></li>');
												break;
			                        		}
			                        	}										
									}
	                        	}
	                        }	                        

                        }
                        price += jacketprice;
                        analysis.push('</ul>');
                    }
                }       
                
                if (that.garmentsList()[1].count() > 0) {
                    for (i in that.dumpOrder().Pant.Pant) {                                        	   	
                        if (that.dumpOrder().Pant.Pant[i].PantFabric.price){
                          //  price = price +  that.dumpOrder().Pant.Pant[i].PantFabric.price * 1.0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Pant.Pant[i].PantFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Pants' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
                        			analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x ' + pricesarray[x].price_range_name + ' Pants (' +  that.dumpOrder().Pant.Pant[i].PantFabric.title + ') </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  +  '</div></h4>');
                        			//analysis.push('<ul style="margin-top:-20px">');
                        			//analysis.push('</ul>');
									break;
                        		}
                        	}
                        }
                    }
                }

                if (that.garmentsList()[4].count() > 0) {
                    for (i in that.dumpOrder().Shirt.Shirt) {                	
                        if (that.dumpOrder().Shirt.Shirt[i].shirtFabric.price && that.dumpOrder().Shirt.Shirt[i].shirtFabric.title != 'none'){
                            //price = price +  that.dumpOrder().Shirt.Shirt[i].shirtFabric.price * 1.0;
                            /*
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Shirt.Shirt[i].shirtFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Shirt' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
                        		}
                        	}
                        	*/
                        	var platinumprice = 0;
                        	var silverprice = 0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Platinum'){
                        			platinumprice = pricesarray[x].price_range_price * 1.0;
                        		}
                        		if(pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Silver'){
                        			silverprice = pricesarray[x].price_range_price * 1.0;
                        		}
                        	}
                        	count = i* 1.0 + 1;
                        	text += "Shirt " + count + ":\n";
                        	if(that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment == true){
                        		price += platinumprice;
                        		analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x Platinum Make Shirt (' +  that.dumpOrder().Shirt.Shirt[i].shirtFabric.title + ') </div><div class="price_itself grid_3"> ' + platinumprice  +'.00&nbsp;'+ currency.symbol  + '</div></h4>');
                        	}else{
                        		price += silverprice;
                        		analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x Standard Make Shirt (' +  that.dumpOrder().Shirt.Shirt[i].shirtFabric.title + ') </div><div class="price_itself grid_3"> ' + silverprice +'.00&nbsp;'+ currency.symbol  + '</div></h4>');
                        	}
                     //   	analysis.push('<ul>');
                       // 	analysis.push('</ul>');
                       
                        }
                        
                    }
                }
                
                if (that.garmentsList()[2].count() > 0) {
					for (i in that.dumpOrder().Suit.suit) {
						suitprice = 0;
						extrajacketprice = 0;
						extrapantsprice = 0;		
                        if (that.dumpOrder().Suit.suit[i].suitFabric.price){
                            //price = price +  that.dumpOrder().Suit.suit[i].suitFabric.price * 1.0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Suit.suit[i].suitFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Suit' ){
                        			suitprice = suitprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
                        			analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x Bespoke ' + pricesarray[x].price_range_name + ' Suit (' +  that.dumpOrder().Suit.suit[i].suitFabric.title + ') </div><div class="price_itself grid_3">' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></h4>');
									break;
                        		}
                        	}
                        	analysis.push('<ul class="grid_12">');
                        	/*
                        	for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
	                        	for(x = 0; x < pricesarray.length; x++){
	                        		if(	that.dumpOrder().Suit.suit[i].suitFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Jacket' ){
	                        			extrajacketprice = extrajacketprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
										break;
	                        		}
	                        	}
                            }
                        	for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraPants; a++){
	                        	for(x = 0; x < pricesarray.length; x++){
	                        		if(	that.dumpOrder().Suit.suit[i].suitFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Pants' ){
	                        			extrapantsprice = extrapantsprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
										break;
	                        		}
	                        	}
                            }                            
                            */
	                        if(that.dumpOrder().Suit.suit[i].suitJacketVentStyle.id == '123'){
	                        	if( that.dumpOrder().Suit.suit[i].suitJacketTail == true){
	                        		//price = price * 1.30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Tail Suits' ){
		                        			extracost = suitprice * (pricesarray[x].price_range_price * 1.0 )/100.0;
		                        			suitprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x Suit Tail </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price  +'&nbsp;'+ currency.symbol + '</div></li>');
		                        			/*
		                        			if(that.dumpOrder().Suit.suit[i].ExtraJacket >0){
		                        				extrajacketprice += extrajacketprice * ((pricesarray[x].price_range_price * 1.0 )/100.0);
		                        			}*/	
											break;
		                        		}
		                        	}	                    	
		                        	                        		
	                        	}
	                        }	
	                                                    
                   //         if (that.dumpOrder().Suit.suit[i].suitJacketDinner == true){
	               //         	price = price + 200;
	               //         }
	                        if (that.dumpOrder().Suit.suit[i].suitJacketKissingButtons == true){
	                        	//price = price + 30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Kissing Buttons' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
		                        			suitprice += extracost;
		                        			analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li>');
		                        			/*
		                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
		                        				extrajacketprice += pricesarray[x].price_range_price * 1.0;
		                        			}*/			                        			
											break;
		                        		}
		                        	}	                        	
	                        }
	                   //     if (that.dumpOrder().Suit.suit[i].suitJacketContrastTrimming == true){
	                        	if( that.dumpOrder().Suit.suit[i].suitJacketCollarTrimming == true || that.dumpOrder().Suit.suit[i].suitJacketLapelTrimming == true || 
	                        		that.dumpOrder().Suit.suit[i].suitJacketCoverButtons == true || that.dumpOrder().Suit.suit[i].suitJacketPocketTrimming == true){
										//price = price + 200;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Satin Trimming' ){
			                        			extracost = pricesarray[x].price_range_price * 1.0;
												suitprice += extracost;
												analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price  +'&nbsp;'+ currency.symbol + '</div></li>');
			                        			/*
			                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
			                        				extrajacketprice += pricesarray[x].price_range_price * 1.0;
			                        			}*/			                        			
												break;
			                        		}
			                        	}
	                        	}
	                   //     }	
	                        	 
							if(that.dumpOrder().Suit.suit[i].suitJacketLining.price == 'yes'){
								//price = price + 100;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Silk Lining' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
											suitprice += extracost;
											analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li>');
		                        			/*
			                        		for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
			                        			extrajacketprice += pricesarray[x].price_range_price * 1.0;
			                        		}*/			                        			
											break;
		                        		}
		                        	}								
							}	 
							
	                        if( /*that.dumpOrder().Suit.suit[i].suitJacketTopStitch == true &&*/ (that.dumpOrder().Suit.suit[i].suitJacketTopStitchPocket == true || that.dumpOrder().Suit.suit[i].suitJacketTopStitchLapel == true) ){
	                        	//price = price + 90;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Top Stitch' ){
		                        			extracost = pricesarray[x].price_range_price * 1.0;
											suitprice += extracost;
											analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price  +'&nbsp;'+ currency.symbol + '</div></li>');
		                        			/*
		                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
			                        			extrajacketprice += pricesarray[x].price_range_price * 1.0;
			                        		}*/
											break;
		                        		}
		                        	}	                        	
	                        }	                        
	                        
	                        if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogram == true){
	                        	if(that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage != ""){
//console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image == that.dumpOrder().Suit.suit[j].suitJacketCustomMonogramImage.image){
											image_already_exists = true;
											break;
										}
									}
									if(image_already_exists == false){
									//	price = price + 100;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
												extracost = pricesarray[x].price_range_price * 1.0;
												suitprice += extracost;
												analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li>');			                        			
												break;
			                        		}
			                        	}									
									}
	                        	}
	                        }
	                        
	                        if (that.dumpOrder().Suit.suit[i].suitJacketEmbroidery == true){
	                        	if(that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage != ""){
//console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image == that.dumpOrder().Suit.suit[j].suitJacketEmbroideryImage.image){
											image_already_exists = true; 
											break;
										}
									}
									if(image_already_exists == false){
										//price = price + 100;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			extracost = pricesarray[x].price_range_price * 1.0;
												suitprice += extracost;
												analysis.push('<li>1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</li>');
												break;	
			                        		}
			                        	}	
									}
	                        	}
	                        }
	                        /*
	                       	if(extrajacketprice > 0){
                        		analysis.push('<li><div class="price_type grid_8">' + that.dumpOrder().Suit.suit[i].ExtraJacket + 'x Extra Jackets </div><div class="price_itself grid_3"> ' + extrajacketprice +'&nbsp;'+ currency.symbol  + '</div></li>');
                        	}
                        	if(extrapantsprice > 0){
                        		analysis.push('<li><div class="price_type grid_8">' + that.dumpOrder().Suit.suit[i].ExtraPants + 'x Extra Pants </div><div class="price_itself grid_3"> ' + extrapantsprice  +'&nbsp;'+ currency.symbol + '</div></li>');
                        	}*/
                        	analysis.push('</ul>');
                        }
                        
                        price = price + suitprice + extrajacketprice + extrapantsprice;
                    }
                    
                }
                
                if (that.garmentsList()[3].count() > 0) {
                    for (i in that.dumpOrder().Vest.vest) {
                        if (that.dumpOrder().Vest.vest[i].vestFabric.price){
                            //price = price +  that.dumpOrder().Vest.vest[i].vestFabric.price * 1.0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Vest.vest[i].vestFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Vest' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
                        			analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x ' + pricesarray[x].price_range_name + ' Vest (' +  that.dumpOrder().Vest.vest[i].vestFabric.title + ') </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></h4>');
                        		//	analysis.push('<ul style="margin-top:-20px">');
                        		//	analysis.push('</ul>');
									break;
                        		}
                        	}

							if(that.dumpOrder().Vest.vest[i].vestLining.price == 'yes' && that.dumpOrder().Vest.vest[i].parentSuitTimestampID == ''){	
								for(x = 0; x < pricesarray.length; x++){
		                       		if(	pricesarray[x].price_range_garment_name == 'Silk Lining' ){
		                       			extracost = pricesarray[x].price_range_price * 1.0;
										price += extracost;
										analysis.push('<ul class="grid_12"><li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price +'&nbsp;'+ currency.symbol  + '</div></li></ul>');			                        			
										break;
		                       		}
		                       	}								
							}
                        }
                    }
                }
                
                /*
				if(that.urgent() == true){			
			       	price += that.urgentCost();
			       	analysis.push("<h4>Express Delivery = " + that.urgentCost()  + '</h4>')
				}*/
				
				var urgent = false;
				if (that.garmentsList()[0].count() > 0) {
                    for (i in that.dumpOrder().Jacket.Jacket) {                    	
                        if (that.dumpOrder().Jacket.Jacket[i].urgent == 1){
                        	urgent = true;
                        	break;
                        }	    
					}
				}	
				if (that.garmentsList()[1].count() > 0 && urgent == false) {
                    for (i in that.dumpOrder().Pant.Pant) {                    	
                        if (that.dumpOrder().Pant.Pant[i].urgent == 1){
                        	urgent = true;
                        	break;
                        }	    
					}
				}		
				if (that.garmentsList()[2].count() > 0 && urgent == false) {
					for (i in that.dumpOrder().Suit.suit) {						
                        if (that.dumpOrder().Suit.suit[i].urgent == 1){
                        	urgent = true;
                        	break;
                        }	    
					}
				}			                        	
				if (that.garmentsList()[3].count() > 0 && urgent == false) {
                    for (i in that.dumpOrder().Vest.vest) {	                    	
                        if (that.dumpOrder().Vest.vest[i].urgent == 1){
                        	urgent = true;
                        	break;
                        }	    
					}
				}	
				if (that.garmentsList()[4].count() > 0 && urgent == false) {
                    for (i in that.dumpOrder().Shirt.Shirt) {                    	
                        if (that.dumpOrder().Shirt.Shirt[i].urgent == 1){
                        	urgent = true;
                        	break;
                        }	    
					}
				}				
				
				if(urgent == true){			
			       	price += that.urgentCost();
			       	analysis.push('<h4 class="grid_12"><div class="price_type grid_8">Express Delivery </div><div class="price_itself grid_3"> ' + that.urgentCost().toFixed(2) +'&nbsp;'+ currency.symbol  + '</div></h4>')
				}
				
				
//console.log(that.extracost());				
				price += that.extracoststotal() * 1.0; 

				analysistext = "";
				for(x = 0; x < analysis.length; x ++){
					analysistext += analysis[x];
				}
				if(analysistext.length < 2){
					analysistext = "-";
				}

				that.costanalysis(analysistext);

			/*	if(that.calculateDiscount() <= price){			
					price = price - that.calculateDiscount();
				}	*/
                return price.round(2); 
            }            
            
            this.computePriceWithDiscount = function() {
            	var price = that.computePrice();
            	if(that.computePrice() >= that.calculateDiscount()){
            		var price = that.computePrice() - that.calculateDiscount();
            	}	
            	return price.round(2);
            }
            

/////////////////////////////////////            
            
           // this.computePriceForGarment = ko.computed(function(garment, index) {
			this.computePriceForGarment = function(garment, index) {

				currency = that.getSellerCurrency();			

				var pricesarray = [];
				pricerange = JSON.parse(localStorage.getItem('pricerangeDS'));
				if(localStorage.getItem('pricerangeDS') != null){
				    if(localStorage.getItem('pricerangeDS').length > 0){
				    	for(var i=0; i< pricerange.store.length; i++){
					    	if(pricerange.store[i].price_range_currency == currency.code){
				    			pricesarray.push(pricerange.store[i]);
				    		}
				 		}
					}
				}
				var price = 0;
				var extrajacketprice = 0;
				var extrapantsprice = 0;
			
				if(garment == "Jacket"){
                
                	if (that.garmentsList()[0].count() > 0) {
                    	i = index;
                        if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.price){
                        //   price = price +  that.dumpOrder().Jacket.Jacket[i].JacketFabric.price * 1.0;	
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Jacket.Jacket[i].JacketFabric.price_range ==  pricesarray[x].price_range_name	&& pricesarray[x].price_range_garment_name == 'Jacket' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
                        		}
                        	}
                            
	                        if(that.dumpOrder().Jacket.Jacket[i].JacketVentStyle.id == '123'){
	                        	if( that.dumpOrder().Jacket.Jacket[i].JacketTail == true){
	                        		//price = price * 1.30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Tail Suits' ){
		                        			price += price * ((pricesarray[x].price_range_price * 1.0 )/100.0);
											break;
		                        		}
		                        	}	                        		
	                        		
	                        	}
	                        }	                            
                            
                      //      if (that.dumpOrder().Jacket.Jacket[i].JacketDinner == true){
	                  //      	price = price + 200;
	                  //      }
	                        if (that.dumpOrder().Jacket.Jacket[i].JacketKissingButtons == true){
	                        	//price = price + 30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Kissing Buttons' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
											break;
		                        		}
		                        	}		                        	
	                        	
	                        }
	                     //   if (that.dumpOrder().Jacket.Jacket[i].JacketContrastTrimming == true){
	                        	if( that.dumpOrder().Jacket.Jacket[i].JacketCollarTrimming == true || that.dumpOrder().Jacket.Jacket[i].JacketLapelTrimming == true || 
	                        		that.dumpOrder().Jacket.Jacket[i].JacketCoverButtons == true || that.dumpOrder().Jacket.Jacket[i].JacketPocketTrimming == true){
										//price = price + 200;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Satin Trimming' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
											break;
		                        		}
		                        	}
	                        	}
	                    //    }	
	                        	    
							if(that.dumpOrder().Jacket.Jacket[i].JacketLining.price == 'yes'){
								//price = price + 100;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Silk Lining' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
											break;
		                        		}
		                        	}
							}	                        	    
	                        	                        
	                        if( /*that.dumpOrder().Jacket.Jacket[i].JacketTopStitch == true &&*/ (that.dumpOrder().Jacket.Jacket[i].JacketTopStitchPocket == true || that.dumpOrder().Jacket.Jacket[i].JacketTopStitchLapel == true) ){
	                        	//price = price + 90;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Top Stitch' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
											break;
		                        		}
		                        	}	                        	
	                        }	                        
	                        
	                        if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogram == true){
	                        	if(that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage != ""){
console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image == that.dumpOrder().Jacket.Jacket[j].JacketCustomMonogramImage.image){
											image_already_exists = true;
											break;
										}
									}
									if(image_already_exists == false){
									//	price = price + 100;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			price += pricesarray[x].price_range_price * 1.0;
												break;
			                        		}
			                        	}										
										
									}
	                        	}
	                        }
	                        
	                        if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroidery == true){
	                        	if(that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage != ""){
console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image == that.dumpOrder().Jacket.Jacket[j].JacketEmbroideryImage.image){
											image_already_exists = true;
											break;
										}
									}
									if(image_already_exists == false){
										//price = price + 100;	
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			price += pricesarray[x].price_range_price * 1.0;
												break;
			                        		}
			                        	}										
									}
	                        	}
	                        }	                        
    	                }
               		}
            	}else if(garment == "Pants"){    
                	if (that.garmentsList()[1].count() > 0) {
                    	i = index;
                        if (that.dumpOrder().Pant.Pant[i].PantFabric.price){
                          //  price = price +  that.dumpOrder().Pant.Pant[i].PantFabric.price * 1.0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Pant.Pant[i].PantFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Pants' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
                        		}
                        	}
                        }
	                }
             	}else if(garment == "Shirt"){   
                	if (that.garmentsList()[4].count() > 0) {
                    	i = index;
                        if (that.dumpOrder().Shirt.Shirt[i].shirtFabric.price){
                        	if(that.dumpOrder().Shirt.Shirt[i].shirtFabric.title != 'none'){
	                            //price = price +  that.dumpOrder().Shirt.Shirt[i].shirtFabric.price * 1.0;
	                            /*
	                        	for(x = 0; x < pricesarray.length; x++){
	                        		if(	that.dumpOrder().Shirt.Shirt[i].shirtFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Shirt' ){
	                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
										break;
	                        		}
	                        	} */
	
								var platinumprice = 0;
	                        	var silverprice = 0;
	                        	for(x = 0; x < pricesarray.length; x++){
	                        		if(pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Platinum'){
	                        			platinumprice = pricesarray[x].price_range_price * 1.0;
	                        		}
	                        		if(pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Silver'){
	                        			silverprice = pricesarray[x].price_range_price * 1.0;
	                        		}
	                        	}
console.log("that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment: " + that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment);	                        	
	                        	if(that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment == true){
	                        		price += platinumprice;
	                        	}else{
	                        		price += silverprice;
	                        	}
	                        }	                        	
                        }
                	}
             	}else if(garment == "Suit"){            		
                	if (that.garmentsList()[2].count() > 0) {
						i = index;
                        if (that.dumpOrder().Suit.suit[i].suitFabric.price){
                            //price = price +  that.dumpOrder().Suit.suit[i].suitFabric.price * 1.0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Suit.suit[i].suitFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Suit' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
                        		}
                        	}
                        	/*
                        	for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
	                        	for(x = 0; x < pricesarray.length; x++){
	                        		if(	that.dumpOrder().Suit.suit[i].suitFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Jacket' ){
	                        			extrajacketprice = extrajacketprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
										break;
	                        		}
	                        	}
                            }
                        	for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraPants; a++){
	                        	for(x = 0; x < pricesarray.length; x++){
	                        		if(	that.dumpOrder().Suit.suit[i].suitFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Pants' ){
	                        			extrapantsprice = extrapantsprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
										break;
	                        		}
	                        	}
                            }                         	
                        	*/
                            
	                        if(that.dumpOrder().Suit.suit[i].suitJacketVentStyle.id == '123'){
	                        	if( that.dumpOrder().Suit.suit[i].suitJacketTail == true){
	                        		//price = price * 1.30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Tail Suits' ){
		                        			price += price * ((pricesarray[x].price_range_price * 1.0 )/100.0);
		                        			/*
		                        			if(that.dumpOrder().Suit.suit[i].ExtraJacket >0){
		                        				extrajacketprice += extrajacketprice * ((pricesarray[x].price_range_price * 1.0 )/100.0);
		                        			}*/			                        			
											break;
		                        		}
		                        	}	                        		
	                        	}
	                        }	
	                                                    
                   //         if (that.dumpOrder().Suit.suit[i].suitJacketDinner == true){
	               //         	price = price + 200;
	               //         }
	                        if (that.dumpOrder().Suit.suit[i].suitJacketKissingButtons == true){
	                        	//price = price + 30;
		                        	for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Kissing Buttons' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
		                        			/*
		                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
		                        				extrajacketprice += pricesarray[x].price_range_price * 1.0;
		                        			}*/
											break;
		                        		}
		                        	}	                        	
	                        }
	                     //   if (that.dumpOrder().Suit.suit[i].suitJacketContrastTrimming == true){
	                        	if( that.dumpOrder().Suit.suit[i].suitJacketCollarTrimming == true || that.dumpOrder().Suit.suit[i].suitJacketLapelTrimming == true || 
	                        		that.dumpOrder().Suit.suit[i].suitJacketCoverButtons == true || that.dumpOrder().Suit.suit[i].suitJacketPocketTrimming == true){
										//price = price + 200;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Satin Trimming' ){
			                        			price += pricesarray[x].price_range_price * 1.0;
			                        			/*
			                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
			                        				extrajacketprice += pricesarray[x].price_range_price * 1.0;
			                        			}*/
												break;
			                        		}
			                        	}
	                        	}
	                   //     }	
	                        	 
							if(that.dumpOrder().Suit.suit[i].suitJacketLining.price == 'yes'){
								//price = price + 100;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Silk Lining' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
		                        			/*
		                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
			                        				extrajacketprice += pricesarray[x].price_range_price * 1.0;
			                        		}*/
											break;
		                        		}
		                        	}								
							}	 
							
	                        if( /*that.dumpOrder().Suit.suit[i].suitJacketTopStitch == true &&*/ (that.dumpOrder().Suit.suit[i].suitJacketTopStitchPocket == true || that.dumpOrder().Suit.suit[i].suitJacketTopStitchLapel == true) ){
	                        	//price = price + 90;
									for(x = 0; x < pricesarray.length; x++){
		                        		if(	pricesarray[x].price_range_garment_name == 'Top Stitch' ){
		                        			price += pricesarray[x].price_range_price * 1.0;
		                        			/*
		                        			for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
			                        				extrajacketprice += pricesarray[x].price_range_price * 1.0;
			                        		}*/
											break;
		                        		}
		                        	}	                        	
	                        }	                        
	                        
	                        if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogram == true){
	                        	if(that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage != ""){
console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image == that.dumpOrder().Suit.suit[j].suitJacketCustomMonogramImage.image){
											image_already_exists = true;
											break;
										}
									}
									if(image_already_exists == false){
									//	price = price + 100;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			price += pricesarray[x].price_range_price * 1.0;
												break;
			                        		}
			                        	}									
									}
	                        	}
	                        }
	                        
	                        if (that.dumpOrder().Suit.suit[i].suitJacketEmbroidery == true){
	                        	if(that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage != ""){
console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image);
									image_already_exists = false;
									for(j = 0; j < i; j++){
										if(that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image == that.dumpOrder().Suit.suit[j].suitJacketEmbroideryImage.image){
											image_already_exists = true; 
											break;
										}
									}
									if(image_already_exists == false){
										//price = price + 100;
										for(x = 0; x < pricesarray.length; x++){
			                        		if(	pricesarray[x].price_range_garment_name == 'Custom Monogram' ){
			                        			price += pricesarray[x].price_range_price * 1.0;
												break;
			                        		}
			                        	}	
									}
	                        	}
	                        }	                        
                        }
                	}
				}else if(garment == "Vest"){
                	if (that.garmentsList()[3].count() > 0) {
                    	i = index;
                        if (that.dumpOrder().Vest.vest[i].vestFabric.price){
                            //price = price +  that.dumpOrder().Vest.vest[i].vestFabric.price * 1.0;
                        	for(x = 0; x < pricesarray.length; x++){
                        		if(	that.dumpOrder().Vest.vest[i].vestFabric.price_range ==  pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Vest' ){
                        			price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
                        		}
                        	} 
													
							if(that.dumpOrder().Vest.vest[i].vestLining.price == 'yes' && that.dumpOrder().Vest.vest[i].parentSuitTimestampID == ''){
								for(x = 0; x < pricesarray.length; x++){
	                        		if(	pricesarray[x].price_range_garment_name == 'Silk Lining' ){
	                        			price += pricesarray[x].price_range_price * 1.0;
										break;
	                        		}
	                        	}								
							}								

                        }
                    }
				}

//that.calculateDiscount("1426A0455A");
				total = price + extrapantsprice + extrajacketprice; 
	            return total.round(2);
		}            
	//	}, this);     
         
        this.extracostFunction = function(){
        	return ((0 + that.extracoststotal())/1);
        }    
               
		this.calculateDiscount = function(){	//1205J0234T00  12: first 2 ammount digits,	05:	day,	A: seller name is JAdmin,	02: month,	34: next 2 ammount digits,	T: seller last name is TAdmin, 00: decimal ammount
			
			var code = that.discountCode();
			if(code == "Freelining"){
				return 100;
			}else if(code == "FreeShirt"){
				return 190;	
			}else if(code == "FreeLinShi"){
				return 290;	
			}else if(code == null || code == undefined || code.length < 12){
				return 0;
			}else{

				var amount = code.substr(0,2) + code.substr(7,2) + "." + code.substr(10,2);
				
				var codeday = code.substr(2,2);
				var codemonth = code.substr(5,2);
				var sellerFirstNameFD = code.substr(4,1).toLowerCase();
				var sellerLastNameFD = code.substr(9,1).toLowerCase();			
				var first_nameFD = authCtrl.userInfo.first_name.substr(0,1).toLowerCase();
				var last_nameFD = authCtrl.userInfo.last_name.substr(0,1).toLowerCase();
				var day = new Date().getDate();
				var month = new Date().getMonth() + 1;
				if(day < 10){
					day = "0" + day;
				}else{
					day = "" + day;
				}
				if(month < 10){
					month = "0" + month;
				}else{
					month = "" + month;
				}
				if(sellerFirstNameFD == first_nameFD && sellerLastNameFD == last_nameFD && codeday == day && codemonth == month && code.length == 12){

					return (amount * 1.0).round(2);
				}else{
					return 0;
				}
			}
		}            

		this.checkDiscountCode = function(){
			var code = that.discountCode();
			if(code == "Freelining" || code == "FreeShirt" || code == "FreeLinShi" ){
				return 1;
			}else if(code == null || code == undefined || code.length < 12){			
				return -1;
			}else{
				
				var amount = (code.substr(0,2) + code.substr(7,2) + "." + code.substr(10,2))*1;
				
				var codeday = code.substr(2,2);
				var codemonth = code.substr(5,2);
				var sellerFirstNameFD = code.substr(4,1).toLowerCase();;
				var sellerLastNameFD = code.substr(9,1).toLowerCase();;			
				var first_nameFD = authCtrl.userInfo.first_name.substr(0,1).toLowerCase();
				var last_nameFD = authCtrl.userInfo.last_name.substr(0,1).toLowerCase();
				var day = new Date().getDate();
				var month = new Date().getMonth() + 1;
				if(day < 10){
					day = "0" + day;
				}else{
					day = "" + day;
				}
				if(month < 10){
					month = "0" + month;
				}else{
					month = "" + month;
				}
				
console.log("amount: " + amount);
console.log("that.computePrice(): " + that.computePrice());				
				var difference = that.computePrice() - amount;
console.log("difference: " + difference);				
				
				if( difference < 0 ){
console.log("CASE NEGATIVE");					
					return 0;
				}else if(sellerFirstNameFD == first_nameFD && sellerLastNameFD == last_nameFD && codeday == day && codemonth == month && code.length == 12  ){					
					return 1;
				}else{
					return 0;
				}
			}
		}              


			this.calculateVat = function(){
				gstAmount = that.computePriceWithDiscount() * that.getGST();
				return gstAmount.round(2);
			}
			
			this.getGST = function(){
				
				currency = that.getSellerCurrency();				
				
				pricerange = JSON.parse(localStorage.getItem('pricerangeDS'));
				if(localStorage.getItem('pricerangeDS') != null){
				    if(localStorage.getItem('pricerangeDS').length > 0){
					    for(var i=0; i< pricerange.store.length; i++){
					    	if(pricerange.store[i].price_range_currency == currency.code){
					    		gst = (pricerange.store[i].price_range_GST * 1.0) / 100;
					    		return gst;
					    	}
					 	}
					}
				}
				return 0;
			}			
			
			this.calculateTotalPrice = function(){
				var total = that.calculateVat() + that.computePriceWithDiscount();
//console.log("calculateTotalPrice:" + total );				
				return total;
			}
			
			this.calculateTotalPriceWithDiscount = function(){
				var total = that.calculateVat() + that.computePriceWithDiscount();// - that.calculateDiscount();
//console.log("calculateTotalPriceWithDiscount:" + total ); 
				return total;
			}
			
					
//this.deposit(this.calculateTotalPriceWithDiscount()/2);	

			this.getCurrencySymbol = function(){
				currency = that.getSellerCurrency();
				return currency.symbol;
			}


			this.urgentCost = function(){
				
				currency = that.getSellerCurrency();
				var pricesarray = [];
				pricerange = JSON.parse(localStorage.getItem('pricerangeDS'));
				if(localStorage.getItem('pricerangeDS') != null){
				    if(localStorage.getItem('pricerangeDS').length > 0){
					    for(var i=0; i< pricerange.store.length; i++){
					    	if(pricerange.store[i].price_range_currency == currency.code){
					    		pricesarray.push(pricerange.store[i]);
					    	}
					 	}
					}
				}				
				var urgentcost = 0;
				for(x = 0; x < pricesarray.length; x++){
			       	if(	pricesarray[x].price_range_garment_name == 'Express Delivery' ){
			           	urgentcost = pricesarray[x].price_range_price * 1.0;
						break;
		        	}
		        }	
		        return urgentcost;
            }               





			this.detectVerticalSquash = function(img) {
				var iw = img.naturalWidth, ih = img.naturalHeight;
//				var canvas = document.createElement('canvas');
				var canvas = document.getElementById("canvas-squash");
				canvas.width = 1;
				canvas.height = ih;
				var ctx = canvas.getContext('2d');
				var t = ctx.drawImage(img, 0, 0);
				t = null;
				var data = ctx.getImageData(0, 0, 1, ih).data;
				// search image edge pixel position in case it is squashed vertically.
				var sy = 0;
				var ey = ih;
				var py = ih;
				while (py > sy) {
					var alpha = data[(py - 1) * 4 + 3];
					if(alpha === 0){
						ey = py;
					}else{
						sy = py;
					}
					py = (ey + sy) >> 1;
				}
				var ratio = (py / ih);
				canvas.width = canvas.width;
				return (ratio===0)?1:ratio;
			}						
					
			this.drawImageIOSFix = function (ctx, img, sx, sy, sw, sh) {  /*(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) */
				var vertSquashRatio = orderItem.detectVerticalSquash(img);
				// Works only if whole image is displayed:
				// ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
				// The following works correct also when only a part of the image is displayed:
				var t = ctx.drawImage(img, sx * vertSquashRatio, sy * vertSquashRatio, sw * vertSquashRatio, sh * vertSquashRatio);
				t = null;
			}	


			this.resizeMyImage = function (url, thumbwidth, postedwidth, page, what){
console.log("resizeMyImage function in app.js");
				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";
				var sourceImage = new Image();
				var thumbnail;
			    var topost;
			    
			    sourceImage.onload = function() {
			        // Create a canvas with the desired dimensions
			        var oldwidth = sourceImage.width;
			        var oldheight = sourceImage.height;
					//var canvas = document.createElement("canvas");
					var canvas = document.getElementById("canvas-0");
					if(oldwidth < oldheight){								
						var width = thumbwidth;
						var height = (oldheight/oldwidth)*width;
						canvas.width = width;
						canvas.height = height;
						orderItem.drawImageIOSFix(canvas.getContext("2d"), sourceImage, 0, 0, width, height);
						thumbnail = canvas.toDataURL("image/jpeg", 0.2);
						canvas.width = canvas.width; // resets canvas
						
						var width2 = postedwidth;
						var height2 = (oldheight/oldwidth)*width2;
						canvas.width = width2;
				        canvas.height = height2;
				        orderItem.drawImageIOSFix(canvas.getContext("2d"), sourceImage, 0, 0, width2, height2);
			    	    topost = canvas.toDataURL("image/jpeg", 0.6);
						canvas.width = canvas.width;

						orderItem.appendImage(topost, thumbnail, page, what);
					}else{
						//var tempcanvas = document.createElement("canvas");
						var tempcanvas = document.getElementById("canvas-1");
						var tempheight = thumbwidth;
						var tempwidth = (oldwidth/oldheight)*tempheight;
						tempcanvas.width = tempwidth;
						tempcanvas.height = tempheight;
						orderItem.drawImageIOSFix(tempcanvas.getContext("2d"), sourceImage, 0, 0, tempwidth, tempheight);
						var tmpimageurl = tempcanvas.toDataURL("image/jpeg", 0.2);
						tempcanvas.width = tempcanvas.width;
						//tempcanvas.remove();
						
						var tempimage = new Image();
						tempimage.src = tmpimageurl;
						tmpimageurl = null;
						delete tempimage;
								
						tempimage.onload = function(){
							var newwidth = tempimage.width;
							var newheight = tempimage.height;
							var cContext = canvas.getContext('2d');
							var cw = tempimage.width, ch = tempimage.height, cx = 0, cy = 0;
							cw = tempimage.height;
							ch = tempimage.width;
							cy = tempimage.height * (-1);
							canvas.setAttribute('width', cw);
							canvas.setAttribute('height', ch);
							cContext.rotate(90 * Math.PI / 180);
							var t = cContext.drawImage(tempimage, cx, cy);
							t = null;
							
							thumbnail = canvas.toDataURL("image/jpeg", 0.2);
							//var tempcanvas2 = document.createElement("canvas");
							var tempcanvas2 = document.getElementById("canvas-2");									
							var tempheight2 = postedwidth;
							var tempwidth2 = (oldwidth/oldheight)*tempheight2;
							tempcanvas2.width = tempwidth2;
							tempcanvas2.height = tempheight2;
							orderItem.drawImageIOSFix(tempcanvas2.getContext("2d"), sourceImage, 0, 0, tempwidth2, tempheight2);
							var tmpimageurl2 = tempcanvas2.toDataURL("image/jpeg", 0.6);
							tempcanvas2.width = tempcanvas2.width;
							//tempcanvas2.remove();
							
							var tempimage2 = new Image();
							tempimage2.src = tmpimageurl2;
							tmpimageurl2 = null;
							delete tempimage2;
							
							tempimage2.onload = function(){
								var newwidth2 = tempimage2.width;
								var newheight2 = tempimage2.height;
								var cContext2 = canvas.getContext('2d');
								var cw2 = tempimage2.width, ch2 = tempimage.height, cx2 = 0, cy2 = 0;
								cw2 = tempimage2.height;
								ch2 = tempimage2.width;
								cy2 = tempimage2.height * (-1);
								canvas.setAttribute('width', cw2);
								canvas.setAttribute('height', ch2);
								cContext2.rotate(90 * Math.PI / 180);
								var t = cContext2.drawImage(tempimage2, cx2, cy2);
								t = null;
								topost = canvas.toDataURL("image/jpeg", 0.6);
								canvas.width = canvas.width;
								//canvas.remove();
								orderItem.appendImage(topost, thumbnail, page, what);
							}
						}
					}
			    }
			    sourceImage.src = url;
			    
				topost = null;
				thumbnail = null;
				delete sourceImage;
			}
			
			this.appendImage = function (topost, thumbnail, page, what){
				if(page == "fittings.htm"){
					appendItemFittingsImage(topost, thumbnail);
				}else if(page == "completions.htm"){
					appendItemCompletionImage(topost, thumbnail, what);	
				}else if(page == "alterationReport.htm"){
					appendItemAlterationReportImage(topost, thumbnail, what);
				}else if(page == "fitting.htm"){	// THIS ONE MIGHT NOT BE USED
					appendItemFittingImage(topost, thumbnail);	
				}else if(page == "fittingImages.htm"){	// THIS ONE MIGHT NOT BE USED
					appendItemFittingImagesImage(topost, thumbnail);
				}else if(page == "garmentsJacket.htm"){
					if(what == "Structure"){
						appendItemJacketStructure(topost, thumbnail);
					}else if(what == "Bottom"){
						appendItemJacketBottom(topost, thumbnail);
					}else if(what == "CustomVent"){
						appendItemJacketCustomVent(topost, thumbnail);
					}else if(what == "Lapel"){
						appendItemJacketLapel(topost, thumbnail);
					}else if(what == "Embroidery"){
						appendItemJacketEmbroidery(topost, thumbnail);
					}else if(what == "Monogram"){
						appendItemJacketMonogram(topost, thumbnail);
					}
				}else if(page == "garmentsPant.htm"){	
					if(what == "Pockets"){
						appendItemPantPockets(topost, thumbnail);
					}	
				}else if(page == "garmentsSuit.htm"){
					if(what == "Structure"){
						appendItemSuitStructure(topost, thumbnail);
					}else if(what == "Bottom"){
						appendItemSuitBottom(topost, thumbnail);
					}else if(what == "CustomVent"){
						appendItemSuitCustomVent(topost, thumbnail);
					}else if(what == "Lapel"){
						appendItemSuitLapel(topost, thumbnail);
					}else if(what == "Embroidery"){
						appendItemSuitEmbroidery(topost, thumbnail);
					}else if(what == "Monogram"){
						appendItemSuitMonogram(topost, thumbnail);
					}else if(what == "Pockets"){
						appendItemSuitPockets(topost, thumbnail);
					}
				}else if(page == "garmentsVest.htm"){
					if(what == "Lapel"){
						appendItemVestLapel(topost, thumbnail);
					}else if(what == "Pockets"){
						appendItemVestButtons(topost, thumbnail);
					}	
				}else if(page == "makePayment.htm"){
					appendItemCreditCard(topost, thumbnail);	
				}else if(page == "measurements.htm"){
					appendItemMeasurements(topost, thumbnail);	
				}else if(page == "orderItemMedia.htm"){
					appendItemMedia(topost, thumbnail);	
				}else if(page == "orderItemReview.htm"){
					appendItemReviewCreditCard(topost, thumbnail);
				}else if(page == "shipping_fabrics.htm"){
					appendItemShippingFabric(topost, thumbnail);		
				}else if(page == "shipping_copy_garment.htm"){
					appendItemShippingCopyGarment(topost, thumbnail);		
				}else if(page == "shipping_faulty_remake_garments.htm"){
					appendItemShippingFaultyRemakeGarments(topost, thumbnail);		
				}else if(page == "shipping_other.htm"){
					appendItemShippingOther(topost, thumbnail);		
				}
			}

			this.openDialog = function(){
				document.getElementById("form_panel").style.display = "block";
/*			var panels = document.getElementsByName("form_panel");
			for(var x = 0; x < panels.length; x++){
console.log("panel " + x);
				
				panels[x].style.display = "block";
			}	
	*/		
        	};


/*
		ko.bindingHandlers.computeDeposit = {
		    init: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "change", function() {  		        	      	
		             var value = valueAccessor();
		            if (ko.isWriteableObservable(value)) { 
		            	tableindex = element.selectedIndex;// -1; // USE -1 IF optionsCaption: 'Fitline' IS USED -1;
		            	if(tableindex >= 0){
							document.getElementsByName('pantsName')[0].value = this.fitlinePantsVM.pantsName();

               				if("createEvent" in document) {
		    					var evt = document.createEvent("HTMLEvents");
		    					evt.initEvent("change", false, true);
		    					document.getElementsByName('pantsName')[0].dispatchEvent(evt);

							}else{
								document.getElementsByName('pantsName')[0].fireEvent("onchange");
		               		}
               		
               		
               		
		            	}	
		            }
		        });
		    }
		};
*/
            this.selectedGarment = new ko.observable();


            this.isEnabled = function(data) {
console.log('isEnabled');				
                for ( var ind in that.garmentsList() ) {
                    if ( that.garmentsList()[ind].id == data && that.garmentsList()[ind].count()>0 ) {
                        return that.garmentsList()[ind].active;
                    }
                }
                return false;
            };


            this.dumpOrder = function() {	
						
				if(	( typeof(that.garmentsJacketDS) == "undefined" && that.garmentsList()[0].count() > 0 ) || 
					( typeof(that.garmentsPantDS) == "undefined" && that.garmentsList()[1].count() > 0 ) ||
					( typeof(that.garmentsSuitDS) == "undefined" && that.garmentsList()[2].count() > 0 ) ||
					( typeof(that.garmentsVestDS) == "undefined" && that.garmentsList()[3].count() > 0 ) ||
					( typeof(that.garmentsShirtDS) == "undefined" && that.garmentsList()[4].count() > 0 )
					){	
					that.populateGarments();		// HOTFIX FOR FIXING THE BUG IF YOU JUMP FROM orderItemPopulateGarments DIRECTLY TO orderItemSelectGarment
				}
				
                var out = {}; 
                
                if (typeof(that.garmentsJacketDS) != "undefined") {
                	if(that.garmentsList()[0].count() > 0 ){                		
                    	out.Jacket = that.garmentsJacketDS.getStore();                    	
                    }	
                }
                if (typeof(that.garmentsPantDS) != "undefined") {
                	if(that.garmentsList()[1].count() > 0 ){
                    	out.Pant = that.garmentsPantDS.getStore();                    	
                    }	
                }                
                if (typeof(that.garmentsSuitDS) != "undefined") {
                	if(that.garmentsList()[2].count() > 0 ){
                    	out.Suit = that.garmentsSuitDS.getStore();                                     	
                    }	
                }
                if (typeof(that.garmentsVestDS) != "undefined") {
                	if(that.garmentsList()[3].count() > 0 ){
                    	out.Vest = that.garmentsVestDS.getStore();
                    }	
                }
                if (typeof(that.garmentsShirtDS) != "undefined") {
                	if(that.garmentsList()[4].count() > 0 ){
                    	out.Shirt = that.garmentsShirtDS.getStore();
                    }	
                }
                return out;
            };


            this.addVariantToGarment = function(data) {
console.log('addVariantToGarment');
                switch(data.id) {
                    case 0:
                        that.garmentsJacketVM.addVariantJacket();
                        break;
                    case 1:
                        that.garmentsPantVM.addVariantPant();
                        break;
                    case 2:
                        that.garmentsSuitVM.addVariantSuit();
                        break;
                    case 3:
                        that.garmentsVestVM.addVariantVest(); 
                        break;
                    case 4:
                        that.garmentsShirtVM.addVariantShirt(); 
                        break;
			
                }
            };

this.alreadypopulated = false;
//if(that.alreadypopulated == false){
	
            this.populateGarments = function() {
console.log('populateGarments');									
                for (var ind in that.garmentsList() ) {
                    if (that.garmentsList()[ind].count() != 0) {
                        that.enableGarment({
                            id: that.garmentsList()[ind].id
                        });
                        for (var cc = 0; cc < that.garmentsList()[ind].count() - 1; cc++ ) {
                            that.addVariantToGarment({ id: that.garmentsList()[ind].id });
                        }
                    }
                }
            };

//that.garmentsList()[ind].id

            this.enableGarment = function(data) {
console.log('enableGarment ' + JSON.stringify(data));
                switch(data.id) {
                    case 0:
                    	if(that.garmentsJacketDS == undefined){
                    		that.jacketfirsttime = true;
                    		that.garmentsJacketDS = new defJacket('garmentsJacketDS', DsRegistry, null, 0);		// resets values
                    		that.garmentsJacketVM = new GarmentsJacket(DsRegistry);
                        	that.garmentsJacketVM.subscribeTo('garmentsJacketDS');
                    	}else{
                    		that.jacketfirsttime = false;
							var currentcount = orderItem.garmentsList()[0].count();
							thegarmentsold = that.garmentsJacketDS.getStore().Jacket;													
							that.garmentsJacketDS = new defJacket('garmentsJacketDS', DsRegistry, thegarmentsold, currentcount);		// resets values
						//	thegarments = that.garmentsJacketDS.getStore().Jacket;
                        	that.garmentsJacketVM = new GarmentsJacket(DsRegistry);
                        	that.garmentsJacketVM.subscribeTo('garmentsJacketDS');      
                    	}                     
                    	break;
                    case 1:
                    	if(that.garmentsPantDS == undefined){
                    		that.pantfirsttime = true;
                    		that.garmentsPantDS = new defPant('garmentsPantDS', DsRegistry, null, 0);		// resets values
                    		that.garmentsPantVM = new GarmentsPant(DsRegistry);
                        	that.garmentsPantVM.subscribeTo('garmentsPantDS'); 
                    	}else{
                    		that.pantfirsttime = false;
							var currentcount = orderItem.garmentsList()[1].count();
							thegarmentsold = that.garmentsPantDS.getStore().Pant;													
							that.garmentsPantDS = new defPant('garmentsPantDS', DsRegistry, thegarmentsold, currentcount);		// resets values
						//	thegarments = that.garmentsPantDS.getStore().Pant;
                        	that.garmentsPantVM = new GarmentsPant(DsRegistry);
                        	that.garmentsPantVM.subscribeTo('garmentsPantDS');      
                    	}                     
                     	break;
                    case 2:
                    	if(that.garmentsSuitDS == undefined){
                    		that.suitfirsttime = true;
                    		that.garmentsSuitDS = new defSuit('garmentsSuitDS', DsRegistry, null, 0);		// resets values
                    		that.garmentsSuitVM = new GarmentsSuit(DsRegistry);
                        	that.garmentsSuitVM.subscribeTo('garmentsSuitDS');
                    	}else{
                    		that.suitfirsttime = false;
							var currentcount = orderItem.garmentsList()[2].count();
							thegarmentsold = that.garmentsSuitDS.getStore().suit;													
							that.garmentsSuitDS = new defSuit('garmentsSuitDS', DsRegistry, thegarmentsold, currentcount);		// resets values
						//	thegarments = that.garmentsSuitDS.getStore().suit;
                        	that.garmentsSuitVM = new GarmentsSuit(DsRegistry);
                        	that.garmentsSuitVM.subscribeTo('garmentsSuitDS');      
                    	}	
                    	
                    	/*
                    	// extra here for extra pant support
						if(that.garmentsPantDS == undefined){
                    		that.pantfirsttime = true;
                    		that.garmentsPantDS = new defPant('garmentsPantDS', DsRegistry, null, 0);		// resets values
                    		that.garmentsPantVM = new GarmentsPant(DsRegistry);
                        	that.garmentsPantVM.subscribeTo('garmentsPantDS'); 
                    	}else{
                    		that.pantfirsttime = false;
							var currentcount = orderItem.garmentsList()[1].count();
							thegarmentsold = that.garmentsPantDS.getStore().Pant;													
							that.garmentsPantDS = new defPant('garmentsPantDS', DsRegistry, thegarmentsold, currentcount);		// resets values
						//	thegarments = that.garmentsPantDS.getStore().Pant;
                        	that.garmentsPantVM = new GarmentsPant(DsRegistry);
                        	that.garmentsPantVM.subscribeTo('garmentsPantDS');      
                    	}                       	
                    	  */   
                    	break;                 
                    case 3:
                    	if(that.garmentsVestDS == undefined){
                    		that.vestfirsttime = true;
                    		that.garmentsVestDS = new defVest('garmentsVestDS', DsRegistry, null, 0);		// resets values
                    		that.garmentsVestVM = new GarmentsVest(DsRegistry);
                        	that.garmentsVestVM.subscribeTo('garmentsVestDS');
                    	}else{
                    		that.vestfirsttime = false;
							var currentcount = orderItem.garmentsList()[3].count();
							thegarmentsold = that.garmentsVestDS.getStore().vest;													
							that.garmentsVestDS = new defVest('garmentsVestDS', DsRegistry, thegarmentsold, currentcount);		// resets values
						//	thegarments = that.garmentsVestDS.getStore().vest;
                        	that.garmentsVestVM = new GarmentsVest(DsRegistry);
                        	that.garmentsVestVM.subscribeTo('garmentsVestDS');      
                    	}	                        
                        break;
                    case 4:
                    	if(that.garmentsShirtDS == undefined){
                    		that.shirtfirsttime = true;
                    		that.garmentsShirtDS = new defShirt('garmentsShirtDS', DsRegistry, null, 0);		// resets values
                    		that.garmentsShirtVM = new GarmentsShirt(DsRegistry);
                        	that.garmentsShirtVM.subscribeTo('garmentsShirtDS');
                    	}else{
                    		that.shirtfirsttime = false;
							var currentcount = orderItem.garmentsList()[4].count();
							thegarmentsold = that.garmentsShirtDS.getStore().Shirt;													
							that.garmentsShirtDS = new defShirt('garmentsShirtDS', DsRegistry, thegarmentsold, currentcount);		// resets values
							//thegarments = that.garmentsShirtDS.getStore().Shirt;
                        	that.garmentsShirtVM = new GarmentsShirt(DsRegistry);
                        	that.garmentsShirtVM.subscribeTo('garmentsShirtDS');      
                    	}		
                        break;
			
                }

                var tA = that.garmentsList();
                for ( var ind in that.garmentsList() ) {
                    if ( that.garmentsList()[ind].id == data.id  ) {
                        tA[ind].active = true;
                    }
                }
                that.garmentsList(tA);
            };

	this.getVariantShirt = function(id) {
		for (var ind in that.garmentsShirtVM.shirtData) {
			if ( that.garmentsShirtVM.shirtData[ind].variantId == id  ) {
				return that.garmentsShirtVM.shirtData[ind];
			}
		}
		return null;

	},


            this.disableGarment = function(data) {
console.log('disableGarment');     
              var tA = that.garmentsList();
                for ( var ind in that.garmentsList() ) {
                    if ( that.garmentsList()[ind].id == data.id  ) {
                        tA[ind].active = false;
                    }
                }
                that.garmentsList(tA);
            };
            
            //that.numberOfPant = ko.observable(3);
            //this.listchoice = function(){
	            // that.garmentsVM     = that.garmentsPantVM;
	            /*
			    this.garmentsList  = new ko.observableArray([
						{id: 0, title: "Jacket", image: "gfx/measurements/jacket.png", active: false , count: ko.observable(0) },
						{id: 1, title: "Pant"  , image: "gfx/measurements/pants.png", active: false, count: ko.observable(0)} , 
						{id: 2, title: "Suit"  , image: "gfx/measurements/suit.png", active: false, count: ko.observable(0)},
						{id: 3, title: "Vest"  , image: "gfx/measurements/vest.png", active: false, count: ko.observable(0)},
						{id: 4, title: "Shirt" , image: "gfx/measurements/shirt.png", active: false, count: ko.observable(0)}             
					]);
	                */
            //};
            // that.numberOfPant = ko.observable("");

			editjacket = function(data) {
console.log('editjacket');               
                that.garmentsVM = that.garmentsJacketVM;
                $.mobile.changePage( that.garmentsVM.currentStep().target );
            };

  			editPant = function(data) {
console.log('editPant');                  
      			that.garmentsVM = that.garmentsPantVM;                     
      			$.mobile.changePage( that.garmentsVM.currentStep().target );
            };

  			editSuit = function(data) {   
     			that.garmentsVM = that.garmentsSuitVM;
				$.mobile.changePage( that.garmentsVM.currentStep().target );
            };


 			editVest = function(data) {
    			that.garmentsVM = that.garmentsVestVM;
				$.mobile.changePage( that.garmentsVM.currentStep().target );
            };


            this.editGarment = function(data) {			
				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";
				var myVar=setTimeout(function(){                       
	                switch(data.id) {
	                    case 0:
	                        that.garmentsVM = that.garmentsJacketVM;                         
	                        $.mobile.changePage( that.garmentsVM.currentStep().target );          
	                        break;
	                    case 1:
	                        that.garmentsVM = that.garmentsPantVM;
	                        $.mobile.changePage( that.garmentsVM.currentStep().target );
	                        break;
	                    case 2:
	                        that.garmentsVM = that.garmentsSuitVM;
	                        $.mobile.changePage( that.garmentsVM.currentStep().target );
	                        break;
	                    case 3:
	                        that.garmentsVM = that.garmentsVestVM;
	                        $.mobile.changePage( that.garmentsVM.currentStep().target );
	                        break;
	                    case 4:
	                        that.garmentsVM = that.garmentsShirtVM;
	                        $.mobile.changePage( that.garmentsVM.currentStep().target );
	                        break;
	                }
                },200);
            };
            
            this.cancelMode = false;
            this.atmeasurements = ko.observable(false);
            this.currentBigStep = ko.observable(this.bigSteps()[0]);
            this.currentBigStep.subscribe(function(data) {
                if (that.cancelMode == true) {
                    that.cancelMode = false;
                    return;
                }

                switch(data.id) {
                    case 0:
                        if ($.isEmptyObject(that.custSelectVM.selectedCustomer())) 
                            $.mobile.changePage('#orderItemSelectCustomer');
                        else
                            $.mobile.changePage('#orderItemCustomerInfo');
                        break;
                    case 1:
                        	$.mobile.changePage('#orderItemPopulateGarments');
                        break;
                    case 2:		
							that.measurementsVM.selectedGarment({name: "none"});// = ko.observable({name: "none"});// FIX FOR FORCING THE POS TO FORGET THE SELECTED GARMENT IN CASE YOU GO BACK TO SELECT GARMENTS, AND RETURN TO MEASUREMENTS
							that.atmeasurements(true);
                        	$.mobile.changePage('#measurements');
                        	setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },1500);
                        break;
                    case 3:
                        	$.mobile.changePage('#orderItemMedia');
                        break;
                    case 4:
                        	$.mobile.changePage('#bodyshape');	
                        break;
                    case 5:
                        	$.mobile.changePage('#orderItemSelectGarment');
                        break;
                    case 6:
                        	$.mobile.changePage('#orderItemReview');	
                        break;
                }
            });
            
            this.currentBigStep2 = ko.observable(this.bigSteps()[0]);
            this.currentBigStep2.subscribe(function(data) {
            	if( that.atmeasurements() == true){
	            	that.currentBigStep(data);
               }
            });            

			this.updateMeasurements = function(data) {
					
				var item = {
					"jacketChest": that.measurementsVM.jacketChest(),
					"jacketStomach": that.measurementsVM.jacketStomach(),
					"jacketHips": that.measurementsVM.jacketHips(),
					"jacketfullShoulder": that.measurementsVM.jacketfullShoulder(),
					"jacketsleeveL": that.measurementsVM.jacketsleeveL(),
					"jacketsleeveR": that.measurementsVM.jacketsleeveR(),
					"jacketFront": that.measurementsVM.jacketFront(),
					"jacketBack": that.measurementsVM.jacketBack(),
					"jacketBicep": that.measurementsVM.jacketBicep(),
					"jacketforeArms": that.measurementsVM.jacketforeArms(),
					"jacketWrist": that.measurementsVM.jacketWrist(),
					"jacketLength": that.measurementsVM.jacketLength(),
					"jacketLapel": that.measurementsVM.jacketLapel(),
					"jacketNeck": that.measurementsVM.jacketNeck(),
					"jacketZeropoint": that.measurementsVM.jacketZeropoint(),
					"jacketZeropointfront": that.measurementsVM.jacketZeropointfront(),
					"jacketZeropointback": that.measurementsVM.jacketZeropointback(),
					"jacketComments": that.measurementsVM.jacketComments(),
					"pantWaist": that.measurementsVM.pantWaist(),
					"pantHips": that.measurementsVM.pantHips(),
					"thigh": that.measurementsVM.thigh(),
					"cuffs": that.measurementsVM.cuffs(),
					"pantFrontCrotch": that.measurementsVM.pantFrontCrotch(),
					"pantInseam": that.measurementsVM.pantInseam(),
					"pantLength": that.measurementsVM.pantLength(),
					"crotch": that.measurementsVM.crotch(),
					"zipper": that.measurementsVM.zipper(),
					"pantAngledWaist": that.measurementsVM.pantAngledWaist(),
					"pantWaistAngle": that.measurementsVM.pantWaistAngle(),
					"pantsComments": that.measurementsVM.pantsComments(),
					"shirtChest": that.measurementsVM.shirtChest(),
					"shirtStomach": that.measurementsVM.shirtStomach(),
					"shirtHips": that.measurementsVM.shirtHips(),
					"shirtfullshoulder": that.measurementsVM.shirtfullshoulder(),
					"shirtsleeveL": that.measurementsVM.shirtsleeveL(),
					"shirtsleeveR": that.measurementsVM.shirtsleeveR(),
					"shirtNeck": that.measurementsVM.shirtNeck(),
					"shirtFront": that.measurementsVM.shirtFront(),
					"shirtBack": that.measurementsVM.shirtBack(),
					"shirtLength": that.measurementsVM.shirtLength(),
					"shirtBicep": that.measurementsVM.shirtBicep(),
					"shirtWrist": that.measurementsVM.shirtWrist(),
					"shirtWristwatch": that.measurementsVM.shirtWristwatch(),
					"shirtComments": that.measurementsVM.shirtComments(),
					"vestChest": that.measurementsVM.vestChest(),
					"vestStomach": that.measurementsVM.vestStomach(),
					"vestNeck": that.measurementsVM.vestNeck(),
					"vestHips": that.measurementsVM.vestHips(),
					"vestLength": that.measurementsVM.vestLength(),
					"vestVLength": that.measurementsVM.vestVLength(),
					"vestfullShoulder": that.measurementsVM.vestfullShoulder(),
					"vestHalfShoulderWidth": that.measurementsVM.vestHalfShoulderWidth(),
					"vestComments": that.measurementsVM.vestComments(),
					"jacketDate": Math.round(new Date().getTime() / 1000),
					"pantsDate": Math.round(new Date().getTime() / 1000),
					"shirtDate": Math.round(new Date().getTime() / 1000),
					"vestDate": Math.round(new Date().getTime() / 1000)
				};
				
	           	var tVar = this.dsRegistry.getDatasource('measurementsDS').getStore();
				tVar[0] = item;
				this.dsRegistry.getDatasource('measurementsDS').setStore(tVar);
				
				
				measurementsDS2 = new SyncableDatasource('measurementsDS','measurementsDS', dsRegistry, 'update_customer_measurements_endpoint'); 
				measurementsDS2.sync();
				
				if (customersVM.systemMode != "order"){ 
					$.mobile.changePage('#customerInfo');
					$.jGrowl("Succesfully updated customer's  measurements", {
						header: 'Update'
					});
				}
				
			};	            
         
         
         	this.measurementserror = function(data){
         		var errorstring = '';		
				if(that.future() == false){	
	                if (that.dumpOrder().Jacket  || that.dumpOrder().Suit) {    	
						if( that.measurementsVM.jacketChest() == "" || that.measurementsVM.jacketChest() <= 0){
							errorstring += 'Jacket Chest, ';
						}	
						if( that.measurementsVM.jacketStomach() == "" || that.measurementsVM.jacketStomach() <= 0){
							errorstring += 'Jacket Stomach, ';
						}
						if( that.measurementsVM.jacketHips() == "" || that.measurementsVM.jacketHips() <= 0){						
							errorstring += 'Jacket Hips, ';
						}						
						if( that.measurementsVM.jacketFront() == "" || that.measurementsVM.jacketFront() <= 0){
							errorstring += 'Jacket Front, ';
						}						
						if( that.measurementsVM.jacketBack() == "" || that.measurementsVM.jacketBack() <= 0){
							errorstring += 'Jacket Chest, ';
						}
						if( that.measurementsVM.jacketfullShoulder() == "" || that.measurementsVM.jacketfullShoulder() <= 0){
							errorstring += 'Jacket Full Shoulder, ';
						}
						if( that.measurementsVM.jacketsleeveL() == "" || that.measurementsVM.jacketsleeveL() <= 0){
							errorstring += 'Jacket Left Sleeve, ';
						}
						if( that.measurementsVM.jacketsleeveR() == "" || that.measurementsVM.jacketsleeveR() <= 0){
							errorstring += 'Jacket Right Sleeve, ';
						}
						if( that.measurementsVM.jacketNeck() == "" || that.measurementsVM.jacketNeck() <= 0){
							errorstring += 'Jacket Neck, ';
						}
						if( that.measurementsVM.jacketBicep() == "" || that.measurementsVM.jacketBicep() <= 0){
							errorstring += 'Jacket Bicep, ';
						}
						if( that.measurementsVM.jacketWrist() == "" || that.measurementsVM.jacketBicep() <= 0){
							errorstring += 'Jacket Wrist, ';
						}
				/*		if( that.measurementsVM.jacketWristWatch() == ""){													// REMOVED
							errorstring += 'Jacket Wrist with Watch, ';
						}*/
				//		if( that.measurementsVM.jacketforeArms() == "" || that.measurementsVM.jacketBicep() <= 0){		// NOT REQUIRED	
				//			errorstring += 'Jacket Forearms, ';
				//		}
						if( that.measurementsVM.jacketLength() == "" || that.measurementsVM.jacketBicep() <= 0){
							errorstring += 'Jacket Length, ';
						}
				//		if( that.measurementsVM.jacketLapel() == "" || that.measurementsVM.jacketBicep() <= 0){		// NOT REQUIRED
				//			errorstring += 'Jacket Lapel Length, ';
				//		}
						if( that.measurementsVM.jacketZeropoint() == "" || that.measurementsVM.jacketBicep() <= 0){
							errorstring += 'Jacket Zero Point, ';
						}
						if( that.measurementsVM.jacketZeropointfront() == "" || that.measurementsVM.jacketZeropointfront() <= 0){
							errorstring += 'Jacket Front S. to Zero, ';
						}
						if( that.measurementsVM.jacketZeropointback() == "" || that.measurementsVM.jacketZeropointback() <= 0){
							errorstring += 'Jacket Back S. to Zero, ';
						}
				/*		if( that.measurementsVM.jacketHalfzero() == ""){
							errorstring += 'Jacket Half Zero, ';
						}
						if( that.measurementsVM.jacketHalfstomach() == ""){
							errorstring += 'Jacket Half Stomach, ';
						}*/
	                }
	                if (that.dumpOrder().Pant || that.dumpOrder().Suit) {
						if( that.measurementsVM.pantWaist() == "" || that.measurementsVM.pantWaist() <= 0){
							errorstring += 'Pants Waist, ';
						}
						if( that.measurementsVM.pantHips() == "" || that.measurementsVM.pantHips() <= 0){
							errorstring += 'Pants Hips, ';
						}
						if( that.measurementsVM.thigh() == "" || that.measurementsVM.thigh() <= 0){
							errorstring += 'Pants Thigh, ';
						}
						if( that.measurementsVM.cuffs() == "" || that.measurementsVM.cuffs() <= 0){
							errorstring += 'Pants Cuffs, ';
						}
						if( that.measurementsVM.pantLength() == "" || that.measurementsVM.pantLength() <= 0){
							errorstring += 'Pants Length, ';
						}
						if( that.measurementsVM.crotch() == "" || that.measurementsVM.crotch() <= 0){
							errorstring += 'Pants Cuffs, ';
						}
				//		if( that.measurementsVM.zipper() == "" || that.measurementsVM.zipper() <= 0){						// NOT REQUIRED
				//			errorstring += 'Pants Zipper Length, ';
				//			error = true;
				//		}	
				//		if( that.measurementsVM.pantFrontCrotch() == "" || that.measurementsVM.pantFrontCrotch() <= 0){	// NOT REQUIRED
				//			errorstring += 'Pants Front Crotch, ';
				//			error = true;
				//		}
				/*		if( that.measurementsVM.pantInseam() == "" || that.measurementsVM.pantInseam() <= 0){				// NOT REQUIRED
							errorstring += 'Pants inseam, ';
							error = true;
						}*/						
	                }
	                if (that.dumpOrder().Shirt) {
						if( that.measurementsVM.shirtChest() == "" || that.measurementsVM.shirtChest() <= 0){
							errorstring += 'Shirt Chest, ';
						}
						if( that.measurementsVM.shirtStomach() == "" || that.measurementsVM.shirtStomach() <= 0){
							errorstring += 'Shirt Stomach, ';
						}
						if( that.measurementsVM.shirtfullshoulder() == "" || that.measurementsVM.shirtfullshoulder() <= 0){
							errorstring += 'Shirt Full Shoulder, ';
						}
						if( that.measurementsVM.shirtsleeveL() == "" || that.measurementsVM.shirtsleeveL() <= 0){
							errorstring += 'Shirt Sleeve Left, ';
						}
						if( that.measurementsVM.shirtsleeveR() == "" || that.measurementsVM.shirtsleeveR() <= 0){
							errorstring += 'Shirt Sleeve Right, ';
						}
						if( that.measurementsVM.shirtNeck() == "" || that.measurementsVM.shirtNeck() <= 0){
							errorstring += 'Shirt Neck, ';
						}
						if( that.measurementsVM.shirtFront() == "" || that.measurementsVM.shirtFront() <= 0){
							errorstring += 'Shirt Front, ';
						}
						if( that.measurementsVM.shirtBack() == "" || that.measurementsVM.shirtBack() <= 0){
							errorstring += 'Shirt Back, ';
						}
						if( that.measurementsVM.shirtLength() == "" || that.measurementsVM.shirtLength() <= 0){
							errorstring += 'Shirt Shirt Length, ';
						}
						if( that.measurementsVM.shirtBicep() == "" || that.measurementsVM.shirtBicep() <= 0){
							errorstring += 'Shirt Bicep, ';
						}
						if( that.measurementsVM.shirtHips() == "" || that.measurementsVM.shirtHips() <= 0){
							errorstring += 'Shirt Hips, ';
						}
						if( that.measurementsVM.shirtWrist() == "" || that.measurementsVM.shirtWrist() <= 0){
							errorstring += 'Shirt Wrist, ';
						}
				//		if( that.measurementsVM.shirtWristwatch() == "" || that.measurementsVM.shirtWristwatch() <= 0){	// NOT REQUIRED
				//			errorstring += 'Shirt Wrist with watch, ';
				//		}
	                }  
	                if (that.dumpOrder().Vest) {
						if( that.measurementsVM.vestHips() == "" || that.measurementsVM.vestHips() <= 0){
							errorstring += 'Vest Hips at V length, ';
						}
						if( that.measurementsVM.vestLength() == "" || that.measurementsVM.vestLength() <= 0){
							errorstring += 'Vest Length, ';
						}
						if( that.measurementsVM.vestChest() == "" || that.measurementsVM.vestChest() <= 0){
							errorstring += 'Vest Chest, ';
						}
						if( that.measurementsVM.vestStomach() == "" || that.measurementsVM.vestStomach() <= 0){
							errorstring += 'Vest Stomach, ';
						}
						if( that.measurementsVM.vestNeck() == "" || that.measurementsVM.vestNeck() <= 0){
							errorstring += 'Vest Neck, ';
						}
						if( that.measurementsVM.vestHalfShoulderWidth() == "" || that.measurementsVM.vestHalfShoulderWidth() <= 0){
							errorstring += 'Vest Half Shoulder Length, ';
						}
						if( that.measurementsVM.vestfullShoulder() == "" || that.measurementsVM.vestfullShoulder() <= 0){
							errorstring += 'Vest Full Shoulder, ';
						}
					/*	if( that.measurementsVM.vestVLength() == "" || that.measurementsVM.vestVLength() <= 0){	// NOT REQUIRED
							errorstring += 'Vest V Length, ';
						}*/				
	                }
	            }    
	             if(errorstring.length > 0){
					errorstring = errorstring.trim();	
					errorstring = errorstring.substring(0, errorstring.length-1);
				}
				
	            return errorstring;
         	}
            
            
            
			this.errorfunction = function(data) {
				var errorstring = '';
				
				errorstring += that.measurementserror();
				
                if(errorstring.length > 0){
                	error = true;
					errorstring = "MEASUREMENTS MISSING\n" + errorstring + "\n";
				}
				
				if(that.future() == false){	
					if(that.checkGarmentsForFabrics() == false){
						errorstring += '\nFABRICS ERROR:\nThe garments do not have a fabric selected.\n';
						error = true; 
					}else{
					/*	if(that.deposit().trim() == ''){
							errorstring += '\nDEPOSIT ERROR:\nYou have not entered a deposit.\n';
							error = true;
						}else if(that.deposit() < that.calculateTotalPriceWithDiscount()/2){
							errorstring += '\nDEPOSIT ERROR:\nThe deposit must be at least half of the total cost of the order.\n';
							error = true;
						}else if(that.deposit() > that.calculateTotalPriceWithDiscount()/1){
							errorstring += '\nDEPOSIT ERROR:\nThe deposit can not be bigger than the total cost of the order.\n';
							error = true;
						}*/	
					}
					if(that.paymentMethod() == '' || that.paymentMethod() == undefined){
						errorstring += '\nPAYMENT METHOD ERROR:\nYou must select a payment method.\n';
						error = true;
					}
					if( (that.paymentMethod() == 'Credit card' || that.paymentMethod() == 'Eftpos') && that.creditCardImage() == ''){
						errorstring += '\nPAYMENT METHOD ERROR:\nYou must upload a photo of the receipt for credit card or Eftpos payments.\n';
						error = true;
					}

				}	
				/*
				if(that.referer() == ''){
					errorstring += '\nREFERRER ERROR:\nYou must enter a referrer.\n';
					error = true;
				}*/
				
				if(that.order_city() == null || that.order_city() == undefined){
					errorstring += '\nORDER CITY ERROR:\nYou must select an order city.\n';
					error = true;
				}
				
				if(that.acceptTerms() == false){
					errorstring += '\nTERMS & CONDITIONS ERROR:\nYou must accept the terms and conditions.\n';
					error = true;
				}

				return errorstring;		
            };
          
		this.initializeDesignHtml = function(){
			var mainlistdisplays = document.getElementsByClassName("MainListElementDisplay");
			var mainlistelements = document.getElementsByClassName("MainListElement");
			var garmentelements = document.getElementsByClassName("triggerr");
			var checkboxes = document.getElementsByClassName("listsimpleclick");
			var garmentsnumber = garmentelements.length/mainlistelements.length;

			var zeroelementchecked = [];
			var zeroelement = true;			

			var garmentpants = false;
			if(document.getElementsByName("PantFabricText").length > 0){
				garmentpants = true;
			}


			for(var a = 0; a < mainlistelements.length; a++){
				if(mainlistdisplays[a].style.display == 'none'){
					zeroelement = true;
				}else{
					zeroelement = false;
				}
				var haschecked = false;
				for(var b = 0; b < garmentsnumber; b++){
					var index = a*garmentsnumber + b;		
					if(checkboxes[index].classList.contains('selecteds')){	
						if(garmentpants == true){
							if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[b].parentSuitTimestampID == ''){
								mainlistelements[a].classList.add('activate');
							}
						}else{
							mainlistelements[a].classList.add('activate');	
						}
						garmentelements[index].classList.remove('offtrigger');
						garmentelements[index].classList.add('showtrigger');
						
						if(zeroelement == true){
							zeroelementchecked[b] = true;
						}else{
							zeroelementchecked[b] = false;
						}
					}else{
						if(zeroelement == true){
							zeroelementchecked[b] = false;
						}
						if(zeroelementchecked[b] == false){
							garmentelements[index].classList.add('offtrigger');
						}	
					}
					
				}
			}
			/*
			try{
				var JacketSleeveButtonNumberDS = this.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore();
				for(var b = 0; b < garmentsnumber; b++){			
					for(var c = 0; c < JacketSleeveButtonNumberDS.length; c++){    
	//console.log("document.getElementsByName('JacketSleeveButtonNumber')["+b+"].getElementsByTagName('option')["+c+"].selected: " + document.getElementsByName("JacketSleeveButtonNumber")[b].getElementsByTagName('option')[c].selected);						
						if( document.getElementsByName("JacketSleeveButtonNumberSpan")[b].innerHTML == JacketSleeveButtonNumberDS[c].title ){
							document.getElementsByName("JacketSleeveButtonNumber")[b].getElementsByTagName('option')[c].selected = true;
							if ("createEvent" in document) {
				    			var evt = document.createEvent("HTMLEvents");
				    			evt.initEvent("change", true, true);
				    			document.getElementsByName("JacketSleeveButtonNumber")[b].dispatchEvent(evt);
				    		}else{
				    			document.getElementsByName("JacketSleeveButtonNumber")[b].fireEvent("onchange");		
				    		}							
				    		break;	
						}         		
	            	}				
				}
			}catch(e){
				;
			}	*/
			/*
			if(document.getElementsByName("JacketSleeveButtonNumberSpan").length > 0){
				var JacketSleeveButtonNumberDS = this.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore();
			//	try{
					for(var b = 0; b < garmentsnumber; b++){			
						for(var c = 0; c < JacketSleeveButtonNumberDS.length; c++){    
		//console.log("document.getElementsByName('JacketSleeveButtonNumber')["+b+"].getElementsByTagName('option')["+c+"].selected: " + document.getElementsByName("JacketSleeveButtonNumber")[b].getElementsByTagName('option')[c].selected);						
							if( document.getElementsByName("JacketSleeveButtonNumberSpan")[b].innerHTML == JacketSleeveButtonNumberDS[c].title ){
								document.getElementsByName("JacketSleeveButtonNumber")[b].getElementsByTagName('option')[c].selected = true;
								if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
					    			evt.initEvent("change", true, true);
					    			document.getElementsByName("JacketSleeveButtonNumber")[b].dispatchEvent(evt);
					    		}else{
					    			document.getElementsByName("JacketSleeveButtonNumber")[b].fireEvent("onchange");		
					    		}							
					    		break;	
							}         		
		            	}				
					}
			//	}catch(e){
			//		;
			//	}	
			}
			
			if(document.getElementsByName("suitJacketSleeveButtonNumberSpan").length > 0){
				var JacketSleeveButtonNumberDS = this.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore();
			//	try{
					for(var b = 0; b < garmentsnumber; b++){			
						for(var c = 0; c < JacketSleeveButtonNumberDS.length; c++){    
		//console.log("document.getElementsByName('JacketSleeveButtonNumber')["+b+"].getElementsByTagName('option')["+c+"].selected: " + document.getElementsByName("JacketSleeveButtonNumber")[b].getElementsByTagName('option')[c].selected);						
							if( document.getElementsByName("suitJacketSleeveButtonNumberSpan")[b].innerHTML == JacketSleeveButtonNumberDS[c].title ){
								document.getElementsByName("suitJacketSleeveButtonNumber")[b].getElementsByTagName('option')[c].selected = true;
								if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
					    			evt.initEvent("change", true, true);
					    			document.getElementsByName("suitJacketSleeveButtonNumber")[b].dispatchEvent(evt);
					    		}else{
					    			document.getElementsByName("suitJacketSleeveButtonNumber")[b].fireEvent("onchange");		
					    		}							
					    		break;	
							}         		
		            	}				
					}
			//	}catch(e){
			//		;
			//	}	
			}
				*/		
				
			
			document.getElementById("selectedfabric").innerHTML = "none";
			document.getElementById("fabricrange").innerHTML = "0";
			document.getElementById("composition").innerHTML = "0";
			document.getElementById("fabricinput").value = "";
			document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
			
			return false;		
		};      
		
		
		
		
		this.initializeHtmlFromExtraPants = function(){
            
            this.lockselects = true;
            
			var pantsnumber = document.getElementsByName("pantFitPreview").length;

			var pantPleats = this.dsRegistry.getDatasource('pantPleatsDS').getStore();
			var pantFit = this.dsRegistry.getDatasource('pantFitDS').getStore();
			var PantFit = ["Fitted", "Semi Fitted", "Standard Fit"];
			var pantPockets = this.dsRegistry.getDatasource('pantPocketsDS').getStore();
			var bpantPockets = this.dsRegistry.getDatasource('bpantPocketsDS').getStore();
			var pbackpocketMethod = ["Both","Right","Left"];
			var beltLoopStyle = this.dsRegistry.getDatasource('beltLoopStyleDS').getStore();
			
console.log("pantsnumber: " + pantsnumber);		
	
			for(var a = 0; a < pantsnumber; a++){
console.log("CHECKING PANT " + a);

				try{
	            	for(var b = 0; b < PantFit.length; b++){
	            		if( document.getElementsByName("PantFitSpan")[a].innerHTML == PantFit[b] ){
							  document.getElementsByName("PantFit")[a].getElementsByTagName('option')[b].selected = true;          			
	            		}
	            	}
	            }catch(e){ ; }	
				 
				try{
	            	for(var b = 0; b < pantPleats.length; b++){            		
	            		if( document.getElementsByName("pantPleatsSpan")[a].innerHTML == pantPleats[b].title ){
							  document.getElementsByName("pantPleats")[a].getElementsByTagName('option')[b].selected = true;       
	            		}
	            	}
            	}catch(e){ ; }
            /*	
for(var b = 0; b < pantPleats.length; b++){            	
 	console.log("pantPleats option " + b + ": " + document.getElementsByName("pantPleats")[a].getElementsByTagName('option')[b].selected);         	
}            */	
	            	
	            try{	
	            	for(var b = 0; b < pantFit.length; b++){
	            		if( document.getElementsByName("pantFitSpan")[a].innerHTML == pantFit[b].title ){
							  document.getElementsByName("pantFit")[a].getElementsByTagName('option')[b].selected = true;          			
	            		}
	            	}
	            }catch(e){ ; }	
	            	
	            try{	
	            	for(var b = 0; b < pantPockets.length; b++){
	            		if( document.getElementsByName("pantPocketsSpan")[a].innerHTML == pantPockets[b].title ){
							  document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[b].selected = true;
							/*  try{
							  	document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[b].selected = true;
							  }catch(e){
							  	;
							  }	*/ 
	            		}
	            	}
	            }catch(e){ ; }	
	            	
	            try{	
	            	for(var b = 0; b < bpantPockets.length; b++){
	            		if( document.getElementsByName("bpantPocketsSpan")[a].innerHTML == bpantPockets[b].title ){
							  document.getElementsByName("bpantPockets")[a].getElementsByTagName('option')[b].selected = true;          			
	            		}
	            	}
	            }catch(e){ ; }	
	            	
	            try{	
	            	for(var b = 0; b < pbackpocketMethod.length; b++){
	            		if( document.getElementsByName("pbackpocketMethodSpan")[a].innerHTML == pbackpocketMethod[b] ){
							  document.getElementsByName("pbackpocketMethod")[a].getElementsByTagName('option')[b].selected = true;          			
	            		}
	            	}
	            }catch(e){ ; }	
	            	
	            try{	
	            	for(var b = 0; b < beltLoopStyle.length; b++){
	            		if( document.getElementsByName("beltLoopStyleSpan")[a].innerHTML == beltLoopStyle[b].title ){
							  document.getElementsByName("beltLoopStyle")[a].getElementsByTagName('option')[b].selected = true;
	            		}
	            	}            	
        		}catch(e){ ; }
        		
            						
				if ("createEvent" in document) {
		    		var evt = document.createEvent("HTMLEvents");
		    		evt.initEvent("change", true, true);
		    		document.getElementsByName('pantPleats')[a].dispatchEvent(evt);
		    		document.getElementsByName('PantFit')[a].dispatchEvent(evt);
		    		document.getElementsByName('pantFit')[a].dispatchEvent(evt);
		    		document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
		    	//	document.getElementsByName('pantPockets2')[a].dispatchEvent(evt);
		    		document.getElementsByName('bpantPockets')[a].dispatchEvent(evt);
		    		document.getElementsByName('beltLoopStyle')[a].dispatchEvent(evt);
		    	}else{
		    		document.getElementsByName('pantPleats')[a].fireEvent("onchange");
		    		document.getElementsByName('PantFit')[a].fireEvent("onchange");
		    		document.getElementsByName('pantFit')[a].fireEvent("onchange");
		    		document.getElementsByName('pantPockets')[a].fireEvent("onchange");
		    //		document.getElementsByName('pantPockets2')[a].fireEvent("onchange");
		    		document.getElementsByName('bpantPockets')[a].fireEvent("onchange");
		    		document.getElementsByName('beltLoopStyle')[a].fireEvent("onchange");
		    	}
	
            }       
            setTimeout(function() { console.log("this.lockselects: " + orderItem.lockselects); console.log("RELEASING LOCKSELECTS"); orderItem.lockselects = false; console.log("this.lockselects: " + orderItem.lockselects);}, 2000);

            return false;                                                                      
		};	
		
		
		
		this.modifyHtmlFromExtraPants = function(){
			
			var pantsnumber = document.getElementsByName("pantFitPreview").length;
//console.log("pantsnumber: " + pantsnumber);
//customAlert( JSON.stringify(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[0].PantFit) );
console.log("modifyHtmlFromExtraPants lockselects " + this.lockselects);
			if(this.lockselects == false){
		
				for(var a = 0; a < pantsnumber; a++){
					
					document.getElementsByName("pantFitPreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.preview;
					document.getElementsByName("pantPleatsPreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.preview;
					document.getElementsByName("pantPocketsPreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.preview;				
					document.getElementsByName("beltLoopStylePreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.preview;
					document.getElementsByName("bpantPocketsPreviewLeft")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.previewleft;
					document.getElementsByName("bpantPocketsPreviewRight")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.previewright;
					
					if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs == true  && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id === 'Normal_Straight' ){
						document.getElementsByName("pantCuffsPreviewNormal")[a].style.display = "";
						document.getElementsByName("pantCuffsPreviewBoot")[a].style.display = "none";
						document.getElementsByName("pantCuffsPreviewNarrow")[a].style.display = "none";
					}else if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs == true  && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id === 'Boot_Flare' ){
						document.getElementsByName("pantCuffsPreviewNormal")[a].style.display = "none";
						document.getElementsByName("pantCuffsPreviewBoot")[a].style.display = ""; 
						document.getElementsByName("pantCuffsPreviewNarrow")[a].style.display = "none";
					}else if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs == true  && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id === 'Narrow_Slim' ){
						document.getElementsByName("pantCuffsPreviewNormal")[a].style.display = "none";
						document.getElementsByName("pantCuffsPreviewBoot")[a].style.display = "none";
						document.getElementsByName("pantCuffsPreviewNarrow")[a].style.display = "";
					}
					
					if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod == 'Both'  || this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod === 'Left' ){
						document.getElementsByName("bpantPocketsPreviewLeft")[a].style.display = "";
					}else{
						document.getElementsByName("bpantPocketsPreviewLeft")[a].style.display = "none";
					}
					
					if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod == 'Both'  || this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod === 'Right' ){
						document.getElementsByName("bpantPocketsPreviewRight")[a].style.display = "";
					}else{
						document.getElementsByName("bpantPocketsPreviewRight")[a].style.display = "none";
					}
					
					document.getElementsByName("pantFitFrame")[a].classList.remove('completedgarments');
					document.getElementsByName("pantPleatsFrame")[a].classList.remove('completedgarments');
					document.getElementsByName("pantPocketsFrame")[a].classList.remove('completedgarments');
					document.getElementsByName("bpantPocketsFrame")[a].classList.remove('completedgarments');
					document.getElementsByName("beltLoopStyleFrame")[a].classList.remove('completedgarments');
					
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id != '0'){
						document.getElementsByName("pantFitFrame")[a].classList.add('completedgarments');
					}
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.id != '0'){
						document.getElementsByName("pantPleatsFrame")[a].classList.add('completedgarments');
					}
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.id != '0'){
						document.getElementsByName("pantPocketsFrame")[a].classList.add('completedgarments');
					}
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.id != '0'){
						document.getElementsByName("bpantPocketsFrame")[a].classList.add('completedgarments');
					}
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.id != '0'){
						document.getElementsByName("beltLoopStyleFrame")[a].classList.add('completedgarments');
					}
					
					
					document.getElementsByName("PantFitSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantFit;
					document.getElementsByName("pantFitSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.title;
					document.getElementsByName("pantPleatsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title;
					document.getElementsByName("pantPocketsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title;
					document.getElementsByName("bpantPocketsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.title;
					document.getElementsByName("beltLoopStyleSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.title;
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.title != 'No Belt Loop'){
						document.getElementsByName("beltLoopStyleSpanVisible")[a].style.display = "";
					}else{	
						document.getElementsByName("beltLoopStyleSpanVisible")[a].style.display = "none";
					}	
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs != true){
						document.getElementsByName("pantCuffsSpanVisible")[a].style.display = "";
					}else{	
						document.getElementsByName("pantCuffsSpanVisible")[a].style.display = "none";
					}
					if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantsBracesProvisions != true){
						document.getElementsByName("PantsBracesProvisionsSpanVisible")[a].style.display = "";
					}else{	
						document.getElementsByName("PantsBracesProvisionsSpanVisible")[a].style.display = "none";
					}
					
					try{	
						if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title != 'Select Pleats' && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title != 'No Pleats' 
							&& this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title !== 'Single Pleat'){
						   	
						//	document.getElementsByName("pantPocketsNoPleatsOrSinglePleat")[a].style.display = "none";
						//	document.getElementsByName("pantPocketsDoubleOrTriplePleats")[a].style.display = "";
							if( this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title != 'Select Pocket' && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title != 'No Pocket' 
								&& this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title != 'Slanted'){
																																	   	
								this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets = this.dsRegistry.getDatasource('pantPocketsDS').getStore()[2];
								document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[2].selected = true;
						//		document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[2].selected = true;
								if ("createEvent" in document) {
				    				var evt = document.createEvent("HTMLEvents");
				    				evt.initEvent("change", true, true);
				    				document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
				    	//			document.getElementsByName('pantPockets2')[a].dispatchEvent(evt);
				    			}else{
				    				document.getElementsByName('pantPockets')[a].fireEvent("onchange"); 
						//			document.getElementsByName('pantPockets2')[a].fireEvent("onchange");				
				    			}	
								document.getElementsByName("pantPocketsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title;
							}
						}/*else{
							document.getElementsByName("pantPocketsNoPleatsOrSinglePleat")[a].style.display = "";
							document.getElementsByName("pantPocketsDoubleOrTriplePleats")[a].style.display = "none";
						}*/	
					}catch(e){
						console.log("EXCEPTION 1");
					}	
					
					
					
					/*
					try{	
						if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title == 'Select Pocket'){
				    		if(document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[0].selected != true){
				    			document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[0].selected = true;
				    			if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
						    		evt.initEvent("change", true, true);
						    		document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
						    	}else{
						    		document.getElementsByName('pantPockets')[a].fireEvent("onchange");				
					    		}	
				    		}
				    		if(document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[0].selected != true){
				    			document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[0].selected = true;
				    			if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
						    		evt.initEvent("change", true, true);
						    		document.getElementsByName('pantPockets2')[a].dispatchEvent(evt);
						    	}else{
						    		document.getElementsByName('pantPockets2')[a].fireEvent("onchange");				
					    		}	
				    		}
									    	
				    	}else if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title == 'No Pocket'){
				    		if(document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[1].selected != true){
				    			document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[1].selected = true;
				    			if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
						    		evt.initEvent("change", true, true);
						    		document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
						    	}else{
						    		document.getElementsByName('pantPockets')[a].fireEvent("onchange");				
					    		}	
				    		}
				    		if(document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[1].selected != true){
				    			document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[1].selected = true;
				    			if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
						    		evt.initEvent("change", true, true);
						    		document.getElementsByName('pantPockets2')[a].dispatchEvent(evt);
						    	}else{
						    		document.getElementsByName('pantPockets2')[a].fireEvent("onchange");				
					    		}	
				    		}
									    	
				    	}else if(this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title == 'Slanted'){
							if(document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[2].selected != true){
				    			document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[2].selected = true;
				    			if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
						    		evt.initEvent("change", true, true);
						    		document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
						    	}else{
						    		document.getElementsByName('pantPockets')[a].fireEvent("onchange");				
					    		}	
				    		}
				    		if(document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[2].selected != true){
				    			document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[2].selected = true;
				    			if ("createEvent" in document) {
					    			var evt = document.createEvent("HTMLEvents");
						    		evt.initEvent("change", true, true);
						    		document.getElementsByName('pantPockets2')[a].dispatchEvent(evt);
						    	}else{
						    		document.getElementsByName('pantPockets2')[a].fireEvent("onchange");				
					    		}	
				    		}		    	
				    	}
					}catch(e){
						console.log("EXCEPTION 2");
					}	*/	
				}
			
			}
					
			return false;		
		};  		
		    
		    

  
 ///////////////////////////////////////////////////////////////               
            this.sync = function() {
console.log('APP.JS SYNC');      
			/*		
					data0 = { 
	                    order: that.dumpOrder(),
	                    customer: that.custSelectVM.selectedCustomer(),
	                    measurements: that.measurementsVM.flushModel(),
	                    fitlinejacket: that.measurementsVM.fitlineJacketVM.flushModel(),
	                    fitlinepants: that.measurementsVM.fitlinePantsVM.flushModel(),
	                    bodyshape: that.bodyshapeVM.flushModel(),
	                    user_id: authCtrl.userInfo.user_id,
	                    review: {
	                        urgent: that.urgent(),
	                        notes: that.reviewNotes(),
	                        deposit: that.deposit(),
	                        fitting_city: that.fitting_city(),
	                        paymentMethod: that.paymentMethod(),
	                        DOP_day: that.DOP_day(),
	                        DOP_month: that.DOP_month()
	                    },
	                    dsregistry: DsRegistry
					};
console.log("testing: " + JSON.stringify( data0 ) ); // JSON.stringify( dsRegistry.getDatasource('measurementsDS') )
*/
				var errorstring = that.errorfunction();
				
				if(errorstring.length > 2){
				//	console.log(error);
				//	console.log('sync measurement error');
				//	console.log(errorstring);
					customAlert(errorstring);

					
				}else{
					var spinner = document.getElementById('loading_jp');
    				spinner.style.display = "block";

					var photosarray = [];
				    var photos = localStorage.getItem('photos');
					photos = (photos == null) ? [] : JSON.parse(photos);
					if(localStorage.getItem('photos') != null){
					    if(localStorage.getItem('photos').length > 0){
						    for(var i=0, len=photos.length; i<len; i++){
						    	if(photos[i].customer_id == that.custSelectVM.selectedCustomer().server_id){
						    		var obj = new Object();
   									obj.name = photos[i].picture;
   									obj.id  = "" + photos[i].image_id + ""; 
   									var jsonString= JSON.stringify(obj);
						    		//photosarray.push(photos[i].picture);
						    		photosarray.push(obj);
						    	}	
						 	}
						}  
				    }
					
					var tagsarray = [];
				    var tags = localStorage.getItem('tags');
					tags = (tags == null) ? [] : JSON.parse(tags);
					if(localStorage.getItem('tags') != null){
					    if(localStorage.getItem('tags').length > 0){
						    for (var j=0; j<tags.length; j++){
						    	for(var i=0; i<photos.length; i++){
						    		if(tags[j].image_id == photos[i].image_id){
						    			tagsarray.push(tags[j]);
						    		}
						    	}
						 	}
						}  
				    }

					var fitlinephotosarray = [];
				    var fitlinephotos = localStorage.getItem('fitlinephotos');
					fitlinephotos = (fitlinephotos == null) ? [] : JSON.parse(fitlinephotos);
					if(localStorage.getItem('fitlinephotos') != null){
					    if(localStorage.getItem('fitlinephotos').length > 0){
						    for(var i=0, len=fitlinephotos.length; i<len; i++){
						    	if(fitlinephotos[i].customer_id == that.custSelectVM.selectedCustomer().server_id){
						    		fitlinephotosarray.push(fitlinephotos[i].picture);
						    	}	
						 	}
						}  
				    }
					
					var fitlinetagsarray = [];
				    var fitlinetags = localStorage.getItem('fitlinetags');
					fitlinetags = (fitlinetags == null) ? [] : JSON.parse(fitlinetags);
					if(localStorage.getItem('fitlinetags') != null){
					    if(localStorage.getItem('fitlinetags').length > 0){
						    for (var j=0; j<fitlinetags.length; j++){
						    	for(var i=0; i<fitlinephotos.length; i++){
						    		if(fitlinetags[j].image_id == fitlinephotos[i].image_id){
						    			fitlinetagsarray.push(fitlinetags[j]);
						    		}
						    	}
						 	}
						}  
				    }
				    
					var signatureid = 0;
				    var signaturephotos = localStorage.getItem('signature');
					signaturephotos = (signaturephotos == null) ? [] : JSON.parse(signaturephotos);
					if(localStorage.getItem('signature') != null){
					    if(localStorage.getItem('signature').length > 0){
						    signatureid = signaturephotos[0].image_id;
						}  
				    }				    
//console.log("signatureid: " + signatureid);

					var creditcardid = 0;
				    var creditcardphotos = localStorage.getItem('CreditCardPhoto');
					creditcardphotos = (creditcardphotos == null) ? [] : JSON.parse(creditcardphotos);
					if(localStorage.getItem('CreditCardPhoto') != null){
					    if(localStorage.getItem('CreditCardPhoto').length > 0){
						    creditcardid = creditcardphotos[0].image_id;
						}  
				    }

					var urgentDigit;
					if(that.urgent() == true){
						urgentDigit = 1;
					}else{
						urgentDigit = 0;
					}
					
					var futureDigit;
					if(that.future() == true){
						futureDigit = 1;
					}else{
						futureDigit = 0;
					}
					
					var companyinvoiceDigit;
					if(that.companyinvoice() == true){
						companyinvoiceDigit = 1;
					}else{
						companyinvoiceDigit = 0;
					}
					
					var discountCodePost;
					var discountAmountPost;
					if(that.checkDiscountCode() == 1){
						discountCodePost = that.discountCode();
						discountAmountPost = that.calculateDiscount();
					}else{
						discountCodePost = '';
						discountAmountPost = 0;
					}
					
					var paymethod;
					if(that.paymentMethod() == '' || that.paymentMethod() == undefined){
						paymethod = '';
					}else{
						paymethod = that.paymentMethod();
					}	
					
					var fittingcitypost;
					if(that.fitting_city() == '' || that.fitting_city() == undefined){						
						this.cities     = ko.observableArray(dsRegistry.getDatasource('citiesDS').getStore());
						for (i in this.cities()){
							if (this.cities()[i].location_name == 'none' ) {
								fittingcitypost = this.cities()[i].locations_id;								
							}
						}	
					}else{
						fittingcitypost = that.fitting_city();
					}
					
					var extracostspost = that.extracosts;
					for(var x = 0; x < extracostspost().length; x++){				
						if(extracostspost()[x].extracosttext().length == 0  || (extracostspost()[x].extracost()*1.0) <= 0){
							extracostspost.remove(extracostspost()[x]);
							x--;
						}
					}
					
					//customerpost = that.custSelectVM.selectedCustomer();

	                data = { 
	                    order: that.dumpOrder(),
	                    customer: that.custSelectVM.selectedCustomer(),
	                    measurements: that.measurementsVM.flushModel(),
	                    fitlinejacket: that.measurementsVM.fitlineJacketVM.flushModel(),
	                    fitlinepants: that.measurementsVM.fitlinePantsVM.flushModel(),
	                    fitline_images: fitlinephotosarray,
	                    fitlinetags: fitlinetagsarray,
	                    customer_images: photosarray,
	                    tags: tagsarray,
	                    customer_images_notes: that.orderMediaNotes(),
	                    bodyshape: that.bodyshapeVM.flushModel(),
	                    user_id: authCtrl.userInfo.user_id,
	                    review: {
	                    	sample: that.sample(),
	                    	sampletext: that.sampletext(),
	                        urgent: urgentDigit,
	                        future: futureDigit,
	                        notes: that.reviewNotes(),
	                        totalcost: that.computePriceWithDiscount(),
	                        GST: that.getGST() * 100,
	                        deposit: that.deposit(),
	                       // extracost: that.extracost(),
	                       // extracosttext: that.extracosttext(),
	                       	extracosts: ko.toJSON(extracostspost()),
	                        discount: discountAmountPost,
	                        discountcode: discountCodePost,
	                        fitting_city: fittingcitypost,
	                        order_city: that.order_city(),
	                        paymentMethod: paymethod,
	                        currency: that.getSellerCurrency(),
	                        referer: that.referer(),
	                        DOP_day: that.DOP_day(),
	                        DOP_month: that.DOP_month(),
	                        customer_signature: signatureid,
	                        customer_credit_card: creditcardid,
	                        companyinvoice: companyinvoiceDigit,
	                        invoicecompanyname: that.invoicecompanyname(),
            				invoiceproductdescription: that.invoiceproductdescription(),
            				fitlinevideo: that.fitlinevideo(),
        					//fitlinevideonotes: that.fitlinevideonotes(),
        					garmentvideo: that.garmentvideo(),
        					//garmentvideonotes: that.garmentvideonotes(),
        					customervideo: that.customervideo(),
        					//customervideonotes: that.customervideonotes()
        					videonotes: that.videonotes()
	                    }
	                };

					var datatopost0 = JSON.stringify(data);
					datatopost0 = datatopost0.replace("\"isDirty\":true,", "");
					while( datatopost0.indexOf("??") >= 0 ){//datatopost0.contains("??")){
						datatopost0 = datatopost0.replace("??", "?");
					}	
					datatopost0 = datatopost0.replace(/&/g,'and');
					var datatopost = 'data='.concat(datatopost0);
console.log(datatopost);
					
			    	request = $.ajax({
			            type: 'POST',
			            url: BUrl + 'client/order_endpoint',
						timeout: 45000, // in milliseconds, use 25000 out of testing
						dataType: 'json',
			            data: datatopost,
			            error: function(dataS){
	       					customAlert("Error in submitting the order. <br/>Error type: " + dataS.statusText + "<br/>Check your data and try again.");
   							var spinner = document.getElementById('loading_jp'); 
   							spinner.style.display = "none";
	    				},
			            success: function(dataS) {
							spinner.style.display = "none";
							console.log(JSON.stringify(dataS));
		                    dataS = JSON.parse(JSON.stringify(dataS));
		                    
		                    if (dataS.valid == '1') {
		                    	thephotos = localStorage.getItem('photos');
		                    	thephotos = (thephotos === null) ? [] : JSON.parse(thephotos);
		                    	for (var a=thephotos.length - 1; a >= 0; a--){	                    		
		                    		if(thephotos[a].customer_id == that.custSelectVM.selectedCustomer().server_id){
		                    			thephotos.splice(a, 1);
		                    		}	
		                    	}
							    thetags = localStorage.getItem('tags');
								thetags = (thetags == null) ? [] : JSON.parse(thetags);
								if(localStorage.getItem('tags') != null){
								    if(localStorage.getItem('tags').length > 0){
									    for (var j=thetags.length-1; j>=0; j--){
									    	var tagimagefound = false;
									    	for(var i=0; i<thephotos.length; i++){
									    		if(thetags[j].image_id == thephotos[i].image_id){
									    			tagimagefound = true;
	console.log("tag found");
									    		}
									    	}
									    	if(tagimagefound == false){
									    		thetags.splice(j, 1);
									    	}
									 	}
									}  
							    }	                    	
								localStorage.removeItem('signature');
								localStorage.removeItem('CreditCardPhoto');
		                    	var tmp = [];
		                    	for(var x = 0; x < thephotos.length; x++){
		                    		tmp.push({customer_id:thephotos[x].customer_id,image_id:thephotos[x].image_id,picture:thephotos[x].picture,thumbnail:thephotos[x].thumbnail});
		           				}
		           				localStorage.removeItem('photos');
		           				localStorage.setItem('photos', JSON.stringify(tmp));
		                    	var tmptags = [];
		                    	for(var x = 0; x < thetags.length; x++){
		                    		tmptags.push({image_id:thetags[x].image_id,left:thetags[x].left,top:thetags[x].top,text:thetags[x].text});
		           				}
		           				localStorage.removeItem('tags');
		           				localStorage.setItem('tags', JSON.stringify(tmptags));
		           ///////////////////////////////////
		                    	thefitlinephotos = localStorage.getItem('fitlinephotos');
		                    	thefitlinephotos = (thefitlinephotos === null) ? [] : JSON.parse(thefitlinephotos);
		                    	for (var a=thefitlinephotos.length - 1; a >= 0; a--){	                    		
		                    		if(thefitlinephotos[a].customer_id == that.custSelectVM.selectedCustomer().server_id){
		                    			thefitlinephotos.splice(a, 1);
		                    		}	
		                    	}
		                    	
							    thefitlinetags = localStorage.getItem('fitlinetags');
								thefitlinetags = (thefitlinetags == null) ? [] : JSON.parse(thefitlinetags);
								if(localStorage.getItem('fitlinetags') != null){
								    if(localStorage.getItem('fitlinetags').length > 0){
									    for (var j=thefitlinetags.length-1; j>=0; j--){
									    	var tagimagefound = false;
									    	for(var i=0; i<thefitlinephotos.length; i++){
									    		if(thefitlinetags[j].image_id == thefitlinephotos[i].image_id){
									    			tagimagefound = true;
	console.log("tag found");
									    		}
									    	}
									    	if(tagimagefound == false){
									    		thefitlinetags.splice(j, 1);
									    	}
									 	}
									}  
							    }	                    	

		                    	var tmp = [];
		                    	for(var x = 0; x < thefitlinephotos.length; x++){
		                    		tmp.push({customer_id:thefitlinephotos[x].customer_id,image_id:thefitlinephotos[x].image_id,picture:thefitlinephotos[x].picture,thumbnail:thefitlinephotos[x].thumbnail});
		           				}
		           				localStorage.removeItem('fitlinephotos');
		           				localStorage.setItem('fitlinephotos', JSON.stringify(tmp));
		                    	var tmpfitlinetags = [];
		                    	for(var x = 0; x < thefitlinetags.length; x++){
		                    		tmpfitlinetags.push({image_id:thefitlinetags[x].image_id,left:thefitlinetags[x].left,top:thefitlinetags[x].top,text:thefitlinetags[x].text});
		           				}
		           				localStorage.removeItem('fitlinetags');
		           				localStorage.setItem('fitlinetags', JSON.stringify(tmpfitlinetags));	           				
		                    	//that.custSelectVM.selectedCustomer().server_id	                    	
		                        // updates selected customer with server_id
		                        for (i in custData.store) {
		                            if (custData.store[i].id == that.custSelectVM.selectedCustomer().id) {
		                                custData.store[i].server_id = parseInt(dataS.customer_id);
		                                that.custSelectVM.selectedCustomer().server_id = parseInt(dataS.customer_id);
		                                custData.modelUpdate();
		                                break;
		                            }
		                        }
		                        // msg + shipping date
		                        weekday = [];
		                        weekday[0]="Sunday";
		                        weekday[1]="Monday";
		                        weekday[2]="Tuesday";
		                        weekday[3]="Wednesday";
		                        weekday[4]="Thursday";
		                        weekday[5]="Friday";
		                        weekday[6]="Saturday";
		                        t = dataS.shipping.split(/[- :]/);
		                        new_date =  new Date(t[0], t[1]-1, t[2]);
		                        shipping_date = new_date.getDate() + '/' + (parseInt(new_date.getMonth()) + 1) + '/' + new_date.getFullYear();
		                        console.log('success');
		                        setTimeout(function() {
		                            that.clearOrder()
		                        }, 1000);
		                        $.mobile.changePage('#main');
		                        $.jGrowl('Order has been saved!');
		                        orderItem  = new OrderItem(dsRegistry);
		                    }else{
		                        console.log('fail');
		                        $.jGrowl(dataS.msg);
		                    }
		                    spinner.style.display = "none";			            	
			        	}
		        	});    	
		        	
	        	}        
            }

            this.selectedCustomer = ko.computed(function() {   	
                if ((typeof(that.custSelectVM.selectedCustomer()) != 'undefined') && (that.custSelectVM.selectedCustomer() != {})) {
                    return that.custSelectVM.selectedCustomer().customer_first_name + ' ' + that.custSelectVM.selectedCustomer().customer_last_name;
                } else {
                    return "";
                }
            });
            
            this.selectedOrder = ko.computed(function() {   	
                if ((typeof(that.custOrdersVM.selectedOrder()) != 'undefined') && (that.custOrdersVM.selectedOrder() != {})) {
                    return that.custOrdersVM.selectedOrder().customer_id + ' ' + that.custOrdersVM.selectedOrder().order_id;
                } else {
                    return "";
                }
            });
            
			this.selectedOrderGarment = ko.computed(function() {   	
                if ((typeof(that.custOrderVM.selectedOrderGarment()) != 'undefined') && (that.custOrderVM.selectedOrderGarment() != {})) {
                    return that.custOrderVM.selectedOrderGarment().orders_products_id + ' ' + that.custOrderVM.selectedOrderGarment().garment_name;
                } else {
                    return "";
                }
            });
            
            
            
            /*
			this.selectedOrder = ko.computed(function() {   	
                if ((typeof(that.customerOrdersVM.selectedOrder()) != 'undefined') && (that.customerOrdersVM.selectedOrder() != {})) {
                    return that.customerOrdersVM.selectedOrder().customer_id + ' ' + that.customerOrdersVM.selectedOrder().order_id;
                } else {
                    return "";
                }
            });            
            */

	},	// END OF INIT

        
		clearStorageAndExit: function(data) {
//console.log("clearStorageAndExit");
			localStorage.clear();
			//$.mobile.changePage('index.htm'); 
			window.location = "index.htm";
		},
		
        
        cancelOrder: function(data) {
console.log('cancelOrder');
            var that = this;
            if(!confirm('Are you sure you want to cancel the current order?')) return;
            orderItem  = new OrderItem(dsRegistry);
            $.mobile.changePage('#main');
            $.jGrowl('Order cancelled');
            setTimeout(function() {
                that.clearOrder()
            }, 1000);		
        },
/*
        logOut: function(data) {
console.log('logOut');        	
            //var that = this;
            $.mobile.changePage('#firstPage');
        },
*/

        clearOrder: function() {
console.log('clearOrder');
			orderItem  = new OrderItem(dsRegistry);
            var that=this;
            this.garmentsList  = new ko.observableArray([
            {
                id: 0, 
                title: "Jacket", 
                image: "gfx/measurements/jacket.png", 
                active: false , 
                count: ko.observable(0)
            },
            {
                id: 1, 
                title: "Pant"  , 
                image: "gfx/measurements/pants.png", 
                active: false, 
                count: ko.observable(0)
            } , 
{
                id: 2, 
                title: "Suit"  , 
                image: "gfx/measurements/suit.png", 
                active: false, 
                count: ko.observable(0)
            },

            {
                id: 3, 
                title: "Vest"  , 
                image: "gfx/measurements/vest.png", 
                active: false, 
                count: ko.observable(0)
            },

            {
                id: 4, 
                title: "Shirt" , 
                image: "gfx/measurements/shirt.png", 
                active: false, 
                count: ko.observable(0)
            }
            ]);
            this.custSelectVM.selectedCustomer({});
            try{
            	this.bodyshapeVM.clear();
            }catch(e){ ; }	
            this.cancelMode = true;
            this.currentBigStep(that.bigSteps()[0]);
        },

        setSelectedCustomer: function(data) {
            var selection = this.custSelectVM.selectedCustomer();
            customersVM.selected_item(selection);
           // if (!$.isEmptyObject(data)) $.mobile.changePage('#orderItemCustomerInfo');
console.log("customersVM.systemMode: " + customersVM.systemMode);           
            if(customersVM.systemMode == "order"){ 
				if(!$.isEmptyObject(data)) $.mobile.changePage('#orderItemCustomerInfo');
			}else if(customersVM.systemMode == "customer"){
				if(!$.isEmptyObject(data)) $.mobile.changePage('#customerInfo');
			}else if(customersVM.systemMode == "shipping_alterations"){	
				$.mobile.changePage('#shipping_alterations_list');
			}else if(customersVM.systemMode == "shipping_fabrics"){
				$.mobile.changePage('#shipping_fabrics');
			}else if(customersVM.systemMode == "shipping_copy_garment"){
				//$.mobile.changePage('#shipping_copy_garment');	// MOVED TO syncmeasurementsVM
			}else if(customersVM.systemMode == "shipping_faulty_garments"){
				$.mobile.changePage('#shipping_faulty_remake_garments');
			}else if(customersVM.systemMode == "shipping_remake_garments"){
				//$.mobile.changePage('#shipping_faulty_remake_garments');	 // MOVED TO syncmeasurementsVM
			}else if(customersVM.systemMode == "shipping_other"){
				$.mobile.changePage('#shipping_other');
			}
			
			/*	
			}else{	
				if (!$.isEmptyObject(data)) {	
					var origin = document.getElementById("shipping_alterations_select_customer");
					if(origin != null ){
						$.mobile.changePage('#shipping_alterations_list');
					}
					origin = document.getElementById("shipping_fabrics_select_customer");
					if(origin != null ){
						$.mobile.changePage('#shipping_fabrics');
					}
					//console.log("test: " + origin);
					origin = undefined;	
				}	
			}*/	
        },
        
		setSelectedCustomerFromList: function(data) {			
            var selection = this.custSelectVM.selectedCustomer();
            customersVM.selected_item(selection);
            if (!$.isEmptyObject(data)) $.mobile.changePage('#customerInfo');
        },
        
        selectedCustomerEdit: function(data) {
            customersVM.selectCustomer(this.custSelectVM.selectedCustomer().id, '#' + $('.ui-page-active').attr('id'));
        },

        setSelectedOrder: function(data) {      
            var selection = this.custOrdersVM.selectedOrder();            
            customersVM.selected_order(selection); 
         	if (!$.isEmptyObject(data)) $.mobile.changePage('#orderInfo');
        },
        
        setSelectedOrderGarment: function(data) {      
            var selection = this.custOrderVM.selectedOrderGarment();            
            customersVM.selected_ordergarment(selection); 
         	//if (!$.isEmptyObject(data)) $.mobile.changePage('#orderInfo');
         	// if jacket view jacket, if pants view pants etc
        },
        
        /*
        setSelectedOrder: function(data) {      
            var selection = this.customerOrdersVM.selectedOrder();
console.log("setSelectedOrder " + JSON.stringify(selection));            
            orderItem.selected_item(selection); 
         	if (!$.isEmptyObject(data)) $.mobile.changePage('#orderInfo');
         	
console.log("orderItem.selected_item: " + JSON.stringify(orderItem.selected_item) )         	
        },
        */
        
        submitOrder: function() {
            $.mobile.changePage('#main');
            $.jGrowl('Order submitted succesfully');
        }
    });

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




    //     ##   ##    ##   ##   ########
    //     ##   ###   ##   ##      ##
    //     ##   ## #  ##   ##      ##
    //     ##   ##  # ##   ##      ##
    //     ##   ##   ###   ##      ##
    //     ##   ##    ##   ##      ##
 
 	authCtrl = new AuthControl();
 	/*
 	$(window).error(function(e){ 
 		var spinner = document.getElementById('loading_jp'); 
 		spinner.style.display = "none";
 		customAlert('Error: ' + errorMsg + '<br/>Script: ' + url + '<br/>Line: ' + lineNumber);
 		e.preventDefault(); 
	}); // IGNORE ALL ERROR JQUERY!
 	*/
 	
 	/*
 	window.onerror=function(errorMsg, url, lineNumber){
 		var spinner = document.getElementById('loading_jp'); 
 		spinner.style.display = "none";
 		customAlert('Error: ' + errorMsg + '<br/>Script: ' + url + '<br/>Line: ' + lineNumber);
 		return true;
 		//customAlert("An unexpected error has occurred! Starting over would be wise!");
	}*/
	
 
//    localStorage['fabricsDS']   = '{"store":[{"id":0,"title":"Select Fabric","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/none.png","price":0},{"id":1,"title":"6503","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6503.jpg","price":299},{"id":2,"title":"6510","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6510.jpg","price":299},{"id":3,"title":"6519","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6519.jpg","price":299},{"id":4,"title":"6520","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6520.jpg","price":299},{"id":5,"title":"6527","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6527.jpg","price":299},{"id":6,"title":"6540","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6540.jpg","price":299},{"id":7,"title":"H7102","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7102.jpg","price":2450},{"id":8,"title":"H7104","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7104.jpg","price":2450},{"id":9,"title":"H7105","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7105.jpg","price":2450},{"id":10,"title":"H7106","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7106.jpg","price":2450},{"id":11,"title":"H7107","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7107.jpg","price":2450},{"id":12,"title":"H7108","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7108.jpg","price":2450},{"id":13,"title":"H7109","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7109.jpg","price":2450},{"id":14,"title":"H7110","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7110.jpg","price":2450},{"id":15,"title":"H7111","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7111.jpg","price":2450},{"id":16,"title":"H7103","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7103.jpg","price":2450}],"timestamp":0}';
//    localStorage['fabricsDSp']  = '{"store":[{"id":0,"title":"Select Fabric","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/none.png","price":0},{"id":1,"title":"6503","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6503.jpg","price":299},{"id":2,"title":"6510","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6510.jpg","price":299},{"id":3,"title":"6519","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6519.jpg","price":299},{"id":4,"title":"6520","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6520.jpg","price":299},{"id":5,"title":"6527","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6527.jpg","price":299},{"id":6,"title":"6540","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/6540.jpg","price":299},{"id":7,"title":"H7102","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7102.jpg","price":2450},{"id":8,"title":"H7104","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7104.jpg","price":2450},{"id":9,"title":"H7105","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7105.jpg","price":2450},{"id":10,"title":"H7106","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7106.jpg","price":2450},{"id":11,"title":"H7107","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7107.jpg","price":2450},{"id":12,"title":"H7108","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7108.jpg","price":2450},{"id":13,"title":"H7109","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7109.jpg","price":2450},{"id":14,"title":"H7110","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7110.jpg","price":2450},{"id":15,"title":"H7111","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7111.jpg","price":2450},{"id":16,"title":"H7103","fabricImage":"http://shirt-tailor.net/thepos/appimg/fabrics/H7103.jpg","price":2450}],"timestamp":0}';


  //  localStorage['pocketTypeDS'] = '{"store":[{"title":"Undefined","styleImage":"http://shirt-tailor.net/thepos/appimg/pocketStyle/style1.png","id":0},{"title":"Inverted Pleat","styleImage":"http://shirt-tailor.net/thepos/appimg/pocketStyle/style1.png","id":1},{"title":"Box Pleat","styleImage":"http://shirt-tailor.net/thepos/appimg/pocketStyle/style2.png","id":2},{"title":"Classic","styleImage":"http://shirt-tailor.net/thepos/appimg/pocketStyle/style3.png","id":3}],"timestamp":0}';

   	localStorage['bodyshapeShouldersDS'] = '{"store":[{"title":"Default","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png","id":"0"},{"title":"Both slightly sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png","id":"1"},{"title":"Both very sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-very-sloped.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-normal.png","id":"3"},{"title":"Left normal Right sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-normal-left-sloped.png","id":"4"},{"title":"Left slightly sloped Right very sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-slightly-sloped-left-very-sloped.png","id":"5"},{"title":"Right normal Left sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-sloped-left-normal.png","id":"6"},{"title":"Right slightly sloped Left very sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-very-sloped-left-slightly-sloped.png","id":"7"},{"title":"Square","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-square.png","id":"8"}],"timestamp":0}';
//    localStorage['bodyshapeShouldersDS'] = '{"store":[{"title":"Both slightly sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png","id":"1"},{"title":"Both very sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-very-sloped.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-normal.png","id":"3"},{"title":"Right normal left sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-normal-left-sloped.png","id":"4"},{"title":"Right slightly sloped left very sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-slightly-sloped-left-very-sloped.png","id":"5"},{"title":"Right sloped left normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-sloped-left-normal.png","id":"6"},{"title":"Right very sloped left slightly sloped","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-very-sloped-left-slightly-sloped.png","id":"7"},{"title":"Square","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-square.png","id":"8"}],"timestamp":0}';
   // localStorage['bodyshapeNeckStanceDS'] = '{"store":[{"title":"Default","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png","id":"0"},{"title":"Head backwords","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckstance/head_backwords.png","id":"1"},{"title":"Neck backwords","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckstance/neck_backwords.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckstance/normal.png","id":"3"}],"timestamp":0}';
//    localStorage['bodyshapeNeckStanceDS'] = '{"store":[{"title":"Head backwards","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckstance/head_backwords.png","id":"1"},{"title":"Neck backwords","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckstance/neck_backwords.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckstance/normal.png","id":"3"}],"timestamp":0}';
    localStorage['bodyshapeArmsDS'] = '{"store":[{"title":"Default","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png","id":"0"},{"title":"Backwards arms","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/backwards_arms.png","id":"1"},{"title":"Balanced","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/balanced.png","id":"2"},{"title":"Forwards arms","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/forwards_arms.png","id":"3"}],"timestamp":0}';
//    localStorage['bodyshapeArmsDS'] = '{"store":[{"title":"Backwards arms","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/backwards_arms.png","id":"1"},{"title":"Balanced","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/balanced.png","id":"2"},{"title":"Forwards arms","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/forwards_arms.png","id":"3"}],"timestamp":0}';
    localStorage['bodyshapeBackDS'] = '{"store":[{"title":"Curved back","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-curved-back.png","id":"1"},{"title":"Hump back","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-hump-back.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-normal.png","id":"3"},{"title":"Upper back curve","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-upper-back-curve.png","id":"4"},{"title":"Upper back curved forward","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-upper-back-curved-forward.png","id":"5"}],"timestamp":0}';
    //localStorage['bodyshapeBeltAngleDS'] = '{"store":[{"title":"belt-position-high-waist-slightly-bulging-stomach","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-high-waist-slightly-bulging-stomach.png","id":"1"},{"title":"belt-position-high-waist-very-bulging-stomach","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-high-waist-very-bulging-stomach.png","id":"2"},{"title":"belt-position-low-in-front-large-belly","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-low-in-front-large-belly.png","id":"3"},{"title":"belt-position-low-in-front-very-bulging-stomach","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-low-in-front-very-bulging-stomach.png","id":"4"},{"title":"belt-position-straight-across","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-straight-across.png","id":"5"},{"title":"belt-position-under-the-belly","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-under-the-belly.png","id":"6"},{"title":"belt-position-very-low-in-front-very-bulging-stomach","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-very-low-in-front-very-bulging-stomach.png","id":"7"},{"title":"belt-position-very-low-under-belly","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltangle/belt-position-very-low-under-belly.png","id":"8"}],"timestamp":0}';
   // localStorage['bodyshapeBeltHeightDS'] = '{"store":[{"title":"belt-position-high-waist","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltheight/belt-position-high-waist.png","id":"1"},{"title":"belt-position-low-waist","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltheight/belt-position-low-waist.png","id":"2"},{"title":"belt-position-normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltheight/belt-position-normal.png","id":"3"},{"title":"belt-position-on-navel","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltheight/belt-position-on-navel.png","id":"4"},{"title":"belt-position-slightly-high-waist","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltheight/belt-position-slightly-high-waist.png","id":"5"},{"title":"belt-position-very-low-on-hips","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_beltheight/belt-position-very-low-on-hips.png","id":"6"}],"timestamp":0}';
    localStorage['bodyshapeFrontDS'] = '{"store":[{"title":"Caving in","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-caving-in.png","id":"1"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-normal.png","id":"2"},{"title":"Slightly protruding","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-slightly-protruding.png","id":"3"},{"title":"Chicken","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-chicken.png","id":"5"},{"title":"Well padded chest","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-well-padded-chest.png","id":"4"}],"timestamp":0}';
    localStorage['bodyshapeLegDS'] = '{"store":[{"title":"Bow legged","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/bow_legged.png","id":"1"},{"title":"Inwards","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/inwards.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/normal.png","id":"3"},{"title":"Outwards","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/outwards.png","id":"4"}],"timestamp":0}';
    localStorage['bodyshapeNeckHeightDS'] = '{"store":[{"title":"Long","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckheight/long_neck.png","id":"1"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckheight/normal_neck.png","id":"2"},{"title":"Very Short","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckheight/very_short_neck.png","id":"3"}],"timestamp":0}';
   // localStorage['bodyshapeSeatDS'] = '{"store":[{"title":"seat-stance-flat","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_seat/seat-stance-flat.png","id":"1"},{"title":"seat-stance-heavy","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_seat/seat-stance-heavy.png","id":"2"},{"title":"seat-stance-normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_seat/seat-stance-normal.png","id":"3"},{"title":"seat-stance-no-seat","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_seat/seat-stance-no-seat.png","id":"4"},{"title":"seat-stance-prominent","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_seat/seat-stance-prominent.png","id":"5"},{"title":"seat-stance-very-bulging","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_seat/seat-stance-very-bulging.png","id":"6"}],"timestamp":0}';
   // localStorage['bodyshapeSleeveDS'] = '{"store":[{"title":"equal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_sleeve/equal.png","id":"1"},{"title":"left_longer","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_sleeve/left_longer.png","id":"2"},{"title":"right_longer","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_sleeve/right_longer.png","id":"3"}],"timestamp":0}';
    localStorage['bodyshapeStandingDS'] = '{"store":[{"title":"Backward","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/backward.png","id":"1"},{"title":"Neutral","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/neutral.png","id":"2"},{"title":"Slight lean","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/slight_lean.png","id":"3"},{"title":"Strong lean","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/strong_lean.png","id":"4"}],"timestamp":0}';
    localStorage['bodyshapeStomachDS'] = '{"store":[{"title":"Beer belly","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-beer-belly.png","id":"1"},{"title":"Bulge","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-bulge.png","id":"2"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-normal.png","id":"3"},{"title":"Pot belly","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-pot-belly.png","id":"4"},{"title":"Slight bulge","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-slight-bulge.png","id":"5"},{"title":"Washboard","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-washboard.png","id":"6"}],"timestamp":0}';
    localStorage['bodyshapeThighDS'] = '{"store":[{"title":"Heavy legs","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-heavy-legs.png","id":"1"},{"title":"Large muscular thighs","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-large-muscular-thighs.png","id":"2"},{"title":"Thick thighs","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-thick-thighs.png","id":"3"},{"title":"Thin legs","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-thin-legs.png","id":"4"},{"title":"Very erect prominent calves","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-very-erect-prominent-calves.png","id":"5"},{"title":"Normal","bodyshapeImage":"http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-normal.png","id":"6"}],"timestamp":0}';

    localStorage['vestLapelStyleDS']   = '{"store":[{"title":"Style V","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/none.png","id":"0"},{"title":"Style V","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/none.png","id":"4"},{"title":"Notch lapel","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/notch.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/notch_preview.png","id":"1"},{"title":"Peak","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/peak.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/peak_preview.png","id":"2"},{"title":"Shawl lapel V","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png","id":"3"},{"title":"Custom","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png","id":"123"}],"timestamp":0}';

    // IMAGES MUST CHANGE
 //   localStorage['vestDesignDS']   = '{"store":[{"title":"WC 1","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/none.png","id":"1"},{"title":"WC 2","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/notch.png", "preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/notch_preview.png","id":"2"},{"title":"WC 3","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/peak.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/peak_preview.png","id":"3"},{"title":"WC 4","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png", "preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png","id":"4"},{"title":"Custom","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png", "preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png","id":"123"}],"timestamp":0}';
    localStorage['vestDesignDS']   = '{"store":[{"title":"WC 1","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/none.png","id":"1"},{"title":"WC 2","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/notch.png", "preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/notch_preview.png","id":"2"},{"title":"WC 3","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/peak.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/peak_preview.png","id":"3"},{"title":"WC 4","image":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png", "preview":"http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png","id":"4"}],"timestamp":0}'; 

	// NEEDS THREE MORE OPTIONS FOR DOUBLE BREASTED
//    localStorage['vestButtonNumberDS'] = '{"store":[{"title":"Four buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four_preview.png","id":"1"},{"title":"Five buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/five.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/five_preview.png","id":"2"},{"title":"Six buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png","id":"3"},{"title":"Custom","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png","id":"123"}],"timestamp":0}';
//    localStorage['vestButtonNumberDS'] = '{"store":[{"title":"Four buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four_preview.png","id":"1"},{"title":"Five buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/five.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/five_preview.png","id":"2"},{"title":"Six buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png","id":"3"},{"title":"One Button Up","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/one_up.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/one_up_preview.png","id":"4"},{"title":"Two Button Up","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/two_up.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/two_up_preview.png","id":"5"},{"title":"Three Button Up","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/three_up.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/three_up_preview.png","id":"6"},{"title":"Custom Button","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/custom_button.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/custom_button_preview.png","id":"123"}],"timestamp":0}';
localStorage['vestButtonNumberDS'] = '{"store":[{"title":"Select Buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four_preview.png","id":"0"},{"title":"Four Buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/four_preview.png","id":"1"},{"title":"Five Buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/five.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/five_preview.png","id":"2"},{"title":"Six Buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png","id":"3"},{"title":"Custom Buttons","image":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png","id":"123"}],"timestamp":0}';          

    localStorage['vestPocketStyleDS']  = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/vest/pockets/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/none.png","id":"0"},{"title":"No ","image":"http://shirt-tailor.net/thepos/appimg/vest/pockets/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/none.png","id":"1"},{"title":"Single","image":"http://shirt-tailor.net/thepos/appimg/vest/pockets/single.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/pockets/single_preview.png","id":"2"},{"title":"Patch","image":"http://shirt-tailor.net/thepos/appimg/vest/pockets/patch.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/pockets/patch_preview.png","id":"3"},{"title":"Flap","image":"http://shirt-tailor.net/thepos/appimg/vest/pockets/flap.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/pockets/flap_preview.png","id":"4"}],"timestamp":0}';
    
    localStorage['vestBottomStyleDS']  = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/vest/bottom/notch.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/bottom/notch_preview.png","id":"0"},{"title":"Angle cut","image":"http://shirt-tailor.net/thepos/appimg/vest/bottom/notch.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/bottom/notch_preview.png","id":"1"},{"title":"Straight cut","image":"http://shirt-tailor.net/thepos/appimg/vest/bottom/round.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/bottom/round_preview.png","id":"2"}],"timestamp":0}';
//    localStorage['vestBackStyleDS']    = '{"store":[{"title":"Belt back","image":"http://shirt-tailor.net/thepos/appimg/vest/back/belt.png","id":"1"},{"title":"Plain back","image":"http://shirt-tailor.net/thepos/appimg/vest/back/single.png","id":"2"}],"timestamp":0}';
	localStorage['vestBackStyleDS']    = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/vest/back/belt.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/back/belt_preview.png","id":"0"},{"title":"Belt","image":"http://shirt-tailor.net/thepos/appimg/vest/back/belt.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/back/belt_preview.png","id":"1"},{"title":"Plain","image":"http://shirt-tailor.net/thepos/appimg/vest/back/single.png","preview":"http://shirt-tailor.net/thepos/appimg/vest/back/single_preview.png","id":"2"}],"timestamp":0}';
    localStorage['pipingColorDS']      = '{"store":[{"title":"DEFAULT","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_default.jpg","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_default_preview.png","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_default_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"0"},{"title":"A 1","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_1.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_1_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_1_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","id":"1"},{"title":"A 2","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_2.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_2_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_2_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"2"},{"title":"A 3","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_3.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_3_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_3_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/red.png","id":"3"},{"title":"A 4","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_4.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_4_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_4_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png","id":"4"},{"title":"A 5","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_5.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_5_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_5_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/pink.png","id":"5"},{"title":"A 6","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_6.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_6_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_6_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"6"},{"title":"A 7","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_7.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_7_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_7_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"7"},{"title":"A 8","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_8.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_8_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_8_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","id":"8"},{"title":"A 9","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_9.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_9_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_9_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"9"},{"title":"A 10","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_10.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_10_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_10_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"10"},{"title":"A 11","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_11.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_11_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_11_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"11"},{"title":"A 12","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_12.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_12_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_12_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"12"},{"title":"A 13","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_13.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_13_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_13_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"13"},{"title":"A 14","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_14.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_14_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_14_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"14"},{"title":"A 15","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_15.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_15_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_15_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"15"},{"title":"A 16","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_16.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_16_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_16_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/yellow.png","id":"16"},{"title":"A 17","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_17.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_17_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_17_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"17"},{"title":"A 18","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_18.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_18_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_18_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"18"},{"title":"A 19","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_19.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_19_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_19_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/yellow.png","id":"19"},{"title":"A 20","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_20.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_20_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_20_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/pink.png","id":"20"},{"title":"A 21","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_21.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_21_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_21_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"21"},{"title":"A 22","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_22.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_22_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_22_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/pink.png","id":"22"},{"title":"A 23","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_23.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_23_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_23_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/green.png","id":"23"},{"title":"A 24","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_24.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_24_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_24_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png","id":"24"},{"title":"A 25","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_25.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_25_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_25_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/pink.png","id":"25"},{"title":"A 26","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_26.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_26_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_26_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","id":"26"},{"title":"A 27","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_27.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_27_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_27_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"27"},{"title":"A 28","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_28.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_28_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_28_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png","id":"28"},{"title":"A 29","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_29.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_29_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_29_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png","id":"29"},{"title":"A 30","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_30.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_30_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_30_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","id":"30"},{"title":"A 31","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_31.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_31_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_31_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","id":"31"},{"title":"A 32","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_32.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_32_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_32_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"32"},{"title":"A 33","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_33.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_33_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_33_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/green.png","id":"33"},{"title":"A 34","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_34.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_34_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_34_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png","id":"34"},{"title":"A 35","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_35.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_35_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_35_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"35"},{"title":"A 36","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_36.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_36_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_36_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/red.png","id":"36"},{"title":"A 37","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_37.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_37_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_37_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png","id":"37"},{"title":"A 38","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_38.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_38_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_38_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/green.png","id":"38"},{"title":"A 39","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_39.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_39_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_39_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/charcoal.png","id":"39"},{"title":"A 40","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_40.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_40_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_40_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"40"},{"title":"A 41","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_41.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_41_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_41_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png","id":"41"},{"title":"A 42","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_42.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_42_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_42_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"42"},{"title":"A 43","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_43.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_43_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_43_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/red.png","id":"43"},{"title":"A 44","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_44.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_44_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_44_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"44"},{"title":"A 45","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_45.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_45_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_45_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png","id":"45"},{"title":"A 46","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_46.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_46_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_46_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"46"},{"title":"A 47","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_47.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_47_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_47_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png","id":"47"},{"title":"A 48","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_48.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_48_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_48_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"48"},{"title":"A 49","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_49.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_49_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_49_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"49"},{"title":"A 50","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_50.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_50_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_50_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/charcoal.png","id":"50"},{"title":"A 51","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_51.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_51_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_51_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","id":"51"},{"title":"A 52","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_52.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_52_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_52_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"52"},{"title":"A 53","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_53.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_53_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_53_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"53"},{"title":"A 54","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_54.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_54_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_54_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"54"},{"title":"A 55","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_55.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_55_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_55_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/yellow.png","id":"55"},{"title":"A 56","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_56.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_56_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_56_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"56"},{"title":"A 57","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_57.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_57_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_57_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"57"},{"title":"A 58","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_58.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_58_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_58_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"58"},{"title":"A 59","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_59.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_59_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_59_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"59"},{"title":"A 60","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_60.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_60_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_60_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/yellow.png","id":"60"},{"title":"A 61","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_61.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_61_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_61_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png","id":"61"},{"title":"A 62","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_62.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_62_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_62_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"62"},{"title":"A 63","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_63.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_63_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_63_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"63"},{"title":"A 64","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_64.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_64_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_64_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"64"},{"title":"A 65","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_65.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_65_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_65_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"65"},{"title":"A 66","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_66.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_66_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_66_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","id":"66"},{"title":"A 67","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_67.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_67_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_67_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"67"},{"title":"A 68","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_68.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_68_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_68_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"68"},{"title":"A 69","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_69.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_69_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_69_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","id":"69"},{"title":"A 70","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_70.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_70_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_70_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"70"},{"title":"A 71","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_71.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_71_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_71_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png","id":"71"},{"title":"A 72","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_72.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_72_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_72_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png","id":"72"},{"title":"A 73","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_73.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_73_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_73_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png","id":"73"},{"title":"A 74","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_74.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_74_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_74_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/charcoal.png","id":"74"},{"title":"A 75","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_75.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_75_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_75_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"75"},{"title":"A 76","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_76.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_76_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_76_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"76"},{"title":"A 77","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_77.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_77_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_77_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"77"},{"title":"A 78","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_78.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_78_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_78_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"78"},{"title":"A 79","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_79.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_79_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_79_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png","id":"79"},{"title":"A 80","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_80.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_80_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_80_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"80"},{"title":"A 81","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_81.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_81_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_81_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"81"},{"title":"A 82","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_82.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_82_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_82_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"82"},{"title":"A 83","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_83.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_83_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_83_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"83"},{"title":"A 84","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_84.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_84_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_84_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png","id":"84"},{"title":"A 85","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_85.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_85_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_85_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","id":"85"},{"title":"A 86","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_86.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_86_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_86_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","id":"86"},{"title":"A 87","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_87.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_87_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_87_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"87"},{"title":"A 88","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_88.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_88_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_88_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"88"},{"title":"A 89","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_89.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_89_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_89_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"89"},{"title":"A 90","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_90.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_90_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_90_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/red.png","id":"90"},{"title":"A 91","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_91.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_91_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_91_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"91"},{"title":"A 92","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_92.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_92_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_92_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/green.png","id":"92"},{"title":"A 93","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_93.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_93_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_93_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/green.png","id":"93"},{"title":"A 94","image":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_94.jpg","preview":"http://shirt-tailor.net/thepos/appimg/pipingColors/piping_94_preview.png","inpocket":"http://shirt-tailor.net/thepos/appimg/inpocket/piping_94_preview.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","tpreview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","tpreview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","tpreview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","tpreview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","tpreview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","id":"94"}],"timestamp":0}';
    localStorage['liningFabricDS']     = '{"store":[{"title":"01","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/01.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/01.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/01.png","price":"no","id":"1"},{"title":"02","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/02.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/02.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/02.png","price":"no","id":"2"},{"title":"03","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/03.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/03.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/03.png","price":"no","id":"3"},{"title":"04","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/04.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/04.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/04.png","price":"no","id":"4"},{"title":"05","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/05.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/05.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/05.png","price":"no","id":"5"},{"title":"06","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/06.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/06.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/06.png","price":"no","id":"6"},{"title":"07","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/07.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/07.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/07.png","price":"no","id":"7"},{"title":"08","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/08.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/08.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/08.png","price":"no","id":"8"},{"title":"09","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/09.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/09.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/09.png","price":"no","id":"9"},{"title":"10","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/010.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/010.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/010.png","price":"no","id":"10"},{"title":"11","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/011.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/011.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/011.png","price":"no","id":"11"},{"title":"12","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/012.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/02.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/012.png","price":"no","id":"12"},{"title":"13","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/013.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/013.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/013.png","price":"no","id":"13"},{"title":"14","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/preview/014.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/neck/014.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/sign/014.png","price":"no","id":"14"},{"title":"100KL001","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0037.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0037_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0037_preview.png","price":"yes","id":"37"},{"title":"100KL002","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0034.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0034_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0034_preview.png","price":"yes","id":"34"},{"title":"100KL003","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0033.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0033_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0033_preview.png","price":"yes","id":"33"},{"title":"100KL004","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0036.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0036_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0036_preview.png","price":"yes","id":"20"},{"title":"100KL005","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0036.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0036_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0036_preview.png","price":"yes","id":"36"},{"title":"100KL006","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0042.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0042_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0042_preview.png","price":"yes","id":"42"},{"title":"100KL007","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0042.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0042_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0042_preview.png","price":"yes","id":"185"},{"title":"100KL008","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0045.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0045_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0045_preview.png","price":"yes","id":"45"},{"title":"100KL009","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0045.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0045_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0045_preview.png","price":"yes","id":"186"},{"title":"100KL010","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0035.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0035_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0035_preview.png","price":"yes","id":"35"},{"title":"100KL011","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0035.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0035_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0035_preview.png","price":"yes","id":"187"},{"title":"100KL012","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0044.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0044_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0044_preview.png","price":"yes","id":"44"},{"title":"100KL013","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0043.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0043_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0043_preview.png","price":"yes","id":"43"},{"title":"100KL014","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0043.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0043_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0043_preview.png","price":"yes","id":"188"},{"title":"100KL015","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0039.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0039_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0039_preview.png","price":"yes","id":"39"},{"title":"100KL016","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0039.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0039_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0039_preview.png","price":"yes","id":"189"},{"title":"100KL017","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0041.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0041_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0041_preview.png","price":"yes","id":"41"},{"title":"100KL018","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"190"},{"title":"100KL019","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"38"},{"title":"100KL020","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"191"},{"title":"100KL021","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"192"},{"title":"100KL022","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"193"},{"title":"100KL023","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"194"},{"title":"100KL024","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"195"},{"title":"100KL025","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"196"},{"title":"100KL026","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"197"},{"title":"100KL027","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"198"},{"title":"100KL028","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"199"},{"title":"100KL029","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"200"},{"title":"100KL030","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"201"},{"title":"100KL031","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"202"},{"title":"100KL032","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"203"},{"title":"100KL033","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"204"},{"title":"100KL034","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"205"},{"title":"100KL035","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"206"},{"title":"100KL036","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"207"},{"title":"100KL037","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"208"},{"title":"100KL038","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"209"},{"title":"100KL039","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"210"},{"title":"100KL040","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"211"},{"title":"100KL041","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"212"},{"title":"100KL042","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0055.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0055_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0055_preview.png","price":"yes","id":"55"},{"title":"100KL043","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0054.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0054_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0054_preview.png","price":"yes","id":"54"},{"title":"100KL044","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"213"},{"title":"100KL045","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"214"},{"title":"100KL046","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"215"},{"title":"100KL047","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0052.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0052_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0052_preview.png","price":"yes","id":"52"},{"title":"100KL048","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0051.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0051_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0051_preview.png","price":"yes","id":"51"},{"title":"100KL049","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"216"},{"title":"100KL050","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0053.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0053_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0053_preview.png","price":"yes","id":"53"},{"title":"100KL051","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0056.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0056_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0056_preview.png","price":"yes","id":"56"},{"title":"100KL052","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0057.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0057_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0057_preview.png","price":"yes","id":"57"},{"title":"100KL053","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0060.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0060_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0060_preview.png","price":"yes","id":"60"},{"title":"100KL054","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0061.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0061_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0061_preview.png","price":"yes","id":"61"},{"title":"100KL055","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0065.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0065_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0065_preview.png","price":"yes","id":"65"},{"title":"100KL056","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0066.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0066_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0066_preview.png","price":"yes","id":"66"},{"title":"100KL057","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0067.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0067_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0067_preview.png","price":"yes","id":"67"},{"title":"100KL058","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"217"},{"title":"100KL059","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"218"},{"title":"100KL060","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"219"},{"title":"100KL061","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"220"},{"title":"100KL062","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"221"},{"title":"100KL063","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"222"},{"title":"100KL064","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0091.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0091_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0091_preview.png","price":"yes","id":"91"},{"title":"100KL065","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0085.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0085_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0085_preview.png","price":"yes","id":"85"},{"title":"100KL066","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0086.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0086_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0086_preview.png","price":"yes","id":"86"},{"title":"100KL067","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0087.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0087_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0087_preview.png","price":"yes","id":"87"},{"title":"100KL068","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0088.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0088_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0088_preview.png","price":"yes","id":"88"},{"title":"100KL069","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0089.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0089_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0089_preview.png","price":"yes","id":"89"},{"title":"100KL070","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0090.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0090_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0090_preview.png","price":"yes","id":"90"},{"title":"100KL071","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"223"},{"title":"100KL072","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"224"},{"title":"100KL073","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0118.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0118_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0118_preview.png","price":"yes","id":"118"},{"title":"100KL074","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0117.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0117_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0117_preview.png","price":"yes","id":"117"},{"title":"100KL075","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"225"},{"title":"100KL076","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0059.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0059_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0059_preview.png","price":"yes","id":"59"},{"title":"100KL077","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0058.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0058_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0058_preview.png","price":"yes","id":"58"},{"title":"100KL078","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"226"},{"title":"100KL079","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"227"},{"title":"100KL080","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0049.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0049_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0049_preview.png","price":"yes","id":"49"},{"title":"100KL081","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0049.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0049_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0049_preview.png","price":"yes","id":"183"},{"title":"100KL082","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0048.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0048_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0048_preview.png","price":"yes","id":"48"},{"title":"100KL083","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0079.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0079_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0079_preview.png","price":"yes","id":"79"},{"title":"100KL084","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0080.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0080_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0080_preview.png","price":"yes","id":"80"},{"title":"100KL085","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"228"},{"title":"100KL086","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0050.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0050_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0050_preview.png","price":"yes","id":"50"},{"title":"100KL087","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0081.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0081_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0081_preview.png","price":"yes","id":"81"},{"title":"100KL088","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0082.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0082_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0082_preview.png","price":"yes","id":"82"},{"title":"100KL089","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0083.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0083_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0083_preview.png","price":"yes","id":"83"},{"title":"100KL090","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0084.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0084_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0084_preview.png","price":"yes","id":"84"},{"title":"100KL091","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"229"},{"title":"100KL092","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"230"},{"title":"100KL093","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"231"},{"title":"100KL094","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"232"},{"title":"100KL095","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0095.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0095_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0095_preview.png","price":"yes","id":"95"},{"title":"100KL096","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0096.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0096_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0096_preview.png","price":"yes","id":"96"},{"title":"100KL097","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0093.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0093_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0093_preview.png","price":"yes","id":"93"},{"title":"100KL098","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0094.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0094_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0094_preview.png","price":"yes","id":"94"},{"title":"100KL099","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0097.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0097_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0097_preview.png","price":"yes","id":"97"},{"title":"100KL100","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0098.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0098_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0098_preview.png","price":"yes","id":"98"},{"title":"100KL101","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"233"},{"title":"100KL102","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"234"},{"title":"100KL103","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"235"},{"title":"100KL104","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"236"},{"title":"100KL105","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"237"},{"title":"100KL106","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"238"},{"title":"100KL107","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"239"},{"title":"100KL108","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"240"},{"title":"100KL109","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"241"},{"title":"100KL110","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"242"},{"title":"100KL111","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"243"},{"title":"100KL112","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"244"},{"title":"100KL113","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"245"},{"title":"100KL114","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0069.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0069_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0069_preview.png","price":"yes","id":"69"},{"title":"100KL115","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0077.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0077_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0077_preview.png","price":"yes","id":"77"},{"title":"100KL116","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0075.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0075_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0075_preview.png","price":"yes","id":"75"},{"title":"100KL117","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0074.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0074_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0074_preview.png","price":"yes","id":"74"},{"title":"100KL118","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0071.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0071_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0071_preview.png","price":"yes","id":"71"},{"title":"100KL119","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0072.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0072_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0072_preview.png","price":"yes","id":"72"},{"title":"100KL120","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0073.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0073_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0073_preview.png","price":"yes","id":"73"},{"title":"100KL121","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0076.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0076_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0076_preview.png","price":"yes","id":"76"},{"title":"100KL122","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0078.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0078_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0078_preview.png","price":"yes","id":"78"},{"title":"100KL123","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0070.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0070_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0070_preview.png","price":"yes","id":"70"},{"title":"100KL124","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0103.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0103_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0103_preview.png","price":"yes","id":"103"},{"title":"100KL125","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0101.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0101_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0101_preview.png","price":"yes","id":"101"},{"title":"100KL126","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0102.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0102_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0102_preview.png","price":"yes","id":"102"},{"title":"100KL127","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"246"},{"title":"100KL128","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0064.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0064_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0064_preview.png","price":"yes","id":"64"},{"title":"100KL129","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0038_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0038_preview.png","price":"yes","id":"247"},{"title":"100KL130","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0063.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0063_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0063_preview.png","price":"yes","id":"63"},{"title":"100KL131","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0099.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0099_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0099_preview.png","price":"yes","id":"99"},{"title":"100KL132","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0100.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0100_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0100_preview.png","price":"yes","id":"100"},{"title":"100KL133","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0105.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0105_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0105_preview.png","price":"yes","id":"105"},{"title":"100KL134","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"106"},{"title":"100KL135","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"248"},{"title":"100KL136","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"249"},{"title":"100KL137","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"250"},{"title":"100KL138","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"251"},{"title":"100KL139","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"252"},{"title":"100KL140","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"253"},{"title":"100KL141","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"254"},{"title":"100KL142","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"255"},{"title":"100KL143","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"256"},{"title":"100KL144","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"257"},{"title":"100KL145","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"258"},{"title":"100KL146","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"259"},{"title":"100KL147","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"260"},{"title":"100KL148","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"261"},{"title":"100KL149","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"262"},{"title":"100KL150","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"263"},{"title":"100KL151","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"264"},{"title":"100KL152","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"265"},{"title":"100KL153","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"266"},{"title":"100KL154","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"267"},{"title":"100KL155","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"268"},{"title":"100KL156","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"269"},{"title":"100KL241","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"270"},{"title":"100KL242","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"271"},{"title":"100KL243","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"272"},{"title":"100KL244","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"273"},{"title":"100KL245","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"274"},{"title":"100KL246","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"275"},{"title":"100KL247","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"276"},{"title":"100KL248","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"277"},{"title":"100KL249","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"278"},{"title":"100KL250","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"279"},{"title":"100KL251","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"280"},{"title":"100KL252","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"281"},{"title":"100KL253","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"282"},{"title":"100KL254","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"283"},{"title":"100KL255","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"284"},{"title":"100KL256","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"285"},{"title":"100KL257","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"286"},{"title":"100KL258","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"287"},{"title":"100KL259","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"288"},{"title":"100KL260","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"289"},{"title":"100KL261","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"290"},{"title":"100KL262","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"291"},{"title":"100KL263","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/0106_preview.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/s0106_preview.png","price":"yes","id":"292"},{"title":"BM001-2","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-2.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-2.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-2.png","price":"yes","id":"119"},{"title":"BM001-3","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-3.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-3.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-3.png","price":"yes","id":"120"},{"title":"BM001-4","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-4.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-4.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-4.png","price":"yes","id":"121"},{"title":"BM001-5","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-5.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-5.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-5.png","price":"yes","id":"122"},{"title":"BM001-6","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-6.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-6.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-6.png","price":"yes","id":"123"},{"title":"BM001-7","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-7.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-7.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-7.png","price":"yes","id":"124"},{"title":"BM001-8","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-8.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-8.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-8.png","price":"yes","id":"125"},{"title":"BM001-9","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-9.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-9.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-9.png","price":"yes","id":"126"},{"title":"BM001-10","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-10.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-10.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-10.png","price":"yes","id":"127"},{"title":"BM001-11","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-11.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-11.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-11.png","price":"yes","id":"128"},{"title":"BM001-12","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-12.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-12.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-12.png","price":"yes","id":"129"},{"title":"BM001-13","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-13.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-13.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-13.png","price":"yes","id":"130"},{"title":"BM001-14","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-14.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-14.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-14.png","price":"yes","id":"131"},{"title":"BM001-15","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-15.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-15.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-15.png","price":"yes","id":"132"},{"title":"BM001-16","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-16.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-16.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-16.png","price":"yes","id":"133"},{"title":"BM001-19","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-19.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-19.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-19.png","price":"yes","id":"134"},{"title":"BM001-18","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-18.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-18.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-18.png","price":"yes","id":"135"},{"title":"BM001-1","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-1.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-1.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-1.png","price":"yes","id":"136"},{"title":"BM001-17","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/preview/BM001-17.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/neck/BM001-17.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM001/sign/BM001-17.png","price":"yes","id":"137"},{"title":"BM002-1","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/preview/BM002-1.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/neck/BM002-1.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/sign/BM002-1.png","price":"yes","id":"138"},{"title":"BM002-3","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/preview/BM002-3.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/neck/BM002-3.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/sign/BM002-3.png","price":"yes","id":"139"},{"title":"BM002-4","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/preview/BM002-4.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/neck/BM002-4.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/sign/BM002-4.png","price":"yes","id":"140"},{"title":"BM002-5","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/preview/BM002-5.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/neck/BM002-5.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/sign/BM002-5.png","price":"yes","id":"141"},{"title":"BM002-2","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/preview/BM002-2.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/neck/BM002-2.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/sign/BM002-2.png","price":"yes","id":"142"},{"title":"BM002-6","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/preview/BM002-6.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/neck/BM002-6.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM002/sign/BM002-6.png","price":"yes","id":"143"},{"title":"BM003-1","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-1.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-1.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-1.png","price":"yes","id":"144"},{"title":"BM003-2","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-2.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-2.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-2.png","price":"yes","id":"145"},{"title":"BM003-3","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-3.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-3.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-3.png","price":"yes","id":"146"},{"title":"BM003-4","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-4.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-4.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-4.png","price":"yes","id":"147"},{"title":"BM003-5","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-5.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-5.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-5.png","price":"yes","id":"148"},{"title":"BM003-6","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-6.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-6.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-6.png","price":"yes","id":"149"},{"title":"BM003-9","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-9.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-9.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-9.png","price":"yes","id":"150"},{"title":"BM003-11","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-11.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-11.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-11.png","price":"yes","id":"151"},{"title":"BM003-7","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-7.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-7.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-7.png","price":"yes","id":"152"},{"title":"BM003-8","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-8.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-8.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-8.png","price":"yes","id":"153"},{"title":"BM003-10","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/preview/BM003-10.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/neck/BM003-10.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM003/sign/BM003-10.png","price":"yes","id":"154"},{"title":"BM004-1","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-1.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-1.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-1.png","price":"yes","id":"155"},{"title":"BM004-2","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-2.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-2.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-2.png","price":"yes","id":"156"},{"title":"BM004-3","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-3.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-3.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-3.png","price":"yes","id":"157"},{"title":"BM004-4","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-4.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-4.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-4.png","price":"yes","id":"158"},{"title":"BM004-5","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-5.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-5.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-5.png","price":"yes","id":"159"},{"title":"BM004-6","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-6.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-6.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-6.png","price":"yes","id":"160"},{"title":"BM004-7","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-7.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-7.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-7.png","price":"yes","id":"161"},{"title":"BM004-8","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-8.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-8.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-8.png","price":"yes","id":"162"},{"title":"BM004-9","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-9.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-9.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-9.png","price":"yes","id":"163"},{"title":"BM004-10","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-10.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-10.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-10.png","price":"yes","id":"164"},{"title":"BM004-11","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-11.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-11.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-11.png","price":"yes","id":"165"},{"title":"BM004-12","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/preview/BM004-12.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/neck/BM004-12.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/BM004/sign/BM004-12.png","price":"yes","id":"166"},{"title":"GERLN-1","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin01.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/01.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-01.png","price":"yes","id":"167"},{"title":"GERLN-2","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin02.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/02.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-02.png","price":"yes","id":"168"},{"title":"GERLN-3","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin03.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/03.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-03.png","price":"yes","id":"169"},{"title":"GERLN-4","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin04.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/04.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-04.png","price":"yes","id":"170"},{"title":"GERLN-5","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin05.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/05.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-05.png","price":"yes","id":"171"},{"title":"GERLN-6","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin06.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/06.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-06.png","price":"yes","id":"172"},{"title":"GERLN-7","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin07.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/07.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-07.png","price":"yes","id":"173"},{"title":"GERLN-8","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin08.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/08.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-08.png","price":"yes","id":"174"},{"title":"GERLN-9","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin09.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/09.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-09.png","price":"yes","id":"175"},{"title":"GERLN-10","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin10.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/10.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-10.png","price":"yes","id":"176"},{"title":"GERLN-11","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin11.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/11.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-11.png","price":"yes","id":"177"},{"title":"GERLN-12","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin12.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/12.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-12.png","price":"yes","id":"178"},{"title":"GERLN-13","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin13.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/13.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-13.png","price":"yes","id":"179"},{"title":"GERLN-14","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin14.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/14.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-14.png","price":"yes","id":"180"},{"title":"GERLN-15","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin15.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/15.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-15.png","price":"yes","id":"181"},{"title":"GERLN-16","image":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/preview/Gerlin16.png","preview":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/neck/16.png","previews":"http://shirt-tailor.net/thepos/appimg/liningFabric/gerlin/sign/Gerlin-16.png","price":"yes","id":"182"}],"timestamp":0}';

    localStorage['vestButtonsDS']	    = '{"store":[{"title":"DEFAULT","image":"http://shirt-tailor.net/thepos/appimg/buttons/default.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_5.png","id":"0"},{"title":"White","image":"http://shirt-tailor.net/thepos/appimg/buttons/white.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_5.png","id":"1"},{"title":"Sky Blue","image":"http://shirt-tailor.net/thepos/appimg/buttons/sky-blue.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_5.png","id":"2"},{"title":"Light Khaki","image":"http://shirt-tailor.net/thepos/appimg/buttons/light-khaki.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_5.png","id":"3"},{"title":"Brown","image":"http://shirt-tailor.net/thepos/appimg/buttons/brown.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_5.png","id":"4"},{"title":"Marron","image":"http://shirt-tailor.net/thepos/appimg/buttons/marron.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_5.png","id":"5"},{"title":"Purple","image":"http://shirt-tailor.net/thepos/appimg/buttons/purple.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_5.png","id":"6"},{"title":"Navy","image":"http://shirt-tailor.net/thepos/appimg/buttons/navy.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_5.png","id":"7"},{"title":"Black","image":"http://shirt-tailor.net/thepos/appimg/buttons/black.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_5.png","id":"8"},{"title":"Pearl White","image":"http://shirt-tailor.net/thepos/appimg/buttons/pearl-white.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_5.png","id":"9"},{"title":"Deep Brown","image":"http://shirt-tailor.net/thepos/appimg/buttons/deep-brown.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_5.png","id":"10"},{"title":"Dull Silver","image":"http://shirt-tailor.net/thepos/appimg/buttons/dull-silver.png","preview1":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_1.png","preview2":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_2.png","preview3":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_3.png","preview4":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_4.png","preview5":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_5.png","id":"11"}],"timestamp":0}';
    localStorage['vestThreadsDS']       = '{"store":[{"title":"DEFAULT","image":"http://shirt-tailor.net/thepos/appimg/color_thread/default.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","id":"0"},{"title":"White","image":"http://shirt-tailor.net/thepos/appimg/color_thread/white.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/white.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png","id":"white"},{"title":"Cream","image":"http://shirt-tailor.net/thepos/appimg/color_thread/cream.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png","id":"cream"},{"title":"Yellow","image":"http://shirt-tailor.net/thepos/appimg/color_thread/yellow.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/yellow.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-5.png","id":"yellow"},{"title":"Light Green","image":"http://shirt-tailor.net/thepos/appimg/color_thread/green.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/green.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png","id":"green"},{"title":"Light Blue","image":"http://shirt-tailor.net/thepos/appimg/color_thread/light_blue.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png","id":"light-blue"},{"title":"Navy","image":"http://shirt-tailor.net/thepos/appimg/color_thread/blue.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png","id":"blue"},{"title":"Purple","image":"http://shirt-tailor.net/thepos/appimg/color_thread/purple.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png","id":"purple"},{"title":"Pink","image":"http://shirt-tailor.net/thepos/appimg/color_thread/pink.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/pink.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-5.png","id":"pink"},{"title":"Light Pink","image":"http://shirt-tailor.net/thepos/appimg/color_thread/light_pink.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png","id":"light-pink"},{"title":"Red","image":"http://shirt-tailor.net/thepos/appimg/color_thread/red.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/red.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-red-5.png","id":"red"},{"title":"Orange","image":"http://shirt-tailor.net/thepos/appimg/color_thread/orange.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png","id":"orange"},{"title":"Bronze","image":"http://shirt-tailor.net/thepos/appimg/color_thread/gold.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png","id":"bronze"},{"title":"Charcoal","image":"http://shirt-tailor.net/thepos/appimg/color_thread/grey.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/charcoal.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-5.png","id":"charcoal"},{"title":"Black","image":"http://shirt-tailor.net/thepos/appimg/color_thread/black.png","lapel1":"http://shirt-tailor.net/thepos/appimg/buttonhole/black.png","preview1":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png","id":"black"}],"timestamp":0}';
    localStorage['buttonholethreadDS']  = '{"store":[{"title":"DEFAULT","image":"http://shirt-tailor.net/thepos/appimg/button_thread/default.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","id":"0"},{"title":"White","image":"http://shirt-tailor.net/thepos/appimg/button_thread/white.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png","id":"white"},{"title":"Cream","image":"http://shirt-tailor.net/thepos/appimg/button_thread/cream.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png","id":"cream"},{"title":"Yellow","image":"http://shirt-tailor.net/thepos/appimg/button_thread/yellow.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-5.png","id":"yellow"},{"title":"Light Green","image":"http://shirt-tailor.net/thepos/appimg/button_thread/green.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png","id":"green"},{"title":"Light Blue","image":"http://shirt-tailor.net/thepos/appimg/button_thread/light_blue.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png","id":"light-blue"},{"title":"Navy","image":"http://shirt-tailor.net/thepos/appimg/button_thread/blue.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png","id":"blue"},{"title":"Purple","image":"http://shirt-tailor.net/thepos/appimg/button_thread/purple.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png","id":"purple"},{"title":"Pink","image":"http://shirt-tailor.net/thepos/appimg/button_thread/pink.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-5.png","id":"pink"},{"title":"Light Pink","image":"http://shirt-tailor.net/thepos/appimg/button_thread/light_pink.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png","id":"light-pink"},{"title":"Red","image":"http://shirt-tailor.net/thepos/appimg/button_thread/red.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-red-5.png","id":"red"},{"title":"Orange","image":"http://shirt-tailor.net/thepos/appimg/button_thread/orange.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png","id":"orange"},{"title":"Bronze","image":"http://shirt-tailor.net/thepos/appimg/button_thread/gold.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png","id":"bronze"},{"title":"Charcoal","image":"http://shirt-tailor.net/thepos/appimg/button_thread/grey.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-5.png","id":"charcoal"},{"title":"Black","image":"http://shirt-tailor.net/thepos/appimg/button_thread/black.png","preview1":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png","preview2":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png","preview3":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png","preview4":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png","preview5":"http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png","id":"black"}],"timestamp":0}';



  //  localStorage['customersDS'] = '{"store":[{"customer_first_name":"Derp","customer_last_name":"Herp","customer_occupation":"ninja","customer_company_name":"","customer_address1":"myaddress","customer_address2":"","customer_country":"1","customer_state":"4","customer_city":"10","customer_postal_code":"12345","customer_mobile_phone":"123456789","customer_landline_phone":"987654321","customer_email":"mail@asdf.com","customer_refered_by":"","customer_DOB":"1908-2-3","customer_gender":"0","customer_email_subscription":1,"id":0},{"customer_first_name":"Yorikas","customer_last_name":"Athanassov","customer_occupation":"nianiania","customer_company_name":"","customer_address1":"myaddress 34","customer_address2":"","customer_country":"6","customer_postal_code":"","customer_mobile_phone":"123456789","customer_state":"4","customer_landline_phone":"987654352","customer_email":"mail@asdf.com","customer_refered_by":"","customer_DOB":"1965-5-7","customer_gender":"1","customer_email_subscription":1,"id":1}],"timestamp":1358342381}';
  
//    localStorage['measurementsDS'] = '{"store": [], "timestamp":0}';
// OLD   localStorage['measurementsDS'] = '{"store": {"pantWaist":"85","pantHips":"85","thigh":"85","cuffs":"85","pantLength":"85","crotch":"85","zipper":"85","jacketChest":"85","jacketStomach":"85","jacketHips":"85","jacketFront":"85","jacketBack":"85", "jacketfullShoulder":"85","jacketsleeveL":"85","jacketsleeveR":"85","jacketNeck":"85","jacketBicep":"85","jacketWrist":"85","jacketWristWatch":"85","jacketforeArms":"85","jacketLength":"85","jacketLapel":"85", "jacketZeropoint":"85","jacketZeropointfront":"85","jacketZeropointback":"85","jacketHalfzero":"85","jacketHalfstomach":"85","shirtChest":"85","shirtStomach":"85","shirtfullshoulder":"85","shirtsleeveL":"85", "shirtsleeveR":"85","shirtNeck":"85","shirtFront":"85","shirtBack":"85","shirtLength":"85","shirtBicep":"85","shirtHips":"85","shirtWrist":"85","shirtWristwatch":"85","vestHips":"85","vestLength":"85","vestChest":"85", "vestStomach":"85","vestfullShoulder":"85","vestComments":"85","suitComments":"85","jacketComments":"85","pantsComments":"85","shirtComments":"85","overcoatComments":"85"}, "timestamp":0}';
//	  localStorage['measurementsDS'] = '{"store": {"jacketChest":"85","jacketStomach":"85","jacketHips":"85","jacketfullShoulder":"85","jacketsleeveL":"85","jacketsleeveR":"85","jacketFront":"85","jacketBack":"85","jacketBicep":"85","jacketforeArms":"85","jacketWrist":"85","jacketLength":"85", "jacketLapel":"85","jacketNeck":"85","jacketZeropoint":"85","jacketZeropointfront":"85","jacketZeropointback":"85","jacketComments":"85","pantWaist":"85","pantHips":"85","thigh":"85","cuffs":"85", "pantFrontCrotch":"85","pantInseam":"85","pantLength":"85","crotch":"85","zipper":"85","pantsComments":"85","shirtChest":"85","shirtStomach":"85","shirtHips":"85", "shirtfullshoulder":"85","shirtsleeveL":"85","shirtsleeveR":"85","shirtNeck":"85","shirtFront":"85","shirtBack":"85","shirtLength":"85","shirtBicep":"85","shirtWrist":"85","shirtWristwatch":"85","shirtComments":"85","vestChest":"85", "vestStomach":"85","vestfullShoulder":"85","vestHips":"85","vestLength":"85","vestVLength":"85","vestHalfShoulderWidth":"85","vestComments":"85","overcoatComments":"85"}, "timestamp":0}';

    localStorage['suitJacketStructureDS'] = '{"store":[{"title":"Select Button","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/one_button.png","id":"0"},{"title":"One Button","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/one_button.png","id":"One_Button"},{"title":"Two Button","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/two_but.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/two_but_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/two_buttons.png","id":"Two_Button"},{"title":"Three Button","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/three_but.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/three_but_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/three_buttons.png","id":"Three_Button"},{"title":"Four Button","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/four_but.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/four_but_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/four_buttons.png","id":"Four_Button"},{"title":"Double Breasted","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/four_double.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/four_double_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/double.png","id":"Double_Breasted"},{"title":"Double Breasted (one to close)","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/four_open_double.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/four_open_double_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/double_one_to_close.png","id":"Double_Breasted_One_to_close"},{"title":"Double breasted (six buttons)","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/six_double.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/six_double_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/double_two_to_close.png","id":"Double_Breasted_six_Buttons"},{"title":"Custom","image":"http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but_preview.png","buttoni":"http://shirt-tailor.net/thepos/appimg/jacket/trimming/one_button.png","id":"123"}],"timestamp":0}';
    localStorage['suitLapelStyleDS']  = '{"store":[{"title":"Select Lapel","image":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel_preview.png","previewone":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/notch_lappel_preview.png","previewtwo":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/notch_lappel_preview.png","previewthree":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/notch_lappel_preview.png","previewfour":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/notch_lappel_preview.png","dpreview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/notch_lappel_preview.png","id":"0"},{"title":"Notch Lapel","image":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel_preview.png","previewone":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/notch_lappel_preview.png","previewtwo":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/notch_lappel_preview.png","previewthree":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/notch_lappel_preview.png","previewfour":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/notch_lappel_preview.png","dpreview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/notch_lappel_preview.png","id":"1"},{"title":"Peak Lapel","image":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/peak_lappel.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/peak_lappel_preview.png","previewone":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/peak_lappel_preview.png","previewtwo":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/peak_lappel_preview.png","previewthree":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/peak_lappel_preview.png","previewfour":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/peak_lappel_preview.png","dpreview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/peak_lappel_preview.png","id":"2"},{"title":"Shawl Lapel","image":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/shawl_lappel.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/shawl_lappel_preview.png","previewone":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/shawl_lappel_preview.png","previewtwo":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/shawl_lappel_preview.png","previewthree":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/shawl_lappel_preview.png","previewfour":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/shawl_lappel_preview.png","dpreview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/shawl_lappel_preview.png","id":"3"},{"title":"Custom Lapel","image":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel_preview.png","previewone":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/notch_lappel_preview.png","previewtwo":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/notch_lappel_preview.png","previewthree":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/notch_lappel_preview.png","previewfour":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/notch_lappel_preview.png","dpreview":"http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/notch_lappel_preview.png","id":"123"}],"timestamp":0}';
    localStorage['suitBottomStyleDS'] = '{"store":[{"title":"Select Bottom","image":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","id":"0"},{"title":"Curved Bottom","image":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","id":"Curved"},{"title":"Straight Bottom","image":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/straight_bottom.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/straight_bottom_preview.png","id":"Straight"},{"title":"Custom Bottom","image":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","id":"123"}],"timestamp":0}';
    localStorage['suitVentStyleDS']   = '{"store":[{"title":"Select Vent Style","image":"http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent_preview.png","id":"0"},{"title":"No Vent","image":"http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent_preview.png","id":"1"},{"title":"One Vent","image":"http://shirt-tailor.net/thepos/appimg/jacket/vent/one_vent.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/vent/one_vent_preview.png","id":"2"},{"title":"Two Vent","image":"http://shirt-tailor.net/thepos/appimg/jacket/vent/two_vent.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/vent/two_vent_preview.png","id":"3"},{"title":"Custom Vent","image":"http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent_preview.png","id":"123"}],"timestamp":0}';
//    localStorage['suitPocketStyleDS'] = '{"store":[{"title":"JP1","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_preview.png","id":"jp1"},{"title":"JP2","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_preview.png","id":"jp2"},{"title":"JP3","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_preview.png","id":"jp3"},{"title":"JP5","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_preview.png","id":"jp5"},{"title":"JP6","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_preview.png","id":"jp6"},{"title":"JP7","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_preview.png","id":"jp7"},{"title":"JP8","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp8.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp8_preview.png","id":"jp8"},{"title":"JP9","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_preview.png","id":"jp9"}],"timestamp":0}';
	localStorage['suitPocketStyleDS'] = '{"store":[{"title":"NO","titles":"Select","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_topstitch.png","id":"0"},{"title":"JP5","titles":"Classic","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp5.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp5_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_ticket_topstitch.png","id":"jp5"},{"title":"JP1","titles":"Slightly Slanted","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_topstitch.png","id":"jp1"},{"title":"JP7","titles":"Welt Pocket","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp7.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp7_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","id":"jp7"},{"title":"JP2","titles":"Batman","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp2.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp2_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_ticket_topstitch.png","id":"jp2"},{"title":"JP3","titles":"Front Curved","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp3.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp3_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_ticket_topstitch.png","id":"jp3"},{"title":"JP6","titles":"Back Curved","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp6.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp6_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_trimmed.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_ticket_topstitch.png","id":"jp6"},{"title":"JP9","titles":"Patch Pocket","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_preview.png","ticket_image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp9.png","ticket_preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp9_preview.png","trimmedpocket":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","trimmedticket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_ticket_trimmed.png","topstitchpocket":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_topstitch.png","topstitchticketpocket":"http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png","id":"jp9"}],"timestamp":0}';
//    localStorage['tickPocketStyleDS'] = '{"store":[{"title":"Undefined","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"0"},{"title":"JP1","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1_preview.png","id":"jp1"},{"title":"JP7","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp7.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp7_preview.png","id":"jp7"},{"title":"JP5","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp5.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp5_preview.png","id":"jp5"},{"title":"JP6","image":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp6.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp6_preview.png","id":"jp6"}],"timestamp":0}';
    localStorage['suitButtonStyleDS'] = '{"store":[{"title":"Three buttons kissing","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_kiss.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_kiss-preview.png","id":"1"},{"title":"Three buttons standing","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_stand.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_stand-preview.png","id":"2"},{"title":"Three buttons working","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_work.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_work-preview.png","id":"3"},{"title":"Four buttons kissing","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_kiss.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_kiss-preview.png","id":"4"},{"title":"Four buttons standing","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_sta.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_sta-preview.png","id":"5"},{"title":"Four buttons working","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_work.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_work-preview.png","id":"6"},{"title":"Five buttons kissing","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_kiss.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_kiss-preview.png","id":"7"},{"title":"Five buttons standing","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_sta.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_sta-preview.png","id":"8"},{"title":"Five buttons working","image":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_work.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_work-preview.png","id":"9"}],"timestamp":0}';
	localStorage['JacketSleeveButtonNumberDS'] = '{"store":[{"title":"One button","id":"1","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/one_button.png","previews":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/one_button_covered.png"},{"title":"Two buttons","id":"2","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/two_buttons.png","previews":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/two_buttons_covered.png"},{"title":"Three buttons","id":"3","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/three_buttons.png","previews":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/three_buttons_covered.png"},{"title":"Four buttons","id":"4","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/four_buttons.png","previews":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/four_buttons_covered.png"},{"title":"Five buttons","id":"5","preview":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/five_buttons.png","previews":"http://shirt-tailor.net/thepos/appimg/jacket/buttons/five_buttons_covered.png"}],"timestamp":0}';
//	localStorage['JacketBreastPocketStyleDS'] = '{"store":[{"title":"Standard","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic.png","preview_straight":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic-preview-left.png","preview_angled":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight-preview-left.png","id":1},{"title":"Patch","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square.png","preview_straight":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview-left.png","preview_angled":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap-preview-left.png","id":2},{"title":"Welt","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle.png","preview_straight":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-preview-left.png","preview_angled":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview-left.png","id":3}],"timestamp":0}';
	localStorage['JacketBreastPocketStyleDS'] = '{"store":[{"title":"No Breastpocket","image":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":0},{"title":"Standard Straight","image":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default_pr.png","id":1},{"title":"Standard Angled","image":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default_angled.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default_angled_pr.png","id":2},{"title":"Patch Pocket","image":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/patch_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/patch_pocket_preview.png","id":3}],"timestamp":0}';
//	,{"title":"Patch","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview-left.png","id":3},{"title":"Welt Straight","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-left.png","id":4},{"title":"Welt Angled","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview-left.png","id":5}
    localStorage['pantBackStyleDS']   = '{"store":[{"title":"Undefined","image":"http://shirt-tailor.net/thepos/appimg/pant/back/3_b_kiss.png","id":"0"},{"title":"curv_flap_wi_bu_pantt.png ","image":"http://shirt-tailor.net/thepos/appimg/pant/back/curv_flap_wi_bu_pantt.png","id":"1"},{"title":"lsolsing_back.png","image":"http://shirt-tailor.net/thepos/appimg/pant/back/lsolsing_back.png","id":"2"},{"title":"nononopocke.png","image":"http://shirt-tailor.net/thepos/appimg/pant/back/nononopocke.png","id":"3"},{"title":"douddl_open_pant_ba.png","image":"http://shirt-tailor.net/thepos/appimg/pant/back/douddl_open_pant_ba.png","id":"4"},{"title":"mod_fl_wif_bu_pantt.png","image":"http://shirt-tailor.net/thepos/appimg/pant/back/mod_fl_wif_bu_pantt.png","id":"5"},{"title":"squ_ba_wi_bu_po_pan.png","image":"http://shirt-tailor.net/thepos/appimg/pant/back/squ_ba_wi_bu_po_pan.png","id":"6"}],"timestamp":0}';
//    localStorage['beltLoopStyleDS']    = '{"store":[{"title":"No Belt Loop","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/nonobeelttloop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/nonobeelttloop_preview.png","id":"No_Beltloop"},{"title":"Single","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/sinnggle_pant_llop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/sinnggle_pant_llop_preview.png","id":"3"},{"title":"Double","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/dobblooe_pant_loup.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/dobblooe_pant_loup_preview.png","id":"Double"},{"title":"Button Side Adjusters","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/but_adj_belt_oop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/but_adj_belt_oop_preview.png","id":"5"},{"title":"Designer","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/mode_lo_pannm.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/mode_lo_pannm_preview.png","id":"2"},{"title":"Buckle Side Adjusters","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/waist_lop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/waist_lop_preview.png","id":"Buckle_Side"}],"timestamp":0}';
	localStorage['beltLoopStyleDS']   = '{"store":[{"title":"Select Belt Loop","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/nonobeelttloop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"0"},{"title":"Single Straight","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/sinnggle_pant_llop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/sinnggle_pant_llop_preview.png","id":"Single_Straight"},{"title":"Double Straight","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/dobblooe_pant_loup.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/dobblooe_pant_loup_preview.png","id":"Double_Straight"},{"title":"Double Angled","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/double_angle.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/double_angle_preview.png","id":"Double_Angle"},{"title":"Criss Cross","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/cross.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/cross_preview.png","id":"Criss_Cross"},{"title":"Designer","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/mode_lo_pannm.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/mode_lo_pannm_preview.png","id":"Designer"},{"title":"Button Side Adjusters","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/but_adj_belt_oop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/but_adj_belt_oop_preview.png","id":"Button_Side_Adjuster"},{"title":"Buckle Side Adjusters","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/waist_lop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/waist_lop_preview.png","id":"Buckle_Side"},{"title":"No Belt Loop","image":"http://shirt-tailor.net/thepos/appimg/pant/belt_loop/nonobeelttloop.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"No_Beltloop"}],"timestamp":0}';
	


    localStorage['cuffsStyleDS']      = '{"store":[{"title":"Undefined","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/3_b_kiss.png","id":"0"},{"title":"cuffff_pant_po.png","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/cuffff_pant_po.png","id":"1"},{"title":"fold_ov_pant.png","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/fold_ov_pant.png","id":"2"},{"title":"singgl_pant_cuf_tab.png","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/singgl_pant_cuf_tab.png","id":"3"},{"title":"doub_pant_cuf_tab.png","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/doub_pant_cuf_tab.png","id":"4"},{"title":"reggulla_nma.png","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/reggulla_nma.png","id":"5"},{"title":"squ_ba_wi_bu_po_pan.png","image":"http://shirt-tailor.net/thepos/appimg/pant/cuffs/squ_ba_wi_bu_po_pan.png","id":"6"}],"timestamp":0}';
    localStorage['pantFitDS'] = '{"store":[{"title":"Select Pant Style","image":"http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan_preview.png","id":"0"},{"title":"Normal Straight","image":"http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan_preview.png","id":"Normal_Straight"},{"title":"Boot Flare","image":"http://shirt-tailor.net/thepos/appimg/pant/fit/boot_fl_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/fit/boot_fl_pan_preview.png","id":"Boot_Flare"},{"title":"Narrow Slim","image":"http://shirt-tailor.net/thepos/appimg/pant/fit/narr_po_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/fit/narr_po_pant_preview.png","id":"Narrow_Slim"}],"timestamp":0}';

//    localStorage['pantPleatsDS']     = '{"store":[{"title":"No Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/no_plll_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/no_plll_pant_preview.png","id":"1_Pleats"},{"title":"Single Pleat","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/singgle_pl_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/singgle_pl_pant_preview.png","id":"Single_Pleat"},{"title":"Double Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/dobb_ple_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/dobb_ple_pan_preview.png","id":"Double_Pleats"},{"title":"Triple Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/trip_ple_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/trip_ple_pan_preview.png","id":"Triple_Pleats"},{"title":"Box Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/boxx_pl_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/boxx_pl_pan_preview.png","id":"Box_Pleats"},{"title":"Scissors Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/scisso_ple_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/scisso_ple_pant_preview.png","id":"Sciccors_Pleats"}],"timestamp":0}';
    localStorage['pantPleatsDS']     = '{"store":[{"title":"Select Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/no_plll_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"0"},{"title":"No Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/no_plll_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"4"},{"title":"Single Pleat","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/singgle_pl_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/singgle_pl_pant_preview.png","id":"1"},{"title":"Double Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/dobb_ple_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/dobb_ple_pan_preview.png","id":"2"},{"title":"Triple Pleats","image":"http://shirt-tailor.net/thepos/appimg/pant/pleats/trip_ple_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pleats/trip_ple_pan_preview.png","id":"3"}],"timestamp":0}';

//    localStorage['pantPocketsDS']     = '{"store":[{"title":"Slanted","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/slanted_pan_pocck.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/slanted_pan_pocck_preview.png","id":"TP1"},{"title":"No Pocket","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/no_pock_pann_t.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"No_Pocket"},{"title":"Slanted Welt","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/stan_welt_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/stan_welt_pant_preview.png","id":"TP2"},{"title":"Straight Welt","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/welt_stri_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/welt_stri_pan_preview.png","id":"TP3"},{"title":"Modern Curved","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/mod_cir_pant_p.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/mod_cir_pant_p_preview.png","id":"TP5"},{"title":"Jeans","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc_preview.png","id":"TP6"}],"timestamp":0}';
	localStorage['pantPocketsDS']     = '{"store":[{"title":"Select Pocket","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/no_pock_pann_t.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"0"},{"title":"No Pocket","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/no_pock_pann_t.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"No_Pocket"},{"title":"Slanted","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/slanted_pan_pocck.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/slanted_pan_pocck_preview.png","id":"TP1"},{"title":"Slanted Welt","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/stan_welt_pant.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/stan_welt_pant_preview.png","id":"TP2"},{"title":"Straight Welt","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/welt_stri_pan.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/welt_stri_pan_preview.png","id":"TP3"},{"title":"Modern Curved","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/mod_cir_pant_p.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/mod_cir_pant_p_preview.png","id":"TP4"},{"title":"Jeans","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc_preview.png","id":"Jeans"},{"title":"Custom Pocket","image":"http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc_preview.png","id":"123"}],"timestamp":0}';
	 
//    localStorage['bpantPocketsDS'] = '{"store":[{"title":"No Pocket Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket.png","id":"no_pocket"},{"title":"Single Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket.png","id":"single"},{"title":"Double Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket.png","id":"double"},{"title":"Modern Flap w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button.png","id":"modern_flap"},{"title":"Square Flap w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap.png","id":"flap"}],"timestamp":0}';
//    localStorage['bpantPocketsDS'] = '{"store":[{"title":"No Pocket Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket_preview.png","id":"no_pocket"},{"title":"Single Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_previewt.png","id":"single"},{"title":"Double Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview.png","id":"double"},{"title":"Modern Flap w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview.png","id":"modern_flap"},{"title":"Square Flap w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview.png","id":"flap"},{"title":"Modern Flap no Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_no_button.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_no_button_preview.png","id":"modern_flap_no_button"},{"title":"Square Flap no Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview.png","id":"flap_no_button"}],"timestamp":0}';
	localStorage['bpantPocketsDS'] = '{"store":[{"title":"Select Pocket Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/none.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"0"},{"title":"No Pocket Opening","image":"http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/none.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/none.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/none.png","id":"no_pocket"},{"title":"Double w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/double_button_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/double_button_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/double_button_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/double_button_preview-left.png","id":"double_buttons"},{"title":"Double","image":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview-left.png","id":"double"},{"title":"Single w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/single_buttons.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/single_button_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/single_button_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/single_button_preview-left.png","id":"single_buttons"},{"title":"Single","image":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_preview-left.png","id":"single"},{"title":"Modern Flap w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview-left.png","id":"modern_flap"},{"title":"Square Flap w. Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview-left.png","id":"flap"},{"title":"Modern Flap no Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_no_button.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/modern_preview-left.png","id":"modern_flap_no_button"},{"title":"Square Flap no Button","image":"http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview.png","previewright":"http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview-right.png","previewleft":"http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview-left.png","id":"flap_no_button"}],"timestamp":0}';

	localStorage['shirtButtonsDS'] = '{"store":[{"title":"DEFAULT","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/default.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/white-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_white-preview.png","id":"0"},{"title":"White","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/white.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/white-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_white-preview.png","id":"1"},{"title":"Cream","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/cream.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/cream-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_cream-preview.png","id":"2"},{"title":"Yellow","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/yellow.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/yellow-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_yellow-preview.png","id":"3"},{"title":"Light Green","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/green.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/green-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_green-preview.png","id":"4"},{"title":"Light Pink","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/light_pink.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/light_pink-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_light_pink-preview.png","id":"5"},{"title":"Pink","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/pink.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/pink-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_pink-preview.png","id":"6"},{"title":"Powder Blue","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/powder-blue.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/power-blue-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_power-blue-preview.png","id":"7"},{"title":"Aqua","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/aqua.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/aqua-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_aqua-preview.png","id":"8"},{"title":"Navy","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/navy.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/navy-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_navy-preview.png","id":"9"},{"title":"Orange","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/orange.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/orange-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_orange-preview.png","id":"10"},{"title":"Red","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/red.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/red-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_red-preview.png","id":"11"},{"title":"Purple","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/purple.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/purple-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_purple-preview.png","id":"12"},{"title":"Charcoal","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/charcoal.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/charcoal-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_charcoal-preview.png","id":"13"},{"title":"Black","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/black.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/black-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_black-preview.png","id":"14"},{"title":"Screw Back Stud","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/stud.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/studs-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_studs-preview.png","id":"15"},{"title":"Cufflink Stud","image":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/stud.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/studs-preview.png","spreview":"http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_studs-preview.png","id":"16"}],"timestamp":0}';
    localStorage['shirtBackStyleDS']   = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat-preview.png","id":0},{"title":"Box Pleat","image":"http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat-preview.png","id":1},{"title":"Plain","image":"http://shirt-tailor.net/thepos/appimg/shirt/back/plain.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png","id":3},{"title":"Side Pleats","image":"http://shirt-tailor.net/thepos/appimg/shirt/back/side_pleats.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/side_pleats-preview.png","id":6},{"title":"Center Pleats","image":"http://shirt-tailor.net/thepos/appimg/shirt/back/center_pleats.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/center_pleats-preview.png","id":2}],"timestamp":0}';
    localStorage['shirtBottomStyleDS'] = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__-preview.png","id":"0"},{"title":"Round TP","image":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/tri_tab.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/tri_tab-preview.png","id":"3"},{"title":"Round","image":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/classi.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/classi-preview.png","id":"4"},{"title":"Straight","image":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__-preview.png","id":"1"},{"title":"Cut Straight","image":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight-preview.png","id":"2"}],"timestamp":0}';
    localStorage['shirtCollarStyleDS'] = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/cut_away_1_button.png","id":0},{"title":"French","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_1_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_1_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/french_one_button.png","id":3},{"title":"Italian","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_1_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_1_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/italian_one_button.png","id":14},{"title":"St-Germain","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_2_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_2_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/french_two_button.png","id":4},{"title":"Button Down","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/button_down.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/button_down-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/button_down.png","id":6},{"title":"Wing Tip","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/tuxedo.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/tuxedo-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/tuxedo.png","id":11},{"title":"Cut Away","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/cut_away_1_button.png","id":12},{"title":"Cut Away Two But.","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_2_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_2_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/cut_away_2_button.png","id":13},{"title":"Italian Two But.","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_2_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_2_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/italian_two_button.png","id":15},{"title":"Hidden Button","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/hidden_button.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/hidden_button-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/hidden-button.png","id":7},{"title":"Tab","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/tab.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/tab-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/tab.png","id":8},{"title":"Batman","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/batman.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/batman-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/batman.png","id":2},{"title":"Round","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/round_collar.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/round_collar-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/round_collar.png","id":5},{"title":"Modern","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/coll_modern.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/collar/coll_modern-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/modern.png","id":9},{"title":"Band","image":"http://shirt-tailor.net/thepos/appimg/shirt/collar/band.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png","previewcontr":"http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png","id":1}],"timestamp":0}';
    localStorage['shirtCuffsStyleDS']  = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/box_pleat.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/box_pleat-preview.png","id":0},{"title":"One Button Angled","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_angled.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_angled-preview.png","id":1},{"title":"One Button Round","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_round.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_round-preview.png","id":2},{"title":"One Button Square","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_square.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_square-preview.png","id":3},{"title":"Two Button Angled","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_angled.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_angled-preview.png","id":4},{"title":"Two Button Round","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_round.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_round-preview.png","id":5},{"title":"Two Button Square","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_square.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_square-preview.png","id":6},{"title":"French Angle","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_cut.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_cut-preview.png","id":7},{"title":"French Round","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_round.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_round-preview.png","id":8},{"title":"French Square","image":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_square.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_square-preview.png","id":9}],"timestamp":0}';
    localStorage['shirtFitStyleDS']    = '{"store":[{"title":"Undefined","image":"http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt-preview.png","id":0},{"title":"fitted_shirt.png","image":"http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt-preview.png","id":1},{"title":"semi_shirt.png","image":"http://shirt-tailor.net/thepos/appimg/shirt/fit/semi_shirt.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/fit/semi_shirt-preview.png","id":2},{"title":"standard_shirt.png","image":"http://shirt-tailor.net/thepos/appimg/shirt/fit/standard_shirt.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/fit/standard_shirt-preview.png","id":3}],"timestamp":0}';
    localStorage['shirtFrontStyleDS']  = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/box.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/front/box-preview.png","id":0},{"title":"Box","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/box.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/front/box-preview.png","id":1},{"title":"Fly","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/hidden.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/front/hidden-preview.png","id":2},{"title":"French","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/single.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png","id":3},{"title":"Pleats","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_pleat.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/front/pleat.png","id":4},{"title":"Full Pleats","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_full_pleat.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/front/full_pleat.png","id":5},{"title":"Angle Pleats","image":"http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_angled_pleat.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/front/angled_pleat.png","id":6}],"timestamp":0}';
//    localStorage['shirtPocketStyleDS'] = '{"store":[{"title":"Undefined","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap-preview.png","id":0},{"title":"No Pocket","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png","id":1},{"title":"Classic Square","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview.png","id":2},{"title":"Classic Angle","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-preview.png","id":3},{"title":"Diamond Straight","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight-preview.png","id":4},{"title":"Round Flap","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap-preview.png","id":5},{"title":"Classic","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic-preview.png","id":6},{"title":"Flap Diamond","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview.png","id":7},{"title":"Round with Glass","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass-preview.png","id":8},{"title":"Angle Flap","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap-preview.png","id":9}],"timestamp":0}';
	localStorage['shirtPocketStyleDS'] = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png","id":0},{"title":"No Pocket","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png","id":1},{"title":"Classic","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic-preview-left.png","id":6},{"title":"Classic Square","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview-left.png","id":2},{"title":"Classic Angle","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-preview-left.png","id":3},{"title":"Diamond Straight","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight-preview-left.png","id":4},{"title":"Round Flap","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap-preview-left.png","id":5},{"title":"Flap Diamond","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview-left.png","id":7},{"title":"Round with Glass","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass-preview-left.png","id":8},{"title":"Angle Flap","image":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap-preview.png","previewleft":"http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap-preview-left.png","id":9}],"timestamp":0}';    
    localStorage['shirtSleeveStyleDS'] = '{"store":[{"title":"Select","image":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal-preview.png","id":0},{"title":"Normal","image":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal-preview.png","id":1},{"title":"Rolled","image":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/rolled.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/rolled-preview.png","id":2},{"title":"Short","image":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/short.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/sleeves/short-preview.png","id":3}],"timestamp":0}'; 

//	localStorage['shirtMonogramFontDS'] = '{"store":[{"title":"font 1","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font1.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font1-preview.png","id":1},{"title":"font 2","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font2.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font2-preview.png","id":2},{"title":"font 3","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font3.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font3-preview.png","id":3},{"title":"font 4","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font4.png","preview":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font4-preview.png","id":4}],"timestamp":0}';
	localStorage['shirtMonogramFontDS'] = '{"store":[{"title":"Block","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font1.png","id":1},{"title":"Text","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font2.png","id":2},{"title":"Fancy","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font3.png","id":3},{"title":"Script","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font4.png","id":4},{"title":"Diamond","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font5.png","id":5},{"title":"Angle Block","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font6.png","id":6},{"title":"Angle Text","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font7.png","id":7},{"title":"Angle Fancy","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font8.png","id":8},{"title":"Angle Script","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font9.png","id":9},{"title":"Small Letters","image":"http://shirt-tailor.net/thepos/appimg/shirt/monogram/font10.png","id":10}],"timestamp":0}';
	localStorage['fitDS'] = '{"store":[{"title":"Fitted", "id":"Fitted"},{"title":"Semi Fitted", "id":"Semi_Fitted"},{"title":"Standard Fit","id":"Standard_Fit"}],"timestamp":0}';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    dsRegistry = new DatasourceRegistry();

//    custData     = new SyncableDatasource('customersDS_' + authCtrl.userInfo.user_id, 'customersDS_' + authCtrl.userInfo.user_id, dsRegistry, 'customer_endpoint');
    fabricsData  = new SyncableDatasource('fabricsDS', 'fabricsDS', dsRegistry, 'fabrics_endpoint');
//    fabricsDatap  = new SyncableDatasource('fabricsDSp', 'fabricsDSp', dsRegistry, 'fabrics_endpointp');


    pocketTypeDS = new SyncableDatasource('pocketTypeDS', 'pocketTypeDS', dsRegistry);
//    pocketTypeDS = new SyncableDatasource('pocketTypeDS', 'pocketTypeDS', dsRegistry, 'pockettype_endpoint');
/*
    localStorage['countriesDS'] = '{"store":[{"location_name":"Australia","locations_id":"1"},{"location_name":"China","locations_id":"6"}],"timestamp":0}';
    localStorage['statesDS'] = '{"store":[{"location_name":"VIC","locations_id":"4","parent_id":"1"},{"location_name":"NSW","locations_id":"11","parent_id":"1"}],"timestamp":0}';
    localStorage['citiesDS'] = '{"store":[{"location_name":"Melbourne","locations_id":"7","parent_id":"4"},{"location_name":"Sydney","locations_id":"10","parent_id":"11"}],"timestamp":0}';
    
    countriesDS = new SyncableDatasource('countriesDS', 'countriesDS', dsRegistry);
    statesData = new SyncableDatasource('statesDS', 'statesDS', dsRegistry);
    citiesData = new SyncableDatasource('citiesDS', 'citiesDS', dsRegistry);
*/

//////////////////////////////////////////////
// MOVE THEM FROM HERE, THEY CAUSE PROBLEMS	//
//////////////////////////////////////////////
/*
	customerOrdersDS = new SyncableDatasource('customerOrdersDS', 'customerOrdersDS', dsRegistry, 'customer_orders_endpoint');
	customerOrderDS = new SyncableDatasource('customerOrderDS', 'customerOrderDS', dsRegistry, 'customer_order_endpoint');
	customerOrderImagesDS = new SyncableDatasource('customerOrderImagesDS', 'customerOrderImagesDS', dsRegistry, 'customer_order_images_endpoint');
	customerOrderMeasurementsAndFitlineDS = new SyncableDatasource('customerOrderMeasurementsAndFitlineDS', 'customerOrderMeasurementsAndFitlineDS', dsRegistry, 'customer_order_fitnmeas_endpoint');
	customerOrderBodyshapeDS = new SyncableDatasource('customerOrderBodyshapeDS', 'customerOrderBodyshapeDS', dsRegistry, 'customer_order_bodyshape_endpoint');
	customerOrderPaymentsDS = new SyncableDatasource('customerOrderPaymentsDS', 'customerOrderPaymentsDS', dsRegistry, 'customer_order_payments_endpoint');
	customerNotesDS = new SyncableDatasource('customerNotesDS', 'customerNotesDS', dsRegistry, 'customer_notes_endpoint');
*/

//////////////////////////////////////////////
//////////////////////////////////////////////
 

	countriesDS = new SyncableDatasource('countriesDS', 'countriesDS', dsRegistry, 'countries_endpoint');	
	statesData = new SyncableDatasource('statesDS', 'statesDS', dsRegistry, 'states_endpoint');
	citiesData = new SyncableDatasource('citiesDS', 'citiesDS', dsRegistry, 'cities_endpoint');	
//	tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');
	
	
//	localStorage['fitlinesDS'] = '{"store":[{"fitline_name":"fitline 1","fitline_id":"1","value1":"1.1","value2":"1.2","value3":"1.3"},{"name":"fitline 2","fitline_id":"2","value1":"1.1","value2":"1.2","value3":"1.3"},{"name":"fitline 3","fitline_id":"3","value1":"1.1","value2":"1.2","value3":"1.3"},{"name":"fitline 4","fitline_id":"4","value1":"1.1","value2":"1.2","value3":"1.3"}],"timestamp":0}';
//	fitlinesDS = new SyncableDatasource('fitlinesDS', 'fitlinesDS', dsRegistry);
//	fitlinesJacketDS = new SyncableDatasource('fitlinesJacketDS', 'fitlinesJacketDS', dsRegistry, 'jacket_fitlines_endpoint');
	

    //Bodyshape
    bodyshapeShouldersDS = new SyncableDatasource('bodyshapeShouldersDS', 'bodyshapeShouldersDS', dsRegistry);
    bodyshapeNeckStanceDS = new SyncableDatasource('bodyshapeNeckStanceDS','bodyshapeNeckStanceDS', dsRegistry);
    bodyshapeArmsDS = new SyncableDatasource('bodyshapeArmsDS','bodyshapeArmsDS', dsRegistry);
    bodyshapeBackDS = new SyncableDatasource('bodyshapeBackDS','bodyshapeBackDS', dsRegistry);
    bodyshapeBeltAngleDS = new SyncableDatasource('bodyshapeBeltAngleDS','bodyshapeBeltAngleDS', dsRegistry);
    bodyshapeBeltHeightDS = new SyncableDatasource('bodyshapeBeltHeightDS','bodyshapeBeltHeightDS', dsRegistry);
    bodyshapeFrontDS   = new SyncableDatasource('bodyshapeFrontDS','bodyshapeFrontDS', dsRegistry);
    bodyshapeLegDS     = new SyncableDatasource('bodyshapeLegDS','bodyshapeLegDS', dsRegistry);
    bodyshapeNeckHeightDS = new SyncableDatasource('bodyshapeNeckHeightDS','bodyshapeNeckHeightDS', dsRegistry);
    bodyshapeSeatDS    = new SyncableDatasource('bodyshapeSeatDS','bodyshapeSeatDS', dsRegistry);
    bodyshapeSleeveDS  = new SyncableDatasource('bodyshapeSleeveDS','bodyshapeSleeveDS', dsRegistry);
    bodyshapeStandingDS= new SyncableDatasource('bodyshapeStandingDS','bodyshapeStandingDS', dsRegistry);
    bodyshapeStomachDS = new SyncableDatasource('bodyshapeStomachDS','bodyshapeStomachDS', dsRegistry);
    bodyshapeThighDS   = new SyncableDatasource('bodyshapeThighDS','bodyshapeThighDS', dsRegistry);
/*
    bodyshapeShouldersDS = new SyncableDatasource('bodyshapeShouldersDS', 'bodyshapeShouldersDS', dsRegistry, 'bodyshape_shoulders_endpoint');
    bodyshapeNeckStanceDS = new SyncableDatasource('bodyshapeNeckStanceDS','bodyshapeNeckStanceDS', dsRegistry, 'bodyshape_neck_endpoint');
    bodyshapeArmsDS = new SyncableDatasource('bodyshapeArmsDS','bodyshapeArmsDS', dsRegistry, 'bodyshape_arms_endpoint');
    bodyshapeBackDS = new SyncableDatasource('bodyshapeBackDS','bodyshapeBackDS', dsRegistry, 'bodyshape_back_endpoint');
    bodyshapeBeltAngleDS = new SyncableDatasource('bodyshapeBeltAngleDS','bodyshapeBeltAngleDS', dsRegistry, 'bodyshape_beltangle_endpoint');
    bodyshapeBeltHeightDS = new SyncableDatasource('bodyshapeBeltHeightDS','bodyshapeBeltHeightDS', dsRegistry, 'bodyshape_beltheight_endpoint');
    bodyshapeFrontDS   = new SyncableDatasource('bodyshapeFrontDS','bodyshapeFrontDS', dsRegistry, 'bodyshape_front_endpoint');
    bodyshapeLegDS     = new SyncableDatasource('bodyshapeLegDS','bodyshapeLegDS', dsRegistry, 'bodyshape_leg_endpoint');
    bodyshapeNeckHeightDS = new SyncableDatasource('bodyshapeNeckHeightDS','bodyshapeNeckHeightDS', dsRegistry, 'bodyshape_heckheight_endpoint');
    bodyshapeSeatDS    = new SyncableDatasource('bodyshapeSeatDS','bodyshapeSeatDS', dsRegistry, 'bodyshape_seat_endpoint');
    bodyshapeSleeveDS  = new SyncableDatasource('bodyshapeSleeveDS','bodyshapeSleeveDS', dsRegistry, 'bodyshape_sleeve_endpoint');
    bodyshapeStandingDS= new SyncableDatasource('bodyshapeStandingDS','bodyshapeStandingDS', dsRegistry, 'bodyshape_standing_endpoint');
    bodyshapeStomachDS = new SyncableDatasource('bodyshapeStomachDS','bodyshapeStomachDS', dsRegistry, 'bodyshape_stomach_endpoint');
    bodyshapeThighDS   = new SyncableDatasource('bodyshapeThighDS','bodyshapeThighDS', dsRegistry, 'bodyshape_thigh_endpoint');
*/

    vestLapelStyleDS   = new SyncableDatasource('vestLapelStyleDS','vestLapelStyleDS', dsRegistry);
    vestDesignDS	   = new SyncableDatasource('vestDesignDS','vestDesignDS', dsRegistry);
    vestButtonNumberDS = new SyncableDatasource('vestButtonNumberDS','vestButtonNumberDS', dsRegistry);
    vestPocketStyleDS  = new SyncableDatasource('vestPocketStyleDS','vestPocketStyleDS', dsRegistry);
    vestBottomStyleDS  = new SyncableDatasource('vestBottomStyleDS','vestBottomStyleDS', dsRegistry);
    vestBackStyleDS    = new SyncableDatasource('vestBackStyleDS','vestBackStyleDS', dsRegistry);
    pipingColorDS      = new SyncableDatasource('pipingColorDS','pipingColorDS', dsRegistry);
    liningFabricDS     = new SyncableDatasource('liningFabricDS','liningFabricDS', dsRegistry);
    vestButtonsDS      = new SyncableDatasource('vestButtonsDS','vestButtonsDS', dsRegistry);
    vestThreadsDS      = new SyncableDatasource('vestThreadsDS','vestThreadsDS', dsRegistry);
    buttonholethreadDS      = new SyncableDatasource('buttonholethreadDS','buttonholethreadDS', dsRegistry);
/*
	vestLapelStyleDS   = new SyncableDatasource('vestLapelStyleDS','vestLapelStyleDS', dsRegistry, 'vestlapelstyle_endpoint');
    vestButtonNumberDS = new SyncableDatasource('vestButtonNumberDS','vestButtonNumberDS', dsRegistry, 'vestbuttonnumber_endpoint');
    vestPocketStyleDS  = new SyncableDatasource('vestPocketStyleDS','vestPocketStyleDS', dsRegistry, 'vestpocketstyle_endpoint');
    vestBottomStyleDS  = new SyncableDatasource('vestBottomStyleDS','vestBottomStyleDS', dsRegistry, 'vestbottomstyle_endpoint');
    vestBackStyleDS    = new SyncableDatasource('vestBackStyleDS','vestBackStyleDS', dsRegistry, 'vestbackstyle_endpoint');
    pipingColorDS      = new SyncableDatasource('pipingColorDS','pipingColorDS', dsRegistry, 'pipingcolor_endpoint');
    liningFabricDS     = new SyncableDatasource('liningFabricDS','liningFabricDS', dsRegistry, 'liningfabric_endpoint');
    vestButtonsDS      = new SyncableDatasource('vestButtonsDS','vestButtonsDS', dsRegistry, 'vestbuttons_endpoint');
    vestThreadsDS      = new SyncableDatasource('vestThreadsDS','vestThreadsDS', dsRegistry, 'vestthreads_endpoint');
*/

    suitLapelStyleDS      = new SyncableDatasource('suitLapelStyleDS','suitLapelStyleDS', dsRegistry);
    suitBottomStyleDS     = new SyncableDatasource('suitBottomStyleDS','suitBottomStyleDS', dsRegistry);
    suitVentStyleDS       = new SyncableDatasource('suitVentStyleDS','suitVentStyleDS', dsRegistry);
    suitPocketStyleDS     = new SyncableDatasource('suitPocketStyleDS','suitPocketStyleDS', dsRegistry);
    tickPocketStyleDS     = new SyncableDatasource('tickPocketStyleDS','tickPocketStyleDS', dsRegistry);
    suitButtonStyleDS     = new SyncableDatasource('suitButtonStyleDS','suitButtonStyleDS', dsRegistry);
    JacketSleeveButtonNumberDS = new SyncableDatasource('JacketSleeveButtonNumberDS','JacketSleeveButtonNumberDS', dsRegistry);
    JacketBreastPocketStyleDS = new SyncableDatasource('JacketBreastPocketStyleDS','JacketBreastPocketStyleDS', dsRegistry);
    suitJacketStructureDS = new SyncableDatasource('suitJacketStructureDS','suitJacketStructureDS', dsRegistry);
/*
    suitLapelStyleDS      = new SyncableDatasource('suitLapelStyleDS','suitLapelStyleDS', dsRegistry, 'suitlapelstyle_endpoint');
    suitBottomStyleDS     = new SyncableDatasource('suitBottomStyleDS','suitBottomStyleDS', dsRegistry, 'suitbottomstyle_endpoint');
    suitVentStyleDS       = new SyncableDatasource('suitVentStyleDS','suitVentStyleDS', dsRegistry, 'suitventstyle_endpoint');
    suitPocketStyleDS     = new SyncableDatasource('suitPocketStyleDS','suitPocketStyleDS', dsRegistry, 'suitpocketstyle_endpoint');
    suitButtonStyleDS     = new SyncableDatasource('suitButtonStyleDS','suitButtonStyleDS', dsRegistry, 'suitbuttonstyle_endpoint');
    JacketSleeveButtonNumberDS = new SyncableDatasource('JacketSleeveButtonNumberDS','JacketSleeveButtonNumberDS', jacketsleevebuttonnumber_endpoint);
    JacketBreastPocketStyleDS = new SyncableDatasource('JacketBreastPocketStyleDS','JacketBreastPocketStyleDS', jacketbreastpocketstyle_endpoint);
    suitJacketStructureDS = new SyncableDatasource('suitJacketStructureDS','suitJacketStructureDS', dsRegistry, 'suitjacketstructure_endpoint');
*/

    pantBackStyleDS  = new SyncableDatasource('pantBackStyleDS', 'pantBackStyleDS', dsRegistry);
    beltLoopStyleDS  = new SyncableDatasource('beltLoopStyleDS', 'beltLoopStyleDS', dsRegistry);
    beltLoopStyleDS  = new SyncableDatasource('beltLoopStyleDS', 'beltLoopStyleDS', dsRegistry);
    cuffsStyleDS     = new SyncableDatasource('cuffsStyleDS', 'cuffsStyleDS', dsRegistry);
    pantFitDS        = new SyncableDatasource('pantFitDS', 'pantFitDS', dsRegistry);
    pantPleatsDS     = new SyncableDatasource('pantPleatsDS', 'pantPleatsDS', dsRegistry);
    pantPocketsDS    = new SyncableDatasource('pantPocketsDS', 'pantPocketsDS', dsRegistry);
    bpantPocketsDS    = new SyncableDatasource('bpantPocketsDS', 'bpantPocketsDS', dsRegistry);
/*
    pantBackStyleDS  = new SyncableDatasource('pantBackStyleDS', 'pantBackStyleDS', dsRegistry, 'pantbackstyle_endpoint');
    beltLoopStyleDS  = new SyncableDatasource('beltLoopStyleDS', 'beltLoopStyleDS', dsRegistry, 'beltloopstyle_endpoint');
    beltLoopStyleDS  = new SyncableDatasource('beltLoopStyleDS', 'beltLoopStyleDS', dsRegistry, 'beltloopstyle_endpoint');
    cuffsStyleDS     = new SyncableDatasource('cuffsStyleDS', 'cuffsStyleDS', dsRegistry, 'cuffsstyle_endpoint');
    pantFitDS        = new SyncableDatasource('pantFitDS', 'pantFitDS', dsRegistry, 'pantfit_endpoint');
    pantPleatsDS     = new SyncableDatasource('pantPleatsDS', 'pantPleatsDS', dsRegistry, 'pantpleats_endpoint');
    pantPocketsDS    = new SyncableDatasource('pantPocketsDS', 'pantPocketsDS', dsRegistry, 'pantpockets_endpoint');
    bpantPocketsDS    = new SyncableDatasource('bpantPocketsDS', 'bpantPocketsDS', dsRegistry, 'bpantpockets_endpoint');
*/
	shirtButtonsDS      = new SyncableDatasource('shirtButtonsDS','shirtButtonsDS', dsRegistry);
    shirtBackStyleDS    = new SyncableDatasource('shirtBackStyleDS', 'shirtBackStyleDS', dsRegistry);
    shirtBottomStyleDS  = new SyncableDatasource('shirtBottomStyleDS', 'shirtBottomStyleDS', dsRegistry);
    shirtCollarStyleDS  = new SyncableDatasource('shirtCollarStyleDS', 'shirtCollarStyleDS', dsRegistry);
    shirtCuffsStyleDS   = new SyncableDatasource('shirtCuffsStyleDS', 'shirtCuffsStyleDS', dsRegistry);
    shirtFitStyleDS     = new SyncableDatasource('shirtFitStyleDS', 'shirtFitStyleDS', dsRegistry);
    shirtFrontStyleDS   = new SyncableDatasource('shirtFrontStyleDS', 'shirtFrontStyleDS', dsRegistry);
    shirtPocketStyleDS  = new SyncableDatasource('shirtPocketStyleDS', 'shirtPocketStyleDS', dsRegistry);
    shirtSleeveStyleDS  = new SyncableDatasource('shirtSleeveStyleDS', 'shirtSleeveStyleDS', dsRegistry);
    shirtMonogramFontDS = new SyncableDatasource('shirtMonogramFontDS', 'shirtMonogramFontDS', dsRegistry);
    
    
    fitDS  				= new SyncableDatasource('fitDS', 'fitDS', dsRegistry); 
/*
    shirtBackStyleDS    = new SyncableDatasource('shirtBackStyleDS', 'shirtBackStyleDS', dsRegistry, 'shirtbackstyle_endpoint');
    shirtBottomStyleDS  = new SyncableDatasource('shirtBottomStyleDS', 'shirtBottomStyleDS', dsRegistry, 'shirtbottomstyle_endpoint');
    shirtCollarStyleDS  = new SyncableDatasource('shirtCollarStyleDS', 'shirtCollarStyleDS', dsRegistry, 'shirtcollarstyle_endpoint');
    shirtCuffsStyleDS   = new SyncableDatasource('shirtCuffsStyleDS', 'shirtCuffsStyleDS', dsRegistry, 'shirtcuffsstyle_endpoint');
    shirtFitStyleDS     = new SyncableDatasource('shirtFitStyleDS', 'shirtFitStyleDS', dsRegistry, 'shirtfitstyle_endpoint');
    shirtFrontStyleDS   = new SyncableDatasource('shirtFrontStyleDS', 'shirtFrontStyleDS', dsRegistry, 'shirtfrontstyle_endpoint');
    shirtPocketStyleDS  = new SyncableDatasource('shirtPocketStyleDS', 'shirtPocketStyleDS', dsRegistry, 'shirtpocketstyle_endpoint');
    shirtSleeveStyleDS  = new SyncableDatasource('shirtSleeveStyleDS', 'shirtSleeveStyleDS', dsRegistry, 'shirtsleevestyle_endpoint');
*/

	
	

    ko.bindingHandlers.updateFormOnChange = {
        update: function(element) {
            $(element).trigger('create');
        }
    };

    ko.bindingHandlers.updateSliderOnChange = {
        init: function (element, valueAccessor) {
            // use setTimeout with 0 to run this after Knockout is done
            setTimeout(function () {
                // $(element) doesn't work as that has been removed from the DOM
                var curSlider = $('#' + element.id);
                // helper function that updates the slider and refreshes the thumb location
                function setSliderValue(newValue) {
                    curSlider.val(newValue).slider('refresh');
                }
                // subscribe to the bound observable and update the slider when it changes
                valueAccessor().subscribe(setSliderValue);
                // set up the initial value, which of course is NOT stored in curSlider, but the original element :\
                setSliderValue($(element).val());
                // subscribe to the slider's change event and update the bound observable
                curSlider.bind('change', function () {
                    valueAccessor()(curSlider.val());
                });
            }, 0);
        }
    };

    ko.bindingHandlers.checkbox = {
        update: function(element, valueAccessor) {
            setTimeout(function () {
                var value = valueAccessor();
                var valueUnwrapped = ko.utils.unwrapObservable(value);
                $(element).attr("checked", valueUnwrapped).checkboxradio("refresh");
            }, 0);
        }
    };

    //fabricsVM   = new FabricsCntClass(dsRegistry);
    //fabricsVM.subscribeTo('fabricsDS');

//    customersVM = new CustomerCntClass(dsRegistry);
//    customersVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);

//    authCtrl = new AuthControl();
    console.log('Version 085');
//    orderItem   = new OrderItem(dsRegistry);




});
