﻿

require.config({
    paths: {
        // RequireJS plugin
        text:'libs/require/text',
        domReady:'libs/require/domReady',
        // jQuery
        jquery:'libs/jquery/jquery-1.8.2',
        jclass:'libs/jquery.class',
        jqmdialog: 'libs/jqmSimpleDialog/jquery.mobile.simpledialog.min',
		// jQuery Mobile framework
        jqm:'libs/jquery.mobile/jquery.mobile-1.2.0',
		jqmrouter: 'libs/jquery.mobile.router/jquery.mobile.router',
        knockout:'libs/knockout/knockout-2.2.0',
		jqmSM: 'libs/jqm.slidemenu',
		kojqm: 'libs/kojqm',
		komap: 'libs/ko.mapping',
		iscroll: 'libs/iscroll',
		jqmSlide: 'libs/jqmSlide',
		jgrowl: 'libs/jquery.jgrowl.min',
		fscreen: 'libs/jquery.fullscreen',
		router: 'router',
		base: 'appBase',
		app: 'app',
	//	signature: 'signature/js/dd_signature_pad',
		signature: 'signature/jp_signature2',
		trie: 'libs/trie',
		upload: 'tets/uploadMediaTest',
	}
});

require(['jquery', 'jqmrouter'], function () {
	require(['jqm', 'knockout', 'iscroll', 'komap', 'jclass', 'libs/jqmExternalAsEmbeddedPages', 'jqmSlide', 'fscreen', 'jgrowl' , 'base', 'signature', 'trie', 'upload'], function (jqm, ko, iscroll, komap) {
		require([
			'jqmdialog',
			'viewmodel/AuthControl',
			'viewmodel/CustomerCntClass',
			'viewmodel/FabricsCntClass',
			'viewmodel/Bodyshape',
            'viewmodel/OrderItemSelectGarment',
			'viewmodel/Measurements',
			'viewmodel/FitlinesJacket',
			'viewmodel/FitlinesPants',
			'viewmodel/Fitting',
			'viewmodel/Completion',
			'viewmodel/Alterations',
			'viewmodel/AlterationReport',
			'viewmodel/ShippingFabric',
			'viewmodel/ShippingCopyGarment',
			'viewmodel/ShippingFaultyRemakeGarments',
			'viewmodel/ShippingOther',
			'viewmodel/ErrorReport',
			'viewmodel/garments/GarmentsSuit',
			'viewmodel/garments/GarmentsVest',
			'viewmodel/garments/GarmentsShirt',
			'viewmodel/garments/GarmentsJacket',
			'viewmodel/garments/GarmentsPant',
			'viewmodel/garments/GarmentsShirt'

			], function() {
			
				$.mobile.pageContainer = $('#container');
				// Setting default transition to slide
				$.mobile.defaultPageTransition = 'slide';
				ko.mapping = komap;
				window.ko  = ko;
				window.BUrl = "http://www.shirt-tailor.net/thepos/";
				//window.BUrl = "http://www.shirt-tailor.net/thepas/";

				//Application entrypoint, define Globals
					
				//Pass application control to router
				require(['app', 'router']);
			
		});
	});
});
