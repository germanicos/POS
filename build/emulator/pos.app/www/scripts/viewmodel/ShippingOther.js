define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingOther = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		this.ShippingOtherData     = [];
		this.ShippingOtherDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);

		this.workflow    = ko.observableArray([
			{id: 0, target: "#completions",  completed: "false", caption: "Completion", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			$.mobile.changePage(data.target);
			$('.cloneDialog').remove(); 
			that.renderShippingOther();
		});
		
		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});
		
		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});

		this.completion	= ko.observable(0);
		this.variantNameShippingOther  = ko.observableArray([{id: 0, title: "ShippingOther 1"}]);
		this.selectedVariantShippingOther  = ko.observable(this.variantNameShippingOther()[0]);	
		this.selectedVariantShippingOther.subscribe(function(data) {
			that.selectedVariantShippingOther();
			that.renderShippingOther();			
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});

				
		this.Description = ko.observable('');
		this.Description.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingOtherData[ that.getRow(that.selectedVariantShippingOther().id)  ].Description = data;
				that.flushModel();
			}	
		});
		
		this.VideoUrl = ko.observable('');
		this.VideoUrl.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingOtherData[ that.getRow(that.selectedVariantShippingOther().id)  ].VideoUrl = data;
				that.flushModel();
			}	
		});

		this.VideoComments = ko.observable('');
		this.VideoComments.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingOtherData[ that.getRow(that.selectedVariantShippingOther().id)  ].VideoComments = data;
				that.flushModel();
			}	
		});
		
			

///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var photosarray = [];
			var images = localStorage.getItem('ShippingOtherPhotos');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('ShippingOtherPhotos') != null){
				if(localStorage.getItem('ShippingOtherPhotos').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			var customerString = orderItem.custSelectVM.selectedCustomer();
	
			if(images.length  == 0){
				customAlert("Please upload at least one image");
			}else if(this.VideoUrl().trim() == ""){
				customAlert("Please enter a video url");
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					customer: customerString,
					date: dateToPost,
					description: this.Description(),
					video: this.VideoUrl(),
					video_comments: this.VideoComments(),
					images: photosarray,
					tags: tagsarray
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				var endpoint_url = BUrl + 'shippings/others_endpoint';
					
				$.post(endpoint_url, {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Succesfully sent");
		                $.mobile.changePage('#shipping_main');
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
/*
	nextStep: function() {
		var tWork = this.workflow();
		for (var ind in tWork ) {
				if (!tWork[ind].completed) {
				this.currentStep( tWork[ind] );
				return;
			}
		}
	},*/
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingOther"  : this.ShippingOtherData
		}, true);
		this.renderShippingOther();
	},

	getVariant: function(id) {
		var toreturn = this.ShippingOtherData[0];
		for (var ind in this.ShippingOtherData) {
			if ( this.ShippingOtherData[ind].variantId == id  ) {
				toreturn = this.ShippingOtherData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingOtherData) {
			if ( this.ShippingOtherData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantShippingOther: function(name) {
		if(this.variantNameShippingOther()[0].title != "ShippingOther 1"){
			this.ShippingOtherDataAID += 1;
			var newid = this.ShippingOtherDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingOther().id ) )  ); //CLONE Object
			this.variantNameShippingOther.push({title: vname, id: this.ShippingOtherDataAID});
			tObj.variantId = this.ShippingOtherDataAID;		
			this.flushModel();
		}else{
			this.variantNameShippingOther()[0].title = name;
		}
	},	
	
	addVariantsShippingOther: function() {
		for(var x in orderItem.ShippingOtherVM.ShippingOtherData){
		//	console.log("addVariantsShippingOther " + x);
			if( x != 0){
				this.ShippingOtherDataAID += 1;
				var vname = "ShippingOther " + x; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingOther().id ) )  ); //CLONE Object
				this.variantNameShippingOther.push({title: vname, id: this.ShippingOtherDataAID});
				tObj.variantId = this.ShippingOtherDataAID;		
				this.flushModel();
			}else{
				var vname = "ShippingOther " + x; 
				//var vname = orderItem.ShippingOtherVM.ShippingOtherData[x].pant;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingOther().id ) )  ); //CLONE Object
				this.variantNameShippingOther()[x].title = vname;
				tObj.variantId = this.ShippingOtherDataAID;		
				this.flushModel();
			}
		}
	},

	digestData: function(data) {
		this.ShippingOtherData  = data.ShippingOther;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingOther();
	},

	renderShippingOther: function() {	
		//Get selected Variant		
		try{
			var tData = this.getVariant(this.selectedVariantShippingOther().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.Description)  		!= "undefined") {this.Description(tData.Description);}
				if (typeof(tData.VideoUrl)   		!= "undefined") {this.VideoUrl(tData.VideoUrl);}
				if (typeof(tData.VideoComments)    	!= "undefined") {this.VideoComments(tData.VideoComments);}
			}
		}catch(e){
			;
		}

	}
});

defShippingOther = SimpleDatasource.extend({
	init: function( name, dsRegistry) {
	//	if(data == null || data == undefined){
			var ShippingOther = {};
			ShippingOther.variantId        = 0;

			ShippingOther.Description = '';
			ShippingOther.VideoUrl = '';
			ShippingOther.VideoComments = '';
			
			var iShippingOther = {
				ShippingOther: []
			};
			iShippingOther.ShippingOther.push(ShippingOther);
			this._super(name, iShippingOther, dsRegistry);
	//	}	
	}
});


//END DEFINE CLOSURE
});
