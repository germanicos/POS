define(['jquery', 'knockout', 'base'], function($, ko) {
Completion = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		this.CompletionData     = [];
		this.CompletionDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);

		this.workflow    = ko.observableArray([
			{id: 0, target: "#completions",  completed: "false", caption: "Completion", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			$.mobile.changePage(data.target);
			$('.cloneDialog').remove();
			that.renderCompletion(); 
		});

		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});


		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});


		this.completion       = ko.observable(0);
		this.variantNameCompletion  = ko.observableArray([{id: 0, title: "Completion 1"}]);
		this.selectedVariantCompletion  = ko.observable(this.variantNameCompletion()[0]);	
		this.selectedVariantCompletion.subscribe(function(data) {
			that.selectedVariantCompletion();
		//	$.mobile.changePage("#completions");
		//	$('.cloneDialog').remove();
			that.renderCompletion();
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});
		

		this.CompletionCustomer = ko.observable('');
		this.CompletionCustomer.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionCustomer = data;
			that.flushModel();
		});

		this.CompletionGarment = ko.observable('');
		this.CompletionGarment.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionGarment = data;
			that.flushModel();
		});
		
		this.CompletionGarmentFabric = ko.observable('');
		this.CompletionGarmentFabric.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionGarmentFabric = data;
			that.flushModel();
		});

		this.CompletionId = ko.observable('');
		this.CompletionId.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionId = data;
			that.flushModel();
		});
		
		this.CompletionOrderId = ko.observable('');
		this.CompletionOrderId.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionOrderId = data;
			that.flushModel();
		});
		
		this.CompletionOrderProductId = ko.observable('');
		this.CompletionOrderProductId.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionOrderProductId = data;
			that.flushModel();
		});

		this.SkippedImages = ko.observable(false);
		this.SkippedImages.subscribe(function(data) {		
			that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].SkippedImages = data;
			that.flushModel();
		});
		
		
		
		this.NoImagesConfirm = function() {
        	
       		text = "By skipping uploading images, you agree that any fault with this garment is no longer in Germanicos control and you are in full responsibility";				
			document.getElementById("warningdialogtext").innerHTML = text;
			dialog = document.getElementById('warningdialog');
			dialog.style.display = "block";
        	return true;
    	};
    		
    	this.buttonNoAccept = function(){
			that.SkippedImages(false);
    		dialog = document.getElementById('warningdialog');
			dialog.style.display = "none";
    	}
    	
    	this.buttonYesAccept = function(){				
			that.SkippedImages(true);
			that.sync();
			dialog = document.getElementById('warningdialog');
			dialog.style.display = "none";	
    	}		

		this.CompletionDialog = function() {
			dialog = document.getElementById('completiondialog');
			dialog.style.display = "block";
        	return true;
    	};
    	
    	this.buttonToFittings = function(){
			$.mobile.changePage('#customerGarmentsFittingsList');
    	}
    	
    	this.buttonToMain = function(){				
			$.mobile.changePage('#main');
    	}	

		this.allCompletedCheck = function(){
			var found = true;
   			for(var x = 0; x < that.variantNameCompletion().length; x++){
   				if( that.CompletionData[ x ].CompletionId == '' && that.selectedVariantCompletion().id != x){
   					found = false;
   					break;
   				}
   			}	
   			return found;
		}	

   		this.submitText = function(){
   			if(this.allCompletedCheck() == true){
   				return "SUBMIT & COMPLETE";
   			}else{
   				return "SUBMIT & NEXT GARMENT";
   			}
   			
		}		
		
		this.goToNextNotCompleted = function(){
			for(var x = 0; x < that.variantNameCompletion().length; x++){
   				if( that.CompletionData[ x ].CompletionId == ''){
   					that.selectedVariantCompletion(that.variantNameCompletion()[x]);
   					break;
   				}
   			}	
		}
		
		

///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";
		
			var whichGarment = that.selectedVariantCompletion().id;
			
			var photosarray = [];
			frontphotos = localStorage.getItem('CompletionFrontImagePhoto' + whichGarment);			
			frontphotos = (frontphotos == null) ? [] : JSON.parse(frontphotos);
			if(localStorage.getItem('CompletionFrontImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('CompletionFrontImagePhoto' + whichGarment).length > 0){				
					for(var i=0, len=frontphotos.length; i<len; i++){
						var obj = new Object();
		   				obj.name = frontphotos[i].picture;
		   				obj.id  = "" + frontphotos[i].image_id + "";
		   				obj.image_type = "1"; 
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			sidephotos = localStorage.getItem('CompletionSideImagePhoto' + whichGarment);
			sidephotos = (sidephotos == null) ? [] : JSON.parse(sidephotos);
			if(localStorage.getItem('CompletionSideImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('CompletionSideImagePhoto' + whichGarment).length > 0){
					for(var i=0, len=sidephotos.length; i<len; i++){
						var obj = new Object();
		   				obj.name = sidephotos[i].picture;
		   				obj.id  = "" + sidephotos[i].image_id + "";
		   				obj.image_type = "2"; 
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			backphotos = localStorage.getItem('CompletionBackImagePhoto' + whichGarment);
			backphotos = (backphotos == null) ? [] : JSON.parse(backphotos);
			if(localStorage.getItem('CompletionBackImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('CompletionBackImagePhoto' + whichGarment).length > 0){
					for(var i=0, len=backphotos.length; i<len; i++){
						var obj = new Object();
		   				obj.name = backphotos[i].picture;
		   				obj.id  = "" + backphotos[i].image_id + "";
		   				obj.image_type = "3"; 
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}

			var customerString = orderItem.custSelectVM.selectedCustomer();
			var completionIdPost = this.CompletionId();

			var dateToPost = '';		
			var date = new Date();
			var yearnow = date.getFullYear();
			var monthnow = date.getMonth() + 1;
			var daynow = date.getDate();			
			dateToPost = daynow + "-" + monthnow + "-" + yearnow;
					
			
//console.log("this.CompletionVideo(): " + this.CompletionVideo());
			if( (frontphotos.length  == 0 || sidephotos.length  == 0 || backphotos.length  == 0) && this.SkippedImages() == false){
				customAlert("Please upload all required images");
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					customer: customerString,
					//completion_id: completionIdPost,
					order_id: this.CompletionOrderId(),//this.selected_order.order_id,
					orders_products_id: this.CompletionOrderProductId(),
					garment: this.CompletionGarment(),
					//skipped_images: this.SkippedImages(),
					images: photosarray
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);

/*
this.CompletionId("TEST");
if(this.allCompletedCheck() == false){
	this.goToNextNotCompleted();
}else{
	this.CompletionDialog();
}
*/

				$.post(BUrl + 'client_fittings/completion_endpoint', { 
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Completion succesfully added");	
		            	
		            	that.CompletionId(dataS.completion);

						if(that.allCompletedCheck() == false){
							that.goToNextNotCompleted();
						}else{	// show dialog
							that.CompletionDialog();
						}	
						
		                //if(that.variantNameCompletion().length == 1){
		                //	$.mobile.changePage('#customerGarmentsFittingsList');
		                //}
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"Completion"  : this.CompletionData
		}, true);
	},

	getVariant: function(id) {
		var toreturn = this.CompletionData[0];
		for (var ind in this.CompletionData) {
			if ( this.CompletionData[ind].variantId == id  ) {
				toreturn = this.CompletionData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.CompletionData) {
			if ( this.CompletionData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantCompletion: function(name) {
//console.log(this.variantNameCompletion);
		if(this.variantNameCompletion()[0].title != "Completion 1"){
			this.CompletionDataAID += 1;
			var newid = this.CompletionDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantCompletion().id ) )  ); //CLONE Object
			this.variantNameCompletion.push({title: vname, id: this.CompletionDataAID});
			tObj.variantId = this.CompletionDataAID;		
			this.flushModel();
		}else{
			this.variantNameCompletion()[0].title = name;
		}
	},	
	
	addVariantsCompletions: function() {
		for(var x in orderItem.completionsVM.CompletionData){
		//	console.log("addVariantsCompletions " + x);
			
			if( x != 0){
				this.CompletionDataAID += 1;
				var vname = orderItem.completionsVM.CompletionData[x].CompletionGarment; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantCompletion().id ) )  ); //CLONE Object
				this.variantNameCompletion.push({title: vname, id: this.CompletionDataAID});
				tObj.variantId = this.CompletionDataAID;		
				this.flushModel();
			}else{
				var vname = orderItem.completionsVM.CompletionData[x].CompletionGarment;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantCompletion().id ) )  ); //CLONE Object
				this.variantNameCompletion()[x].title = vname;
				tObj.variantId = this.CompletionDataAID;		
				this.flushModel();
			}
			
		}
	},
	
	

	digestData: function(data) {
		this.CompletionData  = data.Completion;
		this.renderView();

	},

	renderView: function() {
		this.renderCompletion();
	},

	renderCompletion: function() {	
		
		var whichGarment = this.selectedVariantCompletion().id;
		try{
			photo = JSON.parse( localStorage.getItem('CompletionFrontImagePhoto' + whichGarment) );			
			document.getElementById("CompletionFrontImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'" width="240"/>'].join('');
		}catch(e){			
			try{
				document.getElementById("CompletionFrontImageMedia").innerHTML = ['<img src="gfx/fittings/take-image-front.png" width="240" />'].join('');
			}catch(e2){ ; }
		}
		try{
			photo = JSON.parse( localStorage.getItem('CompletionSideImagePhoto' + whichGarment) );
			document.getElementById("CompletionSideImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'" width="240"/>'].join('');
		}catch(e){
			try{
				document.getElementById("CompletionSideImageMedia").innerHTML = ['<img src="gfx/fittings/take-image-side.png" width="240" />'].join('');
			}catch(e2){ ; }
		}
		try{
			photo = JSON.parse( localStorage.getItem('CompletionBackImagePhoto' + whichGarment) );
			document.getElementById("CompletionBackImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'" width="240" width="240" />'].join('');
		}catch(e){
			try{
				document.getElementById("CompletionBackImageMedia").innerHTML = ['<img src="gfx/fittings/take-image-back.png" width="240" />'].join('');
			}catch(e2){ ; }				
		}
		
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantCompletion().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.CompletionCustomer)		!= "undefined") {this.CompletionCustomer(tData.CompletionCustomer);}
				if (typeof(tData.CompletionGarment)		!= "undefined") {this.CompletionGarment(tData.CompletionGarment);}
				if (typeof(tData.CompletionGarmentFabric)	!= "undefined") {this.CompletionGarmentFabric(tData.CompletionGarmentFabric);}
				if (typeof(tData.CompletionId)    			!= "undefined") {this.CompletionId(tData.CompletionId);}
				if (typeof(tData.CompletionOrderId)    	!= "undefined") {this.CompletionOrderId(tData.CompletionOrderId);}
				if (typeof(tData.CompletionOrderProductId)    	!= "undefined") {this.CompletionOrderProductId(tData.CompletionOrderProductId);}
				if (typeof(tData.SkippedImages)   != "undefined") {this.SkippedImages(tData.SkippedImages);}
			}
		}catch(e){
			;
		}
	}
});

defCompletion = SimpleDatasource.extend({
	init: function( name, dsRegistry, data ) {
		if(data == null || data == undefined){
		//	orderItem.completionsVM.addVariantCompletion('new');
			
console.log("data is null");			
			var Completion = {};
			Completion.variantId        = 0;
			
			Completion.CompletionCustomer = '';
			Completion.CompletionGarment = '';
			Completion.CompletionGarmentFabric = '';
			Completion.CompletionId = '';
			Completion.CompletionOrderId = '';
			Completion.CompletionOrderProductId = '';
			Completion.SkippedImages = false;

			var iCompletion = {
				Completion: []
			};
			iCompletion.Completion.push(Completion);
			this._super(name, iCompletion, dsRegistry);
			
		}else{
//console.log("data is not null");
			var iCompletion = {
				Completion: []
			};
			
			var x = 0;
			for (var a in data) {
				for (var b in data[a].products){
					if( data[a].products[b].checked == true ){
					//	orderItem.completionsVM.addVariantCompletion(data[a].products[b].garment);
					var c = localStorage.getItem("completionindex");
//console.log("data[a].products[b]: " + JSON.stringify(data[a].products[b]) );					
//console.log("completion index: " + c);
						var Completion = {};
						Completion.variantId = x;
						
						Completion.CompletionId = '';		

						Completion.CompletionCustomer = orderItem.custSelectVM.selectedCustomer();
						Completion.CompletionGarment = data[a].products[b].garment;
						Completion.CompletionGarmentFabric = data[a].products[b].fabric;
						Completion.CompletionOrderId = data[a].order_id;
						Completion.CompletionOrderProductId = data[a].products[b].orders_products_id;
						Completion.SkippedImages = false;
						
						x++;
//console.log("Completion: " + JSON.stringify(Completion));
						iCompletion.Completion.push(Completion);	
					}
				}
			}
			this._super(name, iCompletion, dsRegistry);
		}	
	}
});


//END DEFINE CLOSURE
});
