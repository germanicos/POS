define(['jquery', 'knockout', 'base'], function($, ko) {
	
	ErrorReportListVM = SimpleControl.extend({
		
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			
			this.selectedError = 0;
			//where the data will be stored
			this.ErrorData = ko.observableArray();
			this.burl = "http://www.shirt-tailor.net/thepos/";
			
			//function to load a specific error based on click
			loadSpecificError = function(errorInfo){
				that.selectedError = errorInfo.error_id;
				$.mobile.changePage('#errorReport');
			};
			
			this.addNewError = function(){
				$.mobile.changePage('#errorAdd');
			};
			
		},
		
		
		//this function load Errors will send the authentication data(user/password) to the server and then receive the errorsList back
		loadErrorsList: function() {
			var that = this;
			//collect the user data and prepare ir to send.
			data = {
				user_id: authCtrl.userInfo.user_id
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			that.ErrorData.removeAll();
			
			//send the information for the endpoint and wait or an answer 
			$.ajax({
				type: 'POST',
				url: this.burl + 'error_report_endpoint/get_user_errors',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//that.ErrorData.removeAll();
					temp = JSON.parse(dataS);
					for(x in temp){
						that.ErrorData().push(temp[x]);
					}
				},
				async: false
			});
			
			//alert(this.ErrorData.length);
		}
		
	});
	
	ErrorReportVM = SimpleControl.extend({
		init: function(DsRegistry, customerVM) {
			this._super( DsRegistry );
			this.customerSelectVM = customerVM;
			var that = this;
			
			this.orderGarments = new OrderGarmentAutoComplete(this.DsRegistry);
			this.customerOrder = new CustomerOrdersAutoComplete(this.DsRegistry);
			this.customers = new CustomerAutoCompleteVM(this.DsRegistry, this.customerSelectVM);
			this.errorCatgory = new ErrorCategory(this.DsRegistry);
			this.users = new UsersAutoComplete(this.DsRegistry);
			
			//indicates which ErrorReport the user select at a time
			this.selectedError = 0;
			
			//variables that are harded
			this.status = ko.observableArray([
				{status_id: '1', status_text:'Open'},
				{status_id: '2', status_text:'Awaiting Reply'},
				{status_id: '3', status_text:'Holding for Later'},
				{status_id: '4', status_text:'Resolved'},
				{status_id: '5', status_text:'Closed'}
			]);
			
			this.priority = ko.observableArray([
				{priority_id: '1', priority_text: 'Lowest'},
				{priority_id: '2', priority_text: 'Low'},
				{priority_id: '3', priority_text: 'Normal'},
				{priority_id: '4', priority_text: 'High'},
				{priority_id: '5', priority_text: 'Urgent'}
			]);
			
			this.statusTextToID = function(value){
				for(x in this.status()){
					if(value == this.status()[x].status_text)
						return this.status()[x].status_id;
				}
				return '';
				
			};
			
			this.statusIDtoText = function(value){
				for(x in this.status()){
					if(value == this.status()[x].status_id)
						return this.status()[x].status_text;
				}
				return '';
			};
			
			this.priorityTextToID = function(value){
				for(x in this.priority()){
					if(value == this.priority()[x].priority_text)
						return this.priority()[x].priority_id;
				}
				return '';
			};
			
			this.priorityIDToText = function(value){
				for(x in this.priority()){
					if(value == this.priority()[x].priority_id)
						return this.priority()[x].priority_text;
				}
				return '';
			};
			
			//variables that store the error information. Loadaded by the function load info 
			that.error_id = ko.observable();
			that.error_assigned_to = ko.observable();
			that.error_cat_id = ko.observable();
			that.error_date = ko.observable();
			that.error_desc = ko.observable();
			that.error_fix_date = ko.observable();
			that.error_link_customers = ko.observable();
			that.error_link_order = ko.observable();
			that.error_link_users = ko.observableArray();
			that.error_linked_garment = ko.observable();
			that.error_media_ids = ko.observable();
			that.error_media_video = ko.observable();
			that.error_priority = ko.observable();
			that.error_reason = ko.observable();
			that.error_report_by = ko.observable();
			that.error_status = ko.observable();
			that.error_title = ko.observable();
			that.media = ko.observableArray();
			that.replies = ko.observableArray();
			
			//function that will deal with all necessary operations to fill the array media
			this.fillMedia = function(array){
				//that.media = array;
				for(x in array){
					array[x].media_path = 'http://www.shirt-tailor.net/thepos/server/errorreport/files/' + array[x].media_path
					that.media.push(array[x]);
				}
				
			};
			
			//function that will deal with all necessary operations to fill the array replies
			this.fillReplies = function(array){
				for(x in array){
					that.replies.push(array[x]);
				}
			};
			
			//clean all the values from all variables
			this.cleanAll = function(){
				var that = this;
				that.error_id(null);
				that.error_assigned_to(null);
				that.error_cat_id(null);
				that.error_date(null);
				that.error_desc(null);
				that.error_fix_date(null);
				that.error_link_customers(null);
				that.error_link_order(null);
				that.error_link_users.removeAll();
				that.error_linked_garment(null);
				that.error_media_ids(null);
				that.error_media_video(null);
				that.error_priority(null);
				that.error_reason(null);
				that.error_report_by(null);
				that.error_status(null);
				that.error_title(null);
				that.media.removeAll();
				that.replies.removeAll();
				that.orderGarments = new OrderGarmentAutoComplete(this.DsRegistry);
				that.customerOrder = new CustomerOrdersAutoComplete(this.DsRegistry);
				that.customers = new CustomerAutoCompleteVM(this.DsRegistry, this.customerSelectVM);
				that.errorCatgory = new ErrorCategory(this.DsRegistry);
				that.users = new UsersAutoComplete(this.DsRegistry);
			};
		},
		
		routeToEditError :function(){
			$.mobile.changePage('#errorEdit');
		},
		
		//set the error that the class will work with
		setSelectedError: function(errorID){
			this.selectedError = errorID;
		},
		
		//load all information necessary using the selected error
		loadInfo : function(){
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			this.cleanAll();
			this.loadErrorsInfo();
			//alert(this.selectedError);
			//collect the user data and prepare ir to send.
			data = {
				error_id: that.selectedError
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_specific_error',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					var temp = JSON.parse(dataS);
					that.error_id = ko.observable(temp.error.error_id);
					that.error_assigned_to = ko.observable(temp.error.error_assigned_to);
					that.error_cat_id = ko.observable(temp.error.error_cat_id);
					that.errorCatgory.setSelectedCategory(temp.error.error_cat_id);
					that.error_date = ko.observable(temp.error.error_date);
					that.error_desc = ko.observable(temp.error.error_desc);
					that.error_fix_date = ko.observable(temp.error.error_fix_date);
					that.error_link_customers = ko.observable(temp.error.error_link_customers);
					that.error_link_order = ko.observable(temp.error.error_link_order);
					that.error_link_users = ko.observableArray(temp.error.error_link_users);
					that.error_linked_garment = ko.observable(temp.error.error_linked_garment);
					that.error_media_ids = ko.observable(temp.error.error_media_ids);
					that.error_media_video = ko.observable(temp.error.error_media_video);
					that.error_priority = ko.observable(that.priorityIDToText(temp.error.error_priority));
					that.error_reason = ko.observable(temp.error.error_reason);
					that.error_report_by = ko.observable(temp.error.error_report_by);
					that.error_status = ko.observable(that.statusIDtoText(temp.error.error_status));
					that.error_title = ko.observable(temp.error.error_title);
					that.fillMedia(temp.media);
					that.fillReplies(temp.errors_replies);
				},
				async: false
			});
			
		},
		
		addNewReply: function(){
			this.ErrorReplyVM = new ErrorReplyVM(this.DsRegistry);
			this.ErrorReplyVM.setSelectedError(this.selectedError);
			$.mobile.changePage('#errorReply');
		},
		
		loadErrorsInfo: function(){
			this.errorCatgory.loadInfo();
			this.users.loadInfo();
		},
		
		prepareToUpdate: function(){
			console.log('update');
			this.prepareToInsert();
			if(this.error_link_customers()){
				this.customers.selectCustomer(this.error_link_customers());
				this.customerOrder.setSelectedOrder(this.error_link_order());
				if(this.error_linked_garment() > 0)
					this.orderGarments.setSelectedGarments(this.error_linked_garment());
				if(this.error_link_users())
					this.users.selectUsers(this.error_link_users());
				
				console.log('c' + this.errorCatgory.categorie1().category_name);
			}
			
		},
		
		//this function will initialize everything needed to the add and edit error action
		prepareToInsert: function(){
			this.customerOrder.setOrderGarment(this.orderGarments);
			this.customers.setCustomerOrder(this.customerOrder);
			this.loadErrorsInfo();
		},
		
		//cancel the function add new error and return to error list
		cancelAdd : function(){
			$.mobile.changePage('#errorList');
		},
		
		//add a new error into the database
		addError : function(){
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			
			//collect the user data and prepare ir to send.
			data = {
				error_cat_id : that.errorCatgory.categorieID,
				error_desc : that.error_desc(),
				error_link_customers : that.customers.selectedCustomer().server_id,
				error_link_order : that.customerOrder.selectedOrder().order_id,
				error_link_users : that.users.getUsers(),
				error_linked_garment : that.orderGarments.getGarments(),
				error_media_ids : [],
				error_media_video : "",
				error_priority : that.priorityTextToID(that.error_priority()),
				error_reason : that.error_reason(),
				error_report_by : authCtrl.userInfo.user_id,
				error_title : that.error_title()
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			console.log(data);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/new_error',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					
				},
				async: false
			});
			
			$.mobile.changePage('#errorList');
		},
		
		//cancel the update error action and return to the error report
		cancelEdit : function(){
			$.mobile.changePage('#errorReport');
		},
		
		//update an existing error in the database
		editError : function(){
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			
			//collect the user data and prepare ir to send.
			data = {
				error_id: that.error_id(),
				error_assigned_to : that.error_assigned_to(),
				error_cat_id : that.error_cat_id(),
				error_date : that.error_date(),
				error_desc : that.error_desc(),
				error_fix_date : that.error_fix_date(),
				error_link_customers : that.error_link_customers(),
				error_link_order : that.error_link_order(),
				error_link_users : that.error_link_users(),
				error_linked_garment : that.error_linked_garment(),
				error_media_ids : [],
				error_media_video : "",
				error_priority : that.error_priority(),
				error_reason : that.error_reason(),
				error_report_by : that.error_report_by(),
				error_status : that.error_status(),
				error_title : that.error_title()
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			/*$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/update_error',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					
				},
				async: false
			});*/
			$.mobile.changePage('#errorReport');
		},
		
	});
	
	ErrorCategory = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			this.categories = ko.observableArray();
			this.optionsSelect1 = ko.observableArray();
			this.categorie1 = ko.observable();
			this.optionsSelect2 = ko.observableArray();
			this.categorie2 = ko.observable();
			this.optionsSelect3 = ko.observableArray();
			this.categorie3 = ko.observable();
			this.optionsSelect4 = ko.observableArray();
			this.categorie4 = ko.observable();
			this.willCat2Appear = ko.observable(false);
			this.willCat3Appear = ko.observable(false);
			this.willCat4Appear = ko.observable(false);
			this.categorieID = 0;
			
			this.categorie1.subscribe(function(data){
				console.log(data);
				if(data){
					that.optionsSelect2.removeAll();
					that.getChilds(data, that.optionsSelect2);
					if(that.optionsSelect2().length > 0){
						that.willCat2Appear(true);
					}
					else {
						that.willCat2Appear(false);
					}
				} else{
					that.categorie2(null);
					that.willCat2Appear(false);
				}
				that.updateCatID();
			});
			
			this.categorie2.subscribe(function(data){
				if(data){
					that.optionsSelect3.removeAll();
					that.getChilds(data, that.optionsSelect3);
					if(that.optionsSelect3().length > 0){
						that.willCat3Appear(true);
					}
					else {
						that.willCat3Appear(false);
					}
				}
				else{
					that.categorie3(null);
					that.willCat3Appear(false);
				}
				that.updateCatID();
			});
			
			this.categorie3.subscribe(function(data){
				if(data){
					that.optionsSelect4.removeAll();
					that.getChilds(data, that.optionsSelect4);
					if(that.optionsSelect4().length > 0){
						that.willCat4Appear(true);
					}
					else {
						that.willCat4Appear(false);
					}
				}else {
					that.categorie4(null);
					that.willCat4Appear(false);
				}
				that.updateCatID();
			});
			
			this.categorie4.subscribe(function(data){
				that.updateCatID();
			});
			
		},
		
		getChilds: function(cat, observableArray){
			for(ind in this.categories()){
				if(this.categories()[ind].parent_category == cat.category_id){
					observableArray.push(this.categories()[ind]);
				}
			}
		},
		
		getParent: function(cat){
			if(cat.parent_category == '1' )
				return false;
			for(ind in this.categories()){
				if(this.categories()[ind].category_id == cat.parent_category ){
					return this.categories()[ind];
				}
			}
			return false;
		},
		
		getNoParentsOptions: function(observableArray){
			for(i = 0; i < this.categories().length; i++){
				if(this.categories()[i].parent_category == '1'){
					observableArray.push(this.categories()[i]);
				}
			}
		},
		
		setSelectedCategory: function(catID){
			for(ind in this.categories()){
				if(this.categories()[ind].category_id == catID){
					var parent = this.categories()[ind];
					var nivel = 0;
					var cats = [];
					do{
						cats.unshift(parent);
						parent = this.getParent(parent);
						nivel++;
					}while(parent);
					this.categorie1(cats.shift());
					if(nivel > 1)
						this.categorie2(cats.shift());
					if(nivel > 2)
						this.categorie3(cats.shift());
					if(nivel > 3)
						this.categorie4(cats.shift());
					break;
				}
			}
		},
	
		updateCatID: function(){
			if(this.categorie4()){
				this.categorieID = this.categorie4().category_id;
			}else if(this.categorie3()){
				this.categorieID = this.categorie3().category_id;
			}else if(this.categorie2()){
				this.categorieID = this.categorie2().category_id;
			}else if(this.categorie1()){
				this.categorieID = this.categorie1().category_id;
			}else{
				this.categorieID = 0;
			}
			
		},
		
		loadInfo: function(){
			var that = this;
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			salesmanpost = JSON.stringify(salesman);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_all_error_categories',
				data: {
					salesman: salesmanpost
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//store all the users returned into the 
					var temp = JSON.parse(dataS);
					that.categories.removeAll();
					for(x in temp){
						that.categories.push(temp[x]);
					}
				},
				async: false
			});
			this.optionsSelect1.removeAll();
			this.getNoParentsOptions(this.optionsSelect1);
		}
		
	});
	
	//temporary placed onto the Error script, need to be put into an appropriate script
	//this class will get all users from the database and offers a auto-complete function as well
	UsersAutoComplete = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.users = ko.observableArray();
			this.usersAutocompleteList = ko.observableArray();	// subset of customers
			this.userAutocomplete = ko.observable();		// text input
			this.selectedUser = ko.observable({});
			this.selectedUsers = ko.observableArray();	// subset of customers
			
			this.burl = "http://www.shirt-tailor.net/thepos/";
			
			this.userAutocomplete.subscribe(function(data){
				that.usersAutocompleteList.removeAll();
				if(data.length > 2){
					for(u in that.users()){
						var ufull = that.users()[u].first_name + ' ' + that.users()[u].last_name + ' ' + that.users()[u].username;
						ufull = ufull.toLowerCase();
						if (ufull.indexOf(data.toLowerCase()) != -1) {
							that.usersAutocompleteList.push(that.users()[u]);
						}
					}
				}
			});
			
			this.selectedUser.subscribe(function(data){
				that.usersAutocompleteList.removeAll();
				that.selectedUsers.push(data);
			});
			
			this.removeUser = function(data){
				that.selectedUsers.remove(data);
			};
			
		},
		
		selectUsers : function(usersInfo){
			for(x in usersInfo){
				for(u in this.users()){
					console.log('u' + usersInfo[x].user_id + ' ' + this.users()[u].user_id);
					if(this.users()[u].user_id == usersInfo[x].user_id){
						console.log('aqui');
						this.selectedUser(this.users()[u]);
					}
				}	
			}
		},
		
		loadInfo: function(){
			var that = this;
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			salesmanpost = JSON.stringify(salesman);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_all_users',
				data: {
					salesman: salesmanpost
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//store all the users returned into the 
					var temp = JSON.parse(dataS);
					that.users.removeAll();
					for(x in temp){
						that.users.push(temp[x]);
					}
				},
				async: false
			});
		},
		
		getUsers: function(){
			return this.selectedUser().user_id;
		}
		
	});
	
	CustomerAutoCompleteVM = SimpleControl.extend({
		init: function(DsRegistry, csVM) {
			this._super( DsRegistry );
			var that = this;
			this.custOrder = null;
			this.customerSelectVM = csVM;
			this.customerAutocompleteList = ko.observableArray();	// subset of customers
			this.customerAutocomplete = ko.observable();		// text input
			this.selectedCustomer = ko.observable({});
			
			this.customerAutocomplete.subscribe(function(data){
				that.customerAutocompleteList.removeAll();
				if(data.length > 2){
					for(ind in that.customerSelectVM.customers()){
						var cfull = that.customerSelectVM.customers()[ind].customer_last_name + ' ' +  that.customerSelectVM.customers()[ind].customer_first_name + ' ' + that.customerSelectVM.customers()[ind].customer_email;
						cfull = cfull.toLowerCase();
						if (cfull.indexOf(data.toLowerCase()) != -1) {
							that.customerAutocompleteList.push(that.customerSelectVM.customers()[ind]);
						}
					}
				}
				
			});
			
			this.selectedCustomer.subscribe(function(data){
				that.customerAutocompleteList.removeAll();
				if(that.custOrder)	
					that.custOrder.setCustomerID(data.server_id);
			});
			
		},
		
		setCustomerOrder : function(CustomerOrder){
			this.custOrder = CustomerOrder;
		},
		
		setCustomerSelectVM : function(CustomerSelectVM){
			this.customerSelectVM = CustomerSelectVM;
		},
		
		selectCustomer : function(customerInfo){
			for(ind in this.customerSelectVM.customers()){
				var cfull = this.customerSelectVM.customers()[ind].customer_first_name + ' ' +  this.customerSelectVM.customers()[ind].customer_last_name + '(' + this.customerSelectVM.customers()[ind].customer_email + ')';
				cfull = cfull.toLowerCase();
				if (cfull.indexOf(customerInfo.toLowerCase()) != -1) {
					this.selectedCustomer(this.customerSelectVM.customers()[ind]);
				}
			}
		}
		
	});
	
	CustomerOrdersAutoComplete = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.orGarment = null;
			this.orders = ko.observableArray();
			this.ordersAutocompleteList = ko.observableArray();	// subset of customers
			this.ordersAutocomplete = ko.observable();		// text input
			this.selectedOrder = ko.observable({});
			
			this.ordersAutocomplete.subscribe(function(data){
				that.ordersAutocompleteList.removeAll();
				if(data.length > 0){
					console.log(that.orders().length);
					for(ind in that.orders()){
						var ufull = that.orders()[ind].order_id + ' ' + that.orders()[ind].ordered_date;
						if (ufull.indexOf(data.toLowerCase()) != -1) {
							that.ordersAutocompleteList.push(that.orders()[ind]);
						}
					}
				}
			});
			
			this.selectedOrder.subscribe(function(data){
				that.ordersAutocompleteList.removeAll();
				if(that.orGarment)
					that.orGarment.setOrderID(data.order_id);
			});
			
		},
		
		setOrderGarment: function(OrderGarment){
			this.orGarment = OrderGarment;
		},
		
		setCustomerID: function(custID){
			this.getAllOrders(custID);
		},
		
		getAllOrders: function(custID){
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			console.log(custID);
			data = {
				customer_id: custID
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_customer_orders',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//var temp = JSON.parse(dataS);
					var temp = JSON.parse(dataS);
					console.log(temp);
					that.ordersAutocompleteList.removeAll();
					for(x in temp.orders){
						that.orders.push(temp.orders[x]);
					}
				},
				async: false
			});
		},
		
		setSelectedOrder: function(orderID){
			for(ind in this.orders()){
				if(this.orders()[ind].order_id == orderID){
					this.selectedOrder(this.orders()[ind]);
				}
			}
		}
		
	});
	
	OrderGarmentAutoComplete = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.garments = ko.observableArray();
			this.garmentsAutocompleteList = ko.observableArray();	// subset of customers
			this.garmentsAutocomplete = ko.observable();		// text input
			this.selectedGarment = ko.observable({});
			this.selectedGarments = ko.observableArray();
			
			this.garmentsAutocomplete.subscribe(function(data){
				that.garmentsAutocompleteList.removeAll();
				if(data.length > 0){
					console.log(that.garments().length);
					for(ind in that.garments()){
						var ufull = that.garments()[ind].garment_name + ' ' + that.garments()[ind].garment_fabric;
						ufull = ufull.toLowerCase();
						if (ufull.indexOf(data.toLowerCase()) != -1 && that.selectedGarments().indexOf(that.garments()[ind]) < 0) {
							that.garmentsAutocompleteList.push(that.garments()[ind]);
						}
					}
				}
			});
			
			this.selectedGarment.subscribe(function(data){
				that.garmentsAutocompleteList.removeAll();
				that.selectedGarments.push(data);
			});
			
			this.removeGarments = function(data){
				that.selectedGarments.remove(data);
			};
			
		},
		
		setOrderID: function(orderID){
			this.getAllGarments(orderID);
		},
		
		getAllGarments: function(orderID){
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			data = {
				order_id: orderID
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_order_garments',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					var temp = JSON.parse(dataS);
					that.garmentsAutocompleteList.removeAll();
					for(x in temp){
						that.garments.push(temp[x]);
					}
				},
				async: false
			});
		},
		
		getGarments: function(){
			var garments = '';
			var add = '';
			for(x in this.selectedGarments()){
				if(garments != '' && add == '')
					add = ','
					garments = garments + add + this.selectedGarments()[x].garment_id;
			}
			return garments;
		},
		
		setSelectedGarments: function(garmentID){
			for(ind in this.garments()){
				if(this.garments()[ind].garment_id == garmentID){
					this.selectedGarment(this.garments()[ind]);
				}
			}
		}
	});
	
	ErrorReplyVM = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.selectedError = 0;
			this.message = ko.observable('');
		},
		
		cancelAdd: function(){
			$.mobile.changePage('#errorReport');
		},
		
		addReply: function(){
			var that = this;
			this.burl = "http://www.shirt-tailor.net/thepos/";
			//alert(this.selectedError);
			//collect the user data and prepare ir to send.
			data = {
				error_id: that.selectedError,
				message: this.message()
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/new_reply_for_error',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					$.mobile.changePage('#errorReport');
				},
				async: false
			});
		},
		
		//set the error that the class will work with
		setSelectedError: function(errorID){
			this.selectedError = errorID;
		}
		
	});
	
});