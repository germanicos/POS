define(['jquery', 'knockout', 'base'], function($, ko) {
FitlinesPants = SimpleControl.extend({
	init: function(DsRegistry, oI, tableindex){//, fromcached) {
		var self = this;
		this._super( DsRegistry );
		this.orderItemMirror = oI;
		
	//	if(fromcached == true){
			try{
				this.id     			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].id);					//= ko.observable('');		this.id.subscribe(function() { self.flushModel(); });
				this.pantsName     		= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsName);					//= ko.observable('');		this.pantsName.subscribe(function() { self.flushModel(); });
				this.pantsBackCrotch   	= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsBackCrotch);						//= ko.observable('');		this.pantsBackCrotch.subscribe(function() { self.flushModel(); });
				this.pantsCuff			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsCuff);						//= ko.observable('');		this.pantsCuff.subscribe(function() { self.flushModel(); });		
				this.pantsFrontCrotch	= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsFrontCrotch);					//= ko.observable('');		this.pantsFrontCrotch.subscribe(function() { self.flushModel(); });
				this.pantsHip			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsHip);						//= ko.observable('');		this.pantsHip.subscribe(function() { self.flushModel(); });		
				this.pantsId			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsId);						//= ko.observable('');		this.pantsId.subscribe(function() { self.flushModel(); });
				this.pantsKnee			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsKnee);					//= ko.observable('');		this.pantsKnee.subscribe(function() { self.flushModel(); });
				this.pantsLength		= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsLength);				//= ko.observable('');		this.pantsLength.subscribe(function() { self.flushModel(); });
				this.pantsThigh			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsThigh);			//= ko.observable('');		this.pantsThigh.subscribe(function() { self.flushModel(); });
				this.pantsWaist			= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsWaist);				//= ko.observable('');		this.pantsWaist.subscribe(function() { self.flushModel(); });
				this.pantsFitlineComments		= ko.observable(dsRegistry.getDatasource('fitlinesPantsDS').getStore()[tableindex].pantsComments);			//= ko.observable('');		this.pantsFitlineComments.subscribe(function() { self.flushModel(); });
			}catch(e){
				this.id     			= ko.observable('');
				this.pantsName     		= ko.observable('');
				this.pantsBackCrotch   	= ko.observable('');
				this.pantsCuff			= ko.observable('');		
				this.pantsFrontCrotch	= ko.observable('');
				this.pantsHip			= ko.observable('');		
				this.pantsId			= ko.observable('');
				this.pantsKnee			= ko.observable('');
				this.pantsLength		= ko.observable('');
				this.pantsThigh			= ko.observable('');
				this.pantsWaist			= ko.observable('');
				this.pantsFitlineComments	= ko.observable('');
			}	
	
		this.selectedGarment	= ko.observable({name: "none"});
	},
	
	test: function() {
		console.log(this.orderItemMirror);
	},

	enableVisibility: function(){
    var viewModel = {
        shouldShowMessage: ko.observable(false) // Message initially visible
    };
	},
/*
	garmentsList: function() { return ko.computed(function() {
		var gL = this.orderItemMirror.garmentsList();
		result =  [
			{
			  "name":"jacket",
			  "img":"gfx/measurements/jacket_m.png",
			  "count": gL[0].count()
			},
			{
			  "name":"pants",
			  "img":"gfx/measurements/pants_m.png",
			  "count": gL[1].count()
			},
			{
			  "name":"suit",
			  "img":"gfx/measurements/suit_m.png",
			  "count": gL[2].count()
			},
			{
			  "name":"vest",
			  "img":"gfx/measurements/vest_m.png",
			  "count": gL[3].count()
			},
			{
			  "name":"shirt",
			  "img":"gfx/measurements/shirt_m.png",
			  "count": gL[4].count()
			}
		];
		return result;
		}, this);
	},
	*/
	flushModel: function() {
		var tModel = {}; 
		tModel.id					= this.id();
		tModel.pantsName 			= this.pantsName();
		tModel.pantsHip				= this.pantsHip();
		tModel.pantsBackCrotch	    = this.pantsBackCrotch();
		tModel.pantsFrontCrotch		= this.pantsFrontCrotch();
		tModel.pantsCuff			= this.pantsCuff();		
		tModel.pantsId				= this.pantsId();
		tModel.pantsFrontCrotch		= this.pantsFrontCrotch();

		tModel.pantsKnee		    = this.pantsKnee();
		tModel.pantsLength	   		= this.pantsLength();
		tModel.pantsThigh			= this.pantsThigh();
		
		tModel.pantsWaist			= this.pantsWaist();
		tModel.pantsFitlineComments		= this.pantsFitlineComments();
		
		return tModel; 
	},

	clear: function() {
		this.id('');
		this.pantsName('');
		this.pantsBackCrotch('');
		this.pantsCuff('');		
		this.pantsFrontCrotch('');
		this.pantsHip('');
		this.pantsId('');
		
		this.pantsKnee('');
		this.pantsLength('');
		this.pantsThigh('');
		
		this.pantsWaist('');
		this.pantsFitlineComments('');
		
		this.selectedGarment({name: "none"});
	}
	
})
});