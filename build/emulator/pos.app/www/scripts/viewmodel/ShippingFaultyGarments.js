define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingFaultyGarments = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		this.ShippingFaultyGarmentsData     = [];
		this.ShippingFaultyGarmentsDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);

		this.workflow    = ko.observableArray([
			{id: 0, target: "#completions",  completed: "false", caption: "Completion", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			$.mobile.changePage(data.target);
			$('.cloneDialog').remove(); 
			that.renderShippingFaultyGarments();
		});
		
		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});
		
		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});

		this.completion	= ko.observable(0);
		this.variantNameShippingFaultyGarments  = ko.observableArray([{id: 0, title: "ShippingFaultyGarments 1"}]);
		this.selectedVariantShippingFaultyGarments  = ko.observable(this.variantNameShippingFaultyGarments()[0]);	
		this.selectedVariantShippingFaultyGarments.subscribe(function(data) {
			that.selectedVariantShippingFaultyGarments();
			that.renderShippingFaultyGarments();			
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});


		this.WhoseFaultList = ko.observableArray(["Manufacturer", "Fabric supplier", "Salesman / measurer", "HQ error"]);
		this.WhoseFault = ko.observable('');
		this.WhoseFault.subscribe(function(data) {
			if(data != undefined){
				that.ShippingFaultyGarmentsData[ that.getRow(that.selectedVariantShippingFaultyGarments().id)  ].WhoseFault = data;
				that.flushModel();
			}		
		});
				
		this.FaultDescription = ko.observable('');
		this.FaultDescription.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyGarmentsData[ that.getRow(that.selectedVariantShippingFaultyGarments().id)  ].FaultDescription = data;
				that.flushModel();
			}	
		});
		
		this.VideoUrl = ko.observable('');
		this.VideoUrl.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyGarmentsData[ that.getRow(that.selectedVariantShippingFaultyGarments().id)  ].VideoUrl = data;
				that.flushModel();
			}	
		});

		this.VideoComments = ko.observable('');
		this.VideoComments.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyGarmentsData[ that.getRow(that.selectedVariantShippingFaultyGarments().id)  ].VideoComments = data;
				that.flushModel();
			}	
		});

///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var photosarray = [];
			var images = localStorage.getItem('ShippingFaultyGarmentsPhotos');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('ShippingFaultyGarmentsPhotos') != null){
				if(localStorage.getItem('ShippingFaultyGarmentsPhotos').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			var customerString = orderItem.custSelectVM.selectedCustomer();
	
			if(this.FaultDescription().trim() == ""){
				customAlert("Please enter a description for the fault");
			}else if(images.length  == 0){
				customAlert("Please upload a fabric image");
			}else if(this.VideoUrl().trim() == ""){
				customAlert("Please enter a video url");
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					customer: customerString,
					date: dateToPost,
					fault_description: this.FaultDescription(),
					video: this.VideoUrl(),
					video_comments: this.VideoComments(),
					images: photosarray,
					tags: tagsarray
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				$.post(BUrl + 'shippings/faulty_garments_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Succesfully added");
		                $.mobile.changePage('#shipping_main');
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
/*
	nextStep: function() {
		var tWork = this.workflow();
		for (var ind in tWork ) {
				if (!tWork[ind].completed) {
				this.currentStep( tWork[ind] );
				return;
			}
		}
	},*/
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingFaultyGarments"  : this.ShippingFaultyGarmentsData
		}, true);
		this.renderShippingFaultyGarments();
	},

	getVariant: function(id) {
		var toreturn = this.ShippingFaultyGarmentsData[0];
		for (var ind in this.ShippingFaultyGarmentsData) {
			if ( this.ShippingFaultyGarmentsData[ind].variantId == id  ) {
				toreturn = this.ShippingFaultyGarmentsData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingFaultyGarmentsData) {
			if ( this.ShippingFaultyGarmentsData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantShippingFaultyGarment: function(name) {
		if(this.variantNameShippingFaultyGarments()[0].title != "ShippingFaultyGarments 1"){
			this.ShippingFaultyGarmentsDataAID += 1;
			var newid = this.ShippingFaultyGarmentsDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFaultyGarments().id ) )  ); //CLONE Object
			this.variantNameShippingFaultyGarments.push({title: vname, id: this.ShippingFaultyGarmentsDataAID});
			tObj.variantId = this.ShippingFaultyGarmentsDataAID;		
			this.flushModel();
		}else{
			this.variantNameShippingFaultyGarments()[0].title = name;
		}
	},	
	
	addVariantsShippingFaultyGarments: function() {
		for(var x in orderItem.ShippingFaultyGarmentsVM.ShippingFaultyGarmentsData){
		//	console.log("addVariantsShippingFaultyGarments " + x);
			if( x != 0){
				this.ShippingFaultyGarmentsDataAID += 1;
				var vname = "ShippingFaultyGarments " + x; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFaultyGarments().id ) )  ); //CLONE Object
				this.variantNameShippingFaultyGarments.push({title: vname, id: this.ShippingFaultyGarmentsDataAID});
				tObj.variantId = this.ShippingFaultyGarmentsDataAID;		
				this.flushModel();
			}else{
				var vname = "ShippingFaultyGarments " + x; 
				//var vname = orderItem.ShippingFaultyGarmentsVM.ShippingFaultyGarmentsData[x].pant;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFaultyGarments().id ) )  ); //CLONE Object
				this.variantNameShippingFaultyGarments()[x].title = vname;
				tObj.variantId = this.ShippingFaultyGarmentsDataAID;		
				this.flushModel();
			}
		}
	},

	digestData: function(data) {
		this.ShippingFaultyGarmentsData  = data.ShippingFaultyGarments;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingFaultyGarments();
	},

	renderShippingFaultyGarments: function() {	
		//Get selected Variant
console.log("renderShippingFaultyGarments");		
		try{
			var tData = this.getVariant(this.selectedVariantShippingFaultyGarments().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.WhoseFault)    	!= "undefined") {this.WhoseFault(tData.WhoseFault);}
				if (typeof(tData.FaultDescription)    != "undefined") {this.FaultDescription(tData.FaultDescription);}
				if (typeof(tData.VideoUrl)   	!= "undefined") {this.VideoUrl(tData.VideoUrl);}
				if (typeof(tData.VideoComments)    != "undefined") {this.VideoComments(tData.VideoComments);}
			}
		}catch(e){
			;
		}

	}
});

defShippingFaultyGarments = SimpleDatasource.extend({
	init: function( name, dsRegistry) {
	//	if(data == null || data == undefined){
			var ShippingFaultyGarments = {};
			ShippingFaultyGarments.variantId        = 0;
		
			ShippingFaultyGarments.WhoseFault = '';
			ShippingFaultyGarments.FaultDescription = '';
			ShippingFaultyGarments.VideoUrl = '';
			ShippingFaultyGarments.VideoComments = '';
			

			var iShippingFaultyGarments = {
				ShippingFaultyGarments: []
			};
			iShippingFaultyGarments.ShippingFaultyGarments.push(ShippingFaultyGarments);
			this._super(name, iShippingFaultyGarments, dsRegistry);
	//	}	
	}
});


//END DEFINE CLOSURE
});
