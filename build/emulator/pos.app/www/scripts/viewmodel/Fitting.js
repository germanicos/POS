define(['jquery', 'knockout', 'base'], function($, ko) {
Fitting = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		this.FittingData     = [];
		this.FittingDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);
		
		
		
			this.NoTagsConfirm = function() {
       			text = "Are you sure you want to continue without adding notes on the image?";				
				document.getElementById("dialogtext").innerHTML = text;
				dialog = document.getElementById('dialog');
				dialog.style.display = "block";
        		return true;
    		};
    		
    		this.NoVideoConfirm = function() {
       			text = "Are you sure you want to continue without adding a fitting video?";				
				document.getElementById("dialogtext").innerHTML = text;
				dialog = document.getElementById('dialog');
				dialog.style.display = "block";
        		return true;
    		};
    		
    		this.buttonNoAccept = function(){
    			var cs = that.currentStep().id;			
				var whichGarment = that.selectedVariantFitting().id;
				if(cs == 4){
					that.NoFrontTags(false);
					that.SkippedFrontTags(false);
				}else if(cs == 5){
					that.NoSideTags(false);
					that.SkippedSideTags(false);
				}else if(cs == 6){
					that.NoBackTags(false);
					that.SkippedBackTags(false);
				}else if(cs == 7){
					that.NoVideo(false);
					that.SkippedVideo(false);
				}
				
    			dialog = document.getElementById('dialog');
				dialog.style.display = "none";
    		}
    		this.buttonYesAccept = function(){
console.log("buttonYesAccept");
				var cs = that.currentStep().id;			
				var whichGarment = that.selectedVariantFitting().id;
				if(cs == 4){
					that.NoFrontTags(true);
					that.SkippedFrontTags(false);
					that.workflow()[4].completed = "true";
				}else if(cs == 5){
					that.NoSideTags(true);
					that.SkippedSideTags(false);
					that.workflow()[5].completed = "true";
				}else if(cs == 6){
					that.NoBackTags(true);
					that.SkippedBackTags(false);
					that.workflow()[6].completed = "true";
				}else if(cs == 7){
					that.NoVideo(true);
					that.SkippedVideo(false);
					that.workflow()[7].completed = "true";
				}
    			dialog = document.getElementById('dialog');
				dialog.style.display = "none";
				
				this.goToFirstUncompletedStep();		
    		}		
		
		
		this.nextStep = function() {

			var cs = that.currentStep().id;			
			var whichGarment = that.selectedVariantFitting().id;
						
			if(cs == 0){
				var photo = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);
				that.workflow()[cs].completed = "false";				
				if(photo != null){
					that.workflow()[cs].completed = "true";
				}else{
					customAlert("Please upload a front image");
				}	
			}else if(cs == 1){
				var photo = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
				that.workflow()[cs].completed = "false";
				if(photo != null){
					that.workflow()[cs].completed = "true";
				}else{
					customAlert("Please upload a side image");
				}	
			}else if(cs == 2){
				var photo = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
				that.workflow()[cs].completed = "false";
				if(photo != null){
					that.workflow()[cs].completed = "true";
				}else{
					customAlert("Please upload a back image");
				}	
			}else if(cs == 3){
				that.AdditionalImagesStepComplete(true);
				that.workflow()[cs].completed = "true";
			}else if(cs == 4){
				that.SkippedFrontTags(false);
				var tags = localStorage.getItem('FittingFrontImageTags' + whichGarment);		
				that.workflow()[cs].completed = "false";
				if(tags != null){ 
					if(tags.length > 5){
						that.workflow()[cs].completed = "true";
					}else{
						this.NoTagsConfirm();	
					}	
				}else{
					this.NoTagsConfirm();
				}	
			}else if(cs == 5){
				that.SkippedSideTags(false);
				var tags = localStorage.getItem('FittingSideImageTags' + whichGarment);
				that.workflow()[cs].completed = "false";
				if(tags != null){ 
					if(tags.length > 5){
						that.workflow()[cs].completed = "true";
					}else{
						this.NoTagsConfirm();	
					}	
				}else{
					this.NoTagsConfirm();
				}	
			}else if(cs == 6){
				that.SkippedBackTags(false);
				var tags = localStorage.getItem('FittingBackImageTags' + whichGarment);
				that.workflow()[cs].completed = "false";
				if(tags != null){ 
					if(tags.length > 5){
						that.workflow()[cs].completed = "true";
					}else{
						this.NoTagsConfirm();
					}	
				}else{
					this.NoTagsConfirm();
				}
			}else if(cs == 7){
			//	that.SkippedVideo(false);
			//	that.workflow()[cs].completed = "false";	
			//	that.workflow()[cs].completed = "true";
			
				that.SkippedVideo(false);
				that.workflow()[cs].completed = "false";
				if(that.FittingVideo().length > 2){ 
					that.workflow()[cs].completed = "true";
				}else{
					this.NoVideoConfirm();
				}
			}else if(cs == 8){
				that.MeasurementsStepComplete(true);
				that.workflow()[cs].completed = "true";
			}
			
			if(that.workflow()[cs].completed == "true"){
				this.goToFirstUncompletedStep();
				
			}else{
				that.currentStep(that.workflow()[cs] );
			}
		}
		
		this.goToFirstUncompletedStep = function(){
			var cs = that.currentStep().id;	
			var foundnotcompletedstep = false;
			for(var x = 0; x < that.workflow().length; x++){
			//for(var x = cs+1; x < that.workflow().length; x++){
				if(that.workflow()[x].completed == "false"){			
					that.currentStep(that.workflow()[x] );
					foundnotcompletedstep = true;
					break;
				}
			}
			if(foundnotcompletedstep == false){
				that.currentStep(that.workflow()[cs + 1] );
			}	
		}

		this.checkSteps = function() {
			
			var whichGarment = that.selectedVariantFitting().id;
console.log("checkSteps " + whichGarment);
			photo = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);
			that.workflow()[0].completed = "false";
			if(photo != null){
				that.workflow()[0].completed = "true";
			}
			photo = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
			that.workflow()[1].completed = "false";
			if(photo != null){
				that.workflow()[1].completed = "true";
			}
			photo = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
			that.workflow()[2].completed = "false";
			if(photo != null){
				that.workflow()[2].completed = "true";
			}

			that.workflow()[3].completed = "false";
			if( that.AdditionalImagesStepComplete() == true ){
				that.workflow()[3].completed = "true";
			}
			/*	
			if(that.currentStep().id >= 3 ){
				that.workflow()[3].completed = "true";
			}else{
				var c = localStorage.getItem("fittingindex");
				if(c != -1){
					that.workflow()[3].completed = "true";	
				}	
			}*/

			that.workflow()[4].completed = "false";
			if( that.NoFrontTags() == true ){
				that.workflow()[4].completed = "true";
			}else if( that.SkippedFrontTags() == true){
				that.workflow()[4].completed = "pending";
			}else{
				tags = localStorage.getItem('FittingFrontImageTags' + whichGarment);		
				if(tags != null){ 
					if(tags.length > 5){
						that.workflow()[4].completed = "true";
					}
				}
			}	
			that.workflow()[5].completed = "false";
			if( that.NoSideTags() == true ){
				that.workflow()[5].completed = "true";
			}else if( that.SkippedSideTags() == true){
				that.workflow()[5].completed = "pending";
			}else{
				tags = localStorage.getItem('FittingSideImageTags' + whichGarment);		
				if(tags != null){ 
					if(tags.length > 5){
						that.workflow()[5].completed = "true";
					}
				}
			}		
			that.workflow()[6].completed = "false";
			if( that.NoBackTags() == true ){
				that.workflow()[6].completed = "true";
			}else if( that.SkippedBackTags() == true){
				that.workflow()[6].completed = "pending";
			}else{
				tags = localStorage.getItem('FittingBackImageTags' + whichGarment);		
				if(tags != null){ 
					if(tags.length > 5){
						that.workflow()[6].completed = "true";
					}
				}
			}	
			/*that.workflow()[6].completed = "false";
			if( that.SkippedVideo() == true){
				that.workflow()[6].completed = "true";
			}else if(this.FittingVideo().length > 2){	
				that.workflow()[6].completed = "true";
			}*/
			that.workflow()[7].completed = "false";
			if( that.NoVideo() == true ){
				that.workflow()[7].completed = "true";
			}else if( that.SkippedVideo() == true){
				that.workflow()[7].completed = "pending";
			}else if(this.FittingVideo().length > 2){ 
				that.workflow()[7].completed = "true";
			}
			
			that.workflow()[8].completed = "false";
			if( that.MeasurementsStepComplete() == true ){
				that.workflow()[8].completed = "true";
			}
			/*
			if(that.currentStep().id >= 8 ){
				that.workflow()[8].completed = "true";
			}else{
				var c = localStorage.getItem("fittingindex");
				if(c != -1){
					that.workflow()[8].completed = "true";	
				}
			}
			*/
			
			that.workflow()[9].completed = "false";
			if(that.FittingId() != '' && that.FittingTailor() != undefined && that.FittingDeliveryMonth() != undefined && that.FittingDeliveryMonth() != "" 
			&& that.FittingDeliveryDay() != undefined && that.FittingDeliveryDay() != ""){
				that.workflow()[9].completed = "true";
			}
			
			return '';
		}		
		
		
		this.skipStep = function() {
			var whichGarment = that.selectedVariantFitting().id;
			var cs = that.currentStep().id;
			that.workflow()[cs].completed = "true";
console.log("skipStep " + cs);
			if(cs == 4){
				localStorage.removeItem('FittingFrontImageTags' + whichGarment);
				that.NoFrontTags(false);
				that.SkippedFrontTags(true);
			}else if(cs == 5){
				localStorage.removeItem('FittingSideImageTags' + whichGarment);
				that.NoSideTags(false);
				that.SkippedSideTags(true);
			}else if(cs == 6){
				localStorage.removeItem('FittingBackImageTags' + whichGarment);
				that.NoBackTags(false);
				that.SkippedBackTags(true);
			}else if(cs == 7){
				that.FittingVideo("");
				that.FittingVideoNotes("");
				that.SkippedVideo(true);
			}
			this.goToFirstUncompletedStep();
		}
		
		this.goToNextNotSubmittedFitting = function() {
			console.log("that.FittingData.length() " + this.FittingData.length);
			var found = false;
			for(var x = 1; x < this.FittingData.length; x++){
console.log("garment " + x + "  FittingId: " + this.FittingData[x].FittingId);				
				if(this.FittingData[x].FittingId == ''){
					found = true;
					//this.currentStep( that.workflow()[0] );
					this.selectedVariantFitting(this.variantNameFitting()[x]);
					this.goToFirstUncompletedStep();
					break;
				}
			}
			if(found == false){
				$.mobile.changePage('#customerGarmentsFittingsList');
				//var whichGarment = this.selectedVariantFitting().id;
				//this.selectedVariantFitting(this.variantNameFitting()[whichGarment]);
			}
		}		

		this.workflow    = ko.observableArray([
			{id: 0, target: "#fittings",  completed: "false", caption: "Fitting", title: "UPLOAD FRONT IMAGE", myclass: "fimage" },
			{id: 1, target: "#fittings",  completed: "false", caption: "Fitting", title: "UPLOAD SIDE IMAGE", myclass: "fimage" },
			{id: 2, target: "#fittings",  completed: "false", caption: "Fitting", title: "UPLOAD BACK IMAGE", myclass: "fimage" },
			{id: 3, target: "#fittings",  completed: "false", caption: "Fitting", title: "ADDITIONAL IMAGES", myclass: "fimage" },
			{id: 4, target: "#fittings",  completed: "false", caption: "Fitting", title: "NOTES FRONT IMAGE", myclass: "fnote" },
			{id: 5, target: "#fittings",  completed: "false", caption: "Fitting", title: "NOTES SIDE IMAGE", myclass: "fnote" },
			{id: 6, target: "#fittings",  completed: "false", caption: "Fitting", title: "NOTES BACK IMAGE", myclass: "fnote" },
			{id: 7, target: "#fittings",  completed: "false", caption: "Fitting", title: "UPLOAD VIDEO URL", myclass: "fvideo" },
			{id: 8, target: "#fittings",  completed: "false", caption: "Fitting", title: "ADJUST MEASUREMENTS", myclass: "fmeasure" },
			{id: 9, target: "#fittings",  completed: "false", caption: "Fitting", title: "ASSIGN TAILOR", myclass: "ftailor" },
		]);


		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			$.mobile.changePage(data.target);
			$('.cloneDialog').remove();
			that.renderFitting(); 
		});

		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});


		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});


		this.completion       = ko.observable(0);
		this.variantNameFitting = ko.observableArray([{id: 0, title: "Fitting 1"}]);
		this.selectedVariantFitting = ko.observable(this.variantNameFitting()[0]);	
		this.selectedVariantFitting.subscribe(function(data) {			
			that.selectedVariantFitting();
		//	$.mobile.changePage("#fittings");
		//	$('.cloneDialog').remove();
			that.renderFitting();
			
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});
		

		this.FittingCustomer = ko.observable('');
		this.FittingCustomer.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingCustomer = data;
			that.flushModel();
		});

		this.FittingGarment = ko.observable('');
		this.FittingGarment.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingGarment = data;
			that.flushModel();
		});
		
		this.FittingGarmentFabric = ko.observable('');
		this.FittingGarmentFabric.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingGarmentFabric = data;
			that.flushModel();
		});

		this.FittingId = ko.observable('');
		this.FittingId.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingId = data;
			that.flushModel();
		});
		
		this.FittingOrderId = ko.observable('');
		this.FittingOrderId.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingOrderId = data;
			that.flushModel();
		});
		
		this.FittingOrderProductId = ko.observable('');
		this.FittingOrderProductId.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingOrderProductId = data;
			that.flushModel();
		});
		
		this.FittingTailors = ko.observable(that.dsRegistry.getDatasource('tailorsDS').getStore());
		this.FittingTailor = ko.observable('');
		this.FittingTailor.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor = data;
			that.flushModel();
		});
		this.FittingDeliveryDay = ko.observable('');
		this.FittingDeliveryDay.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay = data;
			that.flushModel();
		});
		this.FittingDeliveryMonth = ko.observable('');
		this.FittingDeliveryMonth.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth = data;
			that.flushModel();
		});
		this.FittingTailorNotes = ko.observable('');
		this.FittingTailorNotes.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailorNotes = data;
			that.flushModel();
		});

		this.FittingVideo = ko.observable('');
		this.FittingVideo.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingVideo = data;
			that.flushModel();
		});
		
		this.FittingMeasurements = ko.observable('');
		this.FittingMeasurements.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingMeasurements = data;
			that.flushModel();
		});
		
		this.OrderMeasurements = ko.observable('');
		this.OrderMeasurements.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].OrderMeasurements = data;
			that.flushModel();
		});

		this.SkippedVideo = ko.observable(false);
		this.SkippedVideo.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].SkippedVideo = data;
			that.flushModel();
		});
		this.NoVideo = ko.observable(false);
		this.NoVideo.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].NoVideo = data;
			that.flushModel();
		});
		
		this.SkippedFrontTags = ko.observable(false);
		this.SkippedFrontTags.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].SkippedFrontTags = data;
			that.flushModel();
		});
		this.SkippedSideTags = ko.observable(false);
		this.SkippedSideTags.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].SkippedSideTags = data;
			that.flushModel();
		});
		this.SkippedBackTags = ko.observable(false);
		this.SkippedBackTags.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].SkippedBackTags = data;
			that.flushModel();
		});
		this.NoFrontTags = ko.observable(false);
		this.NoFrontTags.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].NoFrontTags = data;
			that.flushModel();
		});
		this.NoSideTags = ko.observable(false);
		this.NoSideTags.subscribe(function(data) {		
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].NoSideTags = data;
			that.flushModel();
		});
		this.NoBackTags = ko.observable(false);
		this.NoBackTags.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].NoBackTags = data;
			that.flushModel();
		});
		this.isActive = ko.observable(true);
		this.isActive.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].isActive = data;
			that.flushModel();
		});
		this.isCompleted = ko.observable(false);
		this.isCompleted.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].isCompleted = data;
			that.flushModel();
		});
		
		this.AdditionalImagesStepComplete = ko.observable(false);
		this.AdditionalImagesStepComplete.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].AdditionalImagesStepComplete = data;
			that.flushModel();
		});
		this.MeasurementsStepComplete = ko.observable(false);
		this.MeasurementsStepComplete.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].MeasurementsStepComplete = data;
			that.flushModel();
		});
		
		/*
		this.FittingVideoNotes = ko.observableArray();
		this.FittingVideoNotes.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingVideoNotes = data;
			that.flushModel();
		});
		*/
		this.FittingVideoNotes = ko.observable('');
		this.FittingVideoNotes.subscribe(function(data) {
			that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingVideoNotes = data;
			that.flushModel();
		});
				
		this.FittingVideoViewModel = function(thepath, thenotes){
    		var self = this;
    		this.path = ko.observable(thepath);
	        this.notes = ko.observable(thenotes);
		};

/*
		this.FittingVideoNotesViewModel = function(thenotes){
    			var self = this;
           		this.note = ko.observable(thenotes);
		};		
	   	this.addVideoNote = function(){
       		var checkfailed = false;
       		for(var pos = 0; pos < this.FittingVideoNotes().length; pos++){        		
        		if(this.FittingVideoNotes()[pos].note() = undefined || this.FittingVideoNotes()[pos].note().length == 0){
       				checkfailed = true;
       				break;
       			}	
       		}
       		if(checkfailed == false){
       			this.FittingVideoNotes.push(new this.FittingVideoNotesViewModel());
       		}
    	};
    	this.removeVideoNote = function(pos){ 
       		this.FittingVideoNotes.remove(that.FittingVideoNotes()[pos]);
    	}; 
*/
 
		this.MeasurementChangesText = function() {
console.log("that.selectedVariantFitting().id: " + that.selectedVariantFitting().id);			
			var measurements = orderItem.fittingsDS.getStore().Fitting[that.selectedVariantFitting().id].FittingMeasurements;
			var a = "";
			if(measurements.fittings_jacket_measurements_chest != undefined){
				if(measurements.fittings_jacket_measurements_chest != "" 	&& measurements.fittings_jacket_measurements_chest != 0){		a += "Chest " + measurements.fittings_jacket_measurements_chest + "<br/>";	}
				if(measurements.fittings_jacket_measurements_stomach != "" 	&& measurements.fittings_jacket_measurements_stomach != 0){		a += "Stomach " + measurements.fittings_jacket_measurements_stomach + "<br/>";	}
				if(measurements.fittings_jacket_measurements_hips != "" 	&& measurements.fittings_jacket_measurements_hips != 0){		a += "Hips " + measurements.fittings_jacket_measurements_hips + "<br/>";	}
				if(measurements.fittings_jacket_measurements_front != "" 	&& measurements.fittings_jacket_measurements_front != 0){		a += "Front " + measurements.fittings_jacket_measurements_front + "<br/>";	}						
				if(measurements.fittings_jacket_measurements_back != "" 	&& measurements.fittings_jacket_measurements_back != 0){		a += "Back " + measurements.fittings_jacket_measurements_back + "<br/>";	}
				if(measurements.fittings_jacket_measurements_full_shoulder != "" && measurements.fittings_jacket_measurements_full_shoulder != 0){	a += "Full Shoulder " + measurements.fittings_jacket_measurements_full_shoulder + "<br/>";	}
				if(measurements.fittings_jacket_measurements_sleeve_left != "" 	 && measurements.fittings_jacket_measurements_sleeve_left != 0){	a += "Left Sleeve " + measurements.fittings_jacket_measurements_sleeve_left + "<br/>";	}
				if(measurements.fittings_jacket_measurements_sleeve_right != ""  && measurements.fittings_jacket_measurements_sleeve_right != 0){	a += "Right Sleeve " + measurements.fittings_jacket_measurements_sleeve_right + "<br/>";	}
				if(measurements.fittings_jacket_measurements_neck != "" 	&& measurements.fittings_jacket_measurements_neck != 0){		a += "Neck " + measurements.fittings_jacket_measurements_neck + "<br/>";	}
				if(measurements.fittings_jacket_measurements_bicep != ""	&& measurements.fittings_jacket_measurements_bicep != 0){		a += "Bicep " + measurements.fittings_jacket_measurements_bicep + "<br/>";	}
				if(measurements.fittings_jacket_measurements_wrist != "" 	&& measurements.fittings_jacket_measurements_wrist != 0){		a += "Wrist " + measurements.fittings_jacket_measurements_wrist + "<br/>";	}
				if(measurements.fittings_jacket_measurements_fore_arms != "" 	&& measurements.fittings_jacket_measurements_fore_arms != 0){		a += "Forearm " + measurements.fittings_jacket_measurements_fore_arms + "<br/>";	}
				if(measurements.fittings_jacket_measurements_length != "" 	 	&& measurements.fittings_jacket_measurements_length != 0){			a += "Jacket Length " + measurements.fittings_jacket_measurements_length + "<br/>";	}
				if(measurements.fittings_jacket_measurements_lapel_length != "" && measurements.fittings_jacket_measurements_lapel_length != 0){	a += "Lapel Length " + measurements.fittings_jacket_measurements_lapel_length + "<br/>";	}
				if(measurements.fittings_jacket_measurements_zero_point != "" 	&& measurements.fittings_jacket_measurements_zero_point != 0){		a += "Zero Point " + measurements.fittings_jacket_measurements_zero_point + "<br/>";	}
				if(measurements.fittings_jacket_measurements_front_shoulder_to_zero_point != "" && measurements.fittings_jacket_measurements_front_shoulder_to_zero_point != 0){	a += "Front to Zero " + measurements.fittings_jacket_measurements_front_shoulder_to_zero_point + "<br/>";	}
				if(measurements.fittings_jacket_measurements_back_shoulder_to_zero_point != "" && measurements.fittings_jacket_measurements_back_shoulder_to_zero_point != 0){		a += "Back to Zero " + measurements.fittings_jacket_measurements_back_shoulder_to_zero_point + "<br/>";	}
			}
			if(measurements.fittings_pants_measurements_pantwaist != undefined){
				if(measurements.fittings_pants_measurements_pantwaist != "" && measurements.fittings_pants_measurements_pantwaist != 0){	a += "Pant Waist " + measurements.fittings_pants_measurements_pantwaist + "<br/>";	}
				if(measurements.fittings_pants_measurements_hips != "" 		&& measurements.fittings_pants_measurements_hips != 0){			a += "Hips " + measurements.fittings_pants_measurements_hips + "<br/>";	}
				if(measurements.fittings_pants_measurements_thigh != "" 	&& measurements.fittings_pants_measurements_thigh != 0){		a += "Thigh " + measurements.fittings_pants_measurements_thigh + "<br/>";	}
				if(measurements.fittings_pants_measurements_cuffs != "" 	&& measurements.fittings_pants_measurements_cuffs != 0){		a += "Cuffs " + measurements.fittings_pants_measurements_cuffs + "<br/>";	}
				if(measurements.fittings_pants_measurements_length != "" 	&& measurements.fittings_pants_measurements_length != 0){		a += "Pant Length " + measurements.fittings_pants_measurements_length + "<br/>";	}
				if(measurements.fittings_pants_measurements_front_crotch != "" && measurements.fittings_pants_measurements_front_crotch != 0){	a += "Front Crotch " + measurements.fittings_pants_measurements_front_crotch + "<br/>";	}
				if(measurements.fittings_pants_measurements_zipper != "" 	&& measurements.fittings_pants_measurements_zipper != 0){		a += "Zipper Length " + measurements.fittings_pants_measurements_zipper + "<br/>";	}
				if(measurements.fittings_pants_measurements_crotch != "" 	&& measurements.fittings_pants_measurements_crotch != 0){		a += "Crotch " + measurements.fittings_pants_measurements_crotch + "<br/>";	}
				if(measurements.fittings_pants_measurements_inseam != "" 	&& measurements.fittings_pants_measurements_inseam != 0){		a += "Inseam " + measurements.fittings_pants_measurements_inseam + "<br/>";	}
			}
			if(measurements.fittings_shirt_measurements_chest != undefined){
				if(measurements.fittings_shirt_measurements_chest != "" 	&& measurements.fittings_shirt_measurements_chest != 0){		a += "Chest " + measurements.fittings_shirt_measurements_chest + "<br/>";	}
				if(measurements.fittings_shirt_measurements_stomach != "" 	&& measurements.fittings_shirt_measurements_stomach != 0){		a += "Stomach " + measurements.fittings_shirt_measurements_stomach + "<br/>";	}
				if(measurements.fittings_shirt_measurements_full_shoulder != "" && measurements.fittings_shirt_measurements_full_shoulder != 0){		a += "Full Shoulder " + measurements.fittings_shirt_measurements_full_shoulder + "<br/>";	}
				if(measurements.fittings_shirt_measurements_sleeve_left != "" 	&& measurements.fittings_shirt_measurements_sleeve_left != 0){		a += "Right Sleeve " + measurements.fittings_shirt_measurements_sleeve_left + "<br/>";	}
				if(measurements.fittings_shirt_measurements_sleeve_right != "" 	&& measurements.fittings_shirt_measurements_sleeve_right != 0){		a += "Left Sleeve " + measurements.fittings_shirt_measurements_sleeve_right + "<br/>";	}
				if(measurements.fittings_shirt_measurements_neck != "" 		&& measurements.fittings_shirt_measurements_neck != 0){			a += "Neck " + measurements.fittings_shirt_measurements_neck + "<br/>";	}
				if(measurements.fittings_shirt_measurements_front != "" 	&& measurements.fittings_shirt_measurements_front != 0){		a += "Front " + measurements.fittings_shirt_measurements_front + "<br/>";	}
				if(measurements.fittings_shirt_measurements_back != "" 		&& measurements.fittings_shirt_measurements_back != 0){			a += "Back " + measurements.fittings_shirt_measurements_back + "<br/>";	}
				if(measurements.fittings_shirt_measurements_length != "" 	&& measurements.fittings_shirt_measurements_length != 0){		a += "Shirt Length " + measurements.fittings_shirt_measurements_length + "<br/>";	}
				if(measurements.fittings_shirt_measurements_bicep != "" 	&& measurements.fittings_shirt_measurements_bicep != 0){		a += "Bicep " + measurements.fittings_shirt_measurements_bicep + "<br/>";	}
				if(measurements.fittings_shirt_measurements_hips != "" 		&& measurements.fittings_shirt_measurements_hips != 0){			a += "Hips " + measurements.fittings_shirt_measurements_hips + "<br/>";	}
				if(measurements.fittings_shirt_measurements_wrist != "" 	&& measurements.fittings_shirt_measurements_wrist != 0){		a += "Wrist " + measurements.fittings_shirt_measurements_wrist + "<br/>";	}
				if(measurements.fittings_shirt_measurements_watch != "" 	&& measurements.fittings_shirt_measurements_watch != 0){		a += "Wrist with watch " + measurements.fittings_shirt_measurements_watch + "<br/>";	}
			}
			if(measurements.fittings_vest_measurements_hips != undefined){
				if(measurements.fittings_vest_measurements_hips != "" 		&& measurements.fittings_vest_measurements_hips != 0){		a += "Hips at Vest length " + measurements.fittings_vest_measurements_hips + "<br/>";	}
				if(measurements.fittings_vest_measurements_length != "" 	&& measurements.fittings_vest_measurements_length != 0){	a += "Vest length " + measurements.fittings_vest_measurements_length + "<br/>";	}
				if(measurements.fittings_vest_measurements_chest != "" 		&& measurements.fittings_vest_measurements_chest != 0){		a += "Chest " + measurements.fittings_vest_measurements_chest + "<br/>";	}
				if(measurements.fittings_vest_measurements_stomach != "" 	&& measurements.fittings_vest_measurements_stomach != 0){	a += "Stomach " + measurements.fittings_vest_measurements_stomach + "<br/>";	}
				if(measurements.fittings_vest_measurements_neck != "" 		&& measurements.fittings_vest_measurements_neck != 0){		a += "Neck " + measurements.fittings_vest_measurements_neck + "<br/>";	}
				if(measurements.fittings_vest_measurements_half_shoulder_width != "" 	&& measurements.fittings_vest_measurements_half_shoulder_width != 0){		a += "Half Shoulder Width " + measurements.fittings_vest_measurements_half_shoulder_width + "<br/>";	}
				if(measurements.fittings_vest_measurements_full_shoulder != "" 	&& measurements.fittings_vest_measurements_full_shoulder != 0){		a += "Full Shoulder " + measurements.fittings_vest_measurements_full_shoulder + "<br/>";	}
				if(measurements.fittings_vest_measurements_v_length != "" 		&& measurements.fittings_vest_measurements_v_length != 0){			a += "Vest V to button " + measurements.fittings_vest_measurements_v_length + "<br/>";	}
			}

			document.getElementById("MeasurementChanges").innerHTML	= a;	
			if ("createEvent" in document) {
		    	var evt = document.createEvent("HTMLEvents");
		    	evt.initEvent("change", false, true);
		    	document.getElementById("MeasurementChanges").dispatchEvent(evt);
		    //	document.getElementById("Test").dispatchEvent(evt);
			}else{
				document.getElementById("MeasurementChanges").fireEvent("onchange");
		    //	document.getElementById("Test").fireEvent("onchange"); 
			}
			
			return a;
		}
		

		this.ImageTagsText = function() {
			var whichGarment = that.selectedVariantFitting().id;
			var fronttags = JSON.parse(localStorage.getItem('FittingFrontImageTags' + whichGarment));//  [{"image_id":"1400","left":73.994140625,"top":12.560059502720833,"text":"asasdasdasdasd"},{"image_id":"1400","left":69.6875,"top":34.89892669022083,"text":"bbbbbbbbbbbbbbbbbb"}]
			var sidetags = JSON.parse(localStorage.getItem('FittingSideImageTags' + whichGarment));
			var backtags = JSON.parse(localStorage.getItem('FittingBackImageTags' + whichGarment));

			var a = "";
			var count = 1;
			for (var x in fronttags) {
				a += '<span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + fronttags[x].text +  '</span><br/>';
				count++; 
			}
			for (var x in sidetags) {
				a += '<span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + sidetags[x].text +  '</span><br/>';
				count++; 
			}
			for (var x in backtags) {
				a += '<span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + backtags[x].text +  '</span><br/>';
				count++; 
			}

			document.getElementById("ImageTags").innerHTML	= a;		
			if ("createEvent" in document) {
		    	var evt = document.createEvent("HTMLEvents");
		    	evt.initEvent("change", false, true);
		    	document.getElementById("ImageTags").dispatchEvent(evt);	
			}else{
		    	document.getElementById("ImageTags").fireEvent("onchange");
			}
			return a;
		}
		


		this.modifyDropdowns = function(){
            
            var whichGarment = that.selectedVariantFitting().id;
            
           	var tailors = this.dsRegistry.getDatasource('tailorsDS').getStore();
           	var days = orderItem.days();
           	var months = orderItem.months();
//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor);     	
			try{		
				if(that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor == undefined || that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor == ''){
					document.getElementsByName("FittingTailor")[0].getElementsByTagName('option')[0].selected = true;
				}else{			 
					for(var a = 0; a < tailors.length; a++){				
			        	if( that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor.last_name == tailors[a].last_name ){		        		
							  document.getElementsByName("FittingTailor")[0].getElementsByTagName('option')[a+1].selected = true;
			            }
					}
				}
			}catch(e){
				;
			}
//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay);			
			try{		
				if(that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay == undefined || that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay == ''){
					document.getElementsByName("FittingDeliveryDay")[0].getElementsByTagName('option')[0].selected = true;
				}else{			 
					for(var a = 0; a < days.length; a++){
			        	if( that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay == days[a] ){
							  document.getElementsByName("FittingDeliveryDay")[0].getElementsByTagName('option')[a+1].selected = true;
			            }
					}
				}
			}catch(e){
				//console.log("day error: " + e);
			}	
//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth);			
			try{		
				if(that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth == undefined || that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth == '' ){
					document.getElementsByName("FittingDeliveryMonth")[0].getElementsByTagName('option')[0].selected = true;
				}else{			 
					for(var a = 0; a < months.length; a++){
			        	if( that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth == months[a].id ){
							  document.getElementsByName("FittingDeliveryMonth")[0].getElementsByTagName('option')[a+1].selected = true;
			            }
					}
				}
			}catch(e){
				//console.log("month error: " + e);
			}				
			
			try{
				if ("createEvent" in document) {
			    	var evt = document.createEvent("HTMLEvents");
			    	evt.initEvent("change", true, true);
			    	document.getElementsByName("FittingTailor")[0].dispatchEvent(evt);
			    	document.getElementsByName("FittingDeliveryDay")[0].dispatchEvent(evt);
			    	document.getElementsByName("FittingDeliveryMonth")[0].dispatchEvent(evt);
			    }else{
			    	document.getElementsByName("FittingTailor")[0].fireEvent("onchange");
			    	document.getElementsByName("FittingDeliveryDay")[0].fireEvent("onchange");
			    	document.getElementsByName("FittingDeliveryMonth")[0].fireEvent("onchange");
			    }
			}catch(e){
				console.log(e);
			}	
	
            return false;                                                                      
		};	



///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";
		
			var whichGarment = that.selectedVariantFitting().id;

console.log("getting " + 'FittingFrontImagePhoto' + whichGarment);

			var photosarray = [];
			var frontphotos = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);			
			frontphotos = (frontphotos == null) ? [] : JSON.parse(frontphotos);
			if(localStorage.getItem('FittingFrontImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('FittingFrontImagePhoto' + whichGarment).length > 0){				
					for(var i=0, len=frontphotos.length; i<len; i++){
						var obj = new Object();
						var name = frontphotos[i].picture;
						var burl = BUrl;
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}	
						burl = burl.replace("www.", "");				
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}
		   				obj.name = name;
		   				obj.id  = "" + frontphotos[i].image_id + "";
		   				obj.image_type = "1"; 
						photosarray.push(obj);
				 	}
				}  
			}
			var sidephotos = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
			sidephotos = (sidephotos == null) ? [] : JSON.parse(sidephotos);
			if(localStorage.getItem('FittingSideImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('FittingSideImagePhoto' + whichGarment).length > 0){
					for(var i=0, len=sidephotos.length; i<len; i++){
						var obj = new Object();
		   				var name = sidephotos[i].picture;
						var burl = BUrl;
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}	
						burl = burl.replace("www.", "");				
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}
		   				obj.name = name;
		   				obj.id  = "" + sidephotos[i].image_id + "";
		   				obj.image_type = "2"; 
						photosarray.push(obj);
				 	}
				}  
			}
			var backphotos = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
			backphotos = (backphotos == null) ? [] : JSON.parse(backphotos);
			if(localStorage.getItem('FittingBackImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('FittingBackImagePhoto' + whichGarment).length > 0){
					for(var i=0, len=backphotos.length; i<len; i++){
						var obj = new Object();
		   				var name = backphotos[i].picture;
						var burl = BUrl;
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}	
						burl = burl.replace("www.", "");				
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}
		   				obj.name = name;
		   				obj.id  = "" + backphotos[i].image_id + "";
		   				obj.image_type = "3"; 
						photosarray.push(obj);
				 	}
				}  
			}
			
			var customphotos = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
			customphotos = (customphotos == null) ? [] : JSON.parse(customphotos);
			if(localStorage.getItem('FittingCustomImagePhoto' + whichGarment) != null){
				if(localStorage.getItem('FittingCustomImagePhoto' + whichGarment).length > 0){
					for(var i=0, len=customphotos.length; i<len; i++){
						var obj = new Object();
						var name = customphotos[i].picture;
   						var burl = BUrl;
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}	
						burl = burl.replace("www.", "");				
						while(name.indexOf(burl) >= 0){//name.contains(burl)){
							name = name.replace(burl, "");
						}
		   				obj.name = name;
   						obj.id  = "" + customphotos[i].image_id + "";
   						obj.image_type = "4"; 
						photosarray.push(obj);
					}
				}  
		    }
			
							
			var tagsarray = [];
			var fronttags = localStorage.getItem('FittingFrontImageTags' + whichGarment);
			fronttags = (fronttags == null) ? [] : JSON.parse(fronttags);
			if(localStorage.getItem('FittingFrontImageTags' + whichGarment) != null){
				if(localStorage.getItem('FittingFrontImageTags' + whichGarment).length > 0){
					for (var j=0; j<fronttags.length; j++){
						tagsarray.push(fronttags[j]);
				 	}
				}  
			}
			var sidetags = localStorage.getItem('FittingSideImageTags' + whichGarment);
			sidetags = (sidetags == null) ? [] : JSON.parse(sidetags);
			if(localStorage.getItem('FittingSideImageTags' + whichGarment) != null){
				if(localStorage.getItem('FittingSideImageTags' + whichGarment).length > 0){
					for (var j=0; j<sidetags.length; j++){
						tagsarray.push(sidetags[j]);
				 	}
				}  
			}
			var backtags = localStorage.getItem('FittingBackImageTags' + whichGarment);
			backtags = (backtags == null) ? [] : JSON.parse(backtags);
			if(localStorage.getItem('FittingBackImageTags' + whichGarment) != null){
				if(localStorage.getItem('FittingBackImageTags' + whichGarment).length > 0){
					for (var j=0; j<backtags.length; j++){
						tagsarray.push(backtags[j]);
				 	}
				}  
			}
			var customtags = localStorage.getItem('FittingCustomImageTags' + whichGarment);
			customtags = (customtags == null) ? [] : JSON.parse(customtags);
			if(localStorage.getItem('FittingCustomImageTags' + whichGarment) != null){
				if(localStorage.getItem('FittingCustomImageTags' + whichGarment).length > 0){
					for (var j=0; j<customtags.length; j++){
						tagsarray.push(customtags[j]);
				 	}
				}  
			}


			var customerString = orderItem.custSelectVM.selectedCustomer();
			var fittingIdPost = this.FittingId();

			var dateToPost = '';		
			var date = new Date();
			var yearnow = date.getFullYear();
			var monthnow = date.getMonth() + 1;
			var daynow = date.getDate();
			
			var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
			var dateToCompare;
						
			if(this.FittingDeliveryMonth() != undefined && this.FittingDeliveryMonth() !="" && this.FittingDeliveryDay() != undefined && this.FittingDeliveryDay() != ""){ 
				if( this.FittingDeliveryMonth() < monthnow || (this.FittingDeliveryMonth() == monthnow && this.FittingDeliveryDay() < daynow) ){
					var yearnew = yearnow + 1;
					dateToPost = this.FittingDeliveryDay() + "-" + this.FittingDeliveryMonth() + "-" + yearnew;
					dateToCompare = new Date(this.FittingDeliveryMonth() + "/" + this.FittingDeliveryDay() + "/" + yearnew); 
				}else{
					dateToPost = this.FittingDeliveryDay() + "-" + this.FittingDeliveryMonth() + "-" + yearnow;
					dateToCompare = new Date(this.FittingDeliveryMonth() + "/" + this.FittingDeliveryDay() + "/" + yearnow);
				}
			}

			var diffDays = -1;
			try{
				var timeDiff = dateToCompare.getTime() - dateNow.getTime();
				diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				console.log("diffDays: " + diffDays);
			}catch(e){ ; }	

			var fittingvideos = ko.observableArray();
			fittingvideos.push(new orderItem.FittingVM.FittingVideoViewModel(this.FittingVideo(), this.FittingVideoNotes() ));
			
			var tailorIdPost = "";			
			if(this.FittingTailor() != undefined){				
				if(this.FittingTailor().user_id != undefined){
					tailorIdPost = this.FittingTailor().user_id;
				}
			}
		
			var foundnotcompletedstep = false;
			for(var x = 0; x < this.workflow().length-1; x++){
				if(this.workflow()[x].completed == "false"){
					foundnotcompletedstep = true;
					break;
				}
			}
			
			var pendingFrontTagsPost = "0";
			if(this.SkippedFrontTags() == true){
				pendingFrontTagsPost = "1";
			}
			var pendingSideTagsPost = "0";
			if(this.SkippedSideTags() == true){
				pendingSideTagsPost = "1";
			}
			var pendingBackTagsPost = "0";
			if(this.SkippedBackTags() == true){
				pendingBackTagsPost = "1";
			}
			
			var pendingVideoPost = "0";
			if(this.SkippedVideo() == true){
				pendingVideoPost = "1";
			}

//console.log("this.FittingVideo(): " + this.FittingVideo());
			if(tailorIdPost == "" || dateToPost == ""){
				customAlert("Please select a tailor and a date!");
			}else if(diffDays < 0){
				customAlert("There has been an error with the selected date. <br/> Try again or select another.");
			}else if(diffDays > 120){
				customAlert("The selected date is more than 4 months away! <br/> Please select a closer date.");
			}else if(frontphotos.length  == 0){
				customAlert("Please upload a front image");
				this.currentStep(this.workflow()[0] );
			}else if(sidephotos.length  == 0){
				customAlert("Please upload a side image");
				this.currentStep(this.workflow()[1] );
			}else if(backphotos.length  == 0){
				customAlert("Please upload a back image");
				this.currentStep(this.workflow()[2] );				
			}else if(fronttags.length == 0 && this.NoFrontTags() == false && this.SkippedFrontTags() == false ){
				customAlert("Please enter front image notes");
				this.currentStep(this.workflow()[4] );
			}else if(sidetags.length == 0 && this.NoSideTags() == false && this.SkippedSideTags() == false ){
				customAlert("Please enter side image notes");
				this.currentStep(this.workflow()[5] );
			}else if(backtags.length == 0 && this.NoBackTags() == false && this.SkippedBackTags() == false ){
				customAlert("Please enter back image notes");
				this.currentStep(this.workflow()[6] );
			}else if(this.FittingVideo() == "" && this.NoVideo() == false && this.SkippedVideo() == false){
				customAlert("Please enter the fitting video");
				this.currentStep(this.workflow()[7] );
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					customer: customerString,
					fitting_id: fittingIdPost,
					order_id: this.FittingOrderId(),//this.selected_order.order_id,
					orders_products_id: this.FittingOrderProductId(),
					garment: this.FittingGarment(),
					measurements: ko.toJSON(this.FittingMeasurements()),
					pendingFrontNotes: pendingFrontTagsPost,
					pendingSideNotes: pendingSideTagsPost,
					pendingBackNotes: pendingBackTagsPost,
					pendingVideo: pendingVideoPost,
					tailor: {
						user_id: tailorIdPost,
						delivery_date : dateToPost,
						notes: this.FittingTailorNotes()
					},
					fitting_images: photosarray,
					fitting_tags: tagsarray,
					fitting_videos: ko.toJSON(fittingvideos())
					//fitting_id, order_id, garment_id, tailor(user_id, delivery_date, notes), fitting_images(id, name), fitting_tags(left, top, text), fitting_videos(path, notes).
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);

				$.post(BUrl + 'client_fittings/fitting_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	if(that.FittingId() == ''){
		            		customAlert("Fitting succesfully added");	
		            	}else{
		            		customAlert("Fitting succesfully edited");
		            	}
		            	that.FittingId(dataS.fitting);

		                var cs = that.currentStep().id;	
		                that.workflow()[cs].completed = "true";

		                if(that.variantNameFitting().length == 1){
		                	$.mobile.changePage('#customerGarmentsFittingsList');
		                }else{
		                	that.goToNextNotSubmittedFitting();
		                }
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
					spinner.style.display = "none";
				});
	        }        		
			//spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////




		ko.bindingHandlers.changeMeasurement = {		// value[0]: the fitiing_measurement to change, value[1]: "increase"/"decrease" 
			update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();		
					var x  = that.FittingData[ that.getRow(that.selectedVariantFitting().id) ]['FittingMeasurements'][value[0]];
					if(x == "" || x == null || x == undefined){
						x = 0;
					}
					x = x*100;
					if(value[1] == "increase"){
						x = x + 5;
					}else{
						x = x - 5;
					}	 
					x = x/100;
					x = x.toFixed(2);
					
					that.FittingData[ that.getRow(that.selectedVariantFitting().id) ]['FittingMeasurements'][value[0]] = x;
					that.MeasurementChangesText();
				///	if(x != 0){ 
						document.getElementsByName(value[0])[0].innerHTML = x;
				//	}else{
				//		document.getElementsByName(value[0])[0].innerHTML = "";
				//	}	
		            if ("createEvent" in document) {
		    			var evt = document.createEvent("HTMLEvents");
		    			evt.initEvent("change", false, true);
		    			document.getElementsByName(value[0])[0].dispatchEvent(evt);
					}else{
		    			document.getElementsByName(value[0])[0].fireEvent("onchange");
		            }
				});
		    }
		};


		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
/*
	nextStep: function() {
		var tWork = this.workflow();
		for (var ind in tWork ) {
				if (!tWork[ind].completed) {
				this.currentStep( tWork[ind] );
				return;
			}
		}
	},*/
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"Fitting"  : this.FittingData
		}, true);
	},

	getVariant: function(id) {
		var toreturn = this.FittingData[0];
		for (var ind in this.FittingData) {
			if ( this.FittingData[ind].variantId == id  ) {
				toreturn = this.FittingData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.FittingData) {
			if ( this.FittingData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantFitting: function(name) {
//console.log(this.variantNameFitting);
		if(this.variantNameFitting()[0].title != "Fitting 1"){
			this.FittingDataAID += 1;
			var newid = this.FittingDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantFitting().id ) )  ); //CLONE Object
			this.variantNameFitting.push({title: vname, id: this.FittingDataAID});
			tObj.variantId = this.FittingDataAID;		
			this.flushModel();
		}else{
			this.variantNameFitting()[0].title = name;
		}
	},	
	
	addVariantsFittings: function() {
		for(var x in orderItem.fittingsVM.FittingData){
		//	console.log("addVariantsFittings " + x);
			
			if( x != 0){
				this.FittingDataAID += 1;
				var vname = orderItem.fittingsVM.FittingData[x].FittingGarment; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantFitting().id ) )  ); //CLONE Object
				this.variantNameFitting.push({title: vname, id: this.FittingDataAID});
				tObj.variantId = this.FittingDataAID;		
				this.flushModel();
			}else{
				var vname = orderItem.fittingsVM.FittingData[x].FittingGarment;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantFitting().id ) )  ); //CLONE Object
				this.variantNameFitting()[x].title = vname;
				tObj.variantId = this.FittingDataAID;		
				this.flushModel();
			}
			
		}
	},
	
	

	digestData: function(data) {
		this.FittingData  = data.Fitting;
		this.renderView();

	},

	renderView: function() {
		this.renderFitting();
	},

	renderFitting: function() {	
		this.modifyDropdowns();
		
		var whichGarment = this.selectedVariantFitting().id;
		try{
			var photo = JSON.parse( localStorage.getItem('FittingFrontImagePhoto' + whichGarment) );			
			document.getElementById("FittingFrontImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'"  width="480"/>'].join('');
			var thetags = localStorage.getItem('FittingFrontImageTags' + whichGarment);
			thetags = (thetags === null) ? [] : JSON.parse(thetags);
			var taghtml = "";
			for (var a=thetags.length - 1; a >= 0; a--){	                    			
				fromleft = 100 + thetags[a].left;
				arrowwidth = 105 - thetags[a].left;
				taghtml += '<div class="arrow" style="width:' + arrowwidth + '%;height:2px;left:' +  thetags[a].left + '%;top:' + thetags[a].top + '%;"></div><div class="tagged" style="width:35;height:35;left:105%;top:' + thetags[a].top + '%;"><div class="tagged_title" style="display:block;">' + thetags[a].text + '</div><div class="tagged_box" style="width:35;height:35;top:-20;display:block;">';
				if(this.isCompleted() == false && this.isActive() == true){				
					taghtml += '<img class="openDialog" onclick="javascript: deleteImageTag(this, ' + thetags[a].left + ', ' + thetags[a].top + ');" value="Delete" src="http://shirt-tailor.net/thepos/appimg/del.png" style="width: 35px; height:35px; background-color:rgba(0, 0, 0, 0); border: 0px; border-radius: 0px; margin-top: -3px; margin-left: 450px; padding: 0px;"></img></div></div>';
				}else{
					taghtml += '</div></div>';
				}	
			}
			document.getElementById("FittingFrontImageMedia2").innerHTML = ['<div style="width: 100px;"></div><div class="img-wrap" id="', photo[0].image_id ,'"><div class="image_panel"><img src="', photo[0].thumbnail,'" width="480" class="imageMap" id="imagemap_', photo[0].image_id ,'"/><div class="mapper" id="mapper_', photo[0].image_id ,'"><img style="width: 35px; height:35px;" src="http://shirt-tailor.net/thepos/appimg/save.png" onclick="javascript:orderItem.openDialog();" id="dialog_', photo[0].image_id ,'"/></div><div class="planetmap" id="planetmap_', photo[0].image_id ,'">', taghtml, '</div></div></div>'].join('');
			document.getElementById("NoFittingFrontImageMedia2").style.display = "none";
		}catch(e){			
			try{
				document.getElementById("FittingFrontImageMedia").innerHTML = ['<img src="gfx/fittings/take-image-front.png" />'].join('');
				document.getElementById("FittingFrontImageMedia2").innerHTML = [''].join('');
				document.getElementById("NoFittingFrontImageMedia2").style.display = "block";
			}catch(e2){ ; }
		}
		try{
			var photo = JSON.parse( localStorage.getItem('FittingSideImagePhoto' + whichGarment) );
			document.getElementById("FittingSideImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'"  width="480"/>'].join('');
			var thetags = localStorage.getItem('FittingSideImageTags' + whichGarment);
			thetags = (thetags === null) ? [] : JSON.parse(thetags);
			var taghtml = "";
			for (var a=thetags.length - 1; a >= 0; a--){	                    			
				fromleft = 100 + thetags[a].left;
				arrowwidth = 105 - thetags[a].left;
				taghtml += '<div class="arrow" style="width:' + arrowwidth + '%;height:2px;left:' +  thetags[a].left + '%;top:' + thetags[a].top + '%;"></div><div class="tagged" style="width:35;height:35;left:105%;top:' + thetags[a].top + '%;"><div class="tagged_title" style="display:block;">' + thetags[a].text + '</div><div class="tagged_box" style="width:35;height:35;top:-20;display:block;">';
				if(this.isCompleted() == false && this.isActive() == true){				
					taghtml += '<img class="openDialog" onclick="javascript: deleteImageTag(this, ' + thetags[a].left + ', ' + thetags[a].top + ');" value="Delete" src="http://shirt-tailor.net/thepos/appimg/del.png" style="width: 35px; height:35px; background-color:rgba(0, 0, 0, 0); border: 0px; border-radius: 0px; margin-top: -3px; margin-left: 450px; padding: 0px;"></img></div></div>';
				}else{
					taghtml += '</div></div>';
				}
			}
			document.getElementById("FittingSideImageMedia2").innerHTML = ['<div style="width: 100px;"></div><div class="img-wrap" id="', photo[0].image_id ,'"><div class="image_panel"><img src="', photo[0].thumbnail,'" width="480" class="imageMap" id="imagemap_', photo[0].image_id ,'"/><div class="mapper" id="mapper_', photo[0].image_id ,'"><img style="width: 35px; height:35px;" src="http://shirt-tailor.net/thepos/appimg/save.png" onclick="javascript:orderItem.openDialog();" id="dialog_', photo[0].image_id ,'"/></div><div class="planetmap" id="planetmap_', photo[0].image_id ,'">', taghtml, '</div></div></div>'].join('');
			document.getElementById("NoFittingSideImageMedia2").style.display = "none";
		}catch(e){
			try{
				document.getElementById("FittingSideImageMedia").innerHTML = ['<img src="gfx/fittings/take-image-side.png"/>'].join('');
				document.getElementById("FittingSideImageMedia2").innerHTML = [''].join('');
				document.getElementById("NoFittingSideImageMedia2").style.display = "block";
			}catch(e2){ ; }
		}
		try{
			var photo = JSON.parse( localStorage.getItem('FittingBackImagePhoto' + whichGarment) );
			document.getElementById("FittingBackImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'"  width="480"/>'].join('');
			var thetags = localStorage.getItem('FittingBackImageTags' + whichGarment);
			thetags = (thetags === null) ? [] : JSON.parse(thetags);
			var taghtml = ""; 
			for (var a=thetags.length - 1; a >= 0; a--){	                    			
				fromleft = 100 + thetags[a].left;
				arrowwidth = 105 - thetags[a].left;
				taghtml += '<div class="arrow" style="width:' + arrowwidth + '%;height:2px;left:' +  thetags[a].left + '%;top:' + thetags[a].top + '%;"></div><div class="tagged" style="width:35;height:35;left:105%;top:' + thetags[a].top + '%;"><div class="tagged_title" style="display:block;">' + thetags[a].text + '</div><div class="tagged_box" style="width:35;height:35;top:-20;display:block;">';
				if(this.isCompleted() == false && this.isActive() == true){				
					taghtml += '<img class="openDialog" onclick="javascript: deleteImageTag(this, ' + thetags[a].left + ', ' + thetags[a].top + ');" value="Delete" src="http://shirt-tailor.net/thepos/appimg/del.png" style="width: 35px; height:35px; background-color:rgba(0, 0, 0, 0); border: 0px; border-radius: 0px; margin-top: -3px; margin-left: 450px; padding: 0px;"></img></div></div>';
				}else{
					taghtml += '</div></div>'; 
				}
			}
			document.getElementById("FittingBackImageMedia2").innerHTML = ['<div style="width: 100px;"></div><div class="img-wrap" id="', photo[0].image_id ,'"><div class="image_panel"><img src="', photo[0].thumbnail,'" width="480" class="imageMap" id="imagemap_', photo[0].image_id ,'"/><div class="mapper" id="mapper_', photo[0].image_id ,'"><img style="width: 35px; height:35px;" src="http://shirt-tailor.net/thepos/appimg/save.png" onclick="javascript:orderItem.openDialog();" id="dialog_', photo[0].image_id ,'"/></div><div class="planetmap" id="planetmap_', photo[0].image_id ,'">', taghtml, '</div></div></div>'].join('');
			document.getElementById("NoFittingBackImageMedia2").style.display = "none";			
		}catch(e){
			try{
				document.getElementById("FittingBackImageMedia").innerHTML = ['<img src="gfx/fittings/take-image-back.png"/>'].join('');
				document.getElementById("FittingBackImageMedia2").innerHTML = [''].join('');
				document.getElementById("NoFittingBackImageMedia2").style.display = "block";
			}catch(e2){ ; }				
		}
		
		
		try{
			document.getElementById('FittingCustomImageMedia').innerHTML = [''];
			var photos = JSON.parse( localStorage.getItem('FittingCustomImagePhoto' + whichGarment) );
console.log(JSON.stringify(photos));			
			var	thetags = localStorage.getItem('FittingCustomImageTags' + whichGarment);
			thetags = (thetags === null) ? [] : JSON.parse(thetags);
console.log(JSON.stringify(thetags));				
			var taghtml = "";
			if(localStorage.getItem('FittingCustomImagePhoto' + whichGarment) != null){
				for (var i=0, len=photos.length; i<len; i++){
					var taghtml = ""
					for (var a=thetags.length - 1; a >= 0; a--){	                    		
						if(thetags[a].image_id == photos[i].image_id){	
							fromleft = 100 + thetags[a].left;
							arrowwidth = 105 - thetags[a].left;
							taghtml += '<div class="arrow" style="width:' + arrowwidth + '%;height:2px;left:' +  thetags[a].left + '%;top:' + thetags[a].top + '%;"></div><div class="tagged" style="width:35;height:35;left:105%;top:' + thetags[a].top + '%;"><div class="tagged_title" style="display:block;">' + thetags[a].text + '</div><div class="tagged_box" style="width:35;height:35;top:-20;display:block;">';
							if(this.isCompleted() == false && this.isActive() == true){				
								taghtml += '<img class="openDialog" onclick="javascript: deleteImageTag(this, ' + thetags[a].left + ', ' + thetags[a].top + ');" value="Delete" src="http://shirt-tailor.net/thepos/appimg/del.png" style="width: 35px; height:35px; background-color:rgba(0, 0, 0, 0); border: 0px; border-radius: 0px; margin-top: -3px; margin-left: 450px; padding: 0px;"></img></div></div>';
							}else{
								taghtml += '</div></div>'; 
							}	
						}
					}
					document.getElementById('FittingCustomImageMedia').innerHTML += ['<div style="width: 100px;"></div><div class="img-wrap" id="', photos[i].image_id ,'"><span class="close" onclick="javascript: removepic(', photos[i].image_id ,');"></span><div class="image_panel"><img src="', photos[i].thumbnail ,'" width="480" class="imageMap" id="imagemap_', photos[i].image_id ,'"/><div class="mapper" id="mapper_', photos[i].image_id ,'"><img style="width: 35px; height:35px;" src="http://shirt-tailor.net/thepos/appimg/save.png" onclick="javascript:orderItem.openDialog();" id="dialog_', photos[i].image_id ,'"/></div><div class="planetmap" id="planetmap_', photos[i].image_id ,'">', taghtml, '</div></div></div>'].join('');
				}
		    }
		}catch(e){
			console.log("ERROR: " + e);
		}	
		
		
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantFitting().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.FittingCustomer)		!= "undefined") {this.FittingCustomer(tData.FittingCustomer);}
				if (typeof(tData.FittingGarment)		!= "undefined") {this.FittingGarment(tData.FittingGarment);}
				if (typeof(tData.FittingGarmentFabric)	!= "undefined") {this.FittingGarmentFabric(tData.FittingGarmentFabric);}
				if (typeof(tData.FittingId)    			!= "undefined") {this.FittingId(tData.FittingId);}
				if (typeof(tData.FittingOrderId)    	!= "undefined") {this.FittingOrderId(tData.FittingOrderId);}
				if (typeof(tData.FittingOrderProductId)    	!= "undefined") {this.FittingOrderProductId(tData.FittingOrderProductId);}
				if (typeof(tData.FittingTailor)    		!= "undefined") {this.FittingTailor(tData.FittingTailor);}
				if (typeof(tData.FittingDeliveryDay)    != "undefined") {this.FittingDeliveryDay(tData.FittingDeliveryDay);}
				if (typeof(tData.FittingDeliveryMonth)  != "undefined") {this.FittingDeliveryMonth(tData.FittingDeliveryMonth);}
				if (typeof(tData.FittingTailorNotes)    != "undefined") {this.FittingTailorNotes(tData.FittingTailorNotes);}
				if (typeof(tData.FittingVideo)    		!= "undefined") {this.FittingVideo(tData.FittingVideo);}
				if (typeof(tData.FittingMeasurements)   != "undefined") {this.FittingMeasurements(tData.FittingMeasurements);}
				if (typeof(tData.OrderMeasurements)   != "undefined") {this.OrderMeasurements(tData.OrderMeasurements);}
				if (typeof(tData.SkippedVideo)   	!= "undefined") {this.SkippedVideo(tData.SkippedVideo);}
				if (typeof(tData.NoVideo)   		!= "undefined") {this.NoVideo(tData.NoVideo);}
				if (typeof(tData.SkippedFrontTags)   != "undefined") {this.SkippedFrontTags(tData.SkippedFrontTags);}
				if (typeof(tData.SkippedSideTags)   != "undefined") {this.SkippedSideTags(tData.SkippedSideTags);}
				if (typeof(tData.SkippedBackTags)   != "undefined") {this.SkippedBackTags(tData.SkippedBackTags);}
				if (typeof(tData.NoFrontTags)   != "undefined") {this.NoFrontTags(tData.NoFrontTags);}
				if (typeof(tData.NoSideTags)   != "undefined") {this.NoSideTags(tData.NoSideTags);}
				if (typeof(tData.NoBackTags)   != "undefined") {this.NoBackTags(tData.NoBackTags);}
				if (typeof(tData.isActive)   != "undefined") {this.isActive(tData.isActive);}
				if (typeof(tData.isCompleted)   != "undefined") {this.isCompleted(tData.isCompleted);}
				if (typeof(tData.FittingVideoNotes)   != "undefined") {this.FittingVideoNotes(tData.FittingVideoNotes);}
				if (typeof(tData.AdditionalImagesStepComplete)   != "undefined") {this.AdditionalImagesStepComplete(tData.AdditionalImagesStepComplete);}
				if (typeof(tData.MeasurementsStepComplete)   != "undefined") {this.MeasurementsStepComplete(tData.MeasurementsStepComplete);}
				
			}
		}catch(e){
			;
		}

		this.checkSteps();
	}
});

defFitting = SimpleDatasource.extend({
	init: function( name, dsRegistry, data ) {
		if(data == null || data == undefined){
		//	orderItem.fittingsVM.addVariantFitting('new');
			
console.log("data is null");			
			var Fitting = {};
			Fitting.variantId        = 0;
			
			Fitting.FittingCustomer = '';
			Fitting.FittingGarment = '';
			Fitting.FittingGarmentFabric = '';
			Fitting.FittingId = ""//'';
			Fitting.FittingOrderId = '';
			Fitting.FittingOrderProductId = '';
			Fitting.FittingTailor = '';
			Fitting.FittingDeliveryDay = '';
			Fitting.FittingDeliveryMonth = '';
			Fitting.FittingTailorNotes = '';
			Fitting.FittingVideo = '';
			Fitting.FittingVideoNotes = '';
			Fitting.FittingMeasurements = '';
			Fitting.OrderMeasurements = '';
			
			Fitting.SkippedVideo = false;
			Fitting.NoVideo = false;
			Fitting.SkippedFrontTags = false;
			Fitting.SkippedSideTags = false;
			Fitting.SkippedBackTags = false;
			Fitting.NoFrontTags = false;
			Fitting.NoSideTags = false;
			Fitting.NoBackTags = false;
			Fitting.isActive = true;
			Fitting.isCompleted = false;
			Fitting.AdditionalImagesStepComplete = false;
			Fitting.MeasurementsStepComplete = false;

			var iFitting = {
				Fitting: []
			};
			iFitting.Fitting.push(Fitting);
			this._super(name, iFitting, dsRegistry);
			
		}else{
//console.log("data is not null");
			var iFitting = {
				Fitting: []
			};
			
			var x = 0;
			for (var a in data) {
				for (var b in data[a].products){
					if( data[a].products[b].checked == true ){
					//	orderItem.fittingsVM.addVariantFitting(data[a].products[b].garment);
					var c = localStorage.getItem("fittingindex");					
//console.log("data[a].products[b]: " + JSON.stringify(data[a].products[b]) );					
//console.log("fitting index: " + c);
						var Fitting = {};
						Fitting.variantId = x;
						
						Fitting.FittingCustomer = orderItem.custSelectVM.selectedCustomer();
						Fitting.FittingGarment = data[a].products[b].garment;
						Fitting.FittingGarmentFabric = data[a].products[b].fabric;
						Fitting.FittingOrderId = data[a].order_id;
						Fitting.FittingOrderProductId = data[a].products[b].orders_products_id;
						if(c == -1){
							Fitting.FittingId = '';
							Fitting.FittingTailor = '';
							Fitting.FittingDeliveryDay = ''; 
							Fitting.FittingDeliveryMonth = '';
							Fitting.FittingTailorNotes = '';
							Fitting.FittingVideo = '';
							Fitting.FittingVideoNotes = '';
							Fitting.SkippedVideo = false;
							Fitting.NoVideo = false;
							Fitting.SkippedFrontTags = false;
							Fitting.SkippedSideTags = false;
							Fitting.SkippedBackTags = false;
							Fitting.NoFrontTags = false;
							Fitting.NoSideTags = false;
							Fitting.NoBackTags = false;
							Fitting.isActive = true;
							Fitting.isCompleted = false;
							Fitting.AdditionalImagesStepComplete = false;
							Fitting.MeasurementsStepComplete = false;
							
						}else{
							Fitting.AdditionalImagesStepComplete = true;
							Fitting.MeasurementsStepComplete = true;
							
							Fitting.FittingId = data[a].products[b].fittings[c].fitting.fitting_id;							
							if(data[a].products[b].fittings[c].fitting.is_active == "1"){				
								Fitting.isActive = true;
							}else{
								Fitting.isActive = false;
							}
							if(data[a].products[b].completed == "1"){				
								Fitting.isCompleted = true;
							}else{
								Fitting.isCompleted = false;
							}
							
console.log("data[a].products[b].fittings[c].tailor: " + JSON.stringify(data[a].products[b].fittings[c].tailor.tailor_id) );							
console.log("data[a].products[b].fittings[c].tailor.tailor_id: " + data[a].products[b].fittings[c].tailor.tailor_id);							
							var tailors = dsRegistry.getDatasource('tailorsDS').getStore();
console.log("TAILORS: " + JSON.stringify(tailors));							
							for(var ind = 0; ind < tailors.length; ind++){
								if(tailors[ind].user_id == data[a].products[b].fittings[c].tailor.tailor_id ){
									Fitting.FittingTailor = tailors[ind];
									break;
								}
							}
							
							var date = data[a].products[b].fittings[c].tailor.delivery_date;
							var day = date.substring(date.lastIndexOf("-") + 1);
							var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
							var year = date.substring(0, date.indexOf("-"));
							Fitting.FittingDeliveryDay = day*1.0; 
							month = month*1.0;
							Fitting.FittingDeliveryMonth = month*1.0;
							
							Fitting.FittingTailorNotes = data[a].products[b].fittings[c].tailor.tailor_notes;
							Fitting.FittingVideo = data[a].products[b].fittings[c].videos[0].video_path;
							Fitting.FittingVideoNotes = data[a].products[b].fittings[c].videos[0].video_notes;
							Fitting.SkippedVideo = false;
							if(data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0"){
								Fitting.SkippedFrontTags = false;
							}else{
								Fitting.SkippedFrontTags = true;	
							}
							if(data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0"){
								Fitting.SkippedSideTags = false;
							}else{
								Fitting.SkippedSideTags = true;	
							}
							if(data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0"){
								Fitting.SkippedBackTags = false;
							}else{
								Fitting.SkippedBackTags = true;	
							}
							if(data[a].products[b].fittings[c].fitting.video_pending == "1"){
								Fitting.SkippedVideo = true;
							}else{
								Fitting.SkippedVideo = false;	
							}
							Fitting.NoVideo = false;
							if(data[a].products[b].fittings[c].videos[0].video_path == "" && data[a].products[b].fittings[c].fitting.video_pending == "0"){
								Fitting.NoVideo = true;
							} 
							
							Fitting.NoFrontTags = false;
							Fitting.NoSideTags = false;
							Fitting.NoBackTags = false;
							//{"customer_id":1,"image_id":1570,"picture":"uploaded/temp_order_images/1436883505.jpg","thumbnail":"data:image/
							/* 
							var frontIndex = 0;
							var sideIndex = 1;
							var backIndex = 2;
							var customIndex = 3;
							
							if( data[a].products[b].fittings[c].images[0].fitting_image_type == "1"){
								frontIndex = 0;
							}
							if( data[a].products[b].fittings[c].images[1].fitting_image_type == "1"){
								frontIndex = 1;
							}
							if( data[a].products[b].fittings[c].images[2].fitting_image_type == "1"){
								frontIndex = 2;
							}
							
							if( data[a].products[b].fittings[c].images[0].fitting_image_type == "2"){
								sideIndex = 0;
							}
							if( data[a].products[b].fittings[c].images[1].fitting_image_type == "2"){
								sideIndex = 1;
							}
							if( data[a].products[b].fittings[c].images[2].fitting_image_type == "2"){
								sideIndex = 2;
							}
							if( data[a].products[b].fittings[c].images[0].fitting_image_type == "3"){
								backIndex = 0;
							}
							if( data[a].products[b].fittings[c].images[1].fitting_image_type == "3"){
								backIndex = 1;
							}
							if( data[a].products[b].fittings[c].images[2].fitting_image_type == "3"){
								backIndex = 2;
							}
console.log( frontIndex + " " + sideIndex + " " + backIndex);
							
							try{
								var tmp = [];
		        		   		var customer_id = orderItem.custSelectVM.selectedCustomer().id;
		        		   		var image_id = data[a].products[b].fittings[c].images[frontIndex].image_id;
		        		   		var image = data[a].products[b].fittings[c].images[frontIndex].fitting_image_path;
		        		   		var thumbnail = data[a].products[b].fittings[c].images[frontIndex].fitting_image_path;
	        					tmp.push({customer_id:customer_id,image_id:image_id,picture:image,thumbnail:thumbnail});
		           				localStorage.setItem("FittingFrontImagePhoto0", JSON.stringify(tmp));
								tmp = [];
								var servertags = JSON.parse(data[a].products[b].fittings[c].images[frontIndex].fitting_image_tags);
								if(servertags != null){
									for(var ind = 0; ind < servertags.length; ind++){
										tmp.push({image_id:image_id,left:servertags[ind].left,top:servertags[ind].top,text:servertags[ind].text});
									}
									localStorage.setItem("FittingFrontImageTags0", JSON.stringify(tmp));
								}else{
									if(data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0"){
										Fitting.NoFrontTags = true;
									}
								}
							}catch(e){ ; }
							
							try{ 
								tmp = [];
		        		   		customer_id = orderItem.custSelectVM.selectedCustomer().id;
		        		   		image_id = data[a].products[b].fittings[c].images[sideIndex].image_id;
		        		   		image = data[a].products[b].fittings[c].images[sideIndex].fitting_image_path;
		        		   		thumbnail = data[a].products[b].fittings[c].images[sideIndex].fitting_image_path;
	        					tmp.push({customer_id:customer_id,image_id:image_id,picture:image,thumbnail:thumbnail});
		           				localStorage.setItem("FittingSideImagePhoto0", JSON.stringify(tmp));
								tmp = [];
								var servertags = JSON.parse(data[a].products[b].fittings[c].images[sideIndex].fitting_image_tags);
								if(servertags != null){
									for(var ind = 0; ind < servertags.length; ind++){
										tmp.push({image_id:image_id,left:servertags[ind].left,top:servertags[ind].top,text:servertags[ind].text});
									}
									localStorage.setItem("FittingSideImageTags0", JSON.stringify(tmp));
								}else{
									if(data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0"){
										Fitting.NoSideTags = true;
									}
								}
							}catch(e){ ; }	
							
							try{
								tmp = [];
		        		   		customer_id = orderItem.custSelectVM.selectedCustomer().id;
		        		   		image_id = data[a].products[b].fittings[c].images[backIndex].image_id;
		        		   		image = data[a].products[b].fittings[c].images[backIndex].fitting_image_path;
		        		   		thumbnail = data[a].products[b].fittings[c].images[backIndex].fitting_image_path;
	        					tmp.push({customer_id:customer_id,image_id:image_id,picture:image,thumbnail:thumbnail});
		           				localStorage.setItem("FittingBackImagePhoto0", JSON.stringify(tmp));
								tmp = [];
								var servertags = JSON.parse(data[a].products[b].fittings[c].images[backIndex].fitting_image_tags);
								if(servertags != null){
									for(var ind = 0; ind < servertags.length; ind++){
										tmp.push({image_id:image_id,left:servertags[ind].left,top:servertags[ind].top,text:servertags[ind].text});
									}
									localStorage.setItem("FittingBackImageTags0", JSON.stringify(tmp));
								}else{
									if(data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0"){
										Fitting.NoBackTags = true;
									}
								}
							}catch(e){ ; }
							*/

							for( var i = 0; i < data[a].products[b].fittings[c].images.length; i++){	
								try{
									var what = "";
									if( data[a].products[b].fittings[c].images[i].fitting_image_type == "1" ){
		        						what = "Front";
		        					}else if( data[a].products[b].fittings[c].images[i].fitting_image_type == "2" ){
		        						what = "Side";
		        					}else if( data[a].products[b].fittings[c].images[i].fitting_image_type == "3" ){
		        						what = "Back";
		        					}else if( data[a].products[b].fittings[c].images[i].fitting_image_type == "4" ){
		        						what = "Custom";		
		        					}
		        					
									var tmp = localStorage.getItem("Fitting" + what + "ImagePhoto0");
	        		   				tmp = (tmp === null) ? [] : JSON.parse(tmp);
			        		   		var customer_id = orderItem.custSelectVM.selectedCustomer().id;
			        		   		var image_id = data[a].products[b].fittings[c].images[i].image_id;
			        		   		var image = data[a].products[b].fittings[c].images[i].fitting_image_path;
			        		   		var thumbnail = data[a].products[b].fittings[c].images[i].fitting_image_path;
		        					tmp.push({customer_id:customer_id,image_id:image_id,picture:image,thumbnail:thumbnail});

			           				localStorage.setItem("Fitting" + what + "ImagePhoto0", JSON.stringify(tmp));
									tmp = localStorage.getItem("Fitting" + what + "ImageTags0");
	        		   				tmp = (tmp === null) ? [] : JSON.parse(tmp);
									var servertags = JSON.parse(data[a].products[b].fittings[c].images[i].fitting_image_tags);
									if(servertags != null){
										for(var ind = 0; ind < servertags.length; ind++){
											tmp.push({image_id:image_id,left:servertags[ind].left,top:servertags[ind].top,text:servertags[ind].text});
										}
										localStorage.setItem("Fitting" + what + "ImageTags0", JSON.stringify(tmp));
									}else{
										if( what == "Front" ){
											if(data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0"){
												Fitting.NoFrontTags = true;
											}
										}else if( what == "Side" ){
		        							if(data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0"){
												Fitting.NoSideTags = true;
											}
		        						}else if( what == "Back" ){
			        						if(data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0"){
												Fitting.NoBackTags = true;
											}
			        					}	
									}
								}catch(e){ ; }
							}

/*
console.log("Fitting.NoFrontTags: " + Fitting.NoFrontTags);							
console.log("Fitting.NoSideTags: " + Fitting.NoSideTags);
console.log("Fitting.NoBackTags: " + Fitting.NoBackTags);
console.log("Fitting.SkippedFrontTags: " + Fitting.SkippedFrontTags);
console.log("Fitting.SkippedSideTags: " + Fitting.SkippedSideTags);
console.log("Fitting.SkippedBackTags: " + Fitting.SkippedBackTags);
*/
						}
						
						if(data[a].products[b].garment == 'Suit/J' || data[a].products[b].garment == 'Jacket' || data[a].products[b].garment == 'jacket'){
							for(var j in Fitting.OrderMeasurements = data[a].measurements){
								if(Fitting.OrderMeasurements = data[a].measurements[j].customer_jacket_measurements != undefined){				
									Fitting.OrderMeasurements = data[a].measurements[j].customer_jacket_measurements[0];	
									break;	
								}
							}
							if(c == -1){
								Fitting.FittingMeasurements = JSON.parse('{"fittings_jacket_measurements_chest":"0.00","fittings_jacket_measurements_stomach":"0.00","fittings_jacket_measurements_hips":"0.00","fittings_jacket_measurements_full_shoulder":"0.00","fittings_jacket_measurements_sleeve_left":"0.00","fittings_jacket_measurements_sleeve_right":"0.00","fittings_jacket_measurements_front":"0.00","fittings_jacket_measurements_back":"0.00","fittings_jacket_measurements_bicep":"0.00","fittings_jacket_measurements_fore_arms":"0.00","fittings_jacket_measurements_wrist":"0.00","fittings_jacket_measurements_length":"0.00","fittings_jacket_measurements_lapel_length":"0.00","fittings_jacket_measurements_neck":"0.00","fittings_jacket_measurements_zero_point":"0.00","fittings_jacket_measurements_front_shoulder_to_zero_point":"0.00","fittings_jacket_measurements_back_shoulder_to_zero_point":"0.00","fittings_jacket_measurements_notes":""}');
							}else{
								Fitting.FittingMeasurements = data[a].products[b].fittings[c].fitting_measurements.fittings_jacket_measurements[0];
							}	
						}else if( data[a].products[b].garment == 'Suit/P' || data[a].products[b].garment == 'Pants' || data[a].products[b].garment == 'pants' || data[a].products[b].garment == 'Pant' || data[a].products[b].garment == 'pant' ){							
							for(var j in Fitting.OrderMeasurements = data[a].measurements){
								if(Fitting.OrderMeasurements = data[a].measurements[j].customer_pants_measurements != undefined){
									Fitting.OrderMeasurements = data[a].measurements[j].customer_pants_measurements[0];
									break;			
								}
							}
							if(c == -1){		
								Fitting.FittingMeasurements = JSON.parse('{"fittings_pants_measurements_pantwaist":"0.00","fittings_pants_measurements_hips":"0.00","fittings_pants_measurements_thigh":"0.00","fittings_pants_measurements_cuffs":"0.00","fittings_pants_measurements_front_crotch":"0.00","fittings_pants_measurements_inseam":"0.00","fittings_pants_measurements_length":"0.00","fittings_pants_measurements_crotch":"0.00","fittings_pants_measurements_zipper":"0.00","fittings_pants_measurements_notes":""}');
							}else{
								Fitting.FittingMeasurements = data[a].products[b].fittings[c].fitting_measurements.fittings_pants_measurements[0];
							}	
						}else if(data[a].products[b].garment == 'Vest' || data[a].products[b].garment == 'vest'){			
							for(var j in Fitting.OrderMeasurements = data[a].measurements){
								if(Fitting.OrderMeasurements = data[a].measurements[j].customer_vest_measurements != undefined){
									Fitting.OrderMeasurements = data[a].measurements[j].customer_vest_measurements[0];
									break;			
								}
							}	
							if(c == -1){
								Fitting.FittingMeasurements = JSON.parse('{"fittings_vest_measurements_hips":"0.00","fittings_vest_measurements_length":"0.00","fittings_vest_measurements_chest":"0.00","fittings_vest_measurements_stomach":"0.00","fittings_vest_measurements_neck":"0.00","fittings_vest_measurements_half_shoulder_width":"0.00","fittings_vest_measurements_full_shoulder":"0.00","fittings_vest_measurements_v_length":"0.00","fittings_vest_measurements_notes":""}');
							}else{
								Fitting.FittingMeasurements = data[a].products[b].fittings[c].fitting_measurements.fittings_vest_measurements[0];
							}		
						}else if(data[a].products[b].garment == 'Shirt' || data[a].products[b].garment == 'shirt'){
							for(var j in Fitting.OrderMeasurements = data[a].measurements){
								if(Fitting.OrderMeasurements = data[a].measurements[j].customer_shirt_measurements != undefined){
									Fitting.OrderMeasurements = data[a].measurements[j].customer_shirt_measurements[0];
									break;			
								}
							}
							if(c == -1){
								Fitting.FittingMeasurements = JSON.parse('{"fittings_shirt_measurements_chest":"0.00","fittings_shirt_measurements_stomach":"0.00","fittings_shirt_measurements_full_shoulder":"0.00","fittings_shirt_measurements_sleeve_left":"0.00","fittings_shirt_measurements_sleeve_right":"0.00","fittings_shirt_measurements_neck":"0.00","fittings_shirt_measurements_front":"0.00","fittings_shirt_measurements_back":"0.00","fittings_shirt_measurements_length":"0.00","fittings_shirt_measurements_bicep":"0.00","fittings_shirt_measurements_hips":"0.00","fittings_shirt_measurements_wrist":"0.00","fittings_shirt_measurements_watch":"0.00","fittings_shirt_measurements_notes":""}');
							}else{
								Fitting.FittingMeasurements = data[a].products[b].fittings[c].fitting_measurements.fittings_shirt_measurements[0];
							}	
						}
						x++;
//console.log("Fitting: " + JSON.stringify(Fitting));
						iFitting.Fitting.push(Fitting);	
					}
				}
			}
			this._super(name, iFitting, dsRegistry);
		}	
	}
});


//END DEFINE CLOSURE
});
