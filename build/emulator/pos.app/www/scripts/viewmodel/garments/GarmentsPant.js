define(['jquery', 'knockout', 'base'], function($, ko) {
GarmentsPant = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;

		this.fabricsVM   = new FabricsCntClass(DsRegistry);
		this.fabricsVM.subscribeTo('fabricsDS');
		
		this.PantData     = [];
		this.PantDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);
		
		this.nextStepOLD = function() {
			var cs = that.currentStep().id;			
			if(cs == 0){
				if(that.PantFabric().title != "none"){
					that.workflow()[cs].completed = true;
				}	
			}else{
				that.workflow()[cs].completed = true;	
			}
			var foundnotcompletedstep = false;
			for(var x = cs+1; x < that.workflow().length; x++){
				if(that.workflow()[x].completed == false){
					that.currentStep(that.workflow()[x] );
					foundnotcompletedstep = true;
					break;
				}
			}				
			if(foundnotcompletedstep == false){
				for(var x = 0; x <= cs ; x++){
					if(that.workflow()[x].completed == false){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}
			}
			if(foundnotcompletedstep == false){
				$.mobile.changePage('#orderItemSelectGarment');
			}
		}
		
		
		this.nextStep = function() {
			
			var cs = that.currentStep().id;
				
			var notcompletedstep = false;						
			if(cs == 0){
				for(var x = 0; x < that.PantData.length; x++){		
					if(that.PantData[x].parentSuitTimestampID == ''){						
						if (that.PantData[x].PantFabric.title == 'none' ) {
							notcompletedstep = true;
							break;
						}	
					}	
				}
			}else if(cs == 1){
				for(var x = 0; x < that.PantData.length; x++){
					if(that.PantData[x].parentSuitTimestampID == ''){
						if (that.PantData[x].pantFit.id == 0) {
							notcompletedstep = true;
							break;
						}	
					}	
				}			
			}else if(cs == 2){
				for(var x = 0; x < that.PantData.length; x++){
					if(that.PantData[x].parentSuitTimestampID == ''){
						if (that.PantData[x].pantPleats.id == 0) {
							notcompletedstep = true;
							break;
						}
					}		
				}
			}else if(cs == 3){
				for(var x = 0; x < that.PantData.length; x++){
					if(that.PantData[x].parentSuitTimestampID == ''){
						if (that.PantData[x].pantPockets.id == 0  || (that.PantData[x].pantPockets.id == 123 && ( that.PantData[x].pantsCustomPocketsImage == "" && that.PantData[x].pantsCustomPocketsNotes.trim() == "") ) ) {
							notcompletedstep = true;
							break;
						}	
					}	
				}
			}else if(cs == 4){
				for(var x = 0; x < that.PantData.length; x++){
					if(that.PantData[x].parentSuitTimestampID == ''){
						if (that.PantData[x].bpantPockets.id == 0) {
							notcompletedstep = true;
							break;
						}	
					}	
				}
			}else if(cs == 5){
				for(var x = 0; x < that.PantData.length; x++){
					if(that.PantData[x].parentSuitTimestampID == ''){
						if (that.PantData[x].beltLoopStyle.id == 0) {
							notcompletedstep = true;
							break;
						}
					}	
				}
			}
			
			if(notcompletedstep == false){ 
				that.workflow()[cs].completed = true;
				var foundnotcompletedstep = false;
				for(var x = cs+1; x < that.workflow().length; x++){
					if(that.workflow()[x].completed == false){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}			
				if(notcompletedstep == false){
					for(var x = 0; x <= cs ; x++){
						if(that.workflow()[x].completed == false){
							that.currentStep(that.workflow()[x] );
							foundnotcompletedstep = true;
							break;
						}
					}
				}
				if(foundnotcompletedstep == false){
					$.mobile.changePage('#orderItemSelectGarment');
				}				
				
			}else{
				that.workflow()[cs].completed = false;
				that.currentStep(that.workflow()[cs] );
				customAlert("Please fill in all necessary fields");
			}
			
		}
				
		


		this.workflow    = ko.observableArray([
			{id: 0, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Fabric" },
			{id: 1, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Pant Style" },
			{id: 2, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Pleats" },
			{id: 3, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Front Pockets" },
			{id: 4, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Back Pockets" },
			{id: 5, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Belt & Cuffs" },
			{id: 6, target: "#garmentsPant",  completed: false, caption: "Pant", title: "Notes" } 
		]);

		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			
			if(data.id == 4){
				that.PreviewVisible('back');
			}else{
				that.PreviewVisible('front');
			}	
			
			$.mobile.changePage(data.target);
			that.renderPant();
		});

		this.isStepCompleted = ko.computed(function() {

			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					return that.workflow()[ind].completed;
				}
			}
			console.log('isStepCompleted bo');
			return false;
		});

	
		this.setAsCompleted =  function(id, status) {
		//	if (status) {console.log(id + ' setAdCompleted ' + status);}
			var tWork = that.workflow();
			tWork[id].completed = status;
			//that.workflow(tWork);
		};


		this.PantStepACompletion = function() {
			/*
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].PantFabric.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].beltLoopStyle.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].cuffsStyle.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].pantFit.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].pantPleats.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].bpantPockets.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			that.setAsCompleted(0, true);
			*/
		};

		this.PantStepBCompletion = function() {
			/*
			for ( var ind in that.PantData ) {
				if (that.PantData[ind].PantLapelStyle.id == 0 ) {
					that.setAsCompleted(1, false);
						return;
					}
			}
			that.setAsCompleted(1, true);
			*/
		};


		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});


		this.completion       = ko.observable(0);
		this.variantNamePant  = ko.observableArray([{id: 0, title: "Pant 1"}]);
		this.selectedVariantPant  = ko.observable(this.variantNamePant()[0]);
		this.selectedVariantPant.subscribe(function(data) {
			$('.cloneDialog').remove();		
                        that.selectVariantPant();
		});

		//Prev/Next computedz

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== true) {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {

				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});

		this.PantsNotes = ko.observable('');
		this.PantsNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantsNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});

		this.PantFabricNotes = ko.observable('');
		this.PantFabricNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantFabricNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.PantStyleNotes = ko.observable('');
		this.PantStyleNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantStyleNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.PantPleatsNotes = ko.observable('');
		this.PantPleatsNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantPleatsNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.PantFrontPocketsNotes = ko.observable('');
		this.PantFrontPocketsNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantFrontPocketsNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.PantBackPocketsNotes = ko.observable('');
		this.PantBackPocketsNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantBackPocketsNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.PantBeltAndCuffsNotes = ko.observable('');
		this.PantBeltAndCuffsNotes.subscribe(function(data) {		
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantBeltAndCuffsNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});				


		this.pantsCustomPocketsNotes = ko.observable('');
		this.pantsCustomPocketsNotes.subscribe(function(data) {
console.log("ADDING DATA TO pantsCustomPocketsNotes");
console.log(data);				
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantsCustomPocketsNotes = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.pantsCustomPocketsImage = ko.observable('');
		this.pantsCustomPocketsImage.subscribe(function(data) {
console.log("ADDING DATA TO pantsCustomPocketsImage");
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantsCustomPocketsImage = json;
			
			that.flushModel();
			that.PantStepACompletion();
		});


		//PantFabric
		this.PantFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.PantFabric.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantFabric = data;
			that.flushModel();
			that.PantStepACompletion();
		});

		//PantContrastFabric
		this.PantContrastFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.PantContrastFabric.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantContrastFabric = data;
			that.flushModel();
			that.PantStepACompletion();
		});


                //we have to delete this its being replace
		//beltLoopStyle
		this.beltLoopStyleList   = ko.observable(that.dsRegistry.getDatasource('beltLoopStyleDS').getStore());
		this.beltLoopStyleListNoZeroElement   = ko.observable(that.dsRegistry.getDatasource('beltLoopStyleDS').getStore().slice(1));
		this.beltLoopStyle       = ko.observable(that.dsRegistry.getDatasource('beltLoopStyleDS').getStore()[0]);
		this.beltLoopStyle.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].beltLoopStyle = data;
			that.flushModel();
			that.PantStepACompletion();
		});


/*
		this.PantsAngledBeltLoop = ko.observable(false);
		this.PantsAngledBeltLoop.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantsAngledBeltLoop = data;
			that.flushModel();
			that.PantStepACompletion();
		});
*/

		this.pantCuffs        = ko.observable(false);
		this.pantCuffs.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantCuffs = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		
		this.PantsBracesProvisions        = ko.observable(false);
		this.PantsBracesProvisions.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantsBracesProvisions = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		
		//cuffsStyle
		this.cuffsStyleList   = ko.observable(that.dsRegistry.getDatasource('cuffsStyleDS').getStore());
		this.cuffsStyle       = ko.observable(that.dsRegistry.getDatasource('cuffsStyleDS').getStore()[0]);
		this.cuffsStyle.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].cuffsStyle = data;
			that.flushModel();
			that.PantStepACompletion();
		});

		
		//pantFit
		this.pantFitList   = ko.observable(that.dsRegistry.getDatasource('pantFitDS').getStore());
		this.pantFit       = ko.observable(that.dsRegistry.getDatasource('pantFitDS').getStore()[0]);
		this.pantFit.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantFit = data;
			that.flushModel();
			that.PantStepACompletion();
		});


		//pantPleats
		this.pantPleatsList   = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore());
		this.pantPleatsListNoZeroElement   = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore().slice(1));
		this.pantPleats       = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore()[0]);
		this.pantPleats.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantPleats = data;
			that.flushModel();
			that.PantStepACompletion();
		});



        this.pantPleatsList   = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore());
		this.pantPleats       = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore()[0]);
		this.pantPleats.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantPleats = data;
			that.flushModel();
			that.PantStepACompletion();
		});



		this.bpantPocketsList   = ko.observable(that.dsRegistry.getDatasource('bpantPocketsDS').getStore());
		this.bpantPocketsListNoZeroElement   = ko.observable(that.dsRegistry.getDatasource('bpantPocketsDS').getStore().slice(1));
		this.bpantPockets       = ko.observable(that.dsRegistry.getDatasource('bpantPocketsDS').getStore()[2]);
		this.bpantPockets.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].bpantPockets = data;
			that.flushModel();
			that.PantStepACompletion();
		});

/*
		this.PantsBackPocketButton = ko.observable(true);
		this.PantsBackPocketButton.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantsBackPocketButton = data;
			that.flushModel();
			that.PantStepACompletion();
		});
*/

     	this.pbackpocketMethodList    = ko.observableArray(["Both","Right","Left"]);
		this.pbackpocketMethod     = ko.observable('');
		this.pbackpocketMethod.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pbackpocketMethod = data;
			that.flushModel();
			that.PantStepACompletion();
		});


        this.pantPocketsList   = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore());
        this.pantPocketsListNoZeroElement   = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore().slice(1));
        this.pantPocketsListPleats   = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore().slice(0, 3));
        this.pantPocketsListPleatsNoZeroElement   = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore().slice(1, 3));

		this.pantPockets       = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore()[1]);
		this.pantPockets.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantPockets = data;
			that.flushModel();
			that.PantStepACompletion();
		});        
		this.pantPockets1       = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore()[1]);
		this.pantPockets1.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantPockets1 = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.pantPockets2       = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore()[1]);
		this.pantPockets2.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].pantPockets2 = data;
			that.flushModel();
			that.PantStepACompletion();
		});


		//PantLining 
		this.PantLiningList = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore());
		this.PantLining     = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore()[0]);
		this.PantLining.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantLining = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		
		
		this.PantFitList = ko.observableArray(["Fitted", "Semi Fitted", "Standard Fit"]);//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore());
		this.PantFit = ko.observable('Semi Fitted');//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore()[2]);
        this.PantFit.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].PantFit = data;
			that.flushModel();
			that.PantStepACompletion();
		});
 

		this.urgent        = ko.observable(0);
		this.urgent.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].urgent = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		/*
		this.urgentDate        = ko.observable('');
		this.urgentDate.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].urgentDate = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		*/
		this.DOP_day        = ko.observable('');
		this.DOP_day.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].DOP_day = data;
			that.flushModel();
			that.PantStepACompletion();
		});
		this.DOP_month        = ko.observable('');
		this.DOP_month.subscribe(function(data) {
			that.PantData[ that.getRow(that.selectedVariantPant().id)  ].DOP_month = data;
			that.flushModel();
			that.PantStepACompletion();
		});		

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
		this.PreviewVisible = ko.observable('front');		

		this.PantFabricSelect = function() {
//customAlert("11aa pans ciaaac" );
			that.dsRegistry.setContext({
				targetModel:     'garmentsPantDS',
				targetGarment:   'Pant',
				targetDelta:      this.getRow(this.selectedVariantPant().id),
				targetAttribute: 'PantFabric',
				callbackPage:    '#garmentsVestFabric' //'#garmentsPant'//'#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[that.getRow(that.selectedVariantPant().id)].PantFabric;
			that.fabricsVM.setSelectedFabric(fab);
                        
                       // customAlert("fab " + fab);
		};


		this.PantContrastFabricSelect = function() {
//customAlert("11 pans 22");
			that.dsRegistry.setContext({
				targetModel:     'garmentsPantDS',
				targetGarment:   'Pant',
				targetDelta:      this.getRow(this.selectedVariantPant().id),
				targetAttribute: 'PantContrastFabric',
				callbackPage:      '#garmentsVestFabric'           //'#garmentsPantFabric'                    //'#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[that.getRow(that.selectedVariantPant().id)].PantContrastFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};

		this.fnClonePant = function(e) {
				fe = this;
				var parentOffset = $('.content-wrapper').offset(); 
				//or $(this).offset(); if you really just want the current element's offset
				relX = e.pageX - parentOffset.left;
				relY = e.pageY - parentOffset.top;

				//need testing
				if (relY > 500) relY -= 200;
				targetAttr = $(this).parent().parent().parent().attr('data-target-attr');
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().parent().attr('data-target-attr');	
				}	
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().attr('data-target-attr');	
				}	

				currentVariant = that.selectedVariantPant();
				var vnames = that.variantNamePant(); 
				if (vnames.length == 1) return;

				var fa  = 'Select Pant changes ';
			    fa += '<div class="cloneDialogBtnClose" data-role="button"><img src="http://shirt-tailor.net/thepos/appimg/template/topmenu/close.png"/></div><div  data-role="fieldcontain"><fieldset data-role="controlgroup">';
				for (var ind in vnames) {
					//if (vnames[ind].id != currentVariant.id) {
						check = '';
						if (vnames[ind].id == currentVariant.id) check = ' disabled checked="checked" ';
						fa += '<input type="checkbox" '+ check +' data-id="' + vnames[ind].id + '" id="cSuit-'+ vnames[ind].id +'" name="cSuit-'+ vnames[ind].id +'"/>'
						fa += '<label for ="cSuit-'+ vnames[ind].id +'">' + vnames[ind].title + '</label>';
					//}
				}
				fa += "</fieldset></div><div class='cloneDialogBtn' data-role='button'>Apply</div>";
				$('.cloneDialog').remove();
				$('.content-wrapper').append("<div class='cloneDialog' id='cloneSuitDialog' style='left: " + relX + "px !important; top: " + relY + "px !important;'>" + fa + "</div>");
				
				$('#cloneSuitDialog input').checkboxradio();
				$('.cloneDialogBtn').on('click', function() {
					cI = [];
					$('#cloneSuitDialog :checked').each(function(thet,c) {cI.push($(c).attr('data-id'));});
					that.clonePantsAttributes(currentVariant.id, targetAttr, cI);
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
					//$.mobile.sdCurrentDialog.close();
				});
				$('.cloneDialogBtnClose').on('click', function() {
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
				});
		};

		this.attachClonePantsAttribute = function() {
	//		$('#garmentsPant .gAttr img').on('click', that.fnClonePant);
	//		$('#garmentsPant .fbSelectBtn').on('click', that.fnClonePant);
	//		$('#garmentsPant .gAttr .mycheck').on('click', that.fnClonePant);
	//		$('#garmentsPant .textCloneBtn').on('click', that.fnClonePant);
	//		$('#garmentsPant .selectlist').on('change', that.fnClonePant);
		};

		this.clonePantsAttributes = function( pvId, pvTA, cvI)
		{
			var tAD = that.getVariant(pvId)[pvTA];
			for (var ind in that.PantData) {
				if ( cvI.indexOf(that.PantData[ind].variantId.toString()) != -1) {
					//console.log('cloning to ' + that.suitData[ind].variantId);
					that.PantData[ind][pvTA] = tAD; 
				}
			}
			that.flushModel();
		};

		this.deleteVariantPant = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							var currentcount = orderItem.garmentsList()[1].count();
							orderItem.garmentsList()[1].count( currentcount - 1);
							
							var tPantData = [];
							var tvarsData = [];
							for (var ind in that.variantNamePant() ) {
								if ( that.variantNamePant()[ind].id != data.id ) {
									tvarsData.push(that.variantNamePant()[ind]);
									tPantData.push(that.getVariant( that.variantNamePant()[ind].id ));
								}
							}
							that.PantData = tPantData;
							that.flushModel();
							that.variantNamePant(tvarsData);
							that.selectedVariantPant(that.variantNamePant()[0]);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};
		
		this.deleteVariantPant2 = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							var currentcount = orderItem.garmentsList()[1].count();
							orderItem.garmentsList()[1].count( currentcount - 1);
							
							var tPantData = [];
							var tvarsData = [];
							for (var ind in that.variantNamePant() ) {
								if ( that.variantNamePant()[ind].id != data.id ) {
									tvarsData.push(that.variantNamePant()[ind]);
									tPantData.push(that.getVariant( that.variantNamePant()[ind].id ));
								}
							}
							that.PantData = tPantData;
							that.flushModel();
							that.variantNamePant(tvarsData);
							that.selectedVariantPant(that.variantNamePant()[0]);
							
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "block";
							$.mobile.changePage('#bodyshape');
							$.mobile.changePage('#orderItemSelectGarment');
							setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },2400);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};		
		 

		this.deleteSuitExtraPants = function(suitTimestampID) { 
			var tPantData = [];
			var tvarsData = [];
			var extrascount = 0;
			for (var ind in that.variantNamePant() ) {
/*				
try{				
	console.log("parentSuitTimestampID " + that.getVariant( that.variantNamePant()[ind].id ).parentSuitTimestampID);
}catch(e){
	;
}
*/				
				if ( that.getVariant( that.variantNamePant()[ind].id ).parentSuitTimestampID != suitTimestampID ) {
					tvarsData.push(that.variantNamePant()[ind]);
					tPantData.push(that.getVariant( that.variantNamePant()[ind].id ));
				}else{
					extrascount++;
				}
			}
			var currentcount = orderItem.garmentsList()[1].count();
			orderItem.garmentsList()[1].count( currentcount - extrascount);
			that.PantData = tPantData;
			that.flushModel();
			that.variantNamePant(tvarsData);
			that.selectedVariantPant(that.variantNamePant()[0]);
		};	
		
		
		this.addVariantPantFromSuit = function(suitindex, initializationByExtra) { 
			
			var currentcount = orderItem.garmentsList()[1].count();	
			orderItem.garmentsList()[1].count( currentcount + 1);
			this.PantDataAID += 1;
			var newid = this.PantDataAID + 1;
			var vname = "Pant " + newid;
	
			var tObj = jQuery.extend(true, {}, this.getVariantFromSuit( suitindex )  ); //CLONE Object
//console.log("PANT DATA BEFORE: " + JSON.stringify(this.PantData));
			this.variantNamePant.push({title: vname, id: this.PantDataAID});
			tObj.variantId = this.PantDataAID;
			if(initializationByExtra == true){				
				this.PantData = [];
			}
			this.initializationByExtra = false;
			this.PantData.push(tObj);                    //Push to internal		
			this.flushModel();
//console.log("PANT DATA: " + JSON.stringify(this.PantData));			
			var spinner = document.getElementById('loading_jp');
			spinner.style.display = "block";
			orderItem.lockselects = true;
			//$.mobile.changePage('#orderItemPopulateGarments');
			orderItem.currentBigStep(orderItem.bigSteps()[1]);
			//$.mobile.changePage('#bodyshape');
			orderItem.currentBigStep(orderItem.bigSteps()[4]);
			//$.mobile.changePage('#orderItemSelectGarment');
			orderItem.currentBigStep(orderItem.bigSteps()[5]);
			//setTimeout(function() { /*var spinner = document.getElementById('loading_jp'); spinner.style.display = "none";*/ orderItem.initializeHtmlFromExtraPants();},2000);
			
			
			
		//	this.currentStep(this.workflow()[0] );
		//	var lastelemindex = this.variantNamePant().length -1;
		//	this.selectedVariantPant( this.variantNamePant()[lastelemindex] );
		};		
		
		
		

		ko.bindingHandlers.modifiedPantListOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var wantedValue = value[1];	
					var list = document.getElementsByName(value[2]);
					var checkboxes = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;
					var elemdivs = document.getElementsByName(value[5]);

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						if(wantedValue.id != undefined){
							checkValue = wantedValue.id; 
						}else{
							checkValue = wantedValue;
						}	
						if(spanvalue == checkValue){
							checkboxes[ index ].classList.add('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('showtrigger');
								elemdivs[ index ].classList.remove('offtrigger');
							}	
						}else{
							checkboxes[ index ].classList.remove('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('offtrigger');
								elemdivs[ index ].classList.remove('showtrigger');
							}	
						}	
					}
					
					if(value[4] != 'none'){
						var customdivs = document.getElementsByName(value[4]);
						if(customdivs[value[0]] != undefined){
							if(wantedValue.id == '123'){
								customdivs[value[0]].style.display = 'block';
							}else{
								customdivs[value[0]].style.display = 'none';
							}
						}	
					}
					
					
					if(value[6] != undefined && value[6] != 'none'){
						var mainoptionsdivs = document.getElementsByName(value[6]);
						var mainoptionscount = mainoptionsdivs.length;
						
						for(var a = 0; a < mainoptionscount; a++){
							var haschecked = false;
							for(var b = 0; b < garmentscount; b++){
								if(that.PantData[ b ].parentSuitTimestampID == ''){
									var index = a*garmentscount + b;
									if(checkboxes[index].classList.contains('selecteds')){
										haschecked = true;
										break;
									}
								}	
							}
							if(haschecked == true){
								mainoptionsdivs[a].classList.add('activate');
							}else{
								mainoptionsdivs[a].classList.remove('activate');
							}
						}
					}
					
					that.selectedVariantPant(that.variantNamePant()[value[0]]);
		        });
		    }
		};    

		ko.bindingHandlers.modifiedPantListBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( !checkboxes[ index ].classList.contains(value[3]) ){
							checkboxes[ index ].classList.add(value[3]);
						}else{
							checkboxes[ index ].classList.remove(value[3]);						
						}	
					}
					
					that.selectedVariantPant(that.variantNamePant()[value[0]]);					
		        });
		    }
		};
		
		ko.bindingHandlers.modifiedPantListSelectOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var selects = document.getElementsByName(value[2]);
					var garmentscount = selects.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						
						selects[ index ].value = that.PantData[ value[0] ][value[2]];

						if ("createEvent" in document) {
							var evt = document.createEvent("HTMLEvents");
		    				evt.initEvent("change", false, true);
							selects[ index ].dispatchEvent(evt);
						}else{
							selects[ index ].fireEvent("onchange");
						}												
						
					}
					that.selectedVariantPant(that.variantNamePant()[value[0]]);					
		        });
		    }
		};   		   


		ko.bindingHandlers.modifiedPantBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var checkboxes = document.getElementsByName(value[1]);
					var index = value[0];
					var cssclass = "" + value[2];

					if( !checkboxes[ index ].classList.contains(cssclass) ){
						checkboxes[ index ].classList.add(cssclass);
					}else{
						checkboxes[ index ].classList.remove(cssclass);
					}	
					if( value[1] != 'FabricCheck' ){
						that.selectedVariantPant(that.variantNamePant()[value[0]]);
					}	
		        });
		    }
		};


		ko.bindingHandlers.switchpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);

					for(var x = 0; x < popups.length; x++){
						if(x != value[0]){
							popups[x].style.display = "none";	
						}
					}
					if(popups[value[0]].style.display == "block"){
						popups[value[0]].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}else{
						popups[value[0]].style.display = "block";		
					}

		        });
		    }
		}; 
		

		ko.bindingHandlers.pantswitchcustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantPant(that.variantNamePant()[value[0]]);
					
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.PantData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		};
		
		ko.bindingHandlers.pantopencustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantPant(that.variantNamePant()[value[0]]);
					
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.PantData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					popup[0].style.display = "block";		
		        });
		    }
		}; 	
				 	
		
		ko.bindingHandlers.pantopencustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantPant(that.variantNamePant()[value[0]]);
					
					try{			
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.PantData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}	
					}catch(e){
						;
					}
						
					var popup = document.getElementsByName(value[1]);
					popup[0].style.display = "block";		

		        });
		    }
		}; 					
		

		ko.bindingHandlers.openpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);			
					popups[value[0]].style.display = "block";		//.show(), .dialog( "open" ) : not working
		        });
		    }
		}; 

		ko.bindingHandlers.closepopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[0]);
					for(var x = 0; x < popups.length; x++){			
						popups[x].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}	
		        });
		    }
		}; 
		


/*
		ko.bindingHandlers.modifiedPantThreads = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var images = document.getElementsByName(value[3]);
					var options = document.getElementsByName(value[4]);
					images[value[0]].src = value[2].image;

					var garmentssize = options.length/images.length;
					for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[garmentssize*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantPant(that.variantNamePant()[value[0]]);
					
					if(value[5] != undefined && value[6] != undefined && value[7] != undefined && value[8] != undefined){
						if(that.PantInsidePocketsDifferentFromPiping() == false){
							var images2 = document.getElementsByName(value[5]);
							var options2 = document.getElementsByName(value[6]);
							images2[value[0]].src = value[2].image;
							var garmentssize = options2.length/images2.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options2[x].classList.remove('selected');	
							}
							options2[garmentssize*value[0] + value[1]].classList.add('selected');
						}
						if(that.PantMonogramStitchDifferentFromPiping() == false){
							var images3 = document.getElementsByName(value[7]);
							var options3 = document.getElementsByName(value[8]);
							images3[value[0]].src = value[2].image;
							var garmentssize = options3.length/images3.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options3[x].classList.remove('selected');	
							}
							options3[garmentssize*value[0] + value[1]].classList.add('selected');						
						}		
					}
		        });
		    }
		}; 
*/		
		
		ko.bindingHandlers.modifiedPantColors = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var listdivs = document.getElementsByName(value[3]);					
					var listimages = document.getElementsByName(value[4]);
					var listtexts = document.getElementsByName(value[5]);
					var options = document.getElementsByName(value[6]);
					var garmentssize = document.getElementById(value[7]).innerHTML;
					var Pantoptionsnumber = listdivs.length/garmentssize;
					var coloroptionsnumber = options.length/garmentssize;
					
					for(var x = 0; x < Pantoptionsnumber; x++){
						var y = value[0] + x*garmentssize;
						if(listimages.length > 0){	 				
							listimages[y].src = value[2].image;
						}
						if(listtexts.length > 0){
							listtexts[y].innerHTML = value[2].title;
						}
					}					
					for(var x = coloroptionsnumber*value[0]; x < coloroptionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[coloroptionsnumber*value[0] + value[1]].classList.add('selected');
		        });
		    }
		};		
		
		
		ko.bindingHandlers.modifiedPantFit = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					for(var x = 0; x < that.PantData.length; x++){
						that.PantData[ x ].PantFit = value;	
					}
					that.flushModel();
		        });
		    }
		}; 		
		

		ko.bindingHandlers.modifiedPantDropdown = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					that.selectedVariantPant(that.variantNamePant()[value[0]]);
		        });
		    }
		}; 
		
		/*
		ko.bindingHandlers.pantslider = {
		    init: function(element, valueAccessor, allBindingsAccessor) {
		        //initialize the control
		        var options = allBindingsAccessor().pantslideroptions || {};
		        $(element).slider(options);
		
		        //handle the value changing in the UI
		        ko.utils.registerEventHandler(element, "slidechange", function() {
		            //would need to do some more work here, if you want to bind against non-observables
		            var values = valueAccessor(); 
		            var observable = values[0];
		            observable($(element).slider("value"));
		        });
		
		    },
		    //handle the model value changing
		    update: function(element, valueAccessor) {
		    	var values = valueAccessor();
		        var value = ko.utils.unwrapObservable(values[0]);
		       // $(element).slider("value", value);
				var maindivs = document.getElementsByName(values[1]);
				var alltexts = document.getElementsByName(values[2]);
				var garmentscount = alltexts.length/maindivs.length;
				var optionscount = maindivs.length;
				
				for(var a = 0; a < maindivs.length; a++){
					for(var b = 0; b < garmentscount; b++){
						var index = a*garmentscount + b;						
						alltexts[index].innerHTML = that.PantData[ b ].PantLapelWidth; 
					}	
				}
				
				try{
					$(element).val(value).slider('refresh');
				}catch(e){
					;
				} 

		    }
		};		
		*/
		
		
		ko.bindingHandlers.modifyClassOfDiv = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {	        	  		        	      	
					var value = valueAccessor();
					var index = value[0];
					var divs = document.getElementsByName(value[1]);
					var classname = 'activated'; //value[2];
					var activeclassname = 'activate';//value[3];
					//var notactiveclassname = value[4];
					
					var addtheclass = true;
					if( divs[ index ].classList.contains(classname) ){
						addtheclass = false;
					}
					for(var x = 0; x < divs.length; x++){
						divs[ x ].classList.remove(classname);
					}		
					if(addtheclass == true){			
						divs[ index ].classList.add(classname);
					}	
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.pantFabricApplier = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
			//		for(var x = 0; x < value.length; x++){
			//			console.log("VALUE " + x + ": " + JSON.stringify(value[x]) );
			//		}
					
					var checkboxes = document.getElementsByName(value[0]);
					var texts = document.getElementsByName(value[3]);
					var images = document.getElementsByName(value[4]);
					var cssclass = "" + value[2]; 
					var tomakegreenelements = document.getElementsByClassName("tomakegreen");
					
					var garmentsnumber = checkboxes.length;
					var textsnumber = texts.length;
					var differenttypesnumber = textsnumber/garmentsnumber;

					var anychecked = false;
					
					for(var x = 0; x < checkboxes.length; x++){
						if( checkboxes[x].classList.contains(cssclass) && document.getElementById("selectedfabric").innerHTML != "none"){
							checkboxes[x].classList.remove(cssclass);
							tomakegreenelements[x].classList.add('makegreen');
							anychecked = true;
							that.PantData[ x ].PantFabric = value[1];
							if(texts.length > 0){
								//texts[x].innerHTML = value[1].title;
								for(var y = 0; y < differenttypesnumber; y++){
									var pos = y*garmentsnumber + x;
									texts[pos].innerHTML = value[1].title;
								}
							}
							if(images.length > 0){
								images[x].src = value[1].fabricImage;
							}
						}
					}					
					if(anychecked == true){
						document.getElementById("selectedfabric").innerHTML = "none";
						document.getElementById("fabricrange").innerHTML = "0";
						document.getElementById("composition").innerHTML = "0";
						document.getElementById("fabricinput").value = "";
						document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
						document.getElementById("fabricinfo").style.display = "none";
					}
		        });
		    }
		}; 
		
		
		this.pantPleatsPocketsDependencies = function( value0, value1 ){
			var pantPocketsList = document.getElementsByName(value0);
			var pantPocketsEachGarment = document.getElementsByName(value1);
			var garmentsnumber = pantPocketsEachGarment.length/pantPocketsList.length;
			
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < pantPocketsList.length; y++){
					var pos = y*garmentsnumber + x;
					if(pos >= garmentsnumber*3){
						if(that.PantData[ x ].pantPleats.title != 'No Pleats' && that.PantData[ x ].pantPleats.title !== 'Single Pleat' && that.PantData[ x ].pantPleats.title !== 'Select Pleats'){
							pantPocketsEachGarment[pos].style.display = "none";
							
							if( that.PantData[ x ].pantPockets.title != 'Select Pocket' && that.PantData[ x ].pantPockets.title != 'No Pocket' && that.PantData[ x ].pantPockets.title != 'Slanted'){
								that.PantData[ x ].pantPockets = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
								if ("createEvent" in document) {
				    				var evt = document.createEvent("HTMLEvents");
				    				evt.initEvent("click", true, true);
				    				document.getElementsByName('pantPockets')[x].dispatchEvent(evt);
				    			}else{
				    				document.getElementsByName('pantPockets')[x].fireEvent("onclick");			
				    			}		
							}
						}else{
							pantPocketsEachGarment[pos].style.display = "block";
						}		
					}	
				}
			}					
		}; 		
				
			

	},
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
/*
	nextStep: function() {
		var tWork = this.workflow();
		for (var ind in tWork ) {
				if (!tWork[ind].completed) {
				this.currentStep( tWork[ind] );
				return;
			}
		}
	},*/
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"Pant"  : this.PantData
		}, true);
	},

	selectVariantPant: function() {
		this.renderPant();
	},

	getVariant: function(id) {
		
		var toreturn = this.PantData[0];
		for (var ind in this.PantData) {
			if ( this.PantData[ind].variantId == id  ) {
				toreturn = this.PantData[ind];
			}
		}

		return toreturn;

	},
	
	getVariantFromSuit: function(index) {
		 // CODE FOR GETTING PANT DATA FROM SUIT
		//	console.log("test: " + JSON.stringify( this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit ) );		
		var obj = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index];
		obj.PantFabric = obj.suitFabric;
		obj.PantsNotes = obj.SuitNotes;
		obj.parentSuitTimestampID = obj.timestampID;	// set parent suit id
//console.log("SUIT TO COPY FROM: " + JSON.stringify(obj));			
		return obj;

	},	

	getRow: function(id) {
		for (var ind in this.PantData) {
			if ( this.PantData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},

	addVariantPant: function() {
		//var vname = "Pant " + (this.variantNamePant().length + 1 );
		this.PantDataAID += 1;
		var newid = this.PantDataAID + 1;
		var vname = "Pant " + newid;

		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantPant().id )  ); //CLONE Object
		this.variantNamePant.push({title: vname, id: this.PantDataAID});
		if(tObj.parentSuitTimestampID == undefined){
			tObj.parentSuitTimestampID = '';
		}
		tObj.variantId = this.PantDataAID;
		if(orderItem.pantfirsttime == true){
			this.PantData.push(tObj);                                                       //Push to internal
		}	
		this.flushModel();
	},


	addVariantPant2: function() { 
		
		var currentcount = orderItem.garmentsList()[1].count();	
		orderItem.garmentsList()[1].count( currentcount + 1);
		
		//var vname = "Pant " + (this.variantNamePant().length + 1 );
		this.PantDataAID += 1;
		var newid = this.PantDataAID + 1;
		var vname = "Pant " + newid;

		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantPant().id )  ); //CLONE Object
		this.variantNamePant.push({title: vname, id: this.PantDataAID});

		if(tObj.parentSuitTimestampID == undefined){
			tObj.parentSuitTimestampID = '';
		}
		tObj.variantId = this.PantDataAID;
		this.PantData.push(tObj);                                                       //Push to internal
		this.flushModel();
		this.currentStep(this.workflow()[0] );
		var lastelemindex = this.variantNamePant().length -1;
		this.selectedVariantPant( this.variantNamePant()[lastelemindex] );
	},

/*
	addVariantPantFromSuit: function(suitindex) { 
		
		var currentcount = orderItem.garmentsList()[1].count();	
		orderItem.garmentsList()[1].count( currentcount + 1);
		this.PantDataAID += 1;
		var newid = this.PantDataAID + 1;
		var vname = "Pant " + newid;

		var tObj = jQuery.extend(true, {}, this.getVariantFromSuit( suitindex )  ); //CLONE Object
		this.variantNamePant.push({title: vname, id: this.PantDataAID});

           //     customAlert(this.variantNamePant.length + " -- " +  this.PantDataAID + " ---" + this.variantNamePant().length);
		
		tObj.variantId = this.PantDataAID;
		this.PantData.push(tObj);                                                       //Push to internal
		this.flushModel();
		this.currentStep(this.workflow()[0] );
		var lastelemindex = this.variantNamePant().length -1;
		this.selectedVariantPant( this.variantNamePant()[lastelemindex] );
	},
*/


	digestData: function(data) {
		this.PantData  = data.Pant;
		this.renderView();

	},

	renderView: function() {
		this.renderPant();
	},

	renderPant: function() {
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantPant().id);
			if (tData != null) {
	
				//Update observables
				if (typeof(tData.PantsNotes)    != "undefined") {this.PantsNotes(tData.PantsNotes);}
				if (typeof(tData.PantFabricNotes)    	!= "undefined") {this.PantFabricNotes(tData.PantFabricNotes);}
				if (typeof(tData.PantStyleNotes)    	!= "undefined") {this.PantStyleNotes(tData.PantStyleNotes);}
				if (typeof(tData.PantPleatsNotes)    	!= "undefined") {this.PantPleatsNotes(tData.PantPleatsNotes);}
				if (typeof(tData.PantFrontPocketsNotes) != "undefined") {this.PantFrontPocketsNotes(tData.PantFrontPocketsNotes);}
				if (typeof(tData.PantBackPocketsNotes)  != "undefined") {this.PantBackPocketsNotes(tData.PantBackPocketsNotes);}
				if (typeof(tData.PantBeltAndCuffsNotes) != "undefined") {this.PantBeltAndCuffsNotes(tData.PantBeltAndCuffsNotes);}
				if (typeof(tData.pantsCustomPocketsNotes)    != "undefined") {this.pantsCustomPocketsNotes(tData.pantsCustomPocketsNotes);}
				if (typeof(tData.pantsCustomPocketsImage)    != "undefined") {this.pantsCustomPocketsImage(tData.pantsCustomPocketsImage);}			
				if (typeof(tData.PantFabric)			!= "undefined") {this.PantFabric(tData.PantFabric);}
				if (typeof(tData.PantContrastFabric)	!= "undefined") {this.PantContrastFabric(tData.PantContrastFabric);}
				if (typeof(tData.beltLoopStyle)			!= "undefined") {this.beltLoopStyle(tData.beltLoopStyle);}
				if (typeof(tData.cuffsStyle)			!= "undefined") {this.cuffsStyle(tData.cuffsStyle);}
				if (typeof(tData.pantFit)				!= "undefined") {this.pantFit(tData.pantFit);}
				if (typeof(tData.pantPleats)			!= "undefined") {this.pantPleats(tData.pantPleats);}
				if (typeof(tData.bpantPockets)			!= "undefined") {this.bpantPockets(tData.bpantPockets);}
				if (typeof(tData.pbackpocketMethod)		!= "undefined") {this.pbackpocketMethod(tData.pbackpocketMethod);}
				if (typeof(tData.pantCuffs)				!= "undefined") {this.pantCuffs(tData.pantCuffs);}
				if (typeof(tData.PantsBracesProvisions)	!= "undefined") {this.PantsBracesProvisions(tData.PantsBracesProvisions);}
		//		if (typeof(tData.PantsBackPocketButton)	!= "undefined") {this.PantsBackPocketButton(tData.PantsBackPocketButton);}
		//		if (typeof(tData.PantsAngledBeltLoop)	!= "undefined") {this.PantsAngledBeltLoop(tData.PantsAngledBeltLoop);}
				if (typeof(tData.pantPockets)			!= "undefined") {this.pantPockets(tData.pantPockets);}               
				if (typeof(tData.pantPockets1)			!= "undefined") {this.pantPockets1(tData.pantPockets1);}
				if (typeof(tData.pantPockets2)			!= "undefined") {this.pantPockets2(tData.pantPockets2);}
				if (typeof(tData.PantFit)				!= "undefined") {this.PantFit(tData.PantFit);}
				
				if (typeof(tData.urgent)			!= "undefined") {this.urgent(tData.urgent);}
				//if (typeof(tData.urgentDate)		!= "undefined") {this.urgentDate(tData.urgentDate);}
				if (typeof(tData.DOP_day)		!= "undefined") {this.DOP_day(tData.DOP_day);}
				if (typeof(tData.DOP_month)		!= "undefined") {this.DOP_month(tData.DOP_month);}
				
				if ( this.currentStep().id == 0) this.PantFabricSelect();
				if ( this.currentStep().id == 4) this.PantContrastFabricSelect();
			}
		}catch(e){
			;
		}
	}
});

defPant = SimpleDatasource.extend({
	init: function(name, dsRegistry, olddata, count) {
		if(olddata == null || olddata == undefined){
			var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
			var Pant = {};
			Pant.variantId        = 0;
			Pant.parentSuitTimestampID = '';
		
	
			Pant.PantFabric         = df;
			Pant.PantContrastFabric = df;
			Pant.PantsNotes	= '';
			Pant.PantFabricNotes		= '';
			Pant.PantStyleNotes			= '';
			Pant.PantPleatsNotes		= '';
			Pant.PantFrontPocketsNotes	= '';
			Pant.PantBackPocketsNotes	= '';
			Pant.PantBeltAndCuffsNotes	= '';
			
			Pant.pantsCustomPocketsNotes	= '';
			Pant.pantsCustomPocketsImage	= '';		
			Pant.beltLoopStyle      = dsRegistry.getDatasource('beltLoopStyleDS').getStore()[0];
	//		Pant.PantsAngledBeltLoop = false;
			Pant.pantCuffs          = false;
			Pant.PantsBracesProvisions         = false;
			Pant.pbackpocketMethod  = "Both";
	//		Pant.PantsBackPocketButton = true;
			Pant.cuffsStyle         = dsRegistry.getDatasource('cuffsStyleDS').getStore()[0];
			Pant.pantFit            = dsRegistry.getDatasource('pantFitDS').getStore()[0];
			Pant.pantPleats         = dsRegistry.getDatasource('pantPleatsDS').getStore()[0];
			Pant.bpantPockets      = dsRegistry.getDatasource('bpantPocketsDS').getStore()[0];
			Pant.pantPockets       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
	        Pant.pantPockets1       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
	        Pant.pantPockets2       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
	        Pant.PantFit      		= 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2]
	        
	        Pant.urgent = false;
	      //  Pant.urgentDate = '';
	        Pant.DOP_day = '';
			Pant.DOP_month = '';
	        
			var iPant = {
				Pant: []
			};
	
	
			iPant.Pant.push(Pant);
			this._super(name, iPant, dsRegistry);
			
		}else{
			var iPant = {
				Pant: []
			};
			
			for(var x = 0; x < count; x++){
				
				if(olddata[x] != undefined){
					
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var Pant = {};
					Pant.variantId        = x;
					Pant.parentSuitTimestampID = olddata[x].parentSuitTimestampID;
			
					Pant.PantFabric         = olddata[x].PantFabric;
					Pant.PantContrastFabric = olddata[x].PantContrastFabric;
					Pant.PantsNotes	= olddata[x].PantsNotes;
					Pant.PantFabricNotes		= olddata[x].PantFabricNotes;
					Pant.PantStyleNotes			= olddata[x].PantStyleNotes;
					Pant.PantPleatsNotes		= olddata[x].PantPleatsNotes;
					Pant.PantFrontPocketsNotes	= olddata[x].PantFrontPocketsNotes;
					Pant.PantBackPocketsNotes	= olddata[x].PantBackPocketsNotes;
					Pant.PantBeltAndCuffsNotes	= olddata[x].PantBeltAndCuffsNotes;
					
					Pant.pantsCustomPocketsNotes	= olddata[x].pantsCustomPocketsNotes;
					Pant.pantsCustomPocketsImage	= olddata[x].pantsCustomPocketsImage;		
					Pant.beltLoopStyle      = olddata[x].beltLoopStyle;
			//		Pant.PantsAngledBeltLoop = false;
					Pant.pantCuffs          = olddata[x].pantCuffs;
					Pant.PantsBracesProvisions	= olddata[x].PantsBracesProvisions;
					Pant.pbackpocketMethod  = olddata[x].pbackpocketMethod;
			//		Pant.PantsBackPocketButton = true;
					Pant.cuffsStyle         = olddata[x].cuffsStyle;
					Pant.pantFit            = olddata[x].pantFit;
					Pant.pantPleats         = olddata[x].pantPleats;
					Pant.bpantPockets       = olddata[x].bpantPockets;
					Pant.pantPockets       = olddata[x].pantPockets;
			        Pant.pantPockets1       = olddata[x].pantPockets1;
			        Pant.pantPockets2       = olddata[x].pantPockets2;
			        Pant.PantFit      		= olddata[x].PantFit;

					Pant.urgent = olddata[x].urgent;
	        	//	Pant.urgentDate = olddata[x].urgentDate;
					Pant.DOP_day = olddata[x].DOP_day;
					Pant.DOP_month = olddata[x].DOP_month;
					
					iPant.Pant.push(Pant);					
					
				}else{
					
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var Pant = {};
					Pant.variantId        = x;
					Pant.parentSuitTimestampID = '';
			
					Pant.PantFabric         = df;
					Pant.PantContrastFabric = df;
					Pant.PantsNotes	= '';
					Pant.PantFabricNotes		= '';
					Pant.PantStyleNotes			= '';
					Pant.PantPleatsNotes		= '';
					Pant.PantFrontPocketsNotes	= '';
					Pant.PantBackPocketsNotes	= '';
					Pant.PantBeltAndCuffsNotes	= '';
					
					Pant.pantsCustomPocketsNotes	= '';
					Pant.pantsCustomPocketsImage	= '';		
					Pant.beltLoopStyle      = dsRegistry.getDatasource('beltLoopStyleDS').getStore()[0];
			//		Pant.PantsAngledBeltLoop = false;
					Pant.pantCuffs          = false;
					Pant.PantsBracesProvisions         = false;
					Pant.pbackpocketMethod  = "Both";
			//		Pant.PantsBackPocketButton = true;
					Pant.cuffsStyle         = dsRegistry.getDatasource('cuffsStyleDS').getStore()[0];
					Pant.pantFit            = dsRegistry.getDatasource('pantFitDS').getStore()[0];
					Pant.pantPleats         = dsRegistry.getDatasource('pantPleatsDS').getStore()[0];
					Pant.bpantPockets      = dsRegistry.getDatasource('bpantPocketsDS').getStore()[0];
					Pant.pantPockets       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
			        Pant.pantPockets1       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
			        Pant.pantPockets2       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
			        Pant.PantFit      		= 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2]
						
					Pant.urgent = 0;
	        	//	Pant.urgentDate = '';
	        		Pant.DOP_day = '';
					Pant.DOP_month = '';	
			
					iPant.Pant.push(Pant);
				}	
			}
			
			this._super(name, iPant, dsRegistry);
		}	
	}
});


//END DEFINE CLOSURE
});
