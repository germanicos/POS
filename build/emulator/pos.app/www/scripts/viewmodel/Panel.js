define(['jquery', 'knockout', 'base'], function($, ko) {
Panel = SimpleControl.extend({
	init: function(DsRegistry, oI, tableindex){//, fromcached) {
		var self = this;
		this._super( DsRegistry );
		this.orderItemMirror = oI;
		
	//	if(fromcached == true){
			this.jacketAcrossBack     			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketAcrossBack);					//= ko.observable('');		this.jacketAcrossBack.subscribe(function() { self.flushModel(); });
			this.jacketAcrossShoulder     		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketAcrossShoulder);					//= ko.observable('');		this.jacketAcrossShoulder.subscribe(function() { self.flushModel(); });
			this.jacketBackPanelAtXBack     	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBackPanelAtXBack);						//= ko.observable('');		this.jacketBackPanelAtXBack.subscribe(function() { self.flushModel(); });
			this.jacketBackPanelBottomEdge		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBackPanelBottomEdge);						//= ko.observable('');		this.jacketBackPanelBottomEdge.subscribe(function() { self.flushModel(); });		
			this.jacketBackPanelChest			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBackPanelChest);					//= ko.observable('');		this.jacketBackPanelChest.subscribe(function() { self.flushModel(); });
			this.jacketBackPanelHip				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBackPanelHip);						//= ko.observable('');		this.jacketBackPanelHip.subscribe(function() { self.flushModel(); });		
			this.jacketBackPanelTrueWaist		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBackPanelTrueWaist);						//= ko.observable('');		this.jacketBackPanelTrueWaist.subscribe(function() { self.flushModel(); });
			this.jacketFitlineBicep				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBicep);					//= ko.observable('');		this.jacketFitlineBicep.subscribe(function() { self.flushModel(); });
			this.jacketCenterBackLength			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketCenterBackLength);				//= ko.observable('');		this.jacketCenterBackLength.subscribe(function() { self.flushModel(); });
			this.jacketFitlineComments			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketComments);			//= ko.observable('');		this.jacketFitlineComments.subscribe(function() { self.flushModel(); });
			this.jacketElbow					= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketElbow);				//= ko.observable('');		this.jacketElbow.subscribe(function() { self.flushModel(); });
			this.jacketFrontLength				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontLength);				//= ko.observable('');		this.jacketFrontLength.subscribe(function() { self.flushModel(); });
			this.jacketFrontPanelChest			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontPanelChest);					//= ko.observable('');		this.jacketFrontPanelChest.subscribe(function() { self.flushModel(); });
			this.jacketFrontPanelHip			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontPanelHip);					//= ko.observable('');		this.jacketFrontPanelHip.subscribe(function() { self.flushModel(); });
			this.jacketFrontPanelTrueWaist		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontPanelTrueWaist);					//= ko.observable('');		this.jacketFrontPanelTrueWaist.subscribe(function() { self.flushModel(); });
			this.jacketFrontSideSeamLength		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontSideSeamLength);					//= ko.observable('');		this.jacketFrontSideSeamLength.subscribe(function() { self.flushModel(); });
			this.jacketLapelWidthAtChest		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketLapelWidthAtChest);					//= ko.observable('');		this.jacketLapelWidthAtChest.subscribe(function() { self.flushModel(); });
			this.jacketShoulderSeam				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketShoulderSeam);					//= ko.observable('');		this.jacketShoulderSeam.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelBottomEdge		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSidePanelBottomEdge);					//= ko.observable('');		this.jacketSidePanelBottomEdge.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelChest			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSidePanelChest);				//= ko.observable('');		this.jacketSidePanelChest.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelHip				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSidePanelHip);					//= ko.observable('');		this.jacketSidePanelHip.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelTrueWaist		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSidePanelTrueWaist);					//= ko.observable('');		this.jacketSidePanelTrueWaist.subscribe(function() { self.flushModel(); });		
			this.jacketSleeveCuffButtoned		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSleeveCuffButtoned);					//= ko.observable('');		this.jacketSleeveCuffButtoned.subscribe(function() { self.flushModel(); });
			this.jacketSleeveCurve				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSleeveCurve);					//= ko.observable('');		this.jacketSleeveCurve.subscribe(function() { self.flushModel(); });
			this.jacketSleeveCurveFront			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSleeveCurveFront);		//= ko.observable('');		this.jacketSleeveCurveFront.subscribe(function() { self.flushModel(); });
			this.jacketSleeveCurveSide        	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSleeveCurveSide);		    //= ko.observable('');		this.jacketSleeveCurveSide.subscribe(function() { self.flushModel(); });
			this.jacketSleeveLengthRight		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSleeveLengthRight);				//= ko.observable('');		this.jacketSleeveLengthRight.subscribe(function() { self.flushModel(); });
			this.jacketName						= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketName);				//= ko.observable('');		this.jacketName.subscribe(function() { self.flushModel(); });
			this.jacketSleeveLengthLeft			= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSleeveLengthLeft);				//= ko.observable('');		this.jacketSleeveLengthLeft.subscribe(function() { self.flushModel(); });
			this.jacketId          				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketId);			//= ko.observable('');		this.jacketId.subscribe(function() { self.flushModel(); });
			this.id          					= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].id);			//= ko.observable('');		this.jacketId.subscribe(function() { self.flushModel(); });
			
			this.jacketChestRule				= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketChestRule);
			this.jacketStomachRule          	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketStomachRule);	
			this.jacketJacketLengthRule         = ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketJacketLengthRule);	
			this.jacketFullShoulderRule         = ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFullShoulderRule);	
			this.jacketHipsRule          		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketHipsRule);	
			this.jacketFrontRule          		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontRule);	
			this.jacketBackRule          		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBackRule);	
			this.jacketNeckRule          		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketNeckRule);	
			this.jacketBicepRule          		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketBicepRule);	
			this.jacketLeftSleeveRule          	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketLeftSleeveRule);	
			this.jacketRightSleeveRule          = ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketRightSleeveRule);		
			this.jacketWristRule          		= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketWristRule);		
			this.jacketLapelLengthRule          = ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketLapelLengthRule);		
			this.jacketForearmRule          	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketForearmRule);
			this.jacketZeroPointRule          	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketZeroPointRule);	
			this.jacketFrontSZeroRule          	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketFrontSZeroRule);	
			this.jacketSBackSZeroRule          	= ko.observable(dsRegistry.getDatasource('panelDS').getStore()[tableindex].jacketSBackSZeroRule);	
			
	
		this.selectedGarment            = ko.observable({name: "none"});
	},
	
	test: function() {
		console.log(this.orderItemMirror);
	},

	enableVisibility: function(){
    var viewModel = {
        shouldShowMessage: ko.observable(false) // Message initially visible
    };
	},
/*
	garmentsList: function() { return ko.computed(function() {
		var gL = this.orderItemMirror.garmentsList();
		result =  [
			{
			  "name":"jacket",
			  "img":"gfx/measurements/jacket_m.png",
			  "count": gL[0].count()
			},
			{
			  "name":"pants",
			  "img":"gfx/measurements/pants_m.png",
			  "count": gL[1].count()
			},
			{
			  "name":"suit",
			  "img":"gfx/measurements/suit_m.png",
			  "count": gL[2].count()
			},
			{
			  "name":"vest",
			  "img":"gfx/measurements/vest_m.png",
			  "count": gL[3].count()
			},
			{
			  "name":"shirt",
			  "img":"gfx/measurements/shirt_m.png",
			  "count": gL[4].count()
			}
		];
		return result;
		}, this);
	},
	*/
	flushModel: function() {
		var tModel = {}; 
		tModel.jacketAcrossBack				= this.jacketAcrossBack();
		tModel.jacketAcrossShoulder 		= this.jacketAcrossShoulder();
		tModel.jacketBackPanelHip			= this.jacketBackPanelHip();
		tModel.jacketBackPanelAtXBack	    = this.jacketBackPanelAtXBack();
		tModel.jacketBackPanelChest			= this.jacketBackPanelChest();
		tModel.jacketBackPanelBottomEdge	= this.jacketBackPanelBottomEdge();		
		tModel.jacketBackPanelTrueWaist		= this.jacketBackPanelTrueWaist();
				
		tModel.jacketFitlineBicep			= this.jacketFitlineBicep();
		tModel.jacketCenterBackLength	    = this.jacketCenterBackLength();
		tModel.jacketFrontSideSeamLength    = this.jacketFrontSideSeamLength();
		tModel.jacketFitlineComments		= this.jacketFitlineComments();
		tModel.jacketFrontPanelHip		    = this.jacketFrontPanelHip();
		tModel.jacketFrontPanelTrueWaist    = this.jacketFrontPanelTrueWaist();
		tModel.jacketFrontPanelChest	    = this.jacketFrontPanelChest();
		tModel.jacketLapelWidthAtChest	    = this.jacketLapelWidthAtChest();
		tModel.jacketShoulderSeam		    = this.jacketShoulderSeam();
		tModel.jacketElbow				    = this.jacketElbow();
		tModel.jacketFrontLength		    = this.jacketFrontLength();
		tModel.jacketSidePanelBottomEdge    = this.jacketSidePanelBottomEdge();
		tModel.jacketSidePanelChest			= this.jacketSidePanelChest();
		
		tModel.jacketSidePanelTrueWaist		= this.jacketSidePanelTrueWaist();
		tModel.jacketSidePanelHip		    = this.jacketSidePanelHip();
		tModel.jacketSleeveCurveSide		= this.jacketSleeveCurveSide();
		tModel.jacketSleeveCurveFront		= this.jacketSleeveCurveFront();
		tModel.jacketSleeveCuffButtoned		= this.jacketSleeveCuffButtoned();
		tModel.jacketSleeveCurve			= this.jacketSleeveCurve();
		
		tModel.jacketSleeveLengthLeft		= this.jacketSleeveLengthLeft();
		tModel.jacketSleeveLengthRight		= this.jacketSleeveLengthRight();
		tModel.jacketName					= this.jacketName();				
		tModel.jacketId						= this.jacketId();		
		tModel.id							= this.id();
		
		
		tModel.jacketChestRule 				= this.jacketChestRule();
		tModel.jacketStomachRule 			= this.jacketStomachRule();	
		tModel.jacketJacketLengthRule		= this.jacketJacketLengthRule();	
		tModel.jacketFullShoulderRule		= this.jacketFullShoulderRule();	
		tModel.jacketHipsRule				= this.jacketHipsRule();	
		tModel.jacketFrontRule				= this.jacketFrontRule();	
		tModel.jacketBackRule				= this.jacketBackRule();	
		tModel.jacketNeckRule				= this.jacketNeckRule();	
		tModel.jacketBicepRule				= this.jacketBicepRule();	
		tModel.jacketLeftSleeveRule			= this.jacketLeftSleeveRule();	
		tModel.jacketRightSleeveRule		= this.jacketRightSleeveRule();		
		tModel.jacketWristRule				= this.jacketWristRule();		
		tModel.jacketLapelLengthRule		= this.jacketLapelLengthRule();		
		tModel.jacketForearmRule			= this.jacketForearmRule();
		tModel.jacketZeroPointRule			= this.jacketZeroPointRule();	
		tModel.jacketFrontSZeroRule			= this.jacketFrontSZeroRule();	
		tModel.jacketSBackSZeroRule			= this.jacketSBackSZeroRule();
		
		
		return tModel; 
	},

	clear: function() {
		this.jacketAcrossBack('');
		this.jacketAcrossShoulder('');
		this.jacketBackPanelAtXBack('');
		this.jacketBackPanelBottomEdge('');		
		this.jacketBackPanelChest('');
		this.jacketBackPanelHip('');
		this.jacketBackPanelTrueWaist('');
		
		this.jacketFitlineBicep('');
		this.jacketCenterBackLength('');
		this.jacketFitlineComments('');
		this.jacketElbow('');
		this.jacketFrontLength('');
		this.jacketFrontPanelChest('');
		this.jacketFrontPanelHip('');
		this.jacketFrontPanelTrueWaist('');
		this.jacketFrontSideSeamLength('');
		this.jacketLapelWidthAtChest('');
		this.jacketShoulderSeam('');
		this.jacketSidePanelBottomEdge('');
		this.jacketSidePanelChest('');
		
		this.jacketSidePanelHip('');
		this.jacketSidePanelTrueWaist('');
		this.jacketSleeveCuffButtoned('');
		this.jacketSleeveCurve('');
		this.jacketSleeveCurveFront('');
		this.jacketSleeveCurveSide('');
		
		this.jacketSleeveLengthLeft('');
		this.jacketSleeveLengthRight('');
		this.jacketName('');
		this.jacketId('');	
		this.id(0);
		
		
		this.jacketChestRule('');
		this.jacketStomachRule('');	
		this.jacketJacketLengthRule('');	
		this.jacketFullShoulderRule('');	
		this.jacketHipsRule('');	
		this.jacketFrontRule('');	
		this.jacketBackRule('');	
		this.jacketNeckRule('');	
		this.jacketBicepRule('');	
		this.jacketLeftSleeveRule('');	
		this.jacketRightSleeveRule('');		
		this.jacketWristRule('');		
		this.jacketLapelLengthRule('');		
		this.jacketForearmRule('');
		this.jacketZeroPointRule('');	
		this.jacketFrontSZeroRule('');	
		this.jacketSBackSZeroRule('');	
		
		
		this.selectedGarment({name: "none"});
	}
	
})
});