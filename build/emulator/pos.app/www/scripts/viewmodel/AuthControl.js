define(['jquery', 'knockout', 'base'], function($, ko) {
    AuthControl = function AuthController() {
        var self = this;
        self.clickcount = 0;
        self.userInfo    =  {};
        self.username    =  ko.observable("");
        self.password    =  ko.observable("");
        self.offlineMode =  ko.observable(false);
        self.device_id	 = 	ko.observable("");
        self.login       =  function() {
            if (!self.showSpinner()) {
                self.showSpinner(true);
                
                self.device_id( localStorage.getItem('device_id') );
 //customAlert("device_id: " + self.device_id());
 
                $.post(BUrl + 'client/ajax_login', {
                        username: self.username(), 
                        password: self.password(),
                        device_id: self.device_id() }, 
                        function(dataS) {
console.log("dataS:[" + dataS + "]");


                        if (dataS != '0'  && dataS != '' && dataS != null && dataS != '\n' && dataS.length > 5) {
                            self.userInfo = JSON.parse(dataS);
console.log("isactive: " + self.userInfo.is_active);
                            if(self.userInfo.is_active == '1'){
	                            setTimeout(function() {

										tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');
										fitlinesJacketDS = new SyncableDatasource('fitlinesJacketDS', 'fitlinesJacketDS', dsRegistry, 'jacket_fitlines_endpoint');
										fitlinesPantsDS = new SyncableDatasource('fitlinesPantsDS', 'fitlinesPantsDS', dsRegistry, 'pants_fitlines_endpoint');

									    customersVM = new CustomerCntClass(dsRegistry);
	                            	     
	                            	    custData = new SyncableDatasource('customersDS_' + authCtrl.userInfo.user_id, 'customersDS_' + authCtrl.userInfo.user_id, dsRegistry, 'customer_endpoint');
	                           	        customersVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
	                           	         //customerOrdersVM = new OrdersClass(dsRegistry);
	                           	        orderItem  = new OrderItem(dsRegistry);
	                           	         /*
	                           	         if(localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id) == 'undefined'){
	                           	         	this.lastUpdate = '0';
	                           	         }else{                           	         	
	                           	         	this.lastUpdate = localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id);
	                           	         }
	                           	         */
                           	         	currenciesDS = new SyncableDatasource('currenciesDS', 'currenciesDS', dsRegistry, 'currencies_endpoint');
										pricerangeDS = new SyncableDatasource('pricerangeDS', 'pricerangeDS', dsRegistry, 'price_range_endpoint');
										errorReportListVM = new ErrorReportListVM(dsRegistry);
										errorReportVM = new ErrorReportVM(dsRegistry, orderItem.custSelectVM);
	                                    $.mobile.changePage('#main');
	                            },500);
	                        }else{
	                        	//$.jGrowl("User is not active");
	                        	customAlert("User is not active");
	                        	localStorage.clear();
	                        }   
	                            
                        } else {
                        	  customAlert("Login failed");
                              //  $.jGrowl("Login failed");
                             // $.mobile.changePage('#index');
                              $.mobile.changePage('#loginPage');
                              
                        }
                        self.showSpinner(false);
                });
            }
        };
        self.resetApp = function() {
        	self.clickcount += 1;
console.log("self.clickcount " + self.clickcount);        	
        	if(self.clickcount == 5){
        		alert("App data has been reset.");
        		localStorage.clear();  
				window.location = "index.htm";
				self.clickcount = 0;
			}	
        };
        self.showSpinner =  ko.observable(false);
        self.showButton  =  ko.computed(function() {
                if (self.username() !== "" && self.password() !== "") return true;
                return false;
        }, this);
    };
});