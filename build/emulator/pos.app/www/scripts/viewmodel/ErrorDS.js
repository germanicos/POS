define(['jquery', 'knockout', 'base'], function($, ko) {
	error = SimpleControl.extend({
		
		init: function(DsRegistry) {
			this._super( DsRegistry );
			//where the data will be stored
			this.ErrorData = ko.observableArray([]);
			this.burl = "http://www.shirt-tailor.net/thepos/";
						
		},
		
		//the function load Errors will send the authentication data(user/password) to the server and then receive the errorsList back
		loadErrors: function() {
			var that = this;
			//collect the user data and prepare ir to send.
			data = {
				user_id: authCtrl.userInfo.user_id
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			
			//send the information for the endpoint and wait or an answer 
			$.ajax({
				type: 'POST',
				url: this.burl + 'testing_endpoint/testing_post',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					that.ErrorData = ko.observableArray(JSON.parse(dataS));
				},
				async:false
			});
			
			//alert(this.ErrorData.length);
		}
		
	});


});