define(['jquery', 'knockout', 'base'], function($, ko) {
	
AlterationsClass = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var self = this;
		var that = this;
		this.tailors = ko.observable(this.dsRegistry.getDatasource('tailorsDS').getStore());
		this.alterations = ko.observableArray();
		this.selectedAlteration  = ko.observable({});
		
		this.selectedAlteration.subscribe(function(data) {	
			if (self.parentViewModel !== null) {
				self.parentViewModel.setSelectedOrder(data);
			}else{
				console.log("self.parentViewModel == null");
			}
		});

		this.transmuteDate = function(date){  // input is yyyy-mm-dd hh:mm:ss
			if(date != undefined && date != null){
				if( date.indexOf(" ") >= 0 ){
					date = date.substring(0, date.indexOf(" "));
				}
				var day = date.substring(date.lastIndexOf("-") + 1);
				var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
				var year = date.substring(0, date.indexOf("-"));
				return day + "/" + month  + "/" + year;
			}else{
				return 0;
			}
		};

		this.timestampToDate = function(UNIX_timestamp){
  			var a = new Date(UNIX_timestamp*1000);
  			var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  			var year = a.getFullYear();
  			var month = months[a.getMonth()];
  			var date = a.getDate();
  			var time = date + '/' + (a.getMonth() + 1) + '/' + year;
  			return time;
		}


		this.modifiedListOptionsFunction = function(elemname) { // 0: element name
			var checkboxes = document.getElementsByName( elemname );
			var x = 0;						
			for (var a in this.alterations() ) { 
				for (var b in this.alterations()[a].products){
					if (this.alterations()[a].products[b].checked == true){
						checkboxes[x].classList.add('selecteds');
					}else{
						checkboxes[x].classList.remove('selecteds');
					}
					x++;
				}
			}
console.log(JSON.stringify(this.alterations()));			
		};
		
		this.modifiedListOptionsSingleOnlyFunction = function(elemname, parentindex, index, garment ) { // 0: element name
console.log("parentindex: " + parentindex);
console.log("index: " + index);			
console.log("garment: " + JSON.stringify(garment));
			var checkboxes = document.getElementsByName( elemname );
			var x = 0;		
										
			for (var a in this.alterations() ) { 
				for (var b in this.alterations()[a].products){
					if(a != parentindex || b != index ){
						this.alterations()[a].products[b].checked = false;	
					}
					
					if (this.alterations()[a].products[b].checked == true){
						checkboxes[x].classList.add('selecteds');
					}else{
						checkboxes[x].classList.remove('selecteds');
					}
					x++;
				}
			}
			orderItem.ShippingFaultyRemakeGarmentsVM.updateView(garment);
//console.log(JSON.stringify(this.alterations()));			
		};
		
		
		this.getTailorName = function(id){	
			var tailors = ko.observable(dsRegistry.getDatasource('tailorsDS').getStore());					
			for (i in tailors()){				
				if(tailors()[i].user_id == id){
					return tailors()[i].last_name;
				}
			}	
		}

		this.alterationReport = function(alterationOrderIndex,  alterationProductIndex){
			localStorage.removeItem("alterationOrderIndex");
			localStorage.setItem("alterationOrderIndex", alterationOrderIndex);
			localStorage.removeItem("alterationProductIndex");
			localStorage.setItem("alterationProductIndex", alterationProductIndex);
//console.log("loadSelectedGarmentsToAlterationsAndGoToAlterationsPage " + this.alterations().length);
			orderItem.alterationReportsVM = new AlterationReport(orderItem.dsRegistry);			
			orderItem.AlterationReportsDS = new defAlterationReport('AlterationReportsDS', orderItem.dsRegistry, this.alterations() );
			orderItem.alterationReportsVM.subscribeTo('AlterationReportsDS');
console.log("orderItem.AlterationReportsDS.getStore(): " + JSON.stringify( orderItem.AlterationReportsDS.getStore() ) );
//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
 			orderItem.alterationReportsVM.addVariantsAlterationReports();

			if( orderItem.AlterationReportsDS.getStore().AlterationReport.length == 0 ){
				customAlert("Select at least one garment for the alteration report");
			}else{
				$.mobile.changePage('#alterationReport'); 
			}
		};
		
		this.hasFittingsOfStatus = function(status){
			var found = false;
			for(var i = 0; i < this.alterations().length; i++){
				for(var j = 0; j < this.alterations()[i].products.length; j++ ){
					if(this.alterations()[i].products[j].fittings.fitting.status == status){
						found = true;
						break;	
					}	
				}
			}
			return found;
		}
		
		this.saveStatus = function(index){
//	console.log("alterations: " + JSON.stringify(this.alterations()));
//	console.log("alterations length: " + this.alterations().length);
//	console.log("alterations()[0].products.length: " + this.alterations()[0].products.length);
//	console.log("this.alterations()[0].products[0].tailor: " + this.alterations()[0].products[0].tailor);
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var alteration_tailors = [];						
			for(var i = 0; i < this.alterations().length; i++){
				for(var j = 0; j < this.alterations()[i].products.length; j++ ){
					if(this.alterations()[i].products[j].fittings.fitting.status == 0){
						if(this.alterations()[i].products[j].tailor != undefined && this.alterations()[i].products[j].tailor != ""){
							var obj = new Object();
					   		obj.fitting_id = this.alterations()[i].products[j].fittings.fitting.fitting_id;
					   		obj.tailor_id  = this.alterations()[i].products[j].tailor.user_id;
							alteration_tailors.push(obj);
						}	
					}	
				}
			}
			var customerString = orderItem.custSelectVM.selectedCustomer();			
			if( alteration_tailors.length == 0){
				customAlert("Please select tailors for the alterations");
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					alteration_tailors: alteration_tailors
				};		
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				$.post(BUrl + 'client_alterations/assign_alteration_tailor_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	$.mobile.changePage('#main');
		            	$.mobile.changePage('#AlterationsList');
		            	customAlert("Tailor succesfully added");	
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        	
	        var myVar=setTimeout(function(){spinner.style.display = "none";},1500);	
			//spinner.style.display = "none";			

			return false;		
		};
		
		
		this.showContactInfo = function(index){
			
			var countries = ko.observable(dsRegistry.getDatasource('countriesDS').getStore());	
			var country = "";				
			for (i in countries()){
				if(countries()[i].locations_id == this.alterations()[index].customer.customer_country){
					country = countries()[i].location_name;
				}
			}
			var states = ko.observable(dsRegistry.getDatasource('statesDS').getStore());	
			var state = "";				
			for (i in states()){
				if(states()[i].locations_id == this.alterations()[index].customer.customer_state){
					state = states()[i].location_name;
				}
			}
			var cities = ko.observable(dsRegistry.getDatasource('citiesDS').getStore());	
			var city = "";				
			for (i in cities()){
				if(cities()[i].locations_id == this.alterations()[index].customer.customer_city){
					city = cities()[i].location_name;
				}
			}
			
			document.getElementById("customer_first_name").innerHTML = this.alterations()[index].customer.customer_first_name;
			document.getElementById("customer_last_name").innerHTML = this.alterations()[index].customer.customer_last_name;
			document.getElementById("customer_address1").innerHTML = this.alterations()[index].customer.customer_address1;
			document.getElementById("customer_address2").innerHTML = this.alterations()[index].customer.customer_address2;
			document.getElementById("customer_postal_code").innerHTML = this.alterations()[index].customer.customer_postal_code;
			document.getElementById("customer_mobile_phone").innerHTML = this.alterations()[index].customer.customer_mobile_phone;
			document.getElementById("customer_landline_phone").innerHTML = this.alterations()[index].customer.customer_landline_phone;
			document.getElementById("customer_email").innerHTML = this.alterations()[index].customer.customer_email;
			document.getElementById("customer_country").innerHTML = country;
			document.getElementById("customer_state").innerHTML = state;
			document.getElementById("customer_city").innerHTML = city;
			
			if ("createEvent" in document) {
		    	var evt = document.createEvent("HTMLEvents");
		    	evt.initEvent("change", false, true);
		    	document.getElementById("customer_first_name").dispatchEvent(evt);
				document.getElementById("customer_last_name").dispatchEvent(evt);
				document.getElementById("customer_address1").dispatchEvent(evt);
				document.getElementById("customer_address2").dispatchEvent(evt);
				document.getElementById("customer_postal_code").dispatchEvent(evt);
				document.getElementById("customer_mobile_phone").dispatchEvent(evt);
				document.getElementById("customer_landline_phone").dispatchEvent(evt);
				document.getElementById("customer_email").dispatchEvent(evt);
				document.getElementById("customer_country").dispatchEvent(evt);
				document.getElementById("customer_state").dispatchEvent(evt);
				document.getElementById("customer_city").dispatchEvent(evt);
			}else{
		    	document.getElementById("customer_first_name").fireEvent("onchange");
				document.getElementById("customer_last_name").fireEvent("onchange");
				document.getElementById("customer_address1").fireEvent("onchange");
				document.getElementById("customer_address2").fireEvent("onchange");
				document.getElementById("customer_postal_code").fireEvent("onchange");
				document.getElementById("customer_mobile_phone").fireEvent("onchange");
				document.getElementById("customer_landline_phone").fireEvent("onchange");
				document.getElementById("customer_email").fireEvent("onchange");
				document.getElementById("customer_country").fireEvent("onchange");
				document.getElementById("customer_state").fireEvent("onchange");
				document.getElementById("customer_city").fireEvent("onchange");
			}
			document.getElementById("ContactInfoPopup").style.display = "block";
		};
		
		this.exportFittingPopup = function(alterationOrderIndex, alterationProductIndex){
			localStorage.removeItem("alterationOrderIndex");
			localStorage.setItem("alterationOrderIndex", alterationOrderIndex);
			localStorage.removeItem("alterationProductIndex");
			localStorage.setItem("alterationProductIndex", alterationProductIndex);

			document.getElementById('exportFittingPopup').style.display = 'block';
			document.getElementById("exportFittingEmail").value = "";
		};
		
		this.exportFitting = function(){
			var alterationOrderIndex = localStorage.getItem("alterationOrderIndex");	
			var alterationProductIndex = localStorage.getItem("alterationProductIndex");
			var fitting_id = this.alterations()[alterationOrderIndex].products[alterationProductIndex].fittings.fitting.fitting_id;
//console.log("alterationOrderIndex: " + alterationOrderIndex);
//console.log("alterationProductIndex: " + alterationProductIndex);
//console.log("fitting id: " + fitting_id);
			var email = document.getElementById("exportFittingEmail").value;
//console.log("email: " + email);			
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		var result =  re.test(email);
//console.log("email validation: " + result);    		
    		if(result == true){
				var spinner = document.getElementById('loading_jp');
		   		spinner.style.display = "block";
				data = { 
					user_id: authCtrl.userInfo.user_id,
					fitting_id: fitting_id,
					tailor_email: email
				};		
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				$.post(BUrl + 'client_alterations/export_fitting_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	document.getElementById("exportFittingPopup").style.display = "none";
		            	customAlert("Fitting successfully exported");	
					}else{
						customAlert("An error occurred, please try again<br/>Error: " + dataS.msg);
		                $.jGrowl(dataS.msg);
					}
				});
    			spinner.style.display = "none";
    		}else{
    			customAlert("Please enter a valid email address");		
			}
		}
		
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
		this.shipping_alterations_sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var customerString = orderItem.custSelectVM.selectedCustomer();
	
			var photosarray = [];
			var images = localStorage.getItem('ShippingFaultyRemakeGarmentsPhotos');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('ShippingFaultyRemakeGarmentsPhotos') != null){
				if(localStorage.getItem('ShippingFaultyRemakeGarmentsPhotos').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
	
			var products_array = [];
			for (var a in this.alterations() ) { 
				for (var b in this.alterations()[a].products){
					if (this.alterations()[a].products[b].checked == true){
						var obj = new Object();
		   				obj.garment = this.alterations()[a].products[b].garment;
		   				obj.orders_products_id  = this.alterations()[a].products[b].orders_products_id;
						products_array.push(obj);
					}
				}
			}
	
			if( products_array.length == 0){
				customAlert("Select at least one garment");
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					customer: customerString,
					products: products_array
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);

				$.post(BUrl + "shipping_alterations_endpoint", {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Succesfully added");
		                $.mobile.changePage('#shipping_main');
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
	},
	
	digestData: function(data) {
		var newObserve = [];				
		//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
		for (var ind in data) {
			if(data[ind] != null){
				if (!data[ind].isDead){
				//	if(ind < 10){ 
						newObserve.push(data[ind]);
						for (var x in newObserve[ind].products){
							newObserve[ind].products[x].checked = false;
							newObserve[ind].products[x].tailor = "";
						}
						//newObserve[ind].checked = false;
				//	}	
				}	
			}	
		}
		this.alterations(newObserve);
console.log("alterations for alteration: " + JSON.stringify(this.alterations()));		
	},
});  






//END DEFINE CLOSURE
});