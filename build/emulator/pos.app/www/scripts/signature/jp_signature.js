
		var clickX = new Array();
		var clickY = new Array();
		var clickDrag = new Array();
		var paint;
		var canvas;
		var context;
		var drawn = false;

		function _mousedown(e){
		  var mouseX = e.pageX - this.offsetLeft;
		  var mouseY = e.pageY - this.offsetTop;
				
		  paint = true;
		  addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
		  redraw();
		}
		
		function _mousemove(e){
		  if(paint){
			addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
			redraw();
			e.preventDefault();
		  }
		}
		
		function _mouseup(e){
		  paint = false;
		}
		
		function _mouseleave(e){
		  paint = false;
		}
	

		function _touchstart(e){
          var touch = e.touches[0]; // Get the information for finger #1			
		  var mouseX = touch.pageX - touch.target.offsetLeft;
		  var mouseY = touch.pageY - touch.target.offsetTop;
				
		  paint = true;
		  addClick(touch.pageX - touch.target.offsetLeft, touch.pageY - touch.target.offsetTop);
		  redraw();
		  e.preventDefault();
		}

		function _touchmove(e){
		  if(paint){
		  	var touch = e.touches[0]; // Get the information for finger #1
			addClick(touch.pageX - touch.target.offsetLeft, touch.pageY - touch.target.offsetTop, true);
			redraw();
			e.preventDefault();
		  }
		}	

	
		
		function addClick(x, y, dragging){
			drawn = true;	
			clickX.push(x);
			clickY.push(y);
			clickDrag.push(dragging);
		}
		
		function redraw(){
		  context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
		  
		  context.strokeStyle = "#000000";
		  context.lineJoin = "round";
		  context.lineWidth = 5;
					
		  for(var i=0; i < clickX.length; i++) {		
			context.beginPath();
			if(clickDrag[i] && i){
			  context.moveTo(clickX[i-1], clickY[i-1]);
			 }else{
			   context.moveTo(clickX[i]-1, clickY[i]);
			 }
			 context.lineTo(clickX[i], clickY[i]);
			 context.closePath();
			 context.stroke();
		  }
		}




	function prepareCanvas(){
		var canvasDiv = document.getElementById('canvasDiv');
		canvas = document.createElement('canvas');
		canvas.setAttribute('width', 500);
		canvas.setAttribute('height', 350);
		canvas.setAttribute('id', 'canvas');
		canvasDiv.appendChild(canvas);
		if(typeof G_vmlCanvasManager != 'undefined') {
			canvas = G_vmlCanvasManager.initElement(canvas);
		}
		context = canvas.getContext("2d");
	
	    canvas.addEventListener('mousedown', _mousedown, false);
        canvas.addEventListener('mousemove', _mousemove, false);
        window.addEventListener('mouseup', _mouseup, false);
	
		canvas.addEventListener('touchstart', _touchstart, false);
        canvas.addEventListener('touchmove', _touchmove, false);
	
	}
	
	function emptycanvas(){
		clickX = new Array();
		clickY = new Array();
		clickDrag = new Array();
		paint= false;
		drawn = false;
		context.clearRect(0, 0, canvas.width, canvas.height); // Clears the canvas
	}	
	
	function savecanvas(){
	
		if( drawn == false ){
			customAlert("Signature is empty");	
		}else{
		
			var spinner = document.getElementById('loading_jp');
			spinner.style.display = "block";	
				
			dataURL = canvas.toDataURL("image/png", 0.6);       
			var endpoint = 'upload_signature_image';
			var ServerEndPoint = BUrl + 'client_image_upload/' + endpoint;
			var salesmanString = "{username:'" + authCtrl.username() + "',password:'" + authCtrl.password() + "', id:'" + authCtrl.userInfo.user_id + "'}";
			var basedata = dataURL.replace("data:image/png;base64,", "");
			try{
				var customerString = orderItem.custSelectVM.selectedCustomer().server_id;
			}catch(e){
				var customerString = "0";
			}
			console.log(salesmanString);
			console.log(customerString);
	
			request = $.ajax({
				type: 'POST',
				url: ServerEndPoint,
				dataType: 'json',
				async: true,
				data:{
					salesman: JSON.stringify(eval("(" + salesmanString + ")")),
					customer_id: customerString,
					data: basedata
					},
				success: function(dataS) {
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "none";								
					var data = dataS;
					var image_id = data['id'];
					var image = data['image'];
					localStorage.removeItem('signature');
					tmp = localStorage.getItem('signature');
					tmp = (tmp === null) ? [] : JSON.parse(tmp);
					var customer_id = orderItem.custSelectVM.selectedCustomer().server_id;
					tmp.push({customer_id:customer_id,image_id:image_id,picture:image});
					localStorage.setItem('signature', JSON.stringify(tmp));
					customAlert("Signature saved successfully");
				}
			});	
			
		}
	}