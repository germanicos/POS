// Variables for referencing the canvas and 2dcanvas context
    var canvas,context;
    // Variables to keep track of the mouse position and left-button status 
    var mouseX,mouseY;

	var clickX = new Array();
	var clickY = new Array();
	var clickDrag = new Array();
	var paint = false;
	var drawn = false;

    // Keep track of the mouse button being pressed and draw a dot at current location
    function _mousedown(e) {
		mouseX = e.pageX - this.offsetLeft;
	  	mouseY = e.pageY - this.offsetTop;
        paint=true;
        addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
		redraw();
    }

    // Kepp track of the mouse position and draw a dot if mouse button is currently pressed
    function _mousemove(e) { 
        // Update the mouse co-ordinates when moved
		mouseX = e.pageX - this.offsetLeft;
		mouseY = e.pageY - this.offsetTop;

        // Draw a dot if the mouse button is currently being pressed
        if (paint==true) {
            addClick(mouseX,mouseY,true);
			redraw();
        }

    }

    // Keep track of the mouse button being released
    function _mouseup(e) {
        paint=false;
    }
	
	
    // Draw something when a touch start is detected
    function _touchstart(e) {
        // Update the touch co-ordinates
		if (!e)
            var e = event;
		if(e.touches) {
            if (e.touches.length == 1) { // Only deal with one finger
                var touch = e.touches[0]; // Get the information for finger #1
                mouseX=touch.pageX-touch.target.offsetLeft;
                mouseY=touch.pageY-touch.target.offsetTop;
            }
        }

        addClick(mouseX,mouseY);
		redraw();
        // Prevents an additional mousedown event being triggered
        event.preventDefault();
    }

    // Draw something and prevent the default scrolling when touch movement is detected
    function _touchmove(e) { 
        // Update the touch co-ordinates
		if (!e)
            var e = event;
		if(e.touches) {
            if (e.touches.length == 1) { // Only deal with one finger
                var touch = e.touches[0]; // Get the information for finger #1
                mouseX=touch.pageX-touch.target.offsetLeft;
                mouseY=touch.pageY-touch.target.offsetTop;
            }
        }

        // During a touchmove event, unlike a mousemove event, we don't need to check if the touch is engaged, since there will always be contact with the screen by definition.
        addClick(mouseX,mouseY,true); 
		redraw();
        // Prevent a scrolling action as a result of this touchmove triggering.
        event.preventDefault();
    }
	
/*
    // Draws a dot at a specific position on the supplied canvas name
    // Parameters are: A canvas context, the x position, the y position, the size of the dot
    function addClick(x,y,size) {
        // Let's use black by setting RGB values to 0, and 255 alpha (completely opaque)
        r=0; g=0; b=0; a=255;

        // Select a fill style
        context.fillStyle = "rgba("+r+","+g+","+b+","+(a/255)+")";

        // Draw a filled circle
        context.beginPath();
        context.arc(x, y, size, 0, Math.PI*2, true); 
        context.closePath();
        context.fill();
        drawn = true;
    } 
*/
	function addClick(x, y, dragging){
		drawn = true;	
		clickX.push(x);
		clickY.push(y);
		clickDrag.push(dragging);
	}

	function redraw(){
		context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
		  
		context.strokeStyle = "#000000";
		context.lineJoin = "round";
		context.lineWidth = 5;
					
		for(var i=0; i < clickX.length; i++) {		
			context.beginPath();
			if(clickDrag[i] && i){
				context.moveTo(clickX[i-1], clickY[i-1]);
			}else{
				context.moveTo(clickX[i]-1, clickY[i]);
			}
			context.lineTo(clickX[i], clickY[i]);
			context.closePath();
			context.stroke();
		}
	}

    // Set-up the canvas and add our event handlers after the page has loaded
    function prepareCanvas() {
        // Get the specific canvas element from the HTML document
        var canvasDiv = document.getElementById('canvasDiv');
		canvas = document.createElement('canvas');
		canvas.setAttribute('width', 700);
		canvas.setAttribute('height', 350);
		canvas.setAttribute('id', 'canvas');
		canvasDiv.appendChild(canvas);
		if(typeof G_vmlCanvasManager != 'undefined') {
			canvas = G_vmlCanvasManager.initElement(canvas);
		}
		context = canvas.getContext("2d");
        
        //canvas = document.getElementById('sketchpad');
        // If the browser supports the canvas tag, get the 2d drawing context for this canvas
        //if (canvas.getContext)
        //    context = canvas.getContext('2d');

        // Check that we have a valid context to draw on/with before adding event handlers
        if (context) {
            // React to mouse events on the canvas, and mouseup on the entire document
            canvas.addEventListener('mousedown', _mousedown, false);
            canvas.addEventListener('mousemove', _mousemove, false);
            window.addEventListener('mouseup', _mouseup, false);

            // React to touch events on the canvas
            canvas.addEventListener('touchstart', _touchstart, false);
            canvas.addEventListener('touchmove', _touchmove, false);
        }
    }
    
    
	function emptycanvas(){
		clickX = new Array();
		clickY = new Array();
		clickDrag = new Array();
		paint= false;
		drawn = false;
		context.clearRect(0, 0, canvas.width, canvas.height); // Clears the canvas
        drawn = false;
	}	
	
	
	function savecanvas(){
	
		if( drawn == false ){
			customAlert("Signature is empty");	
		}else{
		
			var spinner = document.getElementById('loading_jp');
			spinner.style.display = "block";	
				
			dataURL = canvas.toDataURL("image/png", 0.6);       
			var endpoint = 'upload_signature_image';
			var ServerEndPoint = BUrl + 'client_image_upload/' + endpoint;
			var salesmanString = "{username:'" + authCtrl.username() + "',password:'" + authCtrl.password() + "', id:'" + authCtrl.userInfo.user_id + "'}";
			var basedata = dataURL.replace("data:image/png;base64,", "");
			try{
				var customerString = orderItem.custSelectVM.selectedCustomer().server_id;
			}catch(e){
				var customerString = "0";
			}
			console.log(salesmanString);
			console.log(customerString);
	
			request = $.ajax({
				type: 'POST',
				url: ServerEndPoint,
				dataType: 'json',
				async: true,
				data:{
					salesman: JSON.stringify(eval("(" + salesmanString + ")")),
					customer_id: customerString,
					data: basedata
					},
				success: function(dataS) {
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "none";								
					var data = dataS;
					var image_id = data['id'];
					var image = data['image'];
					localStorage.removeItem('signature');
					tmp = localStorage.getItem('signature');
					tmp = (tmp === null) ? [] : JSON.parse(tmp);
					var customer_id = orderItem.custSelectVM.selectedCustomer().server_id;
					tmp.push({customer_id:customer_id,image_id:image_id,picture:image});
					localStorage.setItem('signature', JSON.stringify(tmp));
					customAlert("Signature saved successfully");
				}
			});	
			
		}
	}