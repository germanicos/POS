
define([], function () {

    // mock for logo positions
    const logo_positions_old_3d = {
        "1": [
            {
                "id": "30",
                "name": "1",
                "garment_id": "1",
                "view": "front",
                "pos_x": "36",
                "pos_y": "7",
                "description": ""
            },
            {
                "id": "31",
                "name": "2",
                "garment_id": "1",
                "view": "front",
                "pos_x": "69",
                "pos_y": "7",
                "description": ""
            },
            {
                "id": "32",
                "name": "3",
                "garment_id": "1",
                "view": "front",
                "pos_x": "70",
                "pos_y": "17",
                "description": ""
            },
            {
                "id": "33",
                "name": "4",
                "garment_id": "1",
                "view": "front",
                "pos_x": "33",
                "pos_y": "18",
                "description": ""
            },
            {
                "id": "43",
                "name": "Custom",
                "garment_id": "1",
                "view": "front",
                "pos_x": "40",
                "pos_y": "107",
                "description": ""
            },
            {
                "id": "34",
                "name": "5",
                "garment_id": "1",
                "view": "back",
                "pos_x": "30",
                "pos_y": "12",
                "description": ""
            },
            {
                "id": "35",
                "name": "6",
                "garment_id": "1",
                "view": "back",
                "pos_x": "61",
                "pos_y": "13",
                "description": ""
            },

            {
                "id": "44",
                "name": "Custom",
                "garment_id": "1",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ],
        "2": [
            {
                "id": "19",
                "name": "1",
                "garment_id": "2",
                "view": "front",
                "pos_x": "45",
                "pos_y": "4",
                "description": ""
            },
            {
                "id": "20",
                "name": "2",
                "garment_id": "2",
                "view": "front",
                "pos_x": "34",
                "pos_y": "13",
                "description": ""
            },
            {
                "id": "21",
                "name": "3",
                "garment_id": "2",
                "view": "front",
                "pos_x": "52",
                "pos_y": "13",
                "description": ""
            },
            {
                "id": "22",
                "name": "4",
                "garment_id": "2",
                "view": "front",
                "pos_x": "29",
                "pos_y": "41",
                "description": ""
            },
            {
                "id": "23",
                "name": "5",
                "garment_id": "2",
                "view": "front",
                "pos_x": "55",
                "pos_y": "41",
                "description": ""
            },
            {
                "id": "24",
                "name": "6",
                "garment_id": "2",
                "view": "front",
                "pos_x": "7",
                "pos_y": "69",
                "description": ""
            },
            {
                "id": "25",
                "name": "7",
                "garment_id": "2",
                "view": "front",
                "pos_x": "57",
                "pos_y": "64",
                "description": ""
            },
            {
                "id": "26",
                "name": "8",
                "garment_id": "2",
                "view": "front",
                "pos_x": "31",
                "pos_y": "78",
                "description": ""
            },
            {
                "id": "27",
                "name": "9",
                "garment_id": "2",
                "view": "front",
                "pos_x": "49",
                "pos_y": "82",
                "description": ""
            },
            {
                "id": "45",
                "name": "Custom",
                "garment_id": "2",
                "view": "front",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            },
            {
                "id": "28",
                "name": "10",
                "garment_id": "2",
                "view": "back",
                "pos_x": "44",
                "pos_y": "6",
                "description": ""
            },
            {
                "id": "29",
                "name": "11",
                "garment_id": "2",
                "view": "back",
                "pos_x": "42",
                "pos_y": "17",
                "description": ""
            },

            {
                "id": "46",
                "name": "Custom",
                "garment_id": "2",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ],
        "3": [
            {
                "id": "36",
                "name": "1",
                "garment_id": "3",
                "view": "front",
                "pos_x": "41",
                "pos_y": "5",
                "description": ""
            },
            {
                "id": "37",
                "name": "2",
                "garment_id": "3",
                "view": "front",
                "pos_x": "22",
                "pos_y": "39",
                "description": ""
            },
            {
                "id": "38",
                "name": "3",
                "garment_id": "3",
                "view": "front",
                "pos_x": "60",
                "pos_y": "40",
                "description": ""
            },
            {
                "id": "39",
                "name": "4",
                "garment_id": "3",
                "view": "front",
                "pos_x": "25",
                "pos_y": "70",
                "description": ""
            },
            {
                "id": "40",
                "name": "5",
                "garment_id": "3",
                "view": "front",
                "pos_x": "56",
                "pos_y": "71",
                "description": ""
            },
            {
                "id": "41",
                "name": "6",
                "garment_id": "3",
                "view": "front",
                "pos_x": "47",
                "pos_y": "88",
                "description": ""
            },
            {
                "id": "47",
                "name": "Custom",
                "garment_id": "3",
                "view": "front",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            },
            {
                "id": "42",
                "name": "7",
                "garment_id": "3",
                "view": "back",
                "pos_x": "45",
                "pos_y": "8",
                "description": ""
            },

            {
                "id": "48",
                "name": "Custom",
                "garment_id": "3",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ],
        "4": [
            {
                "id": "1",
                "name": "1",
                "garment_id": "4",
                "view": "front",
                "pos_x": "49",
                "pos_y": "6",
                "description": ""
            },
            {
                "id": "2",
                "name": "2",
                "garment_id": "4",
                "view": "front",
                "pos_x": "29",
                "pos_y": "17",
                "description": ""
            },
            {
                "id": "3",
                "name": "3",
                "garment_id": "4",
                "view": "front",
                "pos_x": "61",
                "pos_y": "18",
                "description": ""
            },
            {
                "id": "4",
                "name": "4",
                "garment_id": "4",
                "view": "front",
                "pos_x": "22",
                "pos_y": "32",
                "description": ""
            },
            {
                "id": "5",
                "name": "5",
                "garment_id": "4",
                "view": "front",
                "pos_x": "66",
                "pos_y": "25",
                "description": ""
            },
            {
                "id": "6",
                "name": "6",
                "garment_id": "4",
                "view": "front",
                "pos_x": "62",
                "pos_y": "35",
                "description": ""
            },
            {
                "id": "7",
                "name": "7",
                "garment_id": "4",
                "view": "front",
                "pos_x": "13",
                "pos_y": "53",
                "description": ""
            },
            {
                "id": "8",
                "name": "8",
                "garment_id": "4",
                "view": "front",
                "pos_x": "26",
                "pos_y": "66",
                "description": ""
            },
            {
                "id": "9",
                "name": "9",
                "garment_id": "4",
                "view": "front",
                "pos_x": "67",
                "pos_y": "66",
                "description": ""
            },
            {
                "id": "10",
                "name": "10",
                "garment_id": "4",
                "view": "front",
                "pos_x": "7",
                "pos_y": "80",
                "description": ""
            },
            {
                "id": "11",
                "name": "11",
                "garment_id": "4",
                "view": "front",
                "pos_x": "85",
                "pos_y": "84",
                "description": ""
            },
            {
                "id": "12",
                "name": "12",
                "garment_id": "4",
                "view": "front",
                "pos_x": "25",
                "pos_y": "87",
                "description": ""
            },
            {
                "id": "13",
                "name": "13",
                "garment_id": "4",
                "view": "front",
                "pos_x": "59",
                "pos_y": "88",
                "description": ""
            },
            {
                "id": "49",
                "name": "Custom",
                "garment_id": "4",
                "view": "front",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            },
            {
                "id": "14",
                "name": "14",
                "garment_id": "4",
                "view": "back",
                "pos_x": "44",
                "pos_y": "9",
                "description": ""
            },
            {
                "id": "15",
                "name": "15",
                "garment_id": "4",
                "view": "back",
                "pos_x": "12",
                "pos_y": "53",
                "description": ""
            },
            {
                "id": "16",
                "name": "16",
                "garment_id": "4",
                "view": "back",
                "pos_x": "84",
                "pos_y": "56",
                "description": ""
            },
            {
                "id": "17",
                "name": "17",
                "garment_id": "4",
                "view": "back",
                "pos_x": "14",
                "pos_y": "67",
                "description": ""
            },
            {
                "id": "18",
                "name": "18",
                "garment_id": "4",
                "view": "back",
                "pos_x": "80",
                "pos_y": "67",
                "description": ""
            },

            {
                "id": "50",
                "name": "Custom",
                "garment_id": "4",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ]
    }

    const logo_positions_new_3d = {
        "1": [
            {
                "id": "30",
                "name": "1",
                "garment_id": "1",
                "view": "front",
                "pos_x": "37",
                "pos_y": "13",
                "description": ""
            },
            {
                "id": "31",
                "name": "2",
                "garment_id": "1",
                "view": "front",
                "pos_x": "64",
                "pos_y": "13",
                "description": ""
            },
            {
                "id": "32",
                "name": "3",
                "garment_id": "1",
                "view": "front",
                "pos_x": "68",
                "pos_y": "18",
                "description": ""
            },
            {
                "id": "33",
                "name": "4",
                "garment_id": "1",
                "view": "front",
                "pos_x": "33",
                "pos_y": "20",
                "description": ""
            },
            {
                "id": "43",
                "name": "Custom",
                "garment_id": "1",
                "view": "front",
                "pos_x": "40",
                "pos_y": "107",
                "description": ""
            },
            {
                "id": "34",
                "name": "5",
                "garment_id": "1",
                "view": "back",
                "pos_x": "36",
                "pos_y": "12",
                "description": ""
            },
            {
                "id": "35",
                "name": "6",
                "garment_id": "1",
                "view": "back",
                "pos_x": "63",
                "pos_y": "13",
                "description": ""
            },

            {
                "id": "44",
                "name": "Custom",
                "garment_id": "1",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ],
        "2": [
            {
                "id": "19",
                "name": "1",
                "garment_id": "2",
                "view": "front",
                "pos_x": "45",
                "pos_y": "4",
                "description": ""
            },
            {
                "id": "20",
                "name": "2",
                "garment_id": "2",
                "view": "front",
                "pos_x": "34",
                "pos_y": "13",
                "description": ""
            },
            {
                "id": "21",
                "name": "3",
                "garment_id": "2",
                "view": "front",
                "pos_x": "52",
                "pos_y": "13",
                "description": ""
            },
            {
                "id": "22",
                "name": "4",
                "garment_id": "2",
                "view": "front",
                "pos_x": "29",
                "pos_y": "35",
                "description": ""
            },
            {
                "id": "23",
                "name": "5",
                "garment_id": "2",
                "view": "front",
                "pos_x": "55",
                "pos_y": "35",
                "description": ""
            },
            {
                "id": "24",
                "name": "6",
                "garment_id": "2",
                "view": "front",
                "pos_x": "7",
                "pos_y": "69",
                "description": ""
            },
            {
                "id": "25",
                "name": "7",
                "garment_id": "2",
                "view": "front",
                "pos_x": "57",
                "pos_y": "64",
                "description": ""
            },
            {
                "id": "26",
                "name": "8",
                "garment_id": "2",
                "view": "front",
                "pos_x": "31",
                "pos_y": "78",
                "description": ""
            },
            {
                "id": "27",
                "name": "9",
                "garment_id": "2",
                "view": "front",
                "pos_x": "49",
                "pos_y": "82",
                "description": ""
            },
            {
                "id": "45",
                "name": "Custom",
                "garment_id": "2",
                "view": "front",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            },
            {
                "id": "28",
                "name": "10",
                "garment_id": "2",
                "view": "back",
                "pos_x": "44",
                "pos_y": "6",
                "description": ""
            },
            {
                "id": "29",
                "name": "11",
                "garment_id": "2",
                "view": "back",
                "pos_x": "42",
                "pos_y": "17",
                "description": ""
            },

            {
                "id": "46",
                "name": "Custom",
                "garment_id": "2",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ],
        "3": [
            {
                "id": "36",
                "name": "1",
                "garment_id": "3",
                "view": "front",
                "pos_x": "49",
                "pos_y": "8",
                "description": ""
            },
            {
                "id": "37",
                "name": "2",
                "garment_id": "3",
                "view": "front",
                "pos_x": "32",
                "pos_y": "39",
                "description": ""
            },
            {
                "id": "38",
                "name": "3",
                "garment_id": "3",
                "view": "front",
                "pos_x": "67",
                "pos_y": "40",
                "description": ""
            },
            {
                "id": "39",
                "name": "4",
                "garment_id": "3",
                "view": "front",
                "pos_x": "34",
                "pos_y": "70",
                "description": ""
            },
            {
                "id": "40",
                "name": "5",
                "garment_id": "3",
                "view": "front",
                "pos_x": "66",
                "pos_y": "71",
                "description": ""
            },
            {
                "id": "41",
                "name": "6",
                "garment_id": "3",
                "view": "front",
                "pos_x": "47",
                "pos_y": "88",
                "description": ""
            },
            {
                "id": "47",
                "name": "Custom",
                "garment_id": "3",
                "view": "front",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            },
            {
                "id": "42",
                "name": "7",
                "garment_id": "3",
                "view": "back",
                "pos_x": "50",
                "pos_y": "14",
                "description": ""
            },

            {
                "id": "48",
                "name": "Custom",
                "garment_id": "3",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ],
        "4": [
            {
                "id": "1",
                "name": "1",
                "garment_id": "4",
                "view": "front",
                "pos_x": "49",
                "pos_y": "6",
                "description": ""
            },
            {
                "id": "2",
                "name": "2",
                "garment_id": "4",
                "view": "front",
                "pos_x": "33",
                "pos_y": "18",
                "description": ""
            },
            {
                "id": "3",
                "name": "3",
                "garment_id": "4",
                "view": "front",
                "pos_x": "68",
                "pos_y": "18",
                "description": ""
            },
            {
                "id": "4",
                "name": "4",
                "garment_id": "4",
                "view": "front",
                "pos_x": "31",
                "pos_y": "36",
                "description": ""
            },
            {
                "id": "5",
                "name": "5",
                "garment_id": "4",
                "view": "front",
                "pos_x": "72",
                "pos_y": "32",
                "description": ""
            },
            {
                "id": "6",
                "name": "6",
                "garment_id": "4",
                "view": "front",
                "pos_x": "68",
                "pos_y": "38",
                "description": ""
            },
            {
                "id": "7",
                "name": "7",
                "garment_id": "4",
                "view": "front",
                "pos_x": "29",
                "pos_y": "61",
                "description": ""
            },
            {
                "id": "8",
                "name": "8",
                "garment_id": "4",
                "view": "front",
                "pos_x": "26",
                "pos_y": "66",
                "description": ""
            },
            {
                "id": "9",
                "name": "9",
                "garment_id": "4",
                "view": "front",
                "pos_x": "67",
                "pos_y": "66",
                "description": ""
            },
            {
                "id": "10",
                "name": "10",
                "garment_id": "4",
                "view": "front",
                "pos_x": "7",
                "pos_y": "80",
                "description": ""
            },
            {
                "id": "11",
                "name": "11",
                "garment_id": "4",
                "view": "front",
                "pos_x": "85",
                "pos_y": "84",
                "description": ""
            },
            {
                "id": "12",
                "name": "12",
                "garment_id": "4",
                "view": "front",
                "pos_x": "32",
                "pos_y": "89",
                "description": ""
            },
            {
                "id": "13",
                "name": "13",
                "garment_id": "4",
                "view": "front",
                "pos_x": "66",
                "pos_y": "89",
                "description": ""
            },
            {
                "id": "49",
                "name": "Custom",
                "garment_id": "4",
                "view": "front",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            },
            {
                "id": "14",
                "name": "14",
                "garment_id": "4",
                "view": "back",
                "pos_x": "44",
                "pos_y": "9",
                "description": ""
            },
            {
                "id": "15",
                "name": "15",
                "garment_id": "4",
                "view": "back",
                "pos_x": "12",
                "pos_y": "53",
                "description": ""
            },
            {
                "id": "16",
                "name": "16",
                "garment_id": "4",
                "view": "back",
                "pos_x": "84",
                "pos_y": "56",
                "description": ""
            },
            {
                "id": "17",
                "name": "17",
                "garment_id": "4",
                "view": "back",
                "pos_x": "13",
                "pos_y": "84",
                "description": ""
            },
            {
                "id": "18",
                "name": "18",
                "garment_id": "4",
                "view": "back",
                "pos_x": "85",
                "pos_y": "84",
                "description": ""
            },

            {
                "id": "50",
                "name": "Custom",
                "garment_id": "4",
                "view": "back",
                "pos_x": "50",
                "pos_y": "100",
                "description": ""
            }
        ]
    }

    return logo_positions_new_3d;
});