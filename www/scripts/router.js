define(['jquery', 'knockout'], function ($, ko) {
	//var router = {};
	$(document).on("pageinit", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'i');
	});

	$(document).on("pagebeforeload", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'bc');
		finished = false;
	});

	$(document).on("pageload", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'l');
	});

	$(document).on("pagebeforecreate", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'bc');
	});

	$(document).on("pagecreate", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'c');
		lastPage = currentPage;
		currentPage = event.target.id;
	});

	$(document).on("pagebeforeshow", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'bs');
	});

	$(document).on("pageshow", function (event) { // When entering pagetw
		searchRoute(event.target.id, 's');
		if (isSpecial) {
			//runSpecialCase(targetPage);
		}
		finished = true;
	});

	$(document).on("pagebeforehide", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'bh');
	});

	$(document).on("pagehide", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'h');
		removePage(lastPage);
	});

	$(document).on("pageremove", function (event) { // When entering pagetw
		searchRoute(event.target.id, 'rm');
	});




	currentPage = 'index';
	finished = true;
	lastPage = '';
	targetPage = '';

	var router = [

		//Components

		{
			"#selectFabric": {
				handler: function () {
					ko.applyBindings(fabricsVM, document.getElementById('selectFabric'));
				},
				events: "c"
			}
		},

		{
			"#selectFabric": {
				handler: function () {
					fabricsVM.initIscroll();
					fabricsVM.retrieveContext();
				},
				events: "s"
			}
		},
		{
			"#selectFabric": {
				handler: function () {

				},
				events: "bh"
			}
		},

		{
			"#editCustomer": {
				handler: function () {
					ko.applyBindings(customersVM, document.getElementById('editCustomer'));
				},
				events: "c"
			}
		},


		// CUSTOMERS //
		{
			"#customerFittingSearch": {
				handler: function () {
					customersVM.systemMode = "fitting";
				},
				events: "c"
			}
		},
		{
			"#customerFittingSearch": {
				handler: function () {
					customersVM.systemMode = "fitting";
				},
				events: "h"
			}
		},
		{
			"#customerFittingSearch": {
				handler: function () {
					//ko.applyBindings(customersVM, document.getElementById('listCustomer'));
					ko.applyBindings(orderItem, document.getElementById('customerFittingSearch'));
				},
				events: "s"
			}
		},
		{
			"#CustomerFittingSearch": {
				handler: function () {
					//if ((customersVM.selectedCustomer != {}) && (customersVM.selectedCustomer != undefined))
					//    customersVM.customerAutocomplete("");
					if ((orderItem.custSelectVM.selectedCustomer() != {}) && (orderItem.custSelectVM.selectedCustomer() != undefined))
						orderItem.custSelectVM.customerAutocomplete("");
				},
				events: "bs"
			}
		},

		{
			"#listCustomer": {
				handler: function () {
					customersVM.systemMode = "customer";
				},
				events: "s"
			}
		},
		{
			"#listCustomer": {
				handler: function () {
					customersVM.systemMode = "customer";
				},
				events: "h"
			}
		},
		{
			"#listCustomer": {
				handler: function () {
					//ko.applyBindings(customersVM, document.getElementById('listCustomer'));
					ko.applyBindings(orderItem, document.getElementById('listCustomer'));
				},
				events: "c"
			}
		},
		{
			"#listCustomer": {
				handler: function () {
					//if ((customersVM.selectedCustomer != {}) && (customersVM.selectedCustomer != undefined))
					//    customersVM.customerAutocomplete("");
					if ((orderItem.custSelectVM.selectedCustomer() != {}) && (orderItem.custSelectVM.selectedCustomer() != undefined))
						orderItem.custSelectVM.customerAutocomplete("");
				},
				events: "bs"
			}
		},
		{
			"#addCustomer": {
				handler: function () {
					//	customersVM.systemMode = "customer";
					try {
						customersVM.setObservables('');
					} catch (e) { ; }
					ko.applyBindings(customersVM, document.getElementById('addCustomer'));
				},
				events: "c"
			}
		},
		{
			"#updateCustomer": {
				handler: function () {
					ko.applyBindings(customersVM, document.getElementById('updateCustomer'));
				},
				events: "c"
			}
		},

		{
			"#customerInfo": {
				handler: function () {
					customersVM.setObservables(customersVM.selected_item());
					customersVM.systemMode = "customer";
				},
				events: "s"
			}
		},
		{
			"#customerInfo": {
				handler: function () {
					customersVM.systemMode = "customer";
				},
				events: "h"
			}
		},
		{
			"#customerInfo": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('customerInfo'));
				},
				events: "c"
			}
		},
		{
			"#customerNotes": {
				handler: function () {
					//ko.applyBindings(customersVM, document.getElementById('customerInfo'));
					ko.applyBindings(orderItem, document.getElementById('customerNotes'));
				},
				events: "c"
			}
		},
		{
			"#customerOrders": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('customerOrders'));

					localStorage.removeItem('customerOrdersDS');
					customerOrdersDS = new SyncableDatasource('customerOrdersDS', 'customerOrdersDS', dsRegistry, 'customer_orders_endpoint');
					orderItem.custOrdersVM.subscribeTo('customerOrdersDS');
				},
				events: "c"
			}
		},

		{
			"#customerGarmentsFittingsList": {
				handler: function () {
					var html = $('#customerInfo').remove();
					customersVM.setObservables(customersVM.selected_item());
					ko.applyBindings(orderItem, document.getElementById('customerGarmentsFittingsList'));

					orderItem.OrdersGarmentsForFittingVM.ordersgarments([]);
					localStorage.removeItem('OrdersGarmentsForFittingDS');
					OrdersGarmentsForFittingDS = new SyncableDatasource('OrdersGarmentsForFittingDS', 'OrdersGarmentsForFittingDS', dsRegistry, 'customer_garments_fittings_endpoint2', true);


					orderItem.OrdersGarmentsForFittingVM.subscribeTo('OrdersGarmentsForFittingDS');

					for (var x = 0; x < 100; x++)
					{
						localStorage.removeItem('FittingFrontImagePhoto' + x);
						localStorage.removeItem('FittingBackImagePhoto' + x);
						localStorage.removeItem('FittingSideImagePhoto' + x);
						localStorage.removeItem('FittingCustomImagePhoto' + x);
						localStorage.removeItem('FittingFrontImageTags' + x);
						localStorage.removeItem('FittingBackImageTags' + x);
						localStorage.removeItem('FittingSideImageTags' + x);
						localStorage.removeItem('FittingCustomImageTags' + x);

						localStorage.removeItem('CompletionFrontImagePhoto' + x);
						localStorage.removeItem('CompletionBackImagePhoto' + x);
						localStorage.removeItem('CompletionSideImagePhoto' + x);
					}
				},
				events: "s"
			}
		},

		{
			'#fittings': {
				handler: function () {
					//orderItem.MediaUploadVM = new MediaUploadVM(orderItem.DsRegistry);
					orderItem.fittingsVM = null;
					localStorage.removeItem("fittingindex");
				},
				events: "bh" // pagebeforehide
			}
		},

		{
			'#fittings': {
				handler: function () {


					orderItem.MediaUploadVM.clear();
					//orderItem.MediaUploadVM.media = orderItem.fittingsVM.media;
					//orderItem.MediaUploadVM.mediaID = orderItem.fittingsVM.mediaID;
					orderItem.MediaUploadVM.imageGaleryAction = function callClick() { $('#FittingFrontImageFiles').click(); };
					//orderItem.MediaUploadVM = new MediaUploadVM(null);
				},
				events: "bc" //pagebeforeload
			}
		},

		{
			'#fittings': {
				handler: function () {


					orderItem.MediaUploadVM.clear();

					$('#FittingFrontImageMediaBtn').click(function () {
						changeVisibilityDiv('imagesDialog', 'block');
						//$('#FittingFrontImageFiles').click();
						//orderItem.MediaUploadVM.choosePhoto();
						//orderItem.fittingsVM.takePicture();
					});
					$('#FittingSideImageMediaBtn').click(function () {
						changeVisibilityDiv('imagesDialog', 'block');
						//$('#FittingSideImageFiles').click();
						//orderItem.MediaUploadVM.choosePhoto();
						//orderItem.fittingsVM.takePicture();
					});
					$('#FittingBackImageMediaBtn').click(function () {
						changeVisibilityDiv('imagesDialog', 'block');
						//$('#FittingBackImageFiles').click();
						//orderItem.MediaUploadVM.choosePhoto();
						//orderItem.fittingsVM.takePicture();
						changeVisibilityDiv('imagesDialog', true);
					});
					$('#FittingCustomImageMediaBtn').click(function () {
						changeVisibilityDiv('imagesDialog', 'block');
						//$('#FittingCustomImageFiles').click();
						//orderItem.MediaUploadVM.choosePhoto();
						//orderItem.fittingsVM.takePicture();
					});
					$('#FittingVideoBtn').click(function () {
						//$('#FittingCustomImageFiles').click();
						//orderItem.MediaUploadVM.chooseVideo();
						//orderItem.fittingsVM.takePicture();
					});

					// get image type. Can be front/ back / side or "custom".
					// used in fittingVm
					img_type = ""
					$('.img_uploader_btn').click(function () {
						img_type = $(this).data("imageType");
					});

					// Sleep necessary to give time for the tablets to load everything
					sleep(1500).then(() => {
						ko.applyBindings(orderItem.fittingsVM, document.getElementById('fittings'));
					});

				},
				events: "c" //pagecreate
			}
		},

		{
			"#fittings": {
				handler: function () {
					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 400);
				},
				events: "s" //pageshow
			}
		},

		{
			'#fittingList': {
				handler: function () {
					var html = $('#customerInfo').remove();
					orderItem.FittingsListVM.loadFittingPage();
					ko.applyBindings(orderItem.FittingsListVM, document.getElementById('fittingsList'));
				},
				events: "s"
			}
		},
		{
			'#outsideCustomerFitting': {
				handler: function () {
					outsideCustomerFitting = new OutsideCustomerFitting();
					ko.applyBindings(outsideCustomerFitting, document.getElementById('outsideCustomerFitting'));
				},
				events: "s"
			}
		},


		{
			'#completions': {
				handler: function () {
					ko.applyBindings(orderItem.completionsVM, document.getElementById('completions'));


					$('#CompletionFrontImageMediaBtn').click(function () {
						//document.getElementById('CompletionImageFiles').trigger('click');
						//orderItem.MediaUploadVM.choosePhoto();
						orderItem.completionsVM.clickPhoto(1);
					});
					$('#CompletionSideImageMediaBtn').click(function () {
						//document.getElementById('CompletionImageFiles').trigger('click');
						//orderItem.MediaUploadVM.choosePhoto();
						orderItem.completionsVM.clickPhoto(2);
					});
					$('#CompletionBackImageMediaBtn').click(function () {
						//document.getElementById('CompletionImageFiles').trigger('click');
						//orderItem.MediaUploadVM.choosePhoto();
						orderItem.completionsVM.clickPhoto(3);
					});
					$('#CompletionExtraImageMediaBtn').click(function () {
						//document.getElementById('CompletionImageFiles').trigger('click');
						//orderItem.MediaUploadVM.choosePhoto();
						orderItem.completionsVM.clickPhoto(4);
					});
				},
				events: "s"
			}
		},

		{
			'#completions': {
				handler: function () {
					orderItem.MediaUploadVM.clear();
					//orderItem.MediaUploadVM.media = orderItem.completionsVM.media;
					//orderItem.MediaUploadVM.mediaID = orderItem.completionsVM.mediaID;
					orderItem.MediaUploadVM.imageGaleryAction = function callClick() { $('#CompletionImageFiles').click(); };
					//orderItem.MediaUploadVM = new MediaUploadVM(null);
				},
				events: "bc"
			}
		},

		{
			'#completions': {
				handler: function () {
					orderItem.MediaUploadVM = new MediaUploadVM(orderItem.DsRegistry);
				},
				events: "bh"
			}
		},

		{
			'#alterationsMain': {
				handler: function () {
					alterationsMainVM = new AlterationMainPageVM()
					ko.applyBindings(alterationsMainVM, document.getElementById('fittingView'));
				},
				events: "s"
			}
		},

		{
			'#alterationsDashBoard': {
				handler: function () {


					alterationsVM = new AlterationsVM();

					// seccond parameter => for the back button to work properly
					ko.applyBindings(alterationsVM, document.getElementById('fittingView'));

				},
				events: "s"
			}
		},

		{
			'#globalAlterationsBriefList': {
				handler: function () {
					alterationsBriefVM = new AlterationsBriefVM(true);

					// seccond parameter => for the back button to work properly
					ko.applyBindings(alterationsBriefVM, document.getElementById('fittingView'));

				},
				events: "s"
			}
		},
		{
			'#alterationsBriefList': {
				handler: function () {

					alterationsBriefVM = new AlterationsBriefVM(false);

					// seccond parameter => for the back button to work properly
					ko.applyBindings(alterationsBriefVM, document.getElementById('fittingView'));

				},
				events: "s"
			}
		},

		{
			'#briefDetails': {
				handler: function () {
					// first parameter => initialized in AlterationsBriefVM (VM father)
					// seccond parameter => for the back button to work properly
					ko.applyBindings(briefDetails, document.getElementById('fittingView'));

				},
				events: "s"
			}
		},


		{
			'#alterationsReportList': {
				handler: function () {


					alterationReportListVM = new AlterationsReportListVM(false);
					// seccond parameter => for the back button to work properly
					ko.applyBindings(alterationReportListVM, document.getElementById('fittingView'));

				},
				events: "s"
			}
		},
		{
			'#globalAlterationsReportList': {
				handler: function () {


					alterationReportListVM = new AlterationsReportListVM(true);
					// seccond parameter => for the back button to work properly
					ko.applyBindings(alterationReportListVM, document.getElementById('fittingView'));

				},
				events: "s"
			}
		},

		{
			'#activeAlterations': {
				handler: function () {

					console.log("going to ActiveAlterations");

					// Create viewModel
					activeAlterations = new ActiveAlterationsVM();

					// seccond parameter => for the back button to work properly
					ko.applyBindings(activeAlterations, document.getElementById('ActiveAlterations'));

				},
				events: "s"
			}
		},

		{
			'#alterationReportSubmission': {
				handler: function () {
					// give time to load UI
					sleep(1500).then( () => {
						console.log("going to report submission...")
						ko.applyBindings(alterationsReportSubmissionVM, document.getElementById('fittingView'));
					});
				},
				events: "s"
			}
		},







		{
			'#fittingView': {
				handler: function () {
					ko.applyBindings(fittingPreview, document.getElementById('fittingView'));
				},
				events: "s"
			}
		},

		{
			"#alterationsList": {
				handler: function () {
					ko.applyBindings(alterationsList, document.getElementById('alterationsList'));

				},
				events: "s"
			}
		},

		{
			'#alterationReport': {
				handler: function () {

					ko.applyBindings(alterationReport, document.getElementById('alterationReport'));
					$("#sliding").slick({
						lazyLoad: 'ondemand',
						slidesToShow: 6,
						slidesToScroll: 1,
						arrows: false
					});
					alterationReport.initializeSketchpads();

				},
				events: "s"
			}
		},


		{
			"#orderInfo": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('orderInfo'));

					localStorage.removeItem('customerOrderDS');
					customerOrderDS = new SyncableDatasource('customerOrderDS', 'customerOrderDS', dsRegistry, 'customer_order_endpoint');
					//customerOrdersVM.subscribeTo('customerOrdersDS');
					orderItem.custOrderVM.subscribeTo('customerOrderDS');
					//	customerOrderDS.sync();
					localStorage.removeItem('fittingphotos');
					localStorage.removeItem('fittingtags');

					localStorage.removeItem('FittingsDS');
					FittingsDS = new SyncableDatasource('FittingsDS', 'FittingsDS', dsRegistry, 'order_fittings_endpoint');
					orderItem.OrderFittingsVM.subscribeTo('FittingsDS');

					tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');

					orderItem.FittingVM = new FittingClass(dsRegistry);

				},
				events: "c"
			}
		},

		{
			"#fittingImages": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('fittingImages'));
					$('#custMediaBtn').click(function () {
						$('#files').click();
					});
				},
				events: "c"
			}
		},


		{
			"#fittingVideo": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('fittingVideo'));
					/*   document.getElementById('custMediaBtn').on('click', function() {
						document.getElementById('files').trigger('click');
					});*/
				},
				events: "c"
			}
		},

		{
			"#assignTailor": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('assignTailor'));
				},
				events: "c"
			}
		},

		{
			"#viewMeasurements": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('viewMeasurements'));
				},
				events: "c"
			}
		},


		{
			"#paymentsHistory": {
				handler: function () {

					localStorage.removeItem('customerOrderPaymentsDS');
					customerOrderPaymentsDS = new SyncableDatasource('customerOrderPaymentsDS', 'customerOrderPaymentsDS', dsRegistry, 'customer_order_payments_endpoint');
					orderItem.custOrderPaymentsVM.subscribeTo('customerOrderPaymentsDS');

					orderItem.OrderPaymentVM = new OrderPaymentClass(dsRegistry);

					localStorage.removeItem('paymentphoto');
					localStorage.removeItem('PaymentCreditCardPhoto');


					// sleep to give time to all components to render
					sleep(1500).then( () => {
						ko.applyBindings(orderItem, document.getElementById('paymentsHistory'));
						orderItem.custOrderPaymentsVM.configureEventListeners();
					});

				},
				events: "c"
			}
		},

		{
			"#makePayment": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('makePayment'));

					$('#creditCardImageMediaBtn').click(function () {
						$('#creditCardImageFiles').click();
					});
				},
				events: "c"
			}
		},

		{
			"#errorSubmit": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('errorSubmit'));
				},
				events: "c"
			}
		},



		{
			"#mainPage": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('mainPage'));
					customersVM.systemMode = 'main';
					localStorage.removeItem('customerOrdersDS');
					localStorage.removeItem('customerOrderDS');

					localStorage.removeItem('photos');
					localStorage.removeItem('tags');
					localStorage.removeItem('fitlinephotos');
					localStorage.removeItem('fitlinetags');
					localStorage.removeItem('PaymentCreditCardPhoto');
					localStorage.removeItem('CreditCardPhoto');
					for (var x = 0; x < 20; x++) {
						localStorage.removeItem('FittingFrontImagePhoto' + x);
						localStorage.removeItem('FittingBackImagePhoto' + x);
						localStorage.removeItem('FittingSideImagePhoto' + x);
						localStorage.removeItem('FittingCustomImagePhoto' + x);
						localStorage.removeItem('FittingFrontImageTags' + x);
						localStorage.removeItem('FittingBackImageTags' + x);
						localStorage.removeItem('FittingSideImageTags' + x);
						localStorage.removeItem('FittingCustomImageTags' + x);
						localStorage.removeItem('CompletionFrontImagePhoto' + x);
						localStorage.removeItem('CompletionBackImagePhoto' + x);
						localStorage.removeItem('CompletionSideImagePhoto' + x);
						localStorage.removeItem('AlterationReportImage' + x);
						localStorage.removeItem('AlterationReportCostImage' + x);

						localStorage.removeItem('ShirtCustomImages' + x);
						localStorage.removeItem('ShirtCustomImagesTags' + x);
						localStorage.removeItem('JacketCustomImages' + x);
						localStorage.removeItem('JacketCustomImagesTags' + x);
						localStorage.removeItem('SuitCustomImages' + x);
						localStorage.removeItem('SuitCustomImagesTags' + x);
						localStorage.removeItem('PantCustomImages' + x);
						localStorage.removeItem('PantCustomImagesTags' + x);
						localStorage.removeItem('VestCustomImages' + x);
						localStorage.removeItem('VestCustomImagesTags' + x);
					}

					localStorage.removeItem('JacketCustomStructurePhoto');
					localStorage.removeItem('JacketCustomBottomPhoto');
					localStorage.removeItem('JacketCustomVentPhoto');
					localStorage.removeItem('JacketCustomLapelPhoto');
					localStorage.removeItem('JacketEmbroideryPhoto');
					localStorage.removeItem('JacketCustomMonogramPhoto');
					localStorage.removeItem('pantsCustomPocketsPhoto');
					localStorage.removeItem('suitJacketCustomStructurePhoto');
					localStorage.removeItem('suitJacketCustomBottomPhoto');
					localStorage.removeItem('suitJacketCustomVentPhoto');
					localStorage.removeItem('suitJacketCustomLapelPhoto');
					localStorage.removeItem('suitJacketCustomMonogramPhoto');
					localStorage.removeItem('suitJacketEmbroideryPhoto');
					localStorage.removeItem('pantsCustomPocketsPhoto');
					localStorage.removeItem('vestCustomLapelPhoto');
					localStorage.removeItem('vestCustomButtonsPhoto');
					localStorage.removeItem('ShippingFabricPhoto');
					localStorage.removeItem('ShippingCopyGarmentPhotos');
					localStorage.removeItem('ShippingCopyGarmentTags');
					localStorage.removeItem('ShippingFaultyRemakeGarmentsPhotos');
					localStorage.removeItem('ShippingFaultyRemakeGarmentsTags');
					localStorage.removeItem('ShippingOtherPhoto');
					localStorage.removeItem('BoxSendPhoto');

					var loginpage = document.getElementById("loginPage");
					//console.log("test: " + loginpage);
					if (loginpage == null) { // if coming from the login page, don't sync again
						custData.sync();
						fitlinesJacketDS.sync();
						//  	fitlinesPantsDS.sync();
					}

					localStorage.removeItem('ShippingNotificationsDS');
					ShippingNotificationsDS = new SyncableDatasource('ShippingNotificationsDS', 'ShippingNotificationsDS', dsRegistry, 'shipping_notifications_numbers_endpoint');

					try {
						var st = orderItem.dsRegistry.getDatasource('countriesDS').getStore();
						timestamp = st.timestamp;
						currenttimestamp = new Date().getTime();
						currenttimestamp = Math.floor(currenttimestamp / 1000)
						difference = currenttimestamp - timestamp;
						minutesdifference = difference / 60;
						hoursdifference = minutesdifference / 60;

						if (hoursdifference > 3 && loginpage == null) { // sync if not coming from the login page and more than 3 hours have passed since the last sync
							countriesDS.sync();
							statesData.sync();
							citiesData.sync();
							//tailorsDS.sync();
							tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');
							currenciesDS.sync();
							pricerangeDS.sync();
						}
						orderItem = new OrderItem(dsRegistry);
						//orderItem.Notifications = new NotificationsVM(null);
						//orderItem.Notifications.loadInfo();
					} catch (e) {
						customAlert("Unrecoverable data error. Resetting system.");
						orderItem.clearStorageAndExit();
					}

					/*
					* This 2 variables, controls orderVM and preOrderVM.
					* Only create a new instance if, value is false.

					* I'm doing this to prevent loose information of preOrder when going forward them back
					* Only reset this variable in mainPage
					*/
					preOrderDefined = false;
					ordersDefined = false;
				},
				events: "c"
			}
		},

		{
			"#mainPage": {
				handler: function () {
					orderItem.Notifications = new NotificationsVM(null);
					orderItem.Notifications.loadInfo();
				},
				events: "bc"
			}
		},
		{
			"#mainPage": {
				handler: function () {
					// global variable it says if we must to show old/new 3D
					window.should_use_new_3D = $("#use-new-3D").attr('checked') ? true : false;
				},
				events: "bh"
			}
		},

		{
			"#settings": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('customerEditMeasurement'));
				},
				events: "c"
			}
		},
		{
			"#qc_and_error": {
				handler: function () {
					// qualityControlVM = new QualityControlVM();
					ko.applyBindings(null, document.getElementById('qc_and_error'));
				},
				events: "c"
			}
		},
		{
			"#qc": {
				handler: function () {
					qualityControlVM = new QualityControlVM();
					ko.applyBindings(qualityControlVM, document.getElementById('qc'));
				},
				events: "c"
			}
		},
		{
			"#genericError": {
				handler: function () {
					genericError = new GenericErrorVM();
					ko.applyBindings(genericError, document.getElementById('genericError'));
				},
				events: "c"
			}
		},

		{
			"#profitability":{
				handler: function(){
					require.undef('viewmodel/ProfitabilityVM');
					require(['viewmodel/ProfitabilityVM']);


					profitability = new Profitability();
					ko.applyBindings(profitability, document.getElementById('profitability'));

				},
				events: "c"
			}
		},
		{
			"#config_global_vars":{
				handler: function(){

					ko.applyBindings(config_global_vars, document.getElementById('config_global_vars'));

				},
				events: "c"
			}
		},

		{
			"#employee_invoice":{
				handler: function(){
					employeeInvoiceVM = new EmployeeInvoiceVM();
					ko.applyBindings(employeeInvoiceVM, document.getElementById('employee_invoice'));

				},
				events: "c"
			}
		},
		{
			"#employee_invoice":{
				handler: function(){
					require.undef('viewmodel/EmployeeInvoiceVM');
					require(['viewmodel/EmployeeInvoiceVM']);

				},
				events: "rm"
			}
		},

		// order customers create
		{
			"#orderItemAddCustomer": {
				handler: function () {
					console.log('ROUTER 2');
					$("input[type='radio']").on('change', function () {
						$("input[type='radio']").checkboxradio("refresh");
					});


					ko.applyBindings(orderItem, document.getElementById('orderItemAddCustomer'));
				},
				events: "c"
			}
		},
		{
			"#orderItemUpdateCustomer": {
				handler: function () {
					$("input[type='radio']").on('change', function () {
						$("input[type='radio']").checkboxradio("refresh");
					});
					ko.applyBindings(orderItem, document.getElementById('orderItemUpdateCustomer'));
				},
				events: "c"
			}
		},
		{
			"#orderItemCustomerInfo": {
				handler: function () {
					customersVM.systemMode = "order";
					customersVM.setObservables(customersVM.selected_item());
					orderItem.customersVMmirror = customersVM;
				},
				events: "s"
			}
		},
		{
			"#orderItemCustomerInfo": {
				handler: function () {
					customersVM.systemMode = "order";
				},
				events: "h"
			}
		},
		{
			"#orderItemCustomerInfo": {
				handler: function () {
					//custData.sync();
					ko.applyBindings(orderItem, document.getElementById('orderItemCustomerInfo'));
				},
				events: "s"
			}
		},

		// order customers before show
		{
			"#orderItemAddCustomer": {
				handler: function () {
					console.log('ROUTER 3');
					customersVM.setObservables('');
					orderItem.customersVMmirror = customersVM;
					orderItem.customersVMmirror.setObservables('');
					$("#subscribe").attr("checked", true).checkboxradio("refresh");
				},
				events: "bs"
			}
		},


		{
			"#orderItemAddCustomer": {
				handler: function () {
					customersVM.systemMode = "order";
				},
				events: "s"
			}
		},
		{
			"#orderItemUpdateCustomer": {
				handler: function () {
					customersVM.setObservables(customersVM.selected_item());
				},
				events: "bs"
			}
		},
		{
			"#orderItemUpdateCustomer": {
				handler: function () {
					customersVM.systemMode = "order";
				},
				events: "s"
			}
		},
		{
			"#orderItemUpdateCustomer": {
				handler: function () {
					customersVM.systemMode = "order";
				},
				events: "h"
			}
		},

		{
			"#customerEditCustomer": {
				handler: function () {
					customersVM.setObservables(customersVM.selected_item());
				},
				events: "bs"
			}
		},
		{
			"#customerEditCustomer": {
				handler: function () {
					customersVM.systemMode = "customer";
				},
				events: "s"
			}
		},
		{
			"#customerEditCustomer": {
				handler: function () {
					customersVM.systemMode = "customer";
				},
				events: "h"
			}
		},
		{
			"#customerEditCustomer": {
				handler: function () {
					//ko.applyBindings(customersVM, document.getElementById('customerEditCustomer'));
					ko.applyBindings(orderItem, document.getElementById('customerEditCustomer'));
				},
				events: "c"
			}
		},



		{
			"#orderItemAddCustomer": {
				handler: function () {
					console.log('ROUTER 5');
					customersVM.systemMode = "order";
				},
				events: "h"
			}
		},


		{
			"#firstPage": {
				handler: function () {

				},
				events: "c"
			}
		},


		{
			"#loginPage": {
				handler: function () {
					ko.applyBindings(authCtrl, document.getElementById('loginPage'));
				},
				events: "c"
			}
		},
		{
			"#loginPage": {
				handler: function () {
					document.getElementById('iBespoken img').hide();
					document.getElementById('iIntroLogo img').hide();
					document.getElementById('cname').hide();
					document.getElementById('cpass').hide();

					document.getElementById('iIntroLogo img').fadeIn('slow');
					document.getElementById('cname').fadeIn('slow');
					document.getElementById('cpass').fadeIn('slow', function () {
						document.getElementById('iBespoken img').fadeIn('slow');
					});
					//posChangePage("#orderItemSelectCustomer");

				},
				events: "s"
			}
		},

		{
			"#orderItemReview": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('orderItemReview'));

					$('#creditCardImageMediaBtn').click(function () {
						$('#creditCardImageFiles').click();
					});
				},
				events: "s"
			}
		},

		{
			"#orderItemPopulateGarments": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('orderItemPopulateGarments'));
				},
				events: "s"
			}
		},
		{
			"#orderItemPopulateGarments": {
				handler: function () {
					orderItem.populateGarments();

				},
				events: "bh"
			}
		},


		{
			"#orderItemSelectGarment": {
				handler: function () {
					//OrderItemSelectGarmentVM OrderItemSelectGarmentload
					ko.applyBindings(orderItem, document.getElementById('orderItemSelectGarment'));

					if (orderItem.lockselects == true) {
						orderItem.initializeHtmlFromExtraPants();
					}



					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 1500);
				},
				events: "s"
			}
		},
		{
			"#orderItemSelectGarment": {
				handler: function () {

					orderItem.prepareGarments();

				},
				events: "bs"
			}
		},

		{
			"#orderItemSelectCustomer": {
				handler: function () {
					customersVM.systemMode = "order";
					ko.applyBindings(orderItem, document.getElementById('orderItemSelectCustomer'));
				},
				events: "c"
			}
		},
		{
			"#orderItemSelectCustomer": {
				handler: function () {
					customersVM.systemMode = "order";
					if ((orderItem.custSelectVM.selectedCustomer() != {}) && (orderItem.custSelectVM.selectedCustomer() != undefined))
						orderItem.custSelectVM.customerAutocomplete("");
				},
				events: "bs"
			}
		},

		/////////////////////////////////////////

		{
			'#garmentsJacket': {
				handler: function () {
					orderItem.MediaUploadVM.setMediaCallback(orderItem.garmentsVM.mediaCallback);
					orderItem.MediaUploadVM.imageGaleryAction = orderItem.garmentsVM.galleryImages;
					ko.applyBindings(orderItem, document.getElementById('garmentsJacket'));

				},
				events: "c"
			}
		},

		{
			"#garmentsJacket": {
				handler: function () {
					orderItem.ClientFabric(false);
					orderItem.AddClientFabric(false);
					orderItem.garmentsVM.attachCloneJacketAttribute();
					orderItem.garmentsVM.JacketFabricSelect();
					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 400);
				},
				events: "s"
			}
		},



		////////////////////////////////////////////////////////////////////////////////
		{
			'#garmentsPant': {
				handler: function () {
					orderItem.ClientFabric(false);
					orderItem.AddClientFabric(false);
					orderItem.MediaUploadVM.setMediaCallback(orderItem.garmentsVM.mediaCallback);
					orderItem.MediaUploadVM.imageGaleryAction = orderItem.garmentsVM.galleryImages;
					ko.applyBindings(orderItem, document.getElementById('garmentsPant'));
				},
				events: "c"
			}
		},
		{
			"#garmentsPant": {
				handler: function () {
					orderItem.garmentsVM.attachClonePantsAttribute();
					orderItem.garmentsVM.PantFabricSelect();
					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 300);
				},
				events: "s"
			}
		},



		////////////////////////////////////////////////////////////////////////////////

		{
			'#garmentsVest': {
				handler: function () {
					orderItem.ClientFabric(false);
					orderItem.AddClientFabric(false);
					orderItem.MediaUploadVM.setMediaCallback(orderItem.garmentsVM.mediaCallback);
					orderItem.MediaUploadVM.imageGaleryAction = orderItem.garmentsVM.galleryImages;
					ko.applyBindings(orderItem, document.getElementById('garmentsVest'));
				},
				events: "c"
			}
		},
		{
			"#garmentsVest": {
				handler: function () {
					orderItem.garmentsVM.attachCloneVestAttribute();
					orderItem.garmentsVM.vestFabricSelect();
					// $('.garmentsDropdown').trigger( "expand" );
					//orderItem.garmentsVM.vestContrastFabricSelect();
					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 100);
				},
				events: "s"
			}
		},

		///////////////////////////////////////////////////////
		///////////////////////////////////////////////////////
		{
			'#garmentsSuit': {
				handler: function () {
					orderItem.ClientFabric(false);
					orderItem.AddClientFabric(false);
					orderItem.MediaUploadVM.setMediaCallback(orderItem.garmentsVM.mediaCallback);
					orderItem.MediaUploadVM.imageGaleryAction = orderItem.garmentsVM.galleryImages;
					ko.applyBindings(orderItem, document.getElementById('garmentsSuit'));
				},
				events: "c"
			}
		},
		{
			"#garmentsSuit": {
				handler: function () {
					orderItem.garmentsVM.suitFabricSelect();
					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 500);
				},
				events: "s"
			}
		},

		////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////

		{
			'#garmentsShirt': {
				handler: function () {
					orderItem.ClientFabric(false);
					orderItem.AddClientFabric(false);
					orderItem.MediaUploadVM.setMediaCallback(orderItem.garmentsVM.mediaCallback);
					orderItem.MediaUploadVM.imageGaleryAction = orderItem.garmentsVM.galleryImages;
					ko.applyBindings(orderItem, document.getElementById('garmentsShirt'));
				},
				events: "c"
			}
		},
		{
			"#garmentsShirt": {
				handler: function () {

					orderItem.garmentsVM.shirtFabricSelect();

					var spinner = document.getElementById('loading_jp');
					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 300);
				},
				events: "s"
			}
		},





		{
			"#measurements": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('measurements'));
					$('#fitlineMediaBtn').click(function () {
						$('#fitlinefiles').click();
					});
				},
				events: "c"
			}
		},

		{
			"#customerEditMeasurement": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('customerEditMeasurement'));
					console.log("router customerEditMeasurement");
				},
				events: "c"
			}
		},


		{
			"#bodyshape": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshape'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeShoulders": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeShoulders'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeShoulders": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeNeckStance": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeNeckStance'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeNeckStance": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeArms": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeArms'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeArms": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeBack": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeBack'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeBack": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeBeltAngle": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeBeltAngle'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeBeltAngle": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeBeltHeight": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeBeltHeight'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeBeltHeight": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeFront": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeFront'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeFront": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeLeg": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeLeg'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeLeg": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeNeckHeight": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeNeckHeight'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeNeckHeight": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeSeat": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeSeat'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeSeat": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeSleeve": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeSleeve'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeSleeve": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeStanding": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeStanding'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeStanding": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeStomach": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeStomach'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeStomach": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},

		{
			"#bodyshapeThigh": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('bodyshapeThigh'));
				},
				events: "c"
			}
		},
		{
			"#bodyshapeThigh": {
				handler: function () {
					$('.bodyshapeDropdown').trigger("expand");
				},
				events: "s"
			}
		},



		{
			"#customerEditBodyshape": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('customerEditBodyshape'));
				},
				events: "c"
			}
		},

		{
			"#customerEditMeasurment": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('customerEditMeasurment'));
				},
				events: "c"
			}
		},

		{
			"#orderItemCopyGarments": {
				handler: function () {
					orderItem.MediaUploadVM.setMediaCallback(orderItem.callbackMedia);
					orderItem.MediaUploadVM.imageGaleryAction = function callClick() { $('#files').click(); };
					ko.applyBindings(orderItem, document.getElementById('orderItemCopyGarments'));
					$('#custImageBtn').click(function () {
						//orderItem.clickMedia(orderItem.customerImage, 2);
						//$('#files').click();
						changeVisibilityDiv('imagesDialog', 'block');
					});
					$('#custVideoBtn').click(function () {
						orderItem.clickMedia(orderItem.copyGarmentVideo, 1);
						//$('#files').click();

					});
				},
				events: "s"
			}
		},
		{
			"#orderItemMedia": {
				handler: function () {
					orderItem.MediaUploadVM.setMediaCallback(orderItem.callbackMedia);
					orderItem.MediaUploadVM.imageGaleryAction = function callClick() { $('#files').click(); };
					ko.applyBindings(orderItem, document.getElementById('orderItemMedia'));
					$('#custImageBtn').click(function () {
						//orderItem.clickMedia(orderItem.customerImage, 2);
						//$('#files').click();
						changeVisibilityDiv('imagesDialog', 'block');
					});
				},
				events: "s"
			}
		},

		{
			"#orderItemVideo": {
				handler: function () {
					orderItem.MediaUploadVM.setMediaCallback(orderItem.callbackMedia);
					orderItem.MediaUploadVM.imageGaleryAction = function callClick() { alert('This feature is not available yet! Sorry for the incovenience'); };
					//document.getElementById('files').addEventListener('change', handleFileSelect, false);
					ko.applyBindings(orderItem, document.getElementById('orderItemVideo'));
					$('#custVideoBtn').click(function () {
						//orderItem.clickMedia(orderItem.customerVideo, 1);
						$('#files').click();

					});
				},
				events: "s"
			}
		},
		////////////////////////////////////////////////////////////////////////////////////
		//		SHIPPING
		////////////////////////////////////////////////////////////////////////////////////
		{
			"#shipping_main": {
				handler: function () {
					customersVM.systemMode = "shipping";
					ko.applyBindings(orderItem, document.getElementById('shipping_main'));
					localStorage.removeItem('ShippingFabricPhoto');
					localStorage.removeItem('ShippingCopyGarmentPhotos');
					localStorage.removeItem('ShippingCopyGarmentTags');
					localStorage.removeItem('ShippingFaultyRemakeGarmentsPhotos');
					localStorage.removeItem('ShippingFaultyRemakeGarmentsTags');
					localStorage.removeItem('ShippingOtherPhoto');
					localStorage.removeItem('BoxSendPhoto');

					orderItem = new OrderItem(dsRegistry);

					localStorage.removeItem('ShippingNotificationsDS');
					ShippingNotificationsDS = new SyncableDatasource('ShippingNotificationsDS', 'ShippingNotificationsDS', dsRegistry, 'shipping_notifications_numbers_endpoint');
				},
				events: "c"
			}
		},

		{
			"#shipping_select_customer": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('shipping_alterations_select_customer'));
					localStorage.removeItem('ShippingFabricPhoto');
					localStorage.removeItem('ShippingCopyGarmentPhotos');
					localStorage.removeItem('ShippingCopyGarmentTags');
					localStorage.removeItem('ShippingFaultyRemakeGarmentsPhotos');
					localStorage.removeItem('ShippingFaultyRemakeGarmentsTags');
					localStorage.removeItem('ShippingOtherPhoto');
				},
				events: "c"
			}
		},

		{
			"#shipping_addCustomer": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('shipping_addCustomer'));
					//ko.applyBindings(customersVM, document.getElementById('shipping_addCustomer'));
					try {
						customersVM.setObservables('');
					} catch (e) { ; }

				},
				events: "c"
			}
		},

		{
			"#shipping_alterations_list": { // USED FOR BOTH SHIPPING ALTERATIONS AND GARMENTS SCREEENS
				handler: function () {

					ko.applyBindings(orderItem, document.getElementById('shipping_alterations_list'));

					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";



					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 1500);
				},
				events: "c"
			}
		},

		{
			"#shipping_fabrics": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('shipping_fabrics'));
					//orderItem.ShippingFabricVM = new ShippingFabric(dsRegistry);
					$('#ShippingFabricMediaBtn').click(function () {
						$('#ShippingFabricImageFiles').click();
					});

					orderItem.ShippingFabricVM = new ShippingFabric(orderItem.dsRegistry);
					orderItem.ShippingFabricDS = new defShippingFabric('ShippingFabricDS', orderItem.dsRegistry);
					orderItem.ShippingFabricVM.subscribeTo('ShippingFabricDS');
					console.log("orderItem.shippingFabricDS.getStore(): " + JSON.stringify(orderItem.ShippingFabricDS.getStore()));
					//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
					orderItem.ShippingFabricVM.addVariantsShippingFabrics();

				},
				events: "c"
			}
		},

		{
			"#shipping_copy_garment": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('shipping_copy_garment'));
					//orderItem.ShippingCopyGarmentVM = new ShippingCopyGarment(dsRegistry);
					$('#ShippingCopyGarmentMediaBtn').click(function () {
						$('#ShippingCopyGarmentImageFiles').click();
					});

					orderItem.ShippingCopyGarmentVM = new ShippingCopyGarment(orderItem.dsRegistry);
					orderItem.ShippingCopyGarmentDS = new defShippingCopyGarment('ShippingCopyGarmentDS', orderItem.dsRegistry);
					orderItem.ShippingCopyGarmentVM.subscribeTo('ShippingCopyGarmentDS');
					console.log("orderItem.ShippingCopyGarmentDS.getStore(): " + JSON.stringify(orderItem.ShippingCopyGarmentDS.getStore()));
					//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
					orderItem.ShippingCopyGarmentVM.addVariantsShippingCopyGarments();

				},
				events: "c"
			}
		},

		{
			"#shipping_faulty_remake_garments": {
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('shipping_faulty_garments'));

					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";

					//orderItem.ShippingFaultyRemakeGarmentsVM = new ShippingFaultyRemakeGarments(dsRegistry);
					$('#ShippingFaultyRemakeGarmentsMediaBtn').click(function () {
						$('#ShippingFaultyRemakeGarmentsImageFiles').click();
					});

					// USING THE ALTERATIONS CLASS, INSTEAD OF CREATING A NEW GARMENTS CLASS
					localStorage.removeItem('AlterationsDS');
					AlterationsDS = new SyncableDatasource('AlterationsDS', 'AlterationsDS', dsRegistry, 'alterations_endpoint');
					orderItem.AlterationsVM.subscribeTo('AlterationsDS');


					orderItem.ShippingFaultyRemakeGarmentsVM = new ShippingFaultyRemakeGarments(orderItem.dsRegistry);
					orderItem.ShippingFaultyRemakeGarmentsDS = new defShippingFaultyRemakeGarments('ShippingFaultyRemakeGarmentsDS', orderItem.dsRegistry);
					orderItem.ShippingFaultyRemakeGarmentsVM.subscribeTo('ShippingFaultyRemakeGarmentsDS');
					console.log("orderItem.ShippingFaultyRemakeGarmentsDS.getStore(): " + JSON.stringify(orderItem.ShippingFaultyRemakeGarmentsDS.getStore()));
					//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
					orderItem.ShippingFaultyRemakeGarmentsVM.addVariantsShippingFaultyRemakeGarments();

					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 1500);

				},
				events: "c"
			}
		},

		{
			"#shipping_other": {
				handler: function () {
					orderItem.ShippingOtherVM = new ShippingOther(orderItem.dsRegistry, orderItem.custSelectVM, orderItem.MediaUploadVM);
					orderItem.ShippingOtherDS = new defShippingOther('ShippingOtherDS', orderItem.dsRegistry);
					orderItem.ShippingOtherVM.subscribeTo('ShippingOtherDS');
					orderItem.MediaUploadVM.setMediaCallback(orderItem.ShippingOtherVM.callbackMedia);
					orderItem.MediaUploadVM.imageGaleryAction = function callClick() { $('#ShippingOtherImageFiles').click(); };
					$('#custImageBtn').click(function () {
						//orderItem.clickMedia(orderItem.customerImage, 2);
						//$('#files').click();
						changeVisibilityDiv('imagesDialog', 'block');
					});
					$('#custVideoBtn').click(function () {
						orderItem.ShippingOtherVM.clickMedia(orderItem.ShippingOtherVM.otherVideo, 1);
						//$('#files').click();

					});
					ko.applyBindings(orderItem.ShippingOtherVM, document.getElementById('shipping_other'));
					//orderItem.ShippingOtherVM = new ShippingOther(dsRegistry);
					$('#ShippingOtherMediaBtn').click(function () {
						$('#ShippingOtherImageFiles').click();
					});


					//console.log("orderItem.ShippingOtherDS.getStore(): " + JSON.stringify( orderItem.ShippingOtherDS.getStore() ) );
					//console.log("orderItem.alterationReportsVM.AlterationReportData: " + JSON.stringify( orderItem.alterationReportsVM.AlterationReportData ) );
					orderItem.ShippingOtherVM.addVariantsShippingOther();

				},
				events: "c"
			}
		},

		//////////////////////
		{
			"#shipping_box_list": {
				handler: function () {
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";

					ko.applyBindings(ShippingBoxListVM, document.getElementById('shipping_box_list'));


					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 1500);
				},
				events: "c"
			}
		},


		{
			'#shipping_incoming': {
				handler: function () {
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";

					ko.applyBindings(orderItem, document.getElementById('shipping_incoming'));

					localStorage.removeItem('ShippingDataDS');
					ShippingDataDS = new SyncableDatasource('ShippingDataDS', 'ShippingDataDS', dsRegistry, 'incoming_boxes_endpoint');
					orderItem.ShippingDataVM.subscribeTo('ShippingDataDS');

					var myVar = setTimeout(function () { spinner.style.display = "none"; }, 1500);

				},
				events: "c"
			}
		},

		{
			'#shipping_items': {
				handler: function () {
					ko.applyBindings(ShippingBoxesItemsListVM, document.getElementById('shipping_items'));
				},
				events: "c"
			}
		},

		{
			'#shipping_box': {
				handler: function () {
					ko.applyBindings(ShippingBoxVM, document.getElementById('shipping_box'));

					$('#BoxSendMediaBtn').click(function () {
						$('#BoxSendImageFiles').click();
					});
				},
				events: "c"
			}
		},


		{
			'#shipping_incoming_box': { // JUST FOR TESTING, ALL FUNCTIONALITY IN SHIPPING_BOX
				handler: function () {
					ko.applyBindings(orderItem, document.getElementById('shipping_incoming_box'));
				},
				events: "c"
			}
		},

		{
			'#shipping_incomingBoxes': {
				handler: function () {
					shippingIncomingBoxes = new ShippingIncomingBoxes();
					ko.applyBindings(shippingIncomingBoxes, document.getElementById('shipping_incomingBoxes'));
				},
				events: "c"
			}
		},

		{
			'#shipping_missingItems': {
				handler: function () {
					shippingMissingItemsVM = new ShippingMissingItemsVM();
					ko.applyBindings(shippingMissingItemsVM, document.getElementById('shipping_missingItems'));
				},
				events: "c"
			}
		},

		{
			'#shipping_incomingBoxHistory': {
				handler: function () {
					shipping_incomingBoxHistoryVM = new Shipping_incomingBoxHistoryVM();
					ko.applyBindings(shipping_incomingBoxHistoryVM, document.getElementById('shipping_incomingBoxHistory'));
				},
				events: "c"
			}
		},

		{
			'#shipping_outgoingBoxHistory': {
				handler: function () {
					shipping_outgoingBoxHistoryVM = new Shipping_outgoingBoxHistoryVM();
					ko.applyBindings(shipping_outgoingBoxHistoryVM, document.getElementById('shipping_outgoingBoxHistory'));
				},
				events: "c"
			}
		},

		{
			'#shipping_tracking': {
				handler: function () {


					shipping_trackingVM = new Shipping_trackingVM();
					ko.applyBindings(shipping_trackingVM, document.getElementById('shipping_tracking'));
				},
				events: "c"
			}
		},

		{
			'#shipping_outgoingBoxes': {

				handler: function () {

					console.log("router shipping_outgoingBoxes");

					shipping_outgoingBoxesVM = new Shipping_outgoingBoxesVM();
					ko.applyBindings(shipping_outgoingBoxesVM, document.getElementById('shipping_outgoingBoxes'));
				},
				events: "c"
			}
		},

		////////////////////////////////////////////////////////////////////////////////////
		//						ERROR REPORT
		////////////////////////////////////////////////////////////////////////////////////


		{
			"#error_categories": {
				handler: function () {

					ErrorCategoriesListVM.loadInfoCategories();

				},
				events: "bc"
			}

		},

		{
			"#error_categories": {
				handler: function () {

					ko.applyBindings(ErrorCategoriesListVM);
				},
				events: "s"
			}

		},

		{
			"#errorList": {
				handler: function () {

					//errorReportListVM.loadErrorsList();

				},
				events: "bc"
			}

		},

		{
			"#errorList": {
				handler: function () {

					ko.applyBindings(errorReportListVM);
				},
				events: "s"
			}

		},


		{
			"#errorSlideShow": {
				handler: function () {

					ko.applyBindings(ErrorSlideShow);
				},
				events: "s"
			}

		},



		{
			"#errorReport": {
				handler: function () {
					orderItem.MediaUploadVM.setMediaCallback(errorReportVM.mediaCallback);
					ko.applyBindings(errorReportVM);
				},
				events: "s"
			}

		},



		{
			"#errorReply": {
				handler: function () {
					ko.applyBindings(errorReportVM);
				},
				events: "s"
			}

		},

		{
			"#errorAdd": {
				handler: function () {
					orderItem.MediaUploadVM.setMediaCallback(errorReportVM.mediaCallback);
					ko.applyBindings(errorReportVM);
				},
				events: "s"
			}

		},

		{
			"#mediaTagController": {
				handler: function () {
					ko.applyBindings(orderItem.MediaTagManagerVM);
				},
				events: "s"
			}

		},

		{
			"#errorEdit": {
				handler: function () {
					errorReportVM.prepareToUpdate();
				},
				events: "bc"
			}

		},

		{
			"#errorEdit": {
				handler: function () {
					ko.applyBindings(errorReportVM);
				},
				events: "s"
			}

		},

		////////////////////////////////////////////////////////////////////////////////////
		//						Media Sync
		////////////////////////////////////////////////////////////////////////////////////

		{
			"#mediaUploadCenter": {
				handler: function () {
					if (orderItem.SyncFilesVM.isWorking()) {
						alert("the system is working on the background!");
					} else if (orderItem.SyncFilesVM.getNewInfo()) {
						orderItem.SyncFilesVM.getInfo(orderItem.SyncFilesVM.overwrite);
					}
				},
				events: "bc"
			}

		},

		{
			"#mediaUploadCenter": {
				handler: function () {
					ko.applyBindings(orderItem.SyncFilesVM);
				},
				events: "s"
			}

		},

		{
			"#mediaManager": {
				handler: function () {
					ko.applyBindings(orderItem.MediaManagerVM);
				},
				events: "s"
			}

		},

		{
			"#mediaUploadProgress": {
				handler: function () {
					ko.applyBindings(orderItem.SyncFilesVM);
				},
				events: "c"
			}

		},
		{
			"#mediaUploadProgress": {
				handler: function () {
					orderItem.SyncFilesVM.uploadMedias();
				},
				events: "s"
			}

		},

		/////// END MEDIA SYNC
		// Are we using those files ?
		{
			'#orderProcess': {
				handler: function () {

					// reload VM
					console.log("reloading OrderProcessVM");
					require.undef('viewmodel/OrderProcessVM');
					require(['viewmodel/OrderProcessVM']);

					orderProcessVM = new OrderProcessVM();

					// seccond parameter => for the back button to work properly
					ko.applyBindings(orderProcessVM, document.getElementById('orderProcess'));

				},
				events: "s"
			}
		},


		{
			'#preOrders': {
				handler: function () {

					console.log("Pre orders router");

					// only create a new order once
					try {
						if (!preOrderDefined){
							preOrders = new PreOrdersVM();
							preOrderDefined = true;

						}

					} catch (ReferenceError) {
						preOrders = new PreOrdersVM();
						preOrderDefined = true;
					}


					// seccond parameter => for the back button to work properly
					ko.applyBindings(preOrders, document.getElementById('preOrders'));
				},
				events: "s"
			}
		},



		{
			'#preOrders': {
				handler: function () {

					// Reload models
					require.undef('viewmodel/PreOrdersVM');
					require(['viewmodel/PreOrdersVM']);
				},
				events: "rm"
			}
		},



		{
			'#orderProcessType': {
				handler: function () {

					console.log("orderProcessType router");

					/**
					 * Gets the count of pending group orders
					*/
					$.ajax({
					    type: 'POST',
					    timeout: 60000, // sets timeout to 60 seconds
					    url: BUrl + 'orders_pos/get_pending_group_orders',
					    dataType: 'json',
					    data: {
					        "user": authCtrl.userInfo
					    },
					    success: function (dataS) {

					    	const numberPendingGroupOrders = dataS.orders.length;

					    	$("#pending-group-orders-notf").html(numberPendingGroupOrders);

					    },
					    error: function (error) {
					        console.log(JSON.stringify(error));
					        customAlert("TIME OUT - there is a network issue. Please try again later");
					    },
					    async: true
					});


				},
				events: "s"
			}
		},


		{
			'#pendingGroupOrders': {
				handler: function () {

					$('#loading_jp').show( () => {

						console.log("pendingGroupOrders router");
						pendingGroupOrders = new PendingGroupOrdersVM();

						// seccond parameter => for the back button to work properly
						ko.applyBindings(pendingGroupOrders, document.getElementById('pendingGroupOrders'));

						document.getElementById('loading_jp').style.display = "none";
					});

				},
				events: "s"
			}
		},

		{
			'#pendingGroupOrders': {
				handler: function () {

					// Reload models
					require.undef('viewmodel/PendingGroupOrdersVM');
					require(['viewmodel/PendingGroupOrdersVM']);
				},
				events: "rm"
			}
		},


		{
			'#prePendingCustomersOrders': {
				handler: function () {

					console.log("prePendingCustomersOrders router");
					prePendingCustomersOrders = new PrePendingCustomersOrdersVM();

					// seccond parameter => for the back button to work properly
					ko.applyBindings(prePendingCustomersOrders, document.getElementById('prePendingCustomersOrders'));
				},
				events: "s"
			}
		},

		{
			'#prePendingCustomersOrders': {
				handler: function () {

					// Reload models
					require.undef('viewmodel/PrePendingCustomersOrdersVM');
					require(['viewmodel/PrePendingCustomersOrdersVM']);
				},
				events: "rm"
			}
		},


		{
			'#orders': {

				handler: function () {

					let customersIds, leaderId, customersData, leaderPic, groupName, isReorder, reorderData, groupId;

					if( localStorage.getItem('groupOrdersData') !== null )
					{
						const groupOrdersData = JSON.parse(localStorage.getItem('groupOrdersData'));

						console.log("groupOrdersData", groupOrdersData);

						customersIds = groupOrdersData.customers_ids;
						leaderId = groupOrdersData.leader_id;
						customersData = groupOrdersData.customers_data;
						leaderPic = groupOrdersData.leaderPic;
						groupName = groupOrdersData.group_name;
						isReorder = groupOrdersData.is_reorder;
						reorderData = groupOrdersData.reorder_data;
						groupId = groupOrdersData.groupId;

						console.log('customersData', customersData);

						// clear the localStorage
						// localStorage.removeItem('groupOrdersData');
					}
					else
					{
						alert('Customers are not set :((');

						// customersIds = ['1274','2062','2212'];
						// leaderId = '1274';
					}


					document.getElementById('loading_jp').style.display = "block";

					//Create its own viewmodel --- for now using the same
					// orders = new OrdersVM(customersIds, leaderId, customersData, leaderPic, groupName, isReorder, reorderData);

					// only create a new order once
					try {
						if (!ordersDefined){
							orders =  new OrdersVM(customersIds, leaderId, customersData, leaderPic, groupName, isReorder, reorderData, groupId);
							ordersDefined = true;
						}
						else {
							orders.updateInstance(customersIds, leaderId, customersData, leaderPic, groupName, isReorder, reorderData, groupId)
						}
					} catch (ReferenceError) {
						orders =  new OrdersVM(customersIds, leaderId, customersData, leaderPic, groupName, isReorder, reorderData, groupId);
						ordersDefined = true;
					}

					// necessary to give time for the tablets to load everything
					sleep(1500).then(() => {
					   	ko.applyBindings(orders, document.getElementById('orders'));

						document.getElementById('loading_jp').style.display = "none";
					});

					// seccond parameter => for the back button to work properly
				},
				events: "s"
			}
		},

		{
			'#orders': {
				handler: function () {

					// Reload the files on page exit...

					console.log('Reloading viewmodels....');
					require.undef('viewmodel/OrdersVM');
					require(['viewmodel/OrdersVM']);

					require.undef('viewmodel/GarmentsStepVM');
					require(['viewmodel/GarmentsStepVM']);

					require.undef('viewmodel/MeasurementsStepVM');
					require(['viewmodel/MeasurementsStepVM']);

					require.undef('viewmodel/BodyShapesStepVM');
					require(['viewmodel/BodyShapesStepVM']);

					require.undef('viewmodel/UploadImagesStepVM');
					require(['viewmodel/UploadImagesStepVM']);

					require.undef('viewmodel/UploadVideosStepVM');
					require(['viewmodel/UploadVideosStepVM']);

					require.undef('viewmodel/GarmentsDesignStepVM');
					require(['viewmodel/GarmentsDesignStepVM']);

					require.undef('viewmodel/Image3DVM');
					require(['viewmodel/Image3DVM']);

					require.undef('model/image3D/Image3D');
					require(['model/image3D/Image3D']);

					require.undef('model/image3D/Image3D_v1');
					require(['model/image3D/Image3D_v1']);

					require.undef('model/image3D/Image3D_v2');
					require(['model/image3D/Image3D_v2']);

					require.undef('viewmodel/PaymentsStepVM');
					require(['viewmodel/PaymentsStepVM'])

					require.undef('viewmodel/OrderStepsRequirementsVM');
					require(['viewmodel/OrderStepsRequirementsVM'])

					// this modules, are loaded when required. So don't bother to load again (y)
					require.undef('viewmodel/RequiredInfoVM');

					require.undef('model/image3D/Image3D_v2');
					require.undef('model/image3D/Image3D_v1');
					require.undef('model/image3D/Image3D');



				},
				events: "rm"
			}
		},



		{
			'#unlockCustomersFittings': {
				handler: function () {

					// Reload the files on page exit...
					console.log("unlockCustomersFittings router");
					unlockCustomersFittings = new UnlockCustomersFittingsVM();

					// seccond parameter => for the back button to work properly
					ko.applyBindings(unlockCustomersFittings, document.getElementById('unlockCustomersFittings'));
				},
				events: "s"
			}
		},

		{
			'#unlockCustomersFittings': {
				handler: function () {


					console.log('Reloading viewmodels....');
					require.undef('viewmodel/UnlockCustomersFittingsVM');
					require(['viewmodel/UnlockCustomersFittingsVM']);


				},
				events: "rm"
			}
		},





		{
			'#uniformCompany': {
				handler: function () {

					// Reload the files on page exit...
					console.log("uniformCompany router");
					uniformCompany = new UniformCompanyVM();

					// seccond parameter => for the back button to work properly
					ko.applyBindings(uniformCompany, document.getElementById('uniformCompany'));
				},
				events: "s"
			}
		},

		{
			'#uniformCompany': {
				handler: function () {


					console.log('Reloading viewmodels....');
					require.undef('viewmodel/UniformCompanyVM');
					require(['viewmodel/UniformCompanyVM']);


				},
				events: "rm"
			}
		},

		{
			'#uniformTemplate': {
				handler: function () {

					$('#loading_jp').show( () => {

						console.log('Starting uniform template process');

						let company_id = -1;

						if( localStorage.getItem('company_idUniformTemplate') && parseInt(localStorage.getItem('company_idUniformTemplate')) )
						{
							company_id = localStorage.getItem('company_idUniformTemplate');
						}
						else // Just for development -- remove this afterwards
						{
							company_id = 1;
							uniformTemplate = new UniformTemplateVM(company_id);
						}

						// Instanciate the viewmodel
						uniformTemplate = new UniformTemplateVM(company_id);

						// unset the localstorage for the company id
						localStorage.removeItem('company_idUniformTemplate');

						sleep(1500).then(() => {
						   	ko.applyBindings(uniformTemplate, document.getElementById('uniformTemplate'));

						   	sleep(1500).then(() => { /* This will make the UI nicer, avoiding everything to break and coming back */
						   		document.getElementById('loading_jp').style.display = "none";
						   	});

						});


					}); // end loader show

				},
				events: "s"
			}
		},


		{
			'#uniformTemplate': {
				handler: function () {

					// Reload the files on page exit...

					console.log('Reloading viewmodels....');

					require.undef('viewmodel/UniformTemplateVM');
					require(['viewmodel/UniformTemplateVM']);

					require.undef('viewmodel/UniformGarmentsStepVM');
					require(['viewmodel/UniformGarmentsStepVM']);

					require.undef('viewmodel/UniformGarmentsDesignStepVM');
					require(['viewmodel/UniformGarmentsDesignStepVM']);

					require.undef('viewmodel/UniformPortfolioVm');
					require(['viewmodel/UniformPortfolioVm']);

					require.undef('viewmodel/UniformFinalizeStepVM');
					require(['viewmodel/UniformFinalizeStepVM']);

				},
				events: "rm"
			}
		},




		{
			'#uniformOrder': {
				handler: function () {

					$('#loading_jp').show( () => {

						console.log('Starting uniform order process');

						let company_id = -1;

						if( localStorage.getItem('company_iduniformOrder') && parseInt(localStorage.getItem('company_iduniformOrder')) )
						{
							company_id = localStorage.getItem('company_iduniformOrder');
						}
						else // Just for development -- remove this afterwards
						{
							alert('company_iduniformOrder NOT DEFINED exiting...');
							return;
						}

						// Instanciate the viewmodel
						uniformOrder = new UniformOrderVM(company_id);


						// unset the localstorage for the company id
						localStorage.removeItem('company_iduniformOrder');

						sleep(1500).then(() => {
						   	ko.applyBindings(uniformOrder, document.getElementById('uniformOrder'));


							document.getElementById('loading_jp').style.display = "none";


						});

					});

				},
				events: "s"
			}
		},


		{
			'#uniformOrder': {
				handler: function () {

					// Reload the files on page exit...

					console.log('Reloading viewmodels....');

					require.undef('viewmodel/uniformOrderVM');
					require(['viewmodel/uniformOrderVM']);

					require.undef('viewmodel/UniformOrderGarmentsStepVM');
					require(['viewmodel/UniformOrderGarmentsStepVM']);

					require.undef('viewmodel/UniformOrderCustomerDetailsVM');
					require(['viewmodel/UniformOrderCustomerDetailsVM']);

					require.undef('viewmodel/UniformOrderContactPersonVM');
					require(['viewmodel/UniformOrderContactPersonVM']);

					require.undef('viewmodel/UniformOrderPaymentsVM');
					require(['viewmodel/UniformOrderPaymentsVM']);

					require.undef('viewmodel/UniformStatisticsVM');
					require(['viewmodel/UniformStatisticsVM']);

					require.undef('viewmodel/UniformOrderBodyShapesStepVM');
					require(['viewmodel/UniformOrderBodyShapesStepVM']);

					require.undef('viewmodel/UniformOrderUploadImagesStepVM');
					require(['viewmodel/UniformOrderUploadImagesStepVM']);

					require.undef('viewmodel/UniformOrderUploadVideosStepVM');
					require(['viewmodel/UniformOrderUploadVideosStepVM']);

				},
				events: "rm"
			}
		},


		{
			'#uniformMainMenu': {
				handler: function () {

					console.log('Starting uniform main menu');


				},
				events: "s"
			}
		},


		{
			'#uniformCompanySearch': {
				handler: function () {

					console.log('Starting uniformCompanySearch');

					uniformCompanySearch = new UniformCompanySearchVM();

					ko.applyBindings(uniformCompanySearch, document.getElementById('uniformCompanySearch'));
				},
				events: "s"
			}
		},


		{
			'#uniformCompanySearch': {
				handler: function () {

					require.undef('viewmodel/UniformCompanySearchVM');
					require(['viewmodel/UniformCompanySearchVM']);

				},
				events: "rm"
			}
		},


		{
			'#uniformCompanyStatistics': {
				handler: function () {

					console.log('Starting uniformCompanyStatistics');
					ko.applyBindings(uniformCompanySearch.statistics, document.getElementById('uniformCompanyStatistics'));

				},
				events: "s"
			}
		},

		{
			'#tasks': {
				handler: function () {

					tasksVM = new TasksVM();
					ko.applyBindings(tasksVM, document.getElementById('tasks'));

				},
				events: "s"
			}
		},
		{
			'#tasks': {
				handler: function () {
					tasksVM = undefined;
					require.undef("viewmodel/tasksVM");
					require(["viewmodel/tasksVM"]);

				},
				events: "rm"
			}
		},



		//All page route, create/destroy iscroll, menu handling
		{
			"(?:[?/](.*))?": {
				handler: function (type, matchObj, ui, page) {

					//		var indexcalendar = document.getElementById('indexcalendar');
					//		//indexcalendar.style.display = "block";
					//		indexcalendar.style.display = "none";

					//$.backstretch('gfx/template/body_bg.jpg');
					if (document.getElementById('scrollWrapper').length != 0) {
						iSGarments = new iScroll('scrollWrapper', {});
						iSGarments.options.onBeforeScrollStart = function (e) {

							if ($('.cloneDialog').length != 0) {
								$('.cloneDialog').remove();
								//$('.cloneDialog').fadeOut('fast', function(){$('.cloneDialog').remove(); });
							}

							var target = e.target;

							while (target.nodeType != 1) target = target.parentNode;
							if (target.tagName != 'SELECT' && target.tagName != 'INPUT' && target.tagName != 'TEXTAREA') {
								e.preventDefault();
							}
						}
					}
				},
				events: "s"
			}
		},

		{
			"(?:[?/](.*))?": {
				handler: function () {
					try {
						//	document.getElementById("canvas_container").innerHTML = [''].join('');
						document.getElementById("canvas_container").innerHTML = ['<canvas id="canvas-0"></canvas><canvas id="canvas-1"></canvas><canvas id="canvas-2"></canvas><canvas id="canvas-squash"></canvas>'].join('');
					} catch (e) { ; }
				},
				events: "h"
			}
		}
	];

	searchRoute = function searchRoute(page, event) {
		console.log(page + ' + ' + event);
		for (var route in router) {
			if (Object.keys(router[route])[0] == "#" + page) {
				if (router[route][Object.keys(router[route])[0]].events == event) {
					router[route][Object.keys(router[route])[0]].handler();
				}
			}
		}
	}

	isSpecialCase = function isSpecialCase(page, currentPage) {
		isSpecial = false;
		var x = 0;
		for (x in specialCases) {
			if (specialCases[x].page == page) {
				if (currentPage && specialCases[x].previousPage == currentPage) {
					isSpecial = true;
				} else if (!currentPage) {
					isSpecial = true;
				}
			}
		}
	}

	addSpecialCase = function addSpecialCase(destenyPage, previousPage) {
		specialCases.push({ page: destenyPage, previousPage: previousPage });
	}

	runSpecialCase = function runSpecialCase(page) {
		currentPage = page;
		searchRoute(page, 'bc');
		searchRoute(page, 'c');
		searchRoute(page, 'i');
		searchRoute(page, 'bs');
		searchRoute(page, 's');

	}

	removePage = function removePage(page) {
		try {
			var html = $('#' + page).remove();
			console.log('removed ' + page);
		} catch (e) {
			console.log('not removed');
		}
	}

	isSpecial = false;
	specialCases = [];

	addSpecialCase('customerGarmentsFittingsList');
	addSpecialCase('AlterationsList');
	addSpecialCase('customerGarmentsFittingsList');

});