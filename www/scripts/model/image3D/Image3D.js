define(['jquery', 'knockout', 'base'], function ($, ko) {
    
    /**
     * This class holds common code between the different types of 3D. LIke, 3D to uniform and 3D to order.
     */
    Image3D = class Image3D {


        constructor(selectedGarment){
            this.selectedGarment = selectedGarment;
            var self = this;
    
            // shows the front or back side of the garment
            this.side = ko.observable(this.selectedGarment.garment_id == '6' ? "jacket_front" : "front");

            // Shirt
            // ███████╗██╗  ██╗██╗██████╗ ████████╗
            // ██╔════╝██║  ██║██║██╔══██╗╚══██╔══╝
            // ███████╗███████║██║██████╔╝   ██║   
            // ╚════██║██╔══██║██║██╔══██╗   ██║   
            // ███████║██║  ██║██║██║  ██║   ██║   
            // ╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝   ╚═╝   
            //                                     
            this.shirtMonogram = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '39') {
                        for (var option of category.options) {
                            if (option.id == '118' && option.selectedValue()) {
                                return option.selectedValue();
                            }
                        }
                    }
                }
                return "";
            });

            this.shirtMonogramPositionID = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '39') {
                        for (var option of category.options) {
                            if (option.id == '119' && option.selectedValue()) {
                                // if sleeve is "short" or "rolled", attention to "cuff position"
                                // get sleeve category
                                let sleeveCategory = self.selectedGarment.categories.filter(el => el.id == "31")[0];
                                if (!sleeveCategory) return ;

                                // get  style option
                                let sleeve_option = sleeveCategory.options.filter(el => el.id == "89")[0];
                                if (!sleeve_option) return ;


                                // if is "rolled"
                                if(sleeve_option.selectedValue() && sleeve_option.selectedValue().id == "1315"){
                                    return (option.selectedValue().id == "1483") ? "" : option.selectedValue().id;
                                    
                                }
                                // if is "short"
                                else if(sleeve_option.selectedValue() && sleeve_option.selectedValue().id == "1316"){
                                    return (option.selectedValue().id == "1483") ? "shirt_short_cuff" : option.selectedValue().id;

                                }

                                // other styles
                                else {
                                    return option.selectedValue().id;
                                }

                            }
                        }
                    }
                }
                return "";
            });

            // Jacket
            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝
            //      ██║███████║██║     █████╔╝ █████╗     ██║   
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║   
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   
            //                                                  
            this.jacketSleeveKissing = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '30' && option.selectedValue()) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            });

            this.jacketSleeveWorking = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '31' && option.selectedValue()) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            });


            this.selectedJacketNumberOfButtonsSleeve = function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '29' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "225": //1
                                                return 1;
                                            case "226": //2
                                                return 2;
                                            case "227": //3
                                                return 3;
                                            case "228": //4
                                                return 4;
                                            case "229": //5
                                                return 5;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return 0;
            };

            this.JacketTuxedoButtons = ko.computed(function(){

                // check if tuxedo
                const lapelDetailsCategory = self.selectedGarment.categories.filter(el => el.id=="11")[0];
                if (!lapelDetailsCategory) return;
                
                // Option
                const tuxedoOption = lapelDetailsCategory.options.filter(el=>el.id=="23")[0];
                if (!tuxedoOption) return;


                return tuxedoOption.selectedValue() ? true : false;

            });

            this.jacketIsKissing = ko.computed(function () {
                // check if kissing
                // sleeve category
                const sleeveCategory = self.selectedGarment.categories.filter(el => el.id=="7")[0];
                if (!sleeveCategory) return;
                
                // kissingOption
                const kissingOption = sleeveCategory.options.filter(el=>el.id=="30")[0];
                if (!kissingOption) return;


                return kissingOption.selectedValue() ? true : false;
            });

            this.selectedJacketMonogramLine1 = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '46') {
                        for (var option of category.options) {
                            if (option.id == '128' && option.selectedValue()) {
                                return option.selectedValue()
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedJacketMonogramLine2 = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '46') {
                        for (var option of category.options) {
                            if (option.id == '129' && option.selectedValue()) {
                                return option.selectedValue()
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedJacketMonogramLine3 = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '46') {
                        for (var option of category.options) {
                            if (option.id == '130' && option.selectedValue()) {
                                return option.selectedValue()
                            }
                        }
                    }
                }
                return "";
            });

            /**
             * check if tuxedo is on
             * @returns boolean
             */
            this.jacketTuxedoIsOn = ko.computed(function(){
                
                var tuxedoCategory = self.selectedGarment.categories.filter(cat=>cat.id=="1")[0];
                if(!tuxedoCategory){ return false }

                let tuxedoOption = tuxedoCategory.options.filter(opt => opt.id=="3")[0];

                if (!tuxedoOption || (tuxedoOption && !tuxedoOption.selectedValue())){
                    return false;
                }

                return true;
            });


            // suit
            // ███████╗██╗   ██╗██╗████████╗
            // ██╔════╝██║   ██║██║╚══██╔══╝
            // ███████╗██║   ██║██║   ██║   
            // ╚════██║██║   ██║██║   ██║   
            // ███████║╚██████╔╝██║   ██║   
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   
            //                              
            this.suitJacketIsKissing = ko.computed(function () {
                // check if kissing
                // sleeve category
                const sleeveCategory = self.selectedGarment.categories.filter(el => el.id=="54")[0];
                if (!sleeveCategory) return;
                
                // kissingOption
                const kissingOption = sleeveCategory.options.filter(el=>el.id=="170")[0];
                if (!kissingOption) return;

                
                return kissingOption.selectedValue() ? true : false;
            });

            // TODO:
            this.suitJacketSleeveWorking = ko.computed(function() {return false});

            this.selectedSuitJacketNumberOfButtonsSleeve = function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '169' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "2712": //1
                                                return 1;
                                            case "2713": //2
                                                return 2;
                                            case "2714": //3
                                                return 3;
                                            case "2715": //4
                                                return 4;
                                            case "2716": //5
                                                return 5;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return 0;
            };

            this.suitJacketTuxedoButtons = ko.computed(function(){

                // check if tuxedo
                const lapelDetailsCategory = self.selectedGarment.categories.filter(el => el.id=="53")[0];
                if (!lapelDetailsCategory) return;
                
                // Option
                const tuxedoOption = lapelDetailsCategory.options.filter(el=>el.id=="197")[0];
                if (!tuxedoOption) return;


                return tuxedoOption.selectedValue() ? true : false;

            });

            this.selectedSuitJacketMonogramLine1 = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '63') {
                        for (var option of category.options) {
                            if (option.id == '207' && option.selectedValue()) {
                                return option.selectedValue()
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketMonogramLine2 = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '63') {
                        for (var option of category.options) {
                            if (option.id == '208' && option.selectedValue()) {
                                return option.selectedValue()
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketMonogramLine3 = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '63') {
                        for (var option of category.options) {
                            if (option.id == '209' && option.selectedValue()) {
                                return option.selectedValue()
                            }
                        }
                    }
                }
                return "";
            });

            this.getSelectedSuitPantStyleId = function(){
                for (var category of self.selectedGarment.categories) {
                    // 14 is id for pant style
                    if (category.id == '58') {
                        for (var option of category.options) {
                            // 57 is id for pant syle
                            if (option.id == '211' && option.values) {
                                return option.selectedValue() ? option.selectedValue().id : false;
                            }
                        }
                    }
                }

                return false;
            }

            /**
             * check if tuxedo is on
             * @returns boolean
             */
            this.suitJacketTuxedoIsOn = ko.computed(function(){
                
                var tuxedoCategory = self.selectedGarment.categories.filter(cat=>cat.id=="49")[0];
                if(!tuxedoCategory){ return false }

                let tuxedoOption = tuxedoCategory.options.filter(opt => opt.id=="150")[0];

                if (!tuxedoOption || (tuxedoOption && !tuxedoOption.selectedValue())){
                    return false;
                }

                return true;
            });

            // vest

            // pants
            // ██████╗  █████╗ ███╗   ██╗████████╗███████╗
            // ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
            // ██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
            // ██╔═══╝ ██╔══██║██║╚██╗██║   ██║   ╚════██║
            // ██║     ██║  ██║██║ ╚████║   ██║   ███████║
            // ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
            
            this.getSelectedPantStyleId = function(){
                for (var category of self.selectedGarment.categories) {
                    // 14 is id for pant style
                    if (category.id == '14') {
                        for (var option of category.options) {
                            // 57 is id for pant syle
                            if (option.id == '57' && option.values) {
                                return option.selectedValue() ? option.selectedValue().id : false;
                            }
                        }
                    }
                }

                return false;
            }

        }

        /**
         * Show/close a pop up for image 3D by changing the class atribute of the div
         * 
         * @deprecated
         */
        togglePopUp (name = null) {
            // do nothing
            return;

            console.log("togglePopUp");

            if (name) {

                for (const popUp of document.getElementsByName(name)) {
                    for (const element of popUp.classList) {
                        if (element == 'bigger') {
                            popUp.classList.remove('bigger'); // close pop up
                            return; // end function
                        }
                    }
        
                    popUp.classList.add('bigger'); // open pop up
                } 

            }

            else{
                
                var popUp = document.getElementById('image-3d');
    
                for (const element of popUp.classList) {
                    if (element == 'bigger') {
                        popUp.classList.remove('bigger'); // close pop up
                        return; // end function
                    }
                }
    
                popUp.classList.add('bigger'); // open pop up
            }
        }

        /**
         * Changes side from SIDE/TO
         */
        toggle3DSide() {

            console.log('toggle3DSide....');

            const current_side = this.side();
            let new_side = "";

            /**
             * the suit is using a different namming convention 
             */
            if(this.selectedGarment.garment_id == '6')
            {
                /**
                 * For suits => we need to diferentiate the PANTS 3D from the JACKET 3D
                 */
                switch(current_side)
                {
                    case 'jacket_front':
                        new_side = 'jacket_back';
                        break;
                    case 'jacket_back':
                        new_side = 'jacket_front';
                        break;

                    case 'pant_front':
                        new_side = 'pant_back';
                        break;
                    case 'pant_back':
                        new_side = 'pant_front';
                        break;

                    // If not found => use JACKET_FRONT as default
                    default:
                        new_side = 'jacket_front';
                        break;
                }
            }
            else
            {
                /**
                 * for all the other garments => simply use front/back
                 */
                if(current_side === 'front')
                {
                    new_side = 'back';
                }
                else
                {
                    new_side = 'front';
                }
            }

            console.log('new_side', new_side);
            this.side(new_side);
        }
       
    }
});