define(['jquery', 'knockout', 'base', 'model/image3D/Image3D'], function ($, ko) {

    /**
     * This 3D has reference to old images
     */
    Image3D_v1 = class Image3D_v1 extends Image3D {
        constructor(selectedGarment) {
            super(selectedGarment);

            var self = this;

            // Pants

            // ██████╗  █████╗ ███╗   ██╗████████╗███████╗
            // ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
            // ██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
            // ██╔═══╝ ██╔══██║██║╚██╗██║   ██║   ╚════██║
            // ██║     ██║  ██║██║ ╚████║   ██║   ███████║
            // ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
            //
            this.pantsDefaultImage = ko.observableArray([
                "gfx/order/image_3d/pant/pant_bg.png"
            ]);

            this.pantsDefaultImageBack = ko.observableArray([
                "gfx/order/image_3d/pant/back_bg.png"
            ]);

            this.selectedPantStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    // 14 is id for pant style
                    if (category.id == '14') {
                        for (var option of category.options) {
                            // 57 is id for pant syle
                            if (option.id == '57' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1254":
                                                return "gfx/order/image_3d/pant/fit/boot_fl_pan_preview.png"

                                            case "1255":
                                                return "gfx/order/image_3d/pant/fit/narr_po_pant_preview.png"

                                            case "1256":
                                                return "gfx/order/image_3d/pant/fit/nom_str_pan_preview.png"

                                            default:
                                                break;
                                        }
                                    }
                                }

                            }
                        }

                    }
                }


                return "gfx/order/image_3d/pant/fit/boot_fl_pan_preview.png";
            });

            this.selectedPantPleatStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '15') {
                        for (var option of category.options) {
                            if (option.id == '59' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1257":
                                                return "gfx/order/image_3d/pant/pleats/dobb_ple_pan_preview.png"

                                            case "1258":
                                                return "gfx/order/image_3d/pant/none.png"

                                            case "1259":
                                                return "gfx/order/image_3d/pant/pleats/singgle_pl_pant_preview.png"

                                            case "1260":
                                                return "gfx/order/image_3d/pant/pleats/trip_ple_pan_preview.png"

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "gfx/order/image_3d/pant/none.png"
            });

            this.selectedPantFrontPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '16') {
                        for (var option of category.options) {
                            if (option.id == '61' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "1261":
                                                return "gfx/order/image_3d/pant/pockets/jerans_poc_preview.png";

                                            case "1262":
                                                return "gfx/order/image_3d/pant/pockets/jerans_poc_preview.png";

                                            case "1263":
                                                return "gfx/order/image_3d/pant/pockets/mod_cir_pant_p_preview.png";

                                            case "1264":
                                                return "gfx/order/image_3d/pant/none.png";

                                            case "1265":
                                                return "gfx/order/image_3d/pant/pockets/slanted_pan_pocck_preview.png";

                                            case "1266":
                                                return "gfx/order/image_3d/pant/pockets/stan_welt_pant_preview.png";

                                            case "1267":
                                                return "gfx/order/image_3d/pant/pockets/welt_stri_pan_preview.png";

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "gfx/order/image_3d/pant/none.png"
            });

            this.selectedPantBackPockets = ko.computed(function () {
                var style = 'gfx/order/image_3d/pant/none.png';


                // get style of pocket
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '17') {
                        for (var option of category.options) {
                            if (option.id == '63' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "1268":
                                                style = "gfx/order/image_3d/pant/back/double_pocket_preview.png";
                                                break;
                                            case "1269":
                                                style = "gfx/order/image_3d/pant/back/double_button_preview.png";
                                                break;
                                            case "1270":
                                                style = "gfx/order/image_3d/pant/back/modern_preview.png";
                                                break;
                                            case "1271":
                                                style = "gfx/order/image_3d/pant/back/modern_button_preview.png";
                                                break;
                                            case "1272":
                                                style = "gfx/order/image_3d/pant/back/no_pocket_preview.png";
                                                break;
                                            case "1273":
                                                style = "gfx/order/image_3d/pant/back/single_pocket_preview.png";
                                                break;
                                            case "1274":
                                                style = "gfx/order/image_3d/pant/back/single_button_preview.png";
                                                break;
                                            case "1275":
                                                style = "gfx/order/image_3d/pant/back/square_no_flap_preview.png";
                                                break;
                                            case "1276":
                                                style = "gfx/order/image_3d/pant/back/square_flap_preview.png";
                                                break;

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }
                    }
                }

                // when "no pocket" is selected, no need to get "positions"
                if (style == "gfx/order/image_3d/pant/none.png" || style == "gfx/order/image_3d/pant/back/no_pocket_preview.png"){
                    return style;
                }

                // now get position of pocket
                const backPocketCategory = self.selectedGarment.categories.filter(el => el.id == '17')[0];
                const positionOption = backPocketCategory ? backPocketCategory.options.filter(el => el.id == '65')[0] : undefined;
                if (positionOption && positionOption.selectedValue()) {

                    switch (positionOption.selectedValue().id) {

                        case "1277":
                            // nothing to do
                            return style;
                        case "1278":
                            style = style.replace(".png", "-left.png")
                            return style;
                        case "1279":
                            style = style.replace(".png", "-right.png")
                            return style;

                        default:
                            break;

                    }
                }

                return style;
            });

            this.selectedPantBeltAndCuffs = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '18') {
                        for (var option of category.options) {
                            if (option.id == '66' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "1280":
                                                return "gfx/order/image_3d/pant/belt_loop/waist_lop_preview.png";
                                            case "1281":
                                                return "gfx/order/image_3d/pant/belt_loop/but_adj_belt_oop_preview.png";
                                            case "1282":
                                                return "gfx/order/image_3d/pant/belt_loop/cross_preview.png";
                                            case "1283":
                                                return "gfx/order/image_3d/pant/belt_loop/mode_lo_pannm_preview.png";
                                            case "1284":
                                                return "gfx/order/image_3d/pant/belt_loop/double_angle_preview.png";
                                            case "1285":
                                                return "gfx/order/image_3d/pant/belt_loop/dobblooe_pant_loup_preview.png";
                                            case "1286":
                                                return "gfx/order/image_3d/pant/none.png";
                                            case "1287":
                                                return "gfx/order/image_3d/pant/belt_loop/sinnggle_pant_llop_preview.png";

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "gfx/order/image_3d/pant/none.png"
            });

            this.selectedPantBeltAndCuffsBack = ko.computed(function () {
                return "";
            });

            this.selectedPantCuffs = ko.computed(function(){
                // get belt & cuff category
                const cuffCategory = self.selectedGarment.categories.filter(cat => cat.id == "18")[0];
                if (!cuffCategory) return "";

                // get cuff option
                const cuffOption = cuffCategory.options.filter(el => el.id == "68")[0];
                if (!cuffOption) return "";

                if (!cuffOption.selectedValue()){
                    // no cuff
                    return "";
                }

                // get pant style
                const pantStyle = self.selectedPantStyle();

                switch (pantStyle) {
                    case "gfx/order/image_3d/pant/fit/boot_fl_pan_preview.png":
                        return "gfx/order/image_3d/pant/cuffs/cuff_boot.png";
                    case "gfx/order/image_3d/pant/fit/narr_po_pant_preview.png":
                        return "gfx/order/image_3d/pant/cuffs/cuff_narrow.png";
                    case "gfx/order/image_3d/pant/fit/nom_str_pan_preview.png":
                        return "gfx/order/image_3d/pant/cuffs/cuff_norm.png";
                    default:
                        return "";
                }

            });

            this.selectedPantCuffsBack = ko.computed(function(){
                return "";
            });

            // Vest
            // ██╗   ██╗███████╗███████╗████████╗
            // ██║   ██║██╔════╝██╔════╝╚══██╔══╝
            // ██║   ██║█████╗  ███████╗   ██║
            // ╚██╗ ██╔╝██╔══╝  ╚════██║   ██║
            //  ╚████╔╝ ███████╗███████║   ██║
            //   ╚═══╝  ╚══════╝╚══════╝   ╚═╝
            //

            this.vestDefaultImagesFront = ko.observableArray([
                "gfx/order/image_3d/vest/vest_bg.png"
            ]);

            this.selectedVestLapel = ko.computed(function () {

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '22') {
                        for (var option of category.options) {
                            if (option.id == '74' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1291":
                                                return "gfx/order/image_3d/vest/lapel/shawl_lapel_preview.png";
                                            case "1292":
                                                return "gfx/order/image_3d/vest/lapel/notch_preview.png";
                                            case "1293":
                                                return "gfx/order/image_3d/vest/lapel/peak_preview.png";
                                            case "1294":
                                                return "gfx/order/image_3d/vest/lapel/shawl_lapel_preview.png";
                                            case "1295":
                                                return "gfx/order/image_3d/vest/none.png";
                                            default:
                                                break;
                                        }
                                    }
                                }

                            }
                        }

                    }
                }


                return "gfx/order/image_3d/vest/none.png";
            });

            // only for v2
            this.selectedVestUpper = ko.observable("");

            this.selectedVestBottom = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '23') {
                        for (var option of category.options) {
                            if (option.id == '76' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1296":
                                                return "gfx/order/image_3d/vest/bottom/notch_preview.png";
                                            case "1297":
                                                return "gfx/order/image_3d/vest/bottom/round_preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/vest/bottom/notch_preview.png";
            });

            this.selectedVestButton = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '24') {
                        for (var option of category.options) {
                            if (option.id == '78' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1298":
                                                return "gfx/order/image_3d/vest/buttons/six_preview.png";
                                            case "1299":
                                                return "gfx/order/image_3d/vest/buttons/five_preview.png";
                                            case "1300":
                                                return "gfx/order/image_3d/vest/buttons/four_preview.png";
                                            case "1301":
                                                return "gfx/order/image_3d/vest/buttons/six_preview.png";
                                            case "4564":
                                                return "";
                                            case "4565":
                                                return "";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/vest/buttons/four_preview.png";
            });

            this.selectedVestPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '25') {
                        for (var option of category.options) {
                            if (option.id == '80' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1302":
                                                return "gfx/order/image_3d/vest/pockets/flap_preview.png";	 // Flap
                                            case "1303":
                                                return "gfx/order/image_3d/vest/none.png";	 // No
                                            case "1304":
                                                return "gfx/order/image_3d/vest/pockets/patch_preview.png";	 // Patch
                                            case "1305":
                                                return "gfx/order/image_3d/vest/pockets/single_preview.png";	 // Single


                                            default:
                                                break;
                                        }


                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/vest/none.png";
            });

            this.selectedVestBreastPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '25') {
                        for (var option of category.options) {
                            // breast pocket
                            if (option.id == '82' && option.selectedValue()) {
                                // double breast pocket
                                if (category.options.filter(function (el) { return el.id == '225' })[0].selectedValue()) {
                                    return 'gfx/order/image_3d/vest/pockets/doublebreast.png';
                                } else {
                                    return 'gfx/order/image_3d/vest/pockets/breast.png';
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/vest/none.png";
            });

            this.selectedvestBack = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '26') {
                        for (var option of category.options) {
                            if (option.id == '83' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1309":
                                                return "gfx/order/image_3d/vest/back/belt_preview.png";
                                            case "1310":
                                                return "gfx/order/image_3d/vest/back/single_preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/vest/back/belt_preview.png";
            });

            this.selectedVestFrontLiningColor = ko.computed(function () {
                return "";
            });

            this.selectedVestLiningColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            if (option.id == '132' && option.selectedValue()) {
                                // if (option.selectedValue().image.includes("/gerlin/")) {
                                //     return option.selectedValue().image.replace("/preview/", "/sign/").replace("Gerlin", "Gerlin-");

                                // } else if (option.selectedValue().image.includes("/preview/")) {
                                //     return option.selectedValue().image.replace("/preview/", "/sign/");

                                // } else if (option.selectedValue().image.includes("gfx/order/image_3d/liningFabric/100KL")) {
                                //     // default value (TODO)
                                //     $.jGrowl("Image is not available yet for this linning");
                                //     return "gfx/order/image_3d/liningFabric/sign/01.png";
                                // }
                                return option.selectedValue().previews;
                            }
                        }
                    }
                }

                // default value
                return "gfx/order/image_3d/liningFabric/sign/01.png";
            });

            this.selectedVestPipingColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            if (option.id == '133' && option.selectedValue()) {
                                return option.selectedValue().preview;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/pipingColors/piping_default_preview.png";
            });

            this.selectedVestPocketColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            // if (option.id == '134' && option.selectedValue()) {
                            if (option.id == '133' && option.selectedValue()) {
                                return option.selectedValue().inpocket;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/inpocket/piping_default_preview.png";
            });

            // Shirt
            // ███████╗██╗  ██╗██╗██████╗ ████████╗
            // ██╔════╝██║  ██║██║██╔══██╗╚══██╔══╝
            // ███████╗███████║██║██████╔╝   ██║
            // ╚════██║██╔══██║██║██╔══██╗   ██║
            // ███████║██║  ██║██║██║  ██║   ██║
            // ╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝   ╚═╝
            //

            this.shirtBackBg = ko.computed(function(){
                return `gfx/order/image_3d/shirt/back/bg.png`;
            })

            this.selectedShirtSleeve = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '31') {
                        for (var option of category.options) {
                            if (option.id == '89' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1314":
                                                return "gfx/order/image_3d/shirt/sleeves/normal-preview.png";
                                            case "1315":
                                                return "gfx/order/image_3d/shirt/sleeves/rolled-preview.png";
                                            case "1316":
                                                // check if "short cuff" is on
                                                const shortCuffOption = category.options.filter(el => el.id == '250')[0];
                                                if (shortCuffOption && shortCuffOption.selectedValue()){
                                                    return "gfx/order/image_3d/shirt/cuffs/short_cuffs.png";
                                                } else{
                                                    return "gfx/order/image_3d/shirt/sleeves/short-preview.png";

                                                }

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/shirt/sleeves/normal-preview.png";
            });

            this.selectedShirtEpaulette = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '31') {
                        for (var option of category.options) {
                            if (option.id == '91' && option.selectedValue()) {
                                return "gfx/order/image_3d/shirt/epalluetes.png"

                            } else if (option.id == '91' && !option.selectedValue()) {
                                // spaulete off
                                return "";
                            }
                        }
                    }
                }
                return "";
            });

            this.shirtStandCollarContrast_bg = ko.computed(function(){
                // if is not a shirt
                if (self.selectedGarment.garment_id != '2'){
                    return "";
                }

                // check if "outside collar contrast" is true
                let contrastCategory = self.selectedGarment.categories.filter(el => el.id == '38')[0];
                let outsideCollarContrastOption = contrastCategory.options.filter(el => el.id == '108')[0];

                return (outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/band_stand.png" : "gfx/order/image_3d/shirt/collar/band-preview.png");
            });

            this.shirtInsideCollarContrast_bg = ko.computed(function(){
                // if is not a shirt
                if (self.selectedGarment.garment_id != '2') {
                    return "";
                }

                let contastCategory = self.selectedGarment.categories.filter(el => el.id == '38')[0];
                let outsideCollarContrastOption = contastCategory.options.filter(el => el.id == '106')[0];

                return (outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/band_inside.png" : "");
            });


            this.selectedShirtCollar = ko.computed(function () {

                // if is not a shirt
                if (self.selectedGarment.garment_id != '2'){
                    return "";
                }

                // check if "outside collar contrast" is true
                let contrastCategory = self.selectedGarment.categories.filter(el => el.id == '38')[0];
                let outsideCollarContrastOption = contrastCategory.options.filter(el => el.id == '107')[0];


                for (var category of self.selectedGarment.categories) {
                    if (category.id == '33') {
                        for (var option of category.options) {
                            if (option.id == '94' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1326":
                                                return outsideCollarContrastOption.selectedValue() ? ""                                                                            : "gfx/order/image_3d/shirt/collar/band-preview.png";
                                            case "1327":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/batman.png"                         : "gfx/order/image_3d/shirt/collar/batman-preview.png";
                                            case "1328":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/button_down.png"                    : "gfx/order/image_3d/shirt/collar/button_down-preview.png";
                                            case "1329":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/italian_two_button.png"             : "gfx/order/image_3d/shirt/collar/italian_collar_2_button-preview.png";
                                            case "1330":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/cut_away_1_button.png"              : "gfx/order/image_3d/shirt/collar/cut_away_1_button-preview.png";
                                            case "1331":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/cut_away_2_button.png"              : "gfx/order/image_3d/shirt/collar/cut_away_2_button-preview.png";
                                            case "1332":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/french_one_button.png"              : "gfx/order/image_3d/shirt/collar/french_collar_1_button-preview.png";
                                            case "1333":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/hidden-button.png"                  : "gfx/order/image_3d/shirt/collar/hidden_button-preview.png";
                                            case "1334":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/italian_one_button.png"             : "gfx/order/image_3d/shirt/collar/italian_collar_1_button-preview.png";
                                            case "1335":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/italian_two_button.png"             : "gfx/order/image_3d/shirt/collar/italian_collar_2_button-preview.png";
                                            case "1336":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/modern.png"                         : "gfx/order/image_3d/shirt/collar/coll_modern-preview.png";
                                            case "1337":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/round_collar.png"                   : "gfx/order/image_3d/shirt/collar/round_collar-preview.png";
                                            case "1338":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/french_two_button.png"              : "gfx/order/image_3d/shirt/collar/french_collar_2_button-preview.png";
                                            case "1339":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/tab.png"                            : "gfx/order/image_3d/shirt/collar/tab-preview.png";
                                            case "1340":
                                                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/tuxedo.png"                         : "gfx/order/image_3d/shirt/collar/tuxedo-preview.png";


                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return outsideCollarContrastOption.selectedValue() ? "gfx/order/image_3d/shirt/collar/contrast/band_stand.png"                                                    : "gfx/order/image_3d/shirt/collar/band-preview.png";
            });

            this.selectedShirtFront = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '34') {
                        for (var option of category.options) {
                            if (option.id == '96' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1341":
                                                return "gfx/order/image_3d/shirt/front/angled_pleat.png" // Angle Pleats
                                            case "1342":
                                                return "gfx/order/image_3d/shirt/front/box-preview.png" // Box
                                            case "1343":
                                                return "gfx/order/image_3d/shirt/front/angled_pleat.png" // Custom
                                            case "1344":
                                                return "gfx/order/image_3d/shirt/front/hidden-preview.png" // Fly
                                            case "1345":
                                                return "gfx/order/image_3d/shirt/front/single-preview.png" // French
                                            case "1346":
                                                return "gfx/order/image_3d/shirt/front/full_pleat.png" // Full Pleats
                                            case "1347":
                                                return "gfx/order/image_3d/shirt/front/pleat.png" // Pleats

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/shirt/front/box-preview.png";
            });

            this.selectedShirtSeams = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '34') {
                        for (var option of category.options) {
                            if (option.id == '98' && option.selectedValue()) {
                                return "gfx/order/image_3d/shirt/seams.png"
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtBottom = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '35') {
                        for (var option of category.options) {
                            if (option.id == '99' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1348":
                                                return "gfx/order/image_3d/shirt/bottom/straight-preview.png";
                                            case "1349":
                                                return "gfx/order/image_3d/shirt/bottom/classi-preview.png";
                                            case "1350":
                                                return "gfx/order/image_3d/shirt/bottom/tri_tab-preview.png";
                                            case "1351":
                                                return "gfx/order/image_3d/shirt/bottom/straight__-preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/shirt/bottom/straight-preview.png";
            });

            this.selectedShirtBack = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '36') {
                        for (var option of category.options) {
                            if (option.id == '101' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1352":
                                                return "gfx/order/image_3d/shirt/back/box_pleat-preview.png";
                                            case "1353":
                                                return "gfx/order/image_3d/shirt/back/center_pleats-preview.png";
                                            case "1354":
                                                return "gfx/order/image_3d/shirt/back/plain-preview.png";
                                            case "1355":
                                                return "gfx/order/image_3d/shirt/back/side_pleats-preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/shirt/back/plain-preview.png";
            });

            this.selectedShirtPocketsRight = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '37') {
                        for (var option of category.options) {
                            if (option.id == '105' && option.values) {
                                if (option.selectedValue() && (option.selectedValue().id == '1365' || option.selectedValue().id == '1367')) {
                                    for (var option of category.options) {
                                        if (option.id == '103' && option.values) {
                                            for (var value of option.values) {
                                                if (value.selected() == true) {
                                                    switch (value.id) {
                                                        case "1356":
                                                            return "gfx/order/image_3d/shirt/pocket/angle_flap-preview-left.png"; // Angle Flap
                                                        case "1357":
                                                            return "gfx/order/image_3d/shirt/pocket/classic-preview-left.png"; // Classic
                                                        case "1358":
                                                            return "gfx/order/image_3d/shirt/pocket/classic_angle-preview-left.png"; // Classic Angle
                                                        case "1359":
                                                            return "gfx/order/image_3d/shirt/pocket/classic_square-preview-left.png"; // Classic Square
                                                        case "1360":
                                                            return "gfx/order/image_3d/shirt/pocket/diamond_straight-preview-left.png"; // Diamond Straight
                                                        case "1361":
                                                            return "gfx/order/image_3d/shirt/pocket/flap_diamond-preview-left.png"; // Flap Diamond
                                                        case "1362":
                                                            return ""; // No Pocket
                                                        case "1363":
                                                            return "gfx/order/image_3d/shirt/pocket/round_flap-preview-left.png"; // Round Flap
                                                        case "1364":
                                                            return "gfx/order/image_3d/shirt/pocket/round_with_glass-preview-left.png"; // Round with Glass

                                                        default:
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtPocketsLeft = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '37') {
                        for (var option of category.options) {
                            if (option.id == '105' && option.values) {
                                if (option.selectedValue() && (option.selectedValue().id == '1365' || option.selectedValue().id == '1366')) {
                                    for (var option of category.options) {
                                        if (option.id == '103' && option.values) {
                                            for (var value of option.values) {
                                                if (value.selected() == true) {
                                                    switch (value.id) {
                                                        case "1356":
                                                            return "gfx/order/image_3d/shirt/pocket/angle_flap-preview.png"; // Angle Flap
                                                        case "1357":
                                                            return "gfx/order/image_3d/shirt/pocket/classic-preview.png"; // Classic
                                                        case "1358":
                                                            return "gfx/order/image_3d/shirt/pocket/classic_angle-preview.png"; // Classic Angle
                                                        case "1359":
                                                            return "gfx/order/image_3d/shirt/pocket/classic_square-preview.png"; // Classic Square
                                                        case "1360":
                                                            return "gfx/order/image_3d/shirt/pocket/diamond_straight-preview.png"; // Diamond Straight
                                                        case "1361":
                                                            return "gfx/order/image_3d/shirt/pocket/flap_diamond-preview.png"; // Flap Diamond
                                                        case "1362":
                                                            return ""; // No Pocket
                                                        case "1363":
                                                            return "gfx/order/image_3d/shirt/pocket/round_flap-preview.png"; // Round Flap
                                                        case "1364":
                                                            return "gfx/order/image_3d/shirt/pocket/round_with_glass-preview.png"; // Round with Glass

                                                        default:
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtContrastOutsideCollar = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '106' && option.selectedValue()) {
                                return "gfx/order/image_3d/shirt/contrast/inside_collar.png"
                            }
                        }
                    }
                }
                return "";
            });

            this.shirtBackCollar = ko.observable();
            this.selectedShirtContrastOutsideCollar = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '107' && option.selectedValue()) {
                                self.shirtBackCollar("gfx/order/image_3d/shirt/back/back_collar.png");
                                return "gfx/order/image_3d/shirt/contrast/outside_collar.png"
                            }
                        }
                    }
                }
                self.shirtBackCollar("");
                return "";
            });

            this.selectedShirtContrastCollarStand = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '108' && option.selectedValue()) {
                                return "gfx/order/image_3d/shirt/contrast/collar_stand.png"
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtContrastInsideCuff = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '109' && option.selectedValue()) {
                                if (self.selectedShirtSleeve() == 'gfx/order/image_3d/shirt/sleeves/normal-preview.png') {
                                    return 'gfx/order/image_3d/shirt/contrast/inner_cuff.png'
                                } else if (self.selectedShirtSleeve() == 'gfx/order/image_3d/shirt/sleeves/rolled-preview.png') {
                                    return "gfx/order/image_3d/shirt/contrast/innercuff_left.png"
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.shirtBackCuff = ko.observable();
            this.selectedShirtContrastOutsideCuff = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '110' && option.selectedValue()) {
                                if (self.selectedShirtSleeve() == 'gfx/order/image_3d/shirt/sleeves/normal-preview.png') {
                                    self.shirtBackCuff("gfx/order/image_3d/shirt/back/sleeve_cuff.png");
                                    return 'gfx/order/image_3d/shirt/contrast/cuffs.png'
                                } else if (self.selectedShirtSleeve() == 'gfx/order/image_3d/shirt/sleeves/rolled-preview.png') {
                                    self.shirtBackCuff("");
                                    return "gfx/order/image_3d/shirt/contrast/cuff_left.png";
                                }
                            }
                        }
                    }
                }
                self.shirtBackCuff("");
                return "";
            });

            this.selectedShirtContrastInsidePlacket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '112' && option.selectedValue()) {
                                return "gfx/order/image_3d/shirt/contrast/inside_placket.png"
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtContrastInsideCollar = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '106' && option.selectedValue()) {
                                return "gfx/order/image_3d/shirt/contrast/inside_collar.png"
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtContrastOutsidePlacket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '113' && option.selectedValue()) {
                                return self.side() == "front" ? "gfx/order/image_3d/shirt/contrast/front_placket_big.png" : "gfx/order/image_3d/shirt/contrast/outside_placket.png"
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedShirtButtomCollor = ko.computed(function () {
                // fly front hides shirt button
                if (self.selectedShirtFront() == "gfx/order/image_3d/shirt/front/hidden-preview.png"){
                    return "";
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '114' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1368":	// Aqua
                                                return "gfx/order/image_3d/shirt/buttons/aqua-preview.png";
                                            case "1369": //	 Black
                                                return "gfx/order/image_3d/shirt/buttons/black-preview.png";
                                            case "1370": //	 Charcoal
                                                return "gfx/order/image_3d/shirt/buttons/charcoal-preview.png";
                                            case "1371": //	 Cream
                                                return "gfx/order/image_3d/shirt/buttons/cream-preview.png";
                                            case "1372": //	 Cufflink Stud
                                                return "gfx/order/image_3d/shirt/buttons/studs-preview.png";
                                            case "1373": //	 DEFAULT
                                                return "gfx/order/image_3d/shirt/buttons/white-preview.png";
                                            case "1374": //	 Light Green
                                                return "gfx/order/image_3d/shirt/buttons/green-preview.png";
                                            case "1375": //	 Light Pink
                                                return "gfx/order/image_3d/shirt/buttons/light_pink-preview.png";
                                            case "1376": //	 Navy
                                                return "gfx/order/image_3d/shirt/buttons/navy-preview.png";
                                            case "1377": //	 Orange
                                                return "gfx/order/image_3d/shirt/buttons/orange-preview.png";
                                            case "1378": //	 Pink
                                                return "gfx/order/image_3d/shirt/buttons/pink-preview.png";
                                            case "1379": //	 Powder Blue
                                                return ""; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                                            case "1380": //	 Purple
                                                return "gfx/order/image_3d/shirt/buttons/purple-preview.png";
                                            case "1381": //	 Red
                                                return "gfx/order/image_3d/shirt/buttons/red-preview.png";
                                            case "1382": //	 Screw Back Stud
                                                return "gfx/order/image_3d/shirt/buttons/s_studs-preview.png";
                                            case "1383": //	 White
                                                return "gfx/order/image_3d/shirt/buttons/white-preview.png";
                                            case "1384": //	 Yellow
                                                return "gfx/order/image_3d/shirt/buttons/yellow-preview.png";


                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/shirt/buttons/white-preview.png";
            });

            this.selectedShirtContrastButtomCollor = ko.computed(function () {
                 // fly front hides shirt button
                 if (self.selectedShirtFront() == "gfx/order/image_3d/shirt/front/hidden-preview.png"){
                    return "";
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '38') {
                        for (var option of category.options) {
                            if (option.id == '114' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1368":	// Aqua
                                                return "gfx/order/image_3d/shirt/buttons/aqua-preview.png";
                                            case "1369": //	 Black
                                                return "gfx/order/image_3d/shirt/buttons/s_black-preview.png";
                                            case "1370": //	 Charcoal
                                                return "gfx/order/image_3d/shirt/buttons/s_charcoal-preview.png";
                                            case "1371": //	 Cream
                                                return "gfx/order/image_3d/shirt/buttons/s_cream-preview.png";
                                            case "1372": //	 Cufflink Stud
                                                return "gfx/order/image_3d/shirt/buttons/s_studs-preview.png";
                                            case "1373": //	 DEFAULT
                                                return "gfx/order/image_3d/shirt/buttons/s_white-preview.png";
                                            case "1374": //	 Light Green
                                                return "gfx/order/image_3d/shirt/buttons/s_green-preview.png";
                                            case "1375": //	 Light Pink
                                                return "gfx/order/image_3d/shirt/buttons/s_light_pink-preview.png";
                                            case "1376": //	 Navy
                                                return "gfx/order/image_3d/shirt/buttons/s_navy-preview.png";
                                            case "1377": //	 Orange
                                                return "gfx/order/image_3d/shirt/buttons/s_orange-preview.png";
                                            case "1378": //	 Pink
                                                return "gfx/order/image_3d/shirt/buttons/s_pink-preview.png";
                                            case "1379": //	 Powder Blue
                                                return ""; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                                            case "1380": //	 Purple
                                                return "gfx/order/image_3d/shirt/buttons/s_purple-preview.png";
                                            case "1381": //	 Red
                                                return "gfx/order/image_3d/shirt/buttons/s_red-preview.png";
                                            case "1382": //	 Screw Back Stud
                                                return "gfx/order/image_3d/shirt/buttons/s_studs-preview.png";
                                            case "1383": //	 White
                                                return "gfx/order/image_3d/shirt/buttons/s_white-preview.png";
                                            case "1384": //	 Yellow
                                                return "gfx/order/image_3d/shirt/buttons/s_yellow-preview.png";


                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/shirt/buttons/s_white-preview.png";
            });




            this.shirtDarts = ko.computed(function(){
                // if selected back is not "Side Pleats"
                if (self.selectedShirtBack() != 'gfx/order/image_3d/shirt/back/side_pleats-preview.png'){
                    for (var category of self.selectedGarment.categories) {
                        if (category.id == '36') {
                            for (var option of category.options) {
                                if (option.id == '248' && option.selectedValue()) {
                                    return "gfx/order/image_3d/shirt/back/darts.png";
                                }
                            }
                        }
                    }


                }

                return "";
            });

            // Jacket

            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝
            //      ██║███████║██║     █████╔╝ █████╗     ██║
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝
            //
            this.jacketDefaultImagesFront = ko.observableArray([
                "gfx/order/image_3d/jacket/upper.png",
                "gfx/order/image_3d/jacket/sleeves_bg.png"
            ]);

            // used in _v2 only
            this.jacketDefaultImagesBack = ko.observableArray([
            ]);

            this.jacketDefaultImagesSleeve = ko.observableArray([
                "gfx/order/image_3d/jacket/buttons/bg.png"
            ]);
            this.jacketDefaultImagesMonogram = ko.observableArray([
                "gfx/order/image_3d/liningFabric/bg.png"
            ]);

            this.selectedJacketButtonStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '1') {
                        for (var option of category.options) {
                            if (option.id == '1' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "1": // Custom
                                                return "gfx/order/image_3d/jacket/structure/one_but_preview.png";
                                            case "2": // Double breasted
                                                return "gfx/order/image_3d/jacket/structure/four_double_preview.png";
                                            case "3": // Double Breasted(one to close)
                                                return "gfx/order/image_3d/jacket/structure/four_open_double_preview.png";
                                            case "4": // Double breasted(six buttons)
                                                return "gfx/order/image_3d/jacket/structure/six_double_preview.png";
                                            case "5": // Four Button
                                                return "gfx/order/image_3d/jacket/structure/four_but_preview.png";
                                            case "6": // One buttom
                                                return "gfx/order/image_3d/jacket/structure/one_but_preview.png";
                                            case "7": // Three Button
                                                return "gfx/order/image_3d/jacket/structure/three_but_preview.png";
                                            case "8": // Two Button
                                                return "gfx/order/image_3d/jacket/structure/two_but_preview.png";


                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/structure/one_but_preview.png";
            });

            // only for v2
            this.selectedJacketButtonsBHStyle = ko.observable("");

            this.selectedJacketBottom = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '4') {
                        for (var option of category.options) {
                            if (option.id == '7' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "24": // Curved Bottom
                                                return "gfx/order/image_3d/jacket/bottom/curved_bottom_preview.png";
                                            case "25": // Custom Bottom
                                                return "gfx/order/image_3d/jacket/bottom/curved_bottom_preview.png";
                                            case "26": // Straight Bottom
                                                return "gfx/order/image_3d/jacket/bottom/straight_bottom_preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/bottom/curved_bottom_preview.png";
            });

            /** Used only for 3D_v2 */
            this.selectedJacketBottom2 = ko.computed(function () {
                return "";
            });

            this.selectedJacketVent = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '5') {
                        for (var option of category.options) {
                            if (option.id == '9' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "27": // Custom Vent
                                                return "gfx/order/image_3d/jacket/vent/no_vent_preview.png";
                                            case "28": // No Vent
                                                return "gfx/order/image_3d/jacket/vent/no_vent_preview.png";
                                            case "29": // One Vent
                                                return "gfx/order/image_3d/jacket/vent/one_vent_preview.png";
                                            case "30": // Two Vent
                                                return "gfx/order/image_3d/jacket/vent/two_vent_preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/vent/no_vent_preview.png";
            });

            /** Used only for 3D_v2 */
            this.jacketVent2 = ko.computed(function(){
                return "";
            });

            this.selectedJacketLapel = ko.computed(function () {

                // get jacket structure
                /*
                    this step is important because the jacket structure changes the lapel height.

                    The different lapel paths stay in different folders,
                    for example:
                        lapel for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                        lapel for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                */
                var variantPath;
                switch (self.selectedJacketButtonStyle()) {
                    case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/four_double_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/four_open_double_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/six_double_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/four_but_preview.png":
                        variantPath = '4';
                        break;
                    case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/three_but_preview.png":
                        variantPath = '3';
                        break;
                    case "gfx/order/image_3d/jacket/structure/two_but_preview.png":
                        variantPath = '2';
                        break;

                    default:
                        variantPath = 'd';
                        break;
                }


                for (var category of self.selectedGarment.categories) {
                    if (category.id == '6') {
                        for (var option of category.options) {
                            if (option.id == '12' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "31": //	 Custom Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/notch_lappel_preview.png`;
                                            case "32": //	 Notch Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/notch_lappel_preview.png`;
                                            case "33": //	 Peak Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/peak_lappel_preview.png`;
                                            case "34": //	 Shawl Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/shawl_lappel_preview.png`;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `gfx/order/image_3d/jacket/lapel/${variantPath}/notch_lappel_preview.png`;
            });

            /** Used in 3D V2 */
            this.selectedJacketLapelShadow = ko.computed(function () {
                return "";
            });

            this.selectedJacketSleeve = ko.computed(function () {

                // if tuxedo buttons is on
                const coveredVariant = self.JacketTuxedoButtons() ? "_covered" : "";

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '29' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "225": //1
                                                return "gfx/order/image_3d/jacket/buttons/one_button"+coveredVariant+".png"
                                            case "226": //2
                                                return "gfx/order/image_3d/jacket/buttons/two_buttons"+coveredVariant+".png";
                                            case "227": //3
                                                return "gfx/order/image_3d/jacket/buttons/three_buttons"+coveredVariant+".png";
                                            case "228": //4
                                                return "gfx/order/image_3d/jacket/buttons/four_buttons"+coveredVariant+".png";
                                            case "229": //5
                                                return "gfx/order/image_3d/jacket/buttons/five_buttons"+coveredVariant+".png";
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });

            // used in v2 only
            this.selectedJacketSleeveButton1BH = ko.observable("");
            this.selectedJacketSleeveButton2BH = ko.observable("");
            this.selectedJacketSleeveButton3BH = ko.observable("");
            this.selectedJacketSleeveButton4BH = ko.observable("");
            this.selectedJacketSleeveButton5BH = ko.observable("");


            this.selectedJacketSleeveButton1 = ko.computed(function () {
                // get color

                if(self.JacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_1.png";
                }

                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '38' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "241": // Black
                                                buttonColor = "black";
                                                break;
                                            case "242": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "246": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "247": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "248": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "249": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "250": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "251": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "252": // White
                                                buttonColor = "white";
                                                break;
                                            case "243": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "244": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "245": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_1.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_1.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_1.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_1.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_1.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_1.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_1.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_1.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_1.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_1.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_1.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_1.png";

                    default:
                        break;
                }

            });

            this.selectedJacketSleeveButton2 = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                if(self.JacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_2.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '39' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "253": // Black
                                                buttonColor = "black";
                                                break;
                                            case "254": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "258": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "259": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "260": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "261": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "262": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "263": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "264": // White
                                                buttonColor = "white";
                                                break;
                                            case "255": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "256": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "257": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_2.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_2.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_2.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_2.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_2.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_2.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_2.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_2.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_2.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_2.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_2.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_2.png";

                    default:
                        break;
                }
            });

            this.selectedJacketSleeveButton3 = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                if(self.JacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_3.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '40' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "265": // Black
                                                buttonColor = "black";
                                                break;
                                            case "266": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "270": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "271": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "272": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "273": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "274": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "275": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "276": // White
                                                buttonColor = "white";
                                                break;
                                            case "267": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "268": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "269": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_3.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_3.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_3.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_3.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_3.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_3.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_3.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_3.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_3.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_3.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_3.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_3.png";

                    default:
                        break;
                }
            });

            this.selectedJacketSleeveButton4 = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                if(self.JacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_4.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '41' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "277": // Black
                                                buttonColor = "black";
                                                break;
                                            case "278": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "282": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "283": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "284": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "285": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "286": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "287": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "288": // White
                                                buttonColor = "white";
                                                break;
                                            case "279": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "280": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "281": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_4.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_4.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_4.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_4.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_4.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_4.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_4.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_4.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_4.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_4.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_4.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_4.png";

                    default:
                        break;
                }
            });

            this.selectedJacketSleeveButton5 = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                if(self.JacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_5.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '7') {
                        for (var option of category.options) {
                            if (option.id == '42' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "289": // Black
                                                buttonColor = "black";
                                                break;
                                            case "290": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "294": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "295": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "296": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "297": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "298": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "299": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "300": // White
                                                buttonColor = "white";
                                                break;
                                            case "291": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "292": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "293": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_5.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_5.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_5.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_5.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_5.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_5.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_5.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_5.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_5.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_5.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_5.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_5.png";

                    default:
                        break;
                }
            });

            // used in v2 only
            this.selectedJacketSleeveButton1Zoom = this.selectedJacketSleeveButton1;
            this.selectedJacketSleeveButton2Zoom = this.selectedJacketSleeveButton2;
            this.selectedJacketSleeveButton3Zoom = this.selectedJacketSleeveButton3;
            this.selectedJacketSleeveButton4Zoom = this.selectedJacketSleeveButton4;
            this.selectedJacketSleeveButton5Zoom = this.selectedJacketSleeveButton5;


            this.selectedJacketSleeveThread1 = ko.computed(function () {

                if(self.JacketTuxedoButtons()){
                    return "";
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "48")[0];

                return threadOption.selectedValue().tpreview1;
            }),

            this.selectedJacketSleeveThread2 = ko.computed(function () {
                if(self.JacketTuxedoButtons()){
                    return "";
                }

                if (self.selectedJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "49")[0];

                return threadOption.selectedValue().tpreview2;
            }),

            this.selectedJacketSleeveThread3 = ko.computed(function () {
                if(self.JacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "50")[0];

                return threadOption.selectedValue().tpreview3;
            }),

            this.selectedJacketSleeveThread4 = ko.computed(function () {
                if(self.JacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "51")[0];

                return threadOption.selectedValue().tpreview4;
            }),

            this.selectedJacketSleeveThread5 = ko.computed(function () {
                if(self.JacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "52")[0];

                return threadOption.selectedValue().tpreview5;
            }),

            this.selectedJacketSleeveBHColour1 = ko.computed(function () {
                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "43")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview1);

                return bhOption.selectedValue().preview1;
            }),

            this.selectedJacketSleeveBHColour2 = ko.computed(function () {
                 if (self.selectedJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "44")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview2);

                return bhOption.selectedValue().preview2;
            }),

            this.selectedJacketSleeveBHColour3 = ko.computed(function () {
                 if (self.selectedJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "45")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview3);

                return bhOption.selectedValue().preview3;
            }),

            this.selectedJacketSleeveBHColour4 = ko.computed(function () {
                 if (self.selectedJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "46")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview4);

                return bhOption.selectedValue().preview4;
            }),

            this.selectedJacketSleeveBHColour5 = ko.computed(function () {
                 if (self.selectedJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "47")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview5);

                return bhOption.selectedValue().preview5;
            }),



            this.selectedJacketPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '8') {
                        for (var option of category.options) {
                            if (option.id == '33' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "230": // Back Curved
                                                return "gfx/order/image_3d/jacket/pocket/jp6_preview.png";
                                            case "231": // Batman
                                                return "gfx/order/image_3d/jacket/pocket/jp2_preview.png";
                                            case "232": // Classic
                                                return "gfx/order/image_3d/jacket/pocket/jp5_preview.png";
                                            case "233": // Front Curved
                                                return "gfx/order/image_3d/jacket/pocket/jp3_preview.png";
                                            case "234": // Patch Pocket
                                                return "gfx/order/image_3d/jacket/pocket/jp9_preview.png";
                                            case "235": // Slightly Slanted
                                                return "gfx/order/image_3d/jacket/pocket/jp1_preview.png";
                                            case "236": // Welt Pocket
                                                return "gfx/order/image_3d/jacket/pocket/jp7_preview.png";
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/pocket/jp5_preview.png";
            });

            this.selectedJacketTicketPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '8') {
                        for (var option of category.options) {
                            if (option.id == '34' && option.selectedValue()) {
                                switch (self.selectedJacketPocket()) {
                                    case "gfx/order/image_3d/jacket/pocket/jp6_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp6_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp2_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp2_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp5_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp5_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp3_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp3_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp9_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp9_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp1_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp1_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp7_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp7_preview.png";
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedJacketBreastPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '9') {
                        for (var option of category.options) {
                            if (option.id == '36' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "238": // Patch Pocket
                                                return "gfx/order/image_3d/jacket/breastpocket/patch_pocket_preview.png";
                                            case "237": // No Breastpocket
                                                return "";
                                            case "240": // Standard Straight
                                                return "gfx/order/image_3d/jacket/breastpocket/default_pr.png";
                                            case "239": // Standard Angled
                                                return "gfx/order/image_3d/jacket/breastpocket/default_angled_pr.png";
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/breastpocket/patch_pocket_preview.png";
            });

            this.stitchForPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {
                        for (var option of category.options) {
                            if (option.id == '26' && option.selectedValue()) {
                                switch (self.selectedJacketPocket()) {
                                    case "gfx/order/image_3d/jacket/pocket/jp6_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp6_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp2_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp2_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp5_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp5_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp3_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp3_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp9_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp9_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp1_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp1_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp7_preview.png":
                                        return "";
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.stitchForLapel = ko.computed(function () {


                // get jacket structure
                /*
                    this step is important because the jacket structure changes the stitch height.

                    The different stitch paths stay in different folders,
                    for example:
                        stitch for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                        stitch for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                */
                var variantPath;
                switch (self.selectedJacketButtonStyle()) {
                    case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/four_double_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/four_open_double_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/six_double_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/four_but_preview.png":
                        variantPath = '3';
                        break;
                    case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                        variantPath = '1';
                        break;
                    case "gfx/order/image_3d/jacket/structure/three_but_preview.png":
                        variantPath = '3';
                        break;
                    case "gfx/order/image_3d/jacket/structure/two_but_preview.png":
                        variantPath = '2';
                        break;

                    default:
                        variantPath = 'd';
                        break;
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {
                        for (var option of category.options) {
                            if (option.id == '25' && option.selectedValue()) {
                                console.log('option', option);

                                if (self.selectedJacketLapel().includes("notch_lappel_preview.png")){
                                    return `gfx/order/image_3d/jacket/topstitch/notch_${variantPath}.png`;
                                }
                                else if (self.selectedJacketLapel().includes("peak_lappel_preview.png")){
                                    return `gfx/order/image_3d/jacket/topstitch/peak_${variantPath}.png`;
                                }
                                else if (self.selectedJacketLapel().includes("shawl_lappel_preview.png")){
                                    return `gfx/order/image_3d/jacket/topstitch/shawl_${variantPath}.png`;
                                }
                                else {
                                    return "";
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.bh = ko.computed(function () {
                var bh = false;
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '6') {
                        for (var option of category.options) {
                            if (option.id == '14' && option.selectedValue()) {
                                bh = true;
                            }
                        }
                        for (var option of category.options) {
                            if (option.id == '15' && bh) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        // get color piping
                                        return value.lapel1;
                                    }
                                }
                                return "";
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedJacketLiningColorFront = ko.computed(function () {
                return "gfx/order/image_3d/jacket/01.png";
            })

            // For jacket monogram
            this.selectedJacketLiningColor = ko.computed(function () {


                for (var category of self.selectedGarment.categories) {
                    if (category.id == '46') {
                        for (var option of category.options) {
                            if (option.id == '124' && option.selectedValue()) {
                                // if (option.selectedValue().image.includes("/gerlin/")) {
                                //     return option.selectedValue().image.replace("/preview/", "/sign/").replace("Gerlin", "Gerlin-");

                                // } else if (option.selectedValue().image.includes("/preview/")) {
                                //     return option.selectedValue().image.replace("/preview/", "/sign/");

                                // } else if (option.selectedValue().image.includes("gfx/order/image_3d/liningFabric/100KL")) {
                                //     // default value (TODO)
                                //     $.jGrowl("Image is not available yet for this linning");
                                //     return "gfx/order/image_3d/liningFabric/sign/01.png";
                                // }
                                return option.selectedValue().previews;
                            }
                        }
                    }
                }

                // default value
                return "gfx/order/image_3d/liningFabric/sign/01.png";
            });

            this.selectedJacketPipingColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '46') {
                        for (var option of category.options) {
                            if (option.id == '125' && option.selectedValue()) {
                                return option.selectedValue().preview;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/pipingColors/piping_default_preview.png";
            });

            this.selectedJacketPocketColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '46') {
                        for (var option of category.options) {
                            if (option.id == '126' && option.selectedValue()) {
                                return option.selectedValue().inpocket;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/inpocket/piping_default_preview.png";
            });

            this.selectedJacketStitchColor = ko.computed(function () { });


            /**
             * tuxedo lapel
             */
            this.selectedJacketLapelTrimming = ko.computed(function () {

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {
                        for (var option of category.options) {
                            if (option.id == '17' && option.selectedValue()) {
                                // get jacket structure
                                /*
                                    this step is important because the jacket structure changes the lapel size.

                                    The different lapel paths stay in different folders,
                                    for example:
                                        lapel for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                                        lapel for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                                */

                                const lapelStyle = self.selectedJacketLapel();

                                // get lapelSize. Can be '2', '3' or 'd'. ================================
                                let lapelSize = "";

                                // if lapelSyle == '1' or 'd' ==> trimming size = 'd'
                                if (['1', 'd'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = 'd';
                                }

                                // if lapelSyle == '2' ==> trimming size = '2'
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0] == '2') {
                                    lapelSize = '2';

                                // if lapelSyle == '3' or '4 ==> trimming size = '3'
                                } else if (['3', '4'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = '3'
                                }

                                // default
                                else {
                                    lapelSize = 'd';
                                }

                                // ===========================================================================

                                // check 'edging' option
                                const edgingOption = category.options.filter(el=>el.id == '19')[0];

                                // get lapel format. can ben notch/peak/shawl ... ===================================
                                let lapelFormat = "";
                                if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "notch") {
                                    lapelFormat = edgingOption.selectedValue() ? "notch_lower_slice"  : "notch_lower";
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "peak") {
                                    lapelFormat = edgingOption.selectedValue() ? "peak_lower_slice"  : "peak_lower";
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "shawl") {
                                    lapelFormat = edgingOption.selectedValue() ? "shawl_slice_trim"  : "shawl";
                                }
                                else{
                                    lapelFormat = edgingOption.selectedValue() ? "notch_lower_slice"  : "notch_lower";
                                }

                                // ====================================================================================


                                return `gfx/order/image_3d/jacket/trimming/${lapelSize}/${lapelFormat}.png`;
                            }
                        }

                    }
                }

                return "";
            });

            /**
           * tuxedo Collar
           */
            this.selectedJacketCollarTrimming = ko.computed(function () {



                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {
                        for (var option of category.options) {
                            if (option.id == '18' && option.selectedValue()) {
                                // get jacket structure
                                /*
                                    this step is important because the jacket structure changes the lapel size.

                                    The different lapel paths stay in different folders,
                                    for example:
                                        lapel for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                                        lapel for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                                */

                                const lapelStyle = self.selectedJacketLapel();

                                // get lapelSize. Can be '2', '3' or 'd'. ================================
                                let lapelSize = "";

                                // if lapelSyle == '1' or 'd' ==> trimming size == 'd'
                                if (['1', 'd'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = 'd';
                                }

                                // if lapelSyle == '2' ==> trimming size == '2'
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0] == '2') {
                                    lapelSize = '2';

                                    // if lapelSyle == '3' or '4 ==> trimming size == '3'
                                } else if (['3', '4'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = '3'
                                }

                                // default
                                else {
                                    lapelSize = 'd';
                                }

                                // ===========================================================================

                                // check 'edging' option
                                const edgingOption = category.options.filter(el => el.id == '19')[0];

                                // get lapel format. can ben notch/peak/shawl ... ===================================
                                let lapelFormat = "";
                                if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "notch") {
                                    lapelFormat = edgingOption.selectedValue() ? "notch_upper_slice" : "notch_upper"
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "peak") {
                                    lapelFormat = edgingOption.selectedValue() ? "peak_upper_slice" : "peak_upper"
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "shawl") {
                                    lapelFormat = edgingOption.selectedValue() ? "shawl_slice_trim" : "shawl"
                                }

                                // default
                                else {
                                    lapelFormat = edgingOption.selectedValue() ? "notch_upper_slice" : "notch_upper";
                                }

                                // ====================================================================================


                                return `gfx/order/image_3d/jacket/trimming/${lapelSize}/${lapelFormat}.png`;
                            }
                        }

                    }
                }

                return "";
            });


            /**
            * tuxedo Buttons
            */
            this.selectedJacketButtonsTrimming = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {
                        for (var option of category.options) {
                            if (option.id == '23' && option.selectedValue()) {

                                const buttonsStyle = self.selectedJacketButtonStyle();

                                // can be one_but two_but four_but three_but six_double four_double four_open_double
                                let buttonStyle = "";
                                if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "one_but") {
                                    buttonStyle = "one_button";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "two_but") {
                                    buttonStyle = "two_buttons";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "four_but") {
                                    buttonStyle = "four_buttons";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "three_but") {
                                    buttonStyle = "three_buttons";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "six_double") {
                                    buttonStyle = "double_two_to_close";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "four_double") {
                                    buttonStyle = "double";
                                }

                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "four_open_double") {
                                    buttonStyle = "double_one_to_close";
                                }
                                else{
                                    buttonStyle = "one_button";
                                }


                                // ====================================================================================


                                return `gfx/order/image_3d/jacket/trimming/${buttonStyle}.png`;
                            }
                        }

                    }
                }

                return "";
            });


            this.selectedJacketTicketPocketTrimming = ko.computed(function () {
                // check if ticket pocket is true
                if (self.selectedJacketTicketPocket() == ""){
                    return "";
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {


                        for (var option of category.options) {
                            if (option.id == '21' && option.selectedValue()) {

                                const pocketStyle = self.selectedJacketPocket();


                                // get buttons format
                                let buttonStyle = pocketStyle.replace("gfx/order/image_3d/jacket/pocket/", "").replace("preview.png", "ticket_trimmed");


                                // ====================================================================================

                                return `gfx/order/image_3d/jacket/pocket/${buttonStyle}.png`;


                            }
                        }

                    }
                }

                return "";
            });


            this.selectedJacketPocketTrimming = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '11') {
                        for (var option of category.options) {
                            if (option.id == '21' && option.selectedValue()) {

                                const pocketStyle = self.selectedJacketPocket();

                                // get buttons format
                                let buttonStyle = pocketStyle.replace("gfx/order/image_3d/jacket/pocket/", "").replace("preview.png", "trimmed");


                                // ====================================================================================
                                // Patch Pocket has no trimming image
                                if (buttonStyle != "jp9_trimmed"){
                                    return `gfx/order/image_3d/jacket/pocket/${buttonStyle}.png`;
                                }
                                else{
                                    return "";
                                }

                            }
                        }

                    }
                }

                return "";
            });


            // Suit
            // ███████╗██╗   ██╗██╗████████╗
            // ██╔════╝██║   ██║██║╚══██╔══╝
            // ███████╗██║   ██║██║   ██║
            // ╚════██║██║   ██║██║   ██║
            // ███████║╚██████╔╝██║   ██║
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝
            //

            this.suitJacketDefaultImagesFront = this.jacketDefaultImagesFront;
            this.suitJacketDefaultImagesBack = this.jacketDefaultImagesBack;

            this.suitJacketDefaultImagesSleeve = this.jacketDefaultImagesSleeve;
            this.suitJacketDefaultImagesMonogram = this.jacketDefaultImagesMonogram;


            this.selectedSuitJacketButtonStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '49') {
                        for (var option of category.options) {
                            if (option.id == '148' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2583": // Custom
                                                return "gfx/order/image_3d/jacket/structure/one_but_preview.png";
                                            case "2584": // Double breasted
                                                return "gfx/order/image_3d/jacket/structure/four_double_preview.png";
                                            case "2585": // Double Breasted(one to close)
                                                return "gfx/order/image_3d/jacket/structure/four_open_double_preview.png";
                                            case "2586": // Double breasted(six buttons)
                                                return "gfx/order/image_3d/jacket/structure/six_double_preview.png";
                                            case "2587": // Four Button
                                                return "gfx/order/image_3d/jacket/structure/four_but_preview.png";
                                            case "2588": // One buttom
                                                return "gfx/order/image_3d/jacket/structure/one_but_preview.png";
                                            case "2589": // Three Button
                                                return "gfx/order/image_3d/jacket/structure/three_but_preview.png";
                                            case "2590": // Two Button
                                                return "gfx/order/image_3d/jacket/structure/two_but_preview.png";


                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/structure/one_but_preview.png";
            });

            // only for v2
            this.selectedSuitJacketButtonsBHStyle = ko.observable("");

            this.selectedSuitJacketBottom = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '50') {
                        for (var option of category.options) {
                            if (option.id == '159' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2606": // Curved Bottom
                                                return "gfx/order/image_3d/jacket/bottom/curved_bottom_preview.png";
                                            case "2607": // Custom Bottom
                                                return "gfx/order/image_3d/jacket/bottom/curved_bottom_preview.png";
                                            case "2608": // Straight Bottom
                                                return "gfx/order/image_3d/jacket/bottom/straight_bottom_preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/bottom/curved_bottom_preview.png";
            });

            /** Used only for 3D_v2 */
            this.selectedSuitJacketBottom2 = ko.computed(function () {
                return "";
            });

            this.selectedSuitJacketPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '55') {
                        for (var option of category.options) {
                            if (option.id == '187' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "3632": // Back Curved
                                                return "gfx/order/image_3d/jacket/pocket/jp6_preview.png";
                                            case "3633": // Batman
                                                return "gfx/order/image_3d/jacket/pocket/jp2_preview.png";
                                            case "3634": // Classic
                                                return "gfx/order/image_3d/jacket/pocket/jp5_preview.png";
                                            case "3635": // Front Curved
                                                return "gfx/order/image_3d/jacket/pocket/jp3_preview.png";
                                            case "3636": // Patch Pocket
                                                return "gfx/order/image_3d/jacket/pocket/jp9_preview.png";
                                            case "3637": // Slightly Slanted
                                                return "gfx/order/image_3d/jacket/pocket/jp1_preview.png";
                                            case "3638": // Welt Pocket
                                                return "gfx/order/image_3d/jacket/pocket/jp7_preview.png";
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/pocket/jp5_preview.png";
            });

            this.selectedSuitJacketTicketPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '55') {
                        for (var option of category.options) {
                            if (option.id == '188' && option.selectedValue()) {
                                switch (self.selectedSuitJacketPocket()) {
                                    case "gfx/order/image_3d/jacket/pocket/jp6_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp6_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp2_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp2_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp5_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp5_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp3_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp3_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp9_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp9_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp1_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp1_preview.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp7_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/ticket-jp7_preview.png";
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketStitchForPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {
                        for (var option of category.options) {
                            if (option.id == '200' && option.selectedValue()) {
                                switch (self.selectedSuitJacketPocket()) {
                                    case "gfx/order/image_3d/jacket/pocket/jp6_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp6_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp2_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp2_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp5_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp5_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp3_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp3_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp9_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp9_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp1_preview.png":
                                        return "gfx/order/image_3d/jacket/pocket/jp1_topstitch.png";
                                    case "gfx/order/image_3d/jacket/pocket/jp7_preview.png":
                                        return "";
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketBreastPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '56') {
                        for (var option of category.options) {
                            if (option.id == '190' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "3647": // Patch Pocket
                                                return "gfx/order/image_3d/jacket/breastpocket/patch_pocket_preview.png";
                                            case "3646": // No Breastpocket
                                                return "";
                                            case "3649": // Standard Straight
                                                return "gfx/order/image_3d/jacket/breastpocket/default_pr.png";
                                            case "3648": // Standard Angled
                                                return "gfx/order/image_3d/jacket/breastpocket/default_angled_pr.png";
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/breastpocket/patch_pocket_preview.png";
            });

            this.selectedSuitJacketLapel = ko.computed(function () {

                // get jacket structure
                /*
                    this step is important because the jacket structure changes the lapel height.

                    The different lapel paths stay in different folders,
                    for example:
                        lapel for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                        lapel for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                */
               var variantPath;
               switch (self.selectedSuitJacketButtonStyle()) {
                   case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/four_double_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/four_open_double_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/six_double_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/four_but_preview.png":
                       variantPath = '4';
                       break;
                   case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/three_but_preview.png":
                       variantPath = '3';
                       break;
                   case "gfx/order/image_3d/jacket/structure/two_but_preview.png":
                       variantPath = '2';
                       break;

                   default:
                       variantPath = 'd';
                       break;
               }


                for (var category of self.selectedGarment.categories) {
                    if (category.id == '52') {
                        for (var option of category.options) {
                            if (option.id == '164' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2613": //	 Custom Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/notch_lappel_preview.png`;
                                            case "2614": //	 Notch Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/notch_lappel_preview.png`;
                                            case "2615": //	 Peak Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/peak_lappel_preview.png`;
                                            case "2616": //	 Shawl Lapel
                                                return `gfx/order/image_3d/jacket/lapel/${variantPath}/shawl_lappel_preview.png`;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `gfx/order/image_3d/jacket/lapel/${variantPath}/notch_lappel_preview.png`;
            });

            /** Used in 3D V2 */
            this.selectedSuitJacketLapelShadow = ko.computed(function () {
                return "";
            });

            this.selectedSuitJacketStitchForLapel = ko.computed(function () {return ""});

            this.selectedSuitJacketStitchForLapel = ko.computed(function () {
                 // get jacket structure
                /*
                    this step is important because the jacket structure changes the stitch height.

                    The different stitch paths stay in different folders,
                    for example:
                        stitch for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                        stitch for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                */
               var variantPath;
               switch (self.selectedSuitJacketButtonStyle()) {
                   case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/four_double_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/four_open_double_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/six_double_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/four_but_preview.png":
                       variantPath = '3';
                       break;
                   case "gfx/order/image_3d/jacket/structure/one_but_preview.png":
                       variantPath = '1';
                       break;
                   case "gfx/order/image_3d/jacket/structure/three_but_preview.png":
                       variantPath = '3';
                       break;
                   case "gfx/order/image_3d/jacket/structure/two_but_preview.png":
                       variantPath = '2';
                       break;

                   default:
                       variantPath = 'd';
                       break;
               }


                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {
                        for (var option of category.options) {
                            if (option.id == '199' && option.selectedValue()) {
                                console.log('option', option);

                                if (self.selectedSuitJacketLapel().includes("notch_lappel_preview.png")){
                                    return `gfx/order/image_3d/jacket/topstitch/notch_${variantPath}.png`;
                                }
                                else if (self.selectedSuitJacketLapel().includes("peak_lappel_preview.png")){
                                    return `gfx/order/image_3d/jacket/topstitch/peak_${variantPath}.png`;
                                }
                                else if (self.selectedSuitJacketLapel().includes("shawl_lappel_preview.png")){
                                    return `gfx/order/image_3d/jacket/topstitch/shawl_${variantPath}.png`;
                                }
                                else {
                                    return "";
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketBh = ko.computed(function () {
                var bh = false;
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '52') {
                        for (var option of category.options) {
                            if (option.id == '166' && option.selectedValue()) {
                                bh = true;
                            }
                        }
                        for (var option of category.options) {
                            if (option.id == '167' && bh) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        return value.lapel1;
                                    }
                                }
                                return "";

                            }
                        }
                    }
                }
                return "";
            });


            this.selectedSuitJacketSleeve = ko.computed(function () {

                const coveredVariant = self.suitJacketTuxedoButtons() ? "_covered" : "";

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '169' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "2712": //1
                                                return "gfx/order/image_3d/jacket/buttons/one_button"+coveredVariant+".png"
                                            case "2713": //2
                                                return "gfx/order/image_3d/jacket/buttons/two_buttons"+coveredVariant+".png";
                                            case "2714": //3
                                                return "gfx/order/image_3d/jacket/buttons/three_buttons"+coveredVariant+".png";
                                            case "2715": //4
                                                return "gfx/order/image_3d/jacket/buttons/four_buttons"+coveredVariant+".png";
                                            case "2716": //5
                                                return "gfx/order/image_3d/jacket/buttons/five_buttons"+coveredVariant+".png";
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });

            // used in v2 only
            this.selectedSuitJacketSleeveButton1BH = ko.observable("");
            this.selectedSuitJacketSleeveButton2BH = ko.observable("");
            this.selectedSuitJacketSleeveButton3BH = ko.observable("");
            this.selectedSuitJacketSleeveButton4BH = ko.observable("");
            this.selectedSuitJacketSleeveButton5BH = ko.observable("");

            this.selectedSuitJacketVent = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '51') {
                        for (var option of category.options) {
                            if (option.id == '161' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2609": // Custom Vent
                                                return "gfx/order/image_3d/jacket/vent/no_vent_preview.png";
                                            case "2610": // No Vent
                                                return "gfx/order/image_3d/jacket/vent/no_vent_preview.png";
                                            case "2611": // One Vent
                                                return "gfx/order/image_3d/jacket/vent/one_vent_preview.png";
                                            case "2612": // Two Vent
                                                return "gfx/order/image_3d/jacket/vent/two_vent_preview.png";

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/jacket/vent/no_vent_preview.png";
            });

            /** Used only for 3D_v2 */
            this.suitJacketVent2 = ko.computed(function(){
                return "";
            });

            this.selectedSuitJacketSleeveButton1 = ko.computed(function () {

                if(self.suitJacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_1.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '173' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2717": // Black
                                                buttonColor = "black";
                                                break;
                                            case "2718": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "2722": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "2723": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "2724": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "2725": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "2726": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "2727": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "2728": // White
                                                buttonColor = "white";
                                                break;
                                            case "2719": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "2720": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "2721": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_1.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_1.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_1.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_1.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_1.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_1.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_1.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_1.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_1.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_1.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_1.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_1.png";

                    default:
                        break;
                }

            });

            this.selectedSuitJacketSleeveButton2 = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                if(self.suitJacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_2.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '174' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2729": // Black
                                                buttonColor = "black";
                                                break;
                                            case "2730": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "2734": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "2735": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "2736": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "2737": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "2738": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "2739": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "2740": // White
                                                buttonColor = "white";
                                                break;
                                            case "2731": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "2732": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "2733": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_2.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_2.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_2.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_2.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_2.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_2.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_2.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_2.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_2.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_2.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_2.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_2.png";

                    default:
                        break;
                }
            });

            this.selectedSuitJacketSleeveButton3 = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                if(self.suitJacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_3.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '175' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2741": // Black
                                                buttonColor = "black";
                                                break;
                                            case "2742": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "2746": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "2747": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "2748": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "2749": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "2750": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "2751": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "2752": // White
                                                buttonColor = "white";
                                                break;
                                            case "2743": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "2744": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "2745": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_3.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_3.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_3.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_3.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_3.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_3.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_3.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_3.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_3.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_3.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_3.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_3.png";

                    default:
                        break;
                }
            });

            this.selectedSuitJacketSleeveButton4 = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                if(self.suitJacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_4.png";
                }


                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '176' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2753": // Black
                                                buttonColor = "black";
                                                break;
                                            case "2754": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "2758": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "2759": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "2760": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "2761": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "2762": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "2763": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "2764": // White
                                                buttonColor = "white";
                                                break;
                                            case "2755": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "2756": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "2757": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_4.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_4.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_4.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_4.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_4.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_4.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_4.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_4.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_4.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_4.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_4.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_4.png";

                    default:
                        break;
                }
            });

            this.selectedSuitJacketSleeveButton5 = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                if(self.suitJacketTuxedoButtons()){
                    return "gfx/order/image_3d/jacket/buttons/cover_5.png";
                }

                // get color
                var buttonColor = "";
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '54') {
                        for (var option of category.options) {
                            if (option.id == '177' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2765": // Black
                                                buttonColor = "black";
                                                break;
                                            case "2766": // Brown
                                                buttonColor = "brown";
                                                break;
                                            case "2770": // Light Khaki
                                                buttonColor = "light-khaki";
                                                break;
                                            case "2771": // Marron
                                                buttonColor = "marron";
                                                break;
                                            case "2772": // Navy
                                                buttonColor = "navy";
                                                break;
                                            case "2773": // Pearl White
                                                buttonColor = "pearl-white";
                                                break;
                                            case "2774": // Purple
                                                buttonColor = "purple";
                                                break;
                                            case "2775": // Sky Blue
                                                buttonColor = "sky-blue";
                                                break;
                                            case "2776": // White
                                                buttonColor = "white";
                                                break;
                                            case "2767": // Deep Brown
                                                buttonColor = "deep-brown";
                                                break;
                                            case "2768": // DEFAULT
                                                buttonColor = "default";
                                                break;
                                            case "2769": // Dull Silver
                                                buttonColor = "dull silver";
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                switch (buttonColor) {
                    case "black":
                        return "gfx/order/image_3d/jacket/buttons/black_5.png";
                    case "brown":
                        return "gfx/order/image_3d/jacket/buttons/brown_5.png";
                    case "light-khaki":
                        return "gfx/order/image_3d/jacket/buttons/light-khaki_5.png";
                    case "marron":
                        return "gfx/order/image_3d/jacket/buttons/marron_5.png";
                    case "navy":
                        return "gfx/order/image_3d/jacket/buttons/navy_5.png";
                    case "pearl-white":
                        return "gfx/order/image_3d/jacket/buttons/pearl-white_5.png";
                    case "purple":
                        return "gfx/order/image_3d/jacket/buttons/purple_5.png";
                    case "sky-blue":
                        return "gfx/order/image_3d/jacket/buttons/sky-blue_5.png";
                    case "white":
                        return "gfx/order/image_3d/jacket/buttons/white_5.png";
                    case "deep-brown":
                        return "gfx/order/image_3d/jacket/buttons/deep-brown_5.png";
                    case "default":
                        return "gfx/order/image_3d/jacket/buttons/black_5.png";
                    case "dull silver":
                        return "gfx/order/image_3d/jacket/buttons/dull-silver_5.png";

                    default:
                        break;
                }
            });

            // used in v2 only
            this.selectedSuitJacketSleeveButton1Zoom = this.selectedSuitJacketSleeveButton1;
            this.selectedSuitJacketSleeveButton2Zoom = this.selectedSuitJacketSleeveButton2;
            this.selectedSuitJacketSleeveButton3Zoom = this.selectedSuitJacketSleeveButton3;
            this.selectedSuitJacketSleeveButton4Zoom = this.selectedSuitJacketSleeveButton4;
            this.selectedSuitJacketSleeveButton5Zoom = this.selectedSuitJacketSleeveButton5;

            this.selectedSuitJacketSleeveThread1 = ko.computed(function () {

                if(self.suitJacketTuxedoButtons()){
                    return "";
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "182")[0];

                return threadOption.selectedValue().tpreview1;
            }),

            this.selectedSuitJacketSleeveThread2 = ko.computed(function () {

                if(self.suitJacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "183")[0];

                return threadOption.selectedValue().tpreview2;
            }),

            this.selectedSuitJacketSleeveThread3 = ko.computed(function () {

                if(self.suitJacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "184")[0];

                return threadOption.selectedValue().tpreview3;
            }),

            this.selectedSuitJacketSleeveThread4 = ko.computed(function () {

                if(self.suitJacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "185")[0];

                return threadOption.selectedValue().tpreview4;
            }),

            this.selectedSuitJacketSleeveThread5 = ko.computed(function () {

                if(self.suitJacketTuxedoButtons()){
                    return "";
                }

                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const threadOption = sleeveCategory.options.filter(option => option.id == "186")[0];

                return threadOption.selectedValue().tpreview5;
            }),

            this.selectedSuitJacketSleeveBHColour1 = ko.computed(function () {
                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "178")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview1);

                return bhOption.selectedValue().preview1;
            }),

            this.selectedSuitJacketSleeveBHColour2 = ko.computed(function () {
                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "179")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview2);

                return bhOption.selectedValue().preview2;
            }),

            this.selectedSuitJacketSleeveBHColour3 = ko.computed(function () {
                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "180")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview3);

                return bhOption.selectedValue().preview3;
            }),

            this.selectedSuitJacketSleeveBHColour4 = ko.computed(function () {
                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "224")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview4);

                return bhOption.selectedValue().preview4;
            }),

            this.selectedSuitJacketSleeveBHColour5 = ko.computed(function () {
                 if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];

                // not a suit
                if (!sleeveCategory) { return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "181")[0];

                console.log("bhOption.selectedValue()", bhOption.selectedValue().preview5);

                return bhOption.selectedValue().preview5;
            }),


            this.selectedSuitJacketLiningColorFront = ko.computed(function () {
                return "gfx/order/image_3d/jacket/01.png";
            });

            this.selectedSuitJacketLiningColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '63') {
                        for (var option of category.options) {
                            if (option.id == '203' && option.selectedValue()) {
                            //     if (option.selectedValue().image.includes("/gerlin/")) {
                            //         return option.selectedValue().image.replace("/preview/", "/sign/").replace("Gerlin", "Gerlin-");

                            //     } else if (option.selectedValue().image.includes("/preview/")) {
                            //         return option.selectedValue().image.replace("/preview/", "/sign/");

                            //     } else if (option.selectedValue().image.includes("gfx/order/image_3d/liningFabric/100KL")) {
                            //         // default value (TODO)
                            //         $.jGrowl("Image is not available yet for this linning");
                            //         return "gfx/order/image_3d/liningFabric/sign/01.png";
                            //     }
                                return option.selectedValue().previews;
                            }
                        }
                    }
                }

                // default value
                return "gfx/order/image_3d/liningFabric/sign/01.png";
            });

            this.selectedSuitJacketPipingColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '63') {
                        for (var option of category.options) {
                            if (option.id == '204' && option.selectedValue()) {
                                return option.selectedValue().preview;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/pipingColors/piping_default_preview.png";
            });

            this.selectedSuitJacketPocketColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '63') {
                        for (var option of category.options) {
                            if (option.id == '205' && option.selectedValue()) {
                                return option.selectedValue().inpocket;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/inpocket/piping_default_preview.png";
            });

             /**
             * tuxedo lapel
             */
            this.selectedSuitJacketLapelTrimming = ko.computed(function () {

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {
                        for (var option of category.options) {
                            if (option.id == '191' && option.selectedValue()) {
                                // get jacket structure
                                /*
                                    this step is important because the jacket structure changes the lapel size.

                                    The different lapel paths stay in different folders,
                                    for example:
                                        lapel for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                                        lapel for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                                */

                                const lapelStyle = self.selectedSuitJacketLapel();

                                // get lapelSize. Can be '2', '3' or 'd'. ================================
                                let lapelSize = "";

                                // if lapelSyle == '1' or 'd' ==> trimming size = 'd'
                                if (['1', 'd'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = 'd';
                                }

                                // if lapelSyle == '2' ==> trimming size = '2'
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0] == '2') {
                                    lapelSize = '2';

                                // if lapelSyle == '3' or '4 ==> trimming size = '3'
                                } else if (['3', '4'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = '3'
                                }

                                // default
                                else {
                                    lapelSize = 'd';
                                }

                                // ===========================================================================

                                // check 'edging' option
                                const edgingOption = category.options.filter(el=>el.id == '193')[0];

                                // get lapel format. can ben notch/peak/shawl ... ===================================
                                let lapelFormat = "";
                                if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "notch") {
                                    lapelFormat = edgingOption.selectedValue() ? "notch_lower_slice"  : "notch_lower";
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "peak") {
                                    lapelFormat = edgingOption.selectedValue() ? "peak_lower_slice"  : "peak_lower";
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "shawl") {
                                    lapelFormat = edgingOption.selectedValue() ? "shawl_slice_trim"  : "shawl";
                                }
                                else{
                                    lapelFormat = edgingOption.selectedValue() ? "notch_lower_slice"  : "notch_lower";
                                }

                                // ====================================================================================


                                return `gfx/order/image_3d/jacket/trimming/${lapelSize}/${lapelFormat}.png`;
                            }
                        }

                    }
                }

                return "";
            });

            /**
           * tuxedo Collar
           */
            this.selectedSuitJacketCollarTrimming = ko.computed(function () {



                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {
                        for (var option of category.options) {
                            if (option.id == '192' && option.selectedValue()) {
                                // get jacket structure
                                /*
                                    this step is important because the jacket structure changes the lapel size.

                                    The different lapel paths stay in different folders,
                                    for example:
                                        lapel for 1 button  : 'gfx/order/image_3d/jacket/lapel/d/notch_lappel_preview.png
                                        lapel for 2 buttons : 'gfx/order/image_3d/jacket/lapel/2/notch_lappel_preview.png'
                                */

                                const lapelStyle = self.selectedSuitJacketLapel();

                                // get lapelSize. Can be '2', '3' or 'd'. ================================
                                let lapelSize = "";

                                // if lapelSyle == '1' or 'd' ==> trimming size == 'd'
                                if (['1', 'd'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = 'd';
                                }

                                // if lapelSyle == '2' ==> trimming size == '2'
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0] == '2') {
                                    lapelSize = '2';

                                    // if lapelSyle == '3' or '4 ==> trimming size == '3'
                                } else if (['3', '4'].includes(lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[0])) {
                                    lapelSize = '3'
                                }

                                // default
                                else {
                                    lapelSize = 'd';
                                }

                                // ===========================================================================

                                // check 'edging' option
                                const edgingOption = category.options.filter(el => el.id == '193')[0];

                                // get lapel format. can ben notch/peak/shawl ... ===================================
                                let lapelFormat = "";
                                if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "notch") {
                                    lapelFormat = edgingOption.selectedValue() ? "notch_upper_slice" : "notch_upper"
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "peak") {
                                    lapelFormat = edgingOption.selectedValue() ? "peak_upper_slice" : "peak_upper"
                                }
                                else if (lapelStyle.replace("gfx/order/image_3d/jacket/lapel/", "").split('/')[1].replace("_lappel_preview.png", "").toLowerCase() == "shawl") {
                                    lapelFormat = edgingOption.selectedValue() ? "shawl_slice_trim" : "shawl"
                                }

                                // default
                                else {
                                    lapelFormat = edgingOption.selectedValue() ? "notch_upper_slice" : "notch_upper";
                                }

                                // ====================================================================================


                                return `gfx/order/image_3d/jacket/trimming/${lapelSize}/${lapelFormat}.png`;
                            }
                        }

                    }
                }

                return "";
            });


            /**
            * tuxedo Buttons
            */
            this.selectedSuitJacketButtonsTrimming = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {
                        for (var option of category.options) {
                            if (option.id == '197' && option.selectedValue()) {

                                const buttonsStyle = self.selectedSuitJacketButtonStyle();

                                // can be one_but two_but four_but three_but six_double four_double four_open_double
                                let buttonStyle = "";
                                if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "one_but") {
                                    buttonStyle = "one_button";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "two_but") {
                                    buttonStyle = "two_buttons";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "four_but") {
                                    buttonStyle = "four_buttons";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "three_but") {
                                    buttonStyle = "three_buttons";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "six_double") {
                                    buttonStyle = "double_two_to_close";
                                }
                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "four_double") {
                                    buttonStyle = "double";
                                }

                                else if (buttonsStyle.replace("gfx/order/image_3d/jacket/structure/", "").replace("_preview.png", "").toLowerCase() == "four_open_double") {
                                    buttonStyle = "double_one_to_close";
                                }
                                else{
                                    buttonStyle = "one_button";
                                }


                                // ====================================================================================


                                return `gfx/order/image_3d/jacket/trimming/${buttonStyle}.png`;
                            }
                        }

                    }
                }

                return "";
            });

            this.selectedSuitJacketTicketPocketTrimming = ko.computed(function () {
                // check if ticket pocket is true
                if (self.selectedSuitJacketTicketPocket() == ""){
                    return "";
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {


                        for (var option of category.options) {
                            if (option.id == '195' && option.selectedValue()) {

                                const pocketStyle = self.selectedSuitJacketPocket();


                                // get buttons format
                                let buttonStyle = pocketStyle.replace("gfx/order/image_3d/jacket/pocket/", "").replace("preview.png", "ticket_trimmed");


                                // ====================================================================================

                                return `gfx/order/image_3d/jacket/pocket/${buttonStyle}.png`;


                            }
                        }

                    }
                }

                return "";
            });

            this.selectedSuitJacketPocketTrimming = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '53') {
                        for (var option of category.options) {
                            if (option.id == '195' && option.selectedValue()) {

                                const pocketStyle = self.selectedSuitJacketPocket();

                                // get buttons format
                                let buttonStyle = pocketStyle.replace("gfx/order/image_3d/jacket/pocket/", "").replace("preview.png", "trimmed");


                                // ====================================================================================
                                // Patch Pocket has no trimming image
                                if (buttonStyle != "jp9_trimmed"){
                                    return `gfx/order/image_3d/jacket/pocket/${buttonStyle}.png`;
                                }
                                else{
                                    return "";
                                }

                            }
                        }

                    }
                }

                return "";
            });


            // ==============================================================

            this.suitPantsDefaultImage = self.pantsDefaultImage;
            this.suitPantsDefaultImageBack = self.pantsDefaultImageBack;

            this.selectedSuitPantStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    // 14 is id for pant style
                    if (category.id == '58') {
                        for (var option of category.options) {
                            // 57 is id for pant syle
                            if (option.id == '211' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "3840":
                                                return "gfx/order/image_3d/pant/fit/boot_fl_pan_preview.png"

                                            case "3841":
                                                return "gfx/order/image_3d/pant/fit/narr_po_pant_preview.png"

                                            case "3842":
                                                return "gfx/order/image_3d/pant/fit/nom_str_pan_preview.png"

                                            default:
                                                break;
                                        }
                                    }
                                }

                            }
                        }

                    }
                }


                return "gfx/order/image_3d/pant/fit/boot_fl_pan_preview.png";
            });

            this.selectedSuitPantPleatStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '59') {
                        for (var option of category.options) {
                            if (option.id == '213' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "3843":
                                                return "gfx/order/image_3d/pant/pleats/dobb_ple_pan_preview.png"

                                            case "3844":
                                                return "gfx/order/image_3d/pant/none.png"

                                            case "3845":
                                                return "gfx/order/image_3d/pant/pleats/singgle_pl_pant_preview.png"

                                            case "3846":
                                                return "gfx/order/image_3d/pant/pleats/trip_ple_pan_preview.png"

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "gfx/order/image_3d/pant/none.png"
            });

            this.selectedSuitPantFrontPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '60') {
                        for (var option of category.options) {
                            if (option.id == '215' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "3847":
                                                return "gfx/order/image_3d/pant/pockets/jerans_poc_preview.png";

                                            case "3848":
                                                return "gfx/order/image_3d/pant/pockets/jerans_poc_preview.png";

                                            case "3849":
                                                return "gfx/order/image_3d/pant/pockets/mod_cir_pant_p_preview.png";

                                            case "3850":
                                                return "gfx/order/image_3d/pant/none.png";

                                            case "3851":
                                                return "gfx/order/image_3d/pant/pockets/slanted_pan_pocck_preview.png";

                                            case "3852":
                                                return "gfx/order/image_3d/pant/pockets/stan_welt_pant_preview.png";

                                            case "3853":
                                                return "gfx/order/image_3d/pant/pockets/welt_stri_pan_preview.png";

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "gfx/order/image_3d/pant/none.png"
            });

            this.selectedSuitPantBackPockets = ko.computed(function () {
                var style = 'gfx/order/image_3d/pant/none.png';


                for (var category of self.selectedGarment.categories) {
                    if (category.id == '61') {
                        for (var option of category.options) {
                            if (option.id == '217' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "3854":
                                                style = "gfx/order/image_3d/pant/back/double_pocket_preview.png";
                                                break;
                                            case "3855":
                                                style = "gfx/order/image_3d/pant/back/double_button_preview.png";
                                                break;
                                            case "3856":
                                                style = "gfx/order/image_3d/pant/back/modern_preview.png";
                                                break;
                                            case "3857":
                                                style = "gfx/order/image_3d/pant/back/modern_button_preview.png";
                                                break;
                                            case "3858":
                                                style = "gfx/order/image_3d/pant/back/no_pocket_preview.png";
                                                break;
                                            case "3859":
                                                style = "gfx/order/image_3d/pant/back/single_pocket_preview.png";
                                                break;
                                            case "3860":
                                                style = "gfx/order/image_3d/pant/back/single_button_preview.png";
                                                break;
                                            case "3861":
                                                style = "gfx/order/image_3d/pant/back/square_no_flap_preview.png";
                                                break;
                                            case "3862":
                                                style = "gfx/order/image_3d/pant/back/square_flap_preview.png";
                                                break;

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }
                    }
                }

                // when "no pocket" is selected, no need to get "positions"
                if (style == "gfx/order/image_3d/pant/none.png" || style == "gfx/order/image_3d/pant/back/no_pocket_preview.png"){
                    return style;
                }

                // now get position of pocket
                const backPocketCategory = self.selectedGarment.categories.filter(el => el.id == '61')[0];
                const positionOption = backPocketCategory ? backPocketCategory.options.filter(el => el.id == '219')[0] : undefined;
                if (positionOption && positionOption.selectedValue()) {

                    switch (positionOption.selectedValue().id) {

                        case "3863":
                            // nothing to do
                            return style;
                        case "3864":
                            style = style.replace(".png", "-left.png")
                            return style;
                        case "3865":
                            style = style.replace(".png", "-right.png")
                            return style;

                        default:
                            break;

                    }
                }

                return style;


            });

            this.selectedSuitPantBeltAndCuffs = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '62') {
                        for (var option of category.options) {
                            if (option.id == '220' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "3866":
                                                return "gfx/order/image_3d/pant/belt_loop/waist_lop_preview.png";
                                            case "3867":
                                                return "gfx/order/image_3d/pant/belt_loop/but_adj_belt_oop_preview.png";
                                            case "3868":
                                                return "gfx/order/image_3d/pant/belt_loop/cross_preview.png";
                                            case "3869":
                                                return "gfx/order/image_3d/pant/belt_loop/mode_lo_pannm_preview.png";
                                            case "3870":
                                                return "gfx/order/image_3d/pant/belt_loop/double_angle_preview.png";
                                            case "3871":
                                                return "gfx/order/image_3d/pant/belt_loop/dobblooe_pant_loup_preview.png";
                                            case "3872":
                                                return "gfx/order/image_3d/pant/none.png";
                                            case "3873":
                                                return "gfx/order/image_3d/pant/belt_loop/sinnggle_pant_llop_preview.png";

                                            default:
                                                break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "gfx/order/image_3d/pant/none.png"
            });

            this.selectedSuitPantBeltAndCuffsBack = ko.computed(function(){
                return "";
            });

            this.selectedSuitPantCuffs = ko.computed(function(){
                // get belt & cuff category
                const cuffCategory = self.selectedGarment.categories.filter(cat => cat.id == "62")[0];
                if (!cuffCategory) return "";

                // get cuff option
                const cuffOption = cuffCategory.options.filter(el => el.id == "222")[0];
                if (!cuffOption) return "";


                if (!cuffOption.selectedValue()){
                    // no cuff
                    return "";
                }

                // get pant style
                const pantStyle = self.selectedSuitPantStyle();

                switch (pantStyle) {
                    case "gfx/order/image_3d/pant/fit/boot_fl_pan_preview.png":
                        return "gfx/order/image_3d/pant/cuffs/cuff_boot.png";
                    case "gfx/order/image_3d/pant/fit/narr_po_pant_preview.png":
                        return "gfx/order/image_3d/pant/cuffs/cuff_narrow.png";
                    case "gfx/order/image_3d/pant/fit/nom_str_pan_preview.png":
                        return "gfx/order/image_3d/pant/cuffs/cuff_norm.png";
                    default:
                        return "";
                }

            });

            this.selectedSuitPantCuffsBack = ko.computed(function(){
                return "";
            });
        }


    }
});
