define(['jquery', 'knockout', 'base', 'model/image3D/Image3D', 'model/image3D/Image3D_v1'], function ($, ko) {

    /**
    * New 3D!!
    * For now we are extending from Image3D_v1 (old 3D) because we do not have all images yet. So we are importing the old methods
    *
    * For those images we already have, we just need to overwrite the method with the new images
    */

    // Image3D_v2 = class Image3D_v2 extends Image3D{
    Image3D_v2 = class Image3D_v2 extends Image3D_v1{
        constructor(selectedGarment){
            super(selectedGarment);

            this.rootFolder = window.rootFolderImage3D_v2;

            this.defaultFabric = "21102-2";
            this.fabric = ko.observable(this.defaultFabric);

            // use old code while new 3d is incomplete
            // // this.image3D_v1 = new Image3D_v1(selectedGarment);

            var self = this;

            // ██████╗  █████╗ ███╗   ██╗████████╗███████╗
            // ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
            // ██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
            // ██╔═══╝ ██╔══██║██║╚██╗██║   ██║   ╚════██║
            // ██║     ██║  ██║██║ ╚████║   ██║   ███████║
            // ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
            //

            this.pantsDefaultImage = ko.computed(function () {
                return [
                    // `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/normal_straight.png`,
                    `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/waistband/normal.png`
                ]
            });


            this.pantsDefaultImageBack = ko.computed(function () {
                return [
                    `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/body.png`,
                    `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/waistband/waistband_edge.png`,
                ]
            });

            this.selectedPantStyle = ko.computed(function () {

                const selectedPantsStyleID = self.getSelectedPantStyleId();

                switch (selectedPantsStyleID) {
                    case "1254":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/boot_flare.png`;

                    case "1255":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/slim.png`;

                    case "1256":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/normal_straight.png`;

                    default:
                    break;
                }

                return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/normal_straight.png`;
            });

            this.selectedPantPleatStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '15') {
                        for (var option of category.options) {
                            if (option.id == '59' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1257": // double_pleats
                                            return `${self.rootFolder}/Garments/pants/pleats/double_pleats.png`;

                                            case "1258": // no_pleats
                                            return ""

                                            case "1259": // single_pleats
                                            return `${self.rootFolder}/Garments/pants/pleats/single_pleats.png`;

                                            case "1260": // triple_pleats
                                            return `${self.rootFolder}/Garments/pants/pleats/triple_pleats.png`;

                                            default:
                                            break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return ""
            });

            this.selectedPantFrontPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '16') {
                        for (var option of category.options) {
                            if (option.id == '61' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "1261": // Custom Pocket
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/slanted.png`;

                                            case "1262": // Jeans
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/jeans.png`;

                                            case "1263": // Modern Curved
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/modern_curved.png`;

                                            case "1264": // no pocket
                                            return "";

                                            case "1265": // Slanted
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/slanted.png`;

                                            case "1266": // Slanted welt
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/slanted_welt.png`;

                                            case "1267": // Straight Welt
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/straight_welt.png`;

                                            default:
                                            break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            });

            this.selectedPantBackPockets = ko.computed(function () {
                var style = "";

                // pocket category
                const pocketCategory = self.selectedGarment.categories.find(category => category.id == "17");
                if (!pocketCategory) return style;

                // position option
                const positionOption = pocketCategory.options.find(option => option.id == "65");
                if (!positionOption) return style;

                // get position (left/right/both)
                const positionValue = positionOption.selectedValue() ? positionOption.selectedValue().id : null;
                let positionName = "both";

                switch (positionValue) {
                    case "1277":
                    positionName = "both";
                    break;
                    case "1278":
                    positionName = "left";
                    break;
                    case "1279":
                    positionName = "right";
                    break;
                }

                // get style of pocket
                for (var option of pocketCategory.options) {
                    if (option.id == '63' && option.values) {
                        for (var value of option.values) {
                            if (value.selected() == true) {
                                switch (value.id) {

                                    case "1268": // "Double"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/double_opening.png`;
                                    break;
                                    case "1269": // "Double w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/double_opening.png`;
                                    break;
                                    case "1270": // "Modern Flap no Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/modern_flap_with_button.png`;
                                    break;
                                    case "1271": // "Modern Flap w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/modern_flap_with_button.png`;
                                    break;
                                    case "1272": // "No Pocket Opening"
                                    style = "";
                                    break;
                                    case "1273": // "Single"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/single_opening.png`;
                                    break;
                                    case "1274": // "Single w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/single_opening.png`;
                                    break;
                                    case "1275": // "Square Flap no Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/square_flap_with_button.png`;
                                    break;
                                    case "1276": // "Square Flap w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/square_flap_with_button.png`;

                                    break;

                                    default:
                                    break;

                                }
                            }
                        }

                    }
                }



                return style;

            });

            this.selectedPantBeltAndCuffs = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '18') {
                        for (var option of category.options) {
                            if (option.id == '66' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1280": // "Buckle Side Adjusters"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/none+buckle_side_adjusters.png`;
                                            case "1281": // "Button Side Adjusters"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/none+button_side_adjusters.png`;;
                                            case "1282": // "Criss Cross"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/criss_cross.png`;
                                            case "1283": // "Designer"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/modern.png`;
                                            case "1284": // "Double Angled"
                                            return ` Double Angled missing<<<<`;
                                            case "1285": // "Double Straight"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/double.png`;
                                            case "1286": // "No Belt Loop"
                                            return "";
                                            case "1287": // "Single Straight"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/single.png`;

                                            default:
                                            break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            });

            this.selectedPantBeltAndCuffsBack = ko.computed(function () {
                // get belt_loop front end replace prefix
                return self.selectedPantBeltAndCuffs().replace(`${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop`, `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/belt_loop` );
            });

            this.selectedPantCuffs = ko.computed(function(){
                // get belt & cuff category
                const cuffCategory = self.selectedGarment.categories.filter(cat => cat.id == "18")[0];
                if (!cuffCategory) return "";

                // get cuff option
                const cuffOption = cuffCategory.options.filter(el => el.id == "68")[0];
                if (!cuffOption) return "";

                if (!cuffOption.selectedValue()){
                    // no cuff
                    return "";
                }

                // get cuff size (normal, large, slim)
                // default: normal
                const cuffSize = 'normal';

                // get pant style
                const selectedPantsStyleID = self.getSelectedPantStyleId();

                switch (selectedPantsStyleID) {
                    case "1254":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/cuffs/boot_flare/${cuffSize}.png`;
                    case "1255":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/cuffs/slim/${cuffSize}.png`;
                    case "1256":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/cuffs/normal/${cuffSize}.png`;
                    default:
                    return "";
                }

            });

            this.selectedPantCuffsBack = ko.computed(function(){

                if (self.selectedPantCuffs() == ""){
                    return "";
                }

                // get from front and just change the prefix
                const temp = self.selectedPantCuffs().split("/");
                return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/cuffs/${temp[temp.length-1]}`


            });

            this.selectedPantPleatsBack = ko.computed(function () {
                // see if pleats is !== 'no pleats'
                if (self.selectedPantPleatStyle() !== "") {
                    return `${self.rootFolder}/Garments/pants/back_pleats/back_pleats.png`;
                } else {
                    return "";
                }
            });

            // Jacket
            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝
            //      ██║███████║██║     █████╔╝ █████╗     ██║
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝
            //

            this.jacketDefaultImagesFront = ko.computed(function () {
                return [
                    `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/sleeves/sleeves.png`
                ]
            });

            this.jacketDefaultImagesBack = ko.computed(function () {
                return [
                    `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/sleeves/sleeves.png`,
                ]
            });


            // this.jacketDefaultImagesMonogram = ko.observableArray([
            //     ""
            // ]);

            /**
            * Auxiliary method
            */
            this._getButtonsStyleName = function(){
                let _paths = self.selectedJacketButtonStyle().split("/");
                let buttonsStyle = _paths[_paths.length-1].replace(".png", "");

                // double_buttons_blah_blah have the same lapel size
                if (buttonsStyle.includes("double")){
                    buttonsStyle = "double-breasted(2toclose)";
                }

                return buttonsStyle;
            }

            this._getLapelStyleName = function(){
                const default_lapel = "notch_lapel";

                const _lapelCategory = self.selectedGarment.categories.find(cat => cat.id == "6");
                if (!_lapelCategory) return default_lapel;
                const _lapelOption = _lapelCategory.options.find(op => op.id == "12");
                if (!_lapelOption) return default_lapel;
                const _lapelSelected = _lapelOption.values.find(val => val.selected())
                let lapelStyleId = _lapelSelected ? _lapelSelected.id : "32"; // notch as default
                let lapelStyleFoldName = "";
                switch (lapelStyleId) {
                    case "31": // Custom Lapel
                    lapelStyleFoldName = "notch_lapel";
                    break;
                    case "32": // Notch Lapel
                    lapelStyleFoldName = "notch_lapel";
                    break;
                    case "33": // Peak Lapel
                    lapelStyleFoldName = "peak_lapel";
                    break;
                    case "34": // Shawl Lapel
                    lapelStyleFoldName = "shawl_lapel";
                    break;
                    default:
                    lapelStyleFoldName = default_lapel;
                    break;
                }

                return lapelStyleFoldName;
            }

            this.selectedJacketButtonStyle = ko.computed(function () {

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '1') {
                        for (var option of category.options) {
                            if (option.id == '1' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "1": // Custom
                                            return `${self.rootFolder}/Garments/jacket/buttons/1_button.png`;
                                            case "2": // Double breasted
                                            return `${self.rootFolder}/Garments/jacket/buttons/double_breasted.png`;
                                            case "3": // Double Breasted(one to close)
                                            return `${self.rootFolder}/Garments/jacket/buttons/double_breasted_4buttons(1toclose).png`;
                                            case "4": // Double breasted(six buttons)
                                            return `${self.rootFolder}/Garments/jacket/buttons/double-breasted(2toclose).png`;
                                            case "5": // Four Button
                                            return `${self.rootFolder}/Garments/jacket/buttons/4_buttons.png`;
                                            case "6": // One buttom
                                            return `${self.rootFolder}/Garments/jacket/buttons/1_button.png`;
                                            case "7": // Three Button
                                            return `${self.rootFolder}/Garments/jacket/buttons/3_buttons.png`;
                                            case "8": // Two Button
                                            return `${self.rootFolder}/Garments/jacket/buttons/2_buttons.png`;


                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/buttons/1_button.png`;
            });

            this.selectedJacketButtonsBHStyle = ko.computed(function () {
                // get buttons image and get only the style
                const style = self.selectedJacketButtonStyle().replace(`${self.rootFolder}/Garments/jacket/buttons/`, "").replace(".png", "");

                // get colour
                const lapelCategory = self.selectedGarment.categories.find(category => category.id == "6");
                if (!lapelCategory) {return ""};

                const bhColour_option = lapelCategory.options.find(option => option.id == "15");
                // const color = self.getBHProperName(bhColour_option.selectedValue());
                // if (color === ""){
                //     return ""; // hide when default colour
                // }
                const color = "a6"; // do not change bh colors for structure anymore

                const default_path = `${self.rootFolder}/Extras/jacket/body_buttons/center button_threads/${style}/${color}.png`
                return default_path;
            });

            this.selectedJacketBottom = ko.computed(function () {

                // depends on button style
                let buttonsStyle = self._getButtonsStyleName();

                // also depends on lapel style
                // const lapelStyleFoldName = self._getLapelStyleName();
                const lapelStyleFoldName = "notch_lapel"; // for now we only have notch lapel bodies (waiting for Dimitraki)

                const default_body = `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/curved/${buttonsStyle}.png`;

                // double buttons exception
                if (buttonsStyle.toLowerCase().includes("double")){
                    return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/double_breasted/double-breasted.png`
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '4') {
                        for (var option of category.options) {
                            if (option.id == '7' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "24": // Curved Bottom
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/curved/${buttonsStyle}.png`;
                                            case "25": // Custom Bottom
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/curved/${buttonsStyle}.png`;
                                            case "26": // Straight Bottom
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/straight/${buttonsStyle}.png`;

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // default option
                return default_body;


            });

            this.selectedJacketBottom2 = ko.computed(function () {
                // depends on button style
                let buttonsStyle = self._getButtonsStyleName();
                if (!buttonsStyle.toLowerCase().trim().includes("double")){
                    return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/j_bottom_hole/j_bottom_hole.png`;
                }
                else {
                    return "";
                }

            });

            this.selectedJacketLapel = ko.computed(function () {

                // depends on button style
                let buttonsStyle = self._getButtonsStyleName();

                // lapel style
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '6') {
                        for (var option of category.options) {
                            if (option.id == '12' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "31": //	 Custom Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/notch_lapel/${buttonsStyle}.png`;
                                            case "32": //	 Notch Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/notch_lapel/${buttonsStyle}.png`;
                                            case "33": //	 Peak Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/peak_lapel/${buttonsStyle}.png`;
                                            case "34": //	 Shawl Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/shawl_lapel/${buttonsStyle}.png`;
                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/notch_lapel/${buttonsStyle}.png`;

            });

            this.selectedJacketLapelShadow = ko.computed(function () {

                // depends on Lapel style
                let lapelShape = self.selectedJacketLapel().replace(`fabrics/${self.fabric()}/lapel`, "shadows");

                return lapelShape;

            });


            this.selectedJacketSleeve                = ko.computed(function () { return "" });

            // jacket sleeve (back) start  ===================================================

            this.selectedJacketSleeveButton1 = ko.computed(function () {
                // for now we only have default colours
                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/1st.png`
            });
            this.selectedJacketSleeveButton2 = ko.computed(function () {
                if (self.selectedJacketNumberOfButtonsSleeve() < 2) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/2nd.png`
            });
            this.selectedJacketSleeveButton3 = ko.computed(function () {
                if (self.selectedJacketNumberOfButtonsSleeve() < 3) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/3rd.png`
            });
            this.selectedJacketSleeveButton4 = ko.computed(function () {
                if (self.selectedJacketNumberOfButtonsSleeve() < 4) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/4th.png`
            });
            this.selectedJacketSleeveButton5 = ko.computed(function () {
                if (self.selectedJacketNumberOfButtonsSleeve() < 5) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/5th.png`
            });

            this.selectedJacketSleeveButton1BH  = ko.computed(function () {

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const BH1path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/1st/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "7");
                if (!sleeveCategory) return "";

                const optionBhColor1 = sleeveCategory.options.find(option => option.id == "43");
                if (!optionBhColor1) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor1.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH1path}/${image}.png`
            });
            this.selectedJacketSleeveButton2BH = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 2){
                    return "";
                }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const BH2path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/2nd/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "7");
                if (!sleeveCategory) return "";

                const optionBhColor2 = sleeveCategory.options.find(option => option.id == "44");
                if (!optionBhColor2) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor2.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH2path}/${image}.png`
            });
            this.selectedJacketSleeveButton3BH  = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 3){
                    return "";
                }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const BH3path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/3rd/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "7");
                if (!sleeveCategory) return "";

                const optionBhColor3 = sleeveCategory.options.find(option => option.id == "45");
                if (!optionBhColor3) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor3.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH3path}/${image}.png`
            });
            this.selectedJacketSleeveButton4BH  = ko.computed(function () {
                if (self.selectedJacketNumberOfButtonsSleeve() < 4){
                    return "";
                }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const BH4path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/4th/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "7");
                if (!sleeveCategory) return "";

                const optionBhColor4 = sleeveCategory.options.find(option => option.id == "46");
                if (!optionBhColor4) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor4.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH4path}/${image}.png`
            });
            this.selectedJacketSleeveButton5BH  = ko.computed(function () {

                if (self.selectedJacketNumberOfButtonsSleeve() < 5){
                    return "";
                }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const BH5path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/5th/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "7");
                if (!sleeveCategory) return "";

                const optionBhColor5 = sleeveCategory.options.find(option => option.id == "47");
                if (!optionBhColor5) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor5.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH5path}/${image}.png`
            });

            // jacket sleeve (back) end    ===================================================

            // jacket sleeve (zoom) start  ===================================================

            // sleeve view
            this.jacketDefaultImagesSleeve = ko.computed(function () {
                return [
                    `${self.rootFolder}/Extras/jacket/zoom_buttons/sleeve_buttons_zoom.png`
                ]
            });


            this.selectedJacketSleeveButton1Zoom    = ko.computed(function () {
                return self.selectedJacketSleeveButton1().replace("sleeve_buttons", "zoom_buttons")
            });
            this.selectedJacketSleeveButton2Zoom    = ko.computed(function () {
                return self.selectedJacketSleeveButton2().replace("sleeve_buttons", "zoom_buttons")
            });
            this.selectedJacketSleeveButton3Zoom    = ko.computed(function () {
                return self.selectedJacketSleeveButton3().replace("sleeve_buttons", "zoom_buttons")
            });
            this.selectedJacketSleeveButton4Zoom    = ko.computed(function () {
                return self.selectedJacketSleeveButton4().replace("sleeve_buttons", "zoom_buttons")
            });
            this.selectedJacketSleeveButton5Zoom    = ko.computed(function () {
                return self.selectedJacketSleeveButton5().replace("sleeve_buttons", "zoom_buttons")
            });


            this.selectedJacketSleeveBHColour1 = ko.computed(function() {
                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "43")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/1st/ButtonHole/${name}.png`;


            });
            this.selectedJacketSleeveBHColour2 = ko.computed(function() {
                if (self.selectedJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "44")[0];
                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/2nd/ButtonHole/${name}.png`;

            });
            this.selectedJacketSleeveBHColour3 = ko.computed(function() {
                if (self.selectedJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "45")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/3rd/ButtonHole/${name}.png`;
            });
            this.selectedJacketSleeveBHColour4 = ko.computed(function() {
                if (self.selectedJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "46")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/4th/ButtonHole/${name}.png`;
            });
            this.selectedJacketSleeveBHColour5 = ko.computed(function() {
                if (self.selectedJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "47")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/5th/ButtonHole/${name}.png`;
            });



            this.selectedJacketSleeveThread1 = ko.computed(function(){
                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "48")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/1st/ButtonThread/${name}.png`;
            });
            this.selectedJacketSleeveThread2 = ko.computed(function(){
                if (self.selectedJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }


                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "49")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/2nd/ButtonThread/${name}.png`;
            });
            this.selectedJacketSleeveThread3 = ko.computed(function(){
                if (self.selectedJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "50")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/3rd/ButtonThread/${name}.png`;
            });
            this.selectedJacketSleeveThread4 = ko.computed(function(){
                if (self.selectedJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "51")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/4th/ButtonThread/${name}.png`;
            });
            this.selectedJacketSleeveThread5 = ko.computed(function(){
                if (self.selectedJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "7")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "52")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/5th/ButtonThread/${name}.png`;
            });

            // jacket sleeve (zoom) end    ===================================================



            this.selectedJacketPocket                = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '8') {
                        for (var option of category.options) {
                            if (option.id == '33' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "230": // Back Curved
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/back_curved.png`;
                                            case "231": // Batman
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/batman.png`;
                                            case "232": // Classic
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/classic_pocket.png`;
                                            case "233": // Front Curved
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/front_curved.png`;
                                            case "234": // Patch Pocket
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/patch_pocket.png`;
                                            case "235": // Slightly Slanted
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/slightly.png`;
                                            case "236": // Welt Pocket
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/welt_pocket.png`;
                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/classic_pocket.png`;
            });

            this.selectedJacketTicketPocket          = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '8') {
                        for (var option of category.options) {
                            if (option.id == '34' && option.selectedValue()) {
                                return self.selectedJacketPocket().replace(".png", "-ticket.png");
                            }
                        }
                    }
                }

                return "";
            });

            this.selectedJacketBreastPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '9') {
                        for (var option of category.options) {
                            if (option.id == '36' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "238": // Patch Pocket
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/breastpocket/patch_pocket.png`;
                                            case "237": // No Breastpocket
                                            return "";
                                            case "240": // Standard Straight
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/breastpocket/standard_straight.png`;
                                            case "239": // Standard Angled
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/breastpocket/standard_angled.png`;
                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });


            this.stitchForPocket                     = ko.computed(function () {
                const lapelDetailsCategory = self.selectedGarment.categories.find(category => category.id == "11");
                if (!lapelDetailsCategory) return "";

                // stitch pocket is on?
                const stitchPocketOption = lapelDetailsCategory.options.find(option=>option.id == "26");
                if (!stitchPocketOption) return "";
                if (!stitchPocketOption.selectedValue()) return "";


                // get pocket format
                const temp = self.selectedJacketPocket().split("/");
                let pocketFormat = temp[temp.length-1].replace(".png", "");

                // Welt Pocket has no stitch
                if (pocketFormat.toLowerCase().includes("welt")) return "";

                // has ticket?
                const has_ticket = self.selectedJacketTicketPocket().includes("ticket");

                // get stich colour
                const stitchPocketColourOption = lapelDetailsCategory.options.find(option=>option.id == "27");
                if (!stitchPocketColourOption) return "";

                const stitchColour = stitchPocketColourOption.selectedValue() ? self.getBHProperName(stitchPocketColourOption.selectedValue()) : "a1";
                if (stitchColour === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/Extras/jacket/pockets_top_stitch/${pocketFormat}${has_ticket ? "-ticket":""}/${stitchColour}.png`;

            });

            this.stitchForLapel                      = ko.computed(function () {
                // check if stich option is on
                const lapelOptionsCategory = self.selectedGarment.categories.find(cat => cat.id == "11");
                if (!lapelOptionsCategory) return "";

                const stichLapelOption = lapelOptionsCategory.options.find(option => option.id == "25");
                if (!stichLapelOption) return "";

                if (!stichLapelOption.selectedValue()) return "";



                // get selected jacket style and size
                const jacketLapelStyle = self._getLapelStyleName();
                const jacketLapelButtonsStyle = self._getButtonsStyleName();


                // get selected trim color
                const stichColourOption = lapelOptionsCategory.options.find(option => option.id == "27");
                if (!stichColourOption) return "";

                let stitchColour = stichColourOption.selectedValue().name.replace(" ", "").toLowerCase();
                if (!stitchColour || stitchColour === "default") {
                    stitchColour = "a1"; // default
                }

                return `${self.rootFolder}/Extras/jacket/lapel_top_stitch/${jacketLapelStyle}/${jacketLapelButtonsStyle}/${stitchColour}.png`;
            });

            this.bh = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '6') {
                        const bhColour_option = category.options.find(option => option.id == "15");
                        const color = self.getBHProperName(bhColour_option.selectedValue());
                        if (color === ""){
                            return ""; // hide when default colour
                        }

                        for (var option of category.options) {
                            if (option.id == '14' && option.selectedValue()) {
                                return `${self.rootFolder}/Extras/jacket/lapel_buttonhole/${color}.png`;
                            }
                        }
                    }
                }

                return "";

            });

            // this.selectedJacketLiningColor           = ko.computed(function () { return ""; }); // while 3D is incomplete

            this.selectedJacketLiningColorFront      = ko.computed(function () { return `${self.rootFolder}/Garments/jacket/linning/linning.png`; });


            this.selectedJacketLapelTrimming         = ko.computed(function () {

                const lapelStyleFoldName = self._getLapelStyleName();
                const buttonName = self._getButtonsStyleName();

                if (!self.jacketTuxedoIsOn()){
                    return "";
                }

                // check if lapel tuxedo is on
                // check if tuxedo collar is on
                var lapelDetailsCategory = self.selectedGarment.categories.filter(cat=>cat.id=="11")[0];
                if(!lapelDetailsCategory){ return ""; }

                let lapelOption = lapelDetailsCategory.options.filter(opt => opt.id=="17")[0];
                let lapelOptionEdge = lapelDetailsCategory.options.filter(opt => opt.id=="19")[0];

                if (lapelOption && lapelOption.selectedValue()) {
                    // check if edging is on
                    if (lapelOptionEdge && lapelOptionEdge.selectedValue()) {
                        // check current size of lapel and change image folder
                        return `${self.rootFolder}/Extras/jacket/edging/${lapelStyleFoldName}/${buttonName}.png`;
                    } else {
                        // check current size of lapel and change image folder
                        return `${self.rootFolder}/Garments/jacket/tuxedo/${lapelStyleFoldName}/${buttonName}/${buttonName}_main.png`;
                    }
                }
                return "";

            });

            this.selectedJacketCollarTrimming        = ko.computed(function () {
                let upperImage = `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/upper.png`;

                if (!self.jacketTuxedoIsOn()){
                    return upperImage;
                }

                // check if tuxedo collar is on
                var lapelDetailsCategory = self.selectedGarment.categories.filter(cat=>cat.id=="11")[0];
                if(!lapelDetailsCategory){ return upperImage }

                let collarOption = lapelDetailsCategory.options.filter(opt => opt.id=="18")[0];

                if (collarOption && collarOption.selectedValue()){
                    return `${self.rootFolder}/Garments/jacket/tuxedo/tuxedo_upper.png`;
                }

                return upperImage;
            });

            this.selectedJacketCollarUpperTrimming = ko.computed(function() {
                const lapelStyleFoldName = self._getLapelStyleName();
                const buttonName = self._getButtonsStyleName();

                if (lapelStyleFoldName.toLocaleLowerCase().includes('shawl')){
                    return "";
                }

                if (self.selectedJacketCollarTrimming().includes('tuxedo')){
                    return `${self.rootFolder}/Garments/jacket/tuxedo/${lapelStyleFoldName}/${buttonName}/${buttonName}_upper.png`;
                }
                else {
                    return "";
                }

            });

            this.selectedJacketButtonsTrimming       = ko.computed(function () {

                // lapelOptionCategory
                var lapelDetailsCategory = self.selectedGarment.categories.filter(cat=>cat.id=="11")[0];
                if(!lapelDetailsCategory){ return "" }

                // buttons tuxedo is on?
                const tuxedoButtons = lapelDetailsCategory.options.find(option => option.id == "23");
                if (!tuxedoButtons) {return ""}

                // get jacket style and rename prefix folder
                if(tuxedoButtons && tuxedoButtons.selectedValue()){
                    return self.selectedJacketButtonStyle().replace("/buttons/", "/tuxedo/center_buttons/");
                }

                return "";

            });
            this.selectedJacketTicketPocketTrimming  = ko.computed(function () { return ""; }); // while 3D is incomplete
            this.selectedJacketPocketTrimming        = ko.computed(function () { return ""; }); // while 3D is incomplete

            this.selectedJacketVent = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '5') {
                        for (var option of category.options) {
                            if (option.id == '9' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "27": // Custom Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/no_vent.png`;
                                            case "28": // No Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/no_vent.png`;
                                            case "29": // One Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/center_vent_opened.png`;
                                            case "30": // Two Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/side_vents.png`;

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/no_vent.png`;
            });

            this.jacketVent2 = ko.computed(function(){
                // depends on jacket vent

                if (self.selectedJacketVent().toLowerCase().includes("opened") && self.selectedJacketVent().toLowerCase().includes("center") ){
                    return `${self.rootFolder}/Garments/jacket/vent/center/lining.png`
                }

                else if (self.selectedJacketVent().toLowerCase().includes("side") ){
                    return `${self.rootFolder}/Garments/jacket/vent/side/lining.png`
                }

                return "";
            });

            this.selectedJacketBackUpper = ko.computed(function() {
                const default_lapel = `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/upper/normal.png`;

                // check if tuxedo lapel is on
                const tuxedos_cat = self.selectedGarment.categories.find(cat => cat.id == "11");
                if (!tuxedos_cat) return default_lapel;
                const collar_option = tuxedos_cat.options.find(option => option.id == "18");
                if (!collar_option) return default_lapel;

                if (collar_option.selectedValue()){
                    return `${self.rootFolder}/Garments/jacket/tuxedo/tuxedo_upper_back.png`;
                } else{
                    return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/upper/normal.png`
                }
            });

            // Suit
            // ███████╗██╗   ██╗██╗████████╗
            // ██╔════╝██║   ██║██║╚══██╔══╝
            // ███████╗██║   ██║██║   ██║
            // ╚════██║██║   ██║██║   ██║
            // ███████║╚██████╔╝██║   ██║
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝
            //

            this.suitJacketDefaultImagesFront = this.jacketDefaultImagesFront;
            this.suitJacketDefaultImagesBack = this.jacketDefaultImagesBack;

            /**
            * Auxiliary method
            */
            this._getSuitButtonsStyleName = function(){
                let _paths = self.selectedSuitJacketButtonStyle().split("/");
                let buttonsStyle = _paths[_paths.length-1].replace(".png", "");

                // double_buttons_blah_blah have the same lapel size
                if (buttonsStyle.includes("double")){
                    buttonsStyle = "double-breasted(2toclose)";
                }

                return buttonsStyle;
            }

            this._getSuitLapelStyleName = function(){
                const default_lapel = "notch_lapel";

                const _lapelCategory = self.selectedGarment.categories.find(cat => cat.id == "52");
                if (!_lapelCategory) return default_lapel;
                const _lapelOption = _lapelCategory.options.find(op => op.id == "164");
                if (!_lapelOption) return default_lapel;
                const _lapelSelected = _lapelOption.values.find(val => val.selected())
                let lapelStyleId = _lapelSelected ? _lapelSelected.id : "32"; // notch as default
                let lapelStyleFoldName = "";
                switch (lapelStyleId) {
                    case "2613": // Custom Lapel
                    lapelStyleFoldName = "notch_lapel";
                    break;
                    case "2614": // Notch Lapel
                    lapelStyleFoldName = "notch_lapel";
                    break;
                    case "2615": // Peak Lapel
                    lapelStyleFoldName = "peak_Lapel";
                    break;
                    case "2616": // Shawl Lapel
                    lapelStyleFoldName = "shawl_lapel";
                    break;
                    default:
                    lapelStyleFoldName = default_lapel;
                    break;
                }

                return lapelStyleFoldName;
            }

            this.selectedSuitJacketButtonStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '49') {
                        for (var option of category.options) {
                            if (option.id == '148' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2583": // Custom
                                            return `${self.rootFolder}/Garments/jacket/buttons/1_button.png`;
                                            case "2584": // Double breasted
                                            return `${self.rootFolder}/Garments/jacket/buttons/double_breasted.png`;
                                            case "2585": // Double Breasted(one to close)
                                            return `${self.rootFolder}/Garments/jacket/buttons/double_breasted_4buttons(1toclose).png`;
                                            case "2586": // Double breasted(six buttons)
                                            return `${self.rootFolder}/Garments/jacket/buttons/double-breasted(2toclose).png`;
                                            case "2587": // Four Button
                                            return `${self.rootFolder}/Garments/jacket/buttons/4_buttons.png`;
                                            case "2588": // One buttom
                                            return `${self.rootFolder}/Garments/jacket/buttons/1_button.png`;
                                            case "2589": // Three Button
                                            return `${self.rootFolder}/Garments/jacket/buttons/3_buttons.png`;
                                            case "2590": // Two Button
                                            return `${self.rootFolder}/Garments/jacket/buttons/2_buttons.png`;


                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/buttons/1_button.png`;
            });

            this.selectedSuitJacketButtonsBHStyle = ko.computed(function () {
                // get buttons image and get only the style
                const style = self.selectedSuitJacketButtonStyle().replace(`${self.rootFolder}/Garments/jacket/buttons/`, "").replace(".png", "");

                // get colour
                const lapelCategory = self.selectedGarment.categories.find(category => category.id == "52")
                if (!lapelCategory) return "";

                const bhColour_option = lapelCategory.options.find(option => option.id == "167");

                // const color = self.getBHProperName(bhColour_option.selectedValue());
                // if (color === ""){
                //     return ""; // hide when default colour
                // }
                const color = "a6"; // do not change bh colors for structure anymore


                const default_path = `${self.rootFolder}/Extras/jacket/body_buttons/center button_threads/${style}/${color}.png`
                return default_path;
            });

            this.suitJacketDefaultImagesSleeve = this.jacketDefaultImagesSleeve;

            this.selectedSuitJacketSleeve = ko.computed(function(){ return ""; });



            // jacket sleeve (back) start  ===================================================


            this.selectedSuitJacketSleeveButton1 = ko.computed(function () {
                // for now we only have default colours
                const working_or_kissing = self.jacketSleeveKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/1st.png`
            });
            this.selectedSuitJacketSleeveButton2 = ko.computed(function () {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/2nd.png`
            });
            this.selectedSuitJacketSleeveButton3 = ko.computed(function () {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/3rd.png`
            });
            this.selectedSuitJacketSleeveButton4 = ko.computed(function () {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/4th.png`
            });
            this.selectedSuitJacketSleeveButton5 = ko.computed(function () {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5) {
                    return "";
                }

                // for now we only have default colours
                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";
                return `${self.rootFolder}/Extras/jacket/sleeve_buttons/${working_or_kissing}/buttons/5th.png`
            });


            this.selectedSuitJacketSleeveButton1BH  = ko.computed(function () {

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const BH1path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/1st/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "54");
                if (!sleeveCategory) return "";

                const optionBhColor1 = sleeveCategory.options.find(option => option.id == "178");
                if (!optionBhColor1) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor1.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH1path}/${image}.png`
            });
            this.selectedSuitJacketSleeveButton2BH = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2){
                    return "";
                }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const BH2path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/2nd/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "54");
                if (!sleeveCategory) return "";

                const optionBhColor2 = sleeveCategory.options.find(option => option.id == "179");
                if (!optionBhColor2) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor2.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH2path}/${image}.png`
            });
            this.selectedSuitJacketSleeveButton3BH  = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3){
                    return "";
                }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const BH3path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/3rd/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "54");
                if (!sleeveCategory) return "";

                const optionBhColor3 = sleeveCategory.options.find(option => option.id == "180");
                if (!optionBhColor3) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor3.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH3path}/${image}.png`
            });
            this.selectedSuitJacketSleeveButton4BH  = ko.computed(function () {

                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4){
                    return "";
                }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const BH4path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/4th/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "54");
                if (!sleeveCategory) return "";

                const optionBhColor4 = sleeveCategory.options.find(option => option.id == "224");
                if (!optionBhColor4) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor4.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH4path}/${image}.png`
            });
            this.selectedSuitJacketSleeveButton5BH  = ko.computed(function () {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5){
                    return "";
                }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const BH5path = `Extras/jacket/sleeve_buttons/${working_or_kissing}/5th/ButtonHole`;

                const sleeveCategory = self.selectedGarment.categories.find(cat => cat.id == "54");
                if (!sleeveCategory) return "";

                const optionBhColor5 = sleeveCategory.options.find(option => option.id == "181");
                if (!optionBhColor5) return "";

                // get proper name of the image
                const image = self.getBHProperName(optionBhColor5.selectedValue());
                if (image === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/${BH5path}/${image}.png`
            });

            // jacket sleeve (back) end    ===================================================

            // jacket sleeve (zoom) start  ===================================================


            this.selectedSuitJacketSleeveButton1Zoom    = ko.computed(function () {
                return self.selectedSuitJacketSleeveButton1().replace("sleeve_buttons", "zoom_buttons");
            });
            this.selectedSuitJacketSleeveButton2Zoom    = ko.computed(function () {
                return self.selectedSuitJacketSleeveButton2().replace("sleeve_buttons", "zoom_buttons");
            });
            this.selectedSuitJacketSleeveButton3Zoom    = ko.computed(function () {
                return self.selectedSuitJacketSleeveButton3().replace("sleeve_buttons", "zoom_buttons");
            });
            this.selectedSuitJacketSleeveButton4Zoom    = ko.computed(function () {
                return self.selectedSuitJacketSleeveButton4().replace("sleeve_buttons", "zoom_buttons");
            });
            this.selectedSuitJacketSleeveButton5Zoom    = ko.computed(function () {
                return self.selectedSuitJacketSleeveButton5().replace("sleeve_buttons", "zoom_buttons");
            });


            this.selectedSuitJacketSleeveBHColour1 = ko.computed(function() {
                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "178")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/1st/ButtonHole/${name}.png`;


            });
            this.selectedSuitJacketSleeveBHColour2 = ko.computed(function() {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const bhOption = sleeveCategory.options.filter(option => option.id == "179")[0];
                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/2nd/ButtonHole/${name}.png`;

            });
            this.selectedSuitJacketSleeveBHColour3 = ko.computed(function() {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "180")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/3rd/ButtonHole/${name}.png`;
            });
            this.selectedSuitJacketSleeveBHColour4 = ko.computed(function() {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "224")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/4th/ButtonHole/${name}.png`;
            });
            this.selectedSuitJacketSleeveBHColour5 = ko.computed(function() {
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const bhOption = sleeveCategory.options.filter(option => option.id == "181")[0];

                const name = self.getBHProperName(bhOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/5th/ButtonHole/${name}.png`;
            });



            this.selectedSuitJacketSleeveThread1 = ko.computed(function(){
                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "182")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/1st/ButtonThread/${name}.png`;
            });
            this.selectedSuitJacketSleeveThread2 = ko.computed(function(){
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 2) {
                    return ""
                }


                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "183")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/2nd/ButtonThread/${name}.png`;
            });
            this.selectedSuitJacketSleeveThread3 = ko.computed(function(){
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 3) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "184")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/3rd/ButtonThread/${name}.png`;
            });
            this.selectedSuitJacketSleeveThread4 = ko.computed(function(){
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 4) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "185")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/4th/ButtonThread/${name}.png`;
            });
            this.selectedSuitJacketSleeveThread5 = ko.computed(function(){
                if (self.selectedSuitJacketNumberOfButtonsSleeve() < 5) {
                    return ""
                }

                const sleeveCategory = self.selectedGarment.categories.filter(category => category.id == "54")[0];
                // not a jacket
                if (!sleeveCategory){ return ""; }

                const working_or_kissing = self.suitJacketIsKissing() ? "kissing" : "working";

                const threadOption = sleeveCategory.options.filter(option => option.id == "186")[0];
                const name = self.getBHProperName(threadOption.selectedValue());
                if (name === ""){
                    return ""; // hide when default colour
                }
                return `${self.rootFolder}/Extras/jacket/zoom_buttons/${working_or_kissing}/5th/ButtonThread/${name}.png`;
            });

            // jacket sleeve (zoom) end    ===================================================




            this.selectedSuitJacketLiningColorFront = this.selectedJacketLiningColorFront;

            this.selectedSuitJacketBottom = ko.computed(function () {
                // depends on button style
                let buttonsStyle = self._getSuitButtonsStyleName();

                // also depends on lapel style
                const lapelStyleFoldName = "notch_lapel"; // for now we only have notch lapel bodies (waiting for Dimitraki)

                const default_body = `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/notch_lapel/curved/${buttonsStyle}.png`;

                // double buttons exception
                if (buttonsStyle.toLowerCase().includes("double")){
                    return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/notch_lapel/double_breasted/double-breasted.png`
                }

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '50') {
                        for (var option of category.options) {
                            if (option.id == '159' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2606": // Curved Bottom
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/curved/${buttonsStyle}.png`;
                                            case "2607": // Custom Bottom
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/curved/${buttonsStyle}.png`;
                                            case "2608": // Straight Bottom
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/${lapelStyleFoldName}/straight/${buttonsStyle}.png`;

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return default_body;
            });

            this.selectedSuitJacketBottom2 = ko.computed(function () {
                // depends on button style
                let buttonsStyle = self._getSuitButtonsStyleName();
                if (!buttonsStyle.toLowerCase().trim().includes("double")){
                    return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/j_bottom_hole/j_bottom_hole.png`;
                }
                else {
                    return "";
                }
            });

            this.selectedSuitJacketLapel = ko.computed(function () {

                // depends on buttons style
                let buttonsStyle = self._getSuitButtonsStyleName();

                // lapel style
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '52') {
                        for (var option of category.options) {
                            if (option.id == '164' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2613": //	 Custom Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/notch_lapel/${buttonsStyle}.png`;
                                            case "2614": //	 Notch Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/notch_lapel/${buttonsStyle}.png`;
                                            case "2615": //	 Peak Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/peak_lapel/${buttonsStyle}.png`;
                                            case "2616": //	 Shawl Lapel
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/shawl_lapel/${buttonsStyle}.png`;
                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/lapel/notch_lapel/${buttonsStyle}.png`;

            });

            this.selectedSuitJacketLapelShadow = ko.computed(function () {

                // depends on Lapel style
                let lapelShape = self.selectedSuitJacketLapel().replace(`fabrics/${self.fabric()}/lapel`, "shadows");

                return lapelShape;

            });

            this.selectedSuitJacketPocket                = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '55') {
                        for (var option of category.options) {
                            if (option.id == '187' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "3632": // Back Curved
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/back_curved.png`;
                                            case "3633": // Batman
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/batman.png`;
                                            case "3634": // Classic
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/classic_pocket.png`;
                                            case "3635": // Front Curved
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/front_curved.png`;
                                            case "3636": // Patch Pocket
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/patch_pocket.png`;
                                            case "3637": // Slightly Slanted
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/slightly.png`;
                                            case "3638": // Welt Pocket
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/welt_pocket.png`;
                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/pockets/classic_pocket.png`;
            });

            this.selectedSuitJacketTicketPocket          = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '55') {
                        for (var option of category.options) {
                            if (option.id == '188' && option.selectedValue()) {
                                return self.selectedSuitJacketPocket().replace(".png", "-ticket.png");
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketBreastPocket = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '56') {
                        for (var option of category.options) {
                            if (option.id == '190' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "3647": // Patch Pocket
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/breastpocket/patch_pocket.png`;
                                            case "3646": // No Breastpocket
                                            return "";
                                            case "3649": // Standard Straight
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/breastpocket/standard_straight.png`;
                                            case "3648": // Standard Angled
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/breastpocket/standard_angled.png`;
                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });


            this.selectedSuitJacketStitchForPocket       = ko.computed(function () {

                const lapelDetailsCategory = self.selectedGarment.categories.find(category => category.id == "53");
                if (!lapelDetailsCategory) return "";

                // stitch pocket is on?
                const stitchPocketOption = lapelDetailsCategory.options.find(option=>option.id == "200");
                if (!stitchPocketOption) return "";
                if (!stitchPocketOption.selectedValue()) return "";


                // get pocket format
                const temp = self.selectedSuitJacketPocket().split("/");
                let pocketFormat = temp[temp.length-1].replace(".png", "");

                // Welt Pocket has no stitch
                if (pocketFormat.toLowerCase().includes("welt")) return "";

                // has ticket?
                const has_ticket = self.selectedSuitJacketTicketPocket().includes("ticket");

                // get stich colour
                const stitchPocketColourOption = lapelDetailsCategory.options.find(option=>option.id == "201");
                if (!stitchPocketColourOption) return "";

                const stitchColour = stitchPocketColourOption.selectedValue() ? self.getBHProperName(stitchPocketColourOption.selectedValue()) : "a1";
                if (stitchColour === ""){
                    return ""; // hide when default colour
                }

                return `${self.rootFolder}/Extras/jacket/pockets_top_stitch/${pocketFormat}${has_ticket ? "-ticket":""}/${stitchColour}.png`;

            });

            this.selectedSuitJacketStitchForLapel        = ko.computed(function () {
                // check if stich option is on
                const lapelOptionsCategory = self.selectedGarment.categories.find(cat => cat.id == "53");
                if (!lapelOptionsCategory) return "";

                const stichLapelOption = lapelOptionsCategory.options.find(option => option.id == "199");
                if (!stichLapelOption) return "";

                if (!stichLapelOption.selectedValue()) return "";



                // get selected jacket style and size
                const jacketLapelStyle = self._getSuitLapelStyleName();
                const jacketLapelButtonsStyle = self._getSuitButtonsStyleName();


                // get selected trim color
                const stichColourOption = lapelOptionsCategory.options.find(option => option.id == "201");
                if (!stichColourOption) return "";

                let stitchColour = stichColourOption.selectedValue().name.replace(" ", "").toLowerCase();
                if (!stitchColour || stitchColour === "default") {
                    stitchColour = "a1"; // default
                }

                return `${self.rootFolder}/Extras/jacket/lapel_top_stitch/${jacketLapelStyle}/${jacketLapelButtonsStyle}/${stitchColour}.png`;
            });

            this.selectedSuitJacketBh                    = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '52') {
                        const bhColour_option = category.options.find(option => option.id == "167");
                        const color = self.getBHProperName(bhColour_option.selectedValue());
                        if (color === ""){
                            return ""; // hide when default colour
                        }
                        for (var option of category.options) {
                            if (option.id == '166' && option.selectedValue()) {
                                return `${self.rootFolder}/Extras/jacket/lapel_buttonhole/${color}.png`;
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedSuitJacketCollarTrimming        = ko.computed(function () {
                let upperImage = `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/front/upper.png`;

                if (!self.suitJacketTuxedoIsOn()) { return upperImage; }

                // check if tuxedo collar is on
                var lapelDetailsCategory = self.selectedGarment.categories.filter(cat=>cat.id=="53")[0];
                if(!lapelDetailsCategory){ return upperImage }

                let collarOption = lapelDetailsCategory.options.filter(opt => opt.id=="192")[0];

                if (collarOption && collarOption.selectedValue()){
                    return `${self.rootFolder}/Garments/jacket/tuxedo/tuxedo_upper.png`;
                }

                return upperImage;
            });

            this.selectedSuitJacketButtonsTrimming       = ko.computed(function () {

                // lapelOptionCategory
                var lapelDetailsCategory = self.selectedGarment.categories.filter(cat=>cat.id=="53")[0];
                if(!lapelDetailsCategory){ return "" }

                // buttons tuxedo is on?
                const tuxedoButtons = lapelDetailsCategory.options.find(option => option.id == "197");
                if (!tuxedoButtons) {return ""}

                // get jacket style and rename prefix folder
                if(tuxedoButtons && tuxedoButtons.selectedValue()){
                    return self.selectedSuitJacketButtonStyle().replace("/buttons/", "/tuxedo/center_buttons/");
                }

                return "";
            });

            this.selectedSuitJacketPocketTrimming        = ko.computed(function () { return ""; });
            this.selectedSuitJacketTicketPocketTrimming  = ko.computed(function () { return ""; });

            this.selectedSuitJacketLapelTrimming         = ko.computed(function () {
                const lapelStyleFoldName = self._getSuitLapelStyleName();
                const buttonName = self._getSuitButtonsStyleName();

                if (!self.suitJacketTuxedoIsOn()){
                    return "";
                }

                // check if lapel tuxedo is on
                // check if tuxedo collar is on
                var lapelDetailsCategory = self.selectedGarment.categories.filter(cat=>cat.id=="53")[0];
                if(!lapelDetailsCategory){ return ""; }


                let lapelOption = lapelDetailsCategory.options.filter(opt => opt.id=="191")[0];
                let lapelOptionEdge = lapelDetailsCategory.options.filter(opt => opt.id=="193")[0];


                if (lapelOption && lapelOption.selectedValue()) {
                    // check if edging is on
                    if (lapelOptionEdge && lapelOptionEdge.selectedValue()) {
                        // check current size of lapel and change image folder
                        return `${self.rootFolder}/Extras/jacket/edging/${lapelStyleFoldName}/${buttonName}.png`;
                    } else {
                        // check current size of lapel and change image folder
                        return `${self.rootFolder}/Garments/jacket/tuxedo/${lapelStyleFoldName}/${buttonName}/${buttonName}_main.png`;
                    }
                }




                return "";

            });

            this.selectedSuitJacketCollarUpperTrimming = ko.computed(function() {
                const lapelStyleFoldName = self._getSuitLapelStyleName();
                const buttonName = self._getSuitButtonsStyleName();

                if (lapelStyleFoldName.toLocaleLowerCase().includes('shawl')){
                    return "";
                }

                if (self.selectedJacketCollarTrimming().includes('tuxedo')){
                    return `${self.rootFolder}/Garments/jacket/tuxedo/${lapelStyleFoldName}/${buttonName}/${buttonName}_upper.png`;
                }
                else {
                    return "";
                }

            });

            this.selectedSuitJacketVent = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '51') {
                        for (var option of category.options) {
                            if (option.id == '161' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "2609": // Custom Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/no_vent.png`;
                                            case "2610": // No Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/no_vent.png`;
                                            case "2611": // One Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/center_vent_opened.png`;
                                            case "2612": // Two Vent
                                            return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/side_vents.png`;

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/body/no_vent.png`;
            });

            /** Used only for 3D_v2 */
            this.suitJacketVent2 = ko.computed(function(){
                // depends on jacket vent

                if (self.selectedSuitJacketVent().toLowerCase().includes("opened") && self.selectedSuitJacketVent().toLowerCase().includes("center") ){
                    return `${self.rootFolder}/Garments/jacket/vent/center/lining.png`
                }

                else if (self.selectedSuitJacketVent().toLowerCase().includes("side") ){
                    return `${self.rootFolder}/Garments/jacket/vent/side/lining.png`
                }

                return "";
            });

            this.selectedSuitJacketBackUpper = ko.computed(function() {
                const default_lapel = `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/upper/normal.png`;

                // check if tuxedo lapel is on
                const tuxedos_cat = self.selectedGarment.categories.find(cat => cat.id == "53");
                if (!tuxedos_cat) return default_lapel;
                const collar_option = tuxedos_cat.options.find(option => option.id == "192");
                if (!collar_option) return default_lapel;

                if (collar_option.selectedValue()){
                    return `${self.rootFolder}/Garments/jacket/tuxedo/tuxedo_upper_back.png`;
                } else{
                    return `${self.rootFolder}/Garments/jacket/fabrics/${self.fabric()}/body/back/upper/normal.png`
                }
            });


            // ==============================================================

            // Suit pants
            // ...

            this.suitPantsDefaultImage = self.pantsDefaultImage;
            this.suitPantsDefaultImageBack = self.pantsDefaultImageBack;

            this.selectedSuitPantStyle = ko.computed(function () {

                const selectedSuitPantsStyleID = self.getSelectedSuitPantStyleId();

                switch (selectedSuitPantsStyleID) {
                    case "3840":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/boot_flare.png`;

                    case "3841":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/slim.png`;

                    case "3842":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/normal_straight.png`;

                    default:
                    break;
                }

                return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/body/normal_straight.png`;

            });

            this.selectedSuitPantPleatStyle = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '59') {
                        for (var option of category.options) {
                            if (option.id == '213' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "3843": // double_pleats
                                            return `${self.rootFolder}/Garments/pants/pleats/double_pleats.png`;

                                            case "3844": // no_pleats
                                            return ""

                                            case "3845": // single_pleats
                                            return `${self.rootFolder}/Garments/pants/pleats/single_pleats.png`;

                                            case "3846": // triple_pleats
                                            return `${self.rootFolder}/Garments/pants/pleats/triple_pleats.png`;

                                            default:
                                            break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return ""
            });

            this.selectedSuitPantFrontPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '60') {
                        for (var option of category.options) {
                            if (option.id == '215' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {

                                            case "3847":
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/slanted.png`;

                                            case "3848":
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/jeans.png`;

                                            case "3849":
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/modern_curved.png`;

                                            case "3850":
                                            return "";

                                            case "3851":
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/slanted.png`;

                                            case "3852":
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/slanted_welt.png`;

                                            case "3853":
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/pockets/straight_welt.png`;

                                            default:
                                            break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            });

            this.selectedSuitPantBackPockets = ko.computed(function () {
                var style = 'gfx/order/image_3d/pant/none.png';

                // pocket category
                const pocketCategory = self.selectedGarment.categories.find(category => category.id == "61");
                if (!pocketCategory) return style;

                // position option
                const positionOption = pocketCategory.options.find(option => option.id == "219");
                if (!positionOption) return style;

                // get position (left/right/both)
                const positionValue = positionOption.selectedValue() ? positionOption.selectedValue().id : null;
                let positionName = "both";

                switch (positionValue) {
                    case "3863":
                    positionName = "both";
                    break;
                    case "3864":
                    positionName = "left";
                    break;
                    case "3865":
                    positionName = "right";
                    break;
                }


                for (var option of pocketCategory.options) {
                    if (option.id == '217' && option.values) {
                        for (var value of option.values) {
                            if (value.selected() == true) {
                                switch (value.id) {

                                    case "3854": // "Double"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/double_opening.png`;
                                    break;
                                    case "3855": // "Double w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/double_opening.png`;
                                    break;
                                    case "3856": // "Modern Flap no Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/modern_flap_with_button.png`;
                                    break;
                                    case "3857": // "Modern Flap w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/modern_flap_with_button.png`;
                                    break;
                                    case "3858": // "No Pocket Opening"
                                    style = "";
                                    break;
                                    case "3859": // "Single"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/single_opening.png`;
                                    break;
                                    case "3860": // "Single w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/single_opening.png`;
                                    break;
                                    case "3861": // "Square Flap no Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/square_flap_with_button.png`;
                                    break;
                                    case "3862": // "Square Flap w. Button"
                                    style = `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/pockets/${positionName}/square_flap_with_button.png`;
                                    break;

                                    default:
                                    break;

                                }
                            }
                        }

                    }
                }


                return style;


            });

            this.selectedSuitPantBeltAndCuffs = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '62') {
                        for (var option of category.options) {
                            if (option.id == '220' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "3866": // "Buckle Side Adjusters"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/style/belt_loop/none-+-buckle-side-adjusters.png`;
                                            case "3867": // "Button Side Adjusters"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/style/belt_loop/none-+-button-side-adjusters.png`;;
                                            case "3868": // "Criss Cross"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/criss_cross.png`;
                                            case "3869": // "Designer"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/style/belt_loop/modern.png`;
                                            case "3870": // "Double Angled"
                                            return `Double Angled missing<<<<`;
                                            case "3871": // "Double Straight"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop/double.png`;
                                            case "3872": // "No belt_loop"
                                            return "";
                                            case "3873": // "Single Straight"
                                            return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/style/belt_loop/single.png`;

                                            default:
                                            break;

                                        }
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            });

            this.selectedSuitPantBeltAndCuffsBack = ko.computed(function(){
                // get belt_loop front end replace prefix
                return self.selectedSuitPantBeltAndCuffs().replace(`${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/belt_loop`, `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/belt_loop` );
            });

            this.selectedSuitPantCuffs = ko.computed(function(){
                // get belt & cuff category
                const cuffCategory = self.selectedGarment.categories.filter(cat => cat.id == "62")[0];
                if (!cuffCategory) return "";

                // get cuff option
                const cuffOption = cuffCategory.options.filter(el => el.id == "222")[0];
                if (!cuffOption) return "";

                if (!cuffOption.selectedValue()){
                    // no cuff
                    return "";
                }

                // get cuff size (normal, large, slim)
                // default: normal
                const cuffSize = 'normal';

                // get pant style
                const selectedPantsStyleID = self.getSelectedSuitPantStyleId();

                switch (selectedPantsStyleID) {
                    case "3840":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/cuffs/boot_flare/${cuffSize}.png`;
                    case "3841":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/cuffs/slim/${cuffSize}.png`;
                    case "3842":
                    return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/front/cuffs/normal_straight/${cuffSize}.png`;
                    default:
                    return "";
                }
            });

            this.selectedSuitPantCuffsBack = ko.computed(function(){
                if (self.selectedSuitPantCuffs() == ""){
                    return "";
                }

                // get from front and just change the prefix
                const temp = self.selectedSuitPantCuffs().split("/");
                return `${self.rootFolder}/Garments/pants/fabrics/${self.fabric()}/style/back/cuffs/${temp[temp.length-1]}`

            });

            this.selectedSuitPantPleatsBack = ko.computed(function(){
                // see if pleats is !== 'no pleats'
                if (self.selectedSuitPantPleatStyle() !== ""){
                    return `${self.rootFolder}/Garments/pants/back_pleats/back_pleats.png`;
                } else {
                    return "";
                }

            });

            // vest
            // ██╗   ██╗███████╗███████╗████████╗
            // ██║   ██║██╔════╝██╔════╝╚══██╔══╝
            // ██║   ██║█████╗  ███████╗   ██║
            // ╚██╗ ██╔╝██╔══╝  ╚════██║   ██║
            //  ╚████╔╝ ███████╗███████║   ██║
            //   ╚═══╝  ╚══════╝╚══════╝   ╚═╝
            //

            this.vestDefaultImagesFront = ko.computed(function () {
                return [
                    ""
                ]
            });

            this.vestDefaultImagesBack = ko.computed(function () {
                return [
                    `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/back/back.png`
                ]
            });

            this.selectedVestLapel = ko.computed(function () {

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '22') {
                        for (var option of category.options) {
                            if (option.id == '74' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1291": // Custom
                                            return "";
                                            case "1292": // Notch lapel
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/lapel/notch_lapel.png`;
                                            case "1293": // Peak
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/lapel/peak_lapel.png`;
                                            case "1294": // Shawl lapel V
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/lapel/shawl_lapel.png`;
                                            case "1295": // style v
                                            return "";
                                            case "4587": // Lapel U
                                            return "";
                                            default:
                                            break;
                                        }
                                    }
                                }

                            }
                        }

                    }
                }


                return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/lapel/peak_lapel.png`;
            });

            this.selectedVestUpper = ko.computed(function(){

                // depends on lapel
                const lapelStyle = self.selectedVestLapel();
                return lapelStyle == "" ? `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/upper/upper_normal.png` : `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/upper/upper_lapel.png`;
            })

            this.selectedVestBottom = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '23') {
                        for (var option of category.options) {
                            if (option.id == '76' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1296":
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/bottom/angle_cut.png`;
                                            case "1297":
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/bottom/straight_cut.png`;

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/bottom/angle_cut.png`;
            });

            this.selectedVestButton = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '24') {
                        for (var option of category.options) {
                            if (option.id == '78' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {

                                        switch (value.id) {
                                            case "1298":
                                            return `${self.rootFolder}/Garments/vest/buttons/buttons/6_buttons.png`;
                                            case "1299":
                                            return `${self.rootFolder}/Garments/vest/buttons/buttons/5_buttons.png`;
                                            case "1300":
                                            return `${self.rootFolder}/Garments/vest/buttons/buttons/4_buttons.png`;
                                            case "1301":
                                            return `${self.rootFolder}/Garments/vest/buttons/buttons/6_buttons.png`;
                                            case "4564":
                                            return "";
                                            case "4565":
                                            return "";

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return `${self.rootFolder}/Garments/vest/buttons/buttons/4_buttons.png`;
            });

            this.selectedVestPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '25') {
                        for (var option of category.options) {
                            if (option.id == '80' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1302": // Flap
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/pockets/flap_pocket.png`;
                                            case "1303": // No
                                            return "";
                                            case "1304": // Patch
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/pockets/patch_pocket.png`;
                                            case "1305": // Single
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/pockets/single_pocket.png`;

                                            default:
                                            break;
                                        }


                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedVestBreastPockets = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '25') {
                        for (var option of category.options) {
                            // breast pocket
                            if (option.id == '82' && option.selectedValue()) {
                                // double breast pocket
                                if (category.options.filter(function (el) { return el.id == '225' })[0].selectedValue()) {
                                    return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/chest_pockets/chest_double_breast_pocket.png`;
                                } else {
                                    return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/chest_pockets/chest_right_breast_pocket.png`;
                                }
                            }
                        }
                    }
                }
                return "";
            });

            this.selectedvestBack = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '26') {
                        for (var option of category.options) {
                            if (option.id == '83' && option.values) {
                                for (var value of option.values) {
                                    if (value.selected() == true) {
                                        switch (value.id) {
                                            case "1309":
                                            return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/back/belt/belt.png`;
                                            case "1310":
                                            return "";

                                            default:
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            });


            this.selectedVestFrontLiningColor = ko.computed(function () {

                // depends on bottom_style
                const selectedBottomStyle = self.selectedVestBottom();
                selectedBottomStyle.includes("straight") ? `${self.rootFolder}/Garments/vest/lining/straight.png` : `${self.rootFolder}/Garments/vest/lining/style_v.png`;

                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            if (option.id == '132' && option.selectedValue()) {
                                return selectedBottomStyle.includes("straight") ? `${self.rootFolder}/Garments/vest/lining/straight.png` : `${self.rootFolder}/Garments/vest/lining/style_v.png`;
                            }
                        }
                    }
                }

                // default value
                return `${self.rootFolder}/Garments/vest/lining/straight.png`;
            });

            this.selectedVestLiningColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            if (option.id == '132' && option.selectedValue()) {
                                return option.selectedValue().previews;
                            }
                        }
                    }
                }

                // default value
                return "gfx/order/image_3d/liningFabric/sign/01.png";
            });

            this.selectedVestPipingColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            if (option.id == '133' && option.selectedValue()) {
                                return option.selectedValue().preview;
                            }
                        }
                    }
                }
                return "gfx/order/image_3d/pipingColors/piping_default_preview.png";
            });

            this.selectedVestPocketColor = ko.computed(function () {
                for (var category of self.selectedGarment.categories) {
                    if (category.id == '27') {
                        for (var option of category.options) {
                            // if (option.id == '134' && option.selectedValue()) {
                            if (option.id == '133' && option.selectedValue()) {
                                switch (option.selectedValue().id) {
                                    case "1303": // "No Pocket"
                                    return ``
                                    case "1305": // "Single Pocket"
                                    return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/pockets/single_pocket.png`
                                    case "1304": // "Patch Pocket"
                                    return ``
                                    case "1302": // "Flap Pocket"
                                    return `${self.rootFolder}/Garments/vest/fabrics/${self.fabric()}/pockets/flap_pocket.png`
                                    default:
                                    return ``
                                }
                            }
                        }
                    }
                }
                return "";
            });

        }

        changeFabric3d(newFabric){

            let url = false;
            let url2_for_suit = false;
            switch (this.selectedGarment.garment_id) {
                case "4": // jacket
                    url = `${this.rootFolder}/Garments/jacket/fabrics/${newFabric}`
                    break;
                case "1": // pants
                    url = `${this.rootFolder}/Garments/pants/fabrics/${newFabric}`
                    break;
                case "3": // vest
                    url = `${this.rootFolder}/Garments/vest/fabrics/${newFabric}`
                    break;
                case "6": // suit
                    url = `${this.rootFolder}/Garments/jacket/fabrics/${newFabric}`
                    url2_for_suit = `${this.rootFolder}/Garments/pants/fabrics/${newFabric}`
                    break;
            }

            // inner function to avoid duplicate code
            const checkFolder = (url) => {
                if (!url) return false;

                let response = false;

                // check if folder exists
                $.ajax({
                    type: 'HEAD',
                    url,
                    async: false, // we need this to be sync so we can return the response
                    success: () => {
                        // if exists, change
                        console.log('folder exists');
                        response = true;

                    },
                    error: () => {
                        // if not, use default
                        console.log('folder doesn\'t exists');
                        response = false;
                    }
                });

                return response;
            }

            // for 'normal' garment, we only need one url
            if (["1", "3", "4"].includes(this.selectedGarment.garment_id)) {
                if (checkFolder(url)) {
                    this.fabric(newFabric); return;
                }
            }
            // for suit, we need to check jacket and pants
            else if (this.selectedGarment.garment_id === "6") {
                if (checkFolder(url) && checkFolder(url2_for_suit)){
                    this.fabric(newFabric); return;
                }
            }

            // if something fails. use default fabric
            this.fabric(this.defaultFabric);

        }

        /**
        * For Jacket and suit, get BH image name from value name.
        * eg: if color is "A 2", image must be "[...]/a2.png"
        * @param {*} BHValue
        */
        getBHProperName(BHValue){
            let image;
            if (BHValue.name.toLowerCase().trim().includes("default")) {
                image = ""; // hide when default
            } else {
                image = BHValue.name.replace(" ", "").toLowerCase();
            }

            return image;
        }
    }

});