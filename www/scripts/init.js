require.config({
    paths: {
        // RequireJS plugin
        text: 'libs/require/text',
        domReady: 'libs/require/domReady',
        // jQuery
        jquery: 'libs/jquery/jquery-1.8.2',
        jqueryui: 'libs/jquery/jquery-ui',
        jquerytouchpunch: 'libs/jquery-ui-touch-punch',
        jclass: 'libs/jquery.class',
        jqmdialog: 'libs/jqmSimpleDialog/jquery.mobile.simpledialog.min',
        // jQuery Mobile framework
        jqm: 'libs/jquery.mobile/jquery.mobile-1.4.5',
        jqmrouter: 'libs/jquery.mobile.router/jquery.mobile.router',
        knockout: 'libs/knockout/knockout-2.2.0',
        // knockout: 'libs/knockout/knockout-3.4.2',
        jqueryuk: 'libs/jquery/knockout-jqueryui',
        date: 'libs/jquery/datepicker',
        jqmSM: 'libs/jqm.slidemenu',
        kojqm: 'libs/kojqm',
        komap: 'libs/ko.mapping',
        iscroll: 'libs/iscroll',
        jqmSlide: 'libs/jqmSlide',
        jgrowl: 'libs/jquery.jgrowl.min',
        fscreen: 'libs/jquery.fullscreen',
        router: 'router',
        base: 'appBase',
        app: 'app',
        login: 'login',
        signature: 'signature/jp_signature2',
        trie: 'libs/trie',
        raphael: 'libs/raphael',
        slick: 'libs/slick',
        slide: 'libs/itemslide',
        sketchpad: 'libs/raphael.sketchpad',
        html2canvas : 'libs/html2canvas',
        // dom2image : 'libs/dom-to-image', cannot generate from 3D
        hammer: 'libs/hammerJs/hammer'
    },
    shim: {
        jqueryui: {
            deps: ['jquery'],
            exports: '$.ui'
        },
        sketchpad: {
            deps: ['jquery', 'raphael'],
            exports: '$.ui'
        },
        jqueryuk: {
            deps: ['jquery', 'jqueryui', 'knockout'],
            exports: 'kojqui'
        }
    }
});

require(['require', 'jquery', 'jqmrouter', 'jqueryui', 'login', 'router'], function(require) {
    require(['require', 'knockout', 'iscroll', 'komap', 'jclass', 'libs/jqmExternalAsEmbeddedPages', 'jqmSlide', 'fscreen', 'jgrowl', 'base', 'signature', 'trie', 'jqm', 'raphael', 'html2canvas' ], function(require, ko, iscroll, komap) {
        window.ko = ko;
        require(['require',
         'jqmdialog',
         'jqueryuk',
         'sketchpad',
         'slick',
         'slide',
         'jquerytouchpunch',
         "libs/reportVaidityPolyfill",
         "libs/jquery_confirm/jquery_confirm",
         'hammer',

         "utils/utils",

         'viewmodel/AuthControl',
         'viewmodel/CustomerCntClass',
         'viewmodel/FabricsCntClass',
         'viewmodel/Bodyshape',
         'viewmodel/OrderItemSelectGarment',
         'viewmodel/Measurements',
         'viewmodel/FitlinesJacket',
         'viewmodel/FitlinesPants',
         'viewmodel/Fitting',
         'viewmodel/Completion',
         'viewmodel/Alterations',
         'viewmodel/AlterationsVM',
         'viewmodel/AlterationsBriefVM',
         'viewmodel/BriefDetailsVM',
         'viewmodel/AlterationsReportListVM',
         'viewmodel/AlterationsReportSubmissionVM',
         'viewmodel/AlterationReport',
         'viewmodel/AlterationMainPageVM',
         'viewmodel/ShippingFabric',
         'viewmodel/ShippingCopyGarment',
         'viewmodel/ShippingFaultyRemakeGarments',
         'viewmodel/ShippingOther',
         'viewmodel/ShippingData',
         'viewmodel/ShippingBox',
         'viewmodel/ShippingBoxesModel',
         'viewmodel/ShippingBoxesViewModel',
         'viewmodel/AlterationListsViewModel',
         'viewmodel/FittingPreviewVM',
         'viewmodel/ErrorReport',
         'viewmodel/MediaControlClass',
         'viewmodel/Notifications',
         'viewmodel/CustomProperties',
         'viewmodel/AlterationReportVM',
         'viewmodel/garments/GarmentsSuit',
         'viewmodel/garments/GarmentsVest',
         'viewmodel/garments/GarmentsShirt',
         'viewmodel/garments/GarmentsJacket',
         'viewmodel/garments/GarmentsPant',
         'viewmodel/garments/GarmentsShirt',
         'viewmodel/ActiveAlterationsVM',
         'viewmodel/Shipping_incomingBoxesVM',
         'viewmodel/Shipping_missingItemsVM',
         'viewmodel/Shipping_incomingBoxHistoryVM',
         'viewmodel/Shipping_outgoingBoxHistoryVM',
         'viewmodel/Shipping_outgoingBoxesVM',
         'viewmodel/Shipping_trackingVM',
         'viewmodel/PreOrdersVM',
         'viewmodel/OrdersVM',
         'viewmodel/GarmentsStepVM',
         'viewmodel/MeasurementsStepVM',
         'viewmodel/BodyShapesStepVM',
         'viewmodel/UploadImagesStepVM',
         'viewmodel/UploadVideosStepVM',
         'viewmodel/GarmentsDesignStepVM',
         'viewmodel/Image3DVM',
         'viewmodel/PaymentsStepVM',
         'viewmodel/OrderProcessVM',
         'viewmodel/OrderStepsRequirementsVM',
         'viewmodel/UnlockCustomersFittingsVM',
         'viewmodel/OrderOverviewVM',
         'viewmodel/PendingGroupOrdersVM',

         'viewmodel/UniformCompanyVM',
         'viewmodel/UniformTemplateVM',
         'viewmodel/UniformOrderVM',
         'viewmodel/UniformGarmentsStepVM',
         'viewmodel/UniformGarmentsDesignStepVM',
         'viewmodel/UniformPortfolioVM',
         'viewmodel/UniformFinalizeStepVM',
         'viewmodel/UniformCompanySearchVM',

         'viewmodel/UniformOrderGarmentsStepVM',
         'viewmodel/UniformOrderCustomerDetailsVM',
         'viewmodel/UniformOrderContactPersonVM',
         'viewmodel/UniformOrderPaymentsVM',
         'viewmodel/UniformOrderBodyShapesStepVM',
         'viewmodel/UniformOrderUploadImagesStepVM',
         'viewmodel/UniformOrderUploadVideosStepVM',
         'viewmodel/UniformStatisticsVM',
         'viewmodel/TasksVM',

         'viewmodel/ProfitabilityVM',
         'viewmodel/EmployeeInvoiceVM',
         'viewmodel/ConfigGlobalVarsVM',

         'viewmodel/QualityControlVM',
         'viewmodel/GenericErrorVM',

     ], function(require) {

            // Clear local storage
            localStorage.clear();

            // do not remove!
            authCtrl = new AuthControl();

            //$(document).on("ready", function(){
            //$.mobile.pageContainer = $('#container');
            // Setting default transition to slide
            $.mobile.defaultPageTransition = 'slide';
            ko.mapping = komap;

            config_global_vars = new ConfigGlobalVarsVM();

            //Application entrypoint, define Globals
            //Pass application control to router
            require(['app']);


            // authCtrl.username("John");
            // authCtrl.password("Ionikos4");
            // authCtrl.login();

                authCtrl.username("luiz");
                authCtrl.password("medaumpass");
                authCtrl.login();

            // authCtrl.username("Igor");
            // authCtrl.password("_42aja");
            // authCtrl.login();


            // end auto login
            // });

            sleep = function(time)
            {
                return new Promise((resolve) => setTimeout(resolve, time));
            }

        });
    });
});