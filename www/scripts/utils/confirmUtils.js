// This file must define generic confirms
// https://craftpip.github.io/jquery-confirm/



/**
 * Usage:
 * eg1: pos_confirm({cancelBtn:"no"})
 */

// simple confirm with callbacks
window.pos_confirm = function (params) {
    const { msg, confirmAction = (function() {}), cancelAction = (function() {}), finallyAction = (function(){}), confirmBtn= "confirm", cancelBtn= "cancel"} = params;
    $.confirm({
        title: '',
        content: msg,
        buttons: {
            confirmBtn: {
                text: confirmBtn,
                action: function () {
                    confirmAction();
                    finallyAction();
                },
                keys: ['enter', 'shift'],
            },
            cancelBtn: {
                text: cancelBtn,
                action: function () {
                    cancelAction();
                    finallyAction();
                },
                keys: ['esc']
            },

        },


    });
}


// simple alert with callbacks
window.pos_alert = function(params){
    const { msg, confirmAction = (function() {}), finallyAction = (function(){}), confirmBtn= "ok"} = params;
    $.confirm({
        title: '',
        content: msg,
        buttons: {
            confirmBtn: {
                text: confirmBtn,
                action: function () {
                    confirmAction();
                    finallyAction();
                },
                keys: ['enter', 'shift'],
            },
        },
    });
}

// with "!" symbol and orange theme
window.pos_warning = function (params) {
    const {
        msg='',
        title='Warning',
        confirmAction = (function() {}),
        finallyAction = (function(){}),
        confirmBtn= "ok",
        cancelBtn= "cancel",
        cancelAction = () => {},
    } = params;

    $.confirm({
        title: title,
        content: msg,
        type: 'orange',
        icon: 'fa fa-warning personalized',
        typeAnimated: true,
        buttons: {
            confirmBtn: {
                text: confirmBtn,
                btnClass: 'btn-orange',
                action: function () {
                    confirmAction();
                    finallyAction();
                },
                keys: ['enter', 'shift'],
            },
             cancelBtn: {
                text: cancelBtn,
                action: function () {
                    cancelAction();
                    finallyAction();
                },
                keys: ['esc']
            },
        }
    });
}

// red theme
window.pos_error = function (params) {
    const { msg='', title='Error'} = params;

    $.alert({
        title: title,
        content: msg,
        type: 'red'
    });
}

// green theme
window.pos_ok = function (params) {
    const { msg='', title=''} = params;

    $.alert({
        title: title,
        content: msg,
        type: 'green'
    });
}
