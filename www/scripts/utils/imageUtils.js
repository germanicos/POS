ImageUtils = class {
    constructor(){
        throw new Error("Do not instantiate this class");
    }

    static getRandomImage(){
        const mock_images = [
            "https://www.shirt-tailor.net/thepos/uploaded/temp_order_images/1575946192.jpg",
            "https://www.shirt-tailor.net/thepos/uploaded/temp_order_images/1575946099.jpg",
            "https://www.shirt-tailor.net/thepos/uploaded/temp_order_images/1575946156.jpg",
            "https://paristernos.com/wp-content/uploads/2017/02/smoking-4.jpg",
            "https://cdnv2.moovin.com.br/bizzstore/imagens/produtos/original/terno-masculino-bruno-conte-listrado-cf6ab5b01d2ac82391d8e4a5eabf89ba.jpg",
            "https://paristernos.com/wp-content/uploads/2018/07/Terno-louvres-novo-modelo.jpg",
            "https://observatoriodocinema.bol.uol.com.br/wp-content/uploads/2017/05/alexandra-daddario-true-detective.jpg",

        ]

        return mock_images[Math.floor(Math.random()*mock_images.length)];
    }

    static getRandomVideo(){
        const mock_video = [
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",

        ]

        return mock_video[Math.floor(Math.random()*mock_video.length)];

    }

}