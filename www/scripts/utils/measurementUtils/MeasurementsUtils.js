define(['jquery', 'knockout', 'base'], function ($, ko) {

    console.log("Loading meas utils");

    class MeasurementUtils {
        constructor() {
            this.not_mandatory_fields = {
                "jacket": ["jacket_measurements_last_edited", "jacket_measurements_lapel_length", "jacket_measurements_notes"],
                "pants": ["pants_measurements_last_edited", "pants_measurements_front_crotch", "pants_measurements_inseam", "pants_measurements_zipper", "pants_measurements_notes"],
                "vest": ["vest_measurements_last_edited", "vest_measurements_v_length", "vest_measurements_notes"],
                "shirt": ["shirt_measurements_last_edited", "shirt_measurements_wrist", "shirt_measurements_notes", "shirt_measurements_watch"]
            }
        }

        isMandatory(meas_key) {
            let all_not_mandatory = []

            for (const not_mandatory_oer_garment of Object.values(this.not_mandatory_fields)) {
                all_not_mandatory = all_not_mandatory.concat(not_mandatory_oer_garment)
            }

            return !(all_not_mandatory.includes(meas_key))
        }
    }

    window.measurementUtils = new MeasurementUtils();

});