LocationsUtils = class {
    constructor() {
        throw new Error("Do not instantiate this class");
    }

    static getCityById(city_id){
        const cities = dsRegistry.getDatasource('citiesDS').getStore();
        return cities.find(city => city.locations_id == city_id.toString());
    }

    static getStateByCityId(city_id) {
        const states = dsRegistry.getDatasource('statesDS').getStore();
        const city = this.getCityById(city_id);
        if (!city) {
            console.warn(`city (id: ${city_id}) not found`);
            return null;
        }
        return states.find(state => state.locations_id == city.parent_location_id.toString())
    }

    static getCountryByCityId(city_id) {
        const countries = dsRegistry.getDatasource('countriesDS').getStore();

        const state = this.getStateByCityId(city_id);

        if (!state){
            console.warn(`state (city_id: ${city_id}) not found`);
            return null
        }

        return countries.find(country => country.locations_id == state.parent_location_id.toString())
    }


}