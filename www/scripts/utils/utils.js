/**
* This class holds general code to use in all part of code or temporary code
*/

define(['jquery', 'knockout',
    'utils/imageUtils', 'utils/locationsUtils', 'utils/customKOBinds', 'utils/confirmUtils', 'utils/HammerUtils', 'utils/GarmentUtils',
    'utils/measurementUtils/MeasurementsUtils'],
    function ($, ko) {

        // to use multiple inheritance
        var aggregation = (baseClass, ...mixins) => {
            class base extends baseClass {
                constructor(...args) {
                    super(...args);
                    mixins.forEach((mixin) => {
                        copyProps(this, (new mixin));
                    });
                }
            }
            let copyProps = (target, source) => {  // this function copies all properties and symbols, filtering out some special ones
                Object.getOwnPropertyNames(source)
                    .concat(Object.getOwnPropertySymbols(source))
                    .forEach((prop) => {
                        if (!prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
                            Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
                    })
            }
            mixins.forEach((mixin) => { // outside contructor() to allow aggregation(A,B,C).staticFunction() to be called etc.
                copyProps(base.prototype, mixin.prototype);
                copyProps(base, mixin);
            });
            return base;
        }

        // Utils encapsulates codes of other utils.This way we just need to know this file to import
        Utils = class extends aggregation(ImageUtils, LocationsUtils, GarmentUtils) { }

        // set pinch zoom
        Utils.hammer = HammerUtils;

        Utils.garmentUtils = GarmentUtils;

    }


);
