const map_id_to_name = {
    '1': 'pants',
    '2': 'shirt',
    '3': 'vest',
    '4': 'jacket',
    '6': 'suit',
}

const map_name_to_id = {
    'pants' :  '1',
    'shirt' :  '2',
    'vest' :   '3',
    'jacket' : '4',
    'suit' :   '6',
}

GarmentUtils = class {


    constructor(){
        throw new Error("GarmentUtils must be an abstract class")
    }

    static getGarmentNameById(id){
        id = id.toString();
        return map_id_to_name[id];

    }

    static getGarmentIdByName(name){
        name = name.toLowerCase().trim();
        return map_name_to_id[name];

    }
}