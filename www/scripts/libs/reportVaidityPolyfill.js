define(["jquery", "knockout", "base"], function ($, ko) {

    var timer;
    var highlightElement = function(element){
        
        // cancel previous timeout
        clearTimeout(timer);
        var self = $(this);

        console.log("Element empty", element);
        $(element).effect( "shake", {times:4}, 1000 );
        // $(element).effect("highlight", {border: 'red'}, 5000);
        
        // add new class for CSS integration
        $(element).toggleClass("elementMissing");  
    
        timer = setTimeout(function() {
            // reset CSS
            $(element).toggleClass("elementMissing");
        }, 7000); // time in miliseconds, so 5s = 5000ms


    }





    // phonegap does not support reportValidity, so this forces browser do not support too
    // HTMLFormElement.prototype.reportValidity = undefined;
    // HTMLFormElement.prototype.checkValidity = undefined;
    // HTMLInputElement.prototype.reportValidity = undefined;


    if (!HTMLFormElement.prototype.reportValidity) {
        HTMLFormElement.prototype.reportValidity = function () {
            var requiredElements = this.querySelectorAll("input:required, select:required");

            if (HTMLInputElement.prototype.reportValidity) {
                console.log("Using native input.reportValidity");
                for (const element of requiredElements) {
                    if (!element.reportValidity()) {
                        highlightElement(element);
                        return false;
                    }
                }
            }
            else {
                console.log("using polyfill input.ReportValidity");
                for (const element of requiredElements) {
                    if (!element.value) {
                        highlightElement(element);
                        return false;
                    }
                }

            }

            return true;

        };
    }

});


