define(['__tests__/new_3d/utils'], (utils) => {


    const addVestGarment = (orders = window.orders) => {
        orders.customers()[0].garmentsStep.incrementGarment('3');
        orders.selectedCustomer().garmentsDesignStep.selectedGarment(orders.globalGarments.vest()[0].garment);
    };

    /**
    * Check for entire current 3D and check if all images are ok.
    * @param {*} image3D
    * @returns {{result: boolean, missing: String}}
    */
    function check3D(image3D) {
        let response = { result: true, missing_images: [] };

        for (const key in image3D) {
            // this keys are list, not a string
            if (key == "vestDefaultImagesFront" || key == "vestDefaultImagesBack") {
                const attribute = image3D[key];

                if (typeof attribute === "function") {
                    for (const _string of attribute()) {
                        if (!utils.imageExists(_string)) {
                            // return { result: false, missing: _string };
                            response.result = false;
                            response.missing_images.push(_string);
                        }
                    }
                }
            }

            const keys_to_check = [
                "selectedVestFrontLiningColor",
                "selectedVestBottom",
                "selectedVestLapel",
                "selectedVestUpper",
                "selectedVestButton",
                "selectedVestPockets",
                "selectedVestBreastPockets",
                "selectedvestBack",
                "selectedVestLiningColor",
                "selectedVestPipingColor",
                "selectedVestPocketColor",
            ];

            if (!keys_to_check.includes(key)) {
                continue;
            }


            if (image3D.hasOwnProperty(key)) {
                const attribute = image3D[key];

                if (typeof attribute === "function") {
                    if (typeof attribute() === "string") {
                        if (!utils.imageExists(attribute())) {
                            // return { result: false, missing: attribute() };
                            response.result = false;
                            response.missing_images.push(attribute());

                        }
                    }
                }

            }



        }


        return response;

    }




    return { addVestGarment, check3D }

});