define([

], function () {

    // we are using orders like global var inside the code
    window.orders = getOrderObj();
    window.should_use_new_3D = true;



    /**
     * build the GlobalOrdersVM
     */
    function getOrderObj() {
        // mocks
        let customersIds = ["3824"];
        let leaderId = "3824";
        let customersData = [
            {
                "customer_username": null,
                "customer_password": null,
                "customer_first_name": "Luiz",
                "customer_last_name": "Barros2",
                "customer_DOB": "1994-02-12",
                "customer_occupation": "",
                "customer_company_name": "",
                "customer_gender": "1",
                "customer_address1": "Street address",
                "customer_address2": "Suburb",
                "customer_country": "1",
                "customer_state": "5",
                "customer_city": "6",
                "customer_postal_code": "878374",
                "customer_mobile_phone": "83999322442",
                "customer_landline_phone": "55",
                "customer_email": "luiz@tailor.com.au",
                "customer_email_subscription": "0",
                "customer_best_time_to_contact": null,
                "customer_refered_by": "Igor 1234567890",
                "customer_refered_by_id": "1495",
                "customer_referal_method": "7",
                "customer_other_way": "",
                "customer_referal_moreinfo": "",
                "customer_date_added": "2019-07-04 21:58:53",
                "customer_last_seen": "2019-12-31 04:36:21",
                "customer_added_by": "1",
                "customer_price_range": "",
                "customer_cat_id": "1",
                "customer_last_edited": "2019-12-31 04:36:21",
                "customer_how_did_hear": "",
                "customer_landline_code": "0",
                "receive_marketing": "0",
                "server_id": "3824",
                "id": 57,
                "customer_display_state": "VIC",
                "is_leader": true,
                "is_present": true,
                "is_paying": true,
                "is_locked": false
            }
        ];
        let leaderPic = "";
        let groupName = "";
        let isReorder = false;
        let reorderData = undefined;
        let groupId = undefined;

        return new OrdersVM(customersIds, leaderId, customersData, leaderPic, groupName, isReorder, reorderData, groupId);
    }

    /**
     * get a clean order obj and put in global vars (VMs uses as global var)
     */
    function cleanOrder() {
        window.orders = getOrderObj();
        return window.orders;
    }

    /**
     * check if image is broken
     * @param {string} image_url
     * @returns {boolean}
     */
    function imageExists(image_url) {

        // ignore images from server
        if (image_url.includes("http://shirt-tailor.net")) {
            return true;
        }

        var http = new XMLHttpRequest();

        try {
            http.open('HEAD', image_url, false);
            http.send();

        } catch (error) {
            console.warn("Exception");
            return false;
        }

        return http.status != 404;

    }

    /**
    * Changes each value of a radio option and check the 3D
    * @param {*} garment
    * @param {function} check3D_function each garment has different implementation for check3D. pass as parameter
    */
    function checkEachCategory(garment, check3D_function) {

        const response = { result: true, missing_images: [] };

        // put all options of each category in a single array
        let all_options = [];
        garment.categories.forEach(category => {
            all_options = all_options.concat(category.options);
        });


        for (const option of all_options) {
            // radio option, color piping, color lining, color button
            if (["6", /* "5", "8", "9" */].includes(option.type_id)) {

                // test value by value
                if (Object.keys(option).includes("values")) {
                    for (const value of option.values) {
                        orders.customers()[0].garmentsDesignStep.setOptionValue(option, value)
                        const result = check3D_function(garment.image3D);

                        if (!result.result) {
                            // return result;
                            response.result = false;
                            response.missing_images = response.missing_images.concat(result.missing_images);
                        }
                    }
                }
            }
            // boolean
            else if (option.type_id == "4") {
                option.selectedValue(!option.selectedValue());

                const result = check3D_function(garment.image3D);

                if (!result.result) {
                    // return result;
                    response.result = false;
                    response.missing_images = response.missing_images.concat(result.missing_images);
                }
            }
        }


        return response;

    }

    // like "exports"
    return {
        getOrderObj,
        imageExists,
        checkEachCategory,
        cleanOrder,
    }

});