

define(['model/image3D/Image3D_v2','__tests__/new_3d/vest_design_model', '__tests__/new_3d/utils'], function (_, vest_design_model, utils) {

    let orders;
    console.clear();


    describe("Vest 3D files", function () {
        beforeEach(() => {
            orders = utils.cleanOrder();
            vest_design_model.addVestGarment(orders);
        });
        describe("Check initial design", function () {
            it("Should have at least one vest in the design", function () {
                expect(orders.globalGarments.vest().length).toBeGreaterThan(0);
            });

            it("Should all initial images exists", function () {
                const result = vest_design_model.check3D(orders.globalGarments.vest()[0].garment.image3D);
                if (!result.result){
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);

            });

            it("Should have all images if design changes", () => {
                const result = utils.checkEachCategory(orders.globalGarments.vest()[0].garment, vest_design_model.check3D);
                if (!result.result){
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);

            });

        })
    })

})