define(['__tests__/new_3d/utils'], (utils) => {


    const addJacketGarment = (orders = window.orders) => {
        orders.customers()[0].garmentsStep.incrementGarment('4');
        orders.selectedCustomer().garmentsDesignStep.selectedGarment(orders.globalGarments.jacket()[0].garment);
    };

    /**
    * Check for entire current 3D and check if all images are ok.
    * @param {*} image3D
    * @returns {{result: boolean, missing: String}}
    */
    function check3D(image3D) {
        let response = { result: true, missing_images: [] };

        // this keys are list, not a string
        const exceptions = [
            "jacketDefaultImagesFront",
            "jacketDefaultImagesBack",
            "jacketDefaultImagesSleeve",
            "jacketDefaultImagesMonogram",
        ];

        for (const key of exceptions) {
            if (image3D.hasOwnProperty(key)) {
                const attribute = image3D[key];

                if (typeof attribute === "function") {
                    for (const _string of attribute()) {
                        if (!utils.imageExists(_string)) {
                            // return { result: false, missing: _string };
                            response.result = false;
                            response.missing_images.push(_string);
                        }
                    }
                }

            }
        }

        const keys_to_check = [
            "bh",
            "jacketVent2",
            "selectedJacketBackUpper",
            "selectedJacketBottom",
            "selectedJacketBottom2",
            "selectedJacketBreastPocket",
            "selectedJacketButtonsBHStyle",
            "selectedJacketButtonsTrimming",
            "selectedJacketButtonStyle",
            "selectedJacketCollarTrimming",
            "selectedJacketCollarUpperTrimming",
            "selectedJacketLapel",
            "selectedJacketLapelShadow",
            "selectedJacketLapelTrimming",
            "selectedJacketLiningColor",
            "selectedJacketLiningColorFront",
            "selectedJacketPipingColor",
            "selectedJacketPocket",
            "selectedJacketPocketColor",
            "selectedJacketPocketTrimming",
            "selectedJacketSleeve",
            "selectedJacketSleeveBHColour1",
            "selectedJacketSleeveBHColour2",
            "selectedJacketSleeveBHColour3",
            "selectedJacketSleeveBHColour4",
            "selectedJacketSleeveBHColour5",
            "selectedJacketSleeveButton1",
            "selectedJacketSleeveButton1BH",
            "selectedJacketSleeveButton1Zoom",
            "selectedJacketSleeveButton2",
            "selectedJacketSleeveButton2BH",
            "selectedJacketSleeveButton2Zoom",
            "selectedJacketSleeveButton3",
            "selectedJacketSleeveButton3BH",
            "selectedJacketSleeveButton3Zoom",
            "selectedJacketSleeveButton4",
            "selectedJacketSleeveButton4BH",
            "selectedJacketSleeveButton4Zoom",
            "selectedJacketSleeveButton5",
            "selectedJacketSleeveButton5BH",
            "selectedJacketSleeveButton5Zoom",
            "selectedJacketSleeveThread1",
            "selectedJacketSleeveThread2",
            "selectedJacketSleeveThread3",
            "selectedJacketSleeveThread4",
            "selectedJacketSleeveThread5",
            "selectedJacketTicketPocket",
            "selectedJacketTicketPocketTrimming",
            "selectedJacketVent",
            "stitchForLapel",
            "stitchForPocket",
        ];

        for (const key of keys_to_check) {
            if (image3D.hasOwnProperty(key)) {
                const attribute = image3D[key];

                if (typeof attribute === "function") {
                    if (typeof attribute() === "string") {
                        if (!utils.imageExists(attribute())) {
                            // return { result: false, missing: attribute() };
                            response.result = false;
                            response.missing_images.push(attribute());
                        }
                    }
                }

            }
        }

        return response;

    }

    return { addJacketGarment, check3D }

});