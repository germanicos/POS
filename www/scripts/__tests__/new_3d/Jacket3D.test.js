

define(['model/image3D/Image3D_v2','__tests__/new_3d/jacket_design_model', '__tests__/new_3d/utils'], function (_, jacket_design_model, utils) {

    let orders;
    console.clear();


    describe("Jacket 3D files", function () {
        beforeEach(() => {
            orders = utils.cleanOrder();
            jacket_design_model.addJacketGarment(orders);
        });
        describe("Check initial design", function () {
            it("Should have at least one jacket in the design", function () {
                expect(orders.globalGarments.jacket().length).toBeGreaterThan(0);
            });

            it("Should all initial images exists", function () {
                const result = jacket_design_model.check3D(orders.globalGarments.jacket()[0].garment.image3D);
                if (!result.result){
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);
            });

            it("Should have all images if design changes", () => {
                const result = utils.checkEachCategory(orders.globalGarments.jacket()[0].garment, jacket_design_model.check3D);
                if (!result.result){
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);
            });

        })
    })

})