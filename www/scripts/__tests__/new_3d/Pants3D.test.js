

define(['model/image3D/Image3D_v2', '__tests__/new_3d/pants_design_model', '__tests__/new_3d/utils'], function (_, pants_design_model, utils) {

    let orders;
    console.clear();


    describe("Pants 3D files", function () {
        beforeEach(() => {
            orders = utils.cleanOrder();
            pants_design_model.addPantsGarment(orders);
        });
        describe("Check initial design", function () {
            it("Should have at least one pants in the design", function () {
                expect(orders.globalGarments.pants().length).toBeGreaterThan(0);
            });

            it("Should all initial images exists", function () {
                const result = pants_design_model.check3D(orders.globalGarments.pants()[0].garment.image3D);
                if (!result.result) {
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);
            });

            it("Should have all images if design changes", () => {
                const result = utils.checkEachCategory(orders.globalGarments.pants()[0].garment, pants_design_model.check3D);
                if (!result.result) {
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);
            });

        })
    })

})