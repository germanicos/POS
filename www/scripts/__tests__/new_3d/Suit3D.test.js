

define(['model/image3D/Image3D_v2','__tests__/new_3d/suit_design_model', '__tests__/new_3d/utils'], function (_, suit_design_model, utils) {

    let orders;
    console.clear();


    describe("Suit 3D files", function () {
        beforeEach(() => {
            orders = utils.cleanOrder();
            suit_design_model.addSuitGarment(orders);
        });
        describe("Check initial design", function () {
            it("Should have at least one suit in the design", function () {
                expect(orders.globalGarments.suit().length).toBeGreaterThan(0);
            });

            it("Should all initial images exists", function () {
                const result = suit_design_model.check3D(orders.globalGarments.suit()[0].garment.image3D);
                if (!result.result){
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);

            });

            it("Should have all images if design changes", () => {
                const result = utils.checkEachCategory(orders.globalGarments.suit()[0].garment, suit_design_model.check3D);
                if (!result.result){
                }
                expect(result.result).toBe(true, `missing images: \n${result.missing_images.join('\n')}`);

            });

        })
    })

})