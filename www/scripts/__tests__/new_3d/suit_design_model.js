define(['__tests__/new_3d/utils'], (utils) => {


    const addSuitGarment = (orders = window.orders) => {
        orders.customers()[0].garmentsStep.incrementGarment('6');
        orders.selectedCustomer().garmentsDesignStep.selectedGarment(orders.globalGarments.suit()[0].garment);
    };

    /**
    * Check for entire current 3D and check if all images are ok.
    * @param {*} image3D
    * @returns {{result: boolean, missing: String}}
    */
    function check3D(image3D) {
        let response = { result: true, missing_images: [] };

        // this keys are list, not a string
        const exceptions = [
            "suitJacketDefaultImagesFront",
            "suitJacketDefaultImagesBack",
            "suitJacketDefaultImagesSleeve",
            "suitJacketDefaultImagesMonogram",
            "suitPantsDefaultImage",
            "suitPantsDefaultImageBack",
        ];

        for (const key of exceptions) {
            if (image3D.hasOwnProperty(key)) {
                const attribute = image3D[key];

                if (typeof attribute === "function") {
                    for (const _string of attribute()) {
                        if (!utils.imageExists(_string)) {
                            // return { result: false, missing: _string };
                            response.result = false;
                            response.missing_images.push(_string);
                        }
                    }
                }

            }
        }

        const keys_to_check = [
            "selectedSuitJacketBackUpper",
            "selectedSuitJacketBh",
            "selectedSuitJacketBottom",
            "selectedSuitJacketBottom2",
            "selectedSuitJacketBreastPocket",
            "selectedSuitJacketButtonsBHStyle",
            "selectedSuitJacketButtonsTrimming",
            "selectedSuitJacketButtonStyle",
            "selectedSuitJacketCollarTrimming",
            "selectedSuitJacketCollarUpperTrimming",
            "selectedSuitJacketLapel",
            "selectedSuitJacketLapelShadow",
            "selectedSuitJacketLapelTrimming",
            "selectedSuitJacketLiningColor",
            "selectedSuitJacketLiningColorFront",
            "selectedSuitJacketPipingColor",
            "selectedSuitJacketPocket",
            "selectedSuitJacketPocketColor",
            "selectedSuitJacketPocketTrimming",
            "selectedSuitJacketSleeve",
            "selectedSuitJacketSleeveBHColour1",
            "selectedSuitJacketSleeveBHColour2",
            "selectedSuitJacketSleeveBHColour3",
            "selectedSuitJacketSleeveBHColour4",
            "selectedSuitJacketSleeveBHColour5",
            "selectedSuitJacketSleeveButton1",
            "selectedSuitJacketSleeveButton1BH",
            "selectedSuitJacketSleeveButton1Zoom",
            "selectedSuitJacketSleeveButton2",
            "selectedSuitJacketSleeveButton2BH",
            "selectedSuitJacketSleeveButton2Zoom",
            "selectedSuitJacketSleeveButton3",
            "selectedSuitJacketSleeveButton3BH",
            "selectedSuitJacketSleeveButton3Zoom",
            "selectedSuitJacketSleeveButton4",
            "selectedSuitJacketSleeveButton4BH",
            "selectedSuitJacketSleeveButton4Zoom",
            "selectedSuitJacketSleeveButton5",
            "selectedSuitJacketSleeveButton5BH",
            "selectedSuitJacketSleeveButton5Zoom",
            "selectedSuitJacketSleeveThread1",
            "selectedSuitJacketSleeveThread2",
            "selectedSuitJacketSleeveThread3",
            "selectedSuitJacketSleeveThread4",
            "selectedSuitJacketSleeveThread5",
            "selectedSuitJacketStitchForLapel",
            "selectedSuitJacketStitchForPocket",
            "selectedSuitJacketTicketPocket",
            "selectedSuitJacketTicketPocketTrimming",
            "selectedSuitJacketVent",
            "selectedSuitPantBackPockets",
            "selectedSuitPantBeltAndCuffs",
            "selectedSuitPantBeltAndCuffsBack",
            "selectedSuitPantCuffs",
            "selectedSuitPantCuffsBack",
            "selectedSuitPantFrontPockets",
            "selectedSuitPantPleatsBack",
            "selectedSuitPantPleatStyle",
            "selectedSuitPantStyle",
            "suitJacketVent2",
        ];

        for (const key of keys_to_check) {
            if (image3D.hasOwnProperty(key)) {
                const attribute = image3D[key];

                if (typeof attribute === "function") {
                    if (typeof attribute() === "string") {
                        if (!utils.imageExists(attribute())) {
                            // return { result: false, missing: attribute() };
                            response.result = false;
                            response.missing_images.push(attribute());
                        }
                    }
                }

            }
        }

        return response;

    }

    return { addSuitGarment, check3D }

});