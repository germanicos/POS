
// Requirejs Configuration Options
require.config({
    // to set the default folder
    baseUrl: '../www/scripts/',
    // paths: maps ids with paths (no extension)
    paths: {
        // RequireJS plugin
        text: 'libs/require/text',
        domReady: 'libs/require/domReady',
        // jQuery
        jquery: 'libs/jquery/jquery-1.8.2',
        jqueryui: 'libs/jquery/jquery-ui',
        jquerytouchpunch: 'libs/jquery-ui-touch-punch',
        jclass: 'libs/jquery.class',
        jqmdialog: 'libs/jqmSimpleDialog/jquery.mobile.simpledialog.min',
        // jQuery Mobile framework
        jqm: 'libs/jquery.mobile/jquery.mobile-1.4.5',
        jqmrouter: 'libs/jquery.mobile.router/jquery.mobile.router',
        knockout: 'libs/knockout/knockout-2.2.0',
        // knockout: 'libs/knockout/knockout-3.4.2',
        jqueryuk: 'libs/jquery/knockout-jqueryui',
        date: 'libs/jquery/datepicker',
        jqmSM: 'libs/jqm.slidemenu',
        kojqm: 'libs/kojqm',
        komap: 'libs/ko.mapping',
        iscroll: 'libs/iscroll',
        jqmSlide: 'libs/jqmSlide',
        jgrowl: 'libs/jquery.jgrowl.min',
        fscreen: 'libs/jquery.fullscreen',
        router: 'router',
        base: 'appBase',
        app: 'app',
        login: 'login',
        signature: 'signature/jp_signature2',
        trie: 'libs/trie',
        raphael: 'libs/raphael',
        slick: 'libs/slick',
        slide: 'libs/itemslide',
        sketchpad: 'libs/raphael.sketchpad',
        html2canvas: 'libs/html2canvas',
        hammer: 'libs/hammerJs/hammer',

        // jasmine
        jasmine: 'libs/jasmine-3.5.0/jasmine',
        'jasmine-html': ['libs/jasmine-3.5.0/jasmine-html'],
        'jasmine-boot': ['libs/jasmine-3.5.0/boot'],
    },
    // shim: makes external libraries compatible with requirejs (AMD)
    shim: {
        'jasmine-html': {
            deps: ['jasmine']
        },
        'jasmine-boot': {
            deps: ['jasmine', 'jasmine-html']
        }
    }
});

const configureGlobals = () => {
    window.fabricsData = {};
    window.fabricsData.store = [];

    window.authCtrl.userInfo = {
        "user_id": "24",
        "username": "luiz",
        "password": "5ce285172523591aeb311426898bdc20",
        "first_name": "Luiz",
        "last_name": "Test",
        "city": "6",
        "address": null,
        "email": "luiz@tailor.com.au",
        "sms_phone": "558398899826",
        "mobile_phone": "558398899826",
        "class_id": "1",
        "comments": null,
        "abn": null,
        "company": null,
        "bsb": null,
        "acct_number": "",
        "bank": null,
        "swift_code": null,
        "percentage": "10.00",
        "goal": "0.00",
        "date_added": "2014-07-08 00:00:00",
        "last_seen": "2014-07-08 00:00:00",
        "last_modified": "2014-07-08 00:00:00",
        "is_active": "1",
        "device_id": "D52AFA2A-169D-4823-B0B9-D19054B5FE91",
        "last_edited": "2019-06-20 15:32:34",
        "user_image_path": "https://shirt-tailor.net/thepos/theme/images/users/luiz.jpg",
        "user_image_path_2": "",
        "work_title": "Personal Stylist",
        "booking_flag": "0",
        "hourly_rate": "0.00",
        "is_test_user": "1"
    }

    window.device = {
        "available": false,
        "platform": null,
        "version": null,
        "uuid": null,
        "cordova": null,
        "model": null,
        "manufacturer": null,
        "isVirtual": null,
        "serial": null
    }
}

require(['require', 'jquery', 'jqmrouter', /* 'jqueryui' */, 'login', 'router'], function (require) {
    require(['require', 'knockout', 'iscroll', 'komap', 'jclass', 'libs/jqmExternalAsEmbeddedPages', 'jqmSlide', 'fscreen', 'jgrowl',
        'base', 'signature', 'trie', 'jqm', 'raphael', 'html2canvas'], function (require, ko, iscroll, komap) {
            window.ko = ko;
            require(['require',
                'jqmdialog',
                'jqueryuk',
                'sketchpad',
                'slick',
                'slide',
                'jquerytouchpunch',
                "libs/reportVaidityPolyfill",
                "libs/jquery_confirm/jquery_confirm",
                'hammer',


                "utils/utils",

                'viewmodel/AuthControl',
                'viewmodel/CustomerCntClass',
                'viewmodel/FabricsCntClass',
                'viewmodel/Bodyshape',
                'viewmodel/OrderItemSelectGarment',
                'viewmodel/Measurements',
                'viewmodel/FitlinesJacket',
                'viewmodel/FitlinesPants',
                'viewmodel/Fitting',
                'viewmodel/Completion',
                'viewmodel/Alterations',
                'viewmodel/AlterationsVM',
                'viewmodel/AlterationsBriefVM',
                'viewmodel/BriefDetailsVM',
                'viewmodel/AlterationsReportListVM',
                'viewmodel/AlterationsReportSubmissionVM',
                'viewmodel/AlterationReport',
                'viewmodel/AlterationMainPageVM',
                'viewmodel/ShippingFabric',
                'viewmodel/ShippingCopyGarment',
                'viewmodel/ShippingFaultyRemakeGarments',
                'viewmodel/ShippingOther',
                'viewmodel/ShippingData',
                'viewmodel/ShippingBox',
                'viewmodel/ShippingBoxesModel',
                'viewmodel/ShippingBoxesViewModel',
                'viewmodel/AlterationListsViewModel',
                'viewmodel/FittingPreviewVM',
                'viewmodel/ErrorReport',
                'viewmodel/MediaControlClass',
                'viewmodel/Notifications',
                'viewmodel/CustomProperties',
                'viewmodel/AlterationReportVM',
                'viewmodel/garments/GarmentsSuit',
                'viewmodel/garments/GarmentsVest',
                'viewmodel/garments/GarmentsShirt',
                'viewmodel/garments/GarmentsJacket',
                'viewmodel/garments/GarmentsPant',
                'viewmodel/garments/GarmentsShirt',
                'viewmodel/ActiveAlterationsVM',
                'viewmodel/Shipping_incomingBoxesVM',
                'viewmodel/Shipping_missingItemsVM',
                'viewmodel/Shipping_incomingBoxHistoryVM',
                'viewmodel/Shipping_outgoingBoxHistoryVM',
                'viewmodel/Shipping_outgoingBoxesVM',
                'viewmodel/Shipping_trackingVM',
                'viewmodel/PreOrdersVM',
                'viewmodel/OrdersVM',
                'viewmodel/GarmentsStepVM',
                'viewmodel/MeasurementsStepVM',
                'viewmodel/BodyShapesStepVM',
                'viewmodel/UploadImagesStepVM',
                'viewmodel/UploadVideosStepVM',
                'viewmodel/GarmentsDesignStepVM',
                'viewmodel/Image3DVM',
                'viewmodel/PaymentsStepVM',
                'viewmodel/OrderProcessVM',
                'viewmodel/OrderStepsRequirementsVM',
                'viewmodel/UnlockCustomersFittingsVM',
                'viewmodel/OrderOverviewVM',
                'viewmodel/PendingGroupOrdersVM',

                'viewmodel/UniformCompanyVM',
                'viewmodel/UniformTemplateVM',
                'viewmodel/UniformOrderVM',
                'viewmodel/UniformGarmentsStepVM',
                'viewmodel/UniformGarmentsDesignStepVM',
                'viewmodel/UniformPortfolioVM',
                'viewmodel/UniformFinalizeStepVM',
                'viewmodel/UniformCompanySearchVM',

                'viewmodel/UniformOrderGarmentsStepVM',
                'viewmodel/UniformOrderCustomerDetailsVM',
                'viewmodel/UniformOrderContactPersonVM',
                'viewmodel/UniformOrderPaymentsVM',
                'viewmodel/UniformOrderBodyShapesStepVM',
                'viewmodel/UniformOrderUploadImagesStepVM',
                'viewmodel/UniformOrderUploadVideosStepVM',
                'viewmodel/UniformStatisticsVM',
                'viewmodel/TasksVM',

                'viewmodel/ProfitabilityVM',
                'viewmodel/EmployeeInvoiceVM',
                'viewmodel/ConfigGlobalVarsVM',

                'viewmodel/QualityControlVM',
                'viewmodel/GenericErrorVM',

            ], function (require) {

                // Clear local storage
                localStorage.clear();

                posChangePage = function posChangePage(page) {
                    console.log("posChangePage killed");
                }

                // do not remove!
                authCtrl = new AuthControl();

                $.mobile.defaultPageTransition = 'slide';
                ko.mapping = komap;

                config_global_vars = new ConfigGlobalVarsVM();

                require(['app'], () => {
                    configureGlobals();


                    authCtrl.username("luiz");
                    authCtrl.password("medaumpass");
                    authCtrl.login();


                    sleep = async function (time) {
                        return new Promise((resolve) => setTimeout(resolve, time));
                    }


                    require(['jasmine-boot'], function () {
                        require([
                            '__tests__/new_3d/Pants3D.test',
                            '__tests__/new_3d/Vest3D.test',
                            '__tests__/new_3d/Jacket3D.test',
                            '__tests__/new_3d/Suit3D.test',

                        ], function () {
                            //trigger Jasmine
                            window.onload();
                        })
                    });
                });



            });
        });
});

