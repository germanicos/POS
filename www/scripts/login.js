define(['jquery', 'knockout'], function($, ko) {
	
	$('#login').on("keypress ",function(){
		handleLoginButton();
	});
	
	$('#password').on("keypress ",function(){
		handleLoginButton();
	});
	
	$('#login').on("keyup",function(){
		handleLoginButton();
	});
	
	$('#password').on("keyup",function(){
		handleLoginButton();
	});
	
	$('#login').on("keydown",function(){
		handleLoginButton();
	});
	
	$('#password').on("keydown",function(){
		handleLoginButton();
	});
	
	$('#login').on("change ",function(){
		handleLoginButton();
	});
	
	$('#password').on("change",function(){
		handleLoginButton();
	});
	
	$("#loginbtn").click(function(){
		authCtrl.username($('#login').val());
		authCtrl.password($('#password').val());    
		authCtrl.userInfo = {};	
		authCtrl.login();
	});
	
	handleLoginButton = function handleLoginButton(){
	//	if($('#login').val().length > 0 && $('#password').val().length > 0){
	//		$("#loginbtn").show();
	//	}
	//	else{
		//	$("#loginbtn").hide();
	//	}
	}
	
	posChangePage = function posChangePage(page) {
		
		try {
			if(page.charAt(0) == '#'){
				var pagehtml = page.slice(1) + ".htm";
				var pageName = page.slice(1);
				isSpecialCase(pageName);
				targetPage = pageName;
				if(finished)
					$.mobile.pageContainer.pagecontainer("change", pagehtml);
			}
		} catch(e) {
			console.log("error", e)
		}
	}



	$('#login').val('');
	$('#password').val('');
	handleLoginButton();
	
});










