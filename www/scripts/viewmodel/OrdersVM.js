define(['jquery', 'knockout', 'base'], function($, ko) {

    /**
     * This class will receive the customers ids that we will use for the
     * order process.
     *
     * isReorder => flag boolean, tells if the order will be a brand new order (false) or
     *     if we are finalizing the process for a absent customer during the first order process.
     *
     * reorderData => the data abour the orders to be recovered and recreate in here.
     */
    OrdersVM = SimpleControl.extend({

        init: function(customersIds, leaderId, preorderCustomersInfo, leaderPic, groupName = "", isReorder = false, reorderData = false, groupId = undefined) {

            console.log("Init OrdersVM")
            var self = this;

            this.customers = ko.observableArray([]);
            this.selectedCustomer = ko.observable(false);

            this.steps = this.buildSteps();
            this.selectedStep = ko.observable(this.steps[0]);

            // Contains all the fabrics
            this.fabricsData = fabricsData.store;

            console.log('preorderCustomersInfo', preorderCustomersInfo);

            this.preorderCustomersInfo = preorderCustomersInfo;

            this.leaderPic = ko.observable(leaderPic);

            this.groupName = groupName;
            this.groupId = groupId;

            this.orderStepRequirementsVM = new OrderStepsRequirementsVM(this);

            this.isReorder = isReorder;
            this.reorderData = reorderData;

            this.days = ko.observableArray([]);
            // for (i = 1; i <= 31; i++) this.days.push(i.toString());
			for (i = 1; i <= 31; i++) {
				// if number is 1,2,3,4...9 => convert to 01, 02, 03 ... 09
				let formatNumber = i.toString();
				if (formatNumber.length < 2){
					formatNumber = "0"+formatNumber;
				}
				this.days.push(formatNumber);
			};

            this.months  = ko.observableArray([{"name":"January","id":"1"},{"name":"February","id":"2"},{"name":"March","id":"3"},{"name":"April","id":"4"},{"name":"May","id":"5"},{"name":"June","id":"6"},{"name":"July","id":"7"},{"name":"August","id":"8"},{"name":"September","id":"9"},{"name":"October","id":"10"},{"name":"November","id":"11"},{"name":"December","id":"12"}]);

            this.years = ko.observableArray([]);
            const currentDate = new Date();
            for (i = currentDate.getFullYear()+3; i >= currentDate.getFullYear(); i--) this.years.push(i.toString());

            this.kissingButtonsExtraCost = ko.observable('30');

            this.globalDesignCategoriesArray = ko.observableArray([]);
            this.globalGarments = this.getGlobalGarmentsObj();
            this.globalDesignCategories = null;

            this.globalGarmentsArray = this.getGlobalGarmentsArray();

            // Tells the sytem the current garment type that is being desinged
            this.currentDesigingGarmentType = ko.observable(false);

            // Tells the 3D which garment to show
            this.currentGarmentInstance = ko.observable(false);

            this.currentDesignStep = ko.observable(false);

            // about special event
            this.is_specialEvent = ko.observable(false);
            // set special event to zero if new value is FALSE
            this.is_specialEvent.subscribe((newValue)=>{
                if (!newValue){
                    this.is_specialEventDate("");
                }

                else {
                    // set initial date for 4 week and 1 day from now
                    const _4weeks_from_now = new Date(new Date().getTime() + 2505600000);

                    const formattedYear = _4weeks_from_now.getFullYear();
                    const formattedMont = ( "0" + (parseInt(_4weeks_from_now.getMonth())+1)  ).slice(-2);
                    const formattedDay = ("0" + _4weeks_from_now.getDate()).slice(-2);

                    this.is_specialEventDate(`${formattedYear}-${formattedMont}-${formattedDay}`);
                }
            });
            this.is_specialEventDate = ko.observable("");
            this.is_specialEventDate.subscribe(newValue => {

                // if special date is less than 4 weeks (2419200000 milsec)
                if(newValue != "" && new Date(newValue).getTime() - new Date().getTime() <= 2419200000){
                    customAlert("You set 'special date' for less than 4 weeks. Remember to set 'urgent date' too");
                }
            });

            this.orderOverview = new OrderOverviewVM(this);

            // This should be last line
            this.getCustomersInfo(customersIds, leaderId);

            // flag to control process of saving order.
            // this flag will avoid 2 requisitions overlap the other
            // in other words, only save when this flag is false
            // this flag is true when one process of saving is "going on"
            this.orderIsSaving = false;

            // do not save while reorder
            if (!this.isReorder){
                // save order every x milliseconds
                clearInterval(this.saveOrder);
                this.saverOrder = setInterval( () => {
                    this.saveIncompleteOrder(false);
                }, 20000);
            }


        },

        /**
         * This will be a reference to the array of garments that we are using.
         *
         * This will only be a easier way of accessing the garment design options
         *
         */
        getGlobalGarmentsObj : function() {

            let garments = {
                    "jacket" : ko.observableArray([]),
                    "pants" : ko.observableArray([]),
                    "suit" : ko.observableArray([]),
                    "vest" : ko.observableArray([]),
                    "shirt" : ko.observableArray([])
                }

            return garments;
        },

        /**
         * Gets a helper structure to control the garments and the selected cateogry
         * for each one
         */
        getGlobalGarmentsArray : function() {

            let garments = ko.observableArray([]);

            garments.push( {
                    'garmentType' : 'jacket',
                    'selectedCategory' : ko.observable(false)
                    });

            garments.push( {
                    'garmentType' : 'pants',
                    'selectedCategory' : ko.observable(false)
                    });


            garments.push( {
                    'garmentType' : 'suit',
                    'selectedCategory' : ko.observable(false)
                    });

            garments.push( {
                    'garmentType' : 'vest',
                    'selectedCategory' : ko.observable(false)
                    });

            garments.push( {
                    'garmentType' : 'shirt',
                    'selectedCategory' : ko.observable(false)
                    });

            return garments;
        },

        /**
         * We will use this variable to have the option of selection the
         * design option for multiple garments at the same time.
         */
        getGlobalDesignCategories : function(garments_design) {
            // clear arrays
            this.globalDesignCategories = [];
            this.globalDesignCategoriesArray([]);

            const self = this;

            let garments = {
                    "jacket" : {},
                    "pants" : {},
                    "suit" : {},
                    "vest" : {},
                    "shirt" : {}
                }

            console.log("garments_design", garments_design);

            for( let garmentindex in garments_design )
            {
                const garmentType = garments_design[garmentindex];

                const garment_name = garmentType.garment_name.toLowerCase();

                for( let category of garmentType.categories )
                {
                    const cat_id = category.id;

                    category.options_map = {};
                    category.completed = self._getComputedCompletedCategories(category, garment_name);

                    if (category.completed() && this.isReorder){
                        category.visited = ko.observable(true);
                    }
                    else {
                        category.visited = ko.observable(false);
                    }

                    for( let option of category.options)
                    {
                        option.selectedValue = ko.observable(false);

                        category.options_map[option.id] = option;

                        if( option.values != undefined )
                        {
                            for(let value of option.values)
                            {
                                value.selected = ko.observable(false);
                                value.clicked = ko.observable(false);
                            }
                        }

                    }

                    garments[garment_name][cat_id] = category;
                }
            }


            // Also, gets the array representation of the same structure
            for( let garmentindex in garments_design )
            {
                const garmentType = garments_design[garmentindex];
                const garment_name = garmentType.garment_name.toLowerCase();


                this.globalDesignCategoriesArray.push( { 'garmentType' : garment_name, 'categories' : garmentType.categories } );
            }

            return garments;
        },

        /**
         * Chooses the garment type that the salesman is desining at this point.
         * @return {[type]} [description]
         */
        chooseDesignGarmentType : function(garmentType) {

            if( !garmentType )
            {
                return;
            }

            console.log("desining", garmentType);

            this.currentDesigingGarmentType(garmentType);

            // sort garment by customers (first leader then by customer.full_name)
            this.globalGarments[garmentType.garmentType].sort((a, b) => {
                if (a.customer.is_leader){
                    return -1;
                }
                else if(b.customer.is_leader){
                    return 1;
                }

                return a.customer.full_name < b.customer.full_name ? -1 : 1;
            });

            // set current garment instance as the first garment of current type
            this.currentGarmentInstance(this.globalGarments[garmentType.garmentType]()[0]);

                // select first category
                const currentGarmentTypeName = this.currentDesigingGarmentType().garmentType;
                const currentGarmentTypeDesignSteps = this.globalDesignCategoriesArray().filter( e => e.garmentType == currentGarmentTypeName)[0];
                nextStep = currentGarmentTypeDesignSteps.categories[0];
                // Finally, choose the next step.
                this.chooseCurrentDesignStep(nextStep);
        },

        chooseCurrentDesignStep : function(designStep) {

            // In case of multiples garments, change to the first one
            const garmentType = this.currentDesigingGarmentType().garmentType;
            this.currentGarmentInstance(this.globalGarments[garmentType]()[0]);

            if(designStep == undefined)
            {
                console.log("UNDEFINED chooseCurrentDesignStep", designStep);
                return;
            }

            console.log("desining", designStep);
            this.currentDesignStep(designStep);
            designStep.visited(true);

            this.renderCustomTagsImages();

            // change view by current category
            this._change3DView(designStep);
        },

        renderCustomTagsImages(){
            // if category is custom Data, call render for tagsHandler
            if(this.currentDesignStep().name.toLowerCase() === 'custom data'){
                this.selectedCustomer().garmentsDesignStep.customTagsHandlers().forEach(el => el.render());
            }
        },

        /**
         * In a category selection, this function changes the 3D view.
         * For exemple 1: in suit's monogram category, this method changes de 3d to monogram view
         *
         *
         * Another way to do this, is put a data-bind:click in each category tab
         */
        _change3DView(category) {

            // Change the "side" for all garments of the current type
            // get current type
            const currentType = this.currentDesigingGarmentType().garmentType;

            // get garments of current type
            const garments = this.globalGarments[currentType]();

            for (const garment_obj of garments) {

                const garment = garment_obj.garment;

                // monogram step ID for vest, jacket, suit respectively
                if (['27', '46', '63'].includes(category.id)) {
                    // suit
                    if (category.id == '63') {
                        garment.image3D.side('jacket_monogram');

                        // vest and jacket
                    } else {
                        garment.image3D.side('monogram');

                    }

                    // sleeve
                } else if (category.id == '7') {
                    // '7' is ID fot jacket Sleeve
                    garment.image3D.side('sleeve');
                } else if (category.id == '54') {
                    // '54' is ID fot suit Sleeve
                    garment.image3D.side('jacket_sleeve');
                }

                // back
                else if (["36", "5", "17", "26", "51", "61"].includes(category.id)) {
                    // suit
                    if (category.garment_id == "6") {

                        if ("51" == category.id) {
                            garment.image3D.side('jacket_back');
                        } else {
                            garment.image3D.side('pant_back');
                        }
                    }

                    // others
                    else {
                        garment.image3D.side('back');
                    }
                }

                // shirt contrast
                else if (category.id == "38") {
                    garment.image3D.side('contrast');

                }

                // front
                else {
                    // suit
                    if (category.garment_id == '6') {
                        // pants category
                        if (['58', '59', '60', '61', '62', '63', '64', '70'].includes(category.id)) {
                            garment.image3D.side('pant_front');
                        } else {
                            garment.image3D.side('jacket_front');
                        }

                        // others
                    } else {
                        garment.image3D.side('front');
                    }
                }


            }
        },


        /**
         * Builds the order data for submission that will be sent to the ERP.
         *
         * Each step view model will create its own data
         * Gets the data from each step;
         * @return {[type]} [description]
         */
        buildOrderSubmissionData : function() {

            const ordersArray = [];

            for( let customer of this.customers() )
            {
                const customer_obj = this.getCustomerSubmissionData(customer);
                const measurements = customer.measurementsStep.getSubmissionData();
                const bodyshapes   = customer.bodyShapesStep.getSubmissionData();
                const images       = customer.uploadImagesStep.getSubmissionData();
                const videos       = customer.uploadVideosStep.getSubmissionData();
                const payments     = customer.paymentsStep.getSubmissionData();
                const garments     = customer.garmentsDesignStep.getSubmissionData(); // Special Structure

                // Will be reorder if the customer is not locked (is_present) and the ISREORDER flag is TRUE
                const is_reorder   = this.isReorder && !customer.is_locked;

                const order = {
                        'customer'            : customer_obj,
                        'measurements'        : measurements,
                        'bodyshapes'          : bodyshapes,
                        'images'              : images,
                        'videos'              : videos,
                        'payments'            : payments,
                        'garments'            : garments,
                        'is_reorder'          : is_reorder,
                        'group_id'            : this.groupId,
                        'is_specialEvent'     : this.is_specialEvent(),
                        'is_specialEventDate' : this.is_specialEventDate(),
                    }

                ordersArray.push(order);
            }

            return ordersArray;
        },

        showPopUpMissingInfo : function(){


            // Checks requirements before submission
            const checkSubmissionRequirmentsResult = this.orderStepRequirementsVM.checkSubmissionRequirments();
            $('#requiredBeforeSubmitOrder').show(500);

            // if (!checkSubmissionRequirmentsResult){
            //     // show PopUp
            //     $('#requiredBeforeSubmitOrder').show(500);
            // }
            // else{
            //     this.submitOrder();
            // }
        },

        submitOrder : function() {

            var self = this;

            if (!this.allClientsPaymentComplete(true)){
                return;
            }

            // // Replace with notification later
            // if( !confirm('Do you really want to submit the order ? (PLEASE CHECK PAYMENT DETAILS FOR ALL CUSTOMERS)') )
            // {
            //     return;
            // }

            self.orderIsSaving = true;
            const orderData = this.buildOrderSubmissionData();
            const device_id = device.uuid;

            $('#loading_jp').show( () => {

                $.ajax({
                    type: 'POST',
                    timeout: 60000, // sets timeout to 60 seconds
                    url: BUrl + 'orders_pos/submit_order',
                    dataType: 'json',
                    data: {
                        "user": authCtrl.userInfo,
                        "order_data": JSON.stringify(orderData),
                        'device_id'    : device_id
                    },
                    success: function (dataS) {

                        $('#loading_jp').hide();

                        console.log(dataS);

                        if( dataS.result )
                        {
                            $.jGrowl("Order successfully submitted !");
                            posChangePage("#main");
                            self.orderIsSaving = false;

                            const new_order_id = dataS.details.order_id;

                            self.sendEmailToReferral(new_order_id);
                        }

                    },
                    error: function (error) {

                        $('#loading_jp').hide();

                        console.log(JSON.stringify(error));
                        customAlert("TIME OUT - there is a network issue. Please try again later");
                    },
                    async: false
                });

            });
        },


        /**
         * Send email to user who referred Germanicos to this current customer.
         *
         * Example: "Congratulations Fulano, your friend just ordered with us, in order to thank you you just won a Shirt"
         *
         * -> the backend must check if this is the first order of the current client
         * -> the backend must also check if this customer was referred by another
         */
        sendEmailToReferral : function(new_order_id){
            const customers_ids = [];
            for (const customer of this.customers())
            {
                customers_ids.push(customer.customer_id);
            }

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/send_referral_email',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "customers": customers_ids,
                    "new_order_id": new_order_id,
                },
                success: function (dataS) {
                    // empty
                },
                error: function (error) {
                    console.error(JSON.stringify(error));
                },
                async: true,
            });
        },


        /**
         * Filters some data related to each customer
         * and just return the essencial
         * @param  {[type]} customer [description]
         * @return {[type]}          [description]
         */
        getCustomerSubmissionData : function(customer) {

            const reorder_data = customer.reorder_data ? customer.reorder_data : false;

            const customerFiltered = {
                    'customer_id'  : customer.customer_id,
                    'is_leader'    : customer.is_leader,
                    'is_paying'    : customer.is_paying,
                    'is_present'   : customer.is_present,
                    'is_locked'    : customer.is_locked,
                    'leader_pic'   : customer.is_leader ? this.leaderPic() : "",
                    'group_name'   : this.groupName,
                    'group_id'     : this.groupId,
                    'reorder_data' : reorder_data
                }

            return customerFiltered;
        },




        buildSteps: function() {

            return [
                    {
                        'name' : 'Select Garments',
                        'id' : '1',
                        'complete': false
                    },
                    {
                        'name': 'Measurements',
                        'id' : '2',
                        'complete': ko.observable(false)
                    },
                    {
                        'name': 'Upload Images',
                        'id' : '4',
                        'complete': ko.observable(false)
                    },
                    {
                        'name': 'Upload Fitline Video',
                        'id' : '5',
                        'complete': ko.observable(false)
                    },
                    {
                        'name': 'Body Shape',
                        'id' : '3',
                        'complete': ko.observable(false)
                    },
                    {
                        'name': 'Design',
                        'id': '6',
                        'complete': ko.observable(false)
                    },
                    {
                        'name': 'Payments',
                        'id': '7',
                        'complete': ko.observable(false)
                    }
                ];
        },

        stepBack : function() {

            console.log("Steping back...");

            const currentStep = this.selectedStep();
            const currentStepId = currentStep.id;

            if(currentStepId <= 1) {
                posChangePage("#main");
                return;
            }

            this.changeStep(currentStepId - 1);
        },

        /**
         * We need to check if the step is completed before advancing to the next one
         * @return {[type]} [description]
         */
        nextStep: function() {

            const currentStep = this.selectedStep();
            const currentStepId = currentStep.id;

            try {
            // if( currentStepId >= 7 ) { alert('You are in the last step'); return; }

            // const nextStep = this.steps.filter( e => e.id == currentStepId + 1)[0];

            // if in measurements step
            if( currentStepId == 2)
            {
                // for each customer that is not locked and is present => sync measuremnets
                for( let customer of this.customers().filter( e => !e.is_locked && e.is_present ) )
                {
                    customer.measurementsStep.syncMeasurements();
                }

            }


            console.log('Next BTN...');
            this.orderStepRequirementsVM.nextBtn(currentStepId);


            // scroll back to the top
            $('html,body').animate({
                scrollTop: 0
            }, 700);

            /* // Back to top except in design
            if (currentStepId != '6') {
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            } */

                 // save order
                this.saveIncompleteOrder(false);

            } catch (error) {
                alert(error.name + ': ' + error.message);
            }

        },

        cancelOrder : function() {

            const confirmAction = () => {
                this.saveIncompleteOrder();

                // do not save order anymore
                clearInterval(this.saverOrder);

                posChangePage('#main');
                $.jGrowl('Order cancelled');

            };

            pos_warning({msg:"Do you really want to cancel this order ?", confirmAction});

        },

        changeStep: function(stepId) {

            $('#loading_jp').show( () => {

                console.log("changing step: ", stepId);

                // Get first element whose id equals to stepId
                const newStep = this.steps.filter( e => e.id == stepId)[0];

                console.log("new step", newStep);

                // check if has any garment =============
                let hasAnyGarment = false;
                for (const garment in this.globalGarments) {
                    if (this.globalGarments[garment]().length > 0){
                        hasAnyGarment = true;
                        break;
                    }
                }

                if (newStep.id == '6' && !hasAnyGarment){
                    document.getElementById('loading_jp').style.display = "none";
                    $.jGrowl("Please, set a garment to a client");
                    return;
                }
                // =======================================
                this.selectedStep(newStep);


                // call toggleStepComplete of all steps
                this.selectedCustomer().garmentsStep.toggleStepComplete();
                this.selectedCustomer().measurementsStep.toggleStepComplete();
                this.selectedCustomer().bodyShapesStep.toggleStepComplete();
                this.selectedCustomer().uploadImagesStep.toggleStepComplete();
                this.selectedCustomer().uploadVideosStep.toggleStepComplete();
                this.selectedCustomer().garmentsDesignStep.toggleStepComplete();


                // render bodyshapes
                this.selectedCustomer().bodyShapesStep.showBodyShape();

                // Design step => Choose the first garment as current one
                if( stepId == '6')
                {
                    // If no garment is selected, select the first one
                    if( !this.currentDesigingGarmentType() )
                    {
                        // Gets the first category that has garments
                        this.chooseDesignGarmentType(this.globalGarmentsArray().filter( gar => this.globalGarments[gar.garmentType]().length > 0)[0]);
                    }

                    // select first category
                    const currentGarmentTypeName = this.currentDesigingGarmentType().garmentType;
                    const currentGarmentTypeDesignSteps = this.globalDesignCategoriesArray().filter( e => e.garmentType == currentGarmentTypeName)[0];

                    nextStep = currentGarmentTypeDesignSteps.categories[0];
                    // Finally, choose the next step.
                    this.chooseCurrentDesignStep(nextStep);

                }
                // Payments => send saved designs to ERP
                else if( stepId == '7')
                {
                    this.orderStepRequirementsVM.sendSavedDesigns();
                }

                // Ui sugar
                $(".order-step:visible").effect("slide", { direction: 'right' }, 500);

                // Get back to the first customer THAT IS NOT LOCKED and is present
                const notLockedAndPresent = this.customers().filter( e => !e.is_locked && e.is_present)[0]
                if (notLockedAndPresent){
                    this.selectedCustomer(notLockedAndPresent);
                }else{
                    this.selectedCustomer(this.customers().filter( e => !e.is_locked)[0]);
                }


                document.getElementById('loading_jp').style.display = "none";
            }); // end step loader
        },


        changeCustomer(customerId) {
            var self = this;

            console.log("changing customer: ", customerId);
            const selectedCustomer = self.customers().filter( e => e.customer_id == customerId)[0];

            const _changeCustomer = () => {
            // Do nothing if the customer is the same
            if(self.selectedCustomer().customer_id == customerId) { return; }
            $('#loading_jp').show( () => {

                // Get first element whose id equals to customerId
                self.selectedCustomer(selectedCustomer);

                // render bodyshapes
                this.selectedCustomer().bodyShapesStep.showBodyShape();

                document.getElementById('loading_jp').style.display = "none";
            });
            };

            if(selectedCustomer.is_locked)
            {
                customAlert("Sorry, The customer IS LOCKED");
                return;
            }

            if (!selectedCustomer.is_present){
                const confirmAction = () => {
                    _changeCustomer();
                };
                pos_warning({msg:'Selected customer is not present. Do you really want to continue??', confirmAction});
                return;
            }

            _changeCustomer();

        },

        buildCustomers : function(dataS){

            var self = this;

            // remove customers that are not in request
            for (const cust of this.customers()) {
                var present = false;

                for (const customer of dataS.customers) {
                    if (cust.customer_id == customer.customer_id) {
                        present = true;
                        break;
                    }

                }
                if (!present) {
                    console.log('removing older customers');
                    this.customers.remove(cust);

                    // also remove from global garments
                    for (const garmentType in this.globalGarments) { // garmentType == ["jacket", "suit", "vest", ...]
                        this.globalGarments[garmentType].remove(function(gar){
                            // Check if the garment was from a customer that no longer exists
                            console.log('removing garment from old order');
                            return (gar.customer.customer_id == cust.customer_id)
                        });
                    }

                    // Unselect the garments to avoid pointing to a zombie garment
                    this.currentDesigingGarmentType(false);
                    this.currentGarmentInstance(false);
                    this.currentDesignStep(false);
                }
            }

            for (let customer of dataS.customers) {

                // pre building =============================
                // check if customers already in this instance
                let presentCustomer = this.customers().filter( el => el.customer_id == customer.customer_id)[0];

                if (presentCustomer) {
                    console.log('ignoring customers already present');
                    continue;
                }




                // start building =============================

                // Default value for the customers
                customer.is_leader = customer.is_leader !== undefined ? customer.is_leader : false;
                customer.is_paying = customer.is_paying !== undefined ? customer.is_paying : true;
                customer.is_present = customer.is_present !== undefined ? customer.is_present : true;

                // This flags tells if the customer can be selected
                customer.is_locked = false;

                // Unique identifier
                customer.unique_id = new Date().valueOf();

                customer.selectedStep = ko.observable(self.steps[0]);

                // Creates one model for each step
                customer.garmentsStep = new GarmentsStepVM(customer, self);
                customer.measurementsStep = new MeasurementsStepVM(customer, dataS.fitlines, self);
                customer.bodyShapesStep = new BodyShapesStepVM(dataS.bodyshapes, customer.latest_bodyshape, customer.garmentsStep);

                customer.uploadImagesStep = new UploadImagesStepVM(customer);
                customer.uploadVideosStep = new UploadVideosStepVM(customer);

                customer.garmentsDesignStep = new GarmentsDesignStepVM(customer, dataS.garments_design, self);

                customer.paymentsStep = new PaymentsStepVM(customer, dataS.payment_methods, dataS.pricing, dataS.discount_codes, self);

                /**
                 * If we are completing the process for a absent customer
                 */
                if (self.isReorder && customer.reorder_data) {
                    customer.unique_id = customer.reorder_data.customer.unique_id;


                    // Updates the variable IF IS PRESENT >>>>AT THE MOMENT<<<<
                    customer.is_present = self.reorderData.customers_data.filter(cust => cust.customer_id == customer.customer_id)[0].is_present;

                    // Can only edit the information if the customer is <<<<PRESENT AT THE MOMENT>>>>
                    // customer.is_locked = customer.is_present ? false : true; // do not lock customers for now

                    customer.is_paying = self.reorderData.customers_data.filter(cust => cust.customer_id == customer.customer_id)[0].is_paying;

                    // This will preselect the design values for the absent customer
                    customer.garmentsDesignStep.recoverGarments(customer.reorder_data.garments);

                    // populate globalGarments, with pre-existents clients
                    // We are doing this because in reOrder, there will be garments in customers object, but not in globalGarments
                    for (const garmentObject of  customer.garmentsStep.garments()) {
                        const garmentName = garmentObject.name;
                        for (const garmentFromCustomer of garmentObject.garments()) {
                            self.globalGarments[garmentName].push({
                                "customer"    : customer,
                                "garment"     : garmentFromCustomer,
                                "garmentType" : garmentName
                            })
                        }
                    }

                    // for some reason, the computed methods in paymentVM doesn't work. So this forces the bind again during reorder
                    customer.paymentsStep.regularCosts("");
                    customer.paymentsStep.totalOrderCost("");


                    // recover bodyshape.
                    customer.bodyShapesStep.copyBodyShapeFromPreviousOrder(customer.reorder_data.bodyshapes);

                    // Extra cost must be recovered inside paymentVM
                    // recover extra costs:
                    // for (const extraCost of customer.reorder_data.payments.extra_costs) {
                    //     const newExtraCost = customer.paymentsStep.getExtraCostObj(
                    //         description = extraCost.description,
                    //         value = extraCost.value
                    //     );
                    //     customer.paymentsStep.addExtraCost(newExtraCost);
                    // }
                    // recover payment.
                    console.log('customer', customer);
                    customer.paymentsStep.copyPaymentFromPreviousOrder(customer.reorder_data);

                    // special event
                    if (customer.reorder_data.special_date){
                        self.is_specialEvent(customer.reorder_data.special_date.is_specialEvent);
                        self.is_specialEventDate(customer.reorder_data.special_date.is_specialEventDate);
                    }
                }

                // push
                self.customers.push(customer);

                // sort (leader first, then sort by name)
                self.customers.sort((a, b) => {
                    if (a.is_leader) {
                        return -1;
                    }
                    else if (b.is_leader) return 1;

                    return a.full_name < b.full_name ? -1 : 1;
                });

                // get customer details
                self.setCustomerDetails(self.customers);
            }

        },

        prepare_order_success : function(dataS){
            console.log("sendSelectedClients result", dataS);

            var self = this;

            // Sets data from the preorder process

            this.buildCustomers(dataS);


            /*
                * Adds metadata for customer from the PREORDERS step:
                *     1. is_leader
                *     2. is_present
                *     3. is_paying
                */
            if( self.preorderCustomersInfo != undefined )
            {

                for(let preOrderCustomer of self.preorderCustomersInfo)
                {

                    let OrderCustomer = null;

                    // If the customer have server_id
                    if( preOrderCustomer.server_id != 'undefined' && preOrderCustomer.server_id > 0)
                    {
                        OrderCustomer = self.customers().filter(e => e.customer_id == preOrderCustomer.server_id)[0];

                    }
                    else // Find by name/last name
                    {
                        OrderCustomer = self.customers().filter(e => {
                                    return e.customer_first_name == preOrderCustomer.customer_first_name &&
                                            e.customer_last_name == preOrderCustomer.customer_last_name;

                                })[0];

                    }


                    if( OrderCustomer ) // If found => set the data
                    {
                        OrderCustomer.is_present = preOrderCustomer.is_present;
                        OrderCustomer.is_paying = preOrderCustomer.is_paying;
                        OrderCustomer.is_leader = preOrderCustomer.is_leader;
                    }
                }

                self.leaderPic();
            }
            else // if the preordercustomer info is not available => set the leader as the first customer
            {
                self.customers()[0].is_leader = true;
            }


            const leader = self.customers().filter( e => e.is_leader)[0];

            // Remove from the array
            self.customers.remove(leader);
            // Insert in the first postion
            self.customers.unshift(leader);


            self.selectedCustomer(self.customers().filter( e => !e.is_locked)[0]);

            if (!self.globalDesignCategories){
            self.globalDesignCategories = self.getGlobalDesignCategories(dataS.garments_design);
            }

            // init savedDesigns as a global variable
            try {
                savedDesigns = self.buildSavedDesigns(dataS.latest_saved_designs);
            } catch (error) {
                customAlert("Can not build latest saved designs");
                console.log('error', error);
            }
        },

        /**
         * Post to ERP. Send a list of Selected Customers
         * TODO: temporary only. The customers list must come from orderProcess.htm / OrderProcessVM
         */
        getCustomersInfo: function(customersIds, leaderId) {

            var self = this;
                //alert();
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/prepare_order',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "customers": JSON.stringify(customersIds),
                    "leader" : leaderId,
                    "reorderData" : JSON.stringify(self.reorderData)
                },
                success: function (dataS) {

                    self.prepare_order_success(dataS);

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                    posChangePage('#main');
                },
                async: false
            });


        },


        selectGarmentFabric : function(fabric, garment, alertStockLevel = true) {

            console.log("Selecting fabric" + fabric.title + " for garment: ", garment);
            garment.selectedFabric(fabric);
            garment.fabricInsearch(fabric.title);
            garment.fabricsInList([]);

            // Unset customer fabrics...
            garment.useCustomerFabric(false);
            garment.customerFabricImage(false);
            garment.customerFabricMeterage(false);

            // change fabric in 3d
            try {
                garment.image3D.changeFabric3d(fabric.title);
            } catch (error) {
                // probably using older version of 3d
                console.error(error);
            }

            // Applying the same fabric for selected fabricCheckboxes garments
            for(let gar of this.globalGarments[garment.garment_name.toLowerCase()]() )
            {
                if( gar.garment.fabricCheckbox() )
                {
                    gar.garment.selectedFabric(fabric);
                    gar.garment.fabricInsearch(fabric.title);
                    gar.garment.fabricsInList([]);

                    const fabric_category = gar.garment.categories.filter(function (el) { return el.name.toLowerCase() == 'fabric'})[0];
                    const options = fabric_category.options.filter(function(el){return el.name.toLowerCase() == 'fabric'})[0];
                    options.selectedValue(fabric.id);
                }

                gar.garment.fabricCheckbox(false);
            }



            const fabric_category = garment.categories.filter(function (el) { return el.name.toLowerCase() == 'fabric'})[0];
            const options = fabric_category.options.filter(function(el){return el.name.toLowerCase() == 'fabric'})[0];
            options.selectedValue(fabric.id);

            if( alertStockLevel && fabric.in_stock == '0' ){
                pos_warning({msg: "WARNING: THIS FABRIC IS CURRENTLY OUT OF STOCK !!"});
            }
            else if (alertStockLevel && fabric.is_collection_active == "0"){
                pos_warning({msg: "WARNING: THIS COLLECTION IS CURRENTLY INACTIVE !!"});
            }
            else if (alertStockLevel && fabric.is_mill_active == "0"){
                pos_warning({msg: "WARNING: THIS MILL IS CURRENTLY INACTIVE !!"});
            }

        },

        /**
         * Build an observable array and changes the keys of the objects to originals.
         * Basically, this method is an adapter
         *
         * (ex: "created_at -> date")
         * (ex2: user (string) -> user (object)
         * (ex3: customer (string) -> customer (object)
         *
         * @param {Array} latest_saved_designs saved designs from ERP
         */
        buildSavedDesigns: function(latest_saved_designs){
            console.log("building latest_saved_designs");

            console.log('latest_saved_designs', latest_saved_designs);

            var ret = [];

            latest_saved_designs.forEach(design => {
                let newDesign = {};

                newDesign.date      = design.created_at;
                newDesign.device_id = design.device_id;
                newDesign.customer  = {
                                        full_name           : design.customer_full_name,
                                        customer_id         : design.customer_id,
                                        customer_first_name : design.customer_first_name,
                                        customer_last_name  : design.customer_last_name
                                        };

                newDesign.user      = {
                                        username : design.user_name,
                                        user_id  :  design.salesman_id
                                        };

                newDesign.design    = {
                                        categories   : JSON.parse(design.design_json),
                                        garment_name : design.garment_name,
                                        garment_id   : design.garment_type,
                                        unique_id    : design.garment_unique_id
                                        };


                ret.push(newDesign);

            });


            return ko.observableArray(ret);
        },











        /**
         * Checks if all garments have all values selected for each category, thus we can mark the step with Green
         */
        _getComputedCompletedCategories : function(category, garment_name) {

            // console.log("computing... category", category);
            var self = this;

            return ko.computed(function() {

                let allGarmentsWithValue = true;

                // return completed for monogram steps
                if( category.name.toLowerCase().includes('monogram') ) { return true; }

                // Break point for nested for loops
                outsideBreakPoint:

                for(let garmentType of self.globalGarments[garment_name]())
                {
                    const gar = garmentType.garment;

                    for(let opt of gar.categories_map[category.id].options)
                    {

                        /**
                         * Lets not consider:
                         *
                         *     2 => float
                         *     3 => String
                         *     4 => boolean
                         *     9 => Btn colour
                         *
                         */
                        if(opt.type_id == 2 || opt.type_id == 3 || opt.type_id == 4 || opt.type_id == 9 || opt.type_id == 10) { continue; }


                        // If the main option does not have value selected => return false
                        // special cases:

                        // if selected sleeve option (cat_id == 31; opt_id == 89) is short (id == 1316), skip cuff option (id == 89)
                        if (gar.categories_map[31] && gar.categories_map[31].options_map[89].selectedValue() && gar.categories_map[31].options_map[89].selectedValue().id == "1316"
                        && opt.id == '92') {
                            allGarmentsWithValue = true;
                            break outsideBreakPoint; // Goes to the label and breaks the two nested for loops

                        }


                        // normal cases
                        else {
                            if( opt.selectedValue() == undefined || opt.selectedValue() == false || opt.selectedValue() == "" )
                            {
                                allGarmentsWithValue = false;
                                break outsideBreakPoint; // Goes to the label and breaks the two nested for loops
                            }
                        }
                    }
                }


                return allGarmentsWithValue;

            });
        },


        /**
         *
         * @param {List} categories
         * @param {String} garmentType ['1','2','3','4','6']
         */
        _getFabricFromCategoriesList : function(categories, garmentType){

            var fabricID;

            // pants
            if (garmentType == '1') {
                fabricID = categories.filter(el => el.id=='12')[0].options.filter(el=>el.id == '54')[0].selectedValue
            }
            // shirt
            else if (garmentType == '2') {
                fabricID = categories.filter(el => el.id=='29')[0].options.filter(el=>el.id == '86')[0].selectedValue
            }

            // vest
            else if (garmentType == '3') {
                fabricID = categories.filter(el => el.id=='20')[0].options.filter(el=>el.id == '71')[0].selectedValue
            }
            // Jacket
            else if (garmentType == '4') {
                fabricID = categories.filter(el => el.id=='2')[0].options.filter(el=>el.id == '5')[0].selectedValue
            }
            // Suit
            else if (garmentType == '6') {
                fabricID = categories.filter(el => el.id=='47')[0].options.filter(el=>el.id == '156')[0].selectedValue
            }

            // get fabric by ID
            const fabricToReturn = fabricsData.store.filter(el => el.id == fabricID)[0];

            return fabricToReturn ? fabricToReturn.title : 'unknown';

        },

        /**
         * For all paying customers check if
         * - selectedPaymentMethod() is present
         * - selectedOrderCity() is present
         * - selectedFittingCity() is present
         * - terms.accepted() is true
         * - signing() is present
         *
         * if not, alert user to complete this info
         *
         */
        allClientsPaymentComplete: function (showAlerts = true) {
            function showAlertsFunc(alert, show=true) {
                if (show){
                    customAlert(alert);
                }
            }


            for (const customer of this.customers()) {
                if (customer.is_paying == true && !customer.is_locked){

                    // checking
                    if (!customer.paymentsStep.selectedPaymentMethod()) {
                        showAlertsFunc(`Please, fill payment method for customer ${customer.full_name} `, showAlerts);
                        return false;
                    }
                    if (!customer.paymentsStep.selectedOrderCity()) {
                        showAlertsFunc(`Please, fill order city for customer ${customer.full_name} `, showAlerts);
                        return false;
                    }
                    if (!customer.paymentsStep.terms.accepted()) {
                        showAlertsFunc(`Please, check if customer ${customer.full_name} accepted terms`, showAlerts);
                        return false;
                    }
                    if (!customer.paymentsStep.signing()) {
                        showAlertsFunc(`Please, fill signature for customer ${customer.full_name} `, showAlerts);
                        return false;
                    }
                    if (!customer.paymentsStep.selectedFittingCity()) {
                        showAlertsFunc(`Please, fill fill fitting city for customer ${customer.full_name} `, showAlerts);
                        return false;
                    }

                }
            }

            return true;
        },

        updateInstance: function(customersIds, leaderId){
            console.log('update order');

            var self = this;
            // get customers from ERP
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/prepare_order',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "customers": JSON.stringify(customersIds),
                    "leader" : leaderId,
                    "reorderData" : JSON.stringify(self.reorderData)
                },
                success: function (dataS) {
                    self.prepare_order_success(dataS);
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                    posChangePage('#main');
                },
                async: false
            });




        },

        saveIncompleteOrder : async function(printDialog = true){

            // do not save while reorder
            if (this.isReorder){
                return;
            }

            // ordersDefined is a global variable instantiate in router, and tells if we are in order step
            if (!ordersDefined){
                return;
            }

            var self = this;

            // do not send another requisition if another process of saving is going on
            if(self.orderIsSaving === true){
                console.log('previous request still not finished');
                return;
            }

            // also, do not save if we are in payment step
            // reason: avoid save order while another requisition is going on (eg: submission). Avoid problems with synchronization
            if (this.selectedStep().id == "7"){
                return;
            }



            if (printDialog)
                if( !confirm('Do you want to save the order ?') ) { return; }

            self.orderIsSaving = true;

            // get submission data from all VM of order
            new Promise((resolve, reject) => {
                console.log('Building save_order object');
                const orderData = this.buildOrderSubmissionData(); // inside a promise because in large orders, this line may cause lags to the system
                console.log('save_order object created');
                resolve(orderData);
            })
            .then((orderData) => {
            const device_id = device.uuid;

                $.ajax({
                    type: 'POST',
                    timeout: 60000, // sets timeout to 60 seconds
                    url: BUrl + 'orders_pos/save_incomplete_order',
                    dataType: 'json',
                    data: {
                        "user": authCtrl.userInfo,
                        "order_data": JSON.stringify(orderData),
                        'device_id'    : device_id
                    },
                    success: function (dataS) {

                        // $('#loading_jp').hide();

                        console.log(dataS);

                        if( dataS.result )
                        {
                            $.jGrowl("Order auto saved");
                        }

                        if( dataS.group_id){
                            console.log('Updating groupID: new id = ', dataS.group_id);
                            self.groupId = dataS.group_id;
                        }

                        // process of saving has finished
                        self.orderIsSaving = false;

                    },
                    error: function (error) {

                        // $('#loading_jp').hide();

                        $.jGrowl("ERROR : Could not auto save order.....trying again");
                        console.log('error auto saving order....',JSON.stringify(error));
                    },
                    async: true
                });

            })
            .catch(() => {
                console.warn("Could not save order !!!!!");
                self.orderIsSaving = false;
            });

        },

        /**
         * get customer details from pre-order from localstorage and put in this VM
         * @param {*} customers
         */
        setCustomerDetails: function(customers){

            // the customer data is stored on localstorage
            const groupOrdersData = JSON.parse(localStorage.getItem("groupOrdersData"));

            // get data from reorder or first order
            const customersData = groupOrdersData.is_reorder ? groupOrdersData.reorder_data.customers_data : groupOrdersData.customers_data;

            for (const customer of customers()) {
                // get data of this specific customer
                const data = customersData.find(data => {
                    const customer_id = data.customer_id || data.server_id;
                    return customer.customer_id === customer_id;
                });

                customer.customerDetails = ko.mapping.fromJS({
                    ...data,
                    customer_DOB                   : data.customer_DOB.split("/").join("-"), // replace all
                    customer_address1              : data.customer_address1,
                    customer_address2              : data.customer_address2,
                    customer_country               : data.customer_country,
                    customer_state                 : data.customer_state,
                    customer_city                  : data.customer_city,
                    customer_postal_code           : data.customer_postal_code,
                    customer_mobile_phone          : data.customer_mobile_phone,
                    customer_landline_phone        : data.customer_landline_phone,
                    customer_email                 : data.customer_email,
                    customer_best_time_to_contact  : data.customer_best_time_to_contact,
                    customer_id                    : data.server_id || data.customer_id,
                    customer_first_name            : data.customer_first_name,
                    customer_last_name             : data.customer_last_name,
                });
            }
        }
    });
});