define(['jquery', 'knockout', 'base',], function ($, ko) {


    TailorsWorkLoad = class TailorsWorkLoad {
        constructor() {
            this.tailorsWorkLoad = ko.observableArray([]);
            this.getData();

            this.selectedTailor = ko.observable(false);
        }


        getData() {
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'alterations_pos/get_tailors_workload',
                dataType: 'json',
                data: { 
                    "user"         : authCtrl.userInfo,
                    'device_id'    : device.uuid
                },

                success: (dataS) => {
                    console.log("dataS", dataS)
                    this.tailorsWorkLoad(dataS);
                },

                error: function (error) {
                    console.warn("Error on alterations_pos/get_tailors_workload", error);
                    customAlert("Tailor workLoad could not be obtained, please try again");
                },

                async: false
            });
        }





    }

})