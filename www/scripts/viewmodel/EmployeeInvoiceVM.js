define(['jquery', 'knockout', 'base'], function($, ko) {
    /**
     * Main class to deal with entire employee invoice.
     *
     * If we this view became more complex, we must use COMPOSITION PATTERN
     */
    EmployeeInvoiceVM = class {

        constructor () {
            console.log('Init EmployeeInvoice');

            this.monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];

            this.currentMonth = ko.observable(new Date().getMonth()); // number (0,1,2, ...)
            this.currentMonthText = ko.computed(() => {
                return this.monthNames[this.currentMonth()];
            }); // string (eg: january)

            this.currentYear = ko.observable(new Date().getFullYear());

            this.currentYear.subscribe((newYear) => {
                // make a post to get invoices for this month
                this.setYear(newYear);
            });

            this.data = ko.observable();

            // get data from ERP and initialize view
            this._getData();

            // calculates total sold
            this.totalSold = ko.computed(() => {
                let total = 0;
                if (!this.data()){
                    return total
                }
                this.data().invoice_rows[this.currentMonth()+1].forEach(invoice => {
                    const parsedFloat = parseFloat(invoice.metadata.total_no_gst);
                    total += parsedFloat ? parsedFloat : 0;
                });

                return total.toFixed(2);
            });

            // calculates total commission
            this.totalCommission = ko.computed(() => {
                let total = 0;
                if (!this.data()){
                    return total
                }
                this.data().invoice_rows[this.currentMonth()+1].forEach(invoice => {
                    const parsedFloat = parseFloat(invoice.cost);
                    total += parsedFloat ? parsedFloat : 0;
                });

                return total.toFixed(2);
            });

            // COMPOSITION PATTERN: Handle new_order_popup`s stuffs
            this.newOrderPopUp = new NewOrderPopUp();
            this.newCostPopUp = new NewCostPopUp();
            this.editCostAndOrderPopUp = new EditCostAndOrderPopUp();
        }

        /**
         * Get data from ERP
         */
        _getData(year = this.currentYear()){
            var self = this;
            $.ajax({
                type: 'POST',
                url: BUrl + 'employees_payments_pos/get_invoice',
                dataType: 'json',
                async: true,
                data: {
                    "user": authCtrl.userInfo,
                    "year" : year,
                },
                success: function (dataS) {
                    console.log("EmployeeInvoiceVM data:", dataS)
                    self.data(dataS.data);
                },
                error: function(){
                    customAlert("Something wrong with EmployeeInvoiceVM endpoint");
                }
            });
        }

        setYear(newYear){
            // we need another post to get data for this year
            this._getData(newYear);
        }

        setMonth(newMonth){
            console.log('newMonth', newMonth);
            this.currentMonth(newMonth);
        }

        removeRow(invoice_row){
            console.log('invoice_row', invoice_row);

            const confirmAction = () => {
                const salesman_id = authCtrl.userInfo.user_id;
                const year = new Date().getFullYear();
                const month_num = new Date().getMonth()+1; // php january is 1
                const self = this;


                $.ajax({
                    type: 'POST',
                    url: BUrl + 'employees_payments_pos/exclude_cost_from_invoice',
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        'order_id': invoice_row.metadata.order_id,
                        'another_cost_id': invoice_row.metadata.id,
                        'salesman_id': salesman_id,
                        'year': year,
                        'month_num': month_num
                    },
                    success: function (dataS) {
                        console.log("exclude_cost_from_invoice result: ", dataS)
                        self._getData(); // update view
                    },
                    error: function () {
                        customAlert("Something wrong with employees_payments_pos/exclude_cost_from_invoice endpoint");
                    }
                });

            };

            pos_warning({msg:'Are you sure you want to remove this invoice row?', confirmAction});

        }

    };

    /**
     * Handle popup stuffs
     */
    class NewOrderPopUp{
        constructor(){
            // hold result of the search
            this.ordersSearched = ko.observableArray([]);

            // string to search
            this.customerToSearch = ko.observable("");
            // make the search when this value changes
            this.customerToSearch.subscribe((newValue) => {
                this.searchCustomers(newValue);
            });

            // order selected
            this.selectedOrderConfirmation = ko.observable();

        }

        searchCustomers(customerName){
            var self = this;

            // do not search until we have at least 3 chars
            if (customerName.length < 3){
                self.ordersSearched([]);
                return;
            }

            $.ajax({
                type: 'POST',
                url: BUrl + 'employees_payments_pos/search_order',
                dataType: 'json',
                async: true,
                data: {
                    "user": authCtrl.userInfo,
                    'search_term' : customerName
                },
                success: function (dataS) {
                    console.log("search_order result: ", dataS)
                    const { orders } = dataS;
                    self.ordersSearched(orders);
                },
                error: function(){
                    customAlert("Something wrong with employees_payments_pos/search_order endpoint");
                }
            });

        }

        addNewOrder() {
            const currentOrder = this.selectedOrderConfirmation();
            const salesman_id = authCtrl.userInfo.user_id;
            const year = new Date().getFullYear();
            const month_num = new Date().getMonth()+1; // php january is 1

            const confirmAction = () => {
                $.ajax({
                    type: 'POST',
                    url: BUrl + 'employees_payments_pos/add_order_to_invoice',
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        'order_id': currentOrder.order_id,
                        'salesman_id': salesman_id,
                        'year': year,
                        'month_num': month_num },
                    success: function (dataS) {
                        console.log("add_order_to_invoice result: ", dataS);

                        employeeInvoiceVM._getData(); // update view (bad use: using global var, the alterative was passing the father to this child but will create a cyclic object )

                        // close popup
                        $('.order-search-pop-up').hide('slow');
                    },

                    error: function () {
                        customAlert("Something wrong with employees_payments_pos/add_order_to_invoice endpoint");
                    }
                });
            };
            pos_confirm({msg:'Do you really want to ADD this order to your invoice ?', confirmAction});
        }
    }

    /**
     * Handle popup stuffs
     */
    class NewCostPopUp{
        constructor(){
            this.nameOfCost = ko.observable("");
            this.value = ko.observable(0);
            this.description = ko.observable("");
        }

        /** Submit form
         * - Validate info
         */
        submit(){

            // validate form
            let formOK = true;
            $("#add_cost_box_wrapper form.main-form").each((_, el) => {
                if (!el.reportValidity()){
                    formOK = false;
                    return;
                }
            });

            if (!formOK){
                return;
            }

            const self = this;
            const img_path = $('.another-cost-invoice-uploader').data('path');
            const salesman_id = authCtrl.userInfo.user_id;
            const year = new Date().getFullYear();
            const month_num = new Date().getMonth()+1; // php january is 1

            if(!this.nameOfCost()) {
                customAlert('Please, fill cost name before proceed');
                return;
            }

            if(!this.value() || this.value() <= 0) {
                customAlert('Value cannot be ZERO !');
                return;
            }

            const confirmAction = () => {
                $.ajax({
                    type: 'POST',
                    url: BUrl + 'employees_payments_pos/include_new_invoice_cost',
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        'salesman_id' : salesman_id,
                        'year'        : year,
                        'month_num'   : month_num,
                        'name'        : this.nameOfCost(),
                        'description' : this.description(),
                        'value'       : this.value(),
                        'img_path'    : img_path
                    },
                    success: function (dataS) {
                        console.log("include_new_invoice_cost result: ", dataS);

                        employeeInvoiceVM._getData(); // update view (bad use: using global var, the alterative was passing the father to this child but will create a cyclic object )

                        // close popup
                        self.clearPopup()
                        $('.adding-another-cost-wrapper').hide('slow')
                    },

                    error: function () {
                        customAlert("Something wrong with employees_payments_pos/include_new_invoice_cost endpoint");
                    }
                });
            };
            pos_confirm({msg:'Do you really want to SUBMIT this new cost to your Invoice ?', confirmAction});



        }

        configureSubmitUploadImage(){
            /**
             * Creates the submission of the image to the server
             */
            $("#another-cost-invoice-form").on('submit', (function (e) {
                e.preventDefault();
                $.ajax({
                    url: BUrl + "employees_payments_pos/upload_another_cost_invoice_img",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        // TODO: loader
                    },
                    success: function (data) {
                        console.log('data', data);
                        const res = JSON.parse(data);

                        if (res.result === 'success') {
                            // TODO: stop loader

                            $.jGrowl('File Uploaded Successfully !');
                            const full_path = BUrl + res.file_path;

                            $(".another-cost-invoice-uploader").data('path', res.file_path);
                            $("#another-cost-invoice-result-img").attr('src', full_path);
                            $("#another-cost-invoice-result-img").show();
                        }
                        else {
                            alert('Something went wrong :((');
                        }

                    },
                    error: function (e) {
                        console.log('e', e);
                        alert('Something went wrong :(((');
                    }
                });
            }));
        }

        clearPopup(){
            this.nameOfCost("");
            this.value(0);
            this.description("");

            // image stuffs
            $("#another-cost-invoice-uploader").val("");
            $(".another-cost-invoice-uploader").data('path', "");
            $("#another-cost-invoice-result-img").attr('src', "");
            $("#another-cost-invoice-result-img").hide();
        }

    }

    /**
     * Class to handle a edition of a order
     *
     * But remember: user can not change the COST of an 'another cost'
     */
    class EditCostAndOrderPopUp{
        constructor(){
            this.selectedRow = ko.observable();
            this.type = ko.computed(() => (
                this.selectedRow() ? this.selectedRow().type : ""
            ));
        }

        submit(){
            var self = this;

            // validate form     ================================
            let formOK = true;
            $("#edit_cost_and_order_wrapper form.main-form").each((_, el) => {
                if (!el.reportValidity()){
                    formOK = false;
                    return;
                }
            });

            if (!formOK){
                customAlert("Please, check if the form is corrected filled");
                return;
            }

            // new cost can not be more then "Total (ex GST)"
            if (parseFloat(self.selectedRow().cost) > parseFloat(self.selectedRow().metadata.total_no_gst)){
                customAlert('new cost can not be more then "Total (ex GST)"');
                return;
            }


            // end validate form ================================

            const salesman_id = authCtrl.userInfo.user_id;
            const year = new Date().getFullYear();
            const month_num = new Date().getMonth()+1; // php january is 1

            $.ajax({
                type: 'POST',
                url: BUrl + 'employees_payments_pos/overwrite_invoice_row',
                dataType: 'json',
                async: true,
                data: {
                    "user"            : authCtrl.userInfo,
                    'user_id'         : salesman_id,
                    'year'            : year,
                    'month_num'       : month_num,
                    'order_id'        : self.selectedRow().metadata.order_id,
                    'another_cost_id' : self.selectedRow().metadata.id,
                    'new_price'       : self.selectedRow().cost,
                    'new_description' : self.selectedRow().description,
                },
                success: function (dataS) {
                    console.log("dataS:", dataS)
                    employeeInvoiceVM._getData(); // update view (bad use: using global var, the alterative was passing the father to this child but will create a cyclic object )
                    alert('Edited successfully !!');
                    $('.edit-wrapper').hide('slow');

                },
                error: function(){
                    customAlert("Something wrong with overwrite_invoice_row_cost endpoint");
                }
            });


        }
    }
});