define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingData = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var self = this;
		this.boxes = ko.observableArray();
		this.selectedBox  = ko.observable({});
		
		this.transmuteDate = function(date){  // input is yyyy-mm-dd
			if(date != undefined && date != null){				
				var ymd = date.substring(0, date.lastIndexOf(" "));
				var time = date.substring(date.lastIndexOf(" ") + 1);
				var hm = time.substring(0, time.lastIndexOf(":"));
				
				var day = ymd.substring(ymd.lastIndexOf("-") + 1);
				var month = ymd.substring(ymd.indexOf("-") + 1, ymd.lastIndexOf("-"));
				var year = ymd.substring(0, ymd.indexOf("-"));
				return day + "-" + month  + "-" + year + " " + hm;
			}else{
				return 0;
			}	
		};
		
		this.timestampToDate = function(UNIX_timestamp){
  			var a = new Date(UNIX_timestamp);
  			var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  			var year = a.getFullYear();
  			var month = months[a.getMonth()];
  			var date = a.getDate();
  			var time = date + '/' + (a.getMonth() + 1) + '/' + year;
  			return time;
		};		
		
		this.viewBox = function(index){
			localStorage.removeItem('ShippingBoxDS');
			orderItem.ShippingBoxVM = new ShippingBox(orderItem.dsRegistry);
			orderItem.ShippingBoxDS = new defShippingBox('ShippingBoxDS', orderItem.dsRegistry, this.boxes()[index] );
			orderItem.ShippingBoxVM.subscribeTo('ShippingBoxDS');
console.log("orderItem.ShippingBoxDS.getStore(): " + JSON.stringify( orderItem.ShippingBoxDS.getStore() ) );
 			orderItem.ShippingBoxVM.addVariantsShippingBox();
			posChangePage('#shipping_box');
		};
		
		this.loadPendingItems = function(){
			localStorage.removeItem('ShippingBoxDS');
			orderItem.ShippingBoxVM = new ShippingBox(orderItem.dsRegistry);
			orderItem.ShippingBoxDS = new defShippingBox('ShippingBoxDS', orderItem.dsRegistry, this.boxes()[0] );
			orderItem.ShippingBoxVM.subscribeTo('ShippingBoxDS');
 			orderItem.ShippingBoxVM.addVariantsShippingBox();	
			posChangePage('#shipping_items');	
		};
		
	},
	
	digestData: function(data) {	
		var newObserve = [];
		var data2 = data[0];
		if(data2 != undefined ){			
			for (var ind = 0; ind < data2.length; ind++) {
				if(data2[ind] != null){
					if (!data2[ind].isDead){						 
						data2[ind].checked = false;
						
						if(customersVM.systemMode == "shipping_pending"){
							data2[ind].sender_stock_location_name = '';
							data2[ind].shipping_items_box_sent_date = '';
							data2[ind].shipping_method_name = '';
							data2[ind].shipping_items_box_tracking_code = '';
							data2[ind].shipping_items_box_expected_date = '';
							data2[ind].recepient_stock_location_name = '';	
						}
						
						newObserve.push(data2[ind]);	
						//newObserve[ind].checked = false;	
					}	
				}	
			}
		
			this.boxes(newObserve);
//	console.log( "boxes: " + JSON.stringify(this.boxes()) );
//	console.log( "boxes length: " + this.boxes().length );
			if(customersVM.systemMode == "shipping_pending" && this.boxes().length == 1 && orderItem.PendingDigestDataCount == 0){
				orderItem.PendingDigestDataCount++;
				this.loadPendingItems();
			}
		}
	},
});  



//END DEFINE CLOSURE
});