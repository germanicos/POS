define(['jquery', 'knockout', 'base'], function ($, ko) {

    ColorsPopUpTemplate = class {
        constructor(garment, categoryId, optionId) {
            this._wrapper = "pipingColorsWrapper";
            this._garment = garment;
            this._categoryId = parseInt(categoryId);
            this._optionId = parseInt(optionId);
            this._orders = orders; // global reference to order
        }

        set garment(garment) {
            this._garment = garment;
        }

        get garment() {
            return this._garment.garment;
        }

        getCategory() {
            return this.garment.categories_map[this._categoryId];
        }

        getOption() {
            return this.getCategory().options_map[this._optionId];
        }

        getValue(valueId) {
            return this.getOption().values_map[valueId];
        }

        selectOption(value) {
            console.log('Select option called. Value> ', value);
            // close pop-up
            $(".white_content.mynicepop").hide("slow");

            // select option
            this._orders.selectedCustomer().garmentsDesignStep.setOptionValue(this.getOption(), value, this._garment);
        }

        applyBinds() {
            var self = this;
            $(".option-value1").on("click", function (evt) {
                const valueId = $(this).data("valueId");
                console.log('valueId', valueId);

                const value = self.getValue(valueId);
                self.selectOption(value);


            });

            $(".close_popups").on("click", function() {
                $(this).closest(".white_content.mynicepop").hide("slow");
            })

        }

        render() {
            var self = this;
            const pipingColorsTemplate = /* html */`
            <!-- pop up -->
            <div class="white_content mynicepop" style="display: block">
                <div class="inmynicepop inmynicepopthree">
                    <h4>Select ${this.getOption().name}</h4>
                    <div class="option-wrapper2-popup">
                        ${this.getOption().values.map(value => /* html */`
                            <div class="option-value1 grid_4 ${value.selected() ? ' selected-val ' : ''}  ${value.clicked() ? ' clicked ' : ''}  " data-value-id=${value.id} >
                                <span class="new_main_option_title">${value.name}</span>
                                <img src=${value.image} class="example">
                            </div>
                        `).join("")}
                    </div>
                    <a href="javascript:void(0)" class="close_popups mynicepopclose ui-link" >Save And Close</a>
                </div>
            </div>
            `

            $(`.${this._wrapper}`).html(pipingColorsTemplate);

            this.applyBinds();
        }
    }


});