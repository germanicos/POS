define(['jquery', 'knockout', 'base'], function ($, ko) {
    
    /* orders.orderStepRequirementsVM.requiredInfoListVM.requiredInfoList() */
    RequiredInfoListVM = class {
        constructor(ordersVM) {
            this._ordersVM = ordersVM;
            this.requiredInfoList = ko.observableArray([]);
        }

        goToStep(index){
            this.requiredInfoList[index].goToStep();
        }

        addNewRequired(text, step, mandatory = false, customer = null, garment = null){
            this.requiredInfoList.push(
                new RequiredInfo(text, step, mandatory, customer, garment)
            );
        }

        clear(){
            // this.requiredInfoList.removeAll();
            // or
            this.requiredInfoList([]);
        }
    }

    RequiredInfo = class {
        /**
         * 
         * @param {String} text Text to show on view
         * @param {String} step stepId
         * @param {Boolean} mandatory 
         * @param {Customer} customer 
         * @param {Garment} garment 
         */
        constructor(text, step, mandatory = false, customer = null, garment = null) {
            this._text = text;
            this._step = step;
            this._customer = customer;
            this._garment = garment;
            this._mandatory = mandatory;

            this._ordersVM = orders;
        }

        goToStep(){
            // go to step
            this._ordersVM.changeStep(this._step);

            // select customer
            if(this._customer){
                this._ordersVM.changeCustomer(this._customer.customer_id);
            }

            // select garment
            // if (this._garment){
            //     this._ordersVM.currentGarmentInstance(this._garment);
            //     this._ordersVM.chooseDesignGarmentType(this._garment);
            // }

        }

        get text(){
            return this._text;
        }

        get mandatory(){
            return this._mandatory;
        }
    }

});