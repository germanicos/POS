define(['jquery', 'knockout', 'base'], function($, ko) {

    QualityControlVM = class{
        constructor(){
            console.log("QC");

            this.selectedGarmentName = ko.observable(null);
            this.selectedGarmentId = ko.computed(() => {
                switch (this.selectedGarmentName()) {
                    case "jacket":
                        return "4";
                    case "pants":
                        return "1";
                    case "shirt":
                        return "2";
                    case "vest":
                        return "3";
                    default:
                        return null;
                }
            });

            this.tags_list = [];
            this.tagsToShow = ko.observableArray([]);

            this.selectedTag = ko.observable(null);
            this.errorsToShow = ko.computed(() => {
                if (!this.selectedTag()){
                    return [];
                }
                else {
                    const response = [];
                    for (const error of this.selectedTag().errors) {
                        for (const errors_media of error.error_medias) {
                            const errors_media_clone = JSON.parse(JSON.stringify(errors_media));

                            // get necessary data to the view
                            errors_media_clone.title = error.title;
                            errors_media_clone.notes = error.notes;
                            errors_media_clone.garment_name = error.garment_name;
                            errors_media_clone.customer_name = error.customer_name;
                            errors_media_clone.created_at_formatted = error.created_at_formatted;

                            response.push(errors_media_clone);
                        }
                    }

                    console.log('errorsToShow', response);
                    return response;
                }
            })


            this.mustShowTags = ko.observable(false);
            this._getDataFromERP();

        }

        _getDataFromERP() {

            let self = this;

            const url = 'errors_pos/get_qc_data';
            $.ajax({
                type: 'POST',
                url: BUrl + url,
                dataType: 'json',
                async: true,
                data: {
                    "user": authCtrl.userInfo,
                },
                success: function (dataS) {
                    console.log(url, dataS);

                    // get amount of medias
                    for (const tag of Object.values(dataS)) {
                        let count = 0;
                        for (const error of tag.errors)
                        {
                            // only linked images/videos
                            error.error_medias = error.error_medias.filter(media => media.is_image_linked == true);

                            count += error.error_medias.length;
                        }

                        tag.medias_count = count;
                    }

                    // store received data
                    self.tags_list = Object.values(dataS);

                    // force reload tags list of garments
                    self.selectGarment(self.selectedGarmentName() ? self.selectedGarmentName() : "");

                },
                error: function (dataS) {
                    customAlert('Something went wrong with '+ url);
                },
            });

        }

        selectGarment(garmentName){


            if( !['jacket','pants','shirt','vest'].includes( garmentName.toLowerCase().trim() ) ){

                console.warn("'garmentName' must be in ['jacket','pants','shirt','vest']");
                console.log('garmentName', garmentName);

                return;
            }

            let garment_type_id = "";

            switch (garmentName.toLowerCase().trim()) {
                case "jacket":
                    garment_type_id = "4";
                    break;
                case "pants":
                    garment_type_id = "1";
                    break;
                case "shirt":
                    garment_type_id = "2";
                    break;
                case "vest":
                    garment_type_id = "3";
                    break;

            }

            this.selectedGarmentName(garmentName.toLowerCase().trim());

            // filter tags and show tags for this garments
            this.tagsToShow(this.tags_list.filter(tag => tag.garment_type == garment_type_id));
        }

        selectTag(tag) {
            console.log('tag', tag);

            // gabiarra: deal with bad encode of string: replace '&amp;' for '&'
            for (const error of tag.errors) {
                for (const errors_media of error.error_medias) {
                    errors_media.resized_path = errors_media.resized_path.replace('amp;', '');
                }
            }

            /**
             * Slider to make the UX better
             */
            document.getElementById('loading_jp').style.display = "block";
            sleep(3000).then(() => {
                document.getElementById('loading_jp').style.display = "none";
            });

            this.selectedTag(tag);

            // update slider
            this.reloadSlider();



        }

        toggleImageTags(){
            this.mustShowTags(!this.mustShowTags());
            // pos_alert({msg:`The tags are now ${this.mustShowTags() ? "ON" : "OFF"}`});
        }

        /** Remove a media from QC list. Do not show anymore */
        removeMedia(error, errorIndex){
            console.log('error', error);

            const confirmAction = () => {
                const media_id = error.id;

                let self = this;

                const url = 'errors_pos/unlink_image_from_error';
                $.ajax({
                    type: 'POST',
                    url: BUrl + url,
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        media_id
                    },
                    success: function (dataS) {
                        console.log(url, dataS);
                        pos_alert({msg: "Media removed successfully"});

                        // get new list of images
                        self._getDataFromERP();

                        // remove from slider
                        $('#media-section .slider').slick('slickRemove', errorIndex);

                    },
                    error: function (dataS) {
                        customAlert('Something went wrong with '+ url);
                    },
                });
            }

            // confirm
            pos_confirm({msg: "Are you sure you want to remove this media?", confirmBtn:"yes", cancelBtn:"no", confirmAction})


        }

        addTag() {

            var thisVM = this;

            const _addTag = function(tagName, garmentType, description){
                console.log('tagName', tagName);
                console.log('garmentType', garmentType);
                console.log('description', description);

                const url = 'errors_pos/add_new_tag_error';
                $.ajax({
                    type: 'POST',
                    url: BUrl + url,
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        name : tagName,
                        garment_type : garmentType,
                        description,
                    },
                    success: function (dataS) {
                        console.log(url, dataS);

                        thisVM._getDataFromERP();

                        pos_alert({msg: "Tag successfully added"});

                    },
                    error: function (dataS) {
                        customAlert('Something went wrong with ' + url);
                    },
                });


            }

            $.dialog({
                title: 'Submit the new Tag for this garment',
                content: /* html */`<form id="newTagDialog">
                                        <div>
                                            <label>Error Name</label>
                                            <input type="text" id="tagName">
                                        </div>

                                        <div>
                                            <label>Garment Type</label>
                                            <select id="garmentType">
                                                <option value="1" ${thisVM.selectedGarmentId() == '1' ? 'selected' : ' '}>Pants</option>
                                                <option value="2" ${thisVM.selectedGarmentId() == '3' ? 'selected' : ' '}>Shirt</option>
                                                <option value="3" ${thisVM.selectedGarmentId() == '2' ? 'selected' : ' '}>Vest</option>
                                                <option value="4" ${thisVM.selectedGarmentId() == '4' ? 'selected' : ' '}>Jacket</option>
                                            </select>
                                        </div>

                                        <div>
                                            <label>Tag Description</label>
                                            <textarea id="description" cols="30" rows="3"></textarea>
                                        </div>

                                        <button type="submit">Submit</button>
                                    </form>`,
                animation: 'scale',
                onOpen: function () {
                    var thisDialog = this;
                    this.$content.find('form').submit(function (evt) {
                        evt.preventDefault();
                        const tagName = $(this).find("input#tagName").val();
                        const garmentType = $(this).find("select#garmentType").val();
                        const description = $(this).find("textArea#description").val();

                        if (tagName == ""){
                            pos_alert({msg:"Error name can not be empty"});
                            return;
                        }

                        _addTag(tagName, garmentType, description);

                        thisDialog.close();
                    })
                },
                columnClass: 'large',
            });

        }

        reloadSlider(){

            $('.slider.slick-initialized').slick('unslick');

            $('.slider').slick({
                infinite: false,
                dots: false,
                mobileFirst: true,
                adaptiveHeight: false,
                prevArrow : false,
                nextArrow : false,
                touchThreshold: 10,
            });

            // pause video on swipe
            $('.slider.slick-initialized').off('beforeChange');

            $('.slider.slick-initialized').on('beforeChange', function(event, slick, currentSlide, nextSlide) {

                if( $(this).find("video").length )
                {
                    $('video').each(function() {
                        $(this).get(0).pause();
                    });
                }

            });

            // set pinch movement
            $('.slider.slick-initialized .slider-content .image-tag-wrapper').each((_ , element)=>{
                Utils.hammer.setPinchZoom(element);
            })


        }

        goToPreviousStep(){

            // we are in step 3 (errors media)
            if (this.selectedTag()){
                this.selectedTag(null);
                return;
            }

            // we are in step 2 (tag selection)
            if (this.selectedGarmentName()){
                this.selectedGarmentName(null);
                return;
            }

            // we are in step 1 (garment selection)
            posChangePage('#qc_and_error');


        }
    }

});