define(['jquery', 'knockout', 'base'], function($, ko) {
Bodyshape = SimpleControl.extend({
	init: function(DsRegistry) {
		var self = this;
               
		this._super( DsRegistry );
                //testing             
//console.log('TESTING AT BODYSHAPE: ' + dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeShoulders);   
        
		try{ 
			this.bodyshapeNotes     = ko.observable(dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshape_notes);
		}catch(e){ 
			this.bodyshapeNotes     = ko.observable('');
		}
		
		this.bodyshapeShouldersList    = ko.observableArray(dsRegistry.getDatasource('bodyshapeShouldersDS').getStore());
		
		try{ 
			this.bodyshapeShoulders    = ko.observable(dsRegistry.getDatasource('bodyshapeShouldersDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeShoulders]);
		}catch(e){ 
			this.bodyshapeShoulders    = ko.observable(dsRegistry.getDatasource('bodyshapeShouldersDS').getStore()[3]); 
		}	
		
		this.bodyshapeNeckStanceList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeNeckStanceDS').getStore());
		try{ 
			this.bodyshapeNeckStance     = ko.observable(dsRegistry.getDatasource('bodyshapeNeckStanceDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeNeckStance ]);
		}catch(e){ 
			this.bodyshapeNeckStance     = ko.observable(dsRegistry.getDatasource('bodyshapeNeckStanceDS').getStore()[2]);
		}
		
		this.bodyshapeArmsList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeArmsDS').getStore());
		try{ 
			this.bodyshapeArms     = ko.observable(dsRegistry.getDatasource('bodyshapeArmsDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeArms]);
		}catch(e){
			this.bodyshapeArms     = ko.observable(dsRegistry.getDatasource('bodyshapeArmsDS').getStore()[2]);
		}
		 
			
		this.bodyshapeBackList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeBackDS').getStore());
		try{ 
			this.bodyshapeBack     = ko.observable(dsRegistry.getDatasource('bodyshapeBackDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeBack -1]);
		}catch(e){ 
            this.bodyshapeBack     = ko.observable(dsRegistry.getDatasource('bodyshapeBackDS').getStore()[2]);
        }        
                
		this.bodyshapeBeltAngleList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeBeltAngleDS').getStore());
		try{ 
			this.bodyshapeBeltAngle     = ko.observable(dsRegistry.getDatasource('bodyshapeBeltAngleDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeBeltAngle ]);
		}catch(e){
			 this.bodyshapeBeltAngle    = ko.observable(dsRegistry.getDatasource('bodyshapeBeltAngleDS').getStore()[0]);
		}
		
		this.bodyshapeBeltHeightList    = ko.observableArray(dsRegistry.getDatasource('bodyshapeBeltHeightDS').getStore());
		try{ 
			this.bodyshapeBeltHeight    = ko.observable(dsRegistry.getDatasource('bodyshapeBeltHeightDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeBeltHeight ]);
		}catch(e){
			this.bodyshapeBeltHeight    = ko.observable(dsRegistry.getDatasource('bodyshapeBeltHeightDS').getStore()[3]);
		}
		 
		
		this.bodyshapeFrontList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeFrontDS').getStore());
		try{ 
			this.bodyshapeFront     = ko.observable(dsRegistry.getDatasource('bodyshapeFrontDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeFront -1]);
		}catch(e){
			 this.bodyshapeFront    = ko.observable(dsRegistry.getDatasource('bodyshapeFrontDS').getStore()[1]);
		}
		
		this.bodyshapeLegList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeLegDS').getStore());
		try{ 
			this.bodyshapeLeg     = ko.observable(dsRegistry.getDatasource('bodyshapeLegDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeLeg -1]);
		}catch(e){
			this.bodyshapeLeg     = ko.observable(dsRegistry.getDatasource('bodyshapeLegDS').getStore()[2]);
		}
		 
		
		this.bodyshapeNeckHeightList   	= ko.observableArray(dsRegistry.getDatasource('bodyshapeNeckHeightDS').getStore());
		try{ 
			this.bodyshapeNeckHeight    = ko.observable(dsRegistry.getDatasource('bodyshapeNeckHeightDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeNeckHeight -1 ]);
		}catch(e){
			this.bodyshapeNeckHeight    = ko.observable(dsRegistry.getDatasource('bodyshapeNeckHeightDS').getStore()[1]);
		}	 
		
		this.bodyshapeSeatList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeSeatDS').getStore());
		try{ 
			this.bodyshapeSeat     = ko.observable(dsRegistry.getDatasource('bodyshapeSeatDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeSeat ]);
		}catch(e){ 
			this.bodyshapeSeat     = ko.observable(dsRegistry.getDatasource('bodyshapeSeatDS').getStore()[0]);
		}
		
		this.bodyshapeSleeveList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeSleeveDS').getStore());
		try{ 
			this.bodyshapeSleeve     = ko.observable(dsRegistry.getDatasource('bodyshapeSleeveDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeSleeve ]);
		}catch(e){ 
			this.bodyshapeSleeve     = ko.observable(dsRegistry.getDatasource('bodyshapeSleeveDS').getStore()[0]);
		}
		
		this.bodyshapeStandingList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeStandingDS').getStore());
		try{ 
			this.bodyshapeStanding     = ko.observable(dsRegistry.getDatasource('bodyshapeStandingDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeStanding -1]);
		}catch(e){ 
			this.bodyshapeStanding     = ko.observable(dsRegistry.getDatasource('bodyshapeStandingDS').getStore()[1]);
		}
		
		this.bodyshapeStomachList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeStomachDS').getStore());
		try{ 
			this.bodyshapeStomach     = ko.observable(dsRegistry.getDatasource('bodyshapeStomachDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeStomach -1]);
		}catch(e){ 
			this.bodyshapeStomach     = ko.observable(dsRegistry.getDatasource('bodyshapeStomachDS').getStore()[2]);
		}
		
		this.bodyshapeThighList     = ko.observableArray(dsRegistry.getDatasource('bodyshapeThighDS').getStore());
		try{ 
			this.bodyshapeThigh     = ko.observable(dsRegistry.getDatasource('bodyshapeThighDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeThigh -1]);
		}catch(e){ 
			this.bodyshapeThigh     = ko.observable(dsRegistry.getDatasource('bodyshapeThighDS').getStore()[5]);
		}
		
		this.previousStepEnabled = ko.observable(false);
		this.workflow    = ko.observableArray([
			{id: 0,  target: "#bodyshapeShoulders" , title: "Shoulders", modified: false},
			{id: 1,  target: "#bodyshapeNeckStance", title: "NeckStance", modified: false},
			{id: 2,  target: "#bodyshapeArms"      , title: "Arms", modified: false},
			{id: 3,  target: "#bodyshapeBack"      , title: "Back"},
			{id: 4,  target: "#bodyshapeBeltAngle" , title: "Belt Angle"},
			{id: 5,  target: "#bodyshapeBeltHeight", title: "Belt Height"},
			{id: 6,  target: "#bodyshapeFront"     , title: "Front"},
			{id: 7,  target: "#bodyshapeLeg"       , title: "Leg"},
			{id: 8,  target: "#bodyshapeNeckHeight", title: "NeckHeight"},
			{id: 9,  target: "#bodyshapeSeat"      , title: "Seat"},
			{id: 10, target: "#bodyshapeSleeve"    , title: "Sleeve"},
			{id: 11, target: "#bodyshapeStanding"  , title: "Standing"},
			{id: 12, target: "#bodyshapeStomach"   , title: "Stomach"},
			{id: 13, target: "#bodyshapeThigh"     , title: "Thigh"} 
			]);
		
		this.currentStep = ko.observable();
		this.currentStep('');
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
		});
		
		this.completion   = ko.observable(0);
		
	},
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
		
	nextStep: function() {
		if (this.currentStep().id != this.workflow().length) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  + 1]);
		} else {
			this.currentStep( this.workflow()[0]);
		}
	},

	
	flushModel: function() {
		var tModel = {};
		tModel.bodyshapeShoulders  = this.bodyshapeShoulders();
		tModel.bodyshapeNeckHeight = this.bodyshapeNeckHeight();
		tModel.bodyshapeStanding   = this.bodyshapeStanding();
		tModel.bodyshapeLeg        = this.bodyshapeLeg();
		tModel.bodyshapeThigh      = this.bodyshapeThigh();
		tModel.bodyshapeArms       = this.bodyshapeArms();
		tModel.bodyshapeFront      = this.bodyshapeFront();
		tModel.bodyshapeStomach    = this.bodyshapeStomach();
		tModel.bodyshapeBack       = this.bodyshapeBack();
		tModel.bodyshape_notes     = this.bodyshapeNotes();
		return tModel;
		},

	clear: function() {
		this.bodyshapeShoulders  (dsRegistry.getDatasource('bodyshapeShouldersDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeShoulders]);
		this.bodyshapeNeckHeight (dsRegistry.getDatasource('bodyshapeNeckHeightDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeNeckHeight]);
		this.bodyshapeStanding   (dsRegistry.getDatasource('bodyshapeStandingDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeStanding]);
		this.bodyshapeLeg     	 (dsRegistry.getDatasource('bodyshapeLegDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeLeg]);
		this.bodyshapeThigh   	 (dsRegistry.getDatasource('bodyshapeThighDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeThigh]);
		this.bodyshapeArms       (dsRegistry.getDatasource('bodyshapeArmsDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeArms]);
		this.bodyshapeFront      (dsRegistry.getDatasource('bodyshapeFrontDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeFront]);
		this.bodyshapeStomach    (dsRegistry.getDatasource('bodyshapeStomachDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeStomach]);
		this.bodyshapeBack       (dsRegistry.getDatasource('bodyshapeBackDS').getStore()[dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshapeBack]);
		this.bodyshapeNotes      (dsRegistry.getDatasource('bodyshapeDS').getStore()[0].bodyshape_notes);
	}
});
});