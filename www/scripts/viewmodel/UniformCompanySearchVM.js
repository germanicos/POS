define(['jquery', 'knockout', 'base'], function($, ko) {

	/**
	 * View model that controls the search function of existing companies.
	 */
    UniformCompanySearchVM = SimpleControl.extend({


    	init : function() {

    		console.log("init UniformCompanySearchVM");
    		var self = this;

    		this.selectedCompany = ko.observable(false);
    		this.companyInSearch = ko.observable("");
    		this.companyInSearch.subscribe(function(newValue) { self.searchCompany.call(self, newValue); });
			this.companySearchList = ko.observableArray([]);

			// make initial search
			this.searchCompany('', false);

            this.statistics = null;
    	},

		/**
		 *
		 * @param {*} companyName
		 * @param {Boolean} minimumCharacter only make a search if companyName.length is greater than 3
		 */
    	searchCompany : function(companyName, minimumCharacter=true) {

    		var self = this;

    		// Clear list
            self.companySearchList([]);
    		$("#zero-results-span").hide();
    		this.selectedCompany(false);

    		console.log("searching for", companyName);
    		if( companyName.length < 3 && minimumCharacter)
    		{
    			return;
    		}

    		$.ajax({
    		    type: 'POST',
    		    timeout: 60000, // sets timeout to 60 seconds
    		    url: BUrl + 'orders_pos/search_uniform_company',
    		    dataType: 'json',
    		    data: {
    		        "user": authCtrl.userInfo,
    		        "company_name": companyName
    		    },
    		    success: function (dataS) {

    		        console.log("res", dataS);

    		        if(dataS.companies.length === 0) // no result
    		        {
    		        	$("#zero-results-span").show();
    		        }

                    // Clear list again => avoid concurrent requests
                    self.companySearchList([]);

    		        for(let company of dataS.companies)
    		        {
    		        	self.companySearchList.push(company);
    		        }

					$(".company-list-wrapper-1").show();


    		    },
    		    error: function (error) {
    		        console.log(JSON.stringify(error));
    		        customAlert("TIME OUT - there is a network issue. Please try again later");
    		    },
    		    async: true
    		});

    	},

    	selectCompany : function(company) {
    		this.selectedCompany(company);
			$(".company-list-wrapper-1").hide();
    	},



    	addNewMasterDesign : function() {

            var self = this;

			const confirmAction = () => {
				// stores company id and call the router
				localStorage.setItem('company_idUniformTemplate', self.selectedCompany().company_id);
				posChangePage('#uniformTemplate');
			};
			pos_confirm({msg:`Add new master design to ${ self.selectedCompany().name } ? `, confirmAction});

    	},


        proceedToOrder : function() {

            var self = this;

			const confirmAction = () => {
				// stores company id and call the router
				localStorage.setItem('company_iduniformOrder', self.selectedCompany().company_id);
				posChangePage('#uniformOrder');
			};

			pos_confirm({msg:`Proceed to Order with company  ${ self.selectedCompany().name } ? `, confirmAction});

        },


        companyStatistics : function() {

            console.log("companyStatistics...");
            const company_id = this.selectedCompany().company_id;
            this.statistics = new UniformStatisticsVM(company_id);

            posChangePage("#uniformCompanyStatistics");
        }





    });

});