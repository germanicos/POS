define(['jquery', 'knockout', 'base', "viewmodel/RequiredInfoVM"], function($, ko) {

    /**
     * This VM will be responsible to check if all requiremnts are met
     * before moving forward/backward in the Group orders process
     *
     * This VM will have a lot of 'dity' and 'ugly' code, so its better to separate from the
     * more important code related to the Group orders business logic.
     *
     * */
    OrderStepsRequirementsVM = SimpleControl.extend({

    	init: function(ordersVM) {

            this.ordersVM = ordersVM;
            this.requiredInfoListVM = new RequiredInfoListVM(ordersVM);

            console.log("Init OrderStepRequirementsVM")
            var self = this;

        },

        nextBtn : function(currentStepId) {

            try
            {
	            switch(currentStepId)
	            {
	            	case '1':
	            		this.nextBtnSelectGarmentsStep();
	            	break;

	            	case '2':
	            		this.nextBtnMeasurmentsStep();
	            	break;

	            	case '3':
                        this.nextBtnBodyShapesStep();
	            	break;

	            	case '4':
                        this.nextBtnImagesStep();
	            	break;

	            	case '5':
                        this.nextBtnVideoStep();
	            	break;

	            	case '6':
                        this.nextBtnDesignStep();
                    break;

	            	case '7':
                        this.nextBtnPaymentStep();
	            	break;
	            }
	        }
	        catch(errMessage)
	        {
	        	console.error('Error ', errMessage);
	        	alert(errMessage);
	        }

        },

        /**
         * Action for the next btn inside the select garments Step
         *
         * Requirement: All customers needs to have at least one garment
         *
         */
        nextBtnSelectGarmentsStep : function() {

        	const res = this.checkGarmentsStep();
        	/**
    		 * If one customer doest have any garments => throw error
    		 */
    		if( !res )
    		{
                $.jGrowl(`All customers need to have at least one garment !!`, { life: 7000 });
                return false;
    		}

        	const nextStep = this.ordersVM.steps.filter( e => e.id == '2' )[0];
        	this.ordersVM.changeStep(nextStep.id);

            // selects the first PRESENT customer
            const presentCustomer = this.ordersVM.customers().filter(el=>el.is_present)[0];
            if (presentCustomer){
                this.ordersVM.selectedCustomer(presentCustomer);
            }else{
                this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
            }

            return true;
        },

        /**
         * Logic to check if garment step is complete
         * ******* Can be used outside the context of the nextBTN  => for example: to color the step in green
         * @return {[type]} [description]
         */
        checkGarmentsStep : function() {

        	const customers = this.ordersVM.customers();
        	let res = true;

        	for(let customer of customers)
        	{
        		let hasGarments = false;

        		for( let garmentType of customer.garmentsStep.garments() ) // checks if there is at least one garment per customer
        		{
        			if( garmentType.garments().length > 0)
        			{
        				hasGarments = true;
        				continue;
        			}
        		}

        		if( !hasGarments )
        		{
        			res = false;
        			break;
        		}
        	}

        	return res;
        },

        checkMeasurementsStep: function () {
            const customers = this.ordersVM.customers();

            // list of not mandatory measurments
            const notMandatoryMeaskeys = [
                'jacket_measurements_lapel_length',
                'pants_measurements_front_crotch',
                'pants_measurements_zipper',
                'pants_measurements_inseam',
                'shirt_measurements_watch',
                'vest_measurements_v_length',
                'jacket_measurements_notes',
                'pants_measurements_notes',
                'pants_measurements_angled_hems',
                'shirt_measurements_notes',
                'vest_measurements_notes',
                'jacket_measurements_last_edited',
                'pants_measurements_last_edited',
                'shirt_measurements_last_edited',
                'vest_measurements_last_edited'
            ];


            for (let customer of customers) {

                // check if customer is present
                if (!this.customerIsPresent(customer)) {
                    continue;
                }

                // Gets the garments types that the user selected in the select garments step
                const selectedGarmentTypesNames = customer.garmentsStep.garments().filter(e => e.garments().length > 0).map(e => e.name);

                // replace suit for ["jacket" and "pants"]
                if(selectedGarmentTypesNames.includes("suit")){
                    // check if already have "jacket"
                    if (!selectedGarmentTypesNames.includes("jacket")) {
                        selectedGarmentTypesNames.push("jacket");
                    }
                    // check if already have "pants"
                    if (!selectedGarmentTypesNames.includes("pants")) {
                        selectedGarmentTypesNames.push("pants");
                    }

                    // remove "suit"
                    const index = selectedGarmentTypesNames.indexOf("suit");
                    if (index > -1) {
                        selectedGarmentTypesNames.splice(index, 1);
                    }


                }

                console.log("selectedGarmentTypesNames", selectedGarmentTypesNames);

                // only check measurments for the selected garments types in the garments step
                const necessaryMeasurments = customer.measurementsStep.garments.filter(e => selectedGarmentTypesNames.includes(e.name().toLowerCase()));

                console.log("necessaryMeasurments", necessaryMeasurments);

                for (let garmentType of necessaryMeasurments) {
                    for (let meas of garmentType.measurements()) {

                        // console.log("meas.title()", meas.title() );
                        // console.log("meas.value()", meas.value() );

                        if (notMandatoryMeaskeys.includes(meas.key())) { continue; }


                        // If measurment is empty or ZERO -- Do not include anlged pants hems...
                        if ( meas.value == 'undefined' || !meas.value() || meas.value().trim() == '' || parseFloat(meas.value()) <= 0.0) {
                            // console.log('Changing meas selected customer...');
                            // Change to this customer
                            // this.ordersVM.changeCustomer(customer.customer_id);
                            // $.jGrowl(`Please fill in: ${meas.title()} for ${customer.full_name} !!`, { life: 7000 });
                            return {'success':false, 'meas':meas, 'customer':customer};
                        }
                    }
                }
            }

            return {'success':true};
        },

        /**
         * Action for the next btn inside the select garments Step
         *
         * Requirement: All mandatory measurments for all the customers needs to be filled up by the user
         *
         */
        nextBtnMeasurmentsStep : function() {
            var checkMeasurement = this.checkMeasurementsStep()
            if (checkMeasurement.success == false){
                console.log('Changing meas selected customer...');
                // Change to this customer
                this.ordersVM.changeCustomer(checkMeasurement.customer.customer_id);
                $.jGrowl(`Please fill in: ${checkMeasurement.meas.title()} for ${checkMeasurement.customer.full_name} !!`, { life: 7000 });
                return false;
            };

        	// If all customers have all measurments => change to next step
        	const nextStep = this.ordersVM.steps.filter( e => e.id == '4' )[0];
            this.ordersVM.changeStep(nextStep.id);

            // selects the first PRESENT customer
            const presentCustomer = this.ordersVM.customers().filter(el=>el.is_present)[0];
            if (presentCustomer){
                this.ordersVM.selectedCustomer(presentCustomer);
            }else{
                this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
            }

            return true;
        },



        checkBodyshapeStep: function (customer) {
            // check if customer is present
            if (!this.customerIsPresent(customer)) {
                return { "status": true, "customer": customer };
            }

            for (let bodyshapeCategory of customer.bodyShapesStep.bodyShapes()) // checks if there is at least one garment per customer
            {
                if (!bodyshapeCategory.selectedValue()) {
                    return { "status": false, "customer": customer, "bodyshapeMissing": bodyshapeCategory };
                }
            }

            return {"status":true};

        },

        /**
         * requirement : All bodyshapes needs to be filled up
         *
         *         TODO future: Only require the bodyshapes that makes sense for the selected garments
         *                         ex. If customer has only pants => show and require only the lower body bodyshape options
         *
         *
         * @return {[type]} [description]
         */
        nextBtnBodyShapesStep : function() {

            const customers = this.ordersVM.customers();

            for(let customer of customers)
            {
                const bodyShapeCheckResult = this.checkBodyshapeStep(customer);
                if (!bodyShapeCheckResult.status){
                    // Change to this customer
                    this.ordersVM.changeCustomer(customer.customer_id);
                    //alert(`Please select ${bodyshapeCategory.cat_name} bodyshape for ${customer.full_name} !!`);
                    $.jGrowl(`Please select ${bodyShapeCheckResult.bodyshapeMissing.cat_name} bodyshape for ${bodyShapeCheckResult.customer.full_name} !!`, { life: 7000 });
                    return false;
                }
            }

            // If all customers have all bodyshapes => change to next step
            const nextStep = this.ordersVM.steps.filter( e => e.id == '6' )[0];
            this.ordersVM.changeStep(nextStep.id);
            // selects the first customer
            this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);

            return true;
        },


        checkImageStep(customer){
              // check if customer is present
              if (!this.customerIsPresent(customer)) {
                return true;
            }

            for( let image of customer.imageData ) // checks if there is at least one garment per customer
            {
                // additional are not mandatory
                if( image.type === 'additional' ) { continue; }

                if( !image.url() || image.url().trim() == "")
                {

                    return false;
                }
            }

            return true;
        },

        /**
         * requirement: Front/back/side images are mandatory
         *
         *          Maybe: Are tags mandatory ?
         *
         * @return {[type]} [description]
         */
        nextBtnImagesStep : function() {

            const customers = this.ordersVM.customers();

            for(let customer of customers)
            {
                if(!this.checkImageStep){
                    this.ordersVM.changeCustomer(customer.customer_id);
                    $.jGrowl(`Please take ${image.type} image for ${customer.full_name} !!`, { life: 7000 });
                    return false;
                }
            }

            // If all customers have all bodyshapes => change to next step
            const nextStep = this.ordersVM.steps.filter( e => e.id == '5' )[0];
            this.ordersVM.changeStep(nextStep.id);

            // selects the first PRESENT customer
            const presentCustomer = this.ordersVM.customers().filter(el=>el.is_present)[0];
            if (presentCustomer){
                this.ordersVM.selectedCustomer(presentCustomer);
            }else{
                this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
            }

            return true;
        },


        checkVideoStep: function (customer) {
            // if one does not have video. Ask if the program may continue
            if (customer.uploadVideosStep.videos() && customer.uploadVideosStep.videos().length > 0) {
                // has video
            }
            else {
                // does not have video
                if (customer.is_present) {
                        return { "status": false, "present": true };
                } else {
                    return { "status": false, "present": false };
                }
            }

            return {"status":true};
        },

        nextBtnVideoStep : function() {

            // check if all customers has video
            for(let customer of this.ordersVM.customers())
            {
                const checkVideoStepResult = this.checkVideoStep(customer);
                // customer present but has no video
                if (!checkVideoStepResult.status && checkVideoStepResult.present){
                    if (!confirm(`Customer ${customer.full_name} does not have Video. Are you sure you want to proceed without a video?`)) {
                        this.ordersVM.selectedCustomer(customer);
                        return false;
                    }
                }

                // else if (!checkVideoStepResult.status){
                //     return false;
                // }
            }

            const nextStep = this.ordersVM.steps.filter( e => e.id == '3' )[0];
            this.ordersVM.changeStep(nextStep.id);

            // selects the first customer
            this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);

            return true;

        },

        /**
         * This functions loops through the Garments and its design steps.
         *     Once it gets to the last design step of the last garment, it goes to the payments step.
         */
        nextBtnDesignStep: function () {

            var self = this;

            console.log("new next btn click");

            const currentGarmentType = this.ordersVM.currentDesigingGarmentType();
            // gets the active garment type
            const currentGarmentTypeName = currentGarmentType.garmentType;
            // gets the active garment type steps
            const currentGarmentTypeDesignSteps = this.ordersVM.globalDesignCategoriesArray().filter( e => e.garmentType == currentGarmentTypeName)[0];
            // gets the selected design step
            const currentDesignStep = this.ordersVM.currentDesignStep();
            // gets the position in the array of the current one
            var currentDesignArrayIndex = currentGarmentTypeDesignSteps.categories.indexOf(currentDesignStep) == -1 ? 0 : currentGarmentTypeDesignSteps.categories.indexOf(currentDesignStep);

            // this variable should hold the next step for design
            let nextStep = false;

            // if none is selected => select the first one
            if( !currentDesignStep )
            {
                nextStep = currentGarmentTypeDesignSteps.categories[0];
            }
            // Finds the next step and move to it
            else
            {
                console.log("currentDesignArrayIndex i ", currentDesignArrayIndex);
                console.log("length", currentGarmentTypeDesignSteps.categories.length);


                // if current design is one of the above, change garment before change step
                // Lapel details (jacket and suit) => cat_id: [11 and 53]
                // Sleeves (jacket and suit)       => cat_id: [7 and 54]
                // monogram (jacket and suit)      => cat_id: [46 and 63]
                // Contrast (shirt)                => cat_id: [38]
                // Monogram (shirt)                => cat_id: [39]
                // notes (all)                     => cat_id: [10, 19, 64, 28, 40]
                // 65 => custom data for pants
                // 66 => custom data for shirt
                // 67 => custom data for vest
                // 68 => custom data for jacket
                // 69 => custom data for suit
                if(["11", "53", "7", "54", "46", "63", "38", "39", "10", "19", "64", "28", "40","65", "66", "67", "68", "69"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex].id)){
                    // get current garment position of array
                    let currentGarmentPosition =  this.ordersVM.globalGarments[currentGarmentTypeName]().indexOf(this.ordersVM.currentGarmentInstance())

                    // select next garment
                    if (currentGarmentPosition!=-1 && currentGarmentPosition+1 < this.ordersVM.globalGarments[currentGarmentTypeName]().length){
                        let nextGarment = this.ordersVM.globalGarments[currentGarmentTypeName]()[currentGarmentPosition+1];
                        this.ordersVM.currentGarmentInstance(nextGarment);
                        return;
                    }
                }

                // if next step is "custom data" but no custom options was selected, just skip this option
                // 65 => custom data for pants
                // 66 => custom data for shirt
                // 67 => custom data for vest
                // 68 => custom data for jacket
                // 69 => custom data for suit
                if (currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1] &&
                    ["65", "66", "67", "68", "69"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1].id)){
                    // const custom_category = currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1];

                    // check if any garment of current type has custom design

                    console.log('anyGarmentHasCustomDesign = ', this.ordersVM.selectedCustomer().garmentsDesignStep.anyGarmentHasCustomDesign());
                    if (!this.ordersVM.selectedCustomer().garmentsDesignStep.anyGarmentHasCustomDesign() ||
                        !this.ordersVM.selectedCustomer().garmentsDesignStep.anyGarmentHasCustomDesign().length > 0) {

                            // skip
                            currentDesignArrayIndex+=1;
                    }
                }

                // If there is next step in this garment...
                if( currentDesignArrayIndex + 1 < currentGarmentTypeDesignSteps.categories.length )
                {
                    // fabric case
                    // fabric category for pants : 12
                    // fabric category for shirt : 29
                    // fabric category for vest  : 20
                    // fabric category for jacket: 2
                    // fabric category for suit  : 41
                    // fabric category for suit  : 47
                    if (["12", "29", "20", "2", "41", "47"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex].id)) {

                        // check fabric for all garments of current type
                        for (const garment of this.ordersVM.globalGarments[currentGarmentTypeName]()) {
                            if (!garment.garment.categories[0].options[0].selectedValue()){
                                currentDesignStep.completed = ko.observable(false);
                                $.jGrowl(`Missing ${currentDesignStep.name}`, { life: 7000 });
                                return;
                            }

                            // check custom fabric info
                            if (garment.garment.useCustomerFabric()){
                                if (!garment.garment.customerFabricMeterage() || parseFloat(garment.garment.customerFabricMeterage()) <= 0) {
                                    currentDesignStep.completed = ko.observable(false);
                                    $.jGrowl(`Missing Fabric meterage`, { life: 7000 });
                                    return;
                                }

                                try {
                                    if (!garment.garment.categories_map[2].options_map[256].selectedValue()){
                                        currentDesignStep.completed = ko.observable(false);
                                        $.jGrowl(`Missing Custom Fabric Make`, { life: 7000 });
                                        return;
                                    }
                                } catch (error) {
                                    // empty
                                    // garment has no custom_fabric_make
                                }
                            }

                        }
                        currentDesignStep.completed = ko.observable(true);



                    }

                    // check if current category is completed (ignore custom data)
                    // 65 => custom data for pants
                    // 66 => custom data for shirt
                    // 67 => custom data for vest
                    // 68 => custom data for jacket
                    // 69 => custom data for suit
                    else if (currentDesignStep.completed() == false &&  !["65", "66", "67", "68", "69"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex].id)){
                        $.jGrowl(`Missing ${currentDesignStep.name}`, { life: 7000 });
                        return;
                    }

                    nextStep = currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1];
                }
                else // Else => change garment type
                {
                    // gets only the ones with garments count > 0
                    const globalGarmentsArray = this.ordersVM.globalGarmentsArray().filter( e => self.ordersVM.globalGarments[e.garmentType]().length > 0 );

                    // Gets the index of the selected garment
                    const currentGarmentIndex = globalGarmentsArray.indexOf(currentGarmentType);

                    console.log("globalGarmentsArray", globalGarmentsArray);

                    /**
                     * If there is NEXT garment, proceed.
                     *     We select the next garment and call this function again.
                     */
                    if( currentGarmentIndex + 1 < globalGarmentsArray.length )
                    {
                        const nextGarment = globalGarmentsArray[currentGarmentIndex + 1];
                        this.ordersVM.chooseDesignGarmentType(nextGarment);
                        this.nextBtnDesignStep();
                        return;
                    }
                    else // Else => go to last step
                    {
                        this.ordersVM.changeStep('7');
                        return;
                    }
                }
            }


            if (!this.checkUrgentDate()) {
                $.jGrowl("Please check if any garment has an invalid URGENT DATE");
                return;
            }


            // Finally, choose the next step.
            this.ordersVM.chooseCurrentDesignStep(nextStep);
        },

        checkUrgentDate: function (garmentObj = null) {
            // inner function to avoid duplicate code
            const _check = (garmentObj) => {
                const garmentDate = new Date(
                    garmentObj.selected_year(),
                    garmentObj.selected_month(),
                    garmentObj.selected_day()
                ).getTime();

                const todayDate = new Date().getTime();

                if (garmentObj.isUrgent() &&
                    (garmentDate < todayDate || !garmentObj.selected_year() || !garmentObj.selected_month() || !garmentObj.selected_day())) {
                    return false;
                }

                return true;
            }

            if(!garmentObj){

                const currentGarmentType = this.ordersVM.currentDesigingGarmentType();

                // check if any of the current garment type has urgent date in the past
                for (garmentObj of this.ordersVM.globalGarments[currentGarmentType.garmentType]()) {
                   if (!_check(garmentObj.garment)) return false;
                }
            }
            // check specif garment object
            else {
                if (!_check(garmentObj)) return false;
            }

            return true;
        },

        checkPaymentStep: function (customer) {
            if (customer.is_paying && !customer.paymentsStep.selectedPaymentMethod()) {
                return {"status": false, "msg" : `Please, fill payment method for customer ${customer.full_name} ` };
            }
            if (!customer.paymentsStep.selectedOrderCity()) {
                return {"status": false, "msg" : `Please, fill order city for customer ${customer.full_name} ` };
            }
            if (customer.is_paying && !customer.paymentsStep.terms.accepted()) {
                return {"status": false, "msg" : `Please, check if customer ${customer.full_name} accepted terms` };
            }
            if (customer.is_paying &&  !customer.paymentsStep.signing()) {
                return {"status": false, "msg" : `Please, fill signature for customer ${customer.full_name} ` };
            }
            if (!customer.paymentsStep.selectedFittingCity()) {
                return {"status": false, "msg" : `Please, fill fill fitting city for customer ${customer.full_name} `  };
            }

            return {"status": true};
        },

        nextBtnPaymentStep:function(){
            function showAlertsFunc(alert, show=true) {
                if (show){
                    customAlert(alert);
                }
            }

            for (const customer of this.ordersVM.customers()) {
                if (customer.is_paying == true){
                    const videoPaymentResult = this.checkPaymentStep(customer);
                    if(!videoPaymentResult.status){
                        showAlertsFunc(`${videoPaymentResult.msg}`);
                        this.ordersVM.changeCustomer(customer.customer_id);
                        return false;
                    };
                }
            }

            return true;
        },

        /**
         * Checks details for the order submission.
         *
         * orders.orderStepRequirementsVM.checkSubmissionRequirments()
         *
         *
         * @return {[type]} [description]
         */
        checkSubmissionRequirments : function() {

            console.log("Checking steps before sumission....");

            // clear all required info
            this.requiredInfoListVM.clear();

            if (!this.checkGarmentsStep()) {
                // $.jGrowl(`All customers need to have at least one garment !!`, { life: 7000 });
                // return false;
                this.requiredInfoListVM.addNewRequired(`All customers need to have at least one garment !!`, "1" ,true, null, null);
            }

            for(let customer of this.ordersVM.customers())
            {
                for(let garmentType of customer.garmentsStep.garments() )
                {
                    // for(let garment of garmentType.garments() )
                    for (let index = 0; index < garmentType.garments().length; index++) {
                        const garment = garmentType.garments()[index];

                        // checks if the garment has fabric
                        if( !garment.selectedFabric() && !garment.useCustomerFabric() )
                        {
                            // customAlert(`You need to add fabric for ${customer.full_name} ${garment.garment_name} !`);
                            // return false;
                            this.requiredInfoListVM.addNewRequired(
                                `<span class="customer-missing"> [${customer.full_name}] </span>
                                <span class="garment-missing"> [${garment.garment_name} #${index+1}] </span>
                                You need to add a fabric !`,
                                "6",
                                true,
                                customer,
                                garment
                            );
                        }

                        if (garment.useCustomerFabric()){

                            // check "custom fabric make"
                            // fabric category for jacket: 2
                            // fabric category for suit  : 47
                            const fabricCategory = garment.categories.filter(cat => ["2", "47"].includes(cat.id))[0];
                            if (fabricCategory){
                                // custom make option for jacket : 255
                                // custom make option for suit   : 256
                                const fabricCustomMakeOption = fabricCategory.options.filter(opt => ["255","256"].includes(opt.id))[0];
                                if(fabricCustomMakeOption && !fabricCustomMakeOption.selectedValue()){
                                    this.requiredInfoListVM.addNewRequired(
                                        `<span class="customer-missing"> [${customer.full_name}] </span>
                                        <span class="garment-missing"> [${garment.garment_name} #${index+1}] </span>
                                        You need to add custom fabric make !`,
                                        "6",
                                        true,
                                        customer,
                                        garment
                                    );
                                }
                            }

                            // check custom fabric picture
                            if (!garment.customerFabricImage()){
                                this.requiredInfoListVM.addNewRequired(
                                    `<span class="customer-missing"> [${customer.full_name}] </span>
                                    <span class="garment-missing"> [${garment.garment_name} #${index+1}] </span>
                                    You need to add an image to custom fabric !`,
                                    "6",
                                    false,
                                    customer,
                                    garment
                                );
                            }


                            // check fabric meterage
                            if (!garment.customerFabricMeterage()){
                                this.requiredInfoListVM.addNewRequired(
                                    `<span class="customer-missing"> [${customer.full_name}] </span>
                                    <span class="garment-missing"> [${garment.garment_name} #${index+1}] </span>
                                    You need to add set custom fabric's meterage !`,
                                    "6",
                                    true,
                                    customer,
                                    garment
                                );
                            }
                        }

                        // check all categories of garment are complete
                        garment.categories.forEach(category => {
                            if (!category.completed() &&
                                !(category.name.toLowerCase().includes("note") || category.name.toLowerCase().includes("monogram"))){
                                this.requiredInfoListVM.addNewRequired(
                                    `<span class="customer-missing"> [${customer.full_name}] </span>
                                    <span class="garment-missing"> [${garment.garment_name} #${index+1}] </span>
                                    ${category.name} is not complete !`,
                                    "6",
                                    false,
                                    customer,
                                    garment
                                );
                            }
                        })

                        if (!this.checkUrgentDate(garment)) {
                            this.requiredInfoListVM.addNewRequired(
                                "Please check if any garment has an invalid URGENT DATE",
                                "6",
                                true,
                                null,
                                null);
                        }

                    }
                }

                const bodyShapeCheckResult = this.checkBodyshapeStep(customer);
                if(!bodyShapeCheckResult.status){
                    this.requiredInfoListVM.addNewRequired(
                        `<span class="customer-missing"> [${customer.full_name}] </span> Please select ${bodyShapeCheckResult.bodyshapeMissing.cat_name} bodyshape !`,
                        "3",
                        true,
                        customer,
                        null
                    );
                }

                const imageStepResult = this.checkImageStep(customer);
                if(!imageStepResult){
                    this.requiredInfoListVM.addNewRequired(
                        `<span class="customer-missing"> [${customer.full_name}] </span> Please insert all images !`,
                        "4",
                        false,
                        customer,
                        null
                    );
                }

                const videoStepResult = this.checkVideoStep(customer);
                if(!videoStepResult.status && videoStepResult.present){
                    this.requiredInfoListVM.addNewRequired(
                        `<span class="customer-missing"> [${customer.full_name}] </span> Please insert a video !`,
                        "5",
                        false,
                        customer,
                        null
                    );
                }

                const paymentResult = this.checkPaymentStep(customer);
                if(!paymentResult.status){
                    this.requiredInfoListVM.addNewRequired(
                        `<span class="customer-missing"> [${customer.full_name}] </span> ${paymentResult.msg}`,
                        "7",
                        true,
                        customer,
                        null
                    );
                }

            }

            const checkMeasResult = this.checkMeasurementsStep();
            if(!checkMeasResult.success){
                let customer = checkMeasResult.customer;
                this.requiredInfoListVM.addNewRequired(
                    `<span class="customer-missing"> [${customer.full_name}] </span> Please fill in: ${checkMeasResult.meas.title()} measurement`,
                    "2",
                    false,
                    customer,
                    null
                );
            }


            return this.requiredInfoListVM.requiredInfoList().length > 0 ? false : true;
        },

        /**
         * Send the saved designs from local storage to ERP
         */
        sendSavedDesigns() {

            var self = this;

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/submit_saved_designs',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "savedDesigns": self.getSavedDesignStringify()
                },

                success: function (dataS) {
                    // nothing to do
                    console.log("saved designs successfully sent");

                },

                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: true
            });

        },

        /**
         * self.ordersVM.selectedCustomer().garmentsDesignStep.loadGarmentFromLocalStorage()() returns a cyclic object,
         * so the method JSON.stringify crashes. With this method, the problem is solved
         */
        getSavedDesignStringify(){

            const obj = this.ordersVM.selectedCustomer().garmentsDesignStep.loadGarmentFromLocalStorage()();

            var seen = [];

            return JSON.stringify(obj, function(key, val) {
                if (val != null && typeof val == "object") {
                    if (seen.indexOf(val) >= 0) {
                        return;
                    }
                    seen.push(val);
                }
                return val;
            });
        },

        /**
         *
         * @param {*} customer
         * @returns {Boolean} true if is present, e false if is not
         */
        customerIsPresent : function (customer) {
            return customer.is_present ? true : false;
        }

    })


});