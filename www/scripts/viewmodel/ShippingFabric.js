define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingFabric = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		this.ShippingFabricData     = [];
		this.ShippingFabricDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);

		this.workflow    = ko.observableArray([
			{id: 0, target: "#completions",  completed: "false", caption: "Completion", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
			$('.cloneDialog').remove();
			that.renderShippingFabric();
		});
		
		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});
		
		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});

		this.completion	= ko.observable(0);
		this.variantNameShippingFabric  = ko.observableArray([{id: 0, title: "ShippingFabric 1"}]);
		this.selectedVariantShippingFabric  = ko.observable(this.variantNameShippingFabric()[0]);	
		this.selectedVariantShippingFabric.subscribe(function(data) {
			that.selectedVariantShippingFabric();
			that.renderShippingFabric();
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});


		this.addGarment = function(garment) {		
//console.log("shirtcount: " + this.shirtcount());
//console.log("garment: " + garment);
			if( (this.shirtcount() > 0 && garment != 'shirt' ) || ( garment == 'shirt' && ( this.jacketcount() != 0 || this.pantcount() != 0 
			|| this.suitcount() != 0 || this.vestcount() != 0 || this.tiecount() != 0 || this.bowtiecount() != 0 || this.pocketsquarecount() != 0 ) ) ){
				
				customAlert("The same fabric can not be used for shirt and other garments.");	
			}else{
				if(garment == 'jacket'){
					this.jacketcount(this.jacketcount() + 1);
				}else if(garment == 'pant'){
					this.pantcount(this.pantcount() + 1);
				}else if(garment == 'suit'){
					this.suitcount(this.suitcount() + 1);
				}else if(garment == 'shirt'){
					this.shirtcount(this.shirtcount() + 1);
				}else if(garment == 'vest'){
					this.vestcount(this.vestcount() + 1);
				}else if(garment == 'tie'){
					this.tiecount(this.tiecount() + 1);
				}else if(garment == 'bowtie'){
					this.bowtiecount(this.bowtiecount() + 1);
				}else if(garment == 'pocketsquare'){
					this.pocketsquarecount(this.pocketsquarecount() + 1);
				}
			}
		};

		this.removeGarment = function(garment) {
			if(garment == 'jacket' && this.jacketcount() != 0){
				this.jacketcount(this.jacketcount() - 1);
			}else if(garment == 'pant' && this.pantcount() != 0){
				this.pantcount(this.pantcount() - 1);
			}else if(garment == 'suit' && this.suitcount() != 0){
				this.suitcount(this.suitcount() - 1);
			}else if(garment == 'shirt' && this.shirtcount() != 0){
				this.shirtcount(this.shirtcount() - 1);
			}else if(garment == 'vest' && this.vestcount() != 0){
				this.vestcount(this.vestcount() - 1);
			}else if(garment == 'tie' && this.tiecount() != 0){
				this.tiecount(this.tiecount() - 1);
			}else if(garment == 'bowtie' && this.bowtiecount() != 0){
				this.bowtiecount(this.bowtiecount() - 1);
			}else if(garment == 'pocketsquare' && this.pocketsquarecount() != 0){
				this.pocketsquarecount(this.pocketsquarecount() - 1);
			}
		};




		this.FabricCode = ko.observable('');
		this.FabricCode.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].FabricCode = data;
			that.flushModel();
		});
		
		this.FabricMeterage = ko.observable('');
		this.FabricMeterage.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].FabricMeterage = data;
			that.flushModel();
		});
		
		this.FabricComments = ko.observable('');
		this.FabricComments.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].FabricComments = data;
			that.flushModel();
		});


		this.jacketcount = ko.observable(0);
		this.jacketcount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].jacketcount = data;
			that.flushModel();
		});

		this.pantcount = ko.observable(0);
		this.pantcount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].pantcount = data;
			that.flushModel();
		});
		
		this.suitcount = ko.observable(0);
		this.suitcount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].suitcount = data;
			that.flushModel();
		});

		this.shirtcount = ko.observable(0);
		this.shirtcount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].shirtcount = data;
			that.flushModel();
		});

		this.vestcount = ko.observable(0);
		this.vestcount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].vestcount = data;
			that.flushModel();
		});
		
		this.tiecount = ko.observable(0);
		this.tiecount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].tiecount = data;
			that.flushModel();
		});
		this.bowtiecount = ko.observable(0);
		this.bowtiecount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].bowtiecount = data;
			that.flushModel();
		});

		this.pocketsquarecount = ko.observable(0);
		this.pocketsquarecount.subscribe(function(data) {		
			that.ShippingFabricData[ that.getRow(that.selectedVariantShippingFabric().id)  ].pocketsquarecount = data;
			that.flushModel();
		});


///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var photosarray = [];
			var images = localStorage.getItem('ShippingFabricPhoto');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('ShippingFabricPhoto') != null){
				if(localStorage.getItem('ShippingFabricPhoto').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			var customerString = orderItem.custSelectVM.selectedCustomer();
	
			if(images.length  == 0){
				customAlert("Please upload a fabric image");
			}else if(this.FabricCode().trim() == ""){
				customAlert("Please enter a code for the fabric");
			}else if(this.FabricMeterage().trim() == ""){
				customAlert("Please enter a meterage");
			}else if( this.jacketcount() == 0 && this.pantcount() == 0 && this.suitcount() == 0 && this.shirtcount() == 0 && 
					  this.vestcount() == 0 && this.tiecount() == 0 && this.bowtiecount() == 0 && this.pocketsquarecount() == 0){
					  	
				customAlert("Please add at least one garment");
			}else{
				var salesman = new Object();
		   		salesman.username = authCtrl.username();
		   		salesman.password  = authCtrl.password();
		   		salesman.id  = authCtrl.userInfo.user_id;
				
				data = { 
					salesman: salesman,
					customer: customerString,
					fabric_code: this.FabricCode(),
					meterage: this.FabricMeterage(),
					fabric_comments: this.FabricComments(),
					jacketcount: this.jacketcount(),
					pantcount: this.pantcount(),
					suitcount: this.suitcount(),
					shirtcount: this.shirtcount(), 
					vestcount: this.vestcount(),
					tiecount: this.tiecount(),
					bowtiecount: this.bowtiecount(), 
					pocketsquarecount: this.pocketsquarecount(),
					images: photosarray
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				$.post(BUrl + 'client_logistics/shipping_fabric_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Fabric succesfully added");
		                posChangePage('#shipping_main');
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingFabric"  : this.ShippingFabricData
		}, true);
	},

	getVariant: function(id) {
		var toreturn = this.ShippingFabricData[0];
		for (var ind in this.ShippingFabricData) {
			if ( this.ShippingFabricData[ind].variantId == id  ) {
				toreturn = this.ShippingFabricData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingFabricData) {
			if ( this.ShippingFabricData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantShippingFabric: function(name) {
		if(this.variantNameShippingFabric()[0].title != "ShippingFabric 1"){
			this.ShippingFabricDataAID += 1;
			var newid = this.ShippingFabricDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFabric().id ) )  ); //CLONE Object
			this.variantNameShippingFabric.push({title: vname, id: this.ShippingFabricDataAID});
			tObj.variantId = this.ShippingFabricDataAID;		
			this.flushModel();
		}else{
			this.variantNameShippingFabric()[0].title = name;
		}
	},	
	
	addVariantsShippingFabrics: function() {
		for(var x in orderItem.ShippingFabricVM.ShippingFabricData){
		//	console.log("addVariantsShippingFabrics " + x);
			if( x != 0){
				this.ShippingFabricDataAID += 1;
				var vname = "ShippingFabric " + x; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFabric().id ) )  ); //CLONE Object
				this.variantNameShippingFabric.push({title: vname, id: this.ShippingFabricDataAID});
				tObj.variantId = this.ShippingFabricDataAID;		
				this.flushModel();
			}else{
				var vname = "ShippingFabric " + x; 
				//var vname = orderItem.ShippingFabricVM.ShippingFabricData[x].pantcount;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFabric().id ) )  ); //CLONE Object
				this.variantNameShippingFabric()[x].title = vname;
				tObj.variantId = this.ShippingFabricDataAID;		
				this.flushModel();
			}
		}
	},

	digestData: function(data) {
		this.ShippingFabricData  = data.ShippingFabric;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingFabric();
	},

	renderShippingFabric: function() {	
		//Get selected Variant
		console.log("renderShippingFabric");
		try{
			var tData = this.getVariant(this.selectedVariantShippingFabric().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.jacketcount)		!= "undefined") {this.jacketcount(tData.jacketcount);}
				if (typeof(tData.pantcount)		!= "undefined") {this.pantcount(tData.pantcount);}
				if (typeof(tData.suitcount)	!= "undefined") {this.suitcount(tData.suitcount);}
				if (typeof(tData.shirtcount)    			!= "undefined") {this.shirtcount(tData.shirtcount);}
				if (typeof(tData.vestcount)    			!= "undefined") {this.vestcount(tData.vestcount);}
				if (typeof(tData.tiecount)    		!= "undefined") {this.tiecount(tData.tiecount);}
				if (typeof(tData.bowtiecount)    != "undefined") {this.bowtiecount(tData.bowtiecount);}
				if (typeof(tData.pocketsquarecount)    != "undefined") {this.pocketsquarecount(tData.pocketsquarecount);}
				if (typeof(tData.FabricCode)    	!= "undefined") {this.FabricCode(tData.FabricCode);}
				if (typeof(tData.FabricComments)    != "undefined") {this.FabricComments(tData.FabricComments);}
				if (typeof(tData.FabricMeterage)   != "undefined") {this.FabricMeterage(tData.FabricMeterage);}
			}
		}catch(e){
			;
		}

	}
});

defShippingFabric = SimpleDatasource.extend({
	init: function( name, dsRegistry) {
	//	if(data == null || data == undefined){
			var ShippingFabric = {};
			ShippingFabric.variantId        = 0;
			
			ShippingFabric.jacketcount = 0;
			ShippingFabric.pantcount = 0;
			ShippingFabric.suitcount = 0;
			ShippingFabric.shirtcount = 0;
			ShippingFabric.vestcount = 0;
			ShippingFabric.tiecount = 0;
			ShippingFabric.bowtiecount = 0;
			ShippingFabric.pocketsquarecount = 0;
			ShippingFabric.FabricCode = '';
			ShippingFabric.FabricComments = '';
			ShippingFabric.FabricMeterage = '';

			var iShippingFabric = {
				ShippingFabric: []
			};
			iShippingFabric.ShippingFabric.push(ShippingFabric);
			this._super(name, iShippingFabric, dsRegistry);
	//	}	
	}
});


//END DEFINE CLOSURE
});
