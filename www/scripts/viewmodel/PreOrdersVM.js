define(["jquery", "knockout", "base"], function ($, ko) {
    /**
     * This class will receive the customers ids that we will use for the
     * order process.
     */
    PreOrdersVM = SimpleControl.extend({
        init: function () {
            var self = this;

            console.log("Init PreOrdersVM...");

            // Used in the customer search
            this.custSelectVM = new CustomerSelectClass(dsRegistry);
            this.custSelectVM.subscribeTo("customersDS_" + authCtrl.userInfo.user_id);
            this.custSelectVM.setParentVM(this);

            // Stored the customers found by the search
            this.selectedCustomers = ko.observableArray([]);
            // End customer search

            // group orders identification name
            this.groupName = ko.observable("");

            this.customers = ko.observableArray([]);

            // number of people in the group order
            this.groupNumberPeople = ko.observable(0);

            // Steps to gather the pre order info
            this.groupOrderSteps = this.getGroupOrderSteps();

            // Keep track of the current step that the salesman is in at any given moment
            this.currentStep = ko.observable(this.groupOrderSteps[0]);

            this.leaderPic = ko.observable("");

            // Selected customer for tab control in the last page
            this.selectedDetailInfoCustomer = ko.observable(null);

            // subscribe changes. Thus we can parse birth date ('YYYY/MM/DD') to 3 variables
            this.selectedDetailInfoCustomer.subscribe(function ( selectedDetailInfoCustomer) {
                // parse birth date
                const date_split = selectedDetailInfoCustomer
                    .customer_DOB()
                    .split(new RegExp("[/-]"));

                console.log('date_split', date_split);

                if (!selectedDetailInfoCustomer.customer_birth_day) {
                    selectedDetailInfoCustomer.customer_birth_day = ko.observable( date_split[2]);
                } else {
                    selectedDetailInfoCustomer.customer_birth_day( date_split[2]);
                }

                if (!selectedDetailInfoCustomer.customer_birth_month) {
                    selectedDetailInfoCustomer.customer_birth_month = ko.observable( String(parseInt(date_split[1] )));
                } else {
                    selectedDetailInfoCustomer.customer_birth_month( String(parseInt(date_split[1])));
                }

                if (!selectedDetailInfoCustomer.customer_birth_year) {
                    selectedDetailInfoCustomer.customer_birth_year = ko.observable( date_split[0] );
                } else {
                    selectedDetailInfoCustomer.customer_birth_year(date_split[0]);
                }
            });

            // Locations
            this.cities = dsRegistry.getDatasource("citiesDS").getStore();
            this.states = dsRegistry.getDatasource("statesDS").getStore();
            this.countries = dsRegistry.getDatasource("countriesDS").getStore();

            this.referal_methods_list = ko.observableArray([
                { "name": "Google Search",       "id": "1" },
                { "name": "Bing Search",         "id": "8" },
                { "name": "Yahoo Search",        "id": "9" },
                { "name": "Print Advertisement", "id": "2" },
                { "name": "TV Ad",               "id": "3" },
                { "name": "Radio Ad",            "id": "4" },
                { "name": "Billboard",           "id": "5" },
                { "name": "Other",               "id": "6" },
                { "name": "Referral",            "id": "7" },
                {"name": "Facebook",             "id": "10"},
                {"name": "Instagram",             "id": "11"},
                {"name": "LinkedIn",             "id": "12"}
            ]);

            // for dates
            this.days = ko.observableArray([]);
            // for (i = 1; i <= 31; i++) this.days.push(i.toString());
			for (i = 1; i <= 31; i++) {
				// if number is 1,2,3,4...9 => convert to 01, 02, 03 ... 09
				let formatNumber = i.toString();
				if (formatNumber.length < 2){
					formatNumber = "0"+formatNumber;
				}
				this.days.push(formatNumber);
			};

            this.months = ko.observableArray([
                { name: "January", id: "1" },
                { name: "February", id: "2" },
                { name: "March", id: "3" },
                { name: "April", id: "4" },
                { name: "May", id: "5" },
                { name: "June", id: "6" },
                { name: "July", id: "7" },
                { name: "August", id: "8" },
                { name: "September", id: "9" },
                { name: "October", id: "10" },
                { name: "November", id: "11" },
                { name: "December", id: "12" }
            ]);

            this.years = ko.observableArray([]);
            for (i = 1930; i <= 2020; i++) this.years.push(i.toString());
        },

        getGroupOrderSteps: function () {
            return [
                {
                    name: "groupNameStep",
                    id: "1"
                },
                {
                    name: "customerBasicInfoStep",
                    id: "2"
                },
                {
                    name: "customerDeepInfoStep",
                    id: "3"
                }
            ];
        },

        _setRulesForCustomer(customer){
            // rule: if customer is not present, he can not pay
            customer.is_present.subscribe((newValue) => {
                if (!newValue) {
                    customer.is_paying(newValue);
                }
            });

            // if customer will pay, check if he is present
            customer.is_paying.subscribe(newValue => {
                if ((newValue && !customer.is_present())) {
                    customAlert("Sorry, the customer can not pay if he is not present");
                    customer.is_present(true);
                }

            });

            // setting customer DOB elements for customers from ERP (this parameters does not exist in DB, only the computed)
            const year_month_day = customer.customer_DOB ? customer.customer_DOB().split(new RegExp("[/-]")) : ["", "", ""];
            console.log('year_month_day', year_month_day );
            if (!customer.customer_birth_month) {
                customer.customer_birth_month = ko.observable(year_month_day[1]);
            }

            if (!customer.customer_birth_day) {
                customer.customer_birth_day = ko.observable(year_month_day[2]);
            }

            if (!customer.customer_birth_year) {
                customer.customer_birth_year = ko.observable(year_month_day[0]);
            }

            customer.customer_DOB = ko.computed(function () {
                // return customer.customer_birth_month()+"/"+customer.customer_birth_day()+"/"+customer.customer_birth_year();
                return `${customer.customer_birth_year()}/${customer.customer_birth_month()}/${customer.customer_birth_day()}`;
            });
        },

        getNewCustomerInstance() {
            console.log("Gettgin new customer....");

            let customer = {
                customer_id: ko.observable(null),
                is_leader: ko.observable(false), // is customer is the 'leader' of the order
                is_present: ko.observable(true), // is customer is present during the order
                is_paying: ko.observable(true), // If custumer is paying for his order
                customer_first_name: ko.observable(""),
                customer_last_name: ko.observable(""),
                customer_birth_day: ko.observable(""),
                customer_birth_month: ko.observable(""),
                customer_birth_year: ko.observable(""),
                //   "customer_DOB"                   : ko.observable(""),
                customer_occupation: ko.observable(""),
                customer_company_name: ko.observable(""),
                customer_gender: ko.observable("1"), // default
                customer_address1: ko.observable(""),
                customer_address2: ko.observable(""),
                customer_country: ko.observable(""),
                customer_state: ko.observable(""),
                customer_city: ko.observable(""),
                customer_postal_code: ko.observable(""),
                customer_mobile_phone: ko.observable(""),
                customer_landline_phone: ko.observable(""),
                customer_email: ko.observable(""),
                customer_email_subscription: ko.observable(true),
                customer_best_time_to_contact: ko.observable(null),
                customer_referal_method: ko.observable(""),
                customer_refered_by: ko.observable(""),
                customer_refered_by_id: ko.observable(""),
                customer_other_way: ko.observable(""),
                customer_referal_moreinfo: ko.observable(""),
                customer_price_range: ko.observable(""),
                customer_cat_id: ko.observable(""),
                customer_how_did_hear: ko.observable(""),
                customer_landline_code: ko.observable(""),
                receive_marketing: ko.observable("1")
            };

           this._setRulesForCustomer(customer);


            return customer;
        },

        // Listener on changes for the numnber of customers.
        // If the number changes, we create/remove the customers
        generateDefaultCustomerTable: function () {
            this.customers([]); // empty list

            for (let i = 0; i < this.groupNumberPeople(); i++) {
                const newCust = this.getNewCustomerInstance();

                if (i === 0) {
                    newCust.is_leader(true); // make the first one the leader
                }

                this.pushThenSortCustomer(newCust);
            }
        },

        // Adds a customer from the searchbar
        // ONLY IF there is room in the table of customers
        addCustomerFromSearchBar: function (customer) {
            console.log("appending client to list.", customer);

            // trying to clean customers that have empty info
            for (let i = this.customers().length - 1; i >= 0; i--) {
                const currentCustumer = this.customers()[i];

                console.log("analysing ", currentCustumer);

                //if customer is NOT LEADER AND is 'BLANK' => remove it
                if (
                    currentCustumer.customer_first_name() === "" &&
                    currentCustumer.customer_last_name() === "" &&
                    currentCustumer.customer_mobile_phone() === "" &&
                    currentCustumer.customer_email() === ""
                ) {
                    this.customers.remove(currentCustumer);
                    break;
                }
            }

            // if there is no room for a new customer => return
            if (this.customers().length >= this.groupNumberPeople()) {
                $.jGrowl("Please make space for a new customer in the table first !");
                return;
            }

            // Checks if customer is already in the process
            for (let c of this.customers()) {
                if (c.server_id != undefined && c.server_id() == customer.server_id) {
                    $.jGrowl("You have already choosed this customer !!!!!");

                    // add back empty room
                    const newCust = this.getNewCustomerInstance();
                    this.pushThenSortCustomer(newCust);

                    return;
                }
            }

            // Add new fields
            customer.is_leader = false;
            customer.is_paying = true;
            customer.is_present = true;

            const koCustomer = ko.mapping.fromJS(customer);
            this._setRulesForCustomer(koCustomer);

            this.pushThenSortCustomer(koCustomer);

            // Finds out if there is a leader in the group
            const has_leader = this.customers().filter(e => e.is_leader()).length > 0;

            if (!has_leader) {
                // if there is no leader => assigns the first customer as the leader
                console.log("Auto assinging new LEADER....");
                this.customers()[0].is_leader(true);
            }

            this.custSelectVM.customerAutocomplete(""); // clear search
            $.jGrowl("Added successfully !");
        },


        /**
         *
         * @param {*} customer
         * @returns Boolean - true if customer is a default customer with all blank field
         */
        customerIsEmpty(customer){
            const _customer = ko.mapping.toJS(customer);

            if(
                _customer.customer_first_name   ||
                _customer.customer_last_name    ||
                _customer.customer_mobile_phone ||
                _customer.customer_email
                )
                return false;
            else {
                return true;
            }
        },

        // Remove the customer from the order -- Opens space for a new one
        removeCustomer: function (customer) {

            if (this.customerIsEmpty(customer)){
                alert("Customer is already empty");
                return;
            }

            const confirmAction = () => {
                console.log("Removing...", customer);

                this.customers.remove(customer);

                // then, add a new empty customer
                const newCust = this.getNewCustomerInstance();
                this.pushThenSortCustomer(newCust);

                $.jGrowl("Customer Removed successfully !");
            };

            pos_warning({msg: "Remove customer from order ?", confirmAction})

        },

        addCustomer: function () {
            while (this.customers().length < this.groupNumberPeople()){
                const customer = this.getNewCustomerInstance();
                this.pushThenSortCustomer(customer);
            }
        },

        setReferralCustomer: function (customer) {
            this.selectedDetailInfoCustomer().customer_refered_by_id(customer.server_id);
            this.selectedDetailInfoCustomer().customer_refered_by(customer.customer_first_name + " " + customer.customer_last_name);
            this.custSelectVM.customerAutocomplete("");
            this.custSelectVM.customersAutocompleteList([]);
        },

        makeLeader: function (customer) {
            for (let c of this.customers()) {
                c.is_leader(false);
            }

            customer.is_leader(true);
        },

        getLeader: function () {
            if (this.customers().length === 0) {
                return false;
            } else {
                const leader = this.customers().filter(e => e.is_leader())[0];

                return leader;
            }
        },

        getLeaderName: function () {
            const leader = this.getLeader();
            console.log("leader", leader);

            if (leader) {
                return leader.customer_first_name() + " " + leader.customer_last_name();
            } else {
                return false;
            }
        },

        // Change tabs for the last step
        selectDetailInfoStepCurrentCustomer: function (customer) {
            this.selectedDetailInfoCustomer(customer);
            // $(".datepicker").datepicker({});
        },

        // Checks each step requirment before moving to the next one
        checkStepRequirments: function (step) {
            let result = { pass: true, message: "" };

            switch (step) {
                case "1":
                    if (this.groupName() === "") {
                        result.pass = false;
                        result.message = "Please fill in the group name first !";
                        break;
                    }

                    if (this.leaderPic() === "") {
                        result.pass = false;
                        result.message = "Please Take the leader picture first !";
                        break;
                    }

                    if (this.groupNumberPeople() < 2) {
                        result.pass = false;
                        result.message = "We need at least two people in the group !";
                        break;
                    }

                    break;

                case "2":
                    // if there are missing customer in the table
                    if (this.customers().length < this.groupNumberPeople()) {
                        result.pass = false;
                        result.message = `You selected ${this.groupNumberPeople()} customers, but there are only ${
                            this.customers().length
                            } in the table !!`;
                        break;
                    }

                    if (!result.pass) return result;

                    // Checks if the user filled up the fields
                    for (let c of this.customers()) {
                        if (
                            !c.customer_first_name() ||
                            !c.customer_last_name() ||
                            !c.customer_mobile_phone() ||
                            !c.customer_email()
                        ) {
                            result.pass = false;
                            result.message = "Please fill up all the customers information !";
                            return result;
                        }
                    }

                    if (!result.pass) return result;

                    // checks if there is a leader
                    result.pass = false;
                    result.message = "Please, You have to choose a Leader for the group !";

                    for (let c of this.customers()) {
                        if (c.is_leader()) {
                            // if find a leader => pass
                            result.pass = true;
                            result.message = "";
                            break;
                        }
                    }

                    if (!result.pass) return result;

                    // check if there is anyone present
                    result.pass = false;
                    result.message = "You can not continue without a present customer !";
                    for (let c of this.customers()) {
                        if (c.is_present()) {
                            // if find a leader => pass
                            result.pass = true;
                            result.message = "";
                            break;
                        }
                    }

                    if (!result.pass) return result;

                    // check if there is anyone paying
                    result.pass = false;
                    result.message = "You can not continue without a paying customer !";
                    for (let c of this.customers()) {
                        if (c.is_paying()) {
                            // if find a leader => pass
                            result.pass = true;
                            result.message = "";
                            break;
                        }
                    }

                    break;

                case "3":

                    if (!document.getElementById("setp3OfPreOrder").reportValidity()){
                        result.pass = false;
                        result.message = "Please fill up all the customers information !";
                        return result;
                    }
                    for (const customerIndex in this.customers()) {
                        const customer = this.customers()[customerIndex];
                        // check if all info for customer is fill
                        // !! => turn ['', null, 0, false, undefined] to false
                        if(customer.is_present()){
                            if (
                                [
                                    !!customer.customer_address1(),
                                    !!customer.customer_birth_day(),
                                    !!customer.customer_birth_month(),
                                    !!customer.customer_birth_year(),
                                    !!customer.customer_city(),
                                    !!customer.customer_country(),
                                    !!customer.customer_state(),
                                    !!customer.customer_email(),
                                    !!customer.customer_first_name(),
                                    !!customer.customer_last_name(),
                                    !!customer.customer_mobile_phone(),
                                    !!customer.customer_postal_code(),
                                    !!customer.customer_address2()
                                ].includes(false)
                            ) {
                                result.pass = false;
                                result.message = `Please fill up all the customers information for customer #${parseInt(customerIndex)+1}!`;
                                this.selectDetailInfoStepCurrentCustomer(customer);
                                return result;
                            }

                        } else {
                            //If customer is not present, only is required
                            // first_name, last_name, mobile_phone, email.
                            if([
                            !!customer.customer_email(),
                            !!customer.customer_first_name(),
                            !!customer.customer_last_name(),
                            !!customer.customer_mobile_phone(),
                            !!customer.customer_birth_day(),
                            !!customer.customer_birth_month(),
                            !!customer.customer_birth_year()
                            ].includes(false)
                            )
                            {
                                result.pass = false;
                                result.message = `Please fill up all the customers information for customer #${parseInt(customerIndex)+1}!`;
                                this.selectDetailInfoStepCurrentCustomer(customer);
                                return result;
                            }
                        }

                        // check if 'What did you type' is empty and shown
                        if(['1', '8', '9'].includes(customer.customer_referal_method()) && !customer.customer_referal_moreinfo()){
                            result.pass = false;
                            result.message = `Please fill up 'What did you type' information for customer #${parseInt(customerIndex)+1}!`;
                            this.selectDetailInfoStepCurrentCustomer(customer);
                            return result;
                        }

                    }


                    break;
            }

            return result;
        },

        // Se preOrders.customers()[0].is_present() for false, são obrigatorios apenas
        // nome, sobrenome, email e telefone
        nextStep: function () {
            let nextStep;

            let requirements = null;

            try {
                switch (this.currentStep().id) {
                    case "1":
                        requirements = this.checkStepRequirments("1");

                        console.log("requirements", requirements);

                        if (!requirements.pass) {
                            alert(requirements.message);
                            return;
                        }

                        nextStep = this.groupOrderSteps.filter(e => e.id == "2")[0];

                        // Creates the default table of customers
                        if (this.customers().length == 0) {
                            this.generateDefaultCustomerTable();
                        }
                        else{
                            // less then before
                            if (this.customers().length > this.groupNumberPeople()){
                                // removes first customers (by alphabetical order, more chances to remove a null customer)
                                this.customers.splice(0,(this.customers().length-this.groupNumberPeople()));
                            }

                            // more then before
                            else if(this.customers().length < this.groupNumberPeople()){
                                this.addCustomer();
                            }

                        }

                        this.currentStep(nextStep);
                        break;

                    case "2":
                        requirements = this.checkStepRequirments("2");
                        if (!requirements.pass) {
                            alert(requirements.message);
                            return;
                        }

                        nextStep = this.groupOrderSteps.filter(e => e.id == "3")[0];

                        // Use first custumer to show in the tab
                        this.selectedDetailInfoCustomer(this.customers()[0]);
                        this.currentStep(nextStep);

                        // Datepciker object for customer_DOB
                        $(".datepicker").datepicker();
                        break;

                    case "3":
                        requirements = this.checkStepRequirments("3");

                        if (!requirements.pass) {
                            alert(requirements.message);
                            return;
                        }
                        break;
                }
            } catch (error) {
                customAlert(error.name + ': ' + error.message);
            }

            $(".pre-order-step:visible").effect("slide", { direction: "right" }, 500);
        },

        lastStep: function () {
            let nextStep = null;

            switch (this.currentStep().id) {
                case "1":
                    alert("Cannot go back anymore !");
                    break;

                case "2":
                    nextStep = this.groupOrderSteps.filter(e => e.id == "1")[0];
                    this.currentStep(nextStep);
                    break;

                case "3":
                    nextStep = this.groupOrderSteps.filter(e => e.id == "2")[0];
                    this.currentStep(nextStep);
                    break;
            }
        },

        changeGroupNumberPeople: function (signal) {
            console.log("changing...", signal);

            if (signal === "+") {
                this.groupNumberPeople(this.groupNumberPeople() + 1);
            } else {
                if (this.groupNumberPeople() > 0) {
                    this.groupNumberPeople(this.groupNumberPeople() - 1);
                }
            }
        },

        /**
         * Submits the customer info to the ERP and proceed to the order process
         * @return {[type]} [description]
         */
        proceedToOrder: function () {
            var self = this;

            //Verify if email inserted is valid
            //This Regular Expression verify format email
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            if (!validateEmail(self.selectedDetailInfoCustomer().customer_email())) {
                customAlert("Wrong Email");
                return;
            }

            const requirements = this.checkStepRequirments("3");

            if (!requirements.pass) {
                alert(requirements.message);
                return;
            }

            const confirmAction = () => {
                $.ajax({
                    type: "POST",
                    timeout: 60000, // sets timeout to 60 seconds
                    url: BUrl + "orders_pos/prepare_order_customers",
                    dataType: "json",
                    data: {
                        user: authCtrl.userInfo,
                        customers: ko.mapping.toJSON(self.customers())
                    },
                    success: function (ret) {
                        console.log(ret);

                        if (ret.result) {
                            const groupOrdersData = {
                                customers_ids: ret.customers_ids,
                                leader_id: ret.leader_id,
                                customers_data: ko.mapping.toJS(self.customers()),
                                leaderPic: self.leaderPic(),
                                group_name: self.groupName(),
                                is_reorder: false
                            };
                            /**
                             * Writing the data in the local store, so we can retrieve it when we initiate the group orders
                             * process
                             */
                            localStorage.setItem(
                                "groupOrdersData",
                                JSON.stringify(groupOrdersData)
                            );
                            posChangePage("#orders");
                        } else {
                            alert("Something wrong... :( please try again");
                            //posChangePage('#main');
                        }
                    },
                    error: function (error) {
                        console.log(JSON.stringify(error));
                        customAlert(
                            "TIME OUT - there is a network issue. Please try again later"
                        );
                        //posChangePage('#main');
                    },
                    async: false
                });
            };

            pos_confirm({msg:"Submit customer info and proceed to order ?", confirmAction});


        },

        nextCustomer: function () {
            console.log("Next customer btn...");

            var self = this;
            try {
                const requirements = this.checkStepRequirments("3");
                if (!requirements.pass) {
                    alert(requirements.message);
                    return;
                }

                let currentCustIndex = 0;

                for (let i = 0; i < self.customers().length; i++) {
                    if (self.selectedDetailInfoCustomer() == self.customers()[i]) {
                        currentCustIndex = i;
                        break;
                    }
                }

                if (currentCustIndex < self.customers().length - 1) {
                    self.selectedDetailInfoCustomer(self.customers()[currentCustIndex + 1]);
                }

            } catch (error) {
                alert(error.name + ': ' + error.message);
            }

        },

        takePic: function () {
            console.log("taking pic...");

            var self = this;

            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation: true
            });

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(
                    imageURI,
                    resolveOnSuccess,
                    resOnError
                );
            }

            function onFail(message) {
                alert("Failed because: " + message);
            }

            function resolveOnSuccess(entry) {
                const folderName = "ClientMediaStorage";
                const imageName = new Date().getTime() + ".jpg";

                window.requestFileSystem(
                    LocalFileSystem.PERSISTENT,
                    0,
                    function (fileSys) {
                        //The folder is created if doesn't exist

                        fileSys.root.getDirectory(
                            folderName,
                            { create: true, exclusive: false },
                            function (directory) {
                                entry.moveTo(directory, imageName, successMove, resOnError);
                            },
                            resOnError
                        );
                    },
                    resOnError
                );
            }

            function successMove(entry) {
                console.log("After move");

                console.log("entry.toURL()", entry.toURL());
                //console.log("entry.toNativeURL()",entry.toNativeURL());
                //console.log("entry.fullPath", entry.fullPath);
                self.leaderPic(entry.toURL());
                // imageURI = entry.fullPath;
            }

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            }
        },

        /**
         * Sort array after push.
         *
         * leader first then sort by name
         * @param {*} customerToPush
         */
        pushThenSortCustomer: function (customerToPush) {
            // push
            this.customers.push(customerToPush);

            // sort (leader first, then sort by name)
            this.customers.sort((a, b) => {
                let a_ = ko.mapping.toJS(a);
                let b_ = ko.mapping.toJS(b);

                if (a_.is_leader) {
                    return -1;
                } else if (b_.is_leader) return 1;

                return (a_.customer_first_name+a_.customer_last_name).trim().toLowerCase() < (b_.customer_first_name+b_.customer_last_name).trim().toLowerCase() ? -1 : 1;
            });
        },

        clearPreOrder: function () {
            this.selectedCustomers(null);
            this.groupName("");
            this.customers([]);
            this.groupNumberPeople(0);
            this.currentStep(this.groupOrderSteps[0]);
            this.leaderPic("");
            this.selectedDetailInfoCustomer(null);
        }
    });
});
