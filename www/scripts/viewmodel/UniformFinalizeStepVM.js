define(['jquery', 'knockout', 'base'], function($, ko) {


    UniformFinalizeStepVM = SimpleControl.extend({

    	init: function(uniformTemplateVM) {

    		console.log("init uniform finalize step");
    		this.uniformTemplateVM = uniformTemplateVM;
    		var self = this;

    		this.canvasBase64 = ko.observable(false);
    		this.signing = ko.observable(false);
    	},

    	prepareCanvas : function() {

			console.log("preparing canvas...");

            // clear canvas
            this.canvasBase64(false);

			const divId = "canvas-wrapper";

			let canvasDiv = $("#"+divId).find("#canvasDiv");

			let canvas = document.createElement('canvas');

			canvas.setAttribute('width', 710);
			canvas.setAttribute('height', 350);
			canvas.setAttribute('id', 'canvasel');

			canvasDiv.append(canvas);
			if(typeof G_vmlCanvasManager != 'undefined') {
				canvas = G_vmlCanvasManager.initElement(canvas);
			}
			context = canvas.getContext("2d");

			clickX = new Array();
			clickY = new Array();
			clickDrag = new Array();
			context.clearRect(0, 0, canvas.width, canvas.height); // Clears the canvas

		    canvas.addEventListener('mousedown', _mousedown, false);
	        canvas.addEventListener('mousemove', _mousemove, false);
	        window.addEventListener('mouseup', _mouseup, false);

			canvas.addEventListener('touchstart', _touchstart, false);
	        canvas.addEventListener('touchmove', _touchmove, false);

	        $("#"+divId).show();

            // hide the BTN
            this.signing(true);
		},

		saveCanvas : function() {
			console.log("Saving canvas...");
			const canvas = document.getElementById('canvasel');
			this.canvasBase64(canvas.toDataURL("image/png", 0.6));

            //navigator.notification.alert("Signature Saved !");
            //alert("Signature Saved !");

            this.submitSignature()
		},

		clearCanvas : function() {
			console.log("clearing canvas...");

			$("#canvasDiv").html("");
            this.prepareCanvas();
		},

		submitSignature : function() {

		    console.log('Saving Signature...');
		    // using old endpoint for signature image
		    const base64 = this.canvasBase64().split(',')[1];
		    const customer_id = this.uniformTemplateVM.company().main_contact.customer_id;
		    const salesman = { "username" : authCtrl.username(), "password" : authCtrl.password(), "id" : authCtrl.userInfo.user_id };

			$("loading_jp").show();

		    $.ajax({
		        type: 'POST',
		        url: BUrl + 'client_image_upload/upload_signature_image',
		        dataType: 'json',
		        async: false,
		        data:{
		            salesman: JSON.stringify(salesman),
		            customer_id: customer_id,
		            data: base64
		        },
		        success: function(dataS) {
		            console.log(dataS);
		            customAlert("Signature saved successfully !");
		        },
		        error: function (error) {
		            console.log(JSON.stringify(error));
		            $.jGrowl('Signature Saved Succesfully !!');
		        },
                complete: function(data) {
                    $("loading_jp").hide();
                }
		    });


		}


    });

});