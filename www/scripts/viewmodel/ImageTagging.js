ImageTags = class ImageTags {
    /**
     * Class to handle tagging images
     *
     * How to USE:
     * - add wrappers in the view like this:
     *   <div id="image-tag-wrapper-[sufix]" class="image-tag-wrapper"></div>
     *   <div id="image-tag-wrapper-[sufix]-comment" class="image-tags-comment-wrapper"></div>
     *   <div id="image-tag-wrapper-[sufix]-popup"></div>
     * - The classes are important, but you can change the sufix of the ids
     */
    constructor(imageWrapperId, commentsWrapperId, popupWrapperId, image) {
        this.imageWrapperId = imageWrapperId;
        this.commentsWrapperId = commentsWrapperId;
        this.popupWrapperId = popupWrapperId;
        this.tags = [];
        this.imageId = image.id;
        this.imagePath = image.path;

    }

    /**
    * handle new tags
    * @param {*} imageId
    * @param {*} image_type
    * @param {*} pos_x
    * @param {*} pos_y
    */
    newTag(pos_x, pos_y) {
        // get message
        const msg = $("#title_comment_holder").val();

        this.tags.push({
            "left": pos_x,
            "top": pos_y,
            "text": msg,
            "change": true,
            "thai_text": msg
        })

        $('#popup_box').hide(500);
        this.render();

    }

    editTag(tagIndex){
        const msg = $("#title_comment_holder").val();
        this.tags[tagIndex].text = msg;
        $('#popup_box').hide(500);
        this.render();

    }

    deleteTag(tagIndex){
        this.tags.splice(tagIndex,1);
        this.render();
    }

    configureEventListeners() {
        var self = this;
        console.log('Configuring event listeners' );

        /** Clicking on image wrapper */
        $(`#image-tag-wrapper-${self.imageId}`).on("click", function (e) {
            console.log('e', e);

            const offset = $(this).offset();
            const width = e.target.width;
            const height = e.target.height;
            const relativeX = (e.pageX - offset.left);
            const relativeY = (e.pageY - offset.top);

            const x_percent = relativeX * 100 / width;
            const y_percent = relativeY * 100 / height;

            self.renderTagEditPopUp(x_percent, y_percent)

            // self.configureEventListeners2();
            $('#popup_box').show(500);

        });

        /** clicking on tag */
        // prevent bind more than one event to tag
        $(".image-tag-wrapper .tag").each((_, element) => {
            if (
                    !$._data(element, "events") ||
                    ($._data(element, "events") && !("click" in $._data(element, "events")))
                ){
                $(element).on("click", function(evt) {
                    event.stopImmediatePropagation();
                    const tag = $(evt.target).closest(".image-tag-wrapper .tag");

                    const offset = $(this).offset();
                    const width = evt.target.width;
                    const height = evt.target.height;
                    const relativeX = (evt.pageX - offset.left);
                    const relativeY = (evt.pageY - offset.top);

                    const x_percent = relativeX * 100 / width;
                    const y_percent = relativeY * 100 / height;
                    self.renderTagEditPopUp(x_percent, y_percent,tag.data("tagIndex"));
                    $('#popup_box').show(500);
                })
            }
        })

        /** Close button must be overwritten */
        $(".close-x").on("click", function(evt){
            evt.stopImmediatePropagation();

           alert("event must be overwrite");

        });

    }

    /**
     * configure event of popup-insert-new-tag
     */
    configureEventListenersOfNewTag(x_percent, y_percent, tagIndex) {
        var self = this;

        $("#insert-new-tag-button").off("click"); // disable any previous event
        $("#insert-new-tag-button").on("click", () => {
            this.newTag(x_percent, y_percent)
        });


        $("#edit-tag-button").on("click", () => {
            this.editTag(tagIndex);
            $('#popup_box').hide(500);
        })

        $("#delete-tag-button").on("click", () => {
            const confirmAction = () => {
                self.deleteTag(tagIndex);
                $('#popup_box').hide(500);
            };
            
            pos_warning({msg:'Are you sure you want to delete this tag?', confirmAction});
        })
    }

    /**
    * Form to create a new Tag
    * @param {*} imageId
    * @param {*} image_type
    * @param {*} pos_x
    * @param {*} pos_y
    */
    renderTagEditPopUp(pos_x, pos_y, tagIndex=undefined) {

        const html = /*html*/ `
            <div id="popup_box" style="width: 100%; display:none;">
                <div class="form_panel" name="form_panel" style="display: block;">
                    <a class="close_popups ui-link" id="popupBoxClose" onclick="$('#popup_box').hide(500)"> Close </a>
                    <p class="media_pops">
                        <span> Add your comments </span>
                        <textarea class="title ui-input-text ui-shadow-inset ui-body-inherit ui-corner-all ui-textinput-autogrow"
                            id="title_comment_holder" style="height: 35px;">${tagIndex == undefined ? "" : this.tags[tagIndex].text.trim()}</textarea>
                        <div class="" data-role="none" style="display: flex;flex-direction: column;justify-content: space-around;height: 96px;width: 143px;padding-left: 19px;">
                            ${(tagIndex == undefined) ?
                            (
                                /* html */` <button id="insert-new-tag-button" type="button" class="add-tag-button"> Add Comment </button>`
                            ) :
                            (
                                /* html */` <button id="edit-tag-button" type="button" class="edit-tag-button"> Edit Comment </button>
                                 <button id="delete-tag-button" type="button" class="delete-tag-button"> Delete Tag </button>`
                            )}

                        </div>
                    </p>
                    <div class="clear">
                    </div>
                </div>
            </div>
        `;

        $(`#${this.popupWrapperId}`).html(html);

        this.configureEventListenersOfNewTag(pos_x, pos_y, tagIndex);
    }

    /**
    * Render image with tags
    */
    renderImageWrapper() {
        let html;
        if (this.imagePath){
            html = /* html */`
                <img class="custom-design-img"  src="${this.imagePath}" >
                <div class="close-x" data-image-id="${this.imageId}">x</div>
                <div class="planetmap" style="width: 100%;height: 100%;">

                    ${this.tags.map(
                        (tag, index) => /* html */`
                            <div class="tag" data-tag-index="${index}" style="top: ${tag.top}%; left: ${tag.left}%;">
                                <span> ${index+1} </span>
                            </div>
                        `
                    ).join("")}
                </div>
            `
        }
        else {
            html = "";
        }

        $(`#${this.imageWrapperId}`).html(html);
        this.configureEventListeners();
    }

    /**
    * Render comments wrapper
    */
    renderComments(){
        const html = /*html*/` ${this.tags.map((tag, index) =>  `<span> ${index+1} </span>.
        <span> ${tag.text}</span>
        <br>`
        ).join("")}`

        $(`#${this.commentsWrapperId}`).html(html);
    }

    render(){
        this.renderImageWrapper();
        this.renderComments();
    }
}

DesignImageTags = class DesignImageTags extends ImageTags {
    constructor(imageWrapperId, commentsWrapperId, popupWrapperId, image, customCat, customOption, image_index){

        super(imageWrapperId, commentsWrapperId, popupWrapperId, image);

        this.customCat    = customCat ;
        this.customOption = customOption;
        // this.tagsCustomOption = this.tagsToOption(); // deprecated
        this.tagsCustomOption = customOption;
        this.image_index = image_index;
        this.tags = this.tagsCustomOption.selectedValue()[image_index].tags ? this.tagsCustomOption.selectedValue()[image_index].tags : [] ;

        if (!customCat || !customOption){
            console.error("bad initialization");
            return;
        }


    }

    // overwite
    configureEventListeners(){
        let self = this;
        $(".close-x").on("click", function(evt){
            evt.stopImmediatePropagation();

            const confirmAction = () => {
                const image_id = $(this).data("imageId");
                const [customOptionId, image_index] = image_id.split("-");

                let valueToUpdate = self.tagsCustomOption.selectedValue();

                valueToUpdate.splice(image_index,1); // right way

                self.tagsCustomOption.selectedValue(valueToUpdate);
            };

            pos_warning({msg:'Are you sure you want to delete this Image?', confirmAction});


        });

        super.configureEventListeners();
    }


    // overwrite
    render(){
        // this.tagsToOption();
        if(this.tagsCustomOption){
            const valueToUpdate = this.tagsCustomOption.selectedValue();
            valueToUpdate[this.image_index].tags = this.tags;
            this.tagsCustomOption.selectedValue(valueToUpdate);
        }
        else{
            console.warn("tagsCustomOption is undefined");
        }
        super.render();
    }
}

FittingImageTags = class extends ImageTags{
    constructor(imageWrapperId, commentsWrapperId, popupWrapperId, image){
        super(imageWrapperId, commentsWrapperId, popupWrapperId, image);
    }

    /**
    * Render image with tags
    * (overwritten): Do not show close button
    */
    renderImageWrapper() {
        let html;
        if (this.imagePath){
            html = /* html */`
                <img class="custom-design-img fitting-img-tagging"  src="${this.imagePath}" >
                <div class="planetmap" style="width: 100%;height: 100%;">

                    ${this.tags.map(
                        (tag, index) => /* html */`
                            <div class="tag" data-tag-index="${index}" style="top: ${tag.top}%; left: ${tag.left}%;">
                                <span> ${index+1} </span>
                            </div>
                        `
                    ).join("")}
                </div>
            `
        }
        else {
            html = "";
        }

        $(`#${this.imageWrapperId}`).html(html);
        this.configureEventListeners();
    }

    /**
    * Render comments wrapper
    * (overwritten):
    * - The class of the tag
    * - Remove 'dot'
    */
    renderComments(){
        const html = /*html*/` ${this.tags.map((tag, index) =>  /* html */`<span class='comment-tag'> ${index+1} </span>
        <span> ${tag.text}</span>
        <br>`
        ).join("")}`

        $(`#${this.commentsWrapperId}`).html(html);
    }

}