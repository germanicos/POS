define(['jquery', 'knockout', 'base', ], function($, ko, _) {
    AlterationsBriefVM = class {
       constructor(is_global=false){

            this.briefList = ko.observableArray();
            this.currentCustomer = ko.observable("");
            this.url_to_get_data = is_global ? "alterations_pos/get_global_alteration_brief" : "alterations_pos/get_alteration_brief"

            changeVisibilityDiv('loading_jp');

            console.log("Init AlterationsBrifVM");

            this.getAlterationDetails();
        }

        chooseCustomer(customer){
            console.log("changing customer", customer);
            this.currentCustomer(customer)
        }

        /**
         * Init a briefDetails with the corresponding fit and go to next page
         */
        goToBriefDetails(nextPage, data){

            window.briefDetails = new BriefDetailsVM(data);
            posChangePage(nextPage)

        }

        getAlterationDetails(){
            // POST to the server to the get alteration details
            $.ajax({
                type: 'POST',
                url: BUrl + this.url_to_get_data,
                dataType: 'json',
                data :  { "user" : authCtrl.userInfo },
                async: true,
                timeout: 60000,
                success: (ret) => {

                    console.log(ret);

                    changeVisibilityDiv('loading_jp');


                    for (let i = 0; i < ret.length; i++) {
                        this.briefList.push(ret[i]);
                    }


                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    changeVisibilityDiv('loading_jp');

                    customAlert("TIME OUT - there is a network issue. Please try again later");
                    posChangePage("#alterationsDashBoard");


                }
            });
        }

    }
});