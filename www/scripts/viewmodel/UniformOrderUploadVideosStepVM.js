define(['jquery', 'knockout', 'base'], function ($, ko) {
 
 
    UniformOrderUploadVideosStepVM = SimpleControl.extend({
 
        init: function () {
            var self = this;
            console.log("init upload video VM");
 
            this.videos = ko.observableArray([]);
            this.currentComment = ko.observable("");
            /* self = uniformOrder.uploadVideosStep

                self.videos.push({
                    'url' : 'https://youtu.be/PBvCaGBJduc',
                    'comments' : ko.observableArray([])
                })  */
        },
    
        getSubmissionData : function() {

            return ko.mapping.toJS(this.videos);

        },
 
        addComment : function(video) {
 
            console.log("Adding comment...");
            video.comments.push(this.currentComment());
            this.currentComment("");
        },
 
        getNewVideo : function() {
 
            return {
                    'url' : '',
                    'comments' : ko.observableArray([])
                };
 
        },
 
        removeVideo : function(video) {
 
            var self = this;
 
            navigator.notification.confirm(
                'Do you really want to remove this comment ?', // message
                function(btnIndex) {
                     
                    if(btnIndex == 2)
                    {
                        self.videos.remove(video);
                    }
 
                },            
                'Confirmation',           
                ['No', 'Yes'] 
            );
 
        },
 
 
        removeComment : function(video, comment) {
 
            var self = this;
 
            navigator.notification.confirm(
                'Do you really want to remove this comment ?', // message
                function(btnIndex) {
                     
                    if(btnIndex == 2)
                    {
                        video.comments.remove(comment);
                    }
 
                },            
                'Confirmation',           
                ['No', 'Yes'] 
            );
 
 
 
        },
 
        chooseVideoSource : function() {
 
            var self = this;
 
            console.log("choosing Video source...");
 
            navigator.notification.confirm(
                'Video Source', // message
                function(btnIndex) {
                    self.takeVideo(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );
 
        },
 
        takeVideo : function(buttonIndex) {
 
            self = this;
             
            console.log("Taking Video...");
 
            if( buttonIndex == 1 ) // from camera
            {
                navigator.device.capture.captureVideo(onSuccess, onFail,
                {
                    quality: 1,
                    saveToPhotoAlbum: true           
                }); 
            }
            else // From gallery
            {
                navigator.camera.getPicture(onGalleryDataSuccess, onFail, {
                                quality: 1,
                                destinationType: Camera.DestinationType.FILE_URI,
                                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                mediaType: Camera.MediaType.VIDEO,
                                correctOrientation : true
                            });
            }
            
 
            function onSuccess(mediaFiles) { 
 
                console.log("onVideoDataSuccess", mediaFiles);
                var i, path, len;
 
                for (i = 0, len = mediaFiles.length; i < len; i += 1) {
 
                    console.log(mediaFiles[i].localURL);
                    console.log(mediaFiles[i].fullPath);
                    window.resolveLocalFileSystemURL(mediaFiles[i].localURL, resolveOnSuccess, resOnError);
                }
            }  
 
            function onGalleryDataSuccess(videoURI) {
                console.log("onGalleryDataSuccess");
                window.resolveLocalFileSystemURL(videoURI, successMove, resOnError);
            };
 
 
            function onFail(message) { 
                alert('Failed because: ' + message); 
            } 
 
            function resolveOnSuccess(entry) {
                 
                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.mov';
                 
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist
                     
                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },
                resOnError);
            };
 
            function successMove(entry) {
                console.log('After move');
                 
                console.log("entry.toURL()", entry.toURL());
 
                const video = uniformOrder.uploadVideosStep.getNewVideo();
                video.url = entry.toURL();
 
                uniformOrder.uploadVideosStep.videos.push(video);
            };
 
            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };
 
        }, // end Capturing pic
 
        checkStepIsComplete: function () {
            return ko.computed(() => {
                return this.videos().length > 0;
            })
        },
     
    });
});