define(['jquery', 'knockout', 'base'], function($, ko) {


    Profitability = SimpleControl.extend({

        init: function() {
            console.log('Init ProfitabilityVM');

            // to search customer:
            this.custSelectVM = new CustomerSelectClass(dsRegistry);
			this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
            this.custSelectVM.setParentVM(this);

            this.selectedCustomer = ko.observable(false);

            this.availableOrders = ko.observableArray([]);
            this.selectedOrder = ko.observable(false);

            this.availableGarments = ko.observableArray([]);
            this.selectedGarment = ko.observable(false);

            this.selectedAmount = ko.observable(0);
            this.selectedDescription = ko.observable("");

            this.profitabilities = ko.observableArray([]);

            this.configureListeners();

            window.debug_profitability = this;

        },

        configureListeners(){
            this.selectedCustomer.subscribe((newValue) => {
                this.setAvailableOrders();
            })

            this.selectedOrder.subscribe(newValue => {
                this.setAvailableGarments();
            })

            this.selectedGarment.subscribe(newValue => {
                this.setProfitabilities();
            });
        },

        selectCustomer(customer){
            console.log("appending client to list.", customer);

            this.selectedCustomer(customer);
            this.custSelectVM.customersAutocompleteList([]);

            this.custSelectVM.customerAutocomplete(`${customer.customer_first_name} ${customer.customer_last_name}`);

            $.jGrowl("Added successfully");

        },

        setAvailableOrders(customer = this.selectedCustomer()){
            if (customer){

                $.post(window.BUrl + "fittings/get_customer_orders_ajax_v2", { "customer_id" : customer.customer_id }, (ret) => {

                    console.log(ret);
                    const orders = Object.values(ret);

                    for(order of orders) {
                        order.show_text = `${order.order_id} - ${order.ordered_date}`;
                    }

                    console.log('orders', orders);
                    this.availableOrders(orders);


                }, "json");



            }
        },

        setAvailableGarments(order = this.selectedOrder()){
            if (order){
                const garments = order.garments;
                for (const garment of garments) {
                    garment.show_text = `${garment.name} ${garment.fabric}`
                }
                this.availableGarments(garments);
            }
        },

        setProfitabilities(garment = this.selectedGarment()){
            if(garment){
                let garment_id = garment.id;

                $.post(BUrl + "profitability/get_garment_extra_costs", { 'garment_id' : garment_id }, (ret) => {

                    console.log('ret.costs', ret.costs);
                    this.profitabilities(ret.costs);

                }, "json");

            }

            else {
                this.profitabilities([]);
            }
        },

        submitProfitability(){
            const amount = this.selectedAmount();
            const description = this.selectedDescription()

            const customer_name = this.custSelectVM.customerAutocomplete();
            const garment_name = this.selectedGarment().show_text;
            const garment_id = this.selectedGarment().id;

            if( !amount ){
                customAlert('Please set an valid amount !');
                return;
            }


            if( !description ) {
                customAlert('Please set an valid description !');
                return;
            }

            const confirmAction = () => {
                $.post(BUrl + "profitability/pos_add_extra_cost_ajax",
                    { 'garment_id': garment_id,
                    'amount': amount,
                    'description': description,
                    'user_data': authCtrl.userInfo }
                , (ret) => {

                    if(ret.result === 'success') {
                        alert('Cost Added !!');
                        this.setProfitabilities();

                    }
                    else {
                        alert('Something wrong cost not added :((');

                    }

                }, "json");

            };
            pos_confirm({msg:`Do you really want to add an extra cost for ${customer_name} ${garment_name} ?`, confirmAction});
        },

        removeCost(cost_id){

            $.post(BUrl + "profitability/delete_extra_cost_ajax", { 'cost_id' : cost_id }, (ret) => {
                alert('Extra cost deleted !');

                this.setProfitabilities();
            }, "json");
        }


    });
});