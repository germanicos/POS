define(['jquery', 'knockout', 'base'], function($, ko) {


    /**
     * 3rd step for uniform order -- most important
     *
     * This class will be responsible to get a precreated template
     * and proceed with the order, taking measurements, bodyshapes, signature, payments, ETC.
     * This will be like the order process for uniform.
     */
    UniformOrderVM = SimpleControl.extend({

    	init: function(companyId) {

            var self = this;

            console.log("Init UniformOrderVM...");

            this.company = ko.observable(false);

            // Step viewmodels
            this.contactPersonStep = false;
            this.garmentsStep = false;
            this.customerDetailsStep = false;
            this.paymentsStep = false;
            this.bodyShapesStep = false;
            this.uploadImagesStep = false;
            this.uploadVideosStep = false;

            // Controls if the popup for statistics is visible or not
            self.statistics = new UniformStatisticsVM(companyId);

            this.getUniformCompanyData(companyId);

            // Locations
            this.cities = dsRegistry.getDatasource('citiesDS').getStore();
            this.states = dsRegistry.getDatasource('statesDS').getStore();
            this.countries = dsRegistry.getDatasource('countriesDS').getStore();

            this.referal_methods_list = ko.observableArray([
                { "name": "Google Search",       "id": "1" },
                { "name": "Bing Search",         "id": "8" },
                { "name": "Yahoo Search",        "id": "9" },
                { "name": "Print Advertisement", "id": "2" },
                { "name": "TV Ad",               "id": "3" },
                { "name": "Radio Ad",            "id": "4" },
                { "name": "Billboard",           "id": "5" },
                { "name": "Other",               "id": "6" },
                { "name": "Referral",            "id": "7" },
                {"name": "Facebook",             "id": "10"},
                {"name": "Instagram",             "id": "11"},
                {"name": "LinkedIn",             "id": "12"}
            ]);

            // for dates
            this.days = ko.observableArray([]);
            // for (i = 1; i <= 31; i++) this.days.push(i.toString());
			for (i = 1; i <= 31; i++) {
				// if number is 1,2,3,4...9 => convert to 01, 02, 03 ... 09
				let formatNumber = i.toString();
				if (formatNumber.length < 2){
					formatNumber = "0"+formatNumber;
				}
				this.days.push(formatNumber);
			};

            this.months  = ko.observableArray([{"name":"January","id":"1"},{"name":"February","id":"2"},{"name":"March","id":"3"},{"name":"April","id":"4"},{"name":"May","id":"5"},{"name":"June","id":"6"},{"name":"July","id":"7"},{"name":"August","id":"8"},{"name":"September","id":"9"},{"name":"October","id":"10"},{"name":"November","id":"11"},{"name":"December","id":"12"}]);

            this.years = ko.observableArray([]);
            for (i = (new Date()).getFullYear()+1; i >= 1930; i--) this.years.push(i.toString());


            this.isStatisticsVisible = ko.observable(false);

            this.steps = this.buildSteps();
            this.selectedStep = ko.observable(this.steps[0]);

        },

        getUniformCompanyData : function(companyId) {

            console.log("Fetching data from server...");
            var self = this;

            $.ajax({
                type: 'POST',
                timeout: 120000, // sets timeout to 120 seconds
                url: BUrl + 'orders_pos/prepare_uniform_order_process',
                dataType: 'json',
                data: {
                    "user" : authCtrl.userInfo,
                    "company_id" : companyId
                },
                success: function (dataS) {

                    console.log(dataS);

                    if( dataS.templates.length === 0 )
                    {
                        posChangePage("#uniformCompanySearch");
                        customAlert("There is no Master Design (templates) yet for this company, you must create the Master Design (template) first !!!");
                        $('#loading_jp').hide();
                        return;
                    }

                    self.company(ko.mapping.fromJS(dataS.company));

                    // Creates and sets the viewmodels
                    self.contactPersonStep = new UniformOrderContactPersonVM(self);
                    self.garmentsStep = new UniformOrderGarmentsStepVM(self, dataS.templates);
                    self.customerDetailsStep = new UniformOrderCustomerDetailsVM(self, dataS.fitlines, dataS.measurements, dataS.garments_design);

                    self.bodyShapesStep = new UniformOrderBodyShapesStepVM(dataS.bodyshapes, self.garmentsStep);

                    self.uploadImagesStep = new UniformOrderUploadImagesStepVM(self);
                    self.uploadVideosStep = new UniformOrderUploadVideosStepVM(self);

                    self.paymentsStep = new UniformOrderPaymentsVM(self, dataS.payment_methods, dataS.discount_codes);

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: false
            });


        },



        /**
         * TODO : Figure it out how to put statistics/order history in the process
         *     We need to be able to call this pages from within the process and from outside as well.
         *         Consider: Building new viewmodels just for this and putting inside this one ??
         * @return {[type]} [description]
         */
        buildSteps: function() {

            return [
                    {
                        'name' : 'Contact',
                        'id' : '1',
                        'complete': this.contactPersonStep.checkStepIsComplete()
                    },
                    {
                        'name': 'Garments Step',
                        'id' : '2',
                        'complete': this.garmentsStep.checkStepIsComplete()
                    },
                    {
                        /*
                         * This step will be responsible for:
                         *     1. Measurments
                         *     2. Garment specific details: 'fit' / monogram / etc ...
                         */
                        'name': 'Measurments',
                        'id' : '3',
                        'complete': this.customerDetailsStep.checkStepIsComplete()
                    },
                    {
                        'name': 'Upload Images',
                        'id' : '5',
                        'complete': this.uploadImagesStep.checkStepIsComplete()
                    },
                    {
                        'name': 'Upload Fitline Video',
                        'id' : '6',
                        'complete': this.uploadVideosStep.checkStepIsComplete()
                    },
                    {
                        'name': 'Body Shape',
                        'id' : '4',
                        'complete': this.bodyShapesStep.checkStepIsComplete()
                    },
                    {
                        'name': 'Payment',
                        'id' : '7',
                        'complete': this.paymentsStep.checkStepIsComplete()
                    }
                ];
        },


        stepBack : function() {

            console.log("Steping back...");

            const currentStep = this.selectedStep();
            const currentStepId = parseInt(currentStep.id);

            if(currentStepId <= 1)
            {
                posChangePage("#main");
                return;
            }

            this.changeStep(currentStepId - 1);
        },


        nextStep: function() {

            // validate form
            for (const form of document.querySelectorAll("form.validateForm")) {
                if (!form.reportValidity()){
                    return;
                }
            }

            const currentStep = this.selectedStep();
            const currentStepId = parseInt(currentStep.id);

            if( currentStepId == 3 ) // measurements step => auto sync measurements
            {
                this.customerDetailsStep.syncMeasurements();
            }

            if( currentStepId >= 7 ) { alert('You are in the last step'); return; }

            // get current step position in array
            const currentStepPosition = this.steps.indexOf(currentStep);

            const nextStep = this.steps[currentStepPosition + 1];

            console.log('Next BTN...', nextStep);

            this.changeStep(nextStep.id);

            // Back to top
            $('html,body').animate({
                scrollTop: 0
            }, 700);

        },


        changeStep: function(stepId) {


            $('#loading_jp').show( () => {

                console.log("changing step: ", stepId);

                // Get first element whose id equals to stepId
                const newStep = this.steps.filter( e => e.id == stepId)[0];

                console.log("new step", newStep);

                // this.selectedCustomer().selectedStep(newStep);
                this.selectedStep(newStep);

                // Ui sugar
                $(".order-step:visible").effect("slide", { direction: 'right' }, 500);

                document.getElementById('loading_jp').style.display = "none";

                switch(stepId)
                {
                    case '3': // measurements

                        // Select first type of measurements if any is selected
                        this.customerDetailsStep.autoSelectGarmentType();

                    break;
                    case '4':
                        this.bodyShapesStep.stepVisited(true);
                        break;
                    case '7': // finalize step...

                        if( !this.paymentsStep.canvasBase64() ) // if there is no signature yet...
                        {
                            this.paymentsStep.prepareCanvas(); // prepare the canvas to sign
                        }

                    break;
                }


                document.getElementById('loading_jp').style.display = "none";
            }); // end step loader
        },

        exit: function () {

            const confirmAction = () => {
                posChangePage('#main');
                $.jGrowl('Order cancelled');
            };

            pos_warning({msg:"Do you really want to cancel this order ?", confirmAction});


        },

        submitUniformOrder : function() {
            // check blank fields with required attribute of tag form
            for (const el of document.querySelectorAll("form.validateForm")) {
                if(!el.reportValidity()){
                    customAlert("Please, check if all mandatory info are filled");
                    return false;
                }
            }

            const customer_obj = this.contactPersonStep.getSubmissionData();
            const measurements = this.customerDetailsStep.getSubmissionData();
            const payments     = this.paymentsStep.getSubmissionData();
            const bodyshapes   = this.bodyShapesStep.getSubmissionData();
            const images       = this.uploadImagesStep.getSubmissionData();
            const videos       = this.uploadVideosStep.getSubmissionData();

            const garments     = this.garmentsStep.getSubmissionData(); // Special Structure


            // Checks requirements before submission
            let orderRequirements = this.checkOrderRequirements();

            // requirement is missing
            if (!orderRequirements.status) // mandatory missing
            {

                this.changeStep(orderRequirements.stepId);
                if (orderRequirements.message){
                    $.jGrowl(orderRequirements.message);
                }
                return;
            } else if(orderRequirements.status == "warning"){ // not mandatory: ask if user want continue anyway
                if (orderRequirements.message){
                    if (!confirm(orderRequirements.message)){
                        this.changeStep(orderRequirements.stepId);
                        return;
                    }
                }
            }

            const confirmAction = () => {
                // Will be reorder if the customer is not locked (is_present) and the ISREORDER flag is TRUE
                const is_reorder   = this.isReorder && !customer.is_locked;

                const device_id = device.uuid;

                const order = {
                        'customer'     : customer_obj,
                        'videos'       : videos,
                        'images'       : images,
                        'measurements' : measurements,
                        'payments'     : payments,
                        'garments'     : garments,
                        'bodyshapes'   : bodyshapes,
                        'company_id'   : this.company().company_id()
                    }


                $('#loading_jp').show( () => {

                    $.ajax({
                        type: 'POST',
                        timeout: 60000, // sets timeout to 60 seconds
                        url: BUrl + 'orders_pos/submit_uniform_orders',
                        dataType: 'json',
                        data: {
                            "user": authCtrl.userInfo,
                            "submission_data": JSON.stringify(order),
                            "device_id" : device_id
                        },
                        success: function (dataS) {

                            console.log(dataS);

                            if( dataS.result == 'success' )
                            {
                                $.jGrowl("Order successfully taken !");
                                posChangePage("#main");
                            }
                            else
                            {
                                customAlert("Something wrong happned : ( please try again...");
                                customAlert(JSON.stringify(dataS));
                            }

                        },
                        error: function (error) {

                            console.log(JSON.stringify(error));
                            customAlert("TIME OUT - there is a network issue. Please try again later");

                        },
                        async: false
                    });

                });

            };
            pos_confirm({msg:'Do you really want to submit the order ?', confirmAction});




        },

         /**
         * Check all requirements to submit a order.
         *
         * @returns {status: boolean, stepId: number, message:String}.
         *      stepId : if a requirement is missing, this method will return a id to change step
         *      message: (optional): a message to show to user
         */
        checkOrderRequirements(){

            const response = { status: true, stepId: null, message: null };

            if (!this.contactPersonStep.checkStepIsComplete()()){
                response.status = false;
                response.stepId = "1";
                response.message = "Please, check CONTACT step;"
            }
            else if (!this.garmentsStep.checkStepIsComplete()()){
                response.status = false;
                response.stepId = "2";
                response.message = "Please, check GARMENT step;"
            }
            else if (!this.customerDetailsStep.checkStepIsComplete()()){
                response.status = false;
                response.stepId = "3";
                response.message = "Please, check MEASUREMENT step;"
            }
            else if (!this.uploadImagesStep.checkStepIsComplete()()){
                response.status = "warning";
                response.stepId = "5";
                response.message = "Are you sure you want continue without complete IMAGE step;"
            }
            else if (!this.uploadVideosStep.checkStepIsComplete()()){
                response.status = "warning";
                response.stepId = "6";
                response.message = "Are you sure you want continue without complete VIDEO step;"
            }
            else if (!this.bodyShapesStep.checkStepIsComplete()()){
                response.status = false;
                response.stepId = "4";
                response.message = "Please, check BODYSHAPE step;"
            }
            else if (!this.paymentsStep.checkStepIsComplete()()){
                response.status = false;
                response.stepId = "7";
                response.message = "Please, check PAYMENT step;"
            }

            return response;
        }
    });


});