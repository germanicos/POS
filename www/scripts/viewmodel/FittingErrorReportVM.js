define(['jquery', 'knockout', 'base', 'viewmodel/ImageTagging', 'viewmodel/GenericErrorVM'], function ($, ko) {


    /*
    ███████╗██╗████████╗████████╗██╗███╗   ██╗ ██████╗     ███████╗██████╗ ██████╗  ██████╗ ██████╗ ███████╗   ██████╗ ███████╗██████╗  ██████╗ ██████╗ ████████╗
    ██╔════╝██║╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝     ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗██╔════╝   ██╔══██╗██╔════╝██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝
    █████╗  ██║   ██║      ██║   ██║██╔██╗ ██║██║  ███╗    █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝███████╗   ██████╔╝█████╗  ██████╔╝██║   ██║██████╔╝   ██║
    ██╔══╝  ██║   ██║      ██║   ██║██║╚██╗██║██║   ██║    ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗╚════██║   ██╔══██╗██╔══╝  ██╔═══╝ ██║   ██║██╔══██╗   ██║
    ██║     ██║   ██║      ██║   ██║██║ ╚████║╚██████╔╝    ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║███████║   ██║  ██║███████╗██║     ╚██████╔╝██║  ██║   ██║
    ╚═╝     ╚═╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝   ╚═╝  ╚═╝╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝

    */
    /**
     * Global class to deal with a set o fitting errors of a garment
     */
    FittingErrorsReport = class FittingErrorsReport {
        constructor(bar_code, garment_type) {
            this.bar_code = bar_code;

            this.garment_type = garment_type; // 1,2,3 or 4

            // a list of FittingErrors instances
            this.fittingErrors;

            // copy of fittingError list. So we can discard changes if the user wants
            // before we go to next popup, we make this copy (on create a new error or edit existent one)
            this.fittingErrors_copy = ko.observableArray();

            this.tags;
            this.whoIsFault;
            this.getNecessaryDataFromERP();

            this.currentErrorReport = ko.observable(false);

            // in fitting we can have "front", "side", "back" and "additional" (additional is a list).
            // Those variables tell us which one we are tagging
            this.currentTaggingImage = null;
            this.currentTaggingImage_additionalIndex = null;
        }

        /**
         * make a post from ERP to build previous errors from ERP
         */
        getNecessaryDataFromERP() {

            const self = this;

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'errors_pos/get_errors_creation_data',
                dataType: 'json',
                data: {
                    "user"         : authCtrl.userInfo,
                    'device_id'    : device.uuid
                },

                success: function (dataS) {

                    let tags = [];

                    if( self.garment_type )
                    {
                        tags = dataS.tags_list.filter( tag => tag.garment_type == self.garment_type); // keys: {id, name, description}
                    }
                    else
                    {
                        tags = dataS.tags_list; // keys: {id, name, description}
                    }

                    console.log('tags', tags);

                    const whoIsFault = dataS.fault_list; // keys: {id, name, description}


                    self.fittingErrors = ko.observableArray([]);
                    self.whoIsFault = ko.observableArray(whoIsFault);
                    self.tags = ko.observableArray(tags);
                },

                error: function (error) {
                    console.warn("Error on errors_pos/get_errors_creation_data", error);
                    customAlert("Fitting Errors could not be obtained, please try again");
                },

                async: true
            });

        }

        /**
         * select an error and then show popup
         * @param {*} errorReport
         */
        selectErrorReport(errorReport) {
            this.currentErrorReport(errorReport);
            $('#fittingErrorReport').show(500);
            this.currentErrorReport().renderImageTags();
        }

        /**
         *  copy of fittingError list. So we can discard changes if the user wants
         *  before we go to next popup
         */
        cloneFittingErrors(){
            this.fittingErrors_copy(this.fittingErrors().map(error => error.getClone()));
        }

        /**
         * Clone before show popUp
         * @param {*} errorReport
         */
        editOneError(errorReport){
            this.cloneFittingErrors();
            this.selectErrorReport(errorReport);
        }

        addNewErrorReport() {
            this.cloneFittingErrors();

            // create new Error report
            const newErrorReport = new FittingError();

            // insert into list
            this.fittingErrors.push(newErrorReport);

            this.selectErrorReport(newErrorReport);
        }

        /**
         * remove last error already added
         */
        deleteCurrentError() {
            const confirmAction = () =>{
                this.fittingErrors.remove(this.currentErrorReport());
                $('#fittingErrorReport').hide(500);
            }

            pos_warning({msg: "Are you sure, you want to remove this error?", confirmAction})
        }

        deleteErrorByPosition(positionIndex){
            const confirmAction = () =>{
                this.fittingErrors.splice(positionIndex, 1);
            }

            pos_warning({msg: "Are you sure, you want to remove this error?", confirmAction})
        }

        saveError() {
            // validate form
            if (!this.currentErrorReport().title()){
                customAlert("Please, fill 'Title' field");
                return;
            }
            if (!(this.currentErrorReport().tags() && this.currentErrorReport().tags().length > 0)){
                customAlert("Please, set at least one 'Tag'");
                return;
            }
            if (!this.currentErrorReport().currentWhoIsFault()){
                customAlert("Please, set a 'Who is fault");
                return;
            }

            for (const form of document.querySelectorAll("form.validateForm")) {
                if (!form.reportValidity()) {
                    customAlert("Please, check if all mandatory info are filled");
                    return;
                }
            }

            // if this error have at least one image or one video
            if (this.currentErrorReport().images().length == 0 && this.currentErrorReport().videos().length == 0){
                customAlert("Please, check if error have at least one image or one video");
                return;
            }

            // ask if user wants to remove this image from fitting
            // check if the image is in the fitting from the url
            for (const image of orderItem.fittingsVM.customImages.media()) {
                for (const img of this.currentErrorReport().images()) {
                    if (image.preview() == img.imagePath) {
                        orderItem.fittingsVM.customImages.removeMedia_custom_msg(this.currentTaggingImage_additionalIndex);
                    }
                }
            }

            // go to main page of fitting
            $('.additional-img-wrapper').hide();
            $("#step-0").trigger("click");

            // close popup
            $('#fittingErrorReport').hide(500);
        }

        cancelChanges(){
            const confirmAction = () => {
                this.fittingErrors(this.fittingErrors_copy.slice(0));
                $("#fittingErrorReport").hide("slow");
            };

            pos_warning({msg:'Are you sure, you want to cancel and discard all changes?', confirmAction});

        }

        getSubmissionData() {
            return ko.mapping.toJS({
                bar_code      : this.bar_code,
                fittingErrors : this.fittingErrors().map(fittingError => fittingError.getSubmissionData())
            });
        }

        get errorsCount(){
            return this.fittingErrors().length;
        }
    }


    /*
    ███████╗██╗████████╗████████╗██╗███╗   ██╗ ██████╗    ███████╗██████╗ ██████╗  ██████╗ ██████╗
    ██╔════╝██║╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝    ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗
    █████╗  ██║   ██║      ██║   ██║██╔██╗ ██║██║  ███╗   █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝
    ██╔══╝  ██║   ██║      ██║   ██║██║╚██╗██║██║   ██║   ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗
    ██║     ██║   ██║      ██║   ██║██║ ╚████║╚██████╔╝   ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║
    ╚═╝     ╚═╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝    ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝
    */
    /**
     * Private class to deal with a single fitting error
     * >> To test: orderItem.fittingsVM.FittingData[0].fittingErrorsReport.currentErrorReport()
     */
    class FittingError extends GenericErrorBase {
        constructor() {
           super();

           this.isRemake = ko.observable(false);
           this.remakeNotes = ko.observable("");
           this.remakeFullOrPartial = ko.observable("full");
        }

        /**
         * get clone of this object.
         */
        getClone(){
            const temp = new FittingError();

            temp.title(this.title());
            temp.notes(this.notes());
            temp.tags(this.tags());
            temp.errorImportance(this.errorImportance());
            temp.deductMoney(this.deductMoney());
            temp.deductMoneyNote(this.deductMoneyNote());
            temp.currentTag(this.currentTag());
            temp.currentWhoIsFault(this.currentWhoIsFault());
            temp.whoIsFault(this.whoIsFault());

            temp.isRemake(this.isRemake());
            temp.remakeNotes(this.remakeNotes());
            temp.remakeFullOrPartial(this.remakeFullOrPartial());

            // get image clone
            temp.images(this.images())

            // get video clone
            // TODO: ...
            temp.videos(this.videos())

            return temp;
        }

        /**
         * Adds a new tag to the image.
         * Do not confuse with GARMENT tags
         * @param {*} text
         * @param {*} event
         * @param {*} image
         */
        newImageTag(text, event, currentTaggingImage, currentTaggingImage_additionalIndex) {

            let image;
            // get first image of the list
            if (this.images().length == 0){
                console.warn("no image to add tag");
                return;
            } else {
                image = this.images()[currentTaggingImage_additionalIndex];
            }



            if (text) {
                const target = event.target;
                var image_left = $(target).offset().left;
                var click_left = event.pageX;
                var left_distance = click_left - image_left;
                var image_top = $(target).offset().top;
                var click_top = event.pageY;
                var top_distance = click_top - image_top;
                var imagemap_width = $(target).width();
                var imagemap_height = $(target).height();
                var relative_left = (left_distance / imagemap_width) * 100;
                var relative_top = (top_distance / imagemap_height) * 100;

                const newTag = { left: relative_left, top: relative_top, text: text };

                console.log("TAG META", newTag);

                image.tags.push(newTag);

                // render all images (reload view)
                this.renderImageTags();

            }

        }

        /**
         * fix image stuffs
         * overwritten: remake stuffs
         */
        getSubmissionData(){
            const data = ko.mapping.toJS({
                title             : this.title(),
                notes             : this.notes(),
                tags              : this.tags(),
                // errorImportance   : this.errorImportance(),
                deductMoney       : this.deductMoney(),
                deductMoneyNote   : this.deductMoneyNote(),
                currentWhoIsFault : this.currentWhoIsFault(),
                isRemake          : this.isRemake(),
                remakeNotes       : this.remakeNotes(),
                remakeType        : this.remakeFullOrPartial(),

            });

            // if include image is true put in POST
            data.includedImages = this.images().map(image => {
                return {
                    tags: image.tags,
                    imagePath: image.imagePath,
                    }
                });

            // include videos into post
            data.videos = this.videos().map(video => {
                return {
                    videoURL: video.url
                }
            });


            console.log('Send data: ', data);
            return data;
        }

    }

})