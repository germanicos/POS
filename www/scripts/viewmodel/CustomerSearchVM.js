define(['jquery', 'knockout',
'viewmodel/CustomerCntClass',],
    function ($, ko) {
        CustomerSearchVM = class extends CustomerSelectClass {
            /**
             *
             * @param {boolean} fromSalesman should search only customers from current salesman (true) or general customer (false)
             */
            constructor() {
                super(dsRegistry);
            }

            /**
             * @override instead search customers only for current salesman (locally),
             * make a post to search all customers in DB
             *
             * @param {string} search
             */
            populateSuggestions(search) {
                if (search.length < 3) return;
                
                document.getElementById('loading_jp').style.display = "block";
                $.ajax({
                    type: 'POST',
                    url: window.BUrl + "customers/get_customer_by_name",
                    dataType: 'json',
                    async: false,
                    data: {
                        "user": authCtrl.userInfo,
                        "name" : search
                    },
                    success: (dataS) => {
                        this.customers(JSON.parse(dataS.response).movies);
                        console.log('customers', this.customers());

                        for (var ind in this.customers()) {
                            var custTemp = this.customers()[ind];
                            custTemp.customer_display_state = custTemp.customer_state_name;
                            custTemp.customer_mobile_phone = custTemp.mobile_phone;
                            custTemp.customer_id = custTemp.id;
                            custTemp.server_id = custTemp.id;
                            this.customersAutocompleteList.push(custTemp);
                        }

                    },
                    error: function (dataS) {
                        console.log('dataS', dataS);
                        pos_error({ msg: "Could not get customers list" });
                    },
                    complete: (dataS) => {
                        document.getElementById('loading_jp').style.display = "none";
                    }
                });

            }

        }
    }
);

