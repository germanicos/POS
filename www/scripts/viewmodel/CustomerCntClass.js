define(['jquery', 'knockout', 'base'], function ($, ko) {
	CustomerCntClass = SimpleControl.extend({
		init: function (DsRegistry) {

			console.log("CustomerCntClass init");
			//console.log( "COUNTRIESDS: " + JSON.stringify(dsRegistry.getDatasource('countriesDS').getStore())  );

			var self = this;
			this._super(DsRegistry);
			this.systemMode = "";
			this.customers = ko.observableArray();
			this.customer_first_name = ko.observable('');
			this.customer_last_name = ko.observable('');
			this.customer_occupation = ko.observable('');
			this.customer_company_name = ko.observable('');
			this.customer_address1 = ko.observable('');
			this.customer_address2 = ko.observable('');
			this.customer_country = ko.observable('');
			this.customer_state = ko.observable('');
			this.customer_city = ko.observable('');
			this.customer_postal_code = ko.observable('');
			this.customer_mobile_phone = ko.observable('');
			this.customer_landline_phone = ko.observable('');
			this.customer_email = ko.observable('');

			this.countries = ko.observableArray(dsRegistry.getDatasource('countriesDS').getStore());
			this.states = ko.observableArray(dsRegistry.getDatasource('statesDS').getStore());
			this.cities = ko.observableArray(dsRegistry.getDatasource('citiesDS').getStore());

			// having trouble with the ko.computed version of this variables
			this.states_display = ko.observableArray(dsRegistry.getDatasource('statesDS').getStore());
			this.cities_display = ko.observableArray(dsRegistry.getDatasource('citiesDS').getStore());

			this.DOB_day = ko.observable('');
			this.DOB_month = ko.observable('');
			this.DOB_year = ko.observable('');
			this.customer_DOB = ko.computed({
				read: function () {
					return this.DOB_year() + "-" + this.DOB_month() + "-" + this.DOB_day();
				},
				write: function (val) {
					if (val) {
						DOB = (val === '') ? ['', '', ''] : val.split('-');
						this.DOB_day(DOB[2][0] == '0' ? DOB[2][1] : DOB[2]);
						$('select[name="DOB_day"]').selectmenu('refresh');
						this.DOB_month(DOB[1][0] == '0' ? DOB[1][1] : DOB[1]);
						$('select[name="DOB_month"]').selectmenu('refresh');
						this.DOB_year(DOB[0]);
						$('select[name="DOB_year"]').selectmenu('refresh');
					}
				},
				owner: this
			});

			this.customer_gender = ko.observable('1'); // default man.
			this.customer_email_subscription = ko.observable(true);

			this.referal_methods_list = ko.observableArray([{ "name": "Google Search", "id": "1" }, { "name": "Bing Search", "id": "8" }, { "name": "Yahoo Search", "id": "9" }, { "name": "Print Advertisement", "id": "2" }, { "name": "TV Ad", "id": "3" }, { "name": "Radio Ad", "id": "4" }, { "name": "Billboard", "id": "5" }, { "name": "Other", "id": "6" }, { "name": "Referral", "id": "7" }]);
			//this.referal_methods_list		 = ko.observableArray(["Google Search", "Referral", "Print Advertisement", "TV Ad", "Radio Ad", "Billboard", "Other"]);
			this.customer_referal_method = ko.observable('');
			this.customer_referal_moreinfo = ko.observable('');
			this.customer_refered_by = ko.observable('');
			this.customer_other_way = ko.observable('');

			this.selectedId = 0;
			this.selected_item = ko.observable({});
			this.selected_item.subscribe(function (data) {
				//posChangePage('#customerInfo');
				/*
				if (this.systemMode == "order"){
					;
				}else{
					posChangePage('#customerInfo');
				}
				*/
				if (data == null || data == undefined) {
					self.selected_item(self.selected_item_old);
				}
			});
			this.selected_item.subscribe(function (oldValue) {
				self.selected_item_old = oldValue;
			}, null, "beforeChange");
			this.selectedOrderId = 0;
			this.selected_order = ko.observable({});
			this.selected_order.subscribe(function (data) {
				//posChangePage('#customerInfo');
				/*
				if (this.systemMode == "order"){
					;
				}else{
					posChangePage('#customerInfo');
				}
				*/
			});
			this.selectedOrderGarmentId = 0;
			this.selected_ordergarment = ko.observable({});


			this.customer_display_country = ko.computed(function () {
				for (var i = 0; i < this.countries().length; i++) {
					if (this.countries()[i].locations_id === this.selected_item().customer_country)
						return this.countries()[i].location_name;
				}
			}, this);

			this.customer_display_state = ko.computed(function () {
				for (var i = 0; i < this.states().length; i++) {
					if (this.states()[i].locations_id === this.selected_item().customer_state)
						return this.states()[i].location_name;
				}
			}, this);

			this.customer_display_city = ko.computed(function () {
				for (var i = 0; i < this.cities().length; i++) {
					if (this.cities()[i].locations_id === this.selected_item().customer_city)
						return this.cities()[i].location_name;
				}
			}, this);

			this.months = ko.observableArray([{ "name": "January", "id": "1" }, { "name": "February", "id": "2" }, { "name": "March", "id": "3" }, { "name": "April", "id": "4" }, { "name": "May", "id": "5" }, { "name": "June", "id": "6" }, { "name": "July", "id": "7" }, { "name": "August", "id": "8" }, { "name": "September", "id": "9" }, { "name": "October", "id": "10" }, { "name": "November", "id": "11" }, { "name": "December", "id": "12" }]);
			this.days = ko.observableArray([]);
			for (i = 1; i <= 31; i++) this.days.push(i.toString());
			this.years = ko.observableArray([]);
			for (i = 1930; i <= 2013; i++) this.years.push(i.toString());


			this.componentMode = ko.observable(false);
			this.callback = "";


			this.validation = ko.computed(function () {
				return {
					"First name": (this.customer_first_name() == "") ? false : true,
					"Last name": (this.customer_last_name() == "") ? false : true,
					"Date of birth": (this.customer_DOB() == '' || this.customer_DOB() == '--' || this.customer_DOB().indexOf("undefined") != -1) ? false : true,
					// "Gender": (this.customer_gender() == '' || this.customer_gender() == null || this.customer_gender() == undefined) ? false : true,
					"Country": (this.customer_country() == "" || this.customer_country() == undefined) ? false : true,
					"State": (this.customer_state() == '' || this.customer_state() == undefined) ? false : true,
					"City": (this.customer_city() == '' || this.customer_city() == undefined) ? false : true,
					"Street Address": (this.customer_address1() == "") ? false : true,
					"Suburb": (this.customer_address2() == "") ? false : true,
					"Postal Code": (this.customer_postal_code() == '') ? false : true,
					"Mobile phone": (this.customer_mobile_phone() == "") ? false : true,
					"E-mail": (this.customer_email() == "" || this.validateEmail(this.customer_email()) == false) ? false : true,
					"Referral method": (this.customer_referal_method() == '' || this.customer_referal_method() == undefined) ? false : true,
					"Who referred you": (this.customer_referal_method() == '7' && (this.customer_refered_by() == '' || this.customer_refered_by() == undefined || this.customer_refered_by() == null)) ? false : true,
					"Other way of reference": (this.customer_referal_method() == '6' && (this.customer_other_way() == '' || this.customer_other_way() == undefined || this.customer_other_way() == null)) ? false : true,
					"Search query more info": ((this.customer_referal_method() == '1' || this.customer_referal_method() == '8' || this.customer_referal_method() == '9') && (this.customer_referal_moreinfo() == '' || this.customer_referal_moreinfo() == undefined || this.customer_referal_moreinfo() == null)) ? false : true,


				};
			}, this);

			// search criteria for customer's

			// customer's name country and state filters
			this.customer_name_search = ko.observable('');

			this.customer_country_search = ko.observable('');

			this.customer_state_search = ko.observable('');

			//// customer's state end

			this.customer_search_list = ko.computed(function () {
				var filter_result_array = [];
				var name_filter = self.customer_name_search();
				var country_filter = self.customer_country_search();
				var state_filter = self.customer_state_search();
				if (name_filter.length !== 0) {
					for (var ind in self.customers()) {
						var cfull = self.customers()[ind].customer_last_name + ' ' + self.customers()[ind].customer_first_name;
						cfull = cfull.toLowerCase();

						if (cfull.indexOf(name_filter.toLowerCase()) !== -1) {
							var custTemp = self.customers()[ind];
						}
					}
				}
				// if no filter applies
				else {
					filter_result_array = self.customers();
				}

				return filter_result_array;
			}, this);

			///// search criteria for customers end

		},

		testFunction: function () {
			console.log("orderItem.custOrdersVM: " + JSON.stringify(orderItem.custOrdersVM.orders));
		},

		validateEmail: function (email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var result = re.test(email);
			console.log("validateEmail: " + result);
			return result;
		},

		gotoUpdate: function () {
			if (this.systemMode == "order")
				posChangePage('#orderItemUpdateCustomer')
			else
				posChangePage('#customerEditCustomer'); // originally customerEditCustomer, BUT LOOKS LIKE SHIT, MUST MAKE A MODIFIED VERSION OF orderItemUpdateCustomer
		},

		gotoGarments: function () {
			posChangePage('#orderItemSelectGarment');
		},


		cancelCustomer: function () {
			if (this.componentMode()) {
				posChangePage(this.callback);
				this.componentMode(false);
			} else {
				posChangePage('#listCustomer');
			}

		},
		addCustomer: function () {
			console.log("addCustomer this.systemMode: " + this.systemMode);
			var validate = this.validation();
			var msg = "Please fill in the following fields:\n";
			var invalid_fields = [];
			var valid = true;


			/**
			 * Bugfix => the customer gender checkbox is not working for
			 * some devices, lets get the gender using jquery;
			 */
			if (this.customer_gender() == "") {
				customAlert("Please select customer gender");
				return;
			}







			for (key in validate) {
				if (validate.hasOwnProperty(key)) {
					if (validate[key] == false) {
						invalid_fields.push(key);
						valid = false;
					}
				}
			}

			if (valid == false) {
				customAlert(msg + invalid_fields.join(', '));
				return;
			}



			var newCustomer = {
				"customer_first_name": this.customer_first_name().charAt(0).toUpperCase() + this.customer_first_name().slice(1),
				"customer_last_name": this.customer_last_name().charAt(0).toUpperCase() + this.customer_last_name().slice(1),
				"customer_occupation": this.customer_occupation(),
				"customer_company_name": this.customer_company_name(),
				"customer_address1": this.customer_address1(),
				"customer_address2": this.customer_address2(),
				"customer_country": this.customer_country(),
				"customer_state": this.customer_state(),
				"customer_city": this.customer_city(),
				"customer_postal_code": this.customer_postal_code(),
				"customer_mobile_phone": this.customer_mobile_phone(),
				"customer_landline_phone": this.customer_landline_phone(),
				"customer_email": this.customer_email(),
				//	"customer_refered_by": this.customer_refered_by(),
				"customer_DOB": this.customer_DOB(),
				"customer_gender": this.customer_gender(),
				"customer_email_subscription": (this.customer_email_subscription() ? 1 : 0),
				"customer_referal_method": this.customer_referal_method(),
				"customer_refered_by": this.customer_refered_by(),
				"customer_other_way": this.customer_other_way(),
				"customer_referal_moreinfo": this.customer_referal_moreinfo()
			};



			var ds = this.dsRegistry.getDatasource(this.subscribed).upsert(newCustomer, true);
			//		if (this.systemMode == "order") {
			orderItem.newcustomerorder = true;
			custData.sync();
			var new_cust_id = this.dsRegistry.getDatasource(this.subscribed).index - 1;
			var new_cust = this.dsRegistry.getDatasource(this.subscribed).store[new_cust_id];
			new_cust = this.dsRegistry.getDatasource(this.subscribed).store[new_cust_id];
			orderItem.custSelectVM.selectedCustomer(new_cust);
			customersVM.selected_item(new_cust);
			orderItem.customersVMmirror.selected_item(new_cust);
			//posChangePage('#orderItemCustomerInfo');
			//		}else{
			//			posChangePage('#measurements');
			//		}

			$.jGrowl("Succesfully created customer " + this.customer_first_name() + " " + this.customer_last_name(), { header: 'Update' });
			//this.setObservables('');
		},

		updateCustomer: function () {
			var validate = this.validation();
			var msg = "Please fill in the following fields:\n";
			var invalid_fields = [];
			var valid = true;
			for (key in validate) {
				if (validate.hasOwnProperty(key)) {
					if (validate[key] == false) {
						invalid_fields.push(key);
						valid = false;
					}
				}
			}
			if (valid == false) {
				customAlert(msg + invalid_fields.join(', '));
				return;
			}

			var item = {
				"customer_first_name": this.customer_first_name(),
				"customer_last_name": this.customer_last_name(),
				"customer_occupation": this.customer_occupation(),
				"customer_company_name": this.customer_company_name(),
				"customer_address1": this.customer_address1(),
				"customer_address2": this.customer_address2(),
				"customer_country": this.customer_country(),
				"customer_state": this.customer_state(),
				"customer_city": this.customer_city(),
				"customer_postal_code": this.customer_postal_code(),
				"customer_mobile_phone": this.customer_mobile_phone(),
				"customer_landline_phone": this.customer_landline_phone(),
				"customer_email": this.customer_email(),
				"customer_refered_by": this.customer_refered_by(),
				"customer_DOB": this.customer_DOB(),
				"customer_gender": this.customer_gender(),
				"customer_email_subscription": (this.customer_email_subscription() ? 1 : 0),
				"id": this.selected_item().id,
				"customer_referal_method": this.customer_referal_method(),
				"customer_referal_moreinfo": this.customer_referal_moreinfo(),
				"customer_other_way": this.customer_other_way()
			};
			if (this.selected_item().server_id != undefined) item.server_id = this.selected_item().server_id; {
				this.selected_item(item);
			}

			var ds = this.dsRegistry.getDatasource(this.subscribed).upsert(item, true);
			if (this.systemMode == "order") {
				custData.sync();
				posChangePage('#orderItemCustomerInfo');

				orderItem.custSelectVM.selectedCustomer().customer_first_name = this.customer_first_name();
				orderItem.custSelectVM.selectedCustomer().customer_last_name = this.customer_last_name();
				orderItem.custSelectVM.selectedCustomer().customer_occupation = this.customer_occupation();
				orderItem.custSelectVM.selectedCustomer().customer_company_name = this.customer_company_name();
				orderItem.custSelectVM.selectedCustomer().customer_address1 = this.customer_address1();
				orderItem.custSelectVM.selectedCustomer().customer_address2 = this.customer_address2();
				orderItem.custSelectVM.selectedCustomer().customer_country = this.customer_country();
				orderItem.custSelectVM.selectedCustomer().customer_state = this.customer_state();
				orderItem.custSelectVM.selectedCustomer().customer_city = this.customer_city();
				orderItem.custSelectVM.selectedCustomer().customer_postal_code = this.customer_postal_code();
				orderItem.custSelectVM.selectedCustomer().customer_mobile_phone = this.customer_mobile_phone();
				orderItem.custSelectVM.selectedCustomer().customer_landline_phone = this.customer_landline_phone();
				orderItem.custSelectVM.selectedCustomer().customer_email = this.customer_email();
				orderItem.custSelectVM.selectedCustomer().customer_refered_by = this.customer_refered_by();
				orderItem.custSelectVM.selectedCustomer().customer_DOB = this.customer_DOB();
				orderItem.custSelectVM.selectedCustomer().customer_gender = this.customer_gender();
				orderItem.custSelectVM.selectedCustomer().customer_email_subscription = this.customer_email_subscription();
				orderItem.custSelectVM.selectedCustomer().customer_referal_method = this.customer_referal_method();
				orderItem.custSelectVM.selectedCustomer().customer_referal_moreinfo = this.customer_referal_moreinfo();
				orderItem.custSelectVM.selectedCustomer().customer_other_way = this.customer_other_way();

			} else {
				custData.sync();
				posChangePage('#customerInfo');
			}
			$.jGrowl("Succesfully updated customer " + item.customer_first_name + " " + item.customer_last_name, {
				header: 'Update'
			});
		},




		removeCustomer: function () {
			var item = this.selected_item();
			var ds = this.dsRegistry.getDatasource(this.subscribed).deleteItem(item.id, true);
			customersVM.systemMode = "customer";
			posChangePage('#addCustomer');
			$.jGrowl("Succesfully removed customer " + item.customer_first_name + " " + item.customer_last_name, {
				header: 'Removal'
			});
		},
		selectCustomer: function (id, callback) {
			for (var ind in this.customers()) {
				if (this.customers()[ind].id === id) {
					this.selected_item(this.customers()[ind]);
					this.callback = callback;
					break;
				}
			}
			this.componentMode(true);
		},
		cancelUpdate: function () {
			console.log("cancelUpdate");
			if (this.systemMode == "order")
				posChangePage('#orderItemCustomerInfo');
			else
				posChangePage('#customerInfo');
		},
		cancelAdd: function () {
			console.log("this.systemMode: " + this.systemMode);
			if (this.systemMode == "order") {
				orderItem.currentBigStep(orderItem.bigSteps()[0]);
			} else if (this.systemMode == "shipping_fabrics" || this.systemMode == "shipping_copy_garment" || this.systemMode == "shipping_other") {
				posChangePage('#shipping_select_customer');
			} else {
				posChangePage('#listCustomer');
			}
		},
		digestData: function (data) {
			var newObserve = [];
			for (var ind in data) {
				if (data[ind] != null) {
					if (!data[ind].isDead) {
						newObserve.push(data[ind]);
					}
				}
			}
			this.customers(newObserve);
		},
		setObservables: function (val) {
			if (typeof (val) == "object") {
				console.log('case 1');
				if (val == {}) {
					$.jGrowl('Unable to load selected customer!');
					return;
				}
				this.customer_first_name(val.customer_first_name);
				this.customer_last_name(val.customer_last_name);
				this.customer_occupation(val.customer_occupation);
				this.customer_company_name(val.customer_company_name);
				this.customer_address1(val.customer_address1);
				this.customer_address2(val.customer_address2);
				this.customer_country(val.customer_country);
				this.customer_state(val.customer_state);
				this.customer_city(val.customer_city);
				this.customer_postal_code(val.customer_postal_code);
				this.customer_mobile_phone(val.customer_mobile_phone);
				this.customer_landline_phone(val.customer_landline_phone);
				this.customer_email(val.customer_email);
				//this.customer_refered_by(val.customer_refered_by);
				this.customer_DOB(val.customer_DOB);
				this.customer_gender(val.customer_gender);
				//this.customer_email_subscription(val.customer_email_subscription);
				if (val.customer_email_subscription == "1" || val.customer_email_subscription === true) {
					this.customer_email_subscription(true);
				} else {
					this.customer_email_subscription(false);
				}

				this.customer_referal_method(val.customer_referal_method);
				this.customer_refered_by(val.customer_refered_by);
				this.customer_other_way(val.customer_other_way);
				this.customer_referal_moreinfo(val.customer_referal_moreinfo);

				//			this.DOB_day(val.DOB_day);
				//			this.DOB_month(val.DOB_month);
				//			this.DOB_year(val.DOB_year);

			} else {
				console.log('case 2');
				this.customer_first_name(val);
				this.customer_last_name(val);
				this.customer_occupation(val);
				this.customer_company_name(val);
				this.customer_address1(val);
				this.customer_address2(val);
				//	this.customer_country(val);
				//	this.customer_state(val);
				//	this.customer_city(val);
				this.customer_country(null);
				this.customer_state(null);
				this.customer_city(null);
				this.customer_postal_code(val);
				this.customer_mobile_phone(val);
				this.customer_landline_phone(val);
				this.customer_email(val);
				//this.customer_refered_by(val);
				this.customer_DOB(val);
				this.customer_gender(val);
				this.customer_email_subscription(true);
				//this.customer_referal_method(val);
				this.customer_referal_method(null);
				this.customer_refered_by(val);
				this.customer_other_way(val);
				this.customer_referal_moreinfo(val);
				this.DOB_day(null);
				this.DOB_month(null);
				this.DOB_year(null);

			}
			$("input[type=radio]").checkboxradio("refresh");
			// $("input[type=checkbox]").checkboxradio("refresh");
			$("select").selectmenu("refresh");
		}
	});

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	CustomerSelectClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			this.customers = ko.observableArray();
			var self = this;
			this.customersAutocompleteList = ko.observableArray();	// subset of customers
			this.customerAutocomplete = ko.observable();		// text input
			this.selectedCustomer = ko.observable({});
			this.selectedCustomerFromList = ko.observable({});

			this.customerAutocomplete.subscribe(function (data) {
				self.populateSuggestions(data);
			});

			this.selectedCustomer.subscribe(function (oldValue) {
				self.selectedCustomer_old = oldValue;
			}, null, "beforeChange");

			this.selectedCustomer.subscribe(function (data) {
				if (self.parentViewModel !== null && data != undefined && data != "undefined") {
					self.parentViewModel.setSelectedCustomer(data);
				}
				else {
					self.selectedCustomer(self.selectedCustomer_old);
				}

			});

			this.selectedCustomerFromList.subscribe(function (data) {
				if (self.parentViewModel !== null) {
					self.parentViewModel.setSelectedCustomerFromList(data);
				}

			});

		},

		addNewCustomer: function (data) {
			posChangePage('#orderItemAddCustomer');
		},

		digestData: function (data) {


			console.log("digesting data customer", data);

			var newObserve = [];
			//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
			for (var ind in data) {
				if (data[ind] != null) {
					if (!data[ind].isDead) {
						newObserve.push(data[ind]);
					}
				}
			}
			this.customers(newObserve);
		},

		populateSuggestions(search){
			this.customersAutocompleteList.removeAll();

				if (search.length > 2) {

					console.log("Searching for customer2... ", this.customers());


					if (search.length !== 0) {
						for (var ind in this.customers()) {
							var cfull = this.customers()[ind].customer_last_name + ' ' + this.customers()[ind].customer_first_name + ' ' + this.customers()[ind].customer_email;

							cfull = cfull.toLowerCase();

							//if (cfull.indexOf(data.toLowerCase()) !== -1)
							if (cfull.includes(search.toLowerCase())) {
								var custTemp = this.customers()[ind];
								custTemp.customer_display_state = '';
								var statesTemp = orderItem.dsRegistry.getDatasource('statesDS').store;
								for (var j in statesTemp) {
									if (statesTemp[j].locations_id == custTemp.customer_state) {
										custTemp.customer_display_state = statesTemp[j].location_name;
									}
								}
								this.customersAutocompleteList.push(custTemp);
							}
						}
					}
				}
		}
	});

	/////////////////////////////////////////////////

	CustomerOrdersClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var self = this;
			this.orders = ko.observableArray();
			this.selectedOrder = ko.observable({});

			this.selectedOrder.subscribe(function (data) {
				if (self.parentViewModel !== null) {
					self.parentViewModel.setSelectedOrder(data);
				} else {
					console.log("self.parentViewModel == null");
				}
			});

			this.transmuteDate = function (date) {  // input is yyyy-mm-dd
				if (date != undefined && date != null) {

					var ymd = date.substring(0, date.lastIndexOf(" "));
					var time = date.substring(date.lastIndexOf(" ") + 1);
					var hm = time.substring(0, time.lastIndexOf(":"));

					var day = ymd.substring(ymd.lastIndexOf("-") + 1);
					var month = ymd.substring(ymd.indexOf("-") + 1, ymd.lastIndexOf("-"));
					var year = ymd.substring(0, ymd.indexOf("-"));
					return day + "-" + month + "-" + year + " " + hm;
				} else {
					return 0;
				}
			};

		},

		digestData: function (data) {
			var newObserve = [];
			//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
			for (var ind in data) {
				if (data[ind] != null) {
					if (!data[ind].isDead) {
						if (ind < 10) {
							data[ind].garments = '';
							for (x in data[ind].garments_in_order) {
								var comp = ', '
								if (data[ind].garments == '') {
									comp = '';
								}
								data[ind].garments = data[ind].garments + comp + data[ind].garments_in_order[x].garment_name;
							}
							newObserve.push(data[ind]);
						}
					}
				}
			}
			this.orders(newObserve);
			//console.log(JSON.stringify(this.orders()));
		}
	});

	/////////////////////////////////////////////////

	CustomerOrderClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var self = this;
			this.ordergarments = ko.observableArray();
			this.selectedOrderGarment = ko.observable({});
			this.ordernotes = ko.observable('');

			this.selectedOrderGarment.subscribe(function (data) {
				//	console.log("selectedOrderGarment  " + JSON.stringify(data));	// OK
				if (self.parentViewModel !== null) {
					self.parentViewModel.setSelectedOrderGarment(data);
				} else {
					console.log("self.parentViewModel == null");
				}
			});

		},

		digestData: function (data0) {
			var newObserve = [];
			var data = data0[0];
			//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
			for (var ind in data) {
				if (data[ind] != null && JSON.stringify(data[ind]).length > 10) {
					if (!data[ind].isDead) {
						newObserve.push(data[ind]);
					}
				}
			}
			this.ordergarments(newObserve);
		}
	});

	/////////////////////////////////////////////////

	CustomerOrderPaymentsClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var self = this;
			this.orderpayments = ko.observableArray();

			this.day = ko.observable('');
			this.month = ko.observable('');
			this.amount = ko.observable(0);
			this.payment_method = ko.observable(0);
			this.referer = ko.observable('');


			this.creditCardUrlToPopUp = ko.observable("");

			this.ammountRemaining = function () {
				var paymentsammount = 0;
				console.log(JSON.stringify(this.orderpayments()));
				for (var x = 0; x < this.orderpayments().length; x++) {
					paymentsammount += this.orderpayments()[x].deposit * 1.0;
				}
				console.log("paymentsammount: " + paymentsammount);
				var total = 0;

				//console.log("orderItem.customersVMmirror.selected_order().total_amount: " + orderItem.customersVMmirror.selected_order().total_amount);

				//console.log("orderItem.customersVMmirror.selected_order.GST: " + orderItem.customersVMmirror.selected_order().GST);
				total = (orderItem.customersVMmirror.selected_order().total_amount * 1.0 + orderItem.customersVMmirror.selected_order().total_amount * orderItem.customersVMmirror.selected_order().GST / 100).toFixed(2);
				console.log("total: " + total);

				var remaining = total - paymentsammount;
				console.log("remaining: " + remaining);
				return remaining.toFixed(2);
			};

			this.transmuteDate = function (date) {  // input is yyyy-mm-dd
				if (date != undefined && date != null) {
					var day = date.substring(date.lastIndexOf("-") + 1);
					var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
					var year = date.substring(0, date.indexOf("-"));
					return day + "-" + month + "-" + year;
				} else {
					return 0;
				}
			};



		},

		digestData: function (data0) {
			var newObserve = [];
			var data = data0[0];
			//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
			for (var ind in data) {
				if (data[ind] != null && JSON.stringify(data[ind]).length > 10) {
					if (!data[ind].isDead) {

						// fix cc_img_path (for some reason, on ERP some images had path broken ('//' instead '/'))
						// also insert BUrl
						if(data[ind].cc_img_path) // cc_img_path could be NULL, we need this checking before accessing it
						{
							data[ind].cc_img_path = data[ind].cc_img_path.replace('//',"/");
						}


						newObserve.push(data[ind]);
					}
				}
			}
			this.orderpayments(newObserve);
		},

		/**
		 * Only show upload image if payment method is one of above:
		 * -> Credit card V/M
		 * -> Credit card Amex
		 * -> Eftpos
		 */
		mustShowUploadImage(payment){
			const payment_method  = payment.payment_method;
			if (['Credit card V/M','Credit card Amex','Eftpos'].includes(payment_method)){
				return true;
			}
			return false;

		},

		/**
		 * Configure click event on "upload image button".
		 * This method show a thumbnail image and send the image to ERP
		 *
		 * This method is called on router, after the page be rendered
		 */
		configureEventListeners(){
			var this_CustomerOrderPaymentsClass = this;

			const readURLAndUploadImage = function (input, payment) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$(`#imageCreditCard-${payment.payment_id}`).attr('src', e.target.result);
						payment.cc_img_path = e.target.result;
						this_CustomerOrderPaymentsClass.uploadImage(payment);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}

			// set event for all inputs
			for (const payment of this.orderpayments()) {
				const payment_id = payment.payment_id;


				$(`#inputCreditCard-${payment_id}`).off("change"); // remove other "change" events
				$(`#inputCreditCard-${payment_id}`).on('change', function (event) {
					readURLAndUploadImage(this, payment);
				});

			}


		},

		uploadImage(payment){
			const order_id        = payment.order_id;
			const payment_id      = payment.payment_id;
			// const payment_method  = payment.payment_method;
			const creditCardImage = payment.cc_img_path;

			// console.log('order_id', order_id );
			// console.log('payment_id', payment_id );
			// console.log('payment_method', payment_method );
			// console.log('creditCardImage', creditCardImage );

			var image = creditCardImage.replace("data:image/jpeg;base64,", "").replace("data:image/png;base64,", "");
			var salesmanString = "{username:'" + authCtrl.username() + "',password:'" + authCtrl.password() + "', id:'" + authCtrl.userInfo.user_id + "'}";

			request = $.ajax({
				type: 'POST',
				url:  BUrl + 'client_image_upload/upload_creditcard_image',
				dataType: 'json',
				async: true,
				data: {
					salesman: JSON.stringify(eval("(" + salesmanString + ")")),
					data: image
				},
				success: (dataS) => {
					console.log('uploadImage dataS', dataS);

					// now we have to link this image with order
					this.linkUploadedImageWithPaymentHistory(dataS.id, order_id, payment_id);
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					customAlert("some error " + textStatus + "    " + errorThrown);
				}
			});



		},

		linkUploadedImageWithPaymentHistory(image_id, order_id, payment_id) {
			var salesmanString = "{username:'" + authCtrl.username() + "',password:'" + authCtrl.password() + "', id:'" + authCtrl.userInfo.user_id + "'}";
			$.ajax({
				type: 'POST',
				url: BUrl + 'client_image_upload/link_uploaded_image_with_payment_history',
				dataType: 'json',
				async: true,
				data: {
					salesman : JSON.stringify(eval("(" + salesmanString + ")")),
					data     : {
						image_id   : image_id,
						order_id   : order_id,
						payment_id : payment_id
					}
				},
				success: function (dataS) {
					console.log('linkUploadedImageWithPaymentHistory dataS', dataS);

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log("some error " + textStatus + "    " + errorThrown);
					customAlert("Error on link Uploaded Image With PaymentHistory");
				}
			});
		},

		showImagePopUp(payment){
			// set image to show on popup
			const image_src = document.getElementById(`imageCreditCard-${payment.payment_id}`).src
			this.creditCardUrlToPopUp(image_src);

			// show pop-up
			$('#cc_image-popup').show(500);
		}

	});






	OrderPaymentClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var self = this;

			this.payment_ammount = ko.observable('');
			this.payment_notes = ko.observable('');
			this.paymentMethodList = ko.observableArray(["Credit card V/M", "Credit card Amex", "Bank transfer", "Eftpos", "Cheque", "Cash"]);
			this.paymentMethod = ko.observable('');
			this.payment_day = ko.observable('');
			this.PaymentDate = ko.observable('');
			this.payment_month = ko.observable('');
			this.creditCardImage = ko.observable('');

			this.send_invoice_email_checkbox = ko.observable(false);

			this.transmuteDate = function (date) {  // input is yyyy-mm-dd
				if (date != undefined && date != null) {
					var ymd = date.substring(0, date.lastIndexOf(" "));
					var time = date.substring(date.lastIndexOf(" ") + 1);

					var day = ymd.substring(ymd.lastIndexOf("-") + 1);
					var month = ymd.substring(ymd.indexOf("-") + 1, ymd.lastIndexOf("-"));
					var year = ymd.substring(0, ymd.indexOf("-"));
					return day + "-" + month + "-" + year + " " + time;
				} else {
					return 0;
				}
			};

			this.sync = function () {
				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";
				var paymentOK = false;
				if (this.paymentMethod() != '' && this.paymentMethod() != undefined) {
					if (this.paymentMethod() != 'Credit card' && this.paymentMethod() != 'Eftpos') {
						paymentOK = true;
					} else {
						if (this.creditCardImage() != '' && this.creditCardImage() != undefined) {
							paymentOK = true;
						}
					}
				}
				if (this.payment_ammount() == 0 || this.payment_ammount() == '' || this.paymentMethod() == '' || this.paymentMethod() == undefined || paymentOK == false || this.PaymentDate() == '') {

					customAlert("Please fill in all fields!");
				} else {
					var salesmanString = "{username:'" + authCtrl.username() + "',password:'" + authCtrl.password() + "', id:'" + authCtrl.userInfo.user_id + "'}";
					var currency = orderItem.getSellerCurrency();

					var dateToPost = '';
					var date = new Date();
					var yearnow = date.getFullYear();
					var monthnow = date.getMonth() + 1;
					var daynow = date.getDate();
					dateToPost = this.PaymentDate();
					data0 = {
						deposit: this.payment_ammount(),
						notes: this.payment_notes(),
						payment_method: this.paymentMethod(),
						transaction_date: dateToPost,
						currency_id: currency.currency_id,
						order_id: orderItem.custOrdersVM.selectedOrder().order_id

					};

					var creditcardID = "";
					if (this.paymentMethod() == 'Credit card' || this.paymentMethod() == 'Eftpos' || this.paymentMethod() == 'Credit card V/M' || this.paymentMethod() == 'Credit card Amex') {
						try {
							var creditcardphotos = localStorage.getItem('PaymentCreditCardPhoto');
							creditcardphotos = (creditcardphotos == null) ? [] : JSON.parse(creditcardphotos);
							if (localStorage.getItem('PaymentCreditCardPhoto') != null) {
								if (localStorage.getItem('PaymentCreditCardPhoto').length > 0) {
									creditcardID = creditcardphotos[0].image_id;
								} else {
									console.log("error 1");
								}
							} else {
								console.log("error 2");
							}
						} catch (e) { console.log(e); }
					}

					data1 = {
						data: JSON.stringify(data0),
						salesman: JSON.stringify(eval("(" + salesmanString + ")")),
						customer_credit_card: creditcardID,
						send_invoice_email: this.send_invoice_email_checkbox()
					};
					request = $.ajax({
						type: 'POST',
						url: BUrl + 'client/make_payment_endpoint',
						dataType: 'json',
						timeout: 20000,
						data: data1,
						error: function (dataS) {
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "none";
						},
						success: function (dataS) {
							dataS = JSON.parse(JSON.stringify(dataS));
							if (dataS.valid == '1') {
								posChangePage('#paymentsHistory');
								$.jGrowl('Payment has been saved!');


							} else {
								console.log('fail');
								$.jGrowl(dataS.msg);
							}

						}
					});
				}
				spinner.style.display = "none";
			}

		}

	});




	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	OrderFittingsClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var self = this;
			this.fittings = ko.observableArray();
			this.selectedFitting = ko.observable({});

			this.selectedFitting.subscribe(function (data) {
				if (self.parentViewModel !== null) {
					self.parentViewModel.setSelectedFitting(data);
				} else {
					console.log("self.parentViewModel == null");
				}
			});

			this.transmuteDate = function (date) {  // input is yyyy-mm-dd
				if (date != undefined && date != null) {
					var ymd = date.substring(0, date.lastIndexOf(" "));
					var time = date.substring(date.lastIndexOf(" ") + 1);
					var hm = time.substring(0, time.lastIndexOf(":"));
					var day = ymd.substring(ymd.lastIndexOf("-") + 1);
					var month = ymd.substring(ymd.indexOf("-") + 1, ymd.lastIndexOf("-"));
					var year = ymd.substring(0, ymd.indexOf("-"));
					return day + "-" + month + "-" + year + " " + hm;
				} else {
					return 0;
				}
			};

			this.loadFittingAndChangePage = function (data) {
				orderItem.FittingVM.fitting_id(data.fitting.fitting_id);
				var tailors = ko.observable(dsRegistry.getDatasource('tailorsDS').getStore());
				for (i in tailors()) {
					if (tailors()[i].user_id == data.tailor.tailor_id) {
						orderItem.FittingVM.tailor(tailors()[i]);
					}
				}
				orderItem.FittingVM.tailornote(data.tailor.tailor_notes);
				var date = data.tailor.delivery_date;
				var day = (date.substring(date.lastIndexOf("-") + 1)) * 1.0;
				var month = (date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"))) * 1.0;
				orderItem.FittingVM.tailor_day(day);
				orderItem.FittingVM.tailor_month(month);

				for (var x = 0; x < data.videos.length; x++) {
					orderItem.FittingVM.fittingvideos.push(new orderItem.FittingVM.FittingVideoViewModel(data.videos[x].video_path, data.videos[x].video_notes));
				}
				// SAVING LOCALSTORAGE TAGS
				thetags = localStorage.getItem('fittingtags');
				if (thetags == null) {
					thetags = [];
				}
				tmp = [];
				for (var x = 0; x < thetags.length; x++) {
					tmp.push({ image_id: thetags[x].image_id, left: thetags[x].left, top: thetags[x].top, text: thetags[x].text });
				}
				try {
					for (var x = 0; x < data.images.length; x++) {
						var fitting_image_tags = JSON.parse(data.images[x].fitting_image_tags);
						for (var y = 0; y < fitting_image_tags.length; y++) {
							tmp.push({ image_id: data.images[x].image_id, left: fitting_image_tags[y].left, top: fitting_image_tags[y].top, text: fitting_image_tags[y].text });
						}
					}
				} catch (e) { ; }
				localStorage.removeItem('fittingtags');
				localStorage.setItem('fittingtags', JSON.stringify(tmp));
				var thephotos = localStorage.getItem('fittingphotos');
				if (thephotos == null) {
					thephotos = [];
				}
				var tmp = [];
				for (var x = 0; x < thephotos.length; x++) {
					tmp.push({ customer_id: thephotos[x].customer_id, image_id: thephotos[x].image_id, picture: thephotos[x].picture, thumbnail: thephotos[x].thumbnail });
				}
				for (var x = 0; x < data.images.length; x++) {
					tmp.push({ customer_id: orderItem.custSelectVM.selectedCustomer().server_id, image_id: data.images[x].image_id, picture: data.images[x].fitting_image_name, thumbnail: data.images[x].fitting_image_path });
				}
				localStorage.removeItem('fittingphotos');
				localStorage.setItem('fittingphotos', JSON.stringify(tmp));
				posChangePage('#fittingImages');
			};
		},

		digestData: function (data0) {
			var newObserve = [];
			var spinner = document.getElementById('loading_jp');
			spinner.style.display = "block";
			var data = data0;
			//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
			for (var ind in data) {
				if (data[ind] != null && JSON.stringify(data[ind]).length > 10) {
					if (!data[ind].isDead) {
						newObserve.push(data[ind]);
					}
				}
			}
			this.fittings(newObserve);
			spinner.style.display = "none";
		}

	});


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	FittingClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var self = this;

			this.fitting_id = ko.observable('');

			this.tailors = ko.observable(dsRegistry.getDatasource('tailorsDS').getStore());//JSON.parse(localStorage.getItem('tailorsDS'));
			this.tailor = ko.observable();
			this.tailornote = ko.observable('');
			this.tailor_day = ko.observable('');
			this.tailor_month = ko.observable('');

			this.tailor_date = ko.computed({
				read: function () {
					return this.tailor_month() + "-" + this.tailor_day();
				},
				write: function (date) {
					if (date) {
						date = date.split('-');
						this.tailor_month(date[1]);
						//$('select[name="DOP_month"]').selectmenu('refresh');
						this.tailor_day(date[2]);
						//$('select[name="DOP_day"]').selectmenu('refresh');
					}
				},
				owner: this
			});

			this.fittingvideos = ko.observableArray();
			this.FittingVideoViewModel = function (thepath, thenotes) {
				var self = this;
				this.path = ko.observable(thepath);
				this.notes = ko.observable(thenotes);
			};
			this.addFittingVideo = function () {
				var checkfailed = false;
				for (var pos = 0; pos < this.fittingvideos().length; pos++) {
					if (this.fittingvideos()[pos].path().length == 0 || this.fittingvideos()[pos].notes().length == 0) {
						checkfailed = true;
						break;
					}
				}
				if (checkfailed == false) {
					this.fittingvideos.push(new this.FittingVideoViewModel("", ""));
				}
			};

			this.removeFittingVideo = function (pos) {
				this.fittingvideos.remove(this.fittingvideos()[pos]);
			};

			this.transmuteDate = function (date) {  // input is yyyy-mm-dd
				if (date != undefined && date != null) {

					var ymd = date.substring(0, date.lastIndexOf(" "));
					var time = date.substring(date.lastIndexOf(" ") + 1);

					var day = ymd.substring(ymd.lastIndexOf("-") + 1);
					var month = ymd.substring(ymd.indexOf("-") + 1, ymd.lastIndexOf("-"));
					var year = ymd.substring(0, ymd.indexOf("-"));
					return day + "-" + month + "-" + year + " " + time;
				} else {
					return 0;
				}
			};

			this.sync = function () {

				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";

				var photosarray = [];
				photos = localStorage.getItem('fittingphotos');
				photos = (photos == null) ? [] : JSON.parse(photos);
				if (localStorage.getItem('fittingphotos') != null) {
					if (localStorage.getItem('fittingphotos').length > 0) {
						for (var i = 0, len = photos.length; i < len; i++) {
							if (photos[i].customer_id == orderItem.custSelectVM.selectedCustomer().server_id) {
								var obj = new Object();
								obj.name = photos[i].picture;
								obj.id = "" + photos[i].image_id + "";
								var jsonString = JSON.stringify(obj);
								//photosarray.push(photos[i].picture);
								photosarray.push(obj);
							}
						}
					}
				}

				var tagsarray = [];
				tags = localStorage.getItem('fittingtags');
				tags = (tags == null) ? [] : JSON.parse(tags);
				if (localStorage.getItem('fittingtags') != null) {
					if (localStorage.getItem('fittingtags').length > 0) {
						for (var j = 0; j < tags.length; j++) {
							for (var i = 0; i < photos.length; i++) {
								if (tags[j].image_id == photos[i].image_id) {
									tagsarray.push(tags[j]);
								}
							}
						}
					}
				}

				var customerString = orderItem.custSelectVM.selectedCustomer();
				var fittingIdPost = this.fitting_id;
				var dateToPost = '';
				var date = new Date();
				var yearnow = date.getFullYear();
				var monthnow = date.getMonth() + 1;
				var daynow = date.getDate();
				if (this.tailor_month() != undefined && this.tailor_month() != "" && this.tailor_day() != undefined && this.tailor_day() != "") {
					if (this.tailor_month() < monthnow || (this.tailor_month() == monthnow && this.tailor_day() < daynow)) {
						var yearnew = yearnow + 1;
						dateToPost = this.tailor_day() + "-" + this.tailor_month() + "-" + yearnew;
					} else {
						dateToPost = this.tailor_day() + "-" + this.tailor_month() + "-" + yearnow;
					}
				}

				var fittingvideospost = this.fittingvideos;
				for (var x = 0; x < fittingvideospost().length; x++) {
					if (fittingvideospost()[x].path().length == 0) {
						fittingvideospost.remove(fittingvideospost()[x]);
						x--;
					}
				}

				var tailorIdPost = "";
				if (this.tailor() != undefined) {
					if (this.tailor().user_id != undefined) {
						tailorIdPost = this.tailor().user_id;
					}
				}

				if (tailorIdPost == "" && dateToPost == "" && this.tailornote() == "" && photosarray.length == 0 && fittingvideospost().length == 0) {
					customAlert("All fields are empty!");
				} else {
					data = {
						user_id: authCtrl.userInfo.user_id,
						customer: customerString,
						fitting_id: this.fitting_id(),
						order_id: orderItem.custOrdersVM.selectedOrder().order_id,//this.selected_order.order_id,
						garment_id: orderItem.custOrderVM.selectedOrderGarment().orders_products_id,
						tailor: {
							user_id: tailorIdPost,
							delivery_date: dateToPost,
							notes: this.tailornote()
						},
						fitting_images: photosarray,
						fitting_tags: tagsarray,
						fitting_videos: ko.toJSON(fittingvideospost())
						//fitting_id, order_id, garment_id, tailor(user_id, delivery_date, notes), fitting_images(id, name), fitting_tags(left, top, text), fitting_videos(path, notes).
					};

					var datatopost = JSON.stringify(data);
					datatopost = datatopost.replace("\"isDirty\":true,", "");

					$.post(BUrl + 'client_fittings/fitting_endpoint', {
						data: datatopost
					},
						function (dataS) {
							dataS = JSON.parse(dataS);
							if (dataS.valid == '1') {
								thephotos = localStorage.getItem('fittingphotos');
								thephotos = (thephotos === null) ? [] : JSON.parse(thephotos);
								for (var a = thephotos.length - 1; a >= 0; a--) {
									if (thephotos[a].customer_id == orderItem.custSelectVM.selectedCustomer().server_id) {
										thephotos.splice(a, 1);
									}
								}

								thetags = localStorage.getItem('fittingtags');
								thetags = (thetags == null) ? [] : JSON.parse(thetags);
								if (localStorage.getItem('fittingtags') != null) {
									if (localStorage.getItem('fittingtags').length > 0) {
										for (var j = thetags.length - 1; j >= 0; j--) {
											var tagimagefound = false;
											for (var i = 0; i < thephotos.length; i++) {
												if (thetags[j].image_id == thephotos[i].image_id) {
													tagimagefound = true;
												}
											}
											if (tagimagefound == false) {
												thetags.splice(j, 1);
											}
										}
									}
								}

								var tmp = [];
								for (var x = 0; x < thephotos.length; x++) {
									tmp.push({ customer_id: thephotos[x].customer_id, image_id: thephotos[x].image_id, picture: thephotos[x].picture, thumbnail: thephotos[x].thumbnail });
								}
								localStorage.removeItem('fittingphotos');
								localStorage.setItem('fittingphotos', JSON.stringify(tmp));

								var tmptags = [];
								for (var x = 0; x < thetags.length; x++) {
									tmptags.push({ image_id: thetags[x].image_id, left: thetags[x].left, top: thetags[x].top, text: thetags[x].text });
								}

								localStorage.removeItem('fittingtags');
								localStorage.setItem('fittingtags', JSON.stringify(tmptags));
								///////////////////////////////////
								posChangePage('#orderInfo');
								$.jGrowl('Fitting has been saved!');
							} else {
								console.log('fail');
								$.jGrowl(dataS.msg);
							}
						});
				}
				spinner.style.display = "none";
			}
		}

	});

	OrdersGarmentsForFittingClass = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			this.DsRegistry = DsRegistry;
			var self = this;
			this.ordersgarments = ko.observableArray();
			this.selectedOrderGarment = ko.observable({});

			this.selectedOrderGarment.subscribe(function (data) {
				if (self.parentViewModel !== null) {
					self.parentViewModel.setSelectedOrder(data);
				} else {
					console.log("self.parentViewModel == null");
				}
			});

			this.timestampToDate = function (UNIX_timestamp) {
				var a = new Date(UNIX_timestamp * 1000);
				var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				var year = a.getFullYear();
				var month = months[a.getMonth()];
				var date = a.getDate();
				var time = date + '/' + (a.getMonth() + 1) + '/' + year;
				return time;
			}


			this.modifiedListOptionsFunction = function (elemname) { // 0: element name
				var checkboxes = document.getElementsByName(elemname);
				var x = 0;
				//console.log("ordersgarments for fitting: " + JSON.stringify( this.ordersgarments() ) );
				for (var a in this.ordersgarments()) {
					for (var b in this.ordersgarments()[a].products) {
						if (this.ordersgarments()[a].products[b].checked == true) {
							checkboxes[x].classList.add('selecteds');
						} else {
							checkboxes[x].classList.remove('selecteds');
						}
						x++;
					}
				}
			};

			this.loadSelectedGarmentsToFittingsAndGoToFittingsPage = function (orderindex, garmentindex, fittingindex) {

				console.log("orderindex: " + orderindex + ", garmentindex: " + garmentindex + ", fittingindex:" + fittingindex);

				if (fittingindex != -1) {
					for (var a in this.ordersgarments()) {
						for (var b in this.ordersgarments()[a].products) {
							if (a == orderindex && b == garmentindex/* && c == fittingindex*/) {
								this.ordersgarments()[a].products[b].checked = true;
							} else {
								this.ordersgarments()[a].products[b].checked = false;
							}
						}
					}
				}

				localStorage.removeItem("fittingindex");
				localStorage.setItem("fittingindex", fittingindex);
				orderItem.fittingsVM = new Fitting(orderItem.dsRegistry);
				orderItem.fittingsDS = new defFitting('fittingsDS', orderItem.dsRegistry, this.ordersgarments());
				orderItem.fittingsVM.subscribeTo('fittingsDS');
				console.log("orderItem.fittingsDS.getStore(): " + JSON.stringify(orderItem.fittingsDS.getStore()));
				//console.log("orderItem.fittingsVM.FittingData: " + JSON.stringify( orderItem.fittingsVM.FittingData ) );
				orderItem.fittingsVM.addVariantsFittings();

				if (orderItem.fittingsDS.getStore().Fitting.length == 0) {
					customAlert("Select at least one garment for the fitting");
				} else {
					posChangePage('#fittings');
				}
			};

			/**
			 * Automatically set the AVALAIBLE garments as "checked", so the we ca go to fitting screen without
			 * customerGarmentsFittingsList
			 *
			 *
			 * Deprecated -
			 * The decision of removing screen between fitting and fittingList was declined, so this method
			 * will no longer be used
			 */
			this.setAvalaibleChecked = function () {
				// for each order garment
				for (let index = 0; index < this.ordersgarments().length; index++) {
					const garment = this.ordersgarments()[index];
					// for each product
					for (let index2 = 0; index2 < garment.products.length; index2++) {
						const product = garment.products[index2];
						// check fitting status. "1" is the ID for AVALAIBLE
						if (product.fitting_status == "1") {
							product.checked = true;
						}

					}

				}
			};


			this.loadSelectedGarmentsToCompletionsAndGoToCompletionsPage = function () {


				console.log("loadSelectedGarmentsToCompletionsAndGoToCompletionsPage " + this.ordersgarments().length);

				orderItem.completionsVM = new Completion(orderItem.dsRegistry);
				orderItem.completionsDS = new defCompletion('completionsDS', orderItem.dsRegistry, this.ordersgarments());
				orderItem.completionsVM.subscribeTo('completionsDS');
				console.log("orderItem.completionsDS.getStore(): " + JSON.stringify(orderItem.completionsDS.getStore()));
				//console.log("orderItem.completionsVM.CompletionData: " + JSON.stringify( orderItem.completionsVM.CompletionData ) );
				orderItem.completionsVM.addVariantsCompletions();

				if (orderItem.completionsDS.getStore().Completion.length == 0) {
					customAlert("Select at least one garment for the completion");
				}
				else {
					posChangePage('#completions');
				}
			};

		},

		digestData: function (data) {
			var newObserve = [];
			var fabrics = this.DsRegistry.getDatasource('fabricsDS').getStore();
			//for (var ind in data) if (!data[ind].isDead) newObserve.push(data[ind]);
			for (var ind in data) {
				if (data[ind] != null) {
					if (!data[ind].isDead) {
						//	if(ind < 10){
						newObserve.push(data[ind]);
						for (var x in newObserve[ind].products) {
							newObserve[ind].products[x].checked = false;
							newObserve[ind].products[x].fabricImage = '';
							for (y in fabrics) {
								if (fabrics[y].title == newObserve[ind].products[x].fabric) {
									newObserve[ind].products[x].fabricImage = fabrics[y].fabricImage;
								}
							}
							//newObserve[ind].products[x].
						}
						//newObserve[ind].checked = false;
						//	}
					}
				}
			}
			this.ordersgarments(newObserve);
			console.log("ordersgarments for fitting: " + JSON.stringify(this.ordersgarments()));
		},

		/** Resume all shirt's fittings into a single procedure */
		fittingAllShirt: function () {
			console.log("fittingAllShirt");

			// for each order garment
			for (let index = 0; index < this.ordersgarments().length; index++) {
				const garment = this.ordersgarments()[index];
				// for each product
				for (let index2 = 0; index2 < garment.products.length; index2++) {
					const product = garment.products[index2];
					// check fitting status. "1" is the ID for AVALAIBLE
					// check garment is shirt
					if (product.fitting_status == "1" && product.garment == "Shirt") {
						product.checked = true;
					}

				}

			}

			// Go to fitting
			orderItem.OrdersGarmentsForFittingVM.loadSelectedGarmentsToFittingsAndGoToFittingsPage(-1, -1, -1);
		}
	});








});