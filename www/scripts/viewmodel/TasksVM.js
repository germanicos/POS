define(['jquery', 'knockout', 'base'], function ($, ko) {

    TasksVM = class {
        constructor() {
            this.mapTaskIdToPOS_Step = this.getMapTaskIdToPOS_Step();

            this.tasks = ko.observableArray();
            this.getTasksFromERP();
        }

        getTasksFromERP() {
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'outside_controller/get_pos_users_tasks',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo
                },
                success: (ret) => {

                    console.log('ret', ret);

                    // Build POS steps manually (see this.getMapTaskIdToPOS_Step() documentation)
                    for (const task of ret.tasks) {
                        // get POS_Step from map, using step id
                        const matchingMap = this.mapTaskIdToPOS_Step.find(el => el.databaseId == task.id);

                        if (!matchingMap) {
                            task.POS_Step = "";
                        } else {
                            task.POS_Step = matchingMap.POS_Step;
                        }

                    }

                    this.tasks(ret.tasks);

                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                    posChangePage('#main');
                },
                async: false
            });
        }

        /**
         * @returns "MORNING" / "EVENING" / "NIGHT" 
         */
        getTimeOutput() {
            var today = new Date()
            var curHr = today.getHours()

            if (curHr < 12) {
                return ('Good Morning');
            } else if (curHr < 18) {
                return ('Good Afternoon');
            } else {
                return ('Good Evening');
            }
        }

        get tasksNumber() {
            return this.tasks().length;
        }

        get userName() {
            return authCtrl.userInfo.first_name + " " + authCtrl.userInfo.last_name;
        }


        /**
         * For now, we have to manually map a id from database to a step on POS.
         * On ERP we have two columns named "method_generator_name" and "action_link" to use on "go to step" button. On POS we don't.
         * 
         * This method works but must be temporary. It's bad for maintenance 
         */
        getMapTaskIdToPOS_Step() {
            return [
                { databaseId: 1,  POS_Step: "#orderProcessType",     taskName: "New Orders to approve"              },
                { databaseId: 2,  POS_Step: "#orderProcessType",     taskName: "Late Orders to approve"             },
                { databaseId: 3,  POS_Step: "",                      taskName: "Admin Fabrics to order"             },
                { databaseId: 4,  POS_Step: "",                      taskName: "Payments to confirm"                },
                { databaseId: 5,  POS_Step: "#shipping_main_new",    taskName: "Shipping Boxes to receive"          },
                { databaseId: 6,  POS_Step: "",                      taskName: "New Leads to process"               },
                { databaseId: 7,  POS_Step: "",                      taskName: "Put Order In production"            },
                { databaseId: 8,  POS_Step: "",                      taskName: "Put first panel information"        },
                { databaseId: 9,  POS_Step: "#alterationsDashBoard", taskName: "Brief thai tailor for alteration"   },
                { databaseId: 10, POS_Step: "#alterationsDashBoard", taskName: "Pending Alteration reports"         },
                { databaseId: 11, POS_Step: "#orderProcessType",     taskName: "Pending patterns to update"         },
                { databaseId: 12, POS_Step: "",                      taskName: "Pending boxes to ship to AUS"       },
                { databaseId: 13, POS_Step: "",                      taskName: "Pending boxes to receive from AUS"  },
                { databaseId: 14, POS_Step: "",                      taskName: "Shipping boxes pending information" },
                { databaseId: 15, POS_Step: "",                      taskName: "Fabrics to add cost"                },
                { databaseId: 16, POS_Step: "",                      taskName: "Fabrics to receive"                 },
                { databaseId: 17, POS_Step: "",                      taskName: "Input correct fabric code"          },
                { databaseId: 18, POS_Step: "",                      taskName: "Urgent Items to put in a box"       }
            ]
        }

        goToStep(step) {

            if (step.POS_Step) {
                posChangePage(step.POS_Step);
            }

            else {
                customAlert("This step is in development");
            }
        }
    }


});