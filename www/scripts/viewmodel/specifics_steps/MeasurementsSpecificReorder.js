
/*
require.undef('viewmodel/specifics_steps/MeasurementsSpecificReorder');
require(['viewmodel/specifics_steps/MeasurementsSpecificReorder']);
*/
define(['jquery', 'knockout', 'base'], function($, ko) {

    /**
     * handle Specific measurement step.
     *
     * Must be developed in vanillaJs and jQuery only (no knockout)
     *
     * @param {*} wrapper - string: where to load the view
     *
     */
    MeasurementsSpecificReorder  = class {
        constructor(wrapper, order, selectedCustomer) {
            $(wrapper).load('reorder_steps/specifics_steps/specific_measurement_reorder.htm'); // load view

            this.wrapper = wrapper;
            this.order = order;
            this.selectedCustomer = selectedCustomer; // basic info of customer
            this.selectedCustomerFromOrder = order.customers.find(cus => cus.customer_id == selectedCustomer.customer_id); // detailed customer

            // measurement values for all garments
            this.measurements = this.selectedCustomerFromOrder.measurements;

            // get only garments selected in this order
            this.availableMeasurements = this._getAvailableMeasurements();

            // to show de meas values only of selected garment
            this.selectedGarmentTypeId = null;

            console.log('MeasurementsSpecificReorder', this);

            // give time to page be loaded
            sleep(1500).then(() => {
                this.render();
                this.applyBinds();
                $('#loading_jp').hide();
            });

        }

        _getAvailableMeasurements(){
            let availableMeas = this.selectedCustomerFromOrder.reorder_data.garments.filter(garments => garments.garments.length > 0);

            // if there is a suit, split it into jacket and pants
            if (availableMeas.find(_availableMeas => _availableMeas.id == 6)){
                // add jacket if not exists
                if (!availableMeas.find(_availableMeas => _availableMeas.id == 4)){
                    availableMeas.push(this.selectedCustomerFromOrder.reorder_data.garments.find(garments => garments.id == 4))
                }

                // add pants if not exists
                if (!availableMeas.find(_availableMeas => _availableMeas.id == 1)){
                    availableMeas.push(this.selectedCustomerFromOrder.reorder_data.garments.find(garments => garments.id == 1))
                }

                // remove this suit
                availableMeas = availableMeas.filter(_availableMeas => _availableMeas.id != 6);

            }

            // sort measurement (1st JACKET, 2nd PANTS, 3rd SHIRT & 4th VEST)
            availableMeas.sort((a,b) => {
                // jacket
                if (a.id == "4") return -1;
                if (b.id == "4") return 1;

                // pants
                if (a.id == "1") return -1;
                if (b.id == "1") return 1;

                // shirt
                if (a.id == "2") return -1;
                if (b.id == "2") return 1;

                // vest
                if (a.id == "3") return -1;
                if (b.id == "3") return 1;

                return 0;
            });

            // get last measurement date for each garment
            const measHistory = this.selectedCustomerFromOrder.measurements_history;
            for (const meas of availableMeas) {
                const measHistoryOfThisGarment = measHistory[meas.name];

                meas.last_meas = measHistoryOfThisGarment[measHistoryOfThisGarment.length - 1].formatted_date;
            }

            return availableMeas;
        }

        selectGarmentTypeId(garmentTypeId){
            this.selectedGarmentTypeId = garmentTypeId;

            this.render();


        }

        /**
         * Says if a measurement is mandatory or not by his KEY (id)
         * @param {*} meas_key
         */
        isMandatory(meas_key){
            const is_not_mandatory = [
                "jacket_measurements_last_edited", "jacket_measurements_lapel_length", "jacket_measurements_notes",
                "pants_measurements_last_edited", "pants_measurements_front_crotch", "pants_measurements_inseam", "pants_measurements_zipper", "pants_measurements_notes", "pants_measurements_angled_hems",
                "shirt_measurements_last_edited", "shirt_measurements_wrist", "shirt_measurements_notes", "shirt_measurements_watch",
                "vest_measurements_last_edited", "vest_measurements_v_length", "vest_measurements_notes",
            ]

            if (is_not_mandatory.includes(meas_key)){
                return false;
            } else {
                return true;
            }
        }

        get selectedGarmentMeasurement(){
            let measurementToCheck = null
            switch (this.selectedGarmentTypeId) {
                case 1:
                    measurementToCheck = this.measurements['pants']
                    break;
                case 2:
                    measurementToCheck = this.measurements['shirt']
                    break;
                case 3:
                    measurementToCheck = this.measurements['vest']
                    break;
                case 4:
                    measurementToCheck = this.measurements['jacket']
                    break;
                case 6:
                    measurementToCheck = this.measurements['jacket'];
                    measurementToCheck = measurementToCheck.concat(this.measurements['pants']);
                    break;

                default:
                    break;
            }

            return measurementToCheck;
        }


        /**
         * Checks if all mandatory measurements of a garment_type are complete
         * @param {*} garmentTypeId
         */
        isGarmentTypeComplete(garmentTypeId){
            garmentTypeId = parseInt(garmentTypeId);

            let response = true;

            let measurementToCheck = []
            switch (garmentTypeId) {
                case 1:
                    measurementToCheck = this.measurements['pants']
                    break;
                case 2:
                    measurementToCheck = this.measurements['shirt']
                    break;
                case 3:
                    measurementToCheck = this.measurements['vest']
                    break;
                case 4:
                    measurementToCheck = this.measurements['jacket']
                    break;
                case 6:
                    measurementToCheck = this.measurements['jacket'];
                    measurementToCheck = measurementToCheck.concat(this.measurements['pants']);
                    break;

                default:
                    break;
            }

            for (const meas of measurementToCheck) {
                if (this.isMandatory(meas.key) &&
                    ( parseInt(meas.value) === 0 || !meas.value)
                ){
                    response = false;
                    break;
                }
            }

            return response;
        }

        /**
         * Method to update view
         */
        render(){
            const _renderAvailableGarments = () => {
                const html = /* html */`
                    ${this.availableMeasurements.map(availableMeasurement => {
                        return /* html */`
                        <div class="garment-meas-tab">
                            <img
                                class="garment-type-meas-img"
                                src="${availableMeasurement.image}"
                                data-available-measurement-id="${(availableMeasurement.id)}"
                            />

                            <!-- tick of complete garment -->
                            ${this.isGarmentTypeComplete(parseInt(availableMeasurement.id)) ? /* html */`
                                <img class="complete-measurement" src="img/order_process/checkButom2.png"/>` : ``}

                        </div>`
                    }).join("")}
                `

                const wrapper = "#specific_measurement_reorder .garments-step-counter-wrapper";

                $(wrapper).html(html);

            }

            const _renderMeasurementValues = () => {
                let garment = {};
                switch (this.selectedGarmentTypeId) {
                    case 1:
                        garment.measurements = this.measurements['pants']
                        break;
                    case 2:
                        garment.measurements = this.measurements['shirt']
                        break;
                    case 3:
                        garment.measurements = this.measurements['vest']
                        break;
                    case 4:
                        garment.measurements = this.measurements['jacket']
                        break;
                    case 6:
                        garment.measurements = this.measurements['jacket'];
                        garment.measurements = garment.measurements.concat(this.measurements['pants']);
                        break;

                    default:
                        break;
                }

                if (!garment.measurements) return;

                const note_meas = garment.measurements.find(meas => meas.title.trim() == 'Notes');

                const html = /* html */`
                    <div class="meas-wrapper">
                        ${garment.measurements.map($data => {
                            return /* html */`
                                <div class="col-md-2 garment-meas-el-wrapper">

                                    <!-- do not show dates or notes -->
                                    ${$data.title.trim() != 'Notes' && $data.title.trim() != 'date' ? /* html */`
                                        <p style="margin: 0; display: flex;">
                                            <span>${$data.title}</span>
                                            ${this.isMandatory($data.key) ? /* html */`<span class="mandatoris">*</span>` : ``}

                                        </p>
                                        <!-- pants_measurements_angled_hems is a boolean-->
                                        ${($data.key != "pants_measurements_angled_hems") ?
                                        /* html */ `<span>
                                                        <input min="0" step="0.25" type="number" class="garment-meas-input" value="${$data.value}" data-meas-key="${$data.key}">
                                                    </span>`
                                        :
                                        /* html */`<span>
                                                        <input type="checkbox" class="garment-meas-input" style="width: 40px;height: 40px;" ${$data.value ? " checked ": ""} data-meas-key="${$data.key}" />
                                                    </span>`
                                        }
                                    ` : ``}

                                </div>

                            `
                        }).join("")}

                        <!-- Div only for the notes -->
                        <div class="meas-wrapper-notes">

                            <div class="col-md-12">

                                <p style="margin: 0;font-size: 30px">${note_meas.title}</p>

                                <textarea type="text" class="garment-meas-textarea" data-meas-key="${note_meas.key}">${note_meas.value}</textarea>

                            </div>
                        </div>
                    </div>
                `

                const wrapper = "#specific_measurement_reorder .meas-values-wrapper";
                $(wrapper).html(html);
            }

            const _renderMeasHistory = () => {
                // if no meas are selected
                if (!this.selectedGarmentTypeId){
                    $('.meas-additional-info').hide();
                    return;
                }

                // get last edited value and insert on HTML
                const currentMeas = this.availableMeasurements.find(meas => meas.id == this.selectedGarmentTypeId);

                $('.meas-additional-info .value').text(currentMeas.last_meas);
                $('.meas-additional-info').show();
            }

            const _applyBinds = () => {
                $(".garment-type-meas-img").on("click", (event) => {
                    const element = event.target;
                    const availableMeasurementId = JSON.parse($(element).data('availableMeasurementId'));
                    this.selectGarmentTypeId(availableMeasurementId);
                });

                $(".garment-meas-input, .garment-meas-textarea").on("change", (event) => {
                    const element = event.target;

                    const meas_key = $(element).data('measKey');
                    const meas_new_value = $(element).is(":checkbox") ? $(element).prop("checked") : $(element).val();

                    console.log('meas_new_value', meas_new_value);

                    if (!meas_key) return;

                    switch (this.selectedGarmentTypeId) {
                        case 1:
                            this.measurements['pants'].find(meas => meas.key == meas_key).value = meas_new_value;
                            break;
                        case 2:
                            this.measurements['shirt'].find(meas => meas.key == meas_key).value = meas_new_value;
                            break;
                        case 3:
                            this.measurements['vest'].find(meas => meas.key == meas_key).value = meas_new_value;
                            break;
                        case 4:
                            this.measurements['jacket'].find(meas => meas.key == meas_key).value = meas_new_value;
                            break;
                        case 6:
                            const is_jacket_meas = (this.measurements['jacket'].find(meas => meas.key == meas_key));
                            if (is_jacket_meas) {
                                is_jacket_meas.value = meas_new_value;
                            } else {
                                this.measurements['pants'].find(meas => meas.key == meas_key).value = meas_new_value;
                            }
                            break;

                        default:
                            break;
                    }
                });

            }

            _renderAvailableGarments();
            _renderMeasurementValues();
            _renderMeasHistory();

            _applyBinds();
        }

        applyBinds(){
            const self = this;
            $(".j-footer .save-btn").on("click", (event) => {

                // check if all garments are complete
                let is_complete = true;
                for (const availableMeas of this.availableMeasurements) {
                    if (!this.isGarmentTypeComplete(availableMeas.id)){
                        pos_warning({msg: `A measurement for ${availableMeas.name} is incomplete`});
                        is_complete = false;
                        break;
                    }
                }

                if (is_complete){
                    this.updateInBackend();
                }

                this.render();

            });
        }

        updateInBackend(){
            const url = "orders_pos/update_reorder_meas";
            const data = this.buildSubmissionData();

            console.log('data', data);

            pos_confirm({msg:"Update customer measurements info ?", confirmAction: () => {

                document.getElementById('loading_jp').style.display = "block";

                $.ajax({
                    type: 'POST',
                    url: BUrl+url,
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        "data": JSON.stringify(data),
                    },
                    success: function (dataS) {

                        document.getElementById('loading_jp').style.display = "none";

                        pos_ok({ msg: "Measurements updated successfully" });
                        pendingGroupOrders.getPendingOrdersAjax(); // pendingGroupOrders is global
                        pendingGroupOrders.whatIsMissingPopUp.steps(pendingGroupOrders.whatIsMissingPopUp.getPendingSteps()); // refresh steps status
                        $("#reorder_specifics_steps_wrapper .generic-pop-up").hide("fast");;
                    },
                    error: function (dataS) {

                        document.getElementById('loading_jp').style.display = "none";

                        pos_error({ msg: "Something wrong with " + url });
                    },
                });

            }});



        }

        buildSubmissionData(){
            // send only available measurements
            const available_meas_keys = this.availableMeasurements.map(meas => meas.name);

            const meas_to_send = {};

            Object.keys(this.measurements).forEach(key => {
                if (available_meas_keys.includes(key)){
                    meas_to_send[key] = this.measurements[key];
                }

            });

            return {
                measurements: meas_to_send,
                customer_id: this.selectedCustomer.customer_id,
                order_id: this.selectedCustomer.order_id,
            }
        }
    }

});