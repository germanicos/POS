/*
require.undef('viewmodel/specifics_steps/MediaSpecificReorder');
require(['viewmodel/specifics_steps/MediaSpecificReorder']);
*/
define(['jquery', 'knockout', 'base'], function ($, ko) {

    MediaSpecificReorder = class {
        constructor(wrapper, order, selectedCustomer) {
            $(wrapper).load('reorder_steps/specifics_steps/specific_media_reorder.htm'); // load view

            this.wrapper = wrapper;
            this.order = order;
            this.selectedCustomer = selectedCustomer; // basic info of customer
            this.selectedCustomerFromOrder = order.customers.find(cus => cus.customer_id == selectedCustomer.customer_id); // detailed customer
            this.getMediaFromERP();


            this.currentTaggingImage = null;
            this.currentAddingTag = null;


            console.log('mediaSpecificReorder', this);

            // give time to page be loaded
            sleep(1500).then(() => {
                this.render();
                this.applyBinds();
                $('#loading_jp').hide();
            });
        }

        // image tags ====================
        /**
         * Creates a new tag object
         * @param {*} posX
         * @param {*} posY
         */
        getNewCurrentAddingTag(text, posX, posY) {

            // Pos X and Pos Y in percentages
            return {
                'text' : text,
                'posX' : posX,
                'posY' : posY
            };
        }

        /**
         * Add a new tag on image
         * @param {*} image
         * @param {*} text
         * @param {*} event
         */
        tagImage(image, text, event){
            //  this is binded to the item
            // console.log("this", this);
            console.log("item", image);
            console.log("event", event);

            const mediaWidth = $(event.target).width();
            const mediaHheight = $(event.target).height();

            const posX = $(event.target).position().left;
            const posY = $(event.target).position().top;

            // in percentages
            const finalX = (event.offsetX - posX) / mediaWidth;
            const finalY = (event.offsetY - posY) / mediaHheight;

            console.log( "X: " + finalX + ' , ' + " Y: " + finalY );

            // creates the new tag
            const newTag = this.getNewCurrentAddingTag(text, finalX, finalY);

            // sets the clicked image as the 'in tagging process' image
            this.currentTaggingImage = (image);

            // adds the tags to the image
            image.tags.push(newTag);

            // Sets the current tag
            this.currentAddingTag = (newTag);

            // Fix position fixed bug on IOS with keyboard
            $(".add-tag-pop-up").css({
                "position": "absolute",
                "top": ( ((($(parent).height() - $(".add-tag-pop-up").outerHeight()) / 10) + $(parent).scrollTop() ) + "px"),
                "left": ((($(parent).width() - $(".add-tag-pop-up").outerWidth()) / 2) + $(parent).scrollLeft() + "px")
            });

            this.render();

        }

        editTag(imageType, imageIndex, tagIndex, newText){

            const images = this.selectedCustomerFromOrder.reorder_data.images;

            images[imageIndex].tags[tagIndex].text = newText

            this.render();

        }

        removeTag(imageType, imageIndex, tagIndex){

            const images = this.selectedCustomerFromOrder.reorder_data.images;

            images[imageIndex].tags.splice(tagIndex, 1);


            this.render();


        }


        // image ====================


        /**
         * Create a new image object
         */
        getNewImage(type, url) {
            return {
                'type': type,
                'url': url,
                'tags': [],
            };
        }

        addImage(type, url){
            const images = this.selectedCustomerFromOrder.reorder_data.images;

            console.log('After move');

            console.log("url", url);

            const new_img = this.getNewImage(type, url);

            images.push(new_img);

            this.render();
        }

        removeImage(type, index){
            let images = this.selectedCustomerFromOrder.reorder_data.images;




            const confirmAction = () => {
                images.splice(index, 1);
                this.render();
            };

            pos_confirm({
                msg: "Are you sure you want to remove this image?",
                confirmAction,
            })
        }

        /** IOS: ask if user wants to use camera or gallery */
        choosePicSource(imgType) {

            var self = this;

            console.log("choosing Pic source...");

            navigator.notification.confirm(
                'Picture Source', // message
                function(btnIndex) {
                    self.takePicture(btnIndex, imgType);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

        }

        /**
         * Type => front/back/side/additional
         * @param  {[type]} buttonIndex [description]
         * @param  {[type]} type        [description]
         * @return {[type]}             [description]
         */
        takePicture(buttonIndex, type) {

            self = this;

            console.log("Taking Pic...");

            const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            });

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };

            function successMove(entry) {
                self.addImage(type, entry.toURL());
            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        } // end Capturing pic

        // video ====================

        getNewVideo(url) {
            return {
                'url': url,
                'comments': []
            };
        }

        addNewVideo(url){
            const videos = this.selectedCustomerFromOrder.reorder_data.videos;

            const new_video = this.getNewVideo(url);

            videos.push(new_video);

            this.render();
        }

        removeVideo(video_index){

            pos_confirm({
                msg: "Are you sure you want to remove this video?",
                confirmAction: () => {
                    const videos = this.selectedCustomerFromOrder.reorder_data.videos;
                    videos.splice(video_index, 1);
                    this.render();
                },
            })

        }

        chooseVideoSource() {

            var self = this;

            console.log("choosing Video source...");

            navigator.notification.confirm(
                'Video Source', // message
                function(btnIndex) {
                    self.takeVideo(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

        }

        takeVideo(buttonIndex) {

            self = this;



            console.log("Taking Video...");

            if( buttonIndex == 1 ) // from camera
            {
                navigator.device.capture.captureVideo(onSuccess, onFail,
                {
                    quality: 1,
                    saveToPhotoAlbum: true
                });
            }
            else // From gallery
            {
                navigator.camera.getPicture(onGalleryDataSuccess, onFail, {
                                quality: 1,
                                destinationType: Camera.DestinationType.FILE_URI,
                                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                mediaType: Camera.MediaType.VIDEO,
                                correctOrientation : true
                            });
            }


            function onSuccess(mediaFiles) {

                console.log("onVideoDataSuccess", mediaFiles);
                var i, path, len;

                for (i = 0, len = mediaFiles.length; i < len; i += 1) {

                    console.log(mediaFiles[i].localURL);
                    console.log(mediaFiles[i].fullPath);
                    window.resolveLocalFileSystemURL(mediaFiles[i].localURL, resolveOnSuccess, resOnError);
                }
            }

            function onGalleryDataSuccess(videoURI) {
                console.log("onGalleryDataSuccess");
                window.resolveLocalFileSystemURL(videoURI, successMove, resOnError);
            };


            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.mov';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },
                resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                self.addNewVideo(entry.toURL())

            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        } // end Capturing pic

        // video tags ====================

        addNewVideoTag(videoIndex, videoText){
            const videos = this.selectedCustomerFromOrder.reorder_data.videos;
            videos[videoIndex].comments.push(videoText);
            this.render();
        }

        removeVideoTag(videoIndex, commentIndex){
            const videos = this.selectedCustomerFromOrder.reorder_data.videos;
            videos[videoIndex].comments.splice(commentIndex, 1);
            this.render();
        }




        render(){
            const _renderImages = () => {
                const wrapper = "#images-wrapper";

                const images = this.selectedCustomerFromOrder.reorder_data.images;

                const getImageHtml = (image, image_index) => {

                    // if image exists
                    if (image.url){
                        return /* html */`
                            <!-- image div -->
                            <div class="col-md-7">
                                <div>
                                    <div class="image-tag-parent" data-image-type="${image.type}" data-image-index="${image_index}">
                                        <img class="order-img" src="${image.url}">

                                        ${image.tags.map((tag, tag_index) => {
                                            return /* html */`
                                                <button
                                                    class="numberCircle"
                                                    style="left: ${tag.posX * 100 + '%'}; top: ${tag.posY * 100 +'%'}"
                                                    data-tag-index="${tag_index}"
                                                    data-tag-text="${tag.text}">
                                                        ${parseInt(tag_index)+1}
                                                </button>
                                            `;
                                        }).join("")}


                                        <span class="remove-image"> X </span>

                                    </div>
                                </div>
                            </div>

                            <!-- comments -->
                            <div class="col-md-5">
                                <ul class="img-tag-text-list">
                                    ${image.tags.length > 0 ? /* html */`
                                        ${image.tags.map((tag, index) => {
                                            return /* html */`
                                                <li>
                                                    <span class="tag-text-span" >${(index+1) + '. ' + tag.text}</span>
                                                </li>`
                                            }).join("")
                                        }`

                                        :

                                        /* html */`
                                        <li>
                                            <span class="tag-text-span" >No Tags</span>
                                        </li>`
                                    }

                                </ul>

                            </div>
                        `;
                    }

                    else {
                        return /* html */`
                            <button class="upload-img-btn" data-image-type="${image.type}" data-image-index="${image_index}">ADD image</button>
                        `
                    }

                }

                const html = /* html */`
                    <!-- Front back and side images -->
                    ${images.map((image, index) => {
                        return /* html */`
                            <div>
                                <!-- image type -->
                                <h3 class="image-definition-h"></h3>

                                ${getImageHtml(image, index)}

                                <br style="clear: both;">
                                <hr class="payment-hr">
                            </div>
                        `;
                    }).join("")}

                    <button class="upload-img-btn" data-image-type=">>ERP_DOES_NOT SEND_IMAGE_TYPE<<">ADD image</button>

                `;

                $(wrapper).html(html);

            }

            const _renderVideos = () => {

                const wrapper = "#videos-wrapper";
                const videos = this.selectedCustomerFromOrder.reorder_data.videos;

                const html = /* html */`
                    <!-- add new video -->
                    <div class="upload-video-btn" style="left: 5px;" data-bind="click: function() { $root.selectedCustomer().uploadVideosStep.chooseVideoSource(); }">
                        <span class="fvideo"></span>
                        <span>Take Video</span>
                    </div>

                    <br><br>

                    <div>

                        ${videos.map((video, video_index) => {
                            return /* html */`
                                <div class="row a-video" data-video-index="${video_index}">

                                    <div class="col-md-6">
                                        <h2 style="font-weight: 800">${'Video ' + (video_index+1)}</h2>

                                        <div style="position: relative;display: inline-block;">
                                            <video width="400" controls="true">
                                                <source src="${video.url}">
                                            </video>
                                            <span class="remove-video">X</span>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <br>
                                        <h2><strong>Video Comments</strong></h2>

                                        <div class="comments-wrapper">
                                            <div>
                                                ${video.comments.map((comment, comment_index) => {
                                                    return /* html */ `
                                                        <div
                                                            data-comment-index="${comment_index}"
                                                            style="position: relative;margin-bottom: 20px;">
                                                                <p class="video-comment-p">${(comment_index+1) + '. ' + comment}</p>
                                                                <span class="remove-comment">X</span>
                                                        </div>
                                                    `;
                                                }).join("")}
                                            </div>
                                        </div>


                                        <hr style="width: 100%;height: 3px;color: black;border-color: beige;border-color: black;background: black;">

                                        <div>
                                            <p>Add new Comment:</p>
                                            <textarea class="video-text-area" style="height: 80px"></textarea><br>
                                            <button class="add-new-video-comments"> Add New </button>
                                        </div>

                                    </div>

                                </div>
                            `;

                        }).join("")}

                    </div>

                `;

                $(wrapper).html(html);

            }

            const _applyBinds = () => {
                // images media

                // add tag
                $(".order-img").on("click", (event) => {
                    const type = $(event.target).closest(".image-tag-parent").data("imageType");
                    const index = $(event.target).closest(".image-tag-parent").data("imageIndex");
                    const images = this.selectedCustomerFromOrder.reorder_data.images;
                    let image = images[index];


                    const self = this;
                    $.confirm({
                        title: 'New tag',
                        columnClass : 'large',
                        content: /* html */`
                            <form id="new-tag-form">
                                <span> Description </span>
                                <br>
                                <textarea id="new-tag-description" style="width: 100%" rows="5"></textarea>
                            </form>`,
                        buttons: {
                            confirm: {
                                text: 'Confirm',
                                btnClass: 'btn-green',
                                action: function () {
                                    var input = this.$content.find('textarea#new-tag-description');
                                    if (!input.val().trim()) {
                                        $.alert({
                                            title:'',
                                            content: "Please don't keep the description field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    } else {
                                        self.tagImage(image, input.val(), event);
                                    }
                                }
                            },
                            cancel: function () {
                                // do nothing.
                            }
                        }
                    });


                });

                $(".numberCircle").on("click", (event) => {
                    const imageType = $(event.target).closest(".image-tag-parent").data("imageType");
                    const imageIndex = $(event.target).closest(".image-tag-parent").data("imageIndex");
                    const tagIndex = $(event.target).data("tagIndex");
                    const tagPreviousText = $(event.target).data("tagText");

                    const self = this;
                    $.confirm({
                        title: 'Edit / Delete tag',
                        columnClass : 'large',
                        content: /* html */`
                            <form id="edit-delete-tag-form">
                                <span> Description </span>
                                <br>
                                <textarea id="new-tag-description" style="width: 100%" rows="5">${tagPreviousText}</textarea>
                            </form>`,
                        buttons: {
                            edit: {
                                text: 'Edit',
                                btnClass: 'btn-orange',
                                action: function () {
                                    var input = this.$content.find('textarea#new-tag-description');
                                    if (!input.val().trim()) {
                                        $.alert({
                                            title:'',
                                            content: "Please don't keep the description field empty.",
                                            type: 'red'
                                        });
                                        return false;
                                    } else {
                                        self.editTag(imageType, imageIndex, tagIndex, input.val());
                                    }
                                }
                            },
                            delete: {
                                text: 'Delete',
                                btnClass: 'btn-red',
                                action: function () {
                                    self.removeTag(imageType, imageIndex, tagIndex);
                                }
                            },
                            cancel: function () {
                                // do nothing.
                            }
                        }
                    });
                })

                $(".upload-img-btn").on("click", (event) => {
                    const type = $(event.target).data("imageType");
                    const index = $(event.target).data("imageIndex");

                    // while developing
                    this.choosePicSource(type);
                    // this.addImage(type, Utils.getRandomImage());
                });

                $(".remove-image").on("click", (event) => {
                    const type = $(event.target).closest(".image-tag-parent").data("imageType");
                    const index = $(event.target).closest(".image-tag-parent").data("imageIndex");

                    this.removeImage(type, index);
                });

                // video media
                $(".add-new-video-comments").on("click", (event) => {
                    const videoIndex = $(event.target).closest(".a-video").data("videoIndex");
                    const videoText = $(event.target).closest("div").find("textarea").val();

                    if (videoText == ""){
                        pos_error({msg: "comment can not be empty"});
                    }
                    else{
                        this.addNewVideoTag(videoIndex, videoText);
                    }
                });

                $(".remove-comment").on("click", (event)=>{
                    const videoIndex = $(event.target).closest(".a-video").data("videoIndex");
                    const commentIndex = $(event.target).parent().data("commentIndex");

                    this.removeVideoTag(videoIndex, commentIndex);
                });

                $(".upload-video-btn").on("click", (event)=>{
                    // while developing
                    this.chooseVideoSource();
                    // this.addNewVideo(Utils.getRandomVideo());
                })

                $(".remove-video").on("click", (event)=>{
                    const video_index = $(event.target).closest(".a-video").data("videoIndex");
                    this.removeVideo(video_index);
                })



            }

            _renderImages();
            _renderVideos();
            _applyBinds();
        }

        applyBinds(){
            $(".show-hide-images, .show-hide-videos").on("click", (event)=>{
                $(event.target).closest("button").next().toggle('slow');
            })

            $(".j-footer .save-btn").on("click", (event) => {
                // because we are no more using front/back/side types, we don't know if those images were uploaded.
                // So we only need to check if there is 3 images
                if (this.selectedCustomerFromOrder.reorder_data.images.length >= 3){
                    this.updateInBackend();
                } else {
                    pos_warning({msg: `Pleas upload at least 3 images`});
                }
                this.render();

            });
        }

        /**
         * Before we were get the images from crud_json of reorder.
         * But we need to get this medias from official table of medias in backend
         *
         * This method will replace this images
         */
        getMediaFromERP(){
            const url = "orders_pos/get_order_medias";
            console.log('data', data);

            let videos = this.selectedCustomerFromOrder.reorder_data.videos;

            $.ajax({
                type: 'POST',
                url: BUrl+url,
                dataType: 'json',
                async: true,
                data: {
                    "user": authCtrl.userInfo,
                    "order_id": this.selectedCustomer.order_id,
                },
                success: (dataS) => {

                    const _fixImages = () =>{
                        console.log('dataS '+url, dataS);
                        this.selectedCustomerFromOrder.reorder_data.images = dataS.images.map((image, index) => {
                            const pos_image = this.getNewImage("", "");

                            // fix image path
                            // NOTE: the flag is_sync are changed: 0 means true, and 1 means false
                            if (image.is_sync == 0){
                                pos_image.url = window.BUrl + image.image_path
                            }else{
                                pos_image.url = image.image_local_path
                            }

                            // fix image tags
                            pos_image.tags = JSON.parse(image.image_tags).map(tag => {
                                return this.getNewCurrentAddingTag(tag.text, tag.left / 100, tag.top / 100);
                            });

                            return pos_image;
                        });

                    }

                    const _fixVideos = () => {
                        this.selectedCustomerFromOrder.reorder_data.videos = dataS.videos.map((video) => {
                            const pos_video = this.getNewVideo("");

                            // is video sync (remember ERP have flags changed (0 == true and 1 == false))
                            if(video.is_sync == 0){
                                pos_video.url = window.BUrl + video.media_path;
                            } else {
                                pos_video.url = video.image_local_path;
                            }

                            // now get comments
                            pos_video.comments = JSON.parse(video.media_note);

                            return pos_video;

                        });
                    }

                    _fixImages();
                    _fixVideos();


                },
                error: function (dataS) {
                    console.error("Could not get order payment data", dataS);
                    pos_error({ msg: "Something wrong with " + url });
                },
            });
        }


        updateInBackend(){
            const url = "orders_pos/update_reorder_media";
            const data = this.buildSubmissionData();

            console.log('data', data);

            pos_confirm({msg:"Update customer media info ?", confirmAction: () => {

                document.getElementById('loading_jp').style.display = "block";

                $.ajax({
                    type: 'POST',
                    url: BUrl+url,
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        "data": JSON.stringify(data),
                    },
                    success: function (dataS) {

                        document.getElementById('loading_jp').style.display = "none";

                        pos_ok({ msg: "Medias updated successfully" });
                        pendingGroupOrders.getPendingOrdersAjax(); // pendingGroupOrders is global
                        pendingGroupOrders.whatIsMissingPopUp.steps(pendingGroupOrders.whatIsMissingPopUp.getPendingSteps()); // refresh steps status
                        $("#reorder_specifics_steps_wrapper .generic-pop-up").hide("fast");;
                    },
                    error: function (dataS) {

                        document.getElementById('loading_jp').style.display = "none";

                        pos_error({ msg: "Something wrong with " + url });
                    },
                });

            }});



        }

        buildSubmissionData(){
            let images = this.selectedCustomerFromOrder.reorder_data.images;
            const videos = this.selectedCustomerFromOrder.reorder_data.videos;

            images.forEach(image => {
                // and also, rename tags structure (ERP sent a format but expects another):
                // [{posX, posY, text}] => [{left, top, text}]
                image.tags = image.tags.map(tag => {
                    return {
                        left: parseFloat(tag.posX)*100,
                        top: parseFloat(tag.posY)*100,
                        text: tag.text,
                    }
                });
            });

            return {
                images: images,
                videos: videos,
                customer_id: this.selectedCustomer.customer_id,
                order_id: this.selectedCustomer.order_id,
            }
        }
    }



});