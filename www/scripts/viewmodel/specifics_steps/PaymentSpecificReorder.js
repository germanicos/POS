
/*
require.undef('viewmodel/specifics_steps/PaymentSpecificReorder');
require(['viewmodel/specifics_steps/PaymentSpecificReorder']);
*/
define(['jquery', 'knockout', 'base'], function($, ko) {

    /**
     *
     * Must be developed in vanillaJs and jQuery only (no knockout)
     *
     * @param {*} wrapper - string: where to load the view
     *
     */
    PaymentSpecificReorder  = class {
        constructor(wrapper, order, selectedCustomer) {
            $(wrapper).load('reorder_steps/specifics_steps/specific_payment_reorder.htm'); // load view

            this.wrapper = wrapper;
            this.order = order;
            this.selectedCustomer = selectedCustomer; // basic info of customer
            this.selectedCustomerFromOrder = order.customers.find(cus => cus.customer_id == selectedCustomer.customer_id); // detailed customer

            this.pricing = this.order.pricing;
            this.defaultGST = 10; // 10%

            // locations
            this.cities = dsRegistry.getDatasource('citiesDS').getStore();
    		this.states = dsRegistry.getDatasource('statesDS').getStore();
    		this.countries = dsRegistry.getDatasource('countriesDS').getStore();

            this.banks = ['ANZ', 'COM', 'STG'];

            console.log('PaymentSpecificReorder', this);

            this.getPaymentFromERP();

            // give time to page be loaded
            sleep(1500).then(() => {
                this.render();
                this.applyBinds();
                $('#loading_jp').hide();
            });

        }

        // costs ================================
        getRegularCostObj() {

            return {
                'description' : '',
                'value' : 0.0,
                'GST' : 10, // Default GST 10 %
                'price_range_id' : "",
                'garment_unique_id' : "" // From which garment the cost is coming -- This is is artificial (just used before the regular id is created)
            };

        }

        getExtraCostObj(description = '', value = 0.0) {
            return {
                'description': ko.observable(description),
                'value': ko.observable(value)
            };
        }

        getCostPriceRangeId(priceCategory, fabricRange) {
            try {
                var self = this;

                console.log("priceCategory", priceCategory);
                console.log("fabricRange", fabricRange);

                // Convert the prices strcutre in just one array of prices
                const allPricesValues = Object.keys(this.pricing).map(function(objectKey) { return self.pricing[objectKey]; });

                const flatenPrices = allPricesValues.reduce((acc, val) => acc.concat(val), []);

                // Finds the correct price
                const price = flatenPrices.filter( e => e.category.toLowerCase() === priceCategory.toLowerCase() && e.name.toLowerCase() === fabricRange.toLowerCase() );


                if( price.length )
                {
                    return price[0].id;
                }
                else
                {
                    return false;
                }

            } catch (error) {
                console.error("Error in getCostPriceRangeId", error);
                return false;

            }


        }

        getRegularCostsList(customer = this.selectedCustomerFromOrder){
            return customer.reorder_data.payments.regular_costs;
        }

        getExtraCostsList(customer = this.selectedCustomerFromOrder){
            return customer.reorder_data.payments.extra_costs;
        }

        addExtraCost(){
            const _addExtraCost = (description, value) => {
                const new_extra = this.getExtraCostObj();
                new_extra.description = description;
                new_extra.value = parseFloat(value);

                this.selectedCustomerFromOrder.reorder_data.payments.extra_costs.push(new_extra);
                this.render();
            };

            $.dialog({
                title: "Adding new Extra Cost",
                content: /* html */`
                <form id="new-extra-cost">
                    <span>Extra cost description</span>
                    <input type="text" id="new-extra-cost-description" />
                    <br>

                    <span>Extra cost value</span>
                    <input type="number" id="new-extra-cost-value" />
                    <br>

                    <button type="button" id="cancel"> Cancel </button>
                    <button type="submit"> Add </button>
                </form>`,

                onOpen: function () {
                    var thisDialog = this;
                    this.$content.find('form').submit(function (evt) {
                        evt.preventDefault();
                        const description = $(this).find("input#new-extra-cost-description").val();
                        const value = $(this).find("input#new-extra-cost-value").val();

                        if (description == ""){
                            pos_alert({msg:"Error name can not be empty"});
                            return;
                        }

                        if(!value){
                            pos_alert({msg:"Value can not be empty"});
                            return;
                        }

                        _addExtraCost(description, value);

                        thisDialog.close();
                    })

                    this.$content.find('form button#cancel').on("click", (event) => {
                        thisDialog.close();
                    })
                },

                columnClass: 'large',
            })

        }

        removeExtraCost(extraCostId){
            this.selectedCustomerFromOrder.reorder_data.payments.extra_costs.splice(extraCostId, 1);
            this.render();
        }

        // payment =====================
        addCCImage(url) {
            const paymentStep = this.selectedCustomerFromOrder.reorder_data.payments;
            paymentStep.cc_image = (url);

            // update in the view
            const html = /* html */`<img src="${paymentStep.cc_image ? paymentStep.cc_image : ""}" style="width: 19%;" >`
            $(".credit-card-wrapper .cc-image-wrapper").html(html);
        }

        chooseCCPicSource() {

            var self = this;

            console.log("choosing Pic source...");

            navigator.notification.confirm(
                'CC Picture Source', // message
                function (btnIndex) {
                    self._takeCCPic(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

            this._takeCCPic()

        }

        _takeCCPic(buttonIndex) {

			console.log("Taking CC image...");

            var self = this;

            const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            });

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                pos_error({ msg:'Failed because: ' + message, title:'Error'});
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };

            function successMove(entry) {
                console.log('After move');
                self.addCCImage(entry.toURL());
            };

            function resOnError(error) {
                pos_error({ msg:"Error Writing the file....", title:'Error'});
                console.log("Failed... check console");
                console.log(error);
            };


		}


        // for locations ================================
        copyFittingCity(){
            pos_alert({msg: 'copyFittingCity In development'});
        }


        // signature ================================
        saveCanvas() {
            console.log("Saving canvas...");
            const canvas = document.getElementById('canvasel-' + this.selectedCustomer.customer_id);


            const signatureObject = {
                image_path : canvas.toDataURL("image/png", 0.6),
                is_local_only : true,
            }

            this.selectedCustomerFromOrder.reorder_data.signature = signatureObject;

            this.submitSignature();

        }

        prepareCanvas() {

            this.selectedCustomerFromOrder.reorder_data.payments.signature_id = null;

            console.log("preparing canvas...");

            const divId = "canvas-" + this.selectedCustomer.customer_id;

            let canvasDiv = $("#" + divId).find("#canvasDiv-" + this.selectedCustomer.customer_id);

            let canvas = document.createElement('canvas');

            canvas.setAttribute('width', 710);
            canvas.setAttribute('height', 350);
            canvas.setAttribute('id', 'canvasel-' + this.selectedCustomer.customer_id);

            canvasDiv.html(canvas);
            if (typeof G_vmlCanvasManager != 'undefined') {
                canvas = G_vmlCanvasManager.initElement(canvas);
            }
            context = canvas.getContext("2d");

            clickX = new Array();
            clickY = new Array();
            clickDrag = new Array();
            context.clearRect(0, 0, canvas.width, canvas.height); // Clears the canvas

            canvas.addEventListener('mousedown', _mousedown2, false);
            canvas.addEventListener('mousemove', _mousemove2, false);
            canvas.addEventListener('mouseup', _mouseup, false);

            canvas.addEventListener('touchstart', _touchstart2, false);
            canvas.addEventListener('touchmove', _touchmove2, false);

            $("#" + divId).show();

        }

        submitSignature(){
            const self = this;

            const paymentStep = this.selectedCustomerFromOrder.reorder_data.payments;

            console.log('Saving Signature...');

            // using old endpoint for signature image
            const base64 = this.selectedCustomerFromOrder.reorder_data.signature.image_path.split(',')[1];
            const customer_id = this.selectedCustomer.customer_id;

            const salesman = { "username": authCtrl.username(), "password": authCtrl.password(), "id": authCtrl.userInfo.user_id };

            // get order_id to link this signature to this order.
            const order_id = this.selectedCustomer.order_id;

            $("loading_jp").show();

            $.ajax({
                type: 'POST',
                url: BUrl + 'client_image_upload/upload_signature_image',
                dataType: 'json',
                async: false,
                data: {
                    salesman: JSON.stringify(salesman),
                    customer_id: customer_id,
                    data: base64,
                    order_id: order_id
                },
                success: function (dataS) {
                    paymentStep.signature_id = dataS.id;
                    self.selectedCustomerFromOrder.reorder_data.signature = {
                        signature_id : dataS.id,
                        image_path: dataS.image
                    };

                    console.log(dataS);
                    $.jGrowl('Signature Saved successfully !!');
                    self.render();
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("Problem with signature !");
                },
                complete: function(data) {
                    $("loading_jp").hide();
                }
            });
        }

        // terms ========================

        /**
         * hide/show terms
         */
        toggleTerms(){
            $(".terms-wrapper").toggle();
        }

        //  end ====================

        /**
         * Method to update view
         */
        render(){

            // set initial values in the view
            const _setInitialValues = () => {
                const paymentStep = this.selectedCustomerFromOrder.reorder_data.payments;


                // ===========order breakdown===============
                // include invoice company email
                $("#include-company-invoice").prop('checked', paymentStep.include_company_invoice);
                if (paymentStep.include_company_invoice){
                    $(".invoice-company-included").show();
                }else {
                    $(".invoice-company-included").hide();
                }

                $(".company_name").val(paymentStep.invoice_company_name)
                $(".product_description").val(paymentStep.invoice_company_name)

                // mail_customer_invoice
                $("#mail-customer-incoive").prop('checked', paymentStep.mail_customer_invoice);

                // future order
                $("#future-order").prop('checked', paymentStep.is_future_order);

                // special date
                $("#special-event-input").prop('checked', this.selectedCustomerFromOrder.reorder_data.special_date.is_specialEvent);
                $(".special-date-input").val(this.selectedCustomerFromOrder.reorder_data.special_date.is_specialEventDate);


                // =========deposit details============

                // payment method:
                // if payment method was not inputed
                if (!paymentStep.payment_method) {
                    paymentStep.payment_method = {
                        id: null
                    }
                }

                $("select#paymentMethod").html("");
                for (const paymentMethod of this.order.payment_methods) {
                    $("select#paymentMethod").append($('<option>', {
                        value: `${paymentMethod.id}`,
                        text: `${paymentMethod.name}`,
                        selected: (paymentStep.payment_method.id == paymentMethod.id),
                    }));
                }

                // if is bank
                if (paymentStep.payment_method.id == "2"){
                    // show bank options
                    $(".bank-transfer").show();

                    // set initial values
                    $(`#bank-transfer-select option[value='${paymentStep.selected_bank}']`).attr("selected", "selected");
                }
                else{
                    $(".bank-transfer").hide();
                }

                // if is eftpos, creditcard vm, creditcard amex
                if (["3", "4", "5"].includes(paymentStep.payment_method.id)){
                    // show credit card image
                    $(".credit-card-wrapper").show();


                    // initial value
                    const html = /* html */`<img src="${paymentStep.cc_image ? paymentStep.cc_image : ""}" style="width: 19%;" >`
                    $(".credit-card-wrapper .cc-image-wrapper").html(html);
                }

                else{
                    $(".credit-card-wrapper").hide();
                }


                // deposit value
                $("#deposit-value").val(paymentStep.deposit_value);

                // Deposit comments
                $("#deposit-comments").val(paymentStep.deposit_comments);


                // =======location==================

                const selectedOrderCityId = paymentStep.order_city;
                const selectedOrderCountryId = Utils.getCountryByCityId(selectedOrderCityId).locations_id;
                const selectedOrderStateId = Utils.getStateByCityId(selectedOrderCityId).locations_id;

                $("#orderCountry").html("");
                for (const country of this.countries) {
                    $("#orderCountry").append($('<option>', {
                        value: `${country.locations_id}`,
                        text: `${country.location_name}`,
                        selected: (selectedOrderCountryId == country.locations_id),
                    }));
                }


                $("#orderState").html("");
                for (const state of this.states.filter(state => state.parent_location_id == selectedOrderCountryId)) {
                    $("#orderState").append($('<option>', {
                        value: `${state.locations_id}`,
                        text: `${state.location_name}`,
                        selected: (selectedOrderStateId == state.locations_id),
                    }));
                }


                $("#orderCity").html("");
                for (const city of this.cities.filter(city => city.parent_location_id == selectedOrderStateId)) {
                    $("#orderCity").append($('<option>', {
                        value: `${city.locations_id}`,
                        text: `${city.location_name}`,
                        selected: (selectedOrderCityId == city.locations_id),
                    }));
                }

                const selectedFittingCityId = paymentStep.fitting_city;
                const selectedFittingCountryId = Utils.getCountryByCityId(selectedFittingCityId).locations_id;
                const selectedFittingStateId = Utils.getStateByCityId(selectedFittingCityId).locations_id;

                $("#fittingCountry").html("");
                for (const country of this.countries) {
                    $("#fittingCountry").append($('<option>', {
                        value: `${country.locations_id}`,
                        text: `${country.location_name}`,
                        selected: (selectedFittingCountryId == country.locations_id),
                    }));
                }

                $("#fittingState").html("");
                for (const state of this.states.filter(state => state.parent_location_id == selectedFittingCountryId)) {
                    $("#fittingState").append($('<option>', {
                        value: `${state.locations_id}`,
                        text: `${state.location_name}`,
                        selected: (selectedFittingStateId == state.locations_id),
                    }));
                }


                $("#fittingCity").html("");
                for (const city of this.cities.filter(city => city.parent_location_id == selectedFittingStateId)) {
                    $("#fittingCity").append($('<option>', {
                        value: `${city.locations_id}`,
                        text: `${city.location_name}`,
                        selected: (selectedFittingCityId == city.locations_id),
                    }));
                }

                $("#location-comments").val(paymentStep.order_notes);

                // terms ===========
                if(paymentStep.accept_terms){
                    $(".termsbutton").css({'background-color': 'var(--green1)', 'color':'white'});
                }
                else{
                    $(".termsbutton").css({'background-color': '#f29100 none repeat scroll 0 0', 'color':'#3c3c3b'});
                }

            };


            // component for list of orders
            const _listOfCosts = () => {
                const paymentStep = this.selectedCustomerFromOrder.reorder_data.payments;
                const regularCostsLists = this.getRegularCostsList();
                const extraCostsLists = this.getExtraCostsList();
                // const costsLists = regularCostsLists.concat(extraCostsLists);


                const getTotalWithoutGST = () => {
                    let total = 0;
                    for (const cost of regularCostsLists) {
                        total += cost.value;
                    }
                    for (const cost of extraCostsLists) {
                        total += cost.value;
                    }

                    const discount_value = parseFloat(this.selectedCustomerFromOrder.reorder_data.payments.discount_value) ? parseFloat(this.selectedCustomerFromOrder.reorder_data.payments.discount_value) : 0.00;
                    paymentStep.total_order_costs = total + discount_value;
                    return  paymentStep.total_order_costs;
                }

                const getTotalWithGST = () => {
                    return getTotalWithoutGST() + getTotalWithoutGST() * parseFloat(this.defaultGST)/100;
                }

                const html = /* html */`
                    <p>Order Costs:</p>

                        <h3>${this.selectedCustomer.customer_name}</h3>
                        <div>
                            <div class="row">
                                ${regularCostsLists.map(cost => {
                                    return /* html */`
                                    <div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-8">
                                            <p class="cost-description">${cost.description}</p>
                                            <span class="filler"></span>
                                        </div>

                                        <div class="col-md-3">
                                            <strong><p class="cost-description">$ ${cost.value}</p></strong>
                                        </div>
                                    </div>
                                    `;
                                }).join("")}
                            </div>

                            <div class="row">
                                ${extraCostsLists.map((cost, index) => {
                                    return /* html */`
                                    <div>
                                        <div class="col-md-1">
                                            <span class="remove-extra-cost" data-cost-index="${index}">x</span>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="cost-description">${cost.description}</p>
                                            <span class="filler"></span>
                                        </div>

                                        <div class="col-md-3">
                                            <strong><p class="cost-description">$ ${cost.value}</p></strong>
                                        </div>
                                    </div>
                                    `;
                                }).join("")}
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-offset-6 col-md-6" style="font-size: 15px">

                                <!-- in case of discount -->
                                ${(this.selectedCustomerFromOrder.reorder_data.payments.discount_code) ? /* html */`
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>Discount Code</p>
                                        </div>
                                        <div>
                                            <p> - $ ${this.selectedCustomerFromOrder.reorder_data.payments.discount_value}</p>
                                        </div>
                                    </div>
                                ` : ``}

                                <!-- Total costs (NO GST) -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Subtotal: </p>
                                    </div>
                                    <div class="col-md-6">
                                        <p> $ ${getTotalWithoutGST()} </p>
                                    </div>
                                </div>

                                <!-- Just the cost of GST -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Sales Tax (GST 10%):</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>$ ${getTotalWithoutGST() * parseFloat(this.defaultGST)/100}</p>
                                    </div>
                                </div>

                                <!-- Total costs + GST -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Purchase Total:</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p>$ ${getTotalWithGST()}</p>
                                    </div>
                                </div>

                            </div>

                        </div>

                `;

                $(".list-of-costs-wrapper").html(html);

            }

            // render for signature
            const _signature = () => {
                const signatureObject = this.selectedCustomerFromOrder.reorder_data.signature;
                let html = "";
                // have signature
                if (signatureObject && !Array.isArray(signatureObject)){
                    this.selectedCustomerFromOrder.reorder_data.payments.signature_id = signatureObject.signature_id;
                    const signaturePath = signatureObject.is_local_only ? signatureObject.image_path : BUrl+signatureObject.image_path;
                    html = /* html */`
                        <!-- signature saved -->
                        <div>
                            <h2>Signature Saved !</h2>
                            <div class="col-md-8">
                                <img src="${signaturePath}">
                            </div>
                            <div class="col-md-4">
                                <button class="signature-btn prepare-canvas">Sign Again ?</button>
                            </div>
                        </div>
                    `
                    $(".signature-wrapper").html(html);
                }
                else {
                    html = /* html */`
                        <!-- prepare signature -->
                        <div class="">
                            <div  id="canvas-${this.selectedCustomer.customer_id}">
                                <h2>Finalize</h2>
                                <span>
                                    Please ${this.selectedCustomer.customer_name}, sign your order and accept the terms and conditions.
                                </span>
                                <div class="grid_9">
                                    <div style="margin: 0px; width: 710px; height:350px; z-index:500">
                                        <div id="canvasDiv-${this.selectedCustomer.customer_id}" style=" height:350px; margin: 0 auto; width: 710px; background:#ececec; border-radius:9px"></div>
                                    </div>
                                </div>
                                <div class="grid_3" style="position: relative;left: 45px">
                                    <div id="saveDiv" class="row">
                                        <button class="signature-btn save-signature canvassave" style="margin-top:0PX !important;">Save</button>
                                    </div>

                                    <br style="clear: both">
                                    <div id="clearDiv" class="row">
                                        <button class="prepare-canvas signature-btn clear-signature">Clear</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `
                    $(".signature-wrapper").html(html);
                    this.prepareCanvas();
                }

            }

            // apply binds for this elements recent created
            const _applyBinds = ()=> {
                $(".remove-extra-cost").on("click", (evt) => {
                    const target = evt.target;

                    const extraCostIndex = $(target).data("costIndex");


                    this.removeExtraCost(extraCostIndex);
                });

                $(".prepare-canvas").on("click", (event) => {
                    this.selectedCustomerFromOrder.reorder_data.signature = null;
                    this.render();
                    this.prepareCanvas();
                });

                $(".canvassave").on("click", (event) => {
                    this.saveCanvas();
                });
            }

            _listOfCosts();
            _setInitialValues();
            _signature();
            _applyBinds();


        }

        /**
         * Events for elements of www/reorder_steps/specifics_steps/specific_payment_reorder.htm
         */
        applyBinds(){
            // extra cost
            $(".add-extra-cost-btn").on("click", (event) => {
                this.addExtraCost();
            });

            // show/hide company invoice details
            $("#include-company-invoice").on("change", event => {
                const new_val = (event.target).checked;
                console.log('include-company-invoice new_val', new_val);

                if (new_val) {
                    $(".invoice-company-included").show();
                }else {
                    $(".invoice-company-included").hide();
                }
            });


            // show/hide terms
            $(".termsbutton").on("click", (event)=>{
                this.toggleTerms();
            });

            $(".accept-terms").on("click", (event) => {
                this.selectedCustomerFromOrder.reorder_data.payments.accept_terms = true;
                this.toggleTerms();
                this.render();
            });
            $(".decline-terms").on("click", (event) => {
                this.selectedCustomerFromOrder.reorder_data.payments.accept_terms = false;
                this.toggleTerms();
                this.render();
            });

            // filter orderState select
            $("#orderCountry").on("change", (event) => {
                const new_value = $(event.target).val();

                $("#orderState").html("");
                $("#orderCity").html("");
                for (const state of this.states.filter(state => state.parent_location_id == new_value)) {
                    $("#orderState").append($('<option>', {
                        value: `${state.locations_id}`,
                        text: `${state.location_name}`,
                        selected: false,
                    }));
                }
            });

            // filter orderCity select
            $("#orderState").on("change", (event) => {
                const new_value = $(event.target).val();

                $("#orderCity").html("");
                for (const city of this.cities.filter(city => city.parent_location_id == new_value)) {
                    $("#orderCity").append($('<option>', {
                        value: `${city.locations_id}`,
                        text: `${city.location_name}`,
                        selected: false,
                    }));
                }
            });

            // filter fittingState select
            $("#fittingCountry").on("change", (event) => {
                const new_value = $(event.target).val();

                $("#fittingState").html("");
                $("#fittingCity").html("");
                for (const state of this.states.filter(state => state.parent_location_id == new_value)) {
                    $("#fittingState").append($('<option>', {
                        value: `${state.locations_id}`,
                        text: `${state.location_name}`,
                        selected: false,
                    }));
                }
            });

            // filter fittingCity select
            $("#fittingState").on("change", (event) => {
                const new_value = $(event.target).val();

                $("#fittingCity").html("");
                for (const city of this.cities.filter(city => city.parent_location_id == new_value)) {
                    $("#fittingCity").append($('<option>', {
                        value: `${city.locations_id}`,
                        text: `${city.location_name}`,
                        selected: false,
                    }));
                }
            });

            // payment special methods
            /**
             * In case of bank transfer or credit card we must to show some new info
             */
            $("#paymentMethod").on("change", (event) => {
                const new_value = $(event.target).val();

                // update select
                const paymentStep = this.selectedCustomerFromOrder.reorder_data.payments;
                paymentStep.payment_method = this.order.payment_methods.find(payment_method => payment_method.id == new_value);

                this.render();

            });


            $(".upload-cc-img-btn").on("click", (event) => {
                this.chooseCCPicSource();
            })


            // submission
             $(".j-footer .save-btn").on("click", (event) => {
                 this.updateInBackend();
            });


        }

        /**
         * Before we were get the payment from crud_json of reorder.
         * But we need to get this data from official table
         *
         *
         */
        getPaymentFromERP(){

            const url = "orders_pos/get_order_payment_data";
            console.log('data', data);

            $.ajax({
                type: 'POST',
                url: BUrl+url,
                dataType: 'json',
                async: false,
                data: {
                    "user": authCtrl.userInfo,
                    "order_id": this.selectedCustomer.order_id,
                },
                success: (dataS) => {
                    console.log('dataS', dataS);
                    // TODO:
                    // ...
                    // get payment from reorder like a base
                    const newPaymentStep = this.selectedCustomerFromOrder.reorder_data.payments;

                    // deposit info
                    newPaymentStep.payment_method                 = this.order.payment_methods.find(payment_method => payment_method.name == dataS.first_payment_row.payment_method);;
                    newPaymentStep.cc_image                       = dataS.credit_card_image_row.is_sync == "0" ? window.BUrl + dataS.credit_card_image_row.image_path : dataS.credit_card_image_row.image_path ;
                    newPaymentStep.selected_bank                  = dataS.first_payment_row.bank;
                    newPaymentStep.deposit_value                  = dataS.first_payment_row.deposit;
                    newPaymentStep.deposit_comments               = dataS.first_payment_row.notes;

                    // location info
                    newPaymentStep.order_city                     = dataS.order_row.order_city;
                    newPaymentStep.fitting_city                   = dataS.order_row.fitting_city;
                    newPaymentStep.order_notes                    = dataS.order_row.notes;


                    // order breakdown (no need to show in the view)
                    // data.include_company_invoice     = ________;
                    // data.invoice_company_name        = ________;
                    // data.invoice_product_description = ________;
                    // data.mail_customer_invoice       = ________;
                    // data.is_future_order             = ________;

                    // regular cost does not changes
                    // ...

                    // extra costs does not changes
                    // ...

                    // newPaymentStep.accept_terms      = ________; // TODO: missing in the response
                    newPaymentStep.total_order_costs = parseFloat(dataS.order_row.total_amount) * (1 + parseFloat(dataS.order_row.GST)/100);

                    newPaymentStep.discount_code     = dataS.order_row.discount_code;
                    newPaymentStep.discount_value    = dataS.order_row.discount == "" ? 0 : dataS.order_row.discount;

                    // newPaymentStep.signature_id      = ________; // TODO: missing in the response

                    newPaymentStep.payment_id = dataS.first_payment_row.payment_id;


                    // force update payment
                    this.selectedCustomerFromOrder.reorder_data.payments = newPaymentStep;
                },
                error: function (dataS) {
                    console.error("Could not get order payment data", dataS);
                    pos_error({ msg: "Something wrong with " + url });
                },
            });
        }

        updateInBackend() {
            const url = "orders_pos/update_reorder_payment";
            const data = this.buildSubmissionData();

            pos_confirm({msg:"Update customer payment info ?", confirmAction: () => {

                document.getElementById('loading_jp').style.display = "block";


                if (this.checkForMandatoryFields(data)){
                    $.ajax({
                        type: 'POST',
                        url: BUrl + url,
                        dataType: 'json',
                        async: true,
                        data: {
                            "user": authCtrl.userInfo,
                            "data": JSON.stringify(data),
                        },
                        success: function (dataS) {

                            document.getElementById('loading_jp').style.display = "none";

                            pos_ok({ msg: "Payment updated successfully" });
                            pendingGroupOrders.getPendingOrdersAjax(); // pendingGroupOrders is global
                            pendingGroupOrders.whatIsMissingPopUp.steps(pendingGroupOrders.whatIsMissingPopUp.getPendingSteps()); // refresh steps status

                            // check if all steps are complete
                            if(pendingGroupOrders.whatIsMissingPopUp.areAllStepsComplete()){
                                pos_ok({msg: 'Group order completed successfully '});
                                $('#whatIsMissingPopup-wrapper .generic-pop-up').hide('fast');
                            }

                            $("#reorder_specifics_steps_wrapper .generic-pop-up").hide("fast");;
                        },
                        error: function (dataS) {

                            document.getElementById('loading_jp').style.display = "none";

                            pos_error({ msg: "Something wrong with " + url });
                        },
                    });
                }

            }});

        }

        buildSubmissionData() {
            const paymentStep = this.selectedCustomerFromOrder.reorder_data.payments;

            const data = JSON.parse(JSON.stringify(paymentStep)); // get old values and just overwrite values

            // deposit info
            data.payment_method                 = this.order.payment_methods.find(payment => payment.id == $("select#paymentMethod").val());
            data.cc_image                       = paymentStep.cc_image; // already updated
            data.selected_bank                  = $("select#bank-transfer-select").val();
            data.deposit_value                  = $("#deposit-value").val();
            data.deposit_comments               = $("#deposit-comments").val();

            // location info
            data.order_city                     = $("#orderCity").val();
            data.fitting_city                   = $("#fittingCity").val();
            data.order_notes                    = $("#location-comments").val();


            // order breakdown
            // data.include_company_invoice     = $("#include-company-invoice").prop('checked'); // no more need to show in this popup
            // data.invoice_company_name        = $(".company_name").val();                      // no more need to show in this popup
            // data.invoice_product_description = $(".product_description").val();               // no more need to show in this popup
            // data.mail_customer_invoice       = $("#mail-customer-incoive").prop('checked');   // no more need to show in this popup
            // data.is_future_order             = $("#future-order").prop('checked');            // no more need to show in this popup

            // regular cost does not changes
            // ...

            // extra costs already updated
            data.extra_costs = paymentStep.regular_costs;

            data.accept_terms      = paymentStep.accept_terms;
            data.total_order_costs = paymentStep.total_order_costs; // already updated

            // data.discount_code     = "TODO: "; //$("").val();
            // data.discount_value    = "TODO: "; //$("").val();

            data.signature_id    = paymentStep.signature_id; // already updated

            data.order_id     = this.selectedCustomer.order_id;
            data.payment_id   = paymentStep.payment_id;

            console.log('data', data);
            return data;
        }

        checkForMandatoryFields(submissionData){
            const mandatoryFields = [
                {key: "payment_method", error_msg: "Please, check if payment method"},
                {key: "deposit_value",  error_msg: "Please, check if deposit value"},
                {key: "order_city",     error_msg: "Please, check if order city"},
                {key: "fitting_city",   error_msg: "Please, check if fitting city"},
                {key: "accept_terms",   error_msg: "Please, check if customer accept the terms"},
                {key: "signature_id",   error_msg: "Please, check if customer sign in"},
            ];

            for (const mandatoryField of mandatoryFields) {
                if (!submissionData[mandatoryField.key]){
                    pos_warning({msg: mandatoryField.error_msg});
                    return false;
                }
            }

            return true;

        }
    }

});