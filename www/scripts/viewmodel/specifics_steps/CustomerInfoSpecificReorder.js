
/*
require.undef('viewmodel/specifics_steps/CustomerInfoSpecificReorder');
require(['viewmodel/specifics_steps/CustomerInfoSpecificReorder.]);
*/
define(['jquery', 'knockout', 'base'], function($, ko) {

    /**
     *
     * Must be developed in vanillaJs and jQuery only (no knockout)
     *
     * @param {*} wrapper - string: where to load the view
     *
     */
    CustomerInfoSpecificReorder  = class {
        constructor(wrapper, order, selectedCustomer) {
            $(wrapper).load('reorder_steps/specifics_steps/specific_customer_info_reorder.htm'); // load view

            this.wrapper = wrapper;
            this.order = order;
            this.selectedCustomer = selectedCustomer; // basic info of customer
            this.selectedCustomerFromOrder = order.customers.find(cus => cus.customer_id == selectedCustomer.customer_id); // detailed customer

            this.customerInfo = null; this.getCustomerInfo();


            // give time to page be loaded
            sleep(1500).then(() => {
                this.render();
                this.applyBinds();
                $('#loading_jp').hide();
            });

        }


        /**
         * Method to update view
         */
        render(){

            // set initial values in the view
            const _setInitialValues = () => {
                // text inputs
                $("input.detail-info-input").each((index, element)=>{
                    if ($(element).data("valueKey")){
                        $(element).val(this.customerInfo[$(element).data("valueKey")]);
                    }
                });

                // date of birth
                (()=>{
                    const dob = this.customerInfo.customer_DOB;
                    const [year, month, day] = dob.replace("/","-").replace(" ", "").split("-");

                    $("select#DOB_day").val(day);
                    $("select#DOB_month").val(month);

                    const availableYears = [];
                    for (i = 1930; i <= 2020; i++) availableYears.push(i.toString());
                    $("select#DOB_year").html(/* html */`${availableYears.map(availableYear => {
                        return /* html */`
                        <option value=${availableYear} ${year == availableYear ? 'selected' : ''} >${availableYear} </option>
                        `
                    }).join("")}`)
                })()

                // gender
                $('input:radio[name="gender"]').filter(`[value=${this.customerInfo.customer_gender}]`).attr('checked', true);

                // subscribe to newsletter
                $("#customer_email_subscription").attr("checked", !!parseInt(this.customerInfo.customer_email_subscription));

                // location
                (()=>{
                    const cities = dsRegistry.getDatasource("citiesDS").getStore();
                    const states = dsRegistry.getDatasource("statesDS").getStore();
                    const countries = dsRegistry.getDatasource("countriesDS").getStore();

                    const selectedOrderCountryId = this.customerInfo.customer_country;
                    const selectedOrderStateId = this.customerInfo.customer_state;
                    const selectedOrderCityId = this.customerInfo.customer_city;

                    $("#customer_country").html("");
                    for (const country of countries) {
                        $("#customer_country").append($('<option>', {
                            value: `${country.locations_id}`,
                            text: `${country.location_name}`,
                            selected: (selectedOrderCountryId == country.locations_id),
                        }));
                    }


                    $("#customer_state").html("");
                    for (const state of states.filter(state => state.parent_location_id == selectedOrderCountryId)) {
                        $("#customer_state").append($('<option>', {
                            value: `${state.locations_id}`,
                            text: `${state.location_name}`,
                            selected: (selectedOrderStateId == state.locations_id),
                        }));
                    }


                    $("#customer_city").html("");
                    for (const city of cities.filter(city => city.parent_location_id == selectedOrderStateId)) {
                        $("#customer_city").append($('<option>', {
                            value: `${city.locations_id}`,
                            text: `${city.location_name}`,
                            selected: (selectedOrderCityId == city.locations_id),
                        }));
                    }
                })()

            };

            _setInitialValues();

        }

        applyBinds(){

            // when something changes, update VM
            (()=>{
                // text inputs
                $("input.detail-info-input").on("change", (event) => {
                    const element = event.target;
                    const key = $(element).data("valueKey");
                    this.customerInfo[key] = $(element).val();
                });

                // location
                $("select.location").on("change", (event) => {
                    const element = event.target;
                    const key = $(element).data("valueKey");
                    this.customerInfo[key] = $(element).val();

                    // call render again so we can filter locations
                    this.render();
                });

                // DOB
                $("select.DOB").on("change", (event) => {
                    const element = event.target;
                    const key = $(element).data("valueKey");

                    const dob = this.customerInfo.customer_DOB;
                    let [year, month, day] = dob.replace("/","-").replace(" ", "").split("-");

                    if (key == "_day_"){
                        day = $(element).val();
                    }
                    else if (key == "_month_"){
                        month = $(element).val();
                    }
                    else if (key == "_year_"){
                        year = $(element).val();
                    }

                    this.customerInfo.customer_DOB = `${year}-${month}-${day}`;
                });

                // news letter
                $("#customer_email_subscription").on("change", event => {
                    const element = event.target;
                    this.customerInfo.customer_email_subscription = $(element).is(":checked") ? "1" : "0";
                })

                // gender
                $('input:radio[name="gender"]').on("change", (evt)=>{
                    this.customerInfo.customer_gender = $("input[name=gender]:checked").val();
                })
            })()


            // submission
             $(".j-footer .save-btn").on("click", (event) => {
                 this.updateInBackend();
            });

        }

        getCustomerInfo() {
            const url = "orders_pos/get_customer_info";
            $.ajax({
                type: 'POST',
                url: BUrl + url,
                dataType: 'json',
                async: false,
                data: {
                    "user": authCtrl.userInfo,
                    "customer_id": this.selectedCustomer.customer_id,
                },
                success: (dataS) => {
                    this.customerInfo = dataS.customer;
                },
                error: function (dataS) {
                    console.error("Could not get order customer info data", dataS);
                    pos_error({ msg: "Something wrong with " + url });
                },
            });
        }

        updateInBackend() {
            const url = "orders_pos/update_reorder_customer";
            const data = this.buildSubmissionData();

            if (!this.checkForMandatoryFields(data)){
                return;
            };

            pos_confirm({msg:"Update customer info ?", confirmAction: () => {

                document.getElementById('loading_jp').style.display = "block";

                $.ajax({
                    type: 'POST',
                    url: BUrl + url,
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        "data": JSON.stringify(data),
                    },
                    success: function (dataS) {

                        document.getElementById('loading_jp').style.display = "none";

                        pos_ok({ msg: "Customer updated successfully" });
                        pendingGroupOrders.getPendingOrdersAjax(); // pendingGroupOrders is global
                        pendingGroupOrders.whatIsMissingPopUp.steps(pendingGroupOrders.whatIsMissingPopUp.getPendingSteps()); // refresh steps status
                        $("#reorder_specifics_steps_wrapper .generic-pop-up").hide("fast");
                    },
                    error: function (dataS) {

                        document.getElementById('loading_jp').style.display = "none";

                        pos_error({ msg: "Something wrong with " + url });
                    },
                });


            }});

        }

        buildSubmissionData() {
            // for this step just get this.customerInfo
            return this.customerInfo;
        }

        checkForMandatoryFields(submissionData){
            // for now we are accepting everything
            const mandatoryFields =[
                {key: "customer_first_name",   error_msg: "Please, check if customer first name is filled"   },
                {key: "customer_last_name",    error_msg: "Please, check if customer last name is filled"    },
                {key: "customer_DOB",          error_msg: "Please, check if customer DOB is filled"          },
                // {key: "customer_occupation",   error_msg: "Please, check if customer occupation is filled"   },
                {key: "customer_address1",     error_msg: "Please, check if customer address1 is filled"     },
                {key: "customer_address2",     error_msg: "Please, check if customer address2 is filled"     },
                {key: "customer_postal_code",  error_msg: "Please, check if customer post code is filled"    },
                {key: "customer_country",      error_msg: "Please, check if customer country is filled"      },
                {key: "customer_state",        error_msg: "Please, check if customer state is filled"        },
                {key: "customer_city",         error_msg: "Please, check if customer city is filled"         },
                {key: "customer_mobile_phone", error_msg: "Please, check if customer mobile phone is filled" },
                {key: "customer_email",        error_msg: "Please, check if customer email is filled"        },
            ];

            for (const mandatoryField of mandatoryFields) {
                if (!submissionData[mandatoryField.key]){
                    pos_warning({msg: mandatoryField.error_msg});
                    return false;
                }
            }

            // special case: DOB
            let DOB_test = true;
            submissionData.customer_DOB.split("-").forEach(DOB_element => {
                if (!parseInt(DOB_element)) {
                    DOB_test = false;
                    return false;
                }
            })
            if (!DOB_test) {
                pos_warning({ msg: "Please, check if customer DOB is filled" });
                return false
            }

            return true;
        }
    }

});