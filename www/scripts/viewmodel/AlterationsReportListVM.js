define(['jquery', 'knockout', 'base',], function($, ko, base) {

    window.AlterationsReportListVM = class {
        constructor(is_global=false) {
            console.log("Init alterations report list VMs")

            changeVisibilityDiv('loading_jp');

            this.alterationsList = ko.observableArray();
            this.currentCustomer = ko.observable("");

            this.url_to_get_data = is_global ? "alterations_pos/get_global_alteratons_report_list" : "alterations_pos/get_alteratons_report_list"

            this.getAlterationDetails();


        }

        getAlterationDetails(){
            // POST to the server to the get alteration details
            $.ajax({
                type: 'POST',
                url: BUrl + this.url_to_get_data,
                dataType: 'json',
                data: { "user": authCtrl.userInfo },
                async: true,
                success: (ret) => {

                    console.log(ret);

                    changeVisibilityDiv('loading_jp');

                    console.log("Alt report list", ret);

                    for (let i = 0; i < ret.length; i++) {
                        this.alterationsList.push(ret[i]);
                    }


                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#alterationsDashBoard");


                }
            });

        }

        chooseCustomer(customer) {
            console.log("changing customer", customer);
            this.currentCustomer(customer)
        }

        chooseReport(fitting_id) {

            console.log("Report for fitting: ", fitting_id);
            window.alterationsReportSubmissionVM = new AlterationsReportSubmissionVM( {
                "fitting_id" : fitting_id
            });

            posChangePage("#AlterationReportSubmission");
        }
    }

});