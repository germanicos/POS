define(['jquery', 'knockout', 'base'], function ($, ko) {

    /**
     * A View and a ViewModel to change global variables on-the-fly
     */
    ConfigGlobalVarsVM = class {

        constructor() {

            this.base_url = this.getBaseURL();
            this.images_3D = this.getRootForImage3D();

        }

        getBaseURL() {

            switch (ConfigGlobalVarsVM.env) {
                case "production":
                    // production server
                    window.BUrl = "https://www.shirt-tailor.net/thepos/"; //customAlert("Production Server!!!");
                    break;
                case "luiz-test":
                    // test server
                    window.BUrl = "http://www.shirt-tailor.net/test_server/"; alert("Test server");
                    break;
                case "luiz-mac":
                    window.BUrl = `http://${ConfigGlobalVarsVM.ip}:8888/thepos/`; alert("localhost");
                    break;

                case "igor":
                    window.BUrl = "http://localhost:8888/thepos/";
                    break;
                case "luciano-test":
                    window.BUrl = "https://www.shirt-tailor.net/test_server/";
                    break;

                default:
                    break;
            }

            return window.BUrl;
        }

        getRootForImage3D() {
            // window.rootFolderImage3D_v2 = window.rootFolderImage3D_v2 ? window.rootFolderImage3D_v2 : "/Users/yiannisdimitrakis/Germanicos Team Dropbox/3D_used_on_POS-for_igot_and_luiz/3D"; // temporary: local dropbox

            switch (ConfigGlobalVarsVM.env) {
                case "production":
                    // production server
                    window.rootFolderImage3D_v2 = "http://www.shirt-tailor.net/test_server/appimg/3D"; //customAlert("Production Server!!!");
                    break;
                case "luiz-test":
                    // test server
                    window.rootFolderImage3D_v2 = "";
                    break;
                case "luiz-mac":
                    window.rootFolderImage3D_v2 = '/Users/usuario/Germanicos Team Dropbox/3D_used_on_POS-for_igot_and_luiz/3d';
                    break;

                case "igor":
                    window.rootFolderImage3D_v2 = "/Users/igorlealantunes/Germanicos Team Dropbox/3D_used_on_POS-for_igot_and_luiz/3d";
                    break;
                case "luciano-test":
                    window.rootFolderImage3D_v2 = "https://www.shirt-tailor.net/test_server/appimg/3D";
                break;

                default:
                    break;
            }
            return window.rootFolderImage3D_v2;
        }

        updateGlobalVars() {

            console.log('this.base_url', this.base_url);
            console.log('this.images_3D', this.images_3D);


            window.BUrl = this.base_url;
            window.rootFolderImage3D_v2 = this.images_3D;

            alert("Updated!");
        }




    };

    // ConfigGlobalVarsVM.env = "luiz-test";
    // ConfigGlobalVarsVM.env = "luiz-mac";
    // ConfigGlobalVarsVM.env = "production";
    // ConfigGlobalVarsVM.env = "igor";
    ConfigGlobalVarsVM.env = "luciano-test";

    ConfigGlobalVarsVM.ip = "localhost";
});







