define(['jquery', 'knockout', 'base'], function($, ko) {
FitlinesJacket = SimpleControl.extend({
	init: function(DsRegistry, oI, tableindex){//, fromcached) {
		var self = this;
		this._super( DsRegistry );
		this.orderItemMirror = oI;
		
	//	if(fromcached == true){
		try{
			this.jacketAcrossBack     			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketAcrossBack);					//= ko.observable('');		this.jacketAcrossBack.subscribe(function() { self.flushModel(); });
			this.jacketAcrossShoulder     		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketAcrossShoulder);					//= ko.observable('');		this.jacketAcrossShoulder.subscribe(function() { self.flushModel(); });
			this.jacketBackPanelAtXBack     	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelAtXBack);						//= ko.observable('');		this.jacketBackPanelAtXBack.subscribe(function() { self.flushModel(); });
			this.jacketBackPanelBottomEdge		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelBottomEdge);						//= ko.observable('');		this.jacketBackPanelBottomEdge.subscribe(function() { self.flushModel(); });		
			this.jacketBackPanelChest			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelChest);					//= ko.observable('');		this.jacketBackPanelChest.subscribe(function() { self.flushModel(); });
			this.jacketBackPanelHip				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelHip);						//= ko.observable('');		this.jacketBackPanelHip.subscribe(function() { self.flushModel(); });		
			this.jacketBackPanelTrueWaist		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelTrueWaist);						//= ko.observable('');		this.jacketBackPanelTrueWaist.subscribe(function() { self.flushModel(); });
			this.jacketFitlineBicep				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBicep);					//= ko.observable('');		this.jacketFitlineBicep.subscribe(function() { self.flushModel(); });
			this.jacketCenterBackLength			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketCenterBackLength);				//= ko.observable('');		this.jacketCenterBackLength.subscribe(function() { self.flushModel(); });
			this.jacketFitlineComments			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketComments);			//= ko.observable('');		this.jacketFitlineComments.subscribe(function() { self.flushModel(); });
			this.jacketElbow					= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketElbow);				//= ko.observable('');		this.jacketElbow.subscribe(function() { self.flushModel(); });
			this.jacketFrontLength				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontLength);				//= ko.observable('');		this.jacketFrontLength.subscribe(function() { self.flushModel(); });
			this.jacketFrontPanelChest			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontPanelChest);					//= ko.observable('');		this.jacketFrontPanelChest.subscribe(function() { self.flushModel(); });
			this.jacketFrontPanelHip			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontPanelHip);					//= ko.observable('');		this.jacketFrontPanelHip.subscribe(function() { self.flushModel(); });
			this.jacketFrontPanelTrueWaist		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontPanelTrueWaist);					//= ko.observable('');		this.jacketFrontPanelTrueWaist.subscribe(function() { self.flushModel(); });
			this.jacketFrontSideSeamLength		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontSideSeamLength);					//= ko.observable('');		this.jacketFrontSideSeamLength.subscribe(function() { self.flushModel(); });
			this.jacketLapelWidthAtChest		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketLapelWidthAtChest);					//= ko.observable('');		this.jacketLapelWidthAtChest.subscribe(function() { self.flushModel(); });
			this.jacketShoulderSeam				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketShoulderSeam);					//= ko.observable('');		this.jacketShoulderSeam.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelBottomEdge		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelBottomEdge);					//= ko.observable('');		this.jacketSidePanelBottomEdge.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelChest			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelChest);				//= ko.observable('');		this.jacketSidePanelChest.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelHip				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelHip);					//= ko.observable('');		this.jacketSidePanelHip.subscribe(function() { self.flushModel(); });
			this.jacketSidePanelTrueWaist		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelTrueWaist);					//= ko.observable('');		this.jacketSidePanelTrueWaist.subscribe(function() { self.flushModel(); });		
			this.jacketSleeveCuffButtoned		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCuffButtoned);					//= ko.observable('');		this.jacketSleeveCuffButtoned.subscribe(function() { self.flushModel(); });
			this.jacketSleeveCurve				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCurve);					//= ko.observable('');		this.jacketSleeveCurve.subscribe(function() { self.flushModel(); });
			this.jacketSleeveCurveFront			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCurveFront);		//= ko.observable('');		this.jacketSleeveCurveFront.subscribe(function() { self.flushModel(); });
			this.jacketSleeveCurveSide        	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCurveSide);		    //= ko.observable('');		this.jacketSleeveCurveSide.subscribe(function() { self.flushModel(); });
			this.jacketSleeveLengthRight		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveLengthRight);				//= ko.observable('');		this.jacketSleeveLengthRight.subscribe(function() { self.flushModel(); });
			this.jacketName						= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketName);				//= ko.observable('');		this.jacketName.subscribe(function() { self.flushModel(); });
			this.jacketSleeveLengthLeft			= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveLengthLeft);				//= ko.observable('');		this.jacketSleeveLengthLeft.subscribe(function() { self.flushModel(); });
			this.jacketId          				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketId);			//= ko.observable('');		this.jacketId.subscribe(function() { self.flushModel(); });
			this.id          					= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].id);			//= ko.observable('');		this.jacketId.subscribe(function() { self.flushModel(); });
			
			this.jacketChestRule				= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketChestRule);
			this.jacketStomachRule          	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketStomachRule);	
			this.jacketJacketLengthRule         = ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketJacketLengthRule);	
			this.jacketFullShoulderRule         = ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFullShoulderRule);	
			this.jacketHipsRule          		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketHipsRule);	
			this.jacketFrontRule          		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontRule);	
			this.jacketBackRule          		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackRule);	
			this.jacketNeckRule          		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketNeckRule);	
			this.jacketBicepRule          		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBicepRule);	
			this.jacketLeftSleeveRule          	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketLeftSleeveRule);	
			this.jacketRightSleeveRule          = ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketRightSleeveRule);		
			this.jacketWristRule          		= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketWristRule);		
			this.jacketLapelLengthRule          = ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketLapelLengthRule);		
			this.jacketForearmRule          	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketForearmRule);
			this.jacketZeroPointRule          	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketZeroPointRule);	
			this.jacketFrontSZeroRule          	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontSZeroRule);	
			this.jacketSBackSZeroRule          	= ko.observable(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSBackSZeroRule);	
		}catch(e){
			this.jacketAcrossBack     			= ko.observable('');
			this.jacketAcrossShoulder     		= ko.observable('');
			this.jacketBackPanelAtXBack     	= ko.observable('');
			this.jacketBackPanelBottomEdge		= ko.observable('');		
			this.jacketBackPanelChest			= ko.observable('');
			this.jacketBackPanelHip				= ko.observable('');		
			this.jacketBackPanelTrueWaist		= ko.observable('');
			this.jacketFitlineBicep				= ko.observable('');
			this.jacketCenterBackLength			= ko.observable('');
			this.jacketFitlineComments			= ko.observable('');
			this.jacketElbow					= ko.observable('');
			this.jacketFrontLength				= ko.observable('');
			this.jacketFrontPanelChest			= ko.observable('');
			this.jacketFrontPanelHip			= ko.observable('');
			this.jacketFrontPanelTrueWaist		= ko.observable('');
			this.jacketFrontSideSeamLength		= ko.observable('');
			this.jacketLapelWidthAtChest		= ko.observable('');
			this.jacketShoulderSeam				= ko.observable('');
			this.jacketSidePanelBottomEdge		= ko.observable('');
			this.jacketSidePanelChest			= ko.observable('');
			this.jacketSidePanelHip				= ko.observable('');
			this.jacketSidePanelTrueWaist		= ko.observable('');		
			this.jacketSleeveCuffButtoned		= ko.observable('');
			this.jacketSleeveCurve				= ko.observable('');
			this.jacketSleeveCurveFront			= ko.observable('');
			this.jacketSleeveCurveSide        	= ko.observable('');
			this.jacketSleeveLengthRight		= ko.observable('');
			this.jacketName						= ko.observable('');
			this.jacketSleeveLengthLeft			= ko.observable('');
			this.jacketId          				= ko.observable('');
			this.id          					= ko.observable('');
			
			this.jacketChestRule				= ko.observable('');
			this.jacketStomachRule          	= ko.observable('');	
			this.jacketJacketLengthRule         = ko.observable('');	
			this.jacketFullShoulderRule         = ko.observable('');	
			this.jacketHipsRule          		= ko.observable('');	
			this.jacketFrontRule          		= ko.observable('');	
			this.jacketBackRule          		= ko.observable('');	
			this.jacketNeckRule          		= ko.observable('');	
			this.jacketBicepRule          		= ko.observable('');	
			this.jacketLeftSleeveRule          	= ko.observable('');	
			this.jacketRightSleeveRule          = ko.observable('');		
			this.jacketWristRule          		= ko.observable('');		
			this.jacketLapelLengthRule          = ko.observable('');		
			this.jacketForearmRule          	= ko.observable('');
			this.jacketZeroPointRule          	= ko.observable('');	
			this.jacketFrontSZeroRule          	= ko.observable('');	
			this.jacketSBackSZeroRule          	= ko.observable('');	
		}		
	
		this.selectedGarment            = ko.observable({name: "none"});
	},
	
	test: function() {
		console.log(this.orderItemMirror);
	},

	enableVisibility: function(){
    var viewModel = {
        shouldShowMessage: ko.observable(false) // Message initially visible
    };
	},
	update: function(tableindex){
		this.jacketAcrossBack(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketAcrossBack);					//= ko.observable('');		this.jacketAcrossBack.subscribe(function() { self.flushModel(); });
		this.jacketAcrossShoulder(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketAcrossShoulder);					//= ko.observable('');		this.jacketAcrossShoulder.subscribe(function() { self.flushModel(); });
		this.jacketBackPanelAtXBack(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelAtXBack);						//= ko.observable('');		this.jacketBackPanelAtXBack.subscribe(function() { self.flushModel(); });
		this.jacketBackPanelBottomEdge(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelBottomEdge);						//= ko.observable('');		this.jacketBackPanelBottomEdge.subscribe(function() { self.flushModel(); });		
		this.jacketBackPanelChest(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelChest);					//= ko.observable('');		this.jacketBackPanelChest.subscribe(function() { self.flushModel(); });
		this.jacketBackPanelHip(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelHip);						//= ko.observable('');		this.jacketBackPanelHip.subscribe(function() { self.flushModel(); });		
		this.jacketBackPanelTrueWaist(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackPanelTrueWaist);						//= ko.observable('');		this.jacketBackPanelTrueWaist.subscribe(function() { self.flushModel(); });
		this.jacketFitlineBicep(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBicep);					//= ko.observable('');		this.jacketFitlineBicep.subscribe(function() { self.flushModel(); });
		this.jacketCenterBackLength(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketCenterBackLength);				//= ko.observable('');		this.jacketCenterBackLength.subscribe(function() { self.flushModel(); });
		this.jacketFitlineComments(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketComments);			//= ko.observable('');		this.jacketFitlineComments.subscribe(function() { self.flushModel(); });
		this.jacketElbow(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketElbow);				//= ko.observable('');		this.jacketElbow.subscribe(function() { self.flushModel(); });
		this.jacketFrontLength(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontLength);				//= ko.observable('');		this.jacketFrontLength.subscribe(function() { self.flushModel(); });
		this.jacketFrontPanelChest(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontPanelChest);					//= ko.observable('');		this.jacketFrontPanelChest.subscribe(function() { self.flushModel(); });
		this.jacketFrontPanelHip(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontPanelHip);					//= ko.observable('');		this.jacketFrontPanelHip.subscribe(function() { self.flushModel(); });
		this.jacketFrontPanelTrueWaist(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontPanelTrueWaist);					//= ko.observable('');		this.jacketFrontPanelTrueWaist.subscribe(function() { self.flushModel(); });
		this.jacketFrontSideSeamLength(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontSideSeamLength);					//= ko.observable('');		this.jacketFrontSideSeamLength.subscribe(function() { self.flushModel(); });
		this.jacketLapelWidthAtChest(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketLapelWidthAtChest);					//= ko.observable('');		this.jacketLapelWidthAtChest.subscribe(function() { self.flushModel(); });
		this.jacketShoulderSeam(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketShoulderSeam);					//= ko.observable('');		this.jacketShoulderSeam.subscribe(function() { self.flushModel(); });
		this.jacketSidePanelBottomEdge(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelBottomEdge);					//= ko.observable('');		this.jacketSidePanelBottomEdge.subscribe(function() { self.flushModel(); });
		this.jacketSidePanelChest(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelChest);				//= ko.observable('');		this.jacketSidePanelChest.subscribe(function() { self.flushModel(); });
		this.jacketSidePanelHip(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelHip);					//= ko.observable('');		this.jacketSidePanelHip.subscribe(function() { self.flushModel(); });
		this.jacketSidePanelTrueWaist(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSidePanelTrueWaist);					//= ko.observable('');		this.jacketSidePanelTrueWaist.subscribe(function() { self.flushModel(); });		
		this.jacketSleeveCuffButtoned(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCuffButtoned);					//= ko.observable('');		this.jacketSleeveCuffButtoned.subscribe(function() { self.flushModel(); });
		this.jacketSleeveCurve(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCurve);					//= ko.observable('');		this.jacketSleeveCurve.subscribe(function() { self.flushModel(); });
		this.jacketSleeveCurveFront(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCurveFront);		//= ko.observable('');		this.jacketSleeveCurveFront.subscribe(function() { self.flushModel(); });
		this.jacketSleeveCurveSide(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveCurveSide);		    //= ko.observable('');		this.jacketSleeveCurveSide.subscribe(function() { self.flushModel(); });
		this.jacketSleeveLengthRight(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveLengthRight);				//= ko.observable('');		this.jacketSleeveLengthRight.subscribe(function() { self.flushModel(); });
		this.jacketName(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketName);				//= ko.observable('');		this.jacketName.subscribe(function() { self.flushModel(); });
		this.jacketSleeveLengthLeft(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSleeveLengthLeft);				//= ko.observable('');		this.jacketSleeveLengthLeft.subscribe(function() { self.flushModel(); });
		this.jacketId(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketId);			//= ko.observable('');		this.jacketId.subscribe(function() { self.flushModel(); });
		this.id(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].id);			//= ko.observable('');		this.jacketId.subscribe(function() { self.flushModel(); });
		
		this.jacketChestRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketChestRule);
		this.jacketStomachRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketStomachRule);	
		this.jacketJacketLengthRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketJacketLengthRule);	
		this.jacketFullShoulderRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFullShoulderRule);	
		this.jacketHipsRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketHipsRule);	
		this.jacketFrontRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontRule);	
		this.jacketBackRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBackRule);	
		this.jacketNeckRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketNeckRule);	
		this.jacketBicepRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketBicepRule);	
		this.jacketLeftSleeveRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketLeftSleeveRule);	
		this.jacketRightSleeveRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketRightSleeveRule);		
		this.jacketWristRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketWristRule);		
		this.jacketLapelLengthRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketLapelLengthRule);		
		this.jacketForearmRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketForearmRule);
		this.jacketZeroPointRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketZeroPointRule);	
		this.jacketFrontSZeroRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketFrontSZeroRule);	
		this.jacketSBackSZeroRule(dsRegistry.getDatasource('fitlinesJacketDS').getStore()[tableindex].jacketSBackSZeroRule);	
	},
	flushModel: function() {
		var tModel = {}; 
		tModel.jacketAcrossBack				= this.jacketAcrossBack();
		tModel.jacketAcrossShoulder 		= this.jacketAcrossShoulder();
		tModel.jacketBackPanelHip			= this.jacketBackPanelHip();
		tModel.jacketBackPanelAtXBack	    = this.jacketBackPanelAtXBack();
		tModel.jacketBackPanelChest			= this.jacketBackPanelChest();
		tModel.jacketBackPanelBottomEdge	= this.jacketBackPanelBottomEdge();		
		tModel.jacketBackPanelTrueWaist		= this.jacketBackPanelTrueWaist();
				
		tModel.jacketFitlineBicep			= this.jacketFitlineBicep();
		tModel.jacketCenterBackLength	    = this.jacketCenterBackLength();
		tModel.jacketFrontSideSeamLength    = this.jacketFrontSideSeamLength();
		tModel.jacketFitlineComments		= this.jacketFitlineComments();
		tModel.jacketFrontPanelHip		    = this.jacketFrontPanelHip();
		tModel.jacketFrontPanelTrueWaist    = this.jacketFrontPanelTrueWaist();
		tModel.jacketFrontPanelChest	    = this.jacketFrontPanelChest();
		tModel.jacketLapelWidthAtChest	    = this.jacketLapelWidthAtChest();
		tModel.jacketShoulderSeam		    = this.jacketShoulderSeam();
		tModel.jacketElbow				    = this.jacketElbow();
		tModel.jacketFrontLength		    = this.jacketFrontLength();
		tModel.jacketSidePanelBottomEdge    = this.jacketSidePanelBottomEdge();
		tModel.jacketSidePanelChest			= this.jacketSidePanelChest();
		
		tModel.jacketSidePanelTrueWaist		= this.jacketSidePanelTrueWaist();
		tModel.jacketSidePanelHip		    = this.jacketSidePanelHip();
		tModel.jacketSleeveCurveSide		= this.jacketSleeveCurveSide();
		tModel.jacketSleeveCurveFront		= this.jacketSleeveCurveFront();
		tModel.jacketSleeveCuffButtoned		= this.jacketSleeveCuffButtoned();
		tModel.jacketSleeveCurve			= this.jacketSleeveCurve();
		
		tModel.jacketSleeveLengthLeft		= this.jacketSleeveLengthLeft();
		tModel.jacketSleeveLengthRight		= this.jacketSleeveLengthRight();
		tModel.jacketName					= this.jacketName();				
		tModel.jacketId						= this.jacketId();		
		tModel.id							= this.id();
		
		
		tModel.jacketChestRule 				= this.jacketChestRule();
		tModel.jacketStomachRule 			= this.jacketStomachRule();	
		tModel.jacketJacketLengthRule		= this.jacketJacketLengthRule();	
		tModel.jacketFullShoulderRule		= this.jacketFullShoulderRule();	
		tModel.jacketHipsRule				= this.jacketHipsRule();	
		tModel.jacketFrontRule				= this.jacketFrontRule();	
		tModel.jacketBackRule				= this.jacketBackRule();	
		tModel.jacketNeckRule				= this.jacketNeckRule();	
		tModel.jacketBicepRule				= this.jacketBicepRule();	
		tModel.jacketLeftSleeveRule			= this.jacketLeftSleeveRule();	
		tModel.jacketRightSleeveRule		= this.jacketRightSleeveRule();		
		tModel.jacketWristRule				= this.jacketWristRule();		
		tModel.jacketLapelLengthRule		= this.jacketLapelLengthRule();		
		tModel.jacketForearmRule			= this.jacketForearmRule();
		tModel.jacketZeroPointRule			= this.jacketZeroPointRule();	
		tModel.jacketFrontSZeroRule			= this.jacketFrontSZeroRule();	
		tModel.jacketSBackSZeroRule			= this.jacketSBackSZeroRule();
		
		
		return tModel; 
	},

	clear: function() {
		this.jacketAcrossBack('');
		this.jacketAcrossShoulder('');
		this.jacketBackPanelAtXBack('');
		this.jacketBackPanelBottomEdge('');		
		this.jacketBackPanelChest('');
		this.jacketBackPanelHip('');
		this.jacketBackPanelTrueWaist('');
		
		this.jacketFitlineBicep('');
		this.jacketCenterBackLength('');
		this.jacketFitlineComments('');
		this.jacketElbow('');
		this.jacketFrontLength('');
		this.jacketFrontPanelChest('');
		this.jacketFrontPanelHip('');
		this.jacketFrontPanelTrueWaist('');
		this.jacketFrontSideSeamLength('');
		this.jacketLapelWidthAtChest('');
		this.jacketShoulderSeam('');
		this.jacketSidePanelBottomEdge('');
		this.jacketSidePanelChest('');
		
		this.jacketSidePanelHip('');
		this.jacketSidePanelTrueWaist('');
		this.jacketSleeveCuffButtoned('');
		this.jacketSleeveCurve('');
		this.jacketSleeveCurveFront('');
		this.jacketSleeveCurveSide('');
		
		this.jacketSleeveLengthLeft('');
		this.jacketSleeveLengthRight('');
		this.jacketName('');
		this.jacketId('');	
		this.id(0);
		
		
		this.jacketChestRule('');
		this.jacketStomachRule('');	
		this.jacketJacketLengthRule('');	
		this.jacketFullShoulderRule('');	
		this.jacketHipsRule('');	
		this.jacketFrontRule('');	
		this.jacketBackRule('');	
		this.jacketNeckRule('');	
		this.jacketBicepRule('');	
		this.jacketLeftSleeveRule('');	
		this.jacketRightSleeveRule('');		
		this.jacketWristRule('');		
		this.jacketLapelLengthRule('');		
		this.jacketForearmRule('');
		this.jacketZeroPointRule('');	
		this.jacketFrontSZeroRule('');	
		this.jacketSBackSZeroRule('');	
		
		
		this.selectedGarment({name: "none"});
	}
	
})
});