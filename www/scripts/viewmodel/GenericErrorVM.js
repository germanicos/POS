define(['jquery', 'knockout', 'base', 'viewmodel/ImageTagging', 'viewmodel/CustomerSearchVM'], function ($, ko) {


    // Generic Error
    //  ██████╗ ███████╗███╗   ██╗███████╗██████╗ ██╗ ██████╗    ███████╗██████╗ ██████╗  ██████╗ ██████╗
    // ██╔════╝ ██╔════╝████╗  ██║██╔════╝██╔══██╗██║██╔════╝    ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗
    // ██║  ███╗█████╗  ██╔██╗ ██║█████╗  ██████╔╝██║██║         █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝
    // ██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██╗██║██║         ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗
    // ╚██████╔╝███████╗██║ ╚████║███████╗██║  ██║██║╚██████╗    ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║
    //  ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝ ╚═════╝    ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝
    //


    /**
     * Abstract class to be used in fittingError and genericError (common code)
     */
    GenericErrorBase = class {
        constructor() {
            this.title = ko.observable("");
            this.notes = ko.observable("");
            this.tagsList = ko.observableArray(); // list of all tags //garment tags. Do not confuse with image tags.
            this.tags = ko.observableArray([]); // list of selected tags. //garment tags. Do not confuse with image tags.

            this.errorImportance = ko.observable(2); // 2 is default
            this.deductMoney = ko.observable(false);
            this.deductMoneyNote = ko.observable("");
            this.currentTag = ko.observable(); // auxiliary: to show in select
            this.currentWhoIsFault = ko.observable();


            this.whoIsFault = ko.observableArray([]);


            this.images = ko.observableArray();

            this.videos = ko.observableArray();
        }

        /**
         * Adds a new GARMENT tag.
         * Do not confuse with Image tags
         */
        setNewTag() {
            if (!this.currentTag()){
                customAlert("Please, select a tag first");
                return;
            }

            this.tags.push(this.currentTag());
            this.currentTag(null);
        }

        /**
         * Remove a new GARMENT tag.
         * Do not confuse with Image tags
         */
        removeTagError(tag){
            this.tags.remove(tag);
        }

        setRating(number) {
            console.log('Set rating to:', number);
            this.errorImportance(number);
        }


        /**
         * fix image stuffs
         */
        getSubmissionData(){
            const data = ko.mapping.toJS({
                title             : this.title(),
                notes             : this.notes(),
                tags              : this.tags(),
                // errorImportance   : this.errorImportance(),
                deductMoney       : this.deductMoney(),
                deductMoneyNote   : this.deductMoneyNote(),
                currentWhoIsFault : this.currentWhoIsFault()
            });

            // if include image is true put in POST
            data.includedImages = this.images().map(image => {
                return {
                    tags: image.tags,
                    imagePath: image.imagePath,
                    }
                });

            // include videos into post
            data.videos = this.videos().map(video => {
                return {
                    videoURL: video.url
                }
            });


            console.log('Send data: ', data);
            return data;
        }

        /**
         * Adds a new tag to the image.
         * Do not confuse with GARMENT tags
         * @param {*} text
         * @param {*} event
         * @param {*} image
         */
        newImageTag_deprecated(text, event, currentTaggingImage, currentTaggingImage_additionalIndex) {

            let image;
            // get first image of the list
            if (this.images().length == 0){
                console.warn("no image to add tag");
                return;
            } else {
                image = this.images()[0];
            }



            if (text) {
                const target = event.target;
                var image_left = $(target).offset().left;
                var click_left = event.pageX;
                var left_distance = click_left - image_left;
                var image_top = $(target).offset().top;
                var click_top = event.pageY;
                var top_distance = click_top - image_top;
                var imagemap_width = $(target).width();
                var imagemap_height = $(target).height();
                var relative_left = (left_distance / imagemap_width) * 100;
                var relative_top = (top_distance / imagemap_height) * 100;

                const newTag = { left: relative_left, top: relative_top, text: text };

                console.log("TAG META", newTag);

                image.tags.push(newTag);

                // render all images (reload view)
                this.renderImageTags();

            }

        }

        renderImageTags(){
            for (const image of this.images()) {
                image.render();
            }
        }

        /**
         * Adds an image.
         * @param {String} imagePath We only ned the image url.
         *
         * To test:
         * orderItem.fittingsVM.FittingData[0].fittingErrorsReport.currentErrorReport().addImage("http://192.168.0.34:8888/thepos/uploaded/temp_order_images/1572383176.jpg");
         * genericError.addImage(Utils.getRandomImage())
         */
        addImage(imagePath){

            const countImages = this.images().length;
            this.images.push(new FittingImageTags(
                `image-tag-wrapper-errorImage_${countImages}`,
                `image-tag-wrapper-errorImage_${countImages}-comment`,
                `image-tag-wrapper-errorImage-popup`,
                {id: `errorImage_${countImages}`, path: imagePath}
            ));

            this.renderImageTags();
        }

        removeImage(image){
            console.log('image', image);

            const successMove = () => {
                this.images.remove(function (item) { return item.imageId == image.imageId; })

                this.renderImageTags();
            }

            pos_confirm({msg:"Are you sure you want to remove this image?", confirmAction: successMove});
        }

        chooseVideoSource() {

            var self = this;

            console.log("choosing Video source...");

            // testing on browser
            // this.video({
            //     url: '/Users/usuario/Documents/Germanicos/meetings/QC.mp4',
            //     comments: ko.observableArray([])
            // });


            navigator.notification.confirm(
                'Video Source', // message
                function(btnIndex) {
                    self._takeVideo(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

        }

        _takeVideo(buttonIndex) {

            self = this;

            console.log("Taking Video...");

            if( buttonIndex == 1 ) // from camera
            {
                navigator.device.capture.captureVideo(onSuccess, onFail,
                {
                    quality: 1,
                    saveToPhotoAlbum: true
                });
            }
            else // From gallery
            {
                navigator.camera.getPicture(onGalleryDataSuccess, onFail, {
                                quality: 1,
                                destinationType: Camera.DestinationType.FILE_URI,
                                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                mediaType: Camera.MediaType.VIDEO,
                                correctOrientation : true
                            });
            }


            function onSuccess(mediaFiles) {

                console.log("onVideoDataSuccess", mediaFiles);
                var i, path, len;

                for (i = 0, len = mediaFiles.length; i < len; i += 1) {

                    console.log(mediaFiles[i].localURL);
                    console.log(mediaFiles[i].fullPath);
                    window.resolveLocalFileSystemURL(mediaFiles[i].localURL, resolveOnSuccess, resOnError);
                }
            }

            function onGalleryDataSuccess(videoURI) {
                console.log("onGalleryDataSuccess");
                window.resolveLocalFileSystemURL(videoURI, successMove, resOnError);
            };


            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.mov';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },
                resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                console.log("entry.toURL()", entry.toURL());

                const video = {
                    'url' : '',
                    // 'comments' : ko.observableArray([])
                };

                video.url = entry.toURL();

                self.videos.push(video);
            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        } // end Capturing pic

        deleteVideo(video) {
            let confirmAction = () => {
                this.videos.remove(function (item) {return item.url == video.url});
            }

            pos_confirm({msg:"Are you sure you want to remove the video?", confirmAction, confirmBtn:"yes", cancelBtn:"no"});
        }

        /**
         * Is no longer required to insert tag in videos
         */
        newTagVideo_deprecated() {
            const comment = prompt("Please describe the problem ", "");
            if (comment == ""){
                customAlert("Please type something");
                return;
            }

            this.video().comments.push(comment);
        }

        /**
         * Is no longer required to insert tag in videos
         */
        removeTagVideo_deprecated(commentIndex){
            this.video().comments.remove(this.video().comments()[commentIndex]);
        }

        /**
         * Adds a image from CAMERA or GALLERY
         */
        choosePicSource() {

            var self = this;

            console.log("choosing Pic source...");

            navigator.notification.confirm(
                'Picture Source', // message
                function(btnIndex) {
                    self._takePicture(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

        }

        /**
         * Type => front/back/side/additional
         * @param  {[type]} buttonIndex [description]
         * @param  {[type]} type        [description]
         * @return {[type]}             [description]
         */
        _takePicture(buttonIndex) {

            self = this;

            console.log("Taking Pic...");

            const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            });

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                console.log("entry.toURL()", entry.toURL());

               self.addImage(entry.toURL());

            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };


        }// end Capturing pic



    }

    GenericErrorVM = class extends GenericErrorBase{
        constructor(){
            super();

            // to search customer:
            this.custSelectVM = new CustomerSearchVM(dsRegistry);
			this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
            this.custSelectVM.setParentVM(this);
            this.selectedCustomer = ko.observable(null);


            // holds which orders to show in select
            this.ordersList = ko.observableArray([]);
            this.selectedOrder = ko.observable(null);
            this.selectedOrder.subscribe((newValue)=>{
                // when this changes, select which garments to show in select
                this.selectOrder(newValue);
            });

            // holds which garments to show in select
            this.garmentsList = ko.observableArray();
            this.selectedGarment = ko.observable(null);
            this.selectedGarmentTypeId = ko.computed(() => {
                if (this.selectedGarment()){
                    return this.selectedGarment().type;
                }
            });

            this._getNecessaryData();

            this.chooseFittingImagePopUp = new ChooseFittingImagePopUp(this);

        }

        _getNecessaryData(){
            const self = this;


            document.getElementById('loading_jp').style.display = "block";
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'errors_pos/get_errors_creation_data',
                dataType: 'json',
                data: {
                    "user"         : authCtrl.userInfo,
                    'device_id'    : device.uuid
                },

                success: function (dataS) {

                    let tags = [];

                    tags = dataS.tags_list; // keys: {id, name, description}

                    console.log('tags', tags);

                    const whoIsFault = dataS.fault_list; // keys: {id, name, description}

                    self.whoIsFault(whoIsFault);
                    self.tagsList(tags);
                },

                error: function (error) {
                    console.warn("Error on errors_pos/get_errors_creation_data", error);
                    customAlert("Something went wrong, please try again later :( ");
                },

                complete: () => {
                    document.getElementById('loading_jp').style.display = "none";
                },

                async: true
            });
        }

        /**
         * Clear inputs and selects of customer, orders and garments
         */
        clearLinkCustomerOrderGarment(){
            this.custSelectVM.customerAutocomplete("");
            this.selectedOrder(null);
            this.selectedGarment(null);

            this.ordersList([]);
            this.garmentsList([]);
        }

        /**
         * overwritten:
         * We need to get more data
         */
        getSubmissionData(){
            const data = ko.mapping.toJS({
                title                           : this.title(),
                notes                           : this.notes(),
                tags                            : this.tags(),
                // errorImportance              : this.errorImportance(),
                deductMoney                     : this.deductMoney(),
                deductMoneyNote                 : this.deductMoneyNote(),
                currentWhoIsFault               : this.currentWhoIsFault(),
                selectedCustomerId              : this.selectedCustomer().server_id,
                selectedOrderId                 : this.selectedOrder().order_id,
                selectedGarmentTypeId           : this.selectedGarmentTypeId(),
                selectedGarmentProductToOrderId : this.selectedGarment().orders_products_id,
            });

            // if include image is true put in POST
            data.includedImages = this.images().map(image => {
                return {
                    tags: image.tags,
                    imagePath: image.imagePath,
                    }
                });

            // include videos into post
            data.videos = this.videos().map(video => {
                return {
                    videoURL: video.url
                }
            });


            console.log('Sending data: ', data);
            return data;
        }

        selectCustomer(customer){
            console.log("Selecting customer", customer);


            this.selectedCustomer(customer);
            this.custSelectVM.customerAutocomplete(`${customer.customer_first_name} ${customer.customer_last_name}`);
            this.custSelectVM.customersAutocompleteList([]);


            this.getOrdersOfCustomer(customer.server_id);


            $.jGrowl("Added successfully");
        }

        /**
         * sets which garments to show in select
         * @param {*} order
         */
        selectOrder(order){
            if (!order) return;

            // fix suit garment
            // for error we do not handle SUIT, so we must to split those suit into JACKET and PANTS
            (() => {
                const non_suit_garments = order.garments_in_order.filter(el => el.type != "6");
                const suit_garments = order.garments_in_order.filter(el => el.type == "6");
                const suit_garments_splitted = [];

                suit_garments.forEach(garment => {
                    // const new_pant =   {...garment, garment_name: "Pants/S",  type: "1",};
                    const new_pant = Object.assign({}, garment, {garment_name: "Pants/S",  type: "1",});
                    // const new_jacket = {...garment, garment_name: "Jacket/S", type: "4",};
                    const new_jacket = Object.assign({}, garment, {garment_name: "Jacket/S", type: "4",});


                    suit_garments_splitted.push(new_pant);
                    suit_garments_splitted.push(new_jacket);
                });

                order.garments_in_order = non_suit_garments.concat(suit_garments_splitted);

            })()
            // end fix suit garment


            this.garmentsList(order.garments_in_order.map(garment => {
                garment.textToShow = `${garment.garment_name} - ${garment.fabric_code}`;
                return garment;
            }));

        }

        /**
         * After select a customer, we need to get the orders of this customers
         * @param {*} customer_id
         */
        getOrdersOfCustomer(customer_id){
            var self = this;
            console.log('customer_id', customer_id);

            document.getElementById('loading_jp').style.display = "block";
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'errors_pos/get_customer_orders',
                dataType: 'json',
                data: {
                    "user"         : authCtrl.userInfo,
                    'customer_id'    : customer_id
                },

                success: function (dataS) {

                    console.log('dataS', dataS);

                    if (!dataS.orders_list){
                        customAlert('This client has no order');
                        self.ordersList([]);
                    }
                    else{
                        let ordersList = Object.values(dataS.orders_list);
                        ordersList = ordersList.map(order => {
                            // text to show on select (view)
                            order.textToShow = `${order.order_id} / ${order.formatted_order_date}`;
                            return order;
                        });
                        self.ordersList(ordersList);
                    }
                },

                error: function (error) {
                    console.warn("Error on errors_pos/get_customer_errors", error);
                    customAlert("Sorry, unable to get orders , please try again");
                },
                complete: () => {
                    document.getElementById('loading_jp').style.display = "none";
                },

                async: true
            });
        }

        openChooseFittingImagePopup(){
            if (!this.selectedGarment()){
                pos_warning({msg: 'You must select an order garment first'});
                return;
            }

            $("#loading_jp").show();

            $('#choose-fitting-image-popup').show("fast");

            this.chooseFittingImagePopUp.getFittingImages(this.selectedGarment().orders_products_id, this.selectedGarment().type);

        }


        cancelChanges() {
            const confirmAction = () => {
                posChangePage('#main');
            }

            pos_warning({msg: "Are you sure, you want to cancel and discard all changes?", confirmAction});


        }

        saveError() {

            // validate form
            if (!this.title()){
                customAlert("Please, fill 'Title' field");
                return;
            }
            if (!(this.tags() && this.tags().length > 0)){
                customAlert("Please, set at least one 'Tag'");
                return;
            }
            if (!this.currentWhoIsFault()){
                customAlert("Please, set a 'Who is fault");
                return;
            }

            if (!this.selectedCustomer()){
                customAlert('Please, set a CUSTOMER');
                return;
            }

            if (!this.selectedOrder()){
                customAlert('Please, set an ORDER');
                return;
            }

            if (!this.selectedGarmentTypeId()){
                customAlert('Please, set a GARMENT TYPE');
                return;
            }

            for (const form of document.querySelectorAll("form.validateForm")) {
                if (!form.reportValidity()) {
                    customAlert("Please, check if all mandatory info are filled");
                    return;
                }
            }

            // No more mandatory:
            // if this error have at least one image or one video
            // if (this.images().length == 0 && this.videos().length == 0){
            //     customAlert("Please, check if error have at least one image or one video");
            //     return;
            // }


            const confirmAction = () => {
                var self = this;

                document.getElementById('loading_jp').style.display = "block";
                $.ajax({
                    type: 'POST',
                    timeout: 60000, // sets timeout to 60 seconds
                    url: BUrl + 'errors_pos/create_new_generic_error',
                    dataType: 'json',
                    data: {
                        "user"  : authCtrl.userInfo,
                        "error" : self.getSubmissionData(),

                    },

                    success: function (dataS) {

                        pos_confirm({msg: "Error successfully submitted"});

                        // go to main page
                        posChangePage('#main');
                    },

                    error: function (error) {
                        console.warn("Error on errors_pos/create_new_generic_error", error);
                        customAlert("Something went wrong");
                    },
                    complete: () => {
                        document.getElementById('loading_jp').style.display = "none";
                    },

                    async: true
                });
            };

            pos_confirm({msg: "Are you sure you want to submit this error?", confirmAction})


        }


    }

    /**
     * Handler popup stuffs inside generic error (reuse fitting images)
     */
    class ChooseFittingImagePopUp{
        /**
         *
         * @param {GenericErrorBase} errorVM
         */
        constructor(errorVM) {
            this.errorVM = errorVM;
            this.fittingImages = ko.observableArray([]);
            this.mustShowTags = ko.observable(true);
            this.listOfFittings = ko.observableArray([]); // unused for now
        }

        /**
         * @returns {Array} list of selected images
         */
        get selectedFittingImages(){
            const response = [];

            for (const image of this.fittingImages()) {
                if (image.selected()) {
                    response.push(image);
                }
            }

            return response;
        }

        /**
         * Toggle render tags
         */
        toggleImageTags(){
            this.mustShowTags(!this.mustShowTags());
        }


        /**
         * Make a post to get All fitting images of aa garment
         * @param {*} garment_id
         * @param {*} garment_type
         */
        getFittingImages(garment_id, garment_type){
            // POST to the server to the get brief details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/get_garment_fittings",
				dataType: 'json',
				data: {
                    "user": authCtrl.userInfo,
                    garment_id,
                    garment_type,
                },
				async: true,
				timeout: 60000,

				success: (ret) => {

                    let fitting_images = [];
                    const listOfFittings = [];

                    for (const fitting of ret.fittings) {

                        listOfFittings.push({
                            id: fitting.fitting_id,
                            textToShow: `Fitting ${fitting.fitting_id} - ${fitting.fitting_date}`
                        });

                        for (const key in fitting.fitting_images) {
                            let fitting_image = fitting.fitting_images[key];

                            // get FBS images
                            if (key !== "additional_images"){
                                // add more attributes to fittingImage from fitting object
                                fitting_image.customer_name = fitting.customer_name;
                                fitting_image.notes         = fitting.notes;
                                fitting_image.fitting_date  = fitting.fitting_date;
                                fitting_image.ordered_date  = fitting.ordered_date;

                                fitting_images.push(fitting_image)
                            }

                            // additional image its a list of images.
                            else {
                                // add more attributes to fittingImage from fitting object
                                fitting_image = fitting_image.map(fittingImage => {

                                    fittingImage.customer_name = fitting.customer_name;
                                    fittingImage.notes         = fitting.notes;
                                    fittingImage.fitting_date  = fitting.fitting_date;
                                    fittingImage.ordered_date  = fitting.ordered_date;

                                    return fittingImage;
                                })

                                fitting_images = fitting_images.concat(fitting_image)
                            }
                        }
                    }

                    console.log("Fitting images response", ret);

                    // fiz some attributes of each image
                    fitting_images = fitting_images.map(image => {
                        // add resized path
                        // image.resized_path = Utils.getRandomImage();

                        // fix tags
                        image.tags = image.fitting_image_tags ? (image.fitting_image_tags) : [];

                        // select to errorVM
                        image.selected = ko.observable(false);

                        return image;
                    });


					this.fittingImages(fitting_images);
                    this.listOfFittings(listOfFittings);

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log("fitting comments endpoint cannot be reached! ");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
					customAlert("TIME OUT - there is a network issue. Please try again later");
				},
                complete: () => {
                    $("#loading_jp").hide();
                }
			});
        }

        /**
         *
         * @param {string} imageId
         */
        selectImage(imageId){
            const imageToToggle = this.fittingImages().find(image => image.fitting_image_id == imageId);
            if (!imageToToggle){
                console.warn("Image not found");
                return;
            }

            imageToToggle.selected(!imageToToggle.selected());

        }

        /**
         *
         * @param {string} imageId
         */
        isImageSelected(imageId){
            return this.fittingImages().find(image => image.fitting_image_id == imageId).selected;
        }

        /**
         * add to ErrorVM the selected image, then close popup
         */
        save(){
            for (const imageToAdd of this.selectedFittingImages) {
                this.errorVM.addImage(BUrl + imageToAdd.path);

                // add tags in this recent added image
                // no more need to get tags
                // this.errorVM.images()[this.errorVM.images().length-1].tags = imageToAdd.tags;
            }

            $("#choose-fitting-image-popup").hide("fast");
            this.errorVM.renderImageTags();
        }

    }

})