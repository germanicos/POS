define(['jquery', 'knockout', 'base',], function ($, ko) {


    MakeOneFirstToApprove = class MakeOneFirstToApprove {
        constructor() {
            this.garmentsToApprove = ko.observableArray([]);

        }


        /**
         * Given a list of new garments to insert, this method inject a new attribute in each garment and then push into garmentsToApprove list.
         * @param {List} newGarments
         */
        insertGarmentsToApprove(newGarments) {
            // spread operator: (push each element of list into garmentsToApprove)
            this.garmentsToApprove.push(...newGarments.map(garment => {
                // inject new attribute
                garment.toApprove = ko.observable(false);
                return garment;
            }));
        }

        /**
         * Map id to string
         * @param {*} typeId
         */
        getType(typeId) {
            if (typeId == "1") {
                return "Pants";
            }
            else if ((typeId == "2")) {
                return "Shirt";
            }
            else if ((typeId == "3")) {
                return "Vest";
            }
            else if ((typeId == "4")) {
                return "Jacket";
            }
            else if ((typeId == "6")) {
                return "Suit";
            }
            else {
                console.warn("Type undefined");
                return "??";
            }
        }

        submitSelectedGarments() {
            console.log("submitSelectedGarments");

            const approvedGarments = this.garmentsToApprove().filter(garment => garment.toApprove());
            for (const garmentToApprove of approvedGarments) {

                // confirm one more time
                const confirmAction = () => {
                    $.ajax({
                        type: 'POST',
                        timeout: 60000, // sets timeout to 60 seconds
                        url: BUrl + "outside_controller/approve_order_garment",
                        dataType: 'json',
                        data: {
                            "garment_id": garmentToApprove.orders_products_id,
                            "user_id": authCtrl.userInfo.user_id,
                            "factory_instructions": `Please use UPDATED PATTERN ( updated at : ${(new Date())})`,
                            "username": authCtrl.userInfo.username,
                            "password": authCtrl.userInfo.password
                        },
                        success: function (dataS) {
                            $.jGrowl("Garment successfully submitted");
                            $('#make-on-first-popup').hide(500);
                            customAlert("Fitting succesfully added");
                            posChangePage('#fittingList');
                        },
                        error: (error) => {
                            customAlert(`Something wrong on submission of garment ${garmentToApprove.barcode_id}, Please try again later`);
                            console.log('error', error);
                        },
                        async: false
                    });
                };
                pos_confirm({msg:`Are you sure you want to approve garment ${garmentToApprove.barcode_id}?`, confirmAction});

            }
        }

        cancelSubmitMakeOne(){
            $('#make-on-first-popup').hide(500);
            customAlert("Fitting succesfully added");
            posChangePage('#fittingList');
        }
    }
});