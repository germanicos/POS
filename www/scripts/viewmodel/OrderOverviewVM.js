define(['jquery', 'knockout', 'base'], function ($, ko) {

    OrderOverviewVM = class {

        constructor(orderVM){
            this._orderVM = orderVM;
            this.garmentToShowOverview = ko.observable(false);

            // to show in select on the view
            this.cities = dsRegistry.getDatasource('citiesDS').getStore();
            this.states = dsRegistry.getDatasource('statesDS').getStore();
            this.countries = dsRegistry.getDatasource('countriesDS').getStore();
        }

        getAllGarments(){
            // current IOS version do not support Object.values
            var orderGarments = Object.keys(this._orderVM.globalGarments).map(key => this._orderVM.globalGarments[key]());
            // join all lists into only one
            const reducer = (accumulator, currentValue) => accumulator.concat(currentValue);
            return orderGarments.reduce(reducer, []);
        }

        /**
         * Get data to show in the view
         */
        getPortfolioData (garment_uniqueId) {
            console.log('garment_uniqueId', garment_uniqueId);

            const orderGarments = this.getAllGarments();

            var result = ko.observableArray();

            // search garment in all garments array
            for (const garment_object of orderGarments) {
                const garment = garment_object.garment;
                if (garment.unique_id == garment_uniqueId) {
                    for (const category of garment.categories) {
                        for (const option of category.options) {
                            let optionName = option.name;

                            // Custom data (images) and fit
                            if (["custom data", "fit", "jacket fit", "pant fit"].includes(category.name.toLowerCase())){
                                // do not show in portfolio
                                continue;
                            }

                            // special case: fabric is stored in another place
                            if (optionName.toLowerCase() == 'fabric') {
                                const selectedFabric = garment.selectedFabric();
                                result.push([
                                    optionName,
                                    selectedFabric.title + ', ' + (selectedFabric.composition_1 == null ? "" : selectedFabric.composition_1) + ' ' + (selectedFabric.composition_2 == null ? "" : selectedFabric.composition_2) + ' ' + (selectedFabric.composition_3 == null ? "" :selectedFabric.composition_3)]);

                                result.push(["Fabric weight", selectedFabric.weight]);
                            } else {
                                // if value was selected
                                if (option.selectedValue()) {

                                    // do not show any colours if selectedValue is DEFAULT
                                    if (
                                        // type is colour
                                        ['5','8','9'].includes(option.type_id)&&

                                        // selected colour is DEFAULT
                                        option.selectedValue().name && option.selectedValue().name.toLowerCase().trim() == 'default'
                                        ){

                                            // do not show in portfolio
                                            continue;
                                    }

                                    // if selected value is object
                                    let optionValue = option.selectedValue().name == null ? option.selectedValue() : option.selectedValue().name;
                                    result.push([optionName, optionValue]);

                                }

                            }


                        }
                    }
                    return result;
                }
            }

        }

        _getFirstGarment(){
            return this.getAllGarments()[0];
        }

        /**
         * Make a post to update customer details
         * @param {*} customer
         */
        updateCustomerInfo(customer) {

            const customerNewData = ko.mapping.toJS(customer);
            console.log('customer json', customerNewData);

            $('#loading_jp').show();
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/update_reorder_customer',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    'device_id': device.uuid,
                    'data': JSON.stringify(customerNewData),
                },
                success: (dataS) => {
                    console.log(dataS);
                    $('#customerOverview').hide(500)
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                complete: () => {
                    $('#loading_jp').hide();
                },
                async: true,
            });
        }

    }
});