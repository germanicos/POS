define(['jquery', 'knockout', 'base'], function ($, ko) {


    UniformOrderUploadImagesStepVM = SimpleControl.extend({

        init: function (uniformOrderVM) {
            var self = this;

            // structure of the images
            self.imageData = this.getImageDataStructure();

            // Mock images
            /*
            self = uniformOrder.uploadImagesStep;
            self.imageData[0].url('http://npg.si.edu/exhibit/feature/images/schoeller_full.jpg'); // front
            self.imageData[1].url('http://npg.si.edu/exhibit/feature/images/schoeller_full.jpg'); // side
            self.imageData[2].url('http://npg.si.edu/exhibit/feature/images/schoeller_full.jpg'); // back

            self.imageData[3].images.push(self.getNewAdditionalImage()); // gets empty additional img
            self.imageData[3].images.push(self.getNewAdditionalImage()); // gets empty additional img

            self.imageData[3].images()[0].url('http://npg.si.edu/exhibit/feature/images/schoeller_full.jpg'); // adds first additional
            self.imageData[3].images()[1].url('https://poptudo.com/wp-content/uploads/2013/06/emmasite.jpg'); // adds seccond additional
            */

            this.currentAddingTag = ko.observable(false);
            this.currentTaggingImage = ko.observable(false);

            this.nextMissingPicType = ko.computed(function() {
                return self.getNextMissingPicType();
            });


            console.log("Init upload media VM");
        },

        getSubmissionData : function() {

            const flat_imgs = [];

            // Makes the images flat in case of additional image, the strucutre of additinal is a bit different
            for(let img of this.imageData)
            {
                if(img.type === "additional")
                {
                    // Adds the additional images in the array
                    for(let add of img.images())
                    {
                        flat_imgs.push(add);
                    }
                }
                else
                {
                    flat_imgs.push(img);
                }
            }


            return ko.mapping.toJS(flat_imgs);

        },

        getImageDataStructure : function() {

            /**
             * The Additional images will have a different strucure.
             *     Every Image object (inside images array) will follow the same structure of the normal image
             */
            return [
                        {
                            'type' : 'front',
                            'url' : ko.observable(false),
                            'tags' : ko.observableArray([])
                        },
                        {
                            'type' : 'left side',
                            'url' : ko.observable(false),
                            'tags' : ko.observableArray([])
                        },
                        {
                            'type' : 'back',
                            'url' : ko.observable(false),
                            'tags' : ko.observableArray([])
                        },
                        {
                            'type' : 'additional',
                            'images' : ko.observableArray([]),
                        }

                    ];

        },

        getNewCurrentAddingTag : function(posX, posY) {

            // Pos X and Pos Y in percentages
            return {
                'text' : ko.observable(''),
                'posX' : posX,
                'posY' : posY
            };
        },

        /**
         * This method should be called every time a additional image is taken
         * @return {[type]} [description]
         */
        getNewAdditionalImage : function() {

            /**
             * The Additional images will have a different strucure.
             *     Every Image object (inside images array) will follow the same structure of the normal image
             */

            return {
                        'type' : 'additional',
                        'url' : ko.observable(),
                        'tags' : ko.observableArray()
                    };
        },


        /**
         * Gets the next missing picture and takes it using the camera
         */
        globalChoosePicSource : function() {

            const nextMissingPicType = this.getNextMissingPicType();

            this.choosePicSource(nextMissingPicType);

        },

        /**
         * gets the next image that is missing to take
         */
        getNextMissingPicType : function() {

            // By default we return the next type as 'additional'
            let missingType = "additional";

            for( let imageType of this.imageData )
            {

                // checks if image is missing
                if( imageType.type != 'additional' && ( imageType.url() == false || imageType.url() == "") )
                {
                    missingType = imageType.type;
                    break;
                }

            }

            return missingType;
        },


        choosePicSource : function(imgType) {

            var self = this;

            console.log("choosing Pic source...");

            navigator.notification.confirm(
                'Picture Source', // message
                function(btnIndex) {
                    self.takePicture(btnIndex, imgType);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

        },

        /**
         * Opens the pop up for the user to tag the image
         * @param  {[type]} item  [description]
         * @param  {[type]} event [description]
         * @return {[type]}       [description]
         */
        tagImage : function(item, event) {

            //  this is binded to the item
            // console.log("this", this);
            console.log("item", item);
            console.log("event", event);

            const mediaWidth = $(event.target).width();
            const mediaHheight = $(event.target).height();

            const posX = $(event.target).position().left;
            const posY = $(event.target).position().top;

            // in percentages
            const finalX = (event.offsetX - posX) / mediaWidth;
            const finalY = (event.offsetY - posY) / mediaHheight;

            console.log( "X: " + finalX + ' , ' + " Y: " + finalY );

            /**
             * We dont have the refence "this" to this viewmodel, we need to use the global reference in this case
             */

            // creates the new tag
            const newTag = uniformOrder.uploadImagesStep.getNewCurrentAddingTag(finalX, finalY);

            // sets the clicked image as the 'in tagging process' image
            uniformOrder.uploadImagesStep.currentTaggingImage(item);

            // adds the tags to the image
            item.tags.push(newTag);

            // Sets the current tag
            uniformOrder.uploadImagesStep.currentAddingTag(newTag);

            // Fix position fixed bug on IOS with keyboard
            $(".add-tag-pop-up").css({
                "position": "absolute",
                "top": ( ((($(parent).height() - $(".add-tag-pop-up").outerHeight()) / 10) + $(parent).scrollTop() ) + "px"),
                "left": ((($(parent).width() - $(".add-tag-pop-up").outerWidth()) / 2) + $(parent).scrollLeft() + "px")
            });

        },

        /**
         * Adds the tag for real in the image ----- uses the this.currentAddingTag as the tag
         */
        addTag : function() {

            console.log('Sets the current tag as none');

            this.currentAddingTag(false);
            this.currentTaggingImage(false);
        },

        editTag : function(tag, image) {
            console.log("editing tag...");

            this.currentAddingTag(tag);
            this.currentTaggingImage(image);
        },

        deleteTag : function() {
            console.log("deleting tag notifications ios pop up....");

            var self = this;

            navigator.notification.confirm(
                'Delete Tag ?', // message
                function(btnIndex) {

                    if(btnIndex == 2)
                    {
                        self.currentTaggingImage().tags.remove(self.currentAddingTag());
                        self.currentTaggingImage(false);
                        self.currentAddingTag(false);
                    }

                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['No', 'Yes']         // buttonLabels
            );


        },

        closeTagPopUp : function() {
            console.log("closing tag pop up....");
            this.currentTaggingImage(false);
            this.currentAddingTag(false);
        },

        removeImage : function(image) {

            const img = this.imageData.filter( e => e.type === image.type)[0];

            console.log("found img", img);

            const confirmAction = () => {
                if( image.type != 'additional')
                {
                    img.url(false);
                    img.tags([]);
                }
                else
                {
                    const addImage = img.images().filter( e => e.url() === image.url() )[0];
                    const index = img.images().indexOf(addImage);
                    img.images.splice(index,1);

                    // addImage.url(false);
                    // addImage.tags([]);
                }

                $.jGrowl(`Image Deleted sucessfully !!`, { life: 2000 });
            };
            pos_warning({msg:`Do you really want to delete this ${image.type} image ?`, confirmAction});


        },

        /**
         * Type => front/back/side/additional
         * @param  {[type]} buttonIndex [description]
         * @param  {[type]} type        [description]
         * @return {[type]}             [description]
         */
        takePicture : function(buttonIndex, type) {

            self = this;

            console.log("Taking Pic...");

            const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            });

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                console.log("entry.toURL()",entry.toURL());

                if( type === 'additional') // if additional => creates a new image and adds to the array
                {
                    const img = uniformOrder.uploadImagesStep.getNewAdditionalImage();
                    img.url(entry.toURL());

                    uniformOrder.uploadImagesStep.imageData.filter( e => e.type === 'additional')[0].images.push(img);
                }
                else // just add as normal image
                {
                    const img = uniformOrder.uploadImagesStep.imageData.filter( e => e.type == type)[0];
                    img.url(entry.toURL()); // sets the url
                    img.tags([]); // empties the tags
                }

            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };


        }, // end Capturing pic

        checkStepIsComplete: function () {
            return ko.computed(() =>{
                for (const imageData of this.imageData) {
                    if (imageData.type != 'additional') {
                        if (imageData.url() == undefined || imageData.url() == false || imageData.url() == "") {
                            return false;
                        }
                    }
                }

                return true;
            })


        }


    });
});