define(['jquery', 'knockout', 'base', 'viewmodel/ImageTagging', 'viewmodel/GenericErrorVM'], function ($, ko) {
    AlterationErrorReportVM = class {
        /**
         *
         * @param {*} bar_code
         * @param {number} garment_type [1,2,3,4]
         * @param {AlterationsReportSubmissionVM} alterationsReportSubmissionVM
         */
        constructor(bar_code, garment_type, alterationsReportSubmissionVM = alterationErrorReportVM) {
            this.bar_code = bar_code;

            this.garment_type = garment_type; // 1,2,3 or 4

            this.alterationsReportSubmissionVM = alterationsReportSubmissionVM;

            // a list of AlterationsErrors instances
            this.alterationErrors = ko.observableArray();

            // copy of alterationError list. So we can discard changes if the user wants
            // before we go to next popup, we make this copy (on create a new error or edit existent one)
            this.alterationErrors_copy = ko.observableArray();

            this.tags;
            this.whoIsFault;
            this.getNecessaryDataFromERP();

            this.currentErrorReport = ko.observable(false);

            // in alteration we can have "front", "side", "back" and "additional" (additional is a list).
            // Those variables tell us which one we are tagging
            this.currentTaggingImage = null;
            this.currentTaggingImage_additionalIndex = null;
        }

        /**
         *
         */
        getNecessaryDataFromERP() {

            const self = this;

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'errors_pos/get_errors_creation_data',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    'device_id': device.uuid
                },

                success: function (dataS) {

                    let tags = [];

                    if (self.garment_type) {
                        tags = dataS.tags_list.filter(tag => tag.garment_type == self.garment_type); // keys: {id, name, description}
                    }
                    else {
                        tags = dataS.tags_list; // keys: {id, name, description}
                    }

                    console.log('tags', tags);

                    const whoIsFault = dataS.fault_list; // keys: {id, name, description}


                    self.alterationErrors = ko.observableArray([]);
                    self.whoIsFault = ko.observableArray(whoIsFault);
                    self.tags = ko.observableArray(tags);
                },

                error: function (error) {
                    console.warn("Error on errors_pos/get_errors_creation_data", error);
                    customAlert("Alterations Errors could not be obtained, please try again");
                },

                async: true
            });

        }

        /**
         * select an error and then show popup
         * @param {*} errorReport
         */
        selectErrorReport(errorReport) {
            this.currentErrorReport(errorReport);
            $('#alterationErrorReport').show(500);
            this.currentErrorReport().renderImageTags();
        }

        /**
         *  copy of alterationError list. So we can discard changes if the user wants
         *  before we go to next popup
         */
        cloneAlterationErrors() {
            this.alterationErrors_copy(this.alterationErrors().map(error => error.getClone()));
        }

        /**
         * Clone before show popUp
         * @param {*} errorReport
         */
        editOneError(errorReport) {
            this.cloneAlterationErrors();
            this.selectErrorReport(errorReport);
        }

        addNewErrorReport() {
            this.cloneAlterationErrors();

            // create new Error report
            const newErrorReport = new AlterationError(this.alterationsReportSubmissionVM);

            // insert into list
            this.alterationErrors.push(newErrorReport);

            this.selectErrorReport(newErrorReport);
        }

        /**
         * remove last error already added
         */
        deleteCurrentError() {
            const confirmAction = () => {
                this.alterationErrors.remove(this.currentErrorReport());
                $('#alterationErrorReport').hide(500);
            }

            pos_warning({msg: "Are you sure, you want to remove this error?", confirmAction});
        }

        deleteErrorByPosition(positionIndex) {
            const confirmAction = () => {
                this.alterationErrors.splice(positionIndex, 1);
            }

            pos_warning({msg: "Are you sure, you want to remove this error?", confirmAction});
        }

        saveError() {
            // validate form
            if (!this.currentErrorReport().title()) {
                customAlert("Please, fill 'Title' field");
                return;
            }
            if (!(this.currentErrorReport().tags() && this.currentErrorReport().tags().length > 0)) {
                customAlert("Please, set at least one 'Tag'");
                return;
            }
            if (!this.currentErrorReport().currentWhoIsFault()) {
                customAlert("Please, set a 'Who is fault");
                return;
            }

            for (const form of document.querySelectorAll("form.validateForm")) {
                if (!form.reportValidity()) {
                    customAlert("Please, check if all mandatory info are filled");
                    return;
                }
            }

            // if this error have at least one image or one video
            if (this.currentErrorReport().images().length == 0 && this.currentErrorReport().videos().length == 0) {
                customAlert("Please, check if error have at least one image or one video");
                return;
            }

            // close popup
            $('#alterationErrorReport').hide(500);
        }

        cancelChanges() {

            const confirmAction = () => {
                this.alterationErrors(this.alterationErrors_copy.slice(0));
                $("#alterationErrorReport").hide("slow");
            };

            pos_warning({msg: "Are you sure, you want to cancel and discard all changes?", confirmAction});


        }

        getSubmissionData() {
            return ko.mapping.toJS({
                bar_code: this.bar_code,
                error: this.alterationErrors().map(alterationError => alterationError.getSubmissionData()),
            });
        }

        get errorsCount() {
            return this.alterationErrors().length;
        }

        submitErrors(){

            const confirmAction = () => {

                const data = this.getSubmissionData();
                console.log('data', data);


                document.getElementById('loading_jp').style.display = "block";
                $.ajax({
                    type: 'POST',
                    url: window.BUrl + "errors_pos/create_alteration_errors",
                    dataType: 'json',
                    async: true,
                    data: {
                        "user": authCtrl.userInfo,
                        "errors": data,
                    },
                    success: (dataS) => {
                        // clear errors list
                        this.alterationErrors([]);
                        $('#selectErrorReport-popup').hide(500);
                    },
                    error: (dataS) => {
                        pos_error({msg: 'Something went wrong. Please try again later'});
                        console.error('dataS', dataS);
                    },
                    complete: (dataS) => {

                        document.getElementById('loading_jp').style.display = "none";

                    }
                });
            }

            pos_confirm({msg: "Are you sure you want to submit these Errors? The list will be cleaned", confirmAction});





        }

    }

    /*
    ███████╗██╗████████╗████████╗██╗███╗   ██╗ ██████╗    ███████╗██████╗ ██████╗  ██████╗ ██████╗
    ██╔════╝██║╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝    ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗
    █████╗  ██║   ██║      ██║   ██║██╔██╗ ██║██║  ███╗   █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝
    ██╔══╝  ██║   ██║      ██║   ██║██║╚██╗██║██║   ██║   ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗
    ██║     ██║   ██║      ██║   ██║██║ ╚████║╚██████╔╝   ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║
    ╚═╝     ╚═╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝    ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝
    */
    /**
     * Private class to deal with a single alteration error
     * >> To test: alterationsReportSubmissionVM.alterationErrorReport.currentErrorReport()
     */
    class AlterationError extends GenericErrorBase {
        constructor(alterationsReportSubmissionVM = alterationErrorReportVM) {
            super();

            this.isRemake = ko.observable(false);
            this.remakeNotes = ko.observable("");
            this.remakeFullOrPartial = ko.observable("full");

            this.chooseAlterationImagePopUp = new ChooseAlterationImagePopUp(this);

            this.alterationsReportSubmissionVM = alterationsReportSubmissionVM;
        }

        /**
         * get clone of this object.
         */
        getClone() {
            const temp = new AlterationError(this.alterationsReportSubmissionVM);

            temp.title(this.title());
            temp.notes(this.notes());
            temp.tags(this.tags());
            temp.errorImportance(this.errorImportance());
            temp.deductMoney(this.deductMoney());
            temp.deductMoneyNote(this.deductMoneyNote());
            temp.currentTag(this.currentTag());
            temp.currentWhoIsFault(this.currentWhoIsFault());
            temp.whoIsFault(this.whoIsFault());

            temp.isRemake(this.isRemake());
            temp.remakeNotes(this.remakeNotes());
            temp.remakeFullOrPartial(this.remakeFullOrPartial());

            // get image clone
            temp.images(this.images())

            // get video clone
            // TODO: ...
            temp.videos(this.videos())

            return temp;
        }

        chooseImageFromAlteration(){
            $("#choose-alteration-image-popup").show("fast");
        }

        /**
         * fix image stuffs
         * overwritten: remake stuffs
         */
        getSubmissionData() {
            const data = ko.mapping.toJS({
                title: this.title(),
                notes: this.notes(),
                tags: this.tags(),
                // errorImportance   : this.errorImportance(),
                deductMoney: this.deductMoney(),
                deductMoneyNote: this.deductMoneyNote(),
                currentWhoIsFault: this.currentWhoIsFault(),
                isRemake: this.isRemake(),
                remakeNotes: this.remakeNotes(),
                remakeType: this.remakeFullOrPartial(),

            });

            // if include image is true put in POST
            data.includedImages = this.images().map(image => {
                return {
                    tags: image.tags,
                    imagePath: image.imagePath,
                }
            });

            // include videos into post
            data.videos = this.videos().map(video => {
                return {
                    videoURL: video.url
                }
            });

            data.fitting_id = this.alterationsReportSubmissionVM.alteration().fitting_id;
            data.selectedOrderId = this.alterationsReportSubmissionVM.alteration().order_id;
            data.selectedGarmentTypeId = this.alterationsReportSubmissionVM.alteration().garment_type;
            data.selectedGarmentProductToOrderId = this.alterationsReportSubmissionVM.alteration().garment_id;

            console.log('Send data: ', data);
            return data;
        }

    }

    /**
     * Handler popup stuffs inside generic error (reuse alteration images)
     */
    class ChooseAlterationImagePopUp{
        /**
         *
         * @param {GenericErrorBase} errorVM
         */
        constructor(errorVM) {
            this.errorVM = errorVM;
            this.alterationImages = ko.observableArray();
            this.mustShowTags = ko.observable(true);

            this.getAlterationImages()
        }

        /**
         * @returns {Array} list of selected images
         */
        get selectedAlterationImages(){
            const response = [];

            for (const image of this.alterationImages()) {
                if (image.selected()) {
                    response.push(image);
                }
            }

            return response;
        }

        /**
         * Toggle render tags
         */
        toggleImageTags(){
            this.mustShowTags(!this.mustShowTags());
        }


        /**
         * Get images from alterationsReportSubmissionVM and select which one goes to error
         * Bad use: alterationsReportSubmissionVM is a global var
         */
        getAlterationImages(){

            this.alterationImages(
                alterationsReportSubmissionVM.briefDetailsImages().map(image => {

                    // fix names of the attributes
                    image.tags = image.image_tags.map(tag => {
                        const new_tag = {
                            top : tag.top,
                            left : tag.left,
                            text : tag.english_text,
                        };
                        return new_tag
                    });

                    // select to errorVM
                    image.selected = ko.observable(false);

                    return image;
                })
            )

        }

        /**
         *
         * @param {string} imageId
         */
        selectImage(imageId){
            const imageToToggle = this.alterationImages().find(image => image.fitting_image_id == imageId);
            if (!imageToToggle){
                console.warn("Image not found");
                return;
            }

            imageToToggle.selected(!imageToToggle.selected());

        }

        /**
         *
         * @param {string} imageId
         */
        isImageSelected(imageId){
            return this.alterationImages().find(image => image.fitting_image_id == imageId).selected;
        }

        /**
         * add to ErrorVM the selected image, then close popup
         */
        save(){
            for (const imageToAdd of this.selectedAlterationImages) {
                this.errorVM.addImage(BUrl + imageToAdd.image_path);

                // add tags in this recent added image
                // no more need to get the tags
                // this.errorVM.images()[this.errorVM.images().length-1].tags = imageToAdd.tags;
            }

            $("#choose-alteration-image-popup").hide("fast");
            this.errorVM.renderImageTags();
        }

    }

})