define(['jquery', 'knockout', 'base',
	'viewmodel/specifics_steps/MeasurementsSpecificReorder', 'viewmodel/specifics_steps/PaymentSpecificReorder', 'viewmodel/specifics_steps/MediaSpecificReorder', 'viewmodel/specifics_steps/CustomerInfoSpecificReorder'],
	function($, ko) {

    /**
     * This class will receive the customers ids that we will use for the
     * order process.
     */
    PendingGroupOrdersVM = SimpleControl.extend({


    	init: function() {

    		var self = this;

    		console.log("Init pendingGroupOrdersVM...");

			// pending orders
    		this.orders = ko.observableArray([]);
			// incomplete orders
			this.savedOrders = ko.observableArray([]);

    		this.getPendingOrdersAjax();

    		this.pendingOrderSteps = this.getPendingGroupOrderSteps();

    		// Keep track of the current step that the salesman is in at any given moment
    		this.currentStep = ko.observable(this.pendingOrderSteps[0]);

    		// Selected order to peform the edition
    		this.currentOrder = ko.observable(false);

    		// current customer of the currentOrder for edition page
			this.currentCustomer = ko.observable(false);
			this.currentCustomer.subscribe(newCustomer => {
				// configure DOB
				const year_month_day = newCustomer.customer_DOB() ? newCustomer.customer_DOB().split("-") : ["", "", ""];

				newCustomer.customer_birth_year = ko.observable(year_month_day[0]);
				newCustomer.customer_birth_month = ko.observable(year_month_day[1]);
				newCustomer.customer_birth_day = ko.observable(year_month_day[2]);

				newCustomer.customer_DOB = ko.computed(function () {
					return `${newCustomer.customer_birth_year()}-${newCustomer.customer_birth_month()}-${newCustomer.customer_birth_day()}`;
				})

				console.log('newCustomer', newCustomer);

			});

    		this.cities = dsRegistry.getDatasource('citiesDS').getStore();
    		this.states = dsRegistry.getDatasource('statesDS').getStore();
    		this.countries = dsRegistry.getDatasource('countriesDS').getStore();
			this.referal_methods_list = ko.observableArray([
                { "name": "Google Search",       "id": "1" },
                { "name": "Bing Search",         "id": "8" },
                { "name": "Yahoo Search",        "id": "9" },
                { "name": "Print Advertisement", "id": "2" },
                { "name": "TV Ad",               "id": "3" },
                { "name": "Radio Ad",            "id": "4" },
                { "name": "Billboard",           "id": "5" },
                { "name": "Other",               "id": "6" },
                { "name": "Referral",            "id": "7" },
                {"name": "Facebook",             "id": "10"},
                {"name": "Instagram",             "id": "11"},
                {"name": "LinkedIn",             "id": "12"}
            ]);

			this.days = ko.observableArray([]);
            // for (i = 1; i <= 31; i++) this.days.push(i.toString());
			for (i = 1; i <= 31; i++) {
				// if number is 1,2,3,4...9 => convert to 01, 02, 03 ... 09
				let formatNumber = i.toString();
				if (formatNumber.length < 2){
					formatNumber = "0"+formatNumber;
				}
				this.days.push(formatNumber);
			};

			this.months = ko.observableArray([
				{ "name": "January",   "id": "01" },
				{ "name": "February",  "id": "02" },
				{ "name": "March",     "id": "03" },
				{ "name": "April",     "id": "04" },
				{ "name": "May",       "id": "05" },
				{ "name": "June",      "id": "06" },
				{ "name": "July",      "id": "07" },
				{ "name": "August",    "id": "08" },
				{ "name": "September", "id": "09" },
				{ "name": "October",   "id": "10" },
				{ "name": "November",  "id": "11" },
				{ "name": "December",  "id": "12" }
			]);

            this.years = ko.observableArray([]);
            const currentDate = new Date();
            for (i = currentDate.getFullYear()+3; i >= currentDate.getFullYear(); i--) this.years.push(i.toString());

			this.years2 = ko.observableArray([]);
            for (i = currentDate.getFullYear(); i >= currentDate.getFullYear()-110; i--) this.years2.push(i.toString());

			// to handle which step to go while reorder
			this.whatIsMissingPopUp = new WhatIsMissingPopUp();
		},

		handleReceivingGroupOrdersFromPost: function (dataS) {
			var self = this;

			console.log("get_pending_group_orders", dataS);

			// we are storing orders into two arrays. one is for submitted orders and the other is for non submitted
			self.orders.destroyAll(); // clear array
			self.savedOrders.destroyAll(); // clear array

			for (let order of dataS.orders) {
				// single order
				if (order.customers.length == 1) {
					order.group_name = "Single Order: " + order.customers[0].full_name;
				}

				// Transform each customer in an observable
				order.customers = order.customers.map(customer => {

					const mappedCustomer = ko.mapping.fromJS(customer);

					// Create the NON observable value of is_present to know the initial value
					// of the variable
					mappedCustomer.is_present_initial_value = mappedCustomer.is_present();

					return mappedCustomer;
				});

				order.leader = order.customers.filter(e => e.is_leader())[0];

				// if all customers has order_id == "0" => is a saved incomplete order.
				if (order.customers.filter(customer => customer.order_id() == "0").length == 0){
					self.orders.push(order);
				}
				else {
					self.savedOrders.push(order);
				}


			}
		},

        getPendingOrdersAjax : function() {

        	var self = this;

        	$.ajax({
        	    type: 'POST',
        	    timeout: 60000, // sets timeout to 60 seconds
        	    url: BUrl + 'orders_pos/get_pending_group_orders',
        	    dataType: 'json',
        	    data: {
        	        "user": authCtrl.userInfo
        	    },
        	    success: function (dataS) {

        	    	self.handleReceivingGroupOrdersFromPost(dataS);

        	    },
        	    error: function (error) {
        	        console.log(JSON.stringify(error));
        	        customAlert("TIME OUT - there is a network issue. Please try again later");
        	    },
        	    async: false
        	});
        },

        getPendingGroupOrderSteps : function() {

    		return [
    			 	{
                        'name' : 'orderSelection',
                        'id' : '1'
                    },
                    {
                        'name' : 'customerList',
                        'id' : '2'
                    },
                    {
                        'name' : 'customerDetails',
                        'id' : '3'
                    }
    			];
    	},

    	/**
    	 * Selects an order from the order list and goes to the seccond step
    	 * @return {[type]} [description]
    	 */
    	selectOrder : function(order) {

    		console.log("selecting order");
			this.currentOrder(order);

			const firstCustomer = order.customers.filter( e => !e.is_present() && !e.is_leader() )[0];
			if (firstCustomer){
				this.currentCustomer(firstCustomer);
			} else {
				this.currentCustomer(order.customers[0]);
			}


    		// Goes to the second step
    		this.currentStep(this.pendingOrderSteps.filter(e => e.id == '2')[0]);


    		$(".pre-order-step:visible").effect("slide", { direction: 'right' }, 500);
    	},

		openMissingStepPopUp : function(order){
			this.currentOrder(order);
			this.whatIsMissingPopUp.setSelectedOrder(order);
			$('#whatIsMissingPopup-wrapper .decide-reorder-type-wrapper').show();
			$('#whatIsMissingPopup-wrapper .generic-pop-up').show('fast');
		},

		// Checks each step requirment before moving to the next one
		checkStepRequirements: function (step) {
			let result = { pass: true, message: "" };

			switch (step) {
				// orderSelection
				case "1":
					// no requirements
					result.pass = true;
					break;

				// customerList
				case "2":
					// Checks if the user filled up the fields
					for (let c of pendingGroupOrders.currentOrder().customers) {
						if (
							!c.customer_first_name() ||
							!c.customer_last_name() ||
							!c.customer_mobile_phone() ||
							!c.customer_email()
						) {
							result.pass = false;
							result.message = "Please fill up all the customers information !";
							return result;
						}
					}
					if (!result.pass) return result;

					// checks if there is a leader
					result.pass = false;
					result.message = "Please, You have to choose a Leader for the group !";
					for (let c of pendingGroupOrders.currentOrder().customers) {
						if (c.is_leader()) {
							// if find a leader => pass
							result.pass = true;
							result.message = "";
							break;
						}
					}
					if (!result.pass) return result;

					// check if there is anyone present
					result.pass = false;
					result.message = "You can not continue without a present customer !";
					for (let c of pendingGroupOrders.currentOrder().customers) {
						if (c.is_present()) {
							// if find a leader => pass
							result.pass = true;
							result.message = "";
							break;
						}
					}
					if (!result.pass) return result;

					// check if there is anyone paying
					result.pass = false;
					result.message = "You can not continue without a paying customer !";
					for (let c of pendingGroupOrders.currentOrder().customers) {
						if (c.is_paying()) {
							// if find a leader => pass
							result.pass = true;
							result.message = "";
							break;
						}
					}
					if (!result.pass) return result;

					break;

				// customerDetails
				case "3":
					for (const el of document.querySelectorAll("form.validateForm")) {
						if (!el.reportValidity()){
							result.pass = false;
							result.message = `Please fill up all ${c.full_name()} information !`;
							return result;
						}
					}

					// check if all info for customer is fill
					// !! => turn ['', null, 0, false, undefined] to false
					for (const c of this.currentOrder().customers) {

						if (c.is_present()) {
							if (
								[
									!!c.customer_address1(),
									!!c.customer_DOB(),
									!!c.customer_city(),
									!!c.customer_country(),
									!!c.customer_state(),
									!!c.customer_email(),
									!!c.customer_first_name(),
									!!c.customer_last_name(),
									!!c.customer_mobile_phone(),
									!!c.customer_postal_code(),
									!!c.customer_address2()
								].includes(false)
							) {
								result.pass = false;
								result.message = `Please fill up all ${c.full_name()} information !`;
								return result;
							}

						// commented because user can not access not present customer to fill
						// } else {
						// 	//If customer is not present, only is required
						// 	// first_name, last_name, mobile_phone, email.
						// 	if ([
						// 		!!c.customer_email(),
						// 		!!c.customer_first_name(),
						// 		!!c.customer_last_name(),
						// 		!!c.customer_mobile_phone()
						// 	].includes(false)
						// 	) {
						// 		result.pass = false;
						// 		result.message = `Please fill up all ${c.full_name()} information !`;
						// 		return result;
						// 	}
						}

						// check if 'What did you type' is empty and shown
						if (['1', '8', '9'].includes(c.customer_referal_method()) && !c.customer_referal_moreinfo()) {
							result.pass = false;
							result.message = `Please fill up 'What did you type' information for ${c.full_name}!`;
							return result;
						}
					}

					break;
			}

			return result;
		},


    	nextStep : function() {

    		let nextStep;

    		let requirements = null;

    		switch(this.currentStep().id)
    		{
				case '1':
					requirements = this.checkStepRequirements('1');
					if (!requirements.pass) {
						customAlert(requirements.message);
						return;
					}
					nextStep = this.pendingOrderSteps.filter(e => e.id == '2')[0];
					break;

    			case '2':
					requirements = this.checkStepRequirements('2');
					if (!requirements.pass){
						customAlert(requirements.message);
						return;
					}
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '3')[0];
    			break;
    		}

    		this.currentStep(nextStep);

    		$(".pre-order-step:visible").effect("slide", { direction: 'right' }, 500);
    	},

    	lastStep : function() {

    		let nextStep;

    		let requirements = null;

    		switch(this.currentStep().id)
    		{
    			case '2':
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '1')[0];
    			break;

    			case '3':
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '2')[0];
    			break;
    		}

    		this.currentStep(nextStep);

    		$(".pre-order-step:visible").effect("slide", { direction: 'left' }, 500);
    	},

    	selectDetailInfoStepCurrentCustomer : function(customer) {

    		if(!customer.is_present())
    		{
    			customAlert("You cannot select this customer -- He is not present !");
    			return;
    		}

    		console.log("customer", customer);
    		this.currentCustomer(customer);
    		$(".datepicker").datepicker();

    	},

    	proceedToOrder : function() {

			let requirements = this.checkStepRequirements('3');
			if (!requirements.pass){
				customAlert(requirements.message);
				return;
			}

			const confirmAction = () => {
				const customers_ids = this.currentOrder().customers.map( customer => customer.customer_id() );
				const leader = this.currentOrder().customers.filter( customer => customer.is_leader() )[0];
				const order_ids = this.currentOrder().customers.map( customer => customer.order_id() );

				/**
				 * Information to be sent to ERP and recover the order data in order to
				 * recreate the order instance and finish the order.
				 * @type {Object}
				 */
				const reorder_data = {
						'orders_ids' : order_ids,
						'customers_data' : ko.mapping.toJS(this.currentOrder().customers)
					};


				const groupOrdersData = {
						'customers_ids'  : customers_ids,
						'leader_id'      : leader.customer_id(),
						'customers_data' : undefined, // no need to send this here, the information is all in ERP already
						'leaderPic'      : leader.image(),
						'group_name'     : leader.group_name(),
						'is_reorder'     : true,
						'reorder_data'   : reorder_data,
						'groupId'        : this.currentOrder().group_id
					};
				/**
				 * Writing the data in the local store, so we can retrieve it when we initiate the group orders
				 * process
				 */
				localStorage.setItem('groupOrdersData' , JSON.stringify(groupOrdersData) );
				posChangePage('#orders');


				// Sends the new customer data for the selcted order to ERP
				this.updateCustomerData()
			};

			pos_confirm({msg:'Proceed to order ?', confirmAction});


    	},

        /**
         * Updates the customer data for the forms
         * @return {[type]} [description]
         */
        updateCustomerData : function() {

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/update_customers_data',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "customers": ko.mapping.toJSON(this.currentOrder().customers)
                },
                success: function (ret) {

                    console.log(ret);

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                },
                async: true
            });


        },


		/**
		 *
		 * @param {String} groupId
		 * @param {array} customersList - we get customers Ids inside this function
		 * @param {boolean} TODO: Mensagem para Igor: Igor, quando vc fizer o tratamento no ERP, remove essa variável
		 */
		deleteSavedGroup: function (groupId, customersList) {

			const confirmAction = () => {
				var self = this;

				// get customersIds
				let customers_ids = [];
				for (const customer of customersList)
				{
					customers_ids.push(customer.customer_id());
				}

				// Tell server to delete group
				$.ajax({
					type: 'POST',
					timeout: 60000, // sets timeout to 60 seconds
					url: BUrl + 'orders_pos/delete_pending_group_orders',
					dataType: 'json',
					data: {
						"user"          : authCtrl.userInfo,
						"group_id"      : groupId,
						"customers_ids" : customers_ids
					},
					success: function (dataS) {

						self.handleReceivingGroupOrdersFromPost(dataS);

					},
					error: function (error) {
						console.log(JSON.stringify(error));
						customAlert("TIME OUT - there is a network issue. Please try again later");
					},
					async: false
				});
			}

			pos_warning({msg: "Do you really want to delete this order?", confirmAction})

		}

    });

	class WhatIsMissingPopUp {
		constructor() {
			this.selectedOrder = ko.observable();
			this.steps = ko.observable(false)
		}

		setSelectedOrder(order){
			this.selectedOrder(order);
			this.steps(this.getPendingSteps());
		}

		getPendingSteps() {

			/**
			 * Resume info in an object like this:
			 * "steps": {
			 *       "measurement": false,
			 *       "media": false,
			 *       "payment": false,
			 *       "customer_info" : false,
			 * }
			 */
			function _transformResponseToUseInView(dataS) {
				// Transform response to use in the view...
				for (const info_customer of Object.values(dataS.missing_info)) {
					const info = info_customer.missing_info;

					// transform measurement_info
					let is_measurement_missing = false;
					Object.values(info.measurments_missing).forEach(el => {
						if (el) {
							is_measurement_missing = true;
							return; // end loop
						}
					});

					// transform media_info
					let is_media_missing = (info.is_images_missing == true || info.is_videos_missing == true);

					// transform payment_info
					let is_payment_missing = (info.payment_missing.missing_first_payment == true || info.payment_missing.missing_signature == true);

					// customer info
					let is_customer_missing = info.is_customer_info_missing

					info.steps = {
						customer_info : is_customer_missing,
						measurement: is_measurement_missing,
						media: is_media_missing,
						payment: is_payment_missing,
					}
				}

				return {customers: dataS.missing_info};
			}

			let response = null;

			document.getElementById('loading_jp').style.display = "block";

			$.ajax({
				type: 'POST',
				url: BUrl + 'orders_pos/get_missing_data_from_group',
				dataType: 'json',
				async: false, // need to be false
				data: {
					"user": authCtrl.userInfo,
					'group_id' : this.selectedOrder().group_id,
				},
				success: function (dataS) {

					document.getElementById('loading_jp').style.display = "none";

					console.log(dataS);
					response = _transformResponseToUseInView(dataS)
				},
				error: function (dataS) {
					// customAlert('Something went wrong in "get_missing_data_from_group" endpoint');
					console.log(dataS);

					document.getElementById('loading_jp').style.display = "none";

					pos_error({msg: 'Something went wrong.'});

				},
			});


			return response;
		}

		goToStep(step, customer) {

			function _getPreviousOrder() {

				const this_pendingGroupOrders = pendingGroupOrders; // global reference

				let response = null;
				const customers_ids = this_pendingGroupOrders.currentOrder().customers.map(customer => customer.customer_id());
				const leader = this_pendingGroupOrders.currentOrder().customers.filter(customer => customer.is_leader())[0];
				const order_ids = this_pendingGroupOrders.currentOrder().customers.map(customer => customer.order_id());

				/**
				 * Information to be sent to ERP and recover the order data in order to
				 * recreate the order instance and finish the order.
				 * @type {Object}
				 */
				const reorder_data = {
					'orders_ids': order_ids,
					'customers_data': ko.mapping.toJS(this_pendingGroupOrders.currentOrder().customers)
				};

				const groupOrdersData = {
					'customers_ids': customers_ids,
					'leader_id': leader.customer_id(),
					'customers_data': undefined, // no need to send this here, the information is all in ERP already
					'leaderPic': leader.image(),
					'group_name': leader.group_name(),
					'is_reorder': true,
					'reorder_data': reorder_data,
					'groupId': this_pendingGroupOrders.currentOrder().group_id
				};

				document.getElementById('loading_jp').style.display = "block";


				$.ajax({
					type: 'POST',
					timeout: 60000, // sets timeout to 60 seconds
					url: BUrl + 'orders_pos/prepare_order',
					dataType: 'json',
					data: {
						"user": authCtrl.userInfo,
						"customers": JSON.stringify(groupOrdersData.customers_ids),
						"leader": groupOrdersData.leader_id,
						"reorderData": JSON.stringify(groupOrdersData.reorder_data)
					},
					success: function (dataS) {

						document.getElementById('loading_jp').style.display = "none";

						console.log('dataS', dataS);
						response = dataS;

					},
					error: function (error) {

						document.getElementById('loading_jp').style.display = "none";

						console.log(JSON.stringify(error));
						customAlert("TIME OUT - there is a network issue. Please try again later");
					},
					async: false // need to be false
				});

				return response;
			}

			const order = _getPreviousOrder();

			const wrapper = '#reorder_specifics_steps_wrapper';

			$('#loading_jp').show();
			switch (step) {
				case "measurement":
					window.measurementsSpecificReorder = new MeasurementsSpecificReorder(wrapper, order, customer);
					break;

				case "payment":
					window.paymentSpecificReorder = new PaymentSpecificReorder(wrapper, order, customer);
					break;

				case "media":
					window.mediaSpecificReorder = new MediaSpecificReorder(wrapper, order, customer);
					break;

				case "customer_info":
					window.customerInfoSpecificReorder = new CustomerInfoSpecificReorder(wrapper, order, customer);
					break;
				default:
					pos_alert({msg:"Options not ready"});
					$('#loading_jp').hide();
					break;
			}

			// hide first popup
			// $('#whatIsMissingPopup-wrapper .generic-pop-up').hide('fast');

		}


		/**
		 * Check if all steps are complete
		 *
		 * returns boolean
		 */
		areAllStepsComplete(){
			let response = true; // default

			for (const customer of Object.values(this.steps().customers)) {
				if (Object.values(customer.missing_info).includes(true) ||
					Object.values(customer.missing_info.measurments_missing).includes(true) ||
					Object.values(customer.missing_info.payment_missing).includes(true) ||
					Object.values(customer.missing_info.steps).includes(true)) {

					return false;
				}


			}

			return response;

		}
	}



});