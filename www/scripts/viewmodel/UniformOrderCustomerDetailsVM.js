define(['jquery', 'knockout', 'base'], function($, ko) {

    /**
     * Responsible for measurements, fit, monogram and fit of the Template instances
     */
    UniformOrderCustomerDetailsVM = SimpleControl.extend({

    	init: function(uniformOrderVM, fitlines, measurements, designs) {

            console.log("Init UniformOrderCustomerDetailsVM...");

            var self = this;

            this.defaultMeasurments = measurements;

            this.uniformOrderVM = uniformOrderVM;
            this.fitlines = ko.observableArray(fitlines);
            this.selectedFitline = ko.observable(this.fitlines[0]);

            this.designs = designs;

            this.measurements = this.buildGarmentMeasurments(measurements);
            // jacket, pants, shirt or vest -> for measurements only
            this.selectedGarment = ko.observable(false);

            this.availableFits = ko.observableArray(this.buildAvailableFits());

            // Selected instance, will be use to set up the monogram on the tabs
            this.selectedInstance = ko.observable(false);


            // A computed who says if only one garment has instance
            this.hasInstances = ko.computed(function(){

                for (const measurementType of self.measurements) {
                    if (measurementType.hasInstances()){
                        return true;
                    }
                }

                return false;

            });
        },


        /**
         * Special design option build HARDCODED just for this section.
         *
         *     Visible: tells if the fit is selected in the UI
         */
        buildAvailableFits : function() {

            return [
                    {
                        'id' : '1',
                        'name' : 'FITTED',
                        'visible' : ko.observable(false)
                    },
                    {
                        'id' : '2',
                        'name' : 'SEMI FITTED',
                        'visible' : ko.observable(false)
                    },
                    {
                        'id' : '3',
                        'name' : 'STANDARD FIT',
                        'visible' : ko.observable(true)
                    }
                ];
        },

        assignFit : function(templateInstance, fitOption) {
            console.log("assigning fit...");
            templateInstance.selectedFit(fitOption);
        },

        selectFitOption : function(fitOption) {

            console.log("Selecting Fit...");
            for( let availableFit of this.availableFits() )
            {
                availableFit.visible(false);
            }

            fitOption.visible(true);
        },


        /**
         * Returns an garment instance
         *  Template is the master design of this instance.
         *
         * This method are called in [increase / decrease] garment template
         */
        getGarmentObject : function(template) {


            var garment = {
                'selectedFit' : ko.observable(false), // Can be: FITTED, SEMI FITTED, STANDRD FIT
                'template' : template,
            }

            var self = this;

            // build image 3d for monogram. (Each garment has different keys)

            // for jacket, vest and suit
            if (['3','4','6'].includes(template.garment_type)){
                garment.image3D = {
                    // default images.
                    // TODO: get from template
                    'liningColor': ko.observable("gfx/order/image_3d/liningFabric/sign/01.png"),
                    'pipingColor': ko.observable("gfx/order/image_3d/pipingColors/piping_default_preview.png"),
                    'pocketColor': ko.observable("gfx/order/image_3d/inpocket/piping_default_preview.png   "),

                    // monogram
                    'monogramLine1': ko.observable("Special Tailored For"),
                    'monogramLine2': ko.observable(""),
                    'monogramLine3': ko.observable(""),

                    // font styles
                    'jacketStyles' : this.getJacketStyles(),

                    'jacketSelectedStyle' : ko.observable(''),

                    'selectStyle' : function(value){
                        // unselect all, except the clicked value
                        for (const val of self.selectedInstance().image3D.jacketStyles) {
                            if (val.id == value.id){
                                val.selected(true);
                                self.selectedInstance().image3D.jacketSelectedStyle(value);
                            }
                            else {
                                val.selected(false);
                            }
                        }
                    },
                };

            // shirt
            } else if (template.garment_type == '2'){
                garment.image3D = {

                    'shirtMonogram': ko.observable(""),

                    'shirtSelectedPosition' : ko.observable(''),

                    'shirtFrontLogos': ko.observableArray([
                        { name: 'CHEST',       id: '1',   pos_x: 57,  pos_y: 29,  selected: ko.observable(false) },
                        { name: 'CUFF',        id: '2',   pos_x: 56,  pos_y: 61,  selected: ko.observable(false) },
                        { name: 'MID AREA',    id: '3',   pos_x: 53,  pos_y: 48,  selected: ko.observable(false) }

                    ]),

                    'shirtBackLogos': ko.observableArray([
                        { name: 'BACK COLLAR', id: '4',  pos_x: 43,  pos_y: 3,  selected: ko.observable(false) },
                    ]),

                    'selectShirtPosition': function (value) {
                        // unselect all, except the clicked value
                        for (const val of self.selectedInstance().image3D.shirtFrontLogos()) {
                            if (val.id == value.id) {
                                val.selected(true);
                                self.selectedInstance().image3D.shirtSelectedPosition(value);
                            }
                            else {
                                val.selected(false);
                            }
                        }
                        for (const val of self.selectedInstance().image3D.shirtBackLogos()) {
                            if (val.id == value.id) {
                                val.selected(true);
                                self.selectedInstance().image3D.shirtSelectedPosition(value);
                            }
                            else {
                                val.selected(false);
                            }
                        }
                    },

                    'otherOptions' : ko.observableArray([
                        {
                            name: 'Stitching', id: '1', selected: ko.observable(false),
                            image: ko.computed(function(){
                                return  self.selectedInstance() ? self.selectedInstance().image3D.shirtSelectedColor().image : '';
                            })
                        },
                        {
                            name: 'Notes',     id: '2', selected: ko.observable(false),
                            image: 'img/note.png'
                        }
                    ]),

                    'stitchColors' : this.getMonogramStitchColor(),

                    'shirtSelectedColor' : ko.observable(''),


                    'selectStitchColor' : function(value){
                        // unselect all, except the clicked value
                        for (const val of self.selectedInstance().image3D.stitchColors) {
                            if (val.id == value.id){
                                val.selected(true);
                                self.selectedInstance().image3D.shirtSelectedColor(value);
                            }
                            else {
                                val.selected(false);
                            }
                        }
                    },

                    'monogramNotes' : ko.observable(''),

                    'sideToShow' : ko.observable('front'),

                    'changeSideToShow' : function(){
                        if (self.selectedInstance().image3D.sideToShow() == 'front'){
                           self.selectedInstance().image3D.sideToShow('back');
                        } else{
                           self.selectedInstance().image3D.sideToShow('front');
                        }
                    },

                    // font styles
                    'shirtStyles' : this.getShirtStyles(),

                    'shirtSelectedStyle': ko.observable(''),

                    'selectStyle' : function(value){
                        // unselect all, except the clicked value
                        for (const val of self.selectedInstance().image3D.shirtStyles) {
                            if (val.id == value.id){
                                val.selected(true);
                                self.selectedInstance().image3D.shirtSelectedStyle(value);
                            }
                            else {
                                val.selected(false);
                            }
                        }
                    },

                    // to build 3d
                    // TODO: get from template
                    'shirtMain' : ko.observable('gfx/order/image_3d/shirt/main_bg.png'),
                    'shirtCollar' : ko.observable('gfx/order/image_3d/shirt/collar/collar.png'),
                    'shirtCollarBg' : ko.observable('gfx/order/image_3d/shirt/collar/collar_bg.png'),
                    'selectedShirtBottom' : ko.observable('gfx/order/image_3d/shirt/bottom/straight-preview.png'),
                    'selectedShirtFront' : ko.observable('gfx/order/image_3d/shirt/front/box-preview.png'),
                    'selectedShirtSeams' : ko.observable(''),
                    'selectedShirtSleeve' : ko.observable('gfx/order/image_3d/shirt/sleeves/normal-preview.png'),
                    'selectedShirtEpaulette' : ko.observable(''),
                    'selectedShirtCollar' : ko.observable('gfx/order/image_3d/shirt/collar/band-preview.png'),
                    'selectedShirtPocketsLeft' : ko.observable(''),
                    'selectedShirtPocketsRight' : ko.observable(''),
                    'shirtBackBg':ko.observable('gfx/order/image_3d/shirt/back/bg.png'),
                    'selectedShirtBack' : ko.observable('gfx/order/image_3d/shirt/back/plain-preview.png'),
                    'shirtBackCollar' : ko.observable(''),
                    'shirtBackCuff' : ko.observable('')

                };

            // default: no image 3d
            } else {
                garment.image3D = null;
            }

            // Pre select fit
            this.preSelectFit(garment);

        	return garment;
        },


        selectGarment: function(measurement) {

            this.selectedGarment(measurement);

        },


        buildGarmentMeasurments : function(measurements) {

            var self = this;

            for(let type of measurements)
            {
                // Checks if there is any template of the TYPE of the measurment and number of instances is bigger than 0.
                type.hasInstances = ko.computed(function() {
                    return self.uniformOrderVM.garmentsStep.templates().filter( e => e.garment_type_name === type.garment_type ).filter( e => e.instances().length > 0).length > 0;
                });


                type.image = ko.observable("");


                type.measurements = ko.mapping.fromJS(type.measurements);


                // puts the correct image and mandatory
                switch(type.garment_type)
                {
                    case 'shirt':
                        type.image('img/order_process/wetransfer-b60a96/shirt.png');
                        for (const measurement of type.measurements()) {
                            if (["shirt_measurements_last_edited", "shirt_measurements_wrist", "shirt_measurements_notes"].includes(measurement.key().toLowerCase())) {
                                measurement.mandatory = false;
                            }
                            else{
                                measurement.mandatory = true;
                            }
                        }
                    break;

                    case 'jacket':
                        type.image('img/order_process/wetransfer-b60a96/jacket.png' );
                        for (const measurement of type.measurements()) {
                            if (["jacket_measurements_last_edited", "jacket_measurements_lapel_length", "jacket_measurements_notes"].includes(measurement.key().toLowerCase())) {
                                measurement.mandatory = false;
                            }
                            else{
                                measurement.mandatory = true;
                            }
                        }
                    break;

                    case 'pants':
                        type.image('img/order_process/wetransfer-b60a96/pant.png');
                        for (const measurement of type.measurements()) {
                            if (["pants_measurements_last_edited", "pants_measurements_front_crotch", "pants_measurements_inseam", "pants_measurements_zipper", "pants_measurements_angled_hems", "pants_measurements_notes"].includes(measurement.key().toLowerCase())) {
                                measurement.mandatory = false;
                            }
                            else{
                                measurement.mandatory = true;
                            }
                        }
                    break;

                    case 'vest':
                        type.image('img/order_process/wetransfer-b60a96/vest.png');
                        for (const measurement of type.measurements()) {
                            if (["vest_measurements_last_edited", "vest_measurements_v_length", "vest_measurements_notes"].includes(measurement.key().toLowerCase())) {
                                measurement.mandatory = false;
                            }
                            else{
                                measurement.mandatory = true;
                            }
                        }
                    break;
                }

            }

            return measurements;
        },


        getLatestCustomerMeasurments : function(customer_id) {

            var self = this;

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/get_latest_customer_measurements',
                dataType: 'json',
                data: {
                    "user" : authCtrl.userInfo,
                    "customer_id" : customer_id
                },
                success: function (dataS) {

                    console.log(dataS);

                    let meas_map = [];

                    if(dataS.measurements)
                    {

                        console.log("setting latest values for measurments");

                        for( let meas of dataS.measurements)
                        {

                            const measType = meas.garment_type;
                            const measurements = meas.measurements;

                            console.log("measType", measType);

                            console.log("measurements", measurements);

                            // gets the current measurments for the given measurment type
                            const currentMeas = self.measurements.filter( meas => meas.garment_type == measType)[0].measurements();


                            // Gets the current match using the measurment keys
                            // not very quick, but the measurment wont grow t0o much
                            for(let measEl of measurements)
                            {
                                const measKey = measEl.key;
                                const latestVal = measEl.value;

                                // match the meas from server with the current one
                                const currentMeasKey = currentMeas.filter( current => current.key() == measKey)[0];

                                // sets the latest value
                                currentMeasKey.value(latestVal);
                            }
                        }
                    }
                    else
                    {
                        self.cleanCustomerMeasurments();
                    }

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: false
            });



        },

        /**
         * Sets zero to all measurements
         */
        cleanCustomerMeasurments : function() {

            console.log("cleaning the measurements...");

            for(let measType of this.measurements)
            {
                for( let measKey of measType.measurements())
                {
                    measKey.value("");
                }
            }

        },

        getFitline : function(meas) {


            if( meas.fitline_key && this.selectedFitline() && this.selectedFitline()[meas.fitline_key()] )
            {
                const fitline_val = this.selectedFitline()[meas.fitline_key()];

                // eval to solve 45.00 + 2.00
                return eval(fitline_val) > 0 ? fitline_val : false;
            }
            else
            {
                return false;
            }


        },

        /**
         * Opens the monogram pop up during the measurements setep
         */
        openMonogramPopUp : function() {

            // Show it with jquery
            $('.monogram-pop-up').show();

            // Selects a garment if there is none
            if( !this.selectedInstance() )
            {
                // simulates a click => easier
                $(".monogram-step-tab-el").first().trigger('click');
            }


        },

        /**
         * Get stitch color from design (data base) to show in shirt's monogram.
         *
         * Return an object to put in image3D contain the values of stitches
         * like this:
         *  [
         *      {id: "1578", image: "gfx/order/values/thread_color/piping_default.jpg", name: "DEFAULT", opt_id: "120", print_order: "0", selected : ko.observable()}​​​​​​​​,
         *      {id: "1484", image: "gfx/order/values/thread_color/piping_1.jpg",       name: "A 1",     opt_id: "120", print_order: "1", selected : ko.observable()}​​​​​​​​,
         *      {id: "1485", image: "gfx/order/values/thread_color/piping_10.jpg",      name: "A 10",    opt_id: "120", print_order: "2", selected : ko.observable()}
         *  ]
         *
         */
        getMonogramStitchColor : function (){
            // get shirt design
            for (const designKey of Object.keys(this.designs)) {
                 if (this.designs[designKey].garment_id == '2'){
                    var shirtCategories = this.designs[designKey].categories;

                    // get monogram category
                    for (const category of shirtCategories) {
                        if (category.id == '39'){
                            var monogramOtions = category.options;

                            // get stitching option
                            for (const stitchOption of monogramOtions) {
                                if (stitchOption.id == '120'){
                                    // clone values
                                    var stitchValues = JSON.parse(JSON.stringify(stitchOption.values));

                                    // put extra data in object:
                                    stitchValues.map(el => el.selected = ko.observable(false));

                                    return stitchValues;
                                }
                            }
                        }
                    }
                }
            }

            // if an erro occurred
            console.log("no stitching found");
            return [];
        },

        /**
         * Get style from design (data base) to show in shirt's monogram.
         *
         * Return an object to put in image3D contain the values of styles
         * like this:
         *  [
         *      {id: "___", image: "____", name: "___", opt_id: "__", print_order: "0", selected : ko.observable()}​​​​​​​​,
         *      {id: "___", image: "____", name: "___", opt_id: "__", print_order: "1", selected : ko.observable()}​​​​​​​​,
         *      {id: "___", image: "____", name: "___", opt_id: "__", print_order: "2", selected : ko.observable()}
         *  ]
         */
        getShirtStyles: function () {
            // get shirt design
            for (const designKey of Object.keys(this.designs)) {
                if (this.designs[designKey].garment_id == '2') {
                    var shirtCategories = this.designs[designKey].categories;

                    // get monogram category
                    for (const category of shirtCategories) {
                        if (category.id == '39') {
                            var monogramOtions = category.options;

                            // get style option
                            for (const styleOption of monogramOtions) {
                                if (styleOption.id == '122') {
                                    // clone set of values
                                    var styleValues = JSON.parse(JSON.stringify(styleOption.values));

                                    // put extra data in object:
                                    styleValues.map(el => el.selected = ko.observable(false));

                                    return styleValues;
                                }
                            }
                        }
                    }
                }
            }

            // if an erro occurred
            console.log("no style found");
            return [];

        },

        /**
         * Get style from design (data base) to show in jacket, vest and suit's monogram.
         *
         * Return an object to put in image3D contain the values of styles
         * like this:
         *  [
         *      {id: "___", image: "____", name: "___", opt_id: "__", print_order: "0", selected : ko.observable()}​​​​​​​​,
         *      {id: "___", image: "____", name: "___", opt_id: "__", print_order: "1", selected : ko.observable()}​​​​​​​​,
         *      {id: "___", image: "____", name: "___", opt_id: "__", print_order: "2", selected : ko.observable()}
         *  ]
         */
        getJacketStyles: function () {
            // Luckily the monogram styles are the same as shirt
            return this.getShirtStyles();

        },

        /**
         * For each garment, of the client, this method will build an object containing data to submit to ERP
         *
         *
         */
        getSubmissionData: function() {

            let measurements = [];

            for(let mea of this.measurements)
            {

                // Only include measurment type of templates that have instances
                if( !mea.hasInstances() ) { continue; }


                const mea_obj = {
                        "garment_type" : mea.garment_type,
                        "fitline_id" : this.selectedFitline() ? this.selectedFitline().jacket_fitline_id : 1, // 1 => no fitline
                        "measurements" : ko.mapping.toJS(mea.measurements),
                        "hasInstances" : mea.hasInstances()
                    }

                measurements.push(mea_obj);
            }


            return measurements;
        },



        /**
         * Sends the measurements to ERP
         * after the measurment step is done.
         */
        syncMeasurements : function() {

            var self = this;

            const measurements = this.getSubmissionData();
            const customer = self.uniformOrderVM.contactPersonStep.selectedCustomer();
            const customer_id = customer.server_id;

            if( parseInt(customer_id) > 0 )
            {
                $.ajax({
                    type: 'POST',
                    timeout: 60000, // sets timeout to 60 seconds
                    url: BUrl + 'orders_pos/sync_measurements',
                    dataType: 'json',
                    data: {
                        "user": authCtrl.userInfo,
                        "measurements": JSON.stringify(measurements),
                        "customer_id" : customer_id
                    },
                    success: function (dataS) {

                        console.log(dataS);

                        if( dataS.result == 'success' )
                        {
                            $.jGrowl(`${ customer.customer_first_name } measurements Sync ! `);
                        }

                    },
                    error: function (error) {
                        console.log(JSON.stringify(error));
                    },
                    async: false
                });

            }


        },

        /**
         * For each garment, of the client, this method will build an object containing data to submit to ERP
         *
         * Para testar: uniformOrder.customerDetailsStep.getSubmissionData()
         *
         * @deprecated
         *      this VM must return only meausrement data.
         *
         *      fit and others templates info come from UniformOrderGarmentsStepsVM
         *
         */
        getSubmissionData_deprecated : function(){
            var data = {};

            // get measurements data
            var measurementData = this.getSubmissionData_measurement();

            // get fit data
            var TemplatesData = [];
            TemplatesData = ko.toJS(this.uniformOrderVM.garmentsStep.templates());

            // remove unnecessary data
            TemplatesData.map(el => {

                Object.keys(el).forEach(function (itm) {
                    if (!["id",
                        "garment_type",
                        "garment_type_name",
                        "instances",
                        "logo_img",
                        "name",
                        "notes",
                        "price",
                        "template_img"
                    ].includes(itm)) {
                        delete el[itm];
                    } // endif

                });

                // remove unnecessary data of 'instance'
                el.instances.map((instance) => {
                    Object.keys(instance).forEach(function (instance_key) {
                        if (!["selectedFit",
                            "image3D"
                        ].includes(instance_key)) {
                            delete instance[instance_key];
                        } // endif
                    })

                    // remove unnecessary data of 'image3D'
                    Object.keys(instance.image3D).forEach(function (image3D_key) {
                        if (![
                            // to shirt
                            "shirtMonogram",
                            "shirtFrontLogos",
                            "shirtBackLogos",
                            "stitchColors", // (get only the selected color)
                            "monogramNotes",

                            // to vest, jacket en suit
                            "monogramLine1",
                            "monogramLine2"
                        ].includes(image3D_key)) {
                            delete instance.image3D[image3D_key];
                        } // endif
                    })

                    // if has key stitchColors
                    if (instance.image3D.stitchColors){
                        // get only the selected color
                        for (const stitchColor of instance.image3D.stitchColors) {
                            if (stitchColor.selected){
                                instance.image3D.stitchColors = stitchColor;
                                break;
                            }else {
                                delete stitchColor;
                            }

                        }

                    }
                });






            });

            // MONOGRAM DATA IS INSIDE IMAGE3D





            data.measurement    = measurementData;
            data.templates            = TemplatesData;
            return data;

        },

        /**
         * Should be called after the users enters in the measurement step
         * @return {[type]} [description]
         */
        autoSelectGarmentType : function() {

            // unselect any garment
            this.selectedGarment(false);

            // simulates a click using jquery to auto select the measurement type:
            if( $(".garment-meas-tab:visible").length )
            {
                $(".garment-meas-tab:visible").first().trigger('click');
            }



        },

        /**
         * Pre select standar fit for recent garment
         * @param {*} garment
         */
        preSelectFit:function (garment){
            this.assignFit(garment, this.availableFits().filter(el => el.id == '3')[0]);
        },

        /**
         * Computed who check all garment type tha have instance has mandatory measurements filled
         */
        checkStepIsComplete: function () {
            return ko.computed(() => {
                for (const measurement_type of this.measurements) {
                    if (measurement_type.hasInstances()) {
                        for (const meas of measurement_type.measurements()) {
                            if (meas.mandatory && !parseFloat(meas.value())) {
                                console.log('Missing ', measurement_type, meas);
                                return false;
                            }
                        }
                    }
                }
                return this.uniformOrderVM.garmentsStep.checkStepIsComplete()();
            })
        },

        /**
         * Copy measurement value to other garments.
         *
         * ex: chest for jacket must be equal chest for shirt
         *
         *
         * @param {Object} measurement
         */
        copyMeasurement : function(measurement){
            var self = this;
            const _setMeas = function(garment_type, meas_key, meas_value){
                // find measurement by key
                const meas_garment = self.measurements.filter(el => el.garment_type == garment_type)[0];

                if (!meas_garment) return ;

                const meas_to_update = meas_garment.measurements().filter(el => el.key() == meas_key)[0];

                // jacket overwrite other garments
                if (garment_type != 'jacket' && meas_to_update){
                    meas_to_update.value(meas_value);
                }

                // update jacket only if value of current value is zero or null
                else if (meas_to_update && [0, NaN].includes(parseInt(meas_to_update.value()))){
                    meas_to_update.value(meas_value);
                }
            }

            // back
            if (['jacket_measurements_back', 'shirt_measurements_back'].includes(measurement.key())) {
                _setMeas('jacket', 'jacket_measurements_back', measurement.value());
                _setMeas('shirt', 'shirt_measurements_back', measurement.value());

            }

            // bicep
            else if (['jacket_measurements_bicep', 'shirt_measurements_bicep'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_bicep', measurement.value());
                _setMeas('shirt', 'shirt_measurements_bicep', measurement.value());

            }

            // chest
            else if (['jacket_measurements_chest', 'vest_measurements_chest', 'shirt_measurements_chest'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_chest', measurement.value());
                _setMeas('vest', 'vest_measurements_chest', measurement.value());
                _setMeas('shirt', 'shirt_measurements_chest', measurement.value());
            }

            // Forearm
            else if (['jacket_measurements_fore_arms', 'shirt_measurements_forearm'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_fore_arms', measurement.value());
                _setMeas('shirt', 'shirt_measurements_forearm', measurement.value());
            }

            // front
            else if (['jacket_measurements_front', 'shirt_measurements_front'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_front', measurement.value());
                _setMeas('shirt', 'shirt_measurements_front', measurement.value());
            }

            // Front
            else if (['jacket_measurements_front', 'shirt_measurements_front'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_front', measurement.value());
                _setMeas('shirt', 'shirt_measurements_front', measurement.value());
            }

            // // Full Shoulder
            // else if (['jacket_measurements_full_shoulder', 'vest_measurements_full_shoulder', 'shirt_measurements_full_shoulder'].includes(measurement.key())){
            //     _setMeas('jacket', 'jacket_measurements_full_shoulder', measurement.value());
            //     _setMeas('vest', 'vest_measurements_full_shoulder', measurement.value());
            //     _setMeas('shirt', 'shirt_measurements_full_shoulder', measurement.value());
            // }

            // // Hips
            // else if (['jacket_measurements_hips', 'shirt_measurements_hips'].includes(measurement.key())){
            //     _setMeas('jacket', 'jacket_measurements_hips', measurement.value());
            //     _setMeas('shirt', 'shirt_measurements_hips', measurement.value());
            // }

            // Left Back to Zero
            else if (['jacket_measurements_right_back_to_zero', 'shirt_measurements_back_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_right_back_to_zero', measurement.value());
                _setMeas('shirt', 'shirt_measurements_back_to_zero', measurement.value());
            }

            // Left Front to Zero
            else if (['jacket_measurements_right_front_to_zero', 'shirt_measurements_front_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_right_front_to_zero', measurement.value());
                _setMeas('shirt', 'shirt_measurements_front_to_zero', measurement.value());
            }

            // // Left Sleeve
            // else if (['jacket_measurements_sleeve_left', 'shirt_measurements_sleeve_left'].includes(measurement.key())){
            //     _setMeas('jacket', 'jacket_measurements_sleeve_left', measurement.value());
            //     _setMeas('shirt', 'shirt_measurements_sleeve_left', measurement.value());
            // }

            // // Neck
            // else if (['jacket_measurements_neck', 'vest_measurements_length', 'shirt_measurements_neck'].includes(measurement.key())){
            //     _setMeas('jacket', 'jacket_measurements_neck', measurement.value());
            //     _setMeas('vest', 'vest_measurements_length', measurement.value());
            //     _setMeas('shirt', 'shirt_measurements_neck', measurement.value());
            // }

            // Right Back to Zero
            else if (['jacket_measurements_back_shoulder_to_zero_point', 'shirt_measurements_right_back_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_back_shoulder_to_zero_point', measurement.value());
                _setMeas('shirt', 'shirt_measurements_right_back_to_zero', measurement.value());
            }

            // Right Front to Zero
            else if (['jacket_measurements_front_shoulder_to_zero_point', 'shirt_measurements_right_front_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_front_shoulder_to_zero_point', measurement.value());
                _setMeas('shirt', 'shirt_measurements_right_front_to_zero', measurement.value());
            }

            // // Right Sleeve
            // else if (['jacket_measurements_sleeve_right', 'shirt_measurements_sleeve_right'].includes(measurement.key())){
            //     _setMeas('jacket', 'jacket_measurements_sleeve_right', measurement.value());
            //     _setMeas('shirt', 'shirt_measurements_sleeve_right', measurement.value());
            // }

            // Stomach
            else if (['jacket_measurements_stomach', 'vest_measurements_neck', 'shirt_measurements_stomach'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_stomach', measurement.value());
                _setMeas('vest', 'vest_measurements_stomach', measurement.value());
                _setMeas('shirt', 'shirt_measurements_stomach', measurement.value());
            }

            // Tense Bicep
            else if (['jacket_measurements_tense_bicep', 'shirt_measurements_tense_bicep'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_tense_bicep', measurement.value());
                _setMeas('shirt', 'shirt_measurements_tense_bicep', measurement.value());
            }

            // Wrist
            else if (['jacket_measurements_wrist', 'shirt_measurements_wrist'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_wrist', measurement.value());
                _setMeas('shirt', 'shirt_measurements_wrist', measurement.value());
            }

            // Zero Point
            else if (['jacket_measurements_zero_point', 'shirt_measurements_zero_point'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_zero_point', measurement.value());
                _setMeas('shirt', 'shirt_measurements_zero_point', measurement.value());
            }



        }

    });

});