define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingBox = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		this.ShippingBoxData     = [];
		this.ShippingBoxDataAID  = 0;
		
		this.workflow    = ko.observableArray([
			{id: 0, target: "#shippingbox",  completed: "false", caption: "ShippingBox", title: "Shipping Box", myclass: "fimage" },
		]);

		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
			$('.cloneDialog').remove();
			that.renderShippingBox(); 
		});

		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});

		this.completion       = ko.observable(0);
		this.variantNameShippingBox = ko.observableArray([{id: 0, title: "ShippingBox 1"}]);
		this.selectedVariantShippingBox = ko.observable(this.variantNameShippingBox()[0]);	
		this.selectedVariantShippingBox.subscribe(function(data) {			
			that.selectedVariantShippingBox();
			that.renderShippingBox();
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.ShippingBoxId = ko.observable('');
		this.ShippingBoxId.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxId = data;
			that.flushModel();
		});	
		this.ShippingBoxUsersList = ko.observableArray();
		this.ShippingBoxUsersList.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxUsersList = data;
			that.flushModel();
		});
		this.ShippingBoxDestinationsList = ko.observableArray();
		this.ShippingBoxDestinationsList.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxDestinationsList = data;
			that.flushModel();
		});
		this.ShippingBoxDestination = ko.observable('');
		this.ShippingBoxDestination.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxDestination = data;
			that.flushModel();
		});
		
		this.ShippingBoxSender = ko.observable('');
		this.ShippingBoxSender.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxSender = data;
			that.flushModel();
		});

		this.ShippingBoxDate = ko.observable('');
		this.ShippingBoxDate.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxDate = data;
			that.flushModel();
		});
		this.ShippingBoxDay = ko.observable('');
		this.ShippingBoxDay.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxDay = data;
			that.flushModel();
		});
		this.ShippingBoxMonth = ko.observable('');
		this.ShippingBoxMonth.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxMonth = data;
			that.flushModel();
		});
		this.ShippingBoxExpectedDate = ko.observable('');
		this.ShippingBoxExpectedDate.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxExpectedDate = data;
			that.flushModel();
		});
		this.ShippingBoxExpectedDay = ko.observable('');
		this.ShippingBoxExpectedDay.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxExpectedDay = data;
			that.flushModel();
		});
		this.ShippingBoxExpectedMonth = ko.observable('');
		this.ShippingBoxExpectedMonth.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxExpectedMonth = data;
			that.flushModel();
		});
		
		this.ShippingBoxMethodsList = ko.observableArray();
		this.ShippingBoxMethodsList.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxMethodsList = data;
			that.flushModel();
		});
		this.ShippingBoxMethod = ko.observable('');
		this.ShippingBoxMethod.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxMethod = data;
			that.flushModel();
		});
		this.ShippingBoxVariousInput = ko.observable('');
		this.ShippingBoxVariousInput.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxVariousInput = data;
			that.flushModel();
		});

		this.ShippingBoxComments = ko.observable('');
		this.ShippingBoxComments.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxComments = data;
			that.flushModel();
		});
		
		this.ShippingBoxTrackingCode = ko.observable('');
		this.ShippingBoxTrackingCode.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxTrackingCode = data;
			that.flushModel();
		});
		this.UncheckedInformation = ko.observable('');
		this.UncheckedInformation.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].UncheckedInformation = data;
			that.flushModel();
		});
		this.ShippingCost = ko.observable('');
		this.ShippingCost.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingCost = data;
			that.flushModel();
		});
		
		this.ShippingBoxContents = ko.observableArray();
		this.ShippingBoxContents.subscribe(function(data) {		
			that.ShippingBoxData[ that.getRow(that.selectedVariantShippingBox().id)  ].ShippingBoxContents = data;
			that.flushModel();
		});
		
		this.transmuteDate = function(date){  // input is yyyy-mm-dd
			if(date != undefined && date != null){				
				var day = date.substring(date.lastIndexOf("-") + 1);
				var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
				var year = date.substring(0, date.indexOf("-"));
				return day + "-" + month  + "-" + year;
			}else{
				return 0;
			}	
		};
		
		this.timestampToDate = function(UNIX_timestamp){
  			var a = new Date(UNIX_timestamp);
  			var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  			var year = a.getFullYear();
  			var month = months[a.getMonth()];
  			var date = a.getDate();
  			var time = date + '/' + (a.getMonth() + 1) + '/' + year;
  			return time;
		};
		
		this.modifiedListOptionsFunction = function(elemname) { // 0: element name
			var checkboxes = document.getElementsByName( elemname );					
			for (var a in this.ShippingBoxContents() ) { 
				if (this.ShippingBoxContents()[a].checked == true){
					checkboxes[a].classList.add('selecteds');
				}else{
					checkboxes[a].classList.remove('selecteds');
				}
			}
		};
		
		
		this.showContactInfo = function(index){
			var countries = ko.observable(dsRegistry.getDatasource('countriesDS').getStore());	
			var country = "";				
			for (i in countries()){
				if(countries()[i].locations_id == this.ShippingBoxContents()[index].customer.customer_country){
					country = countries()[i].location_name;
				}
			}
			var states = ko.observable(dsRegistry.getDatasource('statesDS').getStore());	
			var state = "";				
			for (i in states()){
				if(states()[i].locations_id == this.ShippingBoxContents()[index].customer.customer_state){
					state = states()[i].location_name;
				}
			}
			var cities = ko.observable(dsRegistry.getDatasource('citiesDS').getStore());	
			var city = "";				
			for (i in cities()){
				if(cities()[i].locations_id == this.ShippingBoxContents()[index].customer.customer_city){
					city = cities()[i].location_name;
				}
			}
			document.getElementById("customer_first_name").innerHTML = this.ShippingBoxContents()[index].customer.customer_first_name;
			document.getElementById("customer_last_name").innerHTML = this.ShippingBoxContents()[index].customer.customer_last_name;
			document.getElementById("customer_address1").innerHTML = this.ShippingBoxContents()[index].customer.customer_address1;
			document.getElementById("customer_address2").innerHTML = this.ShippingBoxContents()[index].customer.customer_address2;
			document.getElementById("customer_postal_code").innerHTML = this.ShippingBoxContents()[index].customer.customer_postal_code;
			document.getElementById("customer_mobile_phone").innerHTML = this.ShippingBoxContents()[index].customer.customer_mobile_phone;
			document.getElementById("customer_landline_phone").innerHTML = this.ShippingBoxContents()[index].customer.customer_landline_phone;
			document.getElementById("customer_email").innerHTML = this.ShippingBoxContents()[index].customer.customer_email;
			document.getElementById("customer_country").innerHTML = country;
			document.getElementById("customer_state").innerHTML = state;
			document.getElementById("customer_city").innerHTML = city;
			if ("createEvent" in document) {
		    	var evt = document.createEvent("HTMLEvents");
		    	evt.initEvent("change", false, true);
		    	document.getElementById("customer_first_name").dispatchEvent(evt);
				document.getElementById("customer_last_name").dispatchEvent(evt);
				document.getElementById("customer_address1").dispatchEvent(evt);
				document.getElementById("customer_address2").dispatchEvent(evt);
				document.getElementById("customer_postal_code").dispatchEvent(evt);
				document.getElementById("customer_mobile_phone").dispatchEvent(evt);
				document.getElementById("customer_landline_phone").dispatchEvent(evt);
				document.getElementById("customer_email").dispatchEvent(evt);
				document.getElementById("customer_country").dispatchEvent(evt);
				document.getElementById("customer_state").dispatchEvent(evt);
				document.getElementById("customer_city").dispatchEvent(evt);
			}else{
		    	document.getElementById("customer_first_name").fireEvent("onchange");
				document.getElementById("customer_last_name").fireEvent("onchange");
				document.getElementById("customer_address1").fireEvent("onchange");
				document.getElementById("customer_address2").fireEvent("onchange");
				document.getElementById("customer_postal_code").fireEvent("onchange");
				document.getElementById("customer_mobile_phone").fireEvent("onchange");
				document.getElementById("customer_landline_phone").fireEvent("onchange");
				document.getElementById("customer_email").fireEvent("onchange");
				document.getElementById("customer_country").fireEvent("onchange");
				document.getElementById("customer_state").fireEvent("onchange");
				document.getElementById("customer_city").fireEvent("onchange");
			}
			document.getElementById("ContactInfoPopup").style.display = "block";
		};		
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////	

		this.save_new_shipping_method = function(){
			var newmethodname = this.ShippingBoxVariousInput().trim();
			var found = false;
			for(var x in this.ShippingBoxMethodsList()){
				if(this.ShippingBoxMethodsList()[x].shipping_method_name == newmethodname){
					found == true;
				}
			}
			if(newmethodname == ""){
				customAlert("Please fill in the name of the new shipping method.");
			}else if(found == true){
				customAlert("This shipping method already exists.");
			}else{
				var method = new Object();
				method.shipping_method_id = "0";
				method.shipping_method_name = newmethodname;
				var temp_list = this.ShippingBoxMethodsList();
				temp_list.push(method);
				this.ShippingBoxMethodsList(temp_list);
				this.ShippingBoxVariousInput('');
				document.getElementById("NewShippingMethodBox").style.display = "none";
				customAlert("The shipping method has been successfully added.");				
			}
		};
		
		this.save_new_destination = function(){
			
			var stock_location_name = document.getElementById("stock_location_name").value;
			var stock_location_address = document.getElementById("stock_location_address").value;
			var stock_location_postal = document.getElementById("stock_location_postal").value;
			var stock_location_suburb = document.getElementById("stock_location_suburb").value;
			var e = document.getElementById("stock_location_city");
			var stock_location_city = e.options[e.selectedIndex].value;
			e = document.getElementById("location_name");
			var location_name = e.options[e.selectedIndex].value;
			var stock_location_country_id = "";
			for(var x in customersVM.countries()){
				if(location_name == customersVM.countries()[x].location_name){
					stock_location_country_id = customersVM.countries()[x].locations_id;
				}
			}
			e = document.getElementById("user");
			var user_id = e.options[e.selectedIndex].value;
			var user = "";
			for(var x in this.ShippingBoxUsersList()){
				if(user_id == this.ShippingBoxUsersList()[x].user_id){
					user = this.ShippingBoxUsersList()[x];
				}
			}
			if(stock_location_name.trim() == ""){
				customAlert("Please enter a destination title");
			}else if(stock_location_address.trim() == ""){
				customAlert("Please enter the destination address");
			}else if(stock_location_postal.trim() == ""){
				customAlert("Please enter the postal code");
			}else if(stock_location_suburb.trim() == ""){
				customAlert("Please enter the suburb");
			}else if(stock_location_city.trim() == ""){
				customAlert("Please select the city");
			}else if(location_name.trim() == ""){
				customAlert("Please select the country");
			}else if(user == undefined || user == ""){
				customAlert("Please select the recipient");
			}else{
//console.log("all checks passed");
				var destination = new Object();
				destination.stock_location_id = "0";
				destination.stock_location_name = stock_location_name;
				destination.stock_location_address = stock_location_address;
				destination.stock_location_postal = stock_location_postal;
				destination.stock_location_suburb = stock_location_suburb;
				destination.stock_location_city = stock_location_city;
				destination.stock_location_country_id = stock_location_country_id;
				destination.stock_location_linked_users = [];
				destination.stock_location_linked_users.push(user)
				destination.stock_location_last_edited = "" + Math.round(new Date().getTime() / 1000);
				destination.location_name = location_name;
				
				var temp_list = this.ShippingBoxDestinationsList();
				temp_list.push(destination);
				this.ShippingBoxDestinationsList(temp_list);
				
				document.getElementById("NewDestinationDialog").style.display = "none";
				customAlert("The destination has been successfully added.");
			}
		};

		this.box_received_cancel = function(){
			document.getElementById("BoxConfirm").style.display = "none";
			document.getElementById("BoxNotAllCheckedConfirm").style.display = "none";
		};	

		this.box_received_step_1 = function(){
			var checked = false;
			for (var a in this.ShippingBoxContents() ) { 
				if (this.ShippingBoxContents()[a].checked == true){
					checked = true;
					break;
				}
			}
			if(checked == false){
				customAlert("Please check the box contents that you have received");
			}else{
				document.getElementById("box_sender").innerHTML = this.ShippingBoxSender();
				document.getElementById("box_send_date").innerHTML = this.transmuteDate(this.ShippingBoxDate());
				if ("createEvent" in document) {
					var evt = document.createEvent("HTMLEvents");
					evt.initEvent("change", false, true);
					document.getElementById("box_sender").dispatchEvent(evt);
					document.getElementById("box_send_date").dispatchEvent(evt);
				}else{
					document.getElementById("box_sender").fireEvent("onchange");
					document.getElementById("box_send_date").dispatchEvent(evt);	
				}
				document.getElementById("BoxConfirm").style.display = "block";
			}
		};
		
		this.box_received_step_2 = function(){
			var found_unchecked = false;
			for (var a in this.ShippingBoxContents() ) { 
				if (this.ShippingBoxContents()[a].checked == false){
					found_unchecked = true;
					break;
				}
			}
			if(found_unchecked == true){ // not all box contents where checked, so show the second dialog that contains the text field
				document.getElementById("BoxConfirm").style.display = "none";
				document.getElementById("BoxNotAllCheckedConfirm").style.display = "block";
			}else{
				this.shipping_box_receive();
			}
		};

		this.box_received_step_3 = function(){
			if(this.UncheckedInformation().trim() == ""){ // not all box contents where checked, so show the second dialog that contains the text field
				customAlert("Please enter information concerning the box contents that you have not received");
			}else{
				this.shipping_box_receive();
			}
		};	


		this.shipping_box_receive = function() {
			document.getElementById("BoxConfirm").style.display = "none";
			document.getElementById("BoxNotAllCheckedConfirm").style.display = "none";
			var spinner = document.getElementById('loading_jp');	
			spinner.style.display = "block";	

			var items_array = [];
			var found_unchecked = false;
			for (var a in this.ShippingBoxContents() ) { 
				if (this.ShippingBoxContents()[a].checked == true){
					var obj = new Object();
		   			obj.shipping_created_item_id = this.ShippingBoxContents()[a].shipping_created_item_id;
					items_array.push(obj);
				}else{
					found_unchecked = true;
				}
			}
			
			var unchecked_information = "";
			if(found_unchecked == true){
				unchecked_information = this.UncheckedInformation().trim();	
			}
				
			var salesman = new Object();
		   	salesman.username = authCtrl.username();
		   	salesman.password  = authCtrl.password();
		   	salesman.id  = authCtrl.userInfo.user_id;
		   		
			var data = { 
				salesman: salesman,
				shipping_items_box_id: this.ShippingBoxId(),
				items_received: items_array,
				shipping_items_box_issue_notes: unchecked_information
			};	
								
			var datatopost = JSON.stringify(data);
			datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);

			$.post(BUrl + 'client_logistics/shipping_box_receive_endpoint', {
				data: datatopost
			},
		    function(dataS){
		       	var spinner = document.getElementById('loading_jp');
				spinner.style.display = "none";
				
		       	dataS = JSON.parse(dataS);
		           if (dataS.valid == '1') {
		           	customAlert("Succesfully added");
		               posChangePage('#shipping_main');
				}else{
					console.log('fail');
		               $.jGrowl(dataS.msg);
				}
			});
 
	        spinner.style.display = "none";       		
		};
		
		this.removeItem = function(index){
			var temp_array = [];
			for(var x in this.ShippingBoxContents()){
				if(x != index){
					temp_array.push(this.ShippingBoxContents()[x]);
				}
			}
			this.ShippingBoxContents(temp_array);
			if(this.ShippingBoxContents().length == 0){
				customersVM.systemMode ='shipping_pending';
				posChangePage('#shipping_box');
			}
		}
		
		
		this.print_labels = function() {
			var items_array = [];
			var has_checked = false;
			for (var a in this.ShippingBoxContents() ) { 
				if (this.ShippingBoxContents()[a].checked == true){
					var obj = new Object();
		   			obj.shipping_created_item_id = this.ShippingBoxContents()[a].shipping_created_item_id;
					items_array.push(obj);
					has_checked = true;
				}
			}
			
			var email = this.ShippingBoxVariousInput().trim();
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		var result =  re.test(email);
			
			if(has_checked == false){
				customAlert("Please select the items that you want to print the labels for");
			}else if(email == "" || result == false){
				customAlert("Please enter a valid email address");
			}else{
				var spinner = document.getElementById('loading_jp');	
				spinner.style.display = "block";
				
				var salesman = new Object();
			   	salesman.username = authCtrl.username();
			   	salesman.password  = authCtrl.password();
			   	salesman.id  = authCtrl.userInfo.user_id;
			   		
				var data = { 
					salesman: salesman,
					items: items_array,
					email: email
				};	
									
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
	console.log("datatopost: " + datatopost);
				$.post(BUrl + 'client_logistics/shipping_labels_endpoint', {
					data: datatopost
				},
			    function(dataS){
			       	var spinner = document.getElementById('loading_jp');
					spinner.style.display = "none";
					
			       	dataS = JSON.parse(dataS.trim());
			           if (dataS.valid == '1') {
			           	document.getElementById("PrintLabelsEmailDialog").style.display = "none";
			           	customAlert("Succesfully added");
			            //   posChangePage('#shipping_main');
					}else{
						console.log('fail');
			               $.jGrowl(dataS.msg);
					}
				});
 			} 		
		};		
		
		this.create_new_box = function(){
			var temp_box = new Object();
			temp_box.destinations = this.ShippingBoxDestinationsList();
			temp_box.shipping_methods = this.ShippingBoxMethodsList();
			temp_box.users = this.ShippingBoxUsersList();
			temp_box.items = [];
			for (var a in this.ShippingBoxContents() ) { 
				if (this.ShippingBoxContents()[a].checked == true){
					temp_box.items.push(this.ShippingBoxContents()[a]);
				}
			}
			
			if(temp_box.items.length == 0){
				customAlert("Please select the items that you want to send");	
			}else{	
				localStorage.removeItem('ShippingNewBoxDS');
				orderItem.ShippingNewBoxVM = new ShippingBox(orderItem.dsRegistry);
				orderItem.ShippingNewBoxDS = new defShippingNewBox('ShippingNewBoxDS', orderItem.dsRegistry, temp_box);
				orderItem.ShippingNewBoxVM.subscribeTo('ShippingNewBoxDS');
	console.log("orderItem.ShippingNewBoxDS.getStore(): " + JSON.stringify( orderItem.ShippingNewBoxDS.getStore() ) );
	 			orderItem.ShippingNewBoxVM.addVariantsShippingNewBox();
				posChangePage('#shipping_box_send');	
			}	
		};		
		
		this.send_box_confirm = function(){
			
			var images = localStorage.getItem('BoxSendPhoto');			
			images = (images == null) ? [] : JSON.parse(images);
			
			var dateToPost = '';		
			var date = new Date();
			var yearnow = date.getFullYear();
			var monthnow = date.getMonth() + 1;
			var daynow = date.getDate();
			var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
			var dateToCompare;
			if(this.ShippingBoxMonth() != undefined && this.ShippingBoxMonth() !="" && this.ShippingBoxDay() != undefined && this.ShippingBoxDay() != ""){ 
				if( this.ShippingBoxMonth() < monthnow || (this.ShippingBoxMonth() == monthnow && this.ShippingBoxDay() < daynow) ){
					var yearnew = yearnow + 1;
					dateToPost = this.ShippingBoxDay() + "-" + this.ShippingBoxMonth() + "-" + yearnew;
					dateToCompare = new Date(this.ShippingBoxMonth() + "/" + this.ShippingBoxDay() + "/" + yearnew); 
				}else{
					dateToPost = this.ShippingBoxDay() + "-" + this.ShippingBoxMonth() + "-" + yearnow;
					dateToCompare = new Date(this.ShippingBoxMonth() + "/" + this.ShippingBoxDay() + "/" + yearnow);
				}
			}
			var diffDays = -1;
			try{
				var timeDiff = dateToCompare.getTime() - dateNow.getTime();
				diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				console.log("diffDays: " + diffDays);
			}catch(e){ ; }
			
			
			var dateToPostExpected = '';		
			var dateToCompareExpected;
			if(this.ShippingBoxExpectedMonth() != undefined && this.ShippingBoxExpectedMonth() !="" && this.ShippingBoxExpectedDay() != undefined && this.ShippingBoxExpectedDay() != ""){ 
				if( this.ShippingBoxExpectedMonth() < monthnow || (this.ShippingBoxExpectedMonth() == monthnow && this.ShippingBoxExpectedDay() < daynow) ){
					var yearnew = yearnow + 1;
					dateToPostExpected = this.ShippingBoxExpectedDay() + "-" + this.ShippingBoxExpectedMonth() + "-" + yearnew;
					dateToCompareExpected = new Date(this.ShippingBoxExpectedMonth() + "/" + this.ShippingBoxExpectedDay() + "/" + yearnew); 
				}else{
					dateToPostExpected = this.ShippingBoxExpectedDay() + "-" + this.ShippingBoxExpectedMonth() + "-" + yearnow;
					dateToCompareExpected = new Date(this.ShippingBoxExpectedMonth() + "/" + this.ShippingBoxExpectedDay() + "/" + yearnow);
				}
			}
			var diffDaysExpected = -1;
			try{
				var timeDiff = dateToCompareExpected.getTime() - dateNow.getTime();
				diffDaysExpected = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				console.log("diffDaysExpected: " + diffDaysExpected);
			}catch(e){ ; }
			
			var diffDaysExpectedShipped = -1;
			try{
				var timeDiff = dateToCompareExpected.getTime() - dateToCompare.getTime();
				diffDaysExpectedShipped = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				console.log("diffDaysExpectedShipped: " + diffDaysExpectedShipped);
			}catch(e){ ; }
			
			
			if(diffDays < 0){
				customAlert("There has been an error with the selected date. <br/> Try again or select another.");
			}else if(diffDays > 120){
				customAlert("The shipping date is more than 4 months away! <br/> Please select a closer date.");
			}else if(diffDays < - 30){
				customAlert("The shipping date is more than a month ago! <br/> Please select a closer date.");				
			}else if(diffDaysExpected > 120){
				customAlert("The expected date is more than 4 months away! <br/> Please select a closer date.");	
			}else if(diffDaysExpectedShipped < 0){
				customAlert("The expected date is before the shipping date! <br/> Please select a correct date.");
			}else if(diffDaysExpected < - 30){
				customAlert("The expected date is more than a month ago! <br/> Please select a closer date.");
			}else if(this.ShippingBoxDestination() == ""){
				customAlert("Please select the destination");
			}else if(this.ShippingCost() == ""){
				customAlert("Please enter the shipping cost");
			}else if(images.length == 0){
				customAlert("Please upload the receipt image");	
			}else if(this.ShippingBoxTrackingCode() == ""){
				customAlert("Please enter the tracking code");	
			}else if(this.ShippingBoxMethod() == ""){
				customAlert("Please enter the shipping method");
			}else{
				this.ShippingBoxDate(dateToPost);
				this.ShippingBoxExpectedDate(dateToPostExpected);
				var text = "You are sending a box with " + this.ShippingBoxContents().length + " items with the following details:<br/><br/>";
				text += "Destination: " + this.ShippingBoxDestination().stock_location_name + "<br/>";
				text += "Cost: " + this.ShippingCost() + "<br/>";
				text += "Shipping Date: " + this.ShippingBoxDate() + "<br/>";
				text += "Expected Date: " + this.ShippingBoxExpectedDate() + "<br/>";
				text += "Tracking Code: " + this.ShippingBoxTrackingCode() + "<br/>";
				text += "Tracking Method: " + this.ShippingBoxMethod().shipping_method_name + "<br/>";
				text += "Items included " + this.ShippingBoxContents().length + "<br/>";
				text += "Comments: " + this.ShippingBoxComments() + "<br/><br/>";
				 
				document.getElementById("dialogtext").innerHTML = text;
				if ("createEvent" in document) {
					var evt = document.createEvent("HTMLEvents");
					evt.initEvent("change", false, true);
					document.getElementById("dialogtext").dispatchEvent(evt);
				}else{
					document.getElementById("dialogtext").fireEvent("onchange");	
				}
				document.getElementById("BoxConfirm").style.display = "block";
			}	
		};		
		
		
		this.send_box = function() {
			var spinner = document.getElementById('loading_jp');	
			spinner.style.display = "block";
			
			var photosarray = [];
			var images = localStorage.getItem('BoxSendPhoto');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('BoxSendPhoto') != null){
				if(localStorage.getItem('BoxSendPhoto').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}	

			var salesman = new Object();
		   	salesman.username = authCtrl.username();
		   	salesman.password  = authCtrl.password();
		   	salesman.id  = authCtrl.userInfo.user_id;
			   		
			var data = { 
				salesman: salesman,
				items: this.ShippingBoxContents(),
				destination: this.ShippingBoxDestination(),
				cost: this.ShippingCost(),
				shipping_date: this.ShippingBoxDate(),
				expected_date: this.ShippingBoxExpectedDate(),
				tracking_code: this.ShippingBoxTrackingCode(),
				shipping_method: this.ShippingBoxMethod(),
				comments: this.ShippingBoxComments(),
				images: photosarray
			};	
									
			var datatopost = JSON.stringify(data);
			datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
			$.post(BUrl + 'client_logistics/shipping_box_endpoint', {
				data: datatopost
			},
		    function(dataS){
		       	var spinner = document.getElementById('loading_jp');
				spinner.style.display = "none";
				
		       	dataS = JSON.parse(dataS);
	           if (dataS.valid == '1') {
		           customAlert("Succesfully added");
	               posChangePage('#shipping_main');
				}else{
					console.log('fail');
	               $.jGrowl(dataS.msg);
				}
			});
	        spinner.style.display = "none";       		
		};				
		

///////////////////////////////////////////////////////////////////////////////////////////////////////////////			
		
		
		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingBox"  : this.ShippingBoxData
		}, true);
	},

	getVariant: function(id) {
		var toreturn = this.ShippingBoxData[0];
		for (var ind in this.ShippingBoxData) {
			if ( this.ShippingBoxData[ind].variantId == id  ) {
				toreturn = this.ShippingBoxData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingBoxData) {
			if ( this.ShippingBoxData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantsShippingBox: function() {
		for(var x in orderItem.ShippingBoxVM.ShippingBoxData){
			if( x != 0){
				this.ShippingBoxDataAID += 1;
				var vname = "" + x;//orderItem.ShippingBoxVM.ShippingBoxData[x].ShippingBoxDate; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingBox().id ) )  ); //CLONE Object
				this.variantNameShippingBox.push({title: vname, id: this.ShippingBoxDataAID});
				tObj.variantId = this.ShippingBoxDataAID;		
				this.flushModel();
			}else{
				var vname = "" + x;//orderItem.ShippingBoxVM.ShippingBoxData[x].ShippingBoxDate;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingBox().id ) )  ); //CLONE Object
				this.variantNameShippingBox()[x].title = vname;
				tObj.variantId = this.ShippingBoxDataAID;		
				this.flushModel();
			}
		}	
	},
	
	addVariantsShippingNewBox: function() {
		for(var x in orderItem.ShippingNewBoxVM.ShippingBoxData){
			if( x != 0){
				this.ShippingBoxDataAID += 1;
				var vname = "" + x;//orderItem.ShippingBoxVM.ShippingBoxData[x].ShippingBoxDate; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingBox().id ) )  ); //CLONE Object
				this.variantNameShippingBox.push({title: vname, id: this.ShippingBoxDataAID});
				tObj.variantId = this.ShippingBoxDataAID;		
				this.flushModel();
			}else{
				var vname = "" + x;//orderItem.ShippingBoxVM.ShippingBoxData[x].ShippingBoxDate;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingBox().id ) )  ); //CLONE Object
				this.variantNameShippingBox()[x].title = vname;
				tObj.variantId = this.ShippingBoxDataAID;		
				this.flushModel();
			}
		}	
	},
		
		


	
//////////////////////////////////////////////////////////////////////////////////////////////////	
	

	digestData: function(data) {
		this.ShippingBoxData  = data.ShippingBox;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingBox();
	},

	renderShippingBox: function() {	
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantShippingBox().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.ShippingBoxId)		!= "undefined") {this.ShippingBoxId(tData.ShippingBoxId);}
				if (typeof(tData.ShippingBoxDestination)		!= "undefined") {this.ShippingBoxDestination(tData.ShippingBoxDestination);}
				if (typeof(tData.ShippingBoxSender)		!= "undefined") {this.ShippingBoxSender(tData.ShippingBoxSender);}
				if (typeof(tData.ShippingBoxDate)		!= "undefined") {this.ShippingBoxDate(tData.ShippingBoxDate);}
				if (typeof(tData.ShippingBoxDay)		!= "undefined") {this.ShippingBoxDay(tData.ShippingBoxDay);}
				if (typeof(tData.ShippingBoxMonth)		!= "undefined") {this.ShippingBoxMonth(tData.ShippingBoxMonth);}
				if (typeof(tData.ShippingBoxExpectedDate)		!= "undefined") {this.ShippingBoxExpectedDate(tData.ShippingBoxExpectedDate);}
				if (typeof(tData.ShippingBoxExpectedDay)		!= "undefined") {this.ShippingBoxExpectedDay(tData.ShippingBoxExpectedDay);}
				if (typeof(tData.ShippingBoxExpectedMonth)		!= "undefined") {this.ShippingBoxExpectedMonth(tData.ShippingBoxExpectedMonth);}
				if (typeof(tData.ShippingBoxMethod)		!= "undefined") {this.ShippingBoxMethod(tData.ShippingBoxMethod);}
				if (typeof(tData.ShippingBoxVariousInput)		!= "undefined") {this.ShippingBoxVariousInput(tData.ShippingBoxVariousInput);}
				if (typeof(tData.ShippingBoxComments)    	!= "undefined") {this.ShippingBoxComments(tData.ShippingBoxComments);}
				if (typeof(tData.ShippingBoxTrackingCode)  	!= "undefined") {this.ShippingBoxTrackingCode(tData.ShippingBoxTrackingCode);}
				if (typeof(tData.UncheckedInformation)  	!= "undefined") {this.UncheckedInformation(tData.UncheckedInformation);}
				if (typeof(tData.ShippingCost)  	!= "undefined") {this.ShippingCost(tData.ShippingCost);}
				if (typeof(tData.ShippingBoxContents)  		!= "undefined") {this.ShippingBoxContents(tData.ShippingBoxContents);}
				if (typeof(tData.ShippingBoxUsersList)  		!= "undefined") {this.ShippingBoxUsersList(tData.ShippingBoxUsersList);}	
				if (typeof(tData.ShippingBoxDestinationsList)  		!= "undefined") {this.ShippingBoxDestinationsList(tData.ShippingBoxDestinationsList);}
				if (typeof(tData.ShippingBoxMethodsList)  		!= "undefined") {this.ShippingBoxMethodsList(tData.ShippingBoxMethodsList);}									
			}
		}catch(e){
			;
		}
	}
});

defShippingBox = SimpleDatasource.extend({
	init: function( name, dsRegistry, data ) {
		if(data == null || data == undefined){
console.log("data is null");
			var ShippingBox = {};
			ShippingBox.variantId = 0;
			ShippingBox.ShippingBoxId = '';
			ShippingBox.ShippingBoxUsersList = [];
			ShippingBox.ShippingBoxDestinationsList = [];
			ShippingBox.ShippingBoxDestination = '';
			ShippingBox.ShippingBoxSender = '';
			ShippingBox.ShippingBoxDate = '';
			ShippingBox.ShippingBoxDay = '';
			ShippingBox.ShippingBoxMonth = '';
			ShippingBox.ShippingBoxExpectedDate = '';
			ShippingBox.ShippingBoxExpectedDay = '';
			ShippingBox.ShippingBoxExpectedMonth = '';
			ShippingBox.ShippingBoxMethodsList = [];
			ShippingBox.ShippingBoxMethod = '';
			ShippingBox.ShippingBoxVariousInput = '';
			ShippingBox.ShippingBoxComments = '';
			ShippingBox.ShippingBoxTrackingCode = '';
			ShippingBox.UncheckedInformation = '';
			ShippingBox.ShippingCost = '';
			ShippingBox.ShippingBoxContents = [];

			var iShippingBox = {
				ShippingBox: []
			};
			iShippingBox.ShippingBox.push(ShippingBox);
			this._super(name, iShippingBox, dsRegistry);
		}else{
//console.log("data is not null");
			var iShippingBox = {
				ShippingBox: []
			};
			var ShippingBox = {};
			ShippingBox.variantId = 0;
			ShippingBox.ShippingBoxId = data.shipping_items_box_id;
			ShippingBox.ShippingBoxUsersList = data.users;
			ShippingBox.ShippingBoxDestinationsList = data.destinations;
			ShippingBox.ShippingBoxDestination = '';
			ShippingBox.ShippingBoxSender = data.sender_stock_location_name; //data.shipping_items_box_sender;
			ShippingBox.ShippingBoxDate = data.shipping_items_box_sent_date;
			ShippingBox.ShippingBoxDay = '';
			ShippingBox.ShippingBoxMonth = '';
			ShippingBox.ShippingBoxExpectedDate = data.shipping_items_box_expected_date;
			ShippingBox.ShippingBoxExpectedDay = '';
			ShippingBox.ShippingBoxExpectedMonth = '';
			ShippingBox.ShippingBoxMethod = data.shipping_method_name;
			ShippingBox.ShippingBoxVariousInput = '';
			ShippingBox.ShippingBoxMethodsList = data.shipping_methods;
			ShippingBox.ShippingBoxComments = data.shipping_items_box_comments;
			ShippingBox.ShippingBoxTrackingCode = data.shipping_items_box_tracking_code;
			ShippingBox.UncheckedInformation = '';
			ShippingBox.ShippingCost = data.shipping_items_box_cost;
		//	ShippingBox.ShippingBoxContents = data.items;
			ShippingBox.ShippingBoxContents = [];
			for(var a in data.items){
				ShippingBox.ShippingBoxContents.push(data.items[a]);
			}
			
			for(var a in ShippingBox.ShippingBoxContents){
				ShippingBox.ShippingBoxContents[a].checked = false;
				if(ShippingBox.ShippingBoxContents[a].customer == undefined){
					ShippingBox.ShippingBoxContents[a].customer = JSON.parse('{"customer_first_name":"-","customer_last_name":"","customer_address1":"-","customer_address2":"-","customer_country":"-","customer_state":"-","customer_city":"-","customer_postal_code":"-","customer_mobile_phone":"-","customer_landline_phone":"-","customer_email":"-"}');
				}
			}
			iShippingBox.ShippingBox.push(ShippingBox);
			this._super(name, iShippingBox, dsRegistry);
		}
	}
});

defShippingNewBox = SimpleDatasource.extend({
	init: function( name, dsRegistry, data ) {
		var ShippingBox = {};
		ShippingBox.variantId = 0;
		ShippingBox.ShippingBoxId = '';
		ShippingBox.ShippingBoxUsersList = data.users;
		ShippingBox.ShippingBoxDestinationsList = data.destinations;
		ShippingBox.ShippingBoxDestination = '';
		ShippingBox.ShippingBoxSender = '';
		ShippingBox.ShippingBoxDate = '';
		ShippingBox.ShippingBoxDay = '';
		ShippingBox.ShippingBoxMonth = '';
		ShippingBox.ShippingBoxExpectedDate = '';
		ShippingBox.ShippingBoxExpectedDay = '';
		ShippingBox.ShippingBoxExpectedMonth = '';
		ShippingBox.ShippingBoxMethod = '';
		ShippingBox.ShippingBoxVariousInput = '';
		ShippingBox.ShippingBoxMethodsList = data.shipping_methods;
		ShippingBox.ShippingBoxComments = '';
		ShippingBox.ShippingBoxTrackingCode = '';
		ShippingBox.UncheckedInformation = '';
		ShippingBox.ShippingCost = '';
		//	ShippingBox.ShippingBoxContents = data.items;
		ShippingBox.ShippingBoxContents = [];
		for(var a in data.items){
			ShippingBox.ShippingBoxContents.push(data.items[a]);
		}
		
		for(var a in ShippingBox.ShippingBoxContents){
			ShippingBox.ShippingBoxContents[a].checked = false;
			if(ShippingBox.ShippingBoxContents[a].customer == undefined){
				ShippingBox.ShippingBoxContents[a].customer = JSON.parse('{"customer_first_name":"-","customer_last_name":"","customer_address1":"-","customer_address2":"-","customer_country":"-","customer_state":"-","customer_city":"-","customer_postal_code":"-","customer_mobile_phone":"-","customer_landline_phone":"-","customer_email":"-"}');
			}
		}

		var iShippingBox = {
			ShippingBox: []
		};
		iShippingBox.ShippingBox.push(ShippingBox);
		this._super(name, iShippingBox, dsRegistry);
	}
});

//END DEFINE CLOSURE
});
