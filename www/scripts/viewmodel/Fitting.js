define(['jquery', 'knockout', 'base', 'viewmodel/FittingErrorReportVM', 'viewmodel/FittingTailorsWorkLoadVM', 'viewmodel/MakeOneFirstToApprove'], function ($, ko) {

	// ███████╗██╗████████╗████████╗██╗███╗   ██╗ ██████╗
	// ██╔════╝██║╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝
	// █████╗  ██║   ██║      ██║   ██║██╔██╗ ██║██║  ███╗
	// ██╔══╝  ██║   ██║      ██║   ██║██║╚██╗██║██║   ██║
	// ██║     ██║   ██║      ██║   ██║██║ ╚████║╚██████╔╝
	// ╚═╝     ╚═╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝
	//
	Fitting = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var that = this;

			window.debug_fitting = this;

			this.FittingData = [];
			this.FittingDataAID = 0;
			this.NumberShirts = 0;
			this.editFittingVideoNotes = ko.observable(-1);
			//this.shirtCloneMessage = ko.observable(true);
			this.changedVideo = ko.observable(false);

			this.previousStepEnabled = ko.observable(false);
			this.skipAppear = ko.observable(true); /** sets skip button visible or not */
			this.skipAppearVideo = ko.observable(true);
			this.skipAppearVideoNotes = ko.observable(false);

			this.mediaUpload = orderItem.MediaUploadVM;
			this.mediaID = this.mediaUpload.mediaID;
			this.tagText = ko.observable('');
			this.media = this.mediaUpload.media;
			this.control = 0;
			this.previewFrontImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.previewSideImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.previewBackImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.previewCustomImage = ko.observableArray([]);
			this.previewCustomVideo = ko.observableArray([]);

			this.fittingTailorsWorkLoad = new TailorsWorkLoad();

			this.makeOneFirstToApprove = new MakeOneFirstToApprove();


			this.updateSkipButtons = function () {

				that.skipAppearVideo(!that.FittingVideo.hasMedia());
				console.log(that.FittingVideo.anyMediaHavetags());
				that.skipAppearVideoNotes(!that.FittingVideo.anyMediaHavetags() && that.FittingVideo.hasMedia());
			};

			this.mediaCallback = function (data) {

				console.log("meediacallback", data);

				console.log(orderItem.MediaUploadVM.pictureTaken());
				if (data) {
					console.log('Fitting medias length after take picture');
					console.log(orderItem.MediaUploadVM.medias().length);

					if (that.currentStep().id == 0 /*|| that.currentStep().id == 2 // I think only step 1 is for upload imgs */) {

						console.log("Putting media in " + img_type);

						// Global variable that tells the application which image is being used right now.
						// Then, the POS knows if the user uplaoded a FRONT/BACK/SIDE/Additional image
						switch (img_type) {
							case 'FittingFrontImage': that.frontImage.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true); break;
							case 'FittingSideImage': that.sideImage.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true); break;
							case 'FittingBackImage': that.backImage.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true); break;
							case 'FittingCustomImage': that.customImages.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true); break;

							default: console.error("IMG TYPE NOT DEFINED!"); break;
						}



					}
					if (that.currentStep().id == 1) {
						that.FittingVideo.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true);
						//that.updateSkipButtons();
						return;
					} else if (that.currentStep().id < 1) {
						$.jGrowl("Your image was saved successfully! Proceed");
						//customAlert('Your image was saved successfully! Proceed');
						// that.nextStep();
					}

				}
			};

			this.mediaUpload.setMediaCallback(this.mediaCallback);

			this.setText = function (text, observable) {
				observable(text);
			};

			this.editFittingVideoNotes.subscribe(function (data) {
				if (data > -1 && that.FittingVideoNotes().length > data) {
					that.FittingVideoNotesSingle(that.FittingVideoNotes()[data]);
				} else if (data > -1) {
					that.editFittingVideoNotes(-1);
				}
			});

			this.correctDate = function (date) {
				if (date != undefined && date != null) {
					if (date == -1 || date == "-1") {
						return 0;
					} else {
						if (date.indexOf(" ") >= 0) {
							date = date.substring(0, date.indexOf(" "));
						}
						var day = date.substring(date.lastIndexOf("-") + 1);
						var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
						var year = date.substring(0, date.indexOf("-"));
						return day + "/" + month + "/" + year;
					}
				} else {
					return 0;
				}
			};

			this.mapperID = function (index) {
				return "mapper" + index;
			};

			this.customImagesHaveTags = function () {

				const hasCustomImgs = orderItem.fittingsVM.FittingData[orderItem.fittingsVM.selectedVariantFitting().id].customImages.media().length;

				if (hasCustomImgs) {
					const hasTags = orderItem.fittingsVM.FittingData[orderItem.fittingsVM.selectedVariantFitting().id].customImages.media()[0].tags().length > 0;

					if (hasTags) {
						return true;
					}
					else {
						return false;
					}
				}
				else {
					return false;
				}

			};

			/** Called to add a tag .
			 * Open a form to set tag text
			*/
			this.dialog = function (mediaData, evt, imageIndex, id, isAdditionalImage=false) {
				this.mediaData = mediaData;
				this.evt = evt;
				this.imageIndex = imageIndex;
				if (id >= 0) {
					this.idCustomImage = id;
				}
				else {
					this.idCustomImage = -1;
				}
				that.tagText('');
				var tag = this.mediaData.buildTagPicture('', this.evt, this.imageIndex);
				if (this.idCustomImage >= 0) {
					$("#mapper" + this.idCustomImage).css("left", tag.left + '%').css("top", tag.top + '%').css("width", "35px").css("height", "35px").show();
				}
				else {
					$('div.mapper').css("left", tag.left + '%').css("top", tag.top + '%').css("width", "35px").css("height", "35px").show();
				}

				orderItem.openDialog();

				// we can only attach fitting images to error if the image is additional
				// because of that we will hide the select for "front", "back" and "side" images
				if(isAdditionalImage){
					// show and select "it's an error select"
					$("input#use_as_error").attr('checked', false);
					$("label#use_as_error_label").closest("div").show();

				} else {
					// hide and unselect "it's an error select"
					$("input#use_as_error").attr('checked', false);
					$("label#use_as_error_label").closest("div").hide();

				}
			};

			/**
			 * This code is a shit. I don't know where else the dialog function is called, but we need to change it.
			 *
			 * the change: We need to inform FittingErrorReportVM which image we are setting image_tag (if it is front, back side or any additional)
			 *
			 * @param {*} mediaData
			 * @param {*} evt
			 * @param {*} imageIndex
			 * @param {*} id
			 * @param {String} imageType - Must be "front", "back", "side", "additional"
			 * @param {int} additionalIndex - in case of additional, we must to set a index because we can have more than one additional image
			 *
			 */
			this.dialog2 = function(mediaData, evt, imageIndex, id, imageType, additionalIndex=null){
				if (!["front", "back", "side", "additional"].includes(imageType)){
					console.warn('imageType: Must be "front", "back", "side", "additional"');
					return;
				}

				if (imageType === 'additional' && (additionalIndex == null)){
					console.warn('missing additionalIndex param');
					return;
				}

				const whichGarment = this.selectedVariantFitting().id;
				this.FittingData[whichGarment].fittingErrorsReport.currentTaggingImage = imageType;
				this.FittingData[whichGarment].fittingErrorsReport.currentTaggingImage_additionalIndex = additionalIndex;

				const isAdditionalImage = (imageType == "additional" ? true : false);
				this.dialog(mediaData, evt, imageIndex, id, isAdditionalImage);
			}

			/**
			 * After answer the form in this.dialog, add the images's tag
			 */
			this.addTag = function (text) {
				console.log("adding image tags...", text);
				console.log("adding for...", this.imageIndex);

				$("div.form_panel").hide();
				$('div.mapper').hide();

				const whichGarment = this.selectedVariantFitting().id;

				// if "use_as_error" is marked, show error popup
				if($("#popup_box #use_as_error").is(":checked")){
					// create a new error and open popup
					this.FittingData[whichGarment].fittingErrorsReport.addNewErrorReport();

					// Copy & paste tag_note into description error
					// this.FittingData[whichGarment].fittingErrorsReport.currentErrorReport().notes(this.tagText());

					// add a copy of this image into fitting error
					const currentTaggingImage = this.FittingData[whichGarment].fittingErrorsReport.currentTaggingImage;
					const currentTaggingImage_additionalIndex = this.FittingData[whichGarment].fittingErrorsReport.currentTaggingImage_additionalIndex;

					let imagePath = '';
					switch (currentTaggingImage) {
						case 'front':
							imagePath = this.frontImage.media().preview();
							break;
						case 'back':
							imagePath = this.backImage.media().preview();
							break;
						case 'side':
							imagePath = this.sideImage.media().preview();
							break;
						case 'additional':
							if (currentTaggingImage_additionalIndex != null){
								imagePath = this.customImages.media()[currentTaggingImage_additionalIndex].preview();
								break;
							} else {
								console.warn('"currentTaggingImage_additionalIndex" is undefined');
								imagePath = '';
								return null;
							}
						default:
							console.warn("currentTaggingImage is undefined");
							return null;
					}

					// add this image to fitting error
					this.FittingData[whichGarment].fittingErrorsReport.currentErrorReport().addImage(imagePath);

					// add image tag to error
					this.FittingData[whichGarment].fittingErrorsReport.currentErrorReport().newImageTag(this.tagText(), this.evt, currentTaggingImage, currentTaggingImage_additionalIndex);




				} else {
					this.mediaData.addTag(this.mediaData.buildTagPicture(this.tagText(), this.evt, this.imageIndex), this.imageIndex);

				}

				this.tagText('');
				this.skipAppear(false);

				// This will fix the bug, of hidding the tags after confirming the first one
				//openAddNotePopUp('frontImage');
			};

			this.takePicture = function () {
				that.mediaUpload.choosePhoto();
				that.control = 1;
			};

			this.recordVideo = function () {
				console.log("recordVideo...");
				that.mediaUpload.chooseVideo();
			}

			this.uploadErrorVideo = function(){
				const whichGarment = this.selectedVariantFitting().id;

				// create a new error and open this error popup
				this.FittingData[whichGarment].fittingErrorsReport.addNewErrorReport();

				// call function to select which video source
				this.FittingData[whichGarment].fittingErrorsReport.currentErrorReport().chooseVideoSource();
			}

			this.cloneShirts = function (data) {
				if (that.NumberShirts > 1) {
					if (!data) {
						data = that.selectedVariantFitting()
					}
					whichGarment = data.id;
					that.FittingData[whichGarment].FittingCloned = 0;
					if (!that.FittingData[whichGarment].SkippedFrontTags && !that.FittingData[whichGarment].SkippedSideTags && !that.FittingData[whichGarment].SkippedBackTags && !that.FittingData[whichGarment].SkippedVideo && !that.FittingData[whichGarment].SkippedVideoNotes) {
						for (var i in that.FittingData) {
							if (i != whichGarment && that.variantNameFitting()[i].title == "Shirt") {
								var tmp = localStorage.getItem('FittingFrontImageTags' + whichGarment);
								localStorage.setItem('FittingFrontImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingSideImageTags' + whichGarment);
								localStorage.setItem('FittingSideImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingBackImageTags' + whichGarment);
								localStorage.setItem('FittingBackImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingCustomImageTags' + whichGarment);
								localStorage.setItem('FittingCustomImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);
								localStorage.setItem('FittingFrontImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
								localStorage.setItem('FittingCustomImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
								localStorage.setItem('FittingSideImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
								localStorage.setItem('FittingBackImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
								localStorage.setItem('FittingCustomImagePhoto' + i, tmp);
								that.FittingData[i].AdditionalImagesStepComplete = that.FittingData[whichGarment].AdditionalImagesStepComplete;
								that.FittingData[i].FittingVideo = that.FittingData[whichGarment].FittingVideo;

								that.FittingData[i].NoFrontTags = that.FittingData[whichGarment].NoFrontTags;
								that.FittingData[i].NoSideTags = that.FittingData[whichGarment].NoSideTags;
								that.FittingData[i].NoBackTags = that.FittingData[whichGarment].NoBackTags;
								that.FittingData[i].NoVideo = that.FittingData[whichGarment].NoVideo;
								that.FittingData[i].FittingCloned = that.FittingData[whichGarment].FittingOrderProductId;
								that.FittingData[i].FittingVideoNotes = [];
								for (x in that.FittingData[whichGarment].FittingVideoNotes) {
									that.FittingData[i].FittingVideoNotes.push(that.FittingData[whichGarment].FittingVideoNotes[x]);
								}
							}
						}

						that.assignTailorText();
						that.renderFitting();
						that.checkSteps();
					}
					else {
						customAlert("Can't clone a garment with skipped steps. Please complete all before cloning.");
					}
				}
			}

			/**
			 * Confirm continue without image note
			 */
			this.NoTagsConfirm = function () {

				/*
				text = "Are you sure you want to continue without adding Notes on the image?";
				var asd = document.getElementById("fittingsDialogText123");
				asd.innerHTML = text;

				$("#fittingsDialogText123").text(text);
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "block";
				*/

				return true;
			};

			/**
			 * Confirm continue without video
			 */
			this.NoVideoConfirm = function () {
				text = "Are you sure you want to continue without adding a Video?";
				document.getElementById("fittingsDialogText123").innerHTML = text;
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "block";
				return true;
			};

			/**
			 * Confirm continue without video note
			 */
			this.NoVideoNotesConfirm = function () {
				// text = "Are you sure you want to continue without adding Video Notes?";
				// document.getElementById("fittingsDialogText123").innerHTML = text;
				// dialog = document.getElementById('fittingsDialog123');
				// dialog.style.display = "block";
				return true;
			};

			this.buttonNoCloneAccept = function () {
				dialog = document.getElementById('dialogClone');
				dialog.style.display = "none";
				this.goToFirstUncompletedStep();
			};

			this.buttonYesCloneAccept = function () {
				that.cloneShirts(that.selectedVariantFitting());
				dialog = document.getElementById('dialogClone');
				dialog.style.display = "none";
				this.goToFirstUncompletedStep();
			};

			/** sets the corresponding flags as false
			 */
			this.buttonNoAccept = function () {
				var cs = that.currentStep().id; /**current step ID */
				var whichGarment = that.selectedVariantFitting().id;
				if (cs == 0) {
					// additional image
					that.AdditionalImagesStepComplete(false);
					that.workflow()[0].completed = "false";

				} else if (cs == 2) {
					// tags
					that.NoFrontTags(false);
					that.SkippedFrontTags(false);

					that.NoSideTags(false);
					that.SkippedSideTags(false);

					that.NoBackTags(false);
					that.SkippedBackTags(false);
				} else if (cs == 1) {
					// videos
					that.NoVideo(false);
					that.SkippedVideo(false);
				}

				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "none";
			}

			/** sets the corresponding flags as true*/
			this.buttonYesAccept = function () {
				console.log("buttonYesAccept");
				var cs = that.currentStep().id;
				var whichGarment = that.selectedVariantFitting().id;
				if (cs == 0) {
					// images
					that.AdditionalImagesStepComplete(true);
					that.selectedVariantFitting().steps()[cs].completed = "true";

				} else if (cs == 3) {
					// tags
					that.NoFrontTags(true);
					that.SkippedFrontTags(false);
					that.selectedVariantFitting().steps()[cs].completed = "true";

					that.NoSideTags(true);
					that.SkippedSideTags(false);
					that.selectedVariantFitting().steps()[cs].completed = "true";

					that.NoBackTags(true);
					that.SkippedBackTags(false);
					that.selectedVariantFitting().steps()[cs].completed = "true";

				} else if (cs == 1) {
					// videos
					that.NoVideo(true);
					//that.SkippedVideo(false);
					//that.SkippedVideoNotes(false);
					that.selectedVariantFitting().steps()[cs].completed = "true";
				}
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "none";

				this.goToFirstUncompletedStep();
			}

			/** check if current step are completed then go to next step */
			this.nextStep = function () {

				var cs = that.currentStep().id;
				var whichGarment = that.selectedVariantFitting().id;

				img = document.getElementById('imagesize');

				console.log('height ' + img.height + ' width ' + img.width);

				const _nextStep = () => {
					// go to next step or repeat
					if (that.selectedVariantFitting().steps()[cs].completed == "true") {
						this.goToFirstUncompletedStep();

					} else {
						that.currentStep(that.selectedVariantFitting().steps()[cs]);
					}
				}

				// sets current step as completed
				// images
				if (cs == 0) {
					if (that.frontImage.hasMedia() && that.sideImage.hasMedia() && that.backImage.hasMedia()) {
						// that.workflow()[cs].completed = "true";
						that.selectedVariantFitting().steps()[cs].completed = "true";
					} else {

						if (!that.frontImage.hasMedia()) {
							customAlert("Please upload a front image");

						}

						else if (!that.sideImage.hasMedia()) {
							customAlert("Please upload a side image");
						}

						else if (!that.backImage.hasMedia()) {
							customAlert("Please upload a back image");
						}

					}


					// additional images (no more needed)
					// initially set as false
					that.AdditionalImagesStepComplete(false);
					if (that.customImages.hasMedia()) {
						if (that.customImages.anyMediaHavetags()) {
							that.AdditionalImagesStepComplete(true);
						} else {
							this.NoTagsConfirm(); // confirm continue without tag
						}
					}
					else {
						that.AdditionalImagesStepComplete(true);
					}

					// tags
				}
				// image notes
				else if (cs == 2) {
					// start with false
					that.selectedVariantFitting().steps()[cs].completed = "false";

					// only can be complete if image upload is complete
					if (that.selectedVariantFitting().steps()[0].completed == "false"){
						that.currentStep(that.selectedVariantFitting().steps()[0]); // go to image upload
						that.selectedVariantFitting().steps()[cs].completed = that.selectedVariantFitting().steps()[0].completed;
						return;
					}

					const _nextGarment = () => {
						// checks if this is the last garment in the group
						const totalNumberOfGarments = orderItem.fittingsVM.FittingData.length;
						if (whichGarment + 1 == totalNumberOfGarments) // IF true => this is the last garment
						{
							console.log("LAST GARMENT...");
							that.currentStep(that.workflow()[3]); // 3 == assign tailor step
							return;
						}
					}

					// CHECK IF ANY MEDIA HAVE TAGS
					if (
						that.FittingData[that.selectedVariantFitting().id].useAsCompletionImages() ||
						(that.frontImage.allMediaHavetags() || that.sideImage.allMediaHavetags() || that.backImage.allMediaHavetags())
						) {
						that.selectedVariantFitting().steps()[cs].completed = "true";
						_nextGarment();

					}
					else if (
						!that.FittingData[that.selectedVariantFitting().id].useAsCompletionImages() &&
						!(that.frontImage.allMediaHavetags() || that.sideImage.allMediaHavetags() || that.backImage.allMediaHavetags())
						) {
						const confirmAction = () => {
							that.selectedVariantFitting().steps()[cs].completed = that.selectedVariantFitting().steps()[0].completed;
							_nextGarment();
							_nextStep();
						};

						const cancelAction = () => (that.selectedVariantFitting().steps()[cs].completed = "false");

						pos_confirm({
							msg: "Are you sure you want to continue without any tag?",
							confirmBtn: "YES",
							cancelBtn: "NO",
							confirmAction,
							cancelAction,})
					}

					else{
						_nextGarment();
						_nextStep();
					}



				}
				// videos
				else if (cs == 1) {

					that.SkippedVideo(false);
					// that.workflow()[cs].completed = "false";
					that.selectedVariantFitting().steps()[cs].completed = "false";
					if ( that.FittingData[that.selectedVariantFitting().id].useAsCompletionImages() || that.FittingVideo.hasMedia()) {
						if (that.FittingVideo.allMediaHavetags()) {
							// that.workflow()[cs].completed = "true";
							that.selectedVariantFitting().steps()[cs].completed = "true";
							that.SkippedVideo(false);
							that.SkippedVideoNotes(true);
						}
						else {
							console.log("Videos dont need tags");
							// that.workflow()[cs].completed = "true";
							that.selectedVariantFitting().steps()[cs].completed = "true";
							that.SkippedVideo(false);
							that.SkippedVideoNotes(true);
							//this.NoVideoNotesConfirm();
						}
					}
					else {
						this.NoVideoConfirm();
					}
				}

				_nextStep();
			}

			/**
			 * @deprecated
			 */
			this.goToFirstUncompletedStep_deprecated = function () {
				var cs = that.currentStep().id;
				var foundnotcompletedstep = false;
				for (var x = 0; x < that.workflow().length - 1; x++) {
					if (that.workflow()[x].completed == "false") {
						that.currentStep(that.workflow()[x]);
						foundnotcompletedstep = true;
						break;
					}
				}
				if (foundnotcompletedstep == false) {
					var variantIncompleted = false;
					that.selectedVariantFitting().completed(true);
					if (that.FittingId() != '') {
						if (that.SkippedFrontTags() || that.SkippedSideTags() || that.SkippedBackTags()) {
							that.currentStep(that.workflow()[2]);
						} else if (that.SkippedVideo() || that.SkippedVideoNotes()) {
							that.currentStep(that.workflow()[1]);
						}
						else {
							that.currentStep(that.workflow()[3]);
						}
					}
					else {
						for (var i = 0; i < that.variantNameFitting().length; i++) {
							if (that.variantNameFitting()[i].completed() === false) {
								variantIncompleted = true;
								break;
							}
						}
						if (variantIncompleted) {
							that.selectedVariantFitting(that.variantNameFitting()[i]);
							that.goToFirstUncompletedStep();
						}
						else {
							that.currentStep(that.workflow()[3]);
						}
					}
				}

			}

			this.goToFirstUncompletedStep = function () {

				// if is completionImage, set video and note steps as 'complete' (this steps are not mandatory)
				const isCompletionImage = that.FittingData[that.selectedVariantFitting().id].useAsCompletionImages();
				if (isCompletionImage){
					that.selectedVariantFitting().steps().forEach(step => {
						// don't touch assign taylor step
						if(step.id != '3') {
							step.completed = 'true';
						}
					});
				}

				// search unvisited steps
				let unvistedSteps = that.selectedVariantFitting().steps().filter(step => step.completed != 'true');

				// go to next uncompleted step (attention to ASSIGN TAILOR)
				if (unvistedSteps.length > 1) {

					that.currentStep(unvistedSteps.filter(step => step.id != '3')[0]);
					return;
				}

				// if only next step is ASSIGN TAILOR, check other garments first
				else if (unvistedSteps.length == 1) {
					// set current garment as complete
					that.selectedVariantFitting().completed(true);

					let nextStep = unvistedSteps[0];

					// check other uncompleted garments
					let otherGarments = that.variantNameFitting().filter(garment => garment.completed() == false);

					// if other garments has uncompleted step (attention to ASSIGN TAILOR), go to it
					if (otherGarments.length > 0) {
						// change garment, than go to next uncompleted step
						that.selectedVariantFitting(otherGarments[0]);
						that.goToFirstUncompletedStep();
						return;
					}

					// if not, go to assign tailor
					else {
						that.currentStep(unvistedSteps.filter(step => step.id == '3')[0]);
						return;
					}
				}

			}

			this.checkSteps = function () {

				var whichGarment = that.selectedVariantFitting().id;
				console.log("checkSteps " + whichGarment);
				that.workflow()[0].completed = "false";
				if (that.frontImage.hasMedia()) {
					that.workflow()[0].completed = "true";
				}


				// do not set tags as uncompleted
				// that.workflow()[2].completed = "false";
				// if (that.NoFrontTags() == true) {
				// 	that.workflow()[2].completed = "true";
				// } else if (that.frontImage.allMediaHavetags()) {
				// 	that.workflow()[2].completed = "true";
				// 	that.NoFrontTags(false)
				// 	that.SkippedFrontTags(false)
				// }
				// else if (that.SkippedFrontTags() == true) {
				// 	that.workflow()[2].completed = "pending";
				// }

				that.workflow()[1].completed = "false";
				if (that.FittingId() != '' && that.SkippedVideo() == false) {
					that.workflow()[1].completed = "true";
				} else if (that.NoVideo() == true) {
					that.workflow()[1].completed = "true";

				// Video should never be pending anymore
				// } else if (that.SkippedVideo() == true || that.SkippedVideoNotes()) {
				// 	that.workflow()[1].completed = "pending";

				}
				else if (this.FittingVideo.hasMedia()) {
					that.workflow()[1].completed = "true";
				}

				that.workflow()[3].completed = "false";
				if (that.FittingId() != '' && that.FittingTailor() != undefined && that.FittingDeliveryMonth() != undefined && that.FittingDeliveryMonth() != ""
					&& that.FittingDeliveryDay() != undefined && that.FittingDeliveryDay() != "") {
					that.workflow()[3].completed = "true";
				}

				return '';
			};

			this.skipVideoComments = function () {
				that.SkippedVideoNotes(true);
				this.goToFirstUncompletedStep();
			};

			this.skipStep = function () {
				var whichGarment = that.selectedVariantFitting().id;
				var cs = that.currentStep().id;
				// that.workflow()[cs].completed = "true";
				console.log("skipStep " + cs);
				if (cs == 2) {
					localStorage.removeItem('FittingFrontImageTags' + whichGarment);
					that.NoFrontTags(false);
					that.SkippedFrontTags(true);

					localStorage.removeItem('FittingSideImageTags' + whichGarment);
					that.NoSideTags(false);
					that.SkippedSideTags(true);

					localStorage.removeItem('FittingBackImageTags' + whichGarment);
					that.NoBackTags(false);
					that.SkippedBackTags(true);
				} else if (cs == 2) {
					that.SkippedVideo(true);
				}
				this.goToFirstUncompletedStep();
			}

			this.goToNextNotSubmittedFitting = function () {
				console.log("that.FittingData.length() " + this.FittingData.length);
				var found = false;
				for (var x = 1; x < this.FittingData.length; x++) {
					console.log("garment " + x + "  FittingId: " + this.FittingData[x].FittingId);
					if (this.FittingData[x].FittingId == '') {
						found = true;
						this.selectedVariantFitting(this.variantNameFitting()[x]);
						this.goToFirstUncompletedStep();
						break;
					}
				}
				if (found == false) {
					posChangePage('#customerGarmentsFittingsList');
				}
			}

			this.workflow = ko.observableArray([
				{
					id: 0, target: "#fittings", caption: "Fitting", title: "UPLOAD IMAGE", myclass: "fimage",
					completed: "false"
				},
				{
					id: 1, target: "#fittings", caption: "Fitting", title: "UPLOAD VIDEO", myclass: "fvideo",
					completed: "false"
				},
				{
					id: 2, target: "#fittings", caption: "Fitting", title: "ADD NOTES", myclass: "fnote",
					completed: "false"
				},
				{
					id: 3, target: "#fittings", caption: "Fitting", title: "ASSIGN TAILOR", myclass: "ftailor",
					completed: "false"
				}
			]);


			this.currentStep = ko.observable();
			this.currentStep(this.workflow()[0]);
			this.currentStep.subscribe(function (data) {
				var whichGarment = that.selectedVariantFitting().id;
				// find current step array position and put in x variable
				for (x in that.workflow()) {
					if (that.workflow()[x] == data) {
						break;
					}
				}

				if (x == 2) {
					if (that.frontImage.allMediaHavetags()) {
						that.skipAppear(false);
					} else {
						that.skipAppear(true);
					}

					if (that.sideImage.allMediaHavetags()) {
						that.skipAppear(false);
					} else {
						that.skipAppear(true);
					}

					if (that.backImage.allMediaHavetags()) {
						that.skipAppear(false);
					} else {
						that.skipAppear(true);
					}
				}
				console.log(JSON.stringify(data));
				console.log(JSON.stringify(data.target));
				posChangePage(data.target);
				$('.cloneDialog').remove();
				that.renderFitting();


				// refreshing wrapper
				that.refreshAdditionalImgsWrapper();




			});

			this.isStepCompleted = ko.computed(function () {
				for (var ind in that.workflow()) {
					if (that.workflow()[ind].id == that.currentStep().id) {
						if (that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending") {
							return true;
						} else {
							return false;
						}
					}
				}
				return false;
			});


			this.stepCaption = ko.computed(function () {
				return that.currentStep().caption;
			});

			this.stepTitle = ko.computed(function () {
				return that.currentStep().title;
			});


			this.completion = ko.observable(0);
			this.variantNameFitting = ko.observableArray([{ id: 0, title: "Fitting 1", completed: ko.observable(false),  steps: ko.observableArray(ko.toJS(this.workflow))}]);
			this.selectedVariantFitting = ko.observable(this.variantNameFitting()[0]);

			/**
			 * OnChangeSelectedVarianFitting:
			 */
			this.selectedVariantFitting.subscribe(function (data) {
				that.selectedVariantFitting();
				//	posChangePage("#fittings");
				//	$('.cloneDialog').remove();
				that.renderFitting();

				var cs = that.currentStep().id;
				that.currentStep(that.workflow()[cs]);

				// refreshing img wrapper
				that.refreshAdditionalImgsWrapper();

			});

			/**
			 * This functin will be responsible to show/hide the additonal img wrapper for tagging images
			 *
			 * this is because the knockout is not wokring very well for its observables : orderItem.fittingsVM.customImages.media().length
			 *
			 * thus, we need to manually check the value of the variable to check if there is additonal images or not for the GIVEN garment.
			 * @return {[type]} [description]
			 */
			this.refreshAdditionalImgsWrapper = function() {


				// This will refresh the wrapper for additional images
				// and show/hide the wrapper every time the garment is changed.
				console.log("Manually checking additinal images visibility...");

				console.log("orderItem.fittingsVM.customImages.media().length", orderItem.fittingsVM.customImages.media().length);

				if( orderItem.fittingsVM.customImages.media().length > 0 )
				{
					console.log("showing wrapper...");
					$(".additonal-img-wrapper-manual").show();
				}
				else
				{
					console.log("hidding wrapper...");
					$(".additonal-img-wrapper-manual").hide();
				}

				// end refreshing the wrapper

			}


			this.nextStepCaption = ko.computed(function () {
				var tWork = that.workflow();
				var tInd = -1;
				for (var ind in tWork) {
					if (tWork[ind].completed !== "true") {
						tInd = ind;
						break;
					}
				}
				if (tInd != -1) {
					return tWork[tInd].title;
				} else {
					return " Not available ";
				}
			});

			this.previousStepCaption = ko.computed(function () {
				if (that.currentStep().id !== 0) {
					return that.workflow()[that.currentStep().id - 1].title;
				} else {
					return " Not available ";
				}
			});


			this.FittingCustomer = ko.observable('');
			this.FittingCustomer.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingCustomer = data;
				that.flushModel();
			});

			this.FittingGarment = ko.observable('');
			this.FittingGarment.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarment = data;
				that.flushModel();
			});

			this.FittingGarmentFabric = ko.observable('');
			this.FittingGarmentFabric.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarmentFabric = data;
				that.flushModel();
			});


			this.FittingGarmentFabricImage = ko.observable('');
			this.FittingGarmentFabricImage.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarmentFabricImage = data;
				that.flushModel();
			});

			this.FittingGarmentBarcode = ko.observable('');
			this.FittingGarmentBarcode.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarmentBarcode = data;
				that.flushModel();
			});

			this.FittingId = ko.observable('');
			this.FittingId.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingId = data;
				that.flushModel();
			});

			this.FittingOrderId = ko.observable('');
			this.FittingOrderId.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingOrderId = data;
				that.flushModel();
			});

			this.FittingOrderProductId = ko.observable('');
			this.FittingOrderProductId.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingOrderProductId = data;
				that.flushModel();
			});

			this.FittingTailors = ko.observable(that.dsRegistry.getDatasource('tailorsDS').getStore());
			this.FittingTailor = ko.observable('');
			this.FittingTailor.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingTailor = data;
				}

				that.flushModel();
			});
			this.FittingDeliveryDay = ko.observable('');
			this.FittingDeliveryDay.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingDeliveryDay = data;
				}

				that.flushModel();
			});
			this.FittingDeliveryMonth = ko.observable('');
			this.FittingDeliveryMonth.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingDeliveryMonth = data;
				}

				that.flushModel();
			});

			this.FittingDeliveryDate = ko.observable('');
			this.FittingDeliveryDate.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingDeliveryDate = data;
				}
				that.flushModel();
			});

			this.FittingTailorNotes = ko.observable('');
			this.FittingTailorNotes.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailorNotes = data;
				that.flushModel();
			});

			this.FittingVideoPreview = ko.observable('');
			this.FittingVideoPreview.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingVideoPreview = data;
				console.log('Fitting video preview data ');
				console.log(that.FittingVideoPreview());
				console.log(data);
				that.flushModel();

			});

			this.SkippedVideo = ko.observable(false);
			this.SkippedVideo.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedVideo = data;
				that.flushModel();
			});

			this.SkippedVideoNotes = ko.observable(true);
			this.SkippedVideoNotes.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedVideoNotes = data;
				that.flushModel();
			});


			this.NoVideo = ko.observable(false);
			this.NoVideo.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoVideo = data;
				that.flushModel();
			});

			this.SkippedFrontTags = ko.observable(false);
			this.SkippedFrontTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedFrontTags = data;
				that.flushModel();
			});
			this.SkippedSideTags = ko.observable(false);
			this.SkippedSideTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedSideTags = data;
				that.flushModel();
			});
			this.SkippedBackTags = ko.observable(false);
			this.SkippedBackTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedBackTags = data;
				that.flushModel();
			});
			this.NoFrontTags = ko.observable(false);
			this.NoFrontTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoFrontTags = data;
				that.flushModel();
			});
			this.NoSideTags = ko.observable(false);
			this.NoSideTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoSideTags = data;
				that.flushModel();
			});
			this.NoBackTags = ko.observable(false);
			this.NoBackTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoBackTags = data;
				that.flushModel();
			});
			this.isActive = ko.observable(true);
			this.isActive.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].isActive = data;
				that.flushModel();
			});
			this.isCompleted = ko.observable(false);
			this.isCompleted.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].isCompleted = data;
				that.flushModel();
			});

			this.AdditionalImagesStepComplete = ko.observable(false);
			this.AdditionalImagesStepComplete.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].AdditionalImagesStepComplete = data;
				that.flushModel();
			});


			this.FittingVideoNotes = ko.observableArray();
			this.FittingVideoNotes.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingVideoNotes = data;
				that.flushModel();
			});

			this.FittingCloned = ko.observable(0);
			this.FittingCloned.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingCloned = data;
				that.flushModel();
			});

			this.CalendarDate = ko.observable(new Date());
			this.CalendarDate.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].CalendarDate = data;
				that.flushModel();
			});

			this.FittingVideoNotesSingle = ko.observable('');

			this.addComment = function () {
				var temp = [];
				temp = that.FittingVideoNotes();
				console.log(temp);
				if (that.FittingVideoNotesSingle()) {
					if (that.editFittingVideoNotes() < 0) {
						temp.push(that.FittingVideoNotesSingle());
						that.FittingVideoNotesSingle(null);
					}
					else {
						temp[that.editFittingVideoNotes()] = that.FittingVideoNotesSingle();
						that.FittingVideoNotesSingle(null);
						that.editFittingVideoNotes(-1);
					}
					that.FittingVideoNotes(temp);
				}
			}

			this.deleteComment = function (index) {
				const confirmAction = () => {
					for (x in that.FittingVideoNotes()) {
						if (that.FittingVideoNotes()[x] == index) {
							index = x;

						}

					}
					that.FittingVideoNotes.splice(index, 1);
					that.editFittingVideoNotes(-1);

				};
				pos_confirm({msg:'Do you wish to delete delete this note?', confirmAction});
			}

			this.FittingVideoViewModel = function (thepath, thenotes) {
				var self = this;
				this.path = ko.observable(thepath);
				this.notes = ko.observable(thenotes);
			};

			this.assignTailorText = function () {
				document.getElementById("roundup").innerHTML = '';
				var a = '';


				document.getElementById("roundup").innerHTML = a;
				if ("createEvent" in document) {
					var evt = document.createEvent("HTMLEvents");
					evt.initEvent("change", false, true);
					document.getElementById("roundup").dispatchEvent(evt);
					//	document.getElementById("Test").dispatchEvent(evt);
				} else {
					document.getElementById("roundup").fireEvent("onchange");
					//	document.getElementById("Test").fireEvent("onchange");
				}

				return a;
			};

			this.ImageTagsText = function (id) {
				var a = "";
				//for(i in that.variantNameFitting()){
				var whichGarment = id;
				var fronttags = JSON.parse(localStorage.getItem('FittingFrontImageTags' + whichGarment));//  [{"image_id":"1400","left":73.994140625,"top":12.560059502720833,"text":"asasdasdasdasd"},{"image_id":"1400","left":69.6875,"top":34.89892669022083,"text":"bbbbbbbbbbbbbbbbbb"}]
				var sidetags = JSON.parse(localStorage.getItem('FittingSideImageTags' + whichGarment));
				var backtags = JSON.parse(localStorage.getItem('FittingBackImageTags' + whichGarment));
				var customtags = JSON.parse(localStorage.getItem('FittingCustomImageTags' + whichGarment));

				//a += that.variantNameFitting()[i].title;
				var count = 1;
				var temp = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				if (temp) {
					a += '<div class="grid_12"><div class="grid_4 centerme"><span>Front Image</span><img src="' + temp[0].thumbnail + '" width="95%"/><ul>';
					for (var x in fronttags) {
						a += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + fronttags[x].text + '</span></li>';
						count++;
					}
					a += '</ul></div>';
				}

				count = 1;
				temp = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				if (temp) {
					a += '<div class="grid_4 centerme"><span>Side Image</span><img src="' + temp[0].thumbnail + '" width="95%"/><ul>';
					for (var x in sidetags) {
						a += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + sidetags[x].text + '</span></li>';
						count++;
					}
					a += '</ul></div>';
				}

				count = 1;
				temp = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				if (temp) {
					a += '<div class="grid_4 centerme"><span>Back Image</span><img src="' + temp[0].thumbnail + '" width="95%"/><ul>';
					for (var x in backtags) {
						a += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + backtags[x].text + '</span></li>';
						count++;
					}
					a += '</ul></div></div>';
				}

				var temp = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				for (var i in temp) {
					var b = '';
					count = 1;
					for (var x in customtags) {
						if (customtags[x].image_id == temp[i].image_id) {
							b += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + customtags[x].text + '</span></li>';
							count++;
						}
					}
					b += '</ul></div>';
					a += '<div class="grid_4 centerme"><span>Additional Image</span><img src="' + temp[i].thumbnail + '" width="95%"/><ul>';
					a += b;

				}

				return a;
			}



			this.modifyDropdowns = function () {

				var whichGarment = that.selectedVariantFitting().id;

				var tailors = this.dsRegistry.getDatasource('tailorsDS').getStore();
				var days = orderItem.days();
				var months = orderItem.months();
				//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor);
				try {
					if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailor == undefined || that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailor == '') {
						document.getElementsByName("FittingTailor")[0].getElementsByTagName('option')[0].selected = true;
					} else {
						for (var a = 0; a < tailors.length; a++) {
							if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailor.last_name == tailors[a].last_name) {
								document.getElementsByName("FittingTailor")[0].getElementsByTagName('option')[a + 1].selected = true;
							}
						}
					}
				} catch (e) {
					;
				}
				//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay);
				try {
					if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryDay == undefined || that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryDay == '') {
						document.getElementsByName("FittingDeliveryDay")[0].getElementsByTagName('option')[0].selected = true;
					} else {
						for (var a = 0; a < days.length; a++) {
							if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryDay == days[a]) {
								document.getElementsByName("FittingDeliveryDay")[0].getElementsByTagName('option')[a + 1].selected = true;
							}
						}
					}
				} catch (e) {
					//console.log("day error: " + e);
				}
				//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth);
				try {
					if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryMonth == undefined || that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryMonth == '') {
						document.getElementsByName("FittingDeliveryMonth")[0].getElementsByTagName('option')[0].selected = true;
					} else {
						for (var a = 0; a < months.length; a++) {
							if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryMonth == months[a].id) {
								document.getElementsByName("FittingDeliveryMonth")[0].getElementsByTagName('option')[a + 1].selected = true;
							}
						}
					}
				} catch (e) {
					//console.log("month error: " + e);
				}

				try {
					if ("createEvent" in document) {
						var evt = document.createEvent("HTMLEvents");
						evt.initEvent("change", true, true);
						document.getElementsByName("FittingTailor")[0].dispatchEvent(evt);
						document.getElementsByName("FittingDeliveryDay")[0].dispatchEvent(evt);
						document.getElementsByName("FittingDeliveryMonth")[0].dispatchEvent(evt);
					} else {
						document.getElementsByName("FittingTailor")[0].fireEvent("onchange");
						document.getElementsByName("FittingDeliveryDay")[0].fireEvent("onchange");
						document.getElementsByName("FittingDeliveryMonth")[0].fireEvent("onchange");
					}
				} catch (e) {
					console.log(e);
				}

				return false;
			};



			///////////////////////////		SYNC PART	///////////////////////////
			//////////////////////////////////////////////////////////////////////

			this.sync = function () {

				const confirmAction = () => {
					var data = [];
					var completedFittings = true;
					var spinner = document.getElementById('loading_jp');
					var error = false;
					var customphotos = "";
					spinner.style.display = "block";
					var ind = '';
					var allComplete = true;
					that.checkSteps()
					for (test in that.variantNameFitting()) {
						console.log(that.variantNameFitting()[test]);
						if (that.variantNameFitting()[test].completed() == false && orderItem.fittingsVM.FittingData[that.variantNameFitting()[test].id].FittingId == '') {
							allComplete = false;
							break;
						}
					}
					for (ind in that.variantNameFitting()) {

						// var whichGarment = that.variantNameFitting()[ind].id;

						var whichGarment = ind;

						if( that.FittingData[whichGarment] == undefined )
						{
							continue; // there is no information about that garment
						}


						console.log("getting " + 'FittingFrontImagePhoto' + whichGarment);

						var frontImage = that.FittingData[whichGarment].frontImage.getSyncInfo();
						var sideImage = that.FittingData[whichGarment].sideImage.getSyncInfo();
						var backImage = that.FittingData[whichGarment].backImage.getSyncInfo();
						var customImages = that.FittingData[whichGarment].customImages.getSyncInfo();

						var customerString = orderItem.custSelectVM.selectedCustomer();
						var fittingIdPost = orderItem.fittingsVM.FittingData[whichGarment].FittingId;

						try {
							var date = new Date();
							var yearnow = date.getFullYear();
							var monthnow = date.getMonth() + 1;
							var daynow = date.getDate();
							var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
							var day = this.FittingDeliveryDate().getDate();
							var month = this.FittingDeliveryDate().getMonth() + 1;
							var year = this.FittingDeliveryDate().getFullYear();
							var dateToCompare = new Date(month + "/" + day + "/" + year);
							var dateToPost = day + "-" + month + "-" + year;
						} catch (e) {
							var dateToPost = null;
							var dateToCompare = null;
						}
						var diffDays = -1;
						try {
							var timeDiff = dateToCompare.getTime() - dateNow.getTime();
							diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
							console.log("diffDays: " + diffDays);
						} catch (e) { ; }
						var fittingvideos = ko.observableArray();
						fittingvideos.push(new orderItem.FittingVM.FittingVideoViewModel(orderItem.fittingsVM.FittingData[whichGarment].FittingVideo, JSON.stringify(orderItem.fittingsVM.FittingData[whichGarment].FittingVideoNotes)));

						var tailorIdPost = "";
						if (this.FittingTailor() != undefined) {
							if (this.FittingTailor().user_id != undefined) {
								tailorIdPost = this.FittingTailor().user_id;
							}
						}
						var foundnotcompletedstep = false;
						for (var x = 0; x < this.workflow().length - 1; x++) {
							if (this.workflow()[x].completed == "false") {
								foundnotcompletedstep = true;
								break;
							}
						}

						var pendingFrontTagsPost = "0";
						if (orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == true) {
							pendingFrontTagsPost = "1";
						}
						var pendingSideTagsPost = "0";
						if (orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == true) {
							pendingSideTagsPost = "1";
						}
						var pendingBackTagsPost = "0";
						if (orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == true) {
							pendingBackTagsPost = "1";
						}

						var pendingVideoPost = "0";
						if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == true) {
							pendingVideoPost = "1";
						}

						var pendingVideoNotesPost = "0";
						if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideoNotes == true) {
							pendingVideoNotesPost = "1";
						}



						//console.log("this.FittingVideo(): " + this.FittingVideo());
						if (!that.FittingData[whichGarment].useAsCompletionImages() && dateToPost == "" && !REMAKE_DATA[whichGarment]) {
							customAlert("Please select a date!");
							error = true;
							break;
						}

						// Not used anymore --- Use the
						// else if (tailorIdPost == "") {
						// 	customAlert("Please select a tailor and a date!!");
						// 	error = true;
						// 	break;
						// }
						// else if (diffDays < 0) {
						// 	customAlert("There has been an error with the selected date. <br/> Try again or select another.");
						// 	error = true;
						// 	break;
						// } else if (diffDays > 120) {
						// 	customAlert("The selected date is more than 4 months away! <br/> Please select a closer date.");
						// 	error = true;
						// 	break;
						// }


						else if (!that.FittingData[whichGarment].frontImage.hasMedia()) {
							customAlert("Please upload a front image");
							error = true;
							this.currentStep(this.workflow()[0]);
							break;
						} else if (!that.FittingData[whichGarment].sideImage.hasMedia()) {
							customAlert("Please upload a side image");
							error = true;
							this.currentStep(this.workflow()[0]);
							break;
						} else if (!that.FittingData[whichGarment].backImage.hasMedia()) {
							customAlert("Please upload a back image");
							error = true;
							this.currentStep(this.workflow()[0]);
							break;
						}

						// No need to check notes anymore
						// else if (!that.FittingData[whichGarment].frontImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoFrontTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == false) {
						// 	error = true;
						// 	customAlert("Please enter front image notes");
						// 	this.currentStep(this.workflow()[2]);
						// 	break;
						// } else if (!that.FittingData[whichGarment].sideImage.allMediaHavetags() && this.NoSideTags() == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == false) {
						// 	error = true;
						// 	customAlert("Please enter side image notes");
						// 	this.currentStep(this.workflow()[2]);
						// 	break;
						// } else if (!that.FittingData[whichGarment].backImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoBackTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == false) {
						// 	error = true;
						// 	customAlert("Please enter back image notes");
						// 	this.currentStep(this.workflow()[2]);
						// 	break;
						// }


						// else if (!orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.hasMedia() && orderItem.fittingsVM.FittingData[whichGarment].NoVideo == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == false) {
						// 	error = true;
						// 	customAlert("Please enter the fitting video");
						// 	this.currentStep(this.workflow()[1]);
						// 	break;
						// }


						else {


							const selectedGarment = orderItem.fittingsVM.FittingData[whichGarment];


							// get selected values
							TailorAssigmentControl.setGarmentValues()
							const selectedGarment_inView = TailorAssigmentControl.allGarments[parseInt(whichGarment)]

							// update values from view
							selectedGarment.selectedTailorId = selectedGarment_inView.selectedTailorId
							selectedGarment.selectedDeliveryDate = selectedGarment_inView.selectedDeliveryDate
							selectedGarment.selectedTailorComments = selectedGarment_inView.selectedTailorComments


							// Getting the tailor and date information
							const selectedTailorId = selectedGarment.selectedTailorId;
							const deliveryDate = selectedGarment.selectedDeliveryDate;
							const selectedTailorComments = selectedGarment.selectedTailorComments;
							const relatedSelectedSalesmanId = selectedGarment.selectedRelatedSalesmanId;

							if (!that.FittingData[whichGarment].useAsCompletionImages() && !parseInt(selectedTailorId) && !REMAKE_DATA[whichGarment]) {
								error = true;
								customAlert("Please Select a tailor for: " + selectedGarment.FittingGarment + " " + selectedGarment.FittingGarmentFabric);
								this.currentStep(this.workflow()[3]);
								break;
							}

							if (!that.FittingData[whichGarment].useAsCompletionImages() && !deliveryDate && !REMAKE_DATA[whichGarment]) {
								error = true;
								customAlert("Please Select a delivery date for: " + selectedGarment.FittingGarment + " " + selectedGarment.FittingGarmentFabric);
								this.currentStep(this.workflow()[3]);
								break;
							}

							that.rememberIsCompletion(that.FittingData[whichGarment]);

							// end getting tailor and date information

							// getting remake information for garment
							const remakeData = REMAKE_DATA[ind] != undefined ? REMAKE_DATA[ind] : null;

							data[ind] = {
								device_id: typeof device != "undefined" ? device.uuid : "",
								useAsCompletionImages : orderItem.fittingsVM.FittingData[whichGarment].useAsCompletionImages(),
								user_id: authCtrl.userInfo.user_id,
								customer: customerString,
								fitting_id: fittingIdPost,
								order_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderId,
								orders_products_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderProductId,
								barcode_id: orderItem.fittingsVM.FittingData[whichGarment].FittingGarmentBarcode,
								garment: orderItem.fittingsVM.FittingData[whichGarment].FittingGarment,
								pendingFrontNotes: pendingFrontTagsPost,
								pendingSideNotes: pendingSideTagsPost,
								pendingBackNotes: pendingBackTagsPost,
								pendingVideo: pendingVideoPost,
								pendingVideoNotes: pendingVideoNotesPost,
								tailor: {
									user_id: selectedTailorId,
									delivery_date: deliveryDate,
									notes: selectedTailorComments
								},
								relatedSalesman : {
									user_id: relatedSelectedSalesmanId,
									delivery_date: deliveryDate,
									notes: selectedTailorComments
								},
								remakeData : remakeData,
								front_image: frontImage,
								side_image: sideImage,
								back_image: backImage,
								custom_images: customImages,
								fitting_videos: orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.getSyncInfo(),
								cloned: orderItem.fittingsVM.FittingData[whichGarment].FittingCloned,

								fitting_errors : orderItem.fittingsVM.FittingData[whichGarment].fittingErrorsReport.getSubmissionData()
							};


						}



					}

					// check if at least one image has tags
					// deprecated: this feature is causing bug. So it is easier to remove
					// if (!that.checkIfAtLeastOneImageHasTag()){
					// 	customAlert("Please ADD tags to your images OR mark the garment as COMPLETE");
					// 	document.getElementById('loading_jp').style.display = "none";
					// 	return;
					// }

					// split data into garments with REMAKE end garments without remake
					const data_with_remake = data.filter(data => data.remakeData);
					const data_without_remake = data.filter(data => !data.remakeData);


					console.log(data);
					var datatopost = JSON.stringify(data_without_remake);
					var datatopost_remake = JSON.stringify(data_with_remake);
					datatopost = datatopost.replace("\"isDirty\":true,", "");
					console.log("datatopost: " + datatopost);

					document.getElementById('loading_jp').style.display = "block";

					const _order_id = orderItem.fittingsVM.FittingData[whichGarment].FittingOrderId;
					const _garment = orderItem.fittingsVM.FittingData[whichGarment].FittingGarment;
					const is_completion = orderItem.fittingsVM.FittingData[whichGarment].useAsCompletionImages();

					if (!error) {

						let success_in_all_submissions = true;

						$.ajax({
							type: 'POST',
							timeout: 60000, // sets timeout to 60 seconds
							url: BUrl + 'remakes_pos/submit_remake_from_fitting',
							dataType: 'json',
							data: {
								"user": authCtrl.userInfo,
								// "device_id" : device_id,
								data: datatopost_remake,
							},
							success: function (dataS) {

							},
							error: function (error) {
								success_in_all_submissions = false;
								pos_error({title: '', msg: 'error on REMAKE submission'});
							},
							async: false
						});

						$.ajax({
							type: 'POST',
							timeout: 60000, // sets timeout to 60 seconds
							url: BUrl + 'client_fittings/fitting_endpoint5',
							dataType: 'json',
							data: {
								"user": authCtrl.userInfo,
								// "device_id" : device_id,
								data: datatopost,
							},
							success: function (dataS) {

								document.getElementById('loading_jp').style.display = "none";

								console.log("fitting result", dataS);

								if(dataS.result)
								{
									if (dataS.make_one_first_to_approve_garments && dataS.make_one_first_to_approve_garments.length > 0)
									{
										console.log("dataS.make_one_first_to_approve_garments. In development");

										that.makeOneFirstToApprove.insertGarmentsToApprove(dataS.make_one_first_to_approve_garments);

										// show popup
										$('#make-on-first-popup').show(500);
									}
									else
									{
										customAlert("Fitting succesfully added");
										if (success_in_all_submissions) {
											if (that.redirectToPayment()){
												posChangePage('#customerOrders');
											}
											else {
												posChangePage('#fittingList');
											}
										}
									}

									/**
									 * Sends data to ERP and check if should send email to manufacturer
									 * about pattern update.
									 */
									if(is_completion)
									{
										$.post(window.BUrl + "alterations_pos/send_manufacturer_new_pattern_to_update_email", { "user": authCtrl.userInfo, 'order_id' : _order_id, "garment_type_name" : _garment }, (ret) => { console.log('manu pattern to update email:', ret); }, "json");
									}

								}
								else
								{
									console.log('fail');
									customAlert("there is a network issue. Please try again later");
									$.jGrowl(dataS.msg);
								}


							},
							error: function (error) {
								console.log(JSON.stringify(error));

								success_in_all_submissions = false;

								document.getElementById('loading_jp').style.display = "none";

								completedFittings = false;

								customAlert("TIME OUT - there is a network issue. Please try again later");
							},
							async: false
						});

						if(completedFittings)
						{
							//posChangePage('#fittingList');
						}


					}
					else
					{
						document.getElementById('loading_jp').style.display = "none";
						that.selectedVariantFitting(that.variantNameFitting()[ind]);
					}

					document.getElementById('loading_jp').style.display = "none";
					// }

				}

				pos_warning({msg: "Finalize and submit the fitting ?", confirmAction})

			}

			this.sync_edit = function () {

				var data = [];
				var completedFittings = true;
				var spinner = document.getElementById('loading_jp');
				var error = false;
				var customphotos = "";
				spinner.style.display = "block";
				that.checkSteps()
				var allComplete = true;
				var test = '';
				for (test in that.variantNameFitting()) {
					console.log(that.variantNameFitting()[test]);
					if (that.variantNameFitting()[test].completed() == false && orderItem.fittingsVM.FittingData[that.variantNameFitting()[test].id].FittingId == '') {
						allComplete = false;
						break;
					}
				}
				for (ind in that.variantNameFitting()) {
					var whichGarment = that.variantNameFitting()[ind].id;

					console.log("getting " + 'FittingFrontImagePhoto' + whichGarment);

					var frontImage = that.FittingData[whichGarment].frontImage.getSyncInfo();
					var sideImage = that.FittingData[whichGarment].sideImage.getSyncInfo();
					var backImage = that.FittingData[whichGarment].backImage.getSyncInfo();
					var customImages = that.FittingData[whichGarment].customImages.getSyncInfo();


					var customerString = orderItem.custSelectVM.selectedCustomer();
					var fittingIdPost = orderItem.fittingsVM.FittingData[whichGarment].FittingId;

					try {
						var date = new Date();
						var yearnow = date.getFullYear();
						var monthnow = date.getMonth() + 1;
						var daynow = date.getDate();
						var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
						var day = this.FittingDeliveryDate().getDate();
						var month = this.FittingDeliveryDate().getMonth() + 1;
						var year = this.FittingDeliveryDate().getFullYear();
						var dateToCompare = new Date(month + "/" + day + "/" + year);
						var dateToPost = day + "-" + month + "-" + year;
					} catch (e) {
						var dateToPost = null;
						var dateToCompare = null;
					}
					var diffDays = -1;
					try {
						var timeDiff = dateToCompare.getTime() - dateNow.getTime();
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						console.log("diffDays: " + diffDays);
					} catch (e) { ; }
					var fittingvideos = ko.observableArray();
					fittingvideos.push(new orderItem.FittingVM.FittingVideoViewModel(orderItem.fittingsVM.FittingData[whichGarment].FittingVideo, JSON.stringify(orderItem.fittingsVM.FittingData[whichGarment].FittingVideoNotes)));

					var tailorIdPost = "";
					if (this.FittingTailor() != undefined) {
						if (this.FittingTailor().user_id != undefined) {
							tailorIdPost = this.FittingTailor().user_id;
						}
					}
					var foundnotcompletedstep = false;
					for (var x = 0; x < this.workflow().length - 1; x++) {
						if (this.workflow()[x].completed == "false") {
							foundnotcompletedstep = true;
							break;
						}
					}

					var pendingFrontTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == true) {
						pendingFrontTagsPost = "1";
					}
					var pendingSideTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == true) {
						pendingSideTagsPost = "1";
					}
					var pendingBackTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == true) {
						pendingBackTagsPost = "1";
					}

					var pendingVideoPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == true) {
						pendingVideoPost = "1";
					}

					var pendingVideoNotesPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideoNotes == true) {
						pendingVideoNotesPost = "1";
					}


					//console.log("this.FittingVideo(): " + this.FittingVideo());
					if (dateToPost == "") {
						customAlert("Please select a date!");
						error = true;
						break;
					} else if (tailorIdPost == "") {
						customAlert("Please select a tailor and a date!");
						error = true;
						break;
					} else if (diffDays < 0) {
						customAlert("There has been an error with the selected date. <br/> Try again or select another.");
						error = true;
						break;
					} else if (diffDays > 120) {
						customAlert("The selected date is more than 4 months away! <br/> Please select a closer date.");
						error = true;
						break;
					} else if (!that.FittingData[whichGarment].frontImage.hasMedia()) {
						customAlert("Please upload a front image");
						error = true;
						this.currentStep(this.workflow()[0]);
						break;
					} else if (!that.FittingData[whichGarment].sideImage.hasMedia()) {
						customAlert("Please upload a side image");
						error = true;
						this.currentStep(this.workflow()[0]);
						break;
					} else if (!that.FittingData[whichGarment].backImage.hasMedia()) {
						customAlert("Please upload a back image");
						error = true;
						this.currentStep(this.workflow()[0]);
						break;
					}


					// not used anymore
					// else if (!that.FittingData[whichGarment].frontImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoFrontTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == false) {
					// 	error = true;
					// 	customAlert("Please enter front image notes");
					// 	this.currentStep(this.workflow()[2]);
					// 	break;
					// } else if (!that.FittingData[whichGarment].sideImage.allMediaHavetags() && this.NoSideTags() == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == false) {
					// 	error = true;
					// 	customAlert("Please enter side image notes");
					// 	this.currentStep(this.workflow()[2]);
					// 	break;
					// } else if (!that.FittingData[whichGarment].backImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoBackTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == false) {
					// 	error = true;
					// 	customAlert("Please enter back image notes");
					// 	this.currentStep(this.workflow()[2]);
					// 	break;
					// }



					// else if (!orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.hasMedia() && orderItem.fittingsVM.FittingData[whichGarment].NoVideo == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == false) {
					// 	error = true;
					// 	customAlert("Please enter the fitting video");
					// 	this.currentStep(this.workflow()[1]);
					// 	break;
					// }

					else {
						var spinner = document.getElementById('loading_jp');
						spinner.style.display = "block";
						data[ind] = {
							device_id: typeof device != "undefined" ? device.uuid : "",
							user_id: authCtrl.userInfo.user_id,
							customer: customerString,
							fitting_id: fittingIdPost,
							order_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderId,//this.selected_order.order_id,
							orders_products_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderProductId,
							barcode_id: orderItem.fittingsVM.FittingData[whichGarment].FittingGarmentBarcode,
							garment: orderItem.fittingsVM.FittingData[whichGarment].FittingGarment,
							pendingFrontNotes: pendingFrontTagsPost,
							pendingSideNotes: pendingSideTagsPost,
							pendingBackNotes: pendingBackTagsPost,
							pendingVideo: pendingVideoPost,
							pendingVideoNotes: pendingVideoNotesPost,
							tailor: {
								user_id: tailorIdPost,
								delivery_date: dateToPost,
								notes: that.FittingTailorNotes()
							},
							front_image: frontImage,
							side_image: sideImage,
							back_image: backImage,
							custom_images: customImages,
							fitting_videos: orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.getSyncInfo(),
							cloned: orderItem.fittingsVM.FittingData[whichGarment].FittingCloned,

							fitting_errors : orderItem.fittingsVM.FittingData[whichGarment].fittingErrorsReport.getSubmissionData()
							//fitting_id, order_id, garment_id, tailor(user_id, delivery_date, notes), fitting_images(id, name), fitting_tags(left, top, text), fitting_videos(path, notes).
						};


					}



				}
				console.log(data);
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");
				console.log("datatopost: " + datatopost);
				if (!error) {
					$.ajax({
						type: 'POST',
						timeout: 45000, // sets timeout to 45 seconds
						url: BUrl + 'client_fittings/fitting_edit_endpoint4',
						data: {
							data: datatopost,
						},
						success: function (dataS) {
							//dataS = JSON.parse(dataS);
							console.log(dataS);
							if (dataS) {
								//if(orderItem.fittingsVM.FittingData[whichGarment].FittingId == ''){
								customAlert("Fitting succesfully added");
								//}else{
								//	customAlert("Fitting succesfully edited");
								//}
								//that.FittingId(dataS.fitting);
								//var cs = that.currentStep().id;
								//that.workflow()[cs].completed = "true";

								//if(that.variantNameFitting().length == 1){
								//}else{
								//	that.goToNextNotSubmittedFitting();
								//}
							} else {
								console.log('fail');
								customAlert("there is a network issue. Please try again later");
								$.jGrowl(dataS.msg);
							}
							spinner.style.display = "none";
						},
						error: function (error) {
							console.log(JSON.stringify(error));
							customAlert("");
							spinner.style.display = "none";
							completedFittings = false;
						},
						async: false
					});
					if (completedFittings) {
						posChangePage('#customerGarmentsFittingsList');
					}
				}

				spinner.style.display = "none";
				//}
			}

		},


		/**
		 *
		 * @param {tData.image} media [front, side, back, custom]Image
		 * @param {Number} custom_pos custom images are array, we need the positions of the image to remove it
		 */
		removeImage(media, custom_pos=null){
			// remove media
			// [front/side/back]
			if (custom_pos == null){
				media.removeMedia();

				// get upload image step
				let cs = this.currentStep().id;

				// set step and garment as incomplete
				this.selectedVariantFitting().steps()[cs].completed = "false";
				this.selectedVariantFitting().completed(false);

			// custom images
			} else {
				media.removeMedia(custom_pos);
			}


		},
		/**
		 *
		 * @param {tData.image} media
		 * @param {Number} custom_pos positions of array
		 */
		removeVideo(media, custom_pos){
			// remove media
			media.removeMedia(custom_pos);


		},

		// @deprecated
		// For some reason, this method is calling for completion garments and blocks the user. It shouldn't
		checkIfAtLeastOneImageHasTag(){

			return true;

			for (const fittingData of this.FittingData) {
				// not completions
				if (!fittingData.useAsCompletionImages()){
					// checking front/side/back
					if (
						fittingData.frontImage.hasImage && fittingData.frontImage.getTags()().length == 0 &&
						fittingData.backImage.hasImage  && fittingData.backImage.getTags()().length  == 0 &&
						fittingData.sideImage.hasImage  && fittingData.sideImage.getTags()().length  == 0
						){
						return false;
					}
				}
			}

			return true;
		},



		/** Return a computed with a count of a fitting errors for selectedVariantFitting */
		countOfErrors(){
			var self = this;
			return ko.computed(function() {
				const variantFitting = self.selectedVariantFitting().id;
				const count = self.FittingData[variantFitting].fittingErrorsReport.fittingErrors().length;
				return count;
			});
		},













		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		previousStep: function () {
			if (this.currentStep().id !== 0) {
				this.previousStepEnabled(true);
				this.currentStep(this.workflow()[this.currentStep().id - 1]);
			}
		},

		flushModel: function () {
			this.dsRegistry.getDatasource(this.subscribed).setStore({
				"Fitting": this.FittingData
			}, true);
		},

		getVariant: function (id) {
			var toreturn = this.FittingData[0];
			for (var ind in this.FittingData) {
				if (this.FittingData[ind].variantId == id) {
					toreturn = this.FittingData[ind];
				}
			}
			return toreturn;
		},

		getRow: function (id) {
			for (var ind in this.FittingData) {
				if (this.FittingData[ind].variantId == id) {
					return ind;
				}
			}
			return -1;
		},

		/**
		 * Remove garment after fitting starts.
		 * Use Case: If the seller sets 3 garments to fitting, but only realizes that the last one was correct.
		 * With this method, he can easily remove the garment without losing all his work.
		 *
		 * BUG: This is wrong, its causing the fitting not to submit.
		 * 	We are getting index == undefined because we are deleting from the array and trying to access the deleted position when submitting the fitting.
		 * Fixed : The reason of this bug were because in fitting data, we are using variantId as position in array (same as id in variantNameFitting).
		 * This method must renumber this attributes
		 *
		 */
		removeVariantFitting: function (index, mustConfirm=true) {
			// body of function
			const _remove = () => {
				this.variantNameFitting.remove(this.variantNameFitting()[index]); // remove from tabs
				this.FittingData.splice(index, 1); // remove from fitting

				// reset variantfitting_id, because we are using variantfitting_id as index in array
				var count = 0;
				this.FittingData.forEach(element => {
					element.variantId = count;
					count++;
				});

				// reset id, because we are using id as index in array
				var count = 0;
				this.variantNameFitting(
					this.variantNameFitting().map(el => {
						el.id = count;
						count++;
						return el;
					})
				);

				$.jGrowl('Garment removed succefully');


				// refresh assign tailor step
				TailorAssigmentControl.rebuildClass();
			}

			if (mustConfirm){
				const confirmAction = () => {
					_remove();
				};
				pos_confirm({msg:'Are you sure you want remove this garment from this fitting?', confirmAction});
			}

			if (!mustConfirm){
				_remove();
			}
		},


		/**
		 * @deprecated
		 */
		addVariantFitting: function (name) {
			//console.log(this.variantNameFitting);
			if (name == "Shirt") {
				this.NumberShirts++;
			}
			if (this.variantNameFitting()[0].title != "Fitting 1") {
				this.FittingDataAID += 1;
				var newid = this.FittingDataAID + 1;
				var vname = name;
				var tObj = jQuery.extend(true, {}, this.getVariant(this.getRow(this.selectedVariantFitting().id))); //CLONE Object
				this.variantNameFitting.push({ title: vname, id: this.FittingDataAID, completed: ko.observable(false) , steps: ko.observableArray(ko.toJS(this.workflow))});
				tObj.variantId = this.FittingDataAID;
				this.flushModel();
			} else {
				this.variantNameFitting()[0].title = name;
			}
		},

		addVariantsFittings: function () {
			for (var x in orderItem.fittingsVM.FittingData) {
				//	console.log("addVariantsFittings " + x);

				if (x != 0) {
					this.FittingDataAID += 1;
					var vname = orderItem.fittingsVM.FittingData[x].FittingGarment;
					if (vname == "Shirt") {
						this.NumberShirts++;
					}
					var tObj = jQuery.extend(true, {}, this.getVariant(this.getRow(this.selectedVariantFitting().id))); //CLONE Object
					this.variantNameFitting.push({ title: vname, id: this.FittingDataAID, completed: ko.observable(false) , steps: ko.observableArray(ko.toJS(this.workflow))});
					tObj.variantId = this.FittingDataAID;
					this.flushModel();
				} else {
					var vname = orderItem.fittingsVM.FittingData[x].FittingGarment;
					if (vname == "Shirt") {
						this.NumberShirts++;
					}
					var tObj = jQuery.extend(true, {}, this.getVariant(this.getRow(this.selectedVariantFitting().id))); //CLONE Object
					this.variantNameFitting()[x].title = vname;
					tObj.variantId = this.FittingDataAID;
					this.flushModel();
				}

				let garment_type = false;

				switch (orderItem.fittingsVM.FittingData[x].FittingGarment.toLowerCase())
				{
					case 'pants':garment_type = '1';break;
					case 'jacket':garment_type = '4';break;
					case 'shirt':garment_type = '2';break;
					case 'vest':garment_type = '3';break;
					case 'jacket/s':garment_type = '4';break;
					case 'pant/s':garment_type = '1';break;
				}

				// add FittingErrorsReport object to each garment
				orderItem.fittingsVM.FittingData[x].fittingErrorsReport = new FittingErrorsReport(orderItem.fittingsVM.FittingData[x].FittingGarmentBarcode, garment_type);


			}
		},

		openError: function () {
			var garment = {};
			garment.garment_id = this.FittingOrderProductId();

			switch (this.FittingGarment().toLowerCase())
			{
				case 'pants':
					garment.garment_type = '1';
					break;
				case 'jacket':
					garment.garment_type = '4';
					break;
				case 'shirt':
					garment.garment_type = '2';
					break;
				case 'vest':
					garment.garment_type = '3';
					break;
				case 'jacket/s':
					garment.garment_type = '4';
					break;
				case 'pant/s':
					garment.garment_type = '1';
					break;
			}
			errorReportVM.buildError(this.FittingCustomer(), this.FittingOrderId(), garment);
		},

		digestData: function (data) {
			this.FittingData = data.Fitting;
			this.renderView();
		},

		renderView: function () {
			this.renderFitting();
		},

		renderFitting: function () {
			this.modifyDropdowns();

			var whichGarment = this.selectedVariantFitting().id;

			//Get selected Variant
			try {
				var tData = this.getVariant(this.selectedVariantFitting().id);
				if (tData != null) {
					//Update observables
					if (typeof (tData.frontImage) != "undefined") {
						this.frontImage = tData.frontImage;
						this.frontImage.setCustomPreview(this.previewFrontImage);
						this.frontImage.setCustomPreviewValue();

					}
					if (typeof (tData.sideImage) != "undefined") {
						this.sideImage = tData.sideImage;
						this.sideImage.setCustomPreview(this.previewSideImage);
						this.sideImage.setCustomPreviewValue();
					}
					if (typeof (tData.backImage) != "undefined") {
						this.backImage = tData.backImage;
						this.backImage.setCustomPreview(this.previewBackImage);
						this.backImage.setCustomPreviewValue();
					}
					if (typeof (tData.customImages) != "undefined") {
						this.customImages = tData.customImages;
						this.customImages.setCustomPreview(this.previewCustomImage);
						this.customImages.setCustomPreviewValue();
					}
					if (typeof (tData.FittingVideo) != "undefined") {
						this.FittingVideo = tData.FittingVideo;
						this.FittingVideo.setCustomPreview(this.previewCustomVideo);
						this.FittingVideo.setCustomPreviewValue();
					}
					this.updateSkipButtons();
					if (typeof (tData.FittingCustomer) != "undefined") { this.FittingCustomer(tData.FittingCustomer); }
					if (typeof (tData.FittingGarment) != "undefined") { this.FittingGarment(tData.FittingGarment); }
					if (typeof (tData.FittingGarmentFabric) != "undefined") { this.FittingGarmentFabric(tData.FittingGarmentFabric); }
					if (typeof (tData.FittingGarmentFabricImage) != "undefined") { this.FittingGarmentFabricImage(tData.FittingGarmentFabricImage); }
					if (typeof (tData.FittingGarmentBarcode) != "undefined") { this.FittingGarmentBarcode(tData.FittingGarmentBarcode); }
					if (typeof (tData.FittingId) != "undefined") { this.FittingId(tData.FittingId); }
					if (typeof (tData.FittingOrderId) != "undefined") { this.FittingOrderId(tData.FittingOrderId); }
					if (typeof (tData.FittingOrderProductId) != "undefined") { this.FittingOrderProductId(tData.FittingOrderProductId); }
					if (typeof (tData.FittingTailor) != "undefined") { this.FittingTailor(tData.FittingTailor); }
					if (typeof (tData.FittingDeliveryDay) != "undefined") { this.FittingDeliveryDay(tData.FittingDeliveryDay); }
					if (typeof (tData.FittingDeliveryMonth) != "undefined") { this.FittingDeliveryMonth(tData.FittingDeliveryMonth); }
					if (typeof (tData.FittingDeliveryDate) != "undefined") { this.FittingDeliveryDate(tData.FittingDeliveryDate); }
					if (typeof (tData.CalendarDate) != "undefined") { this.CalendarDate(tData.CalendarDate); }
					if (typeof (tData.FittingTailorNotes) != "undefined") { this.FittingTailorNotes(tData.FittingTailorNotes); }
					//if (typeof(tData.FittingVideo)    		!= "undefined") {this.FittingVideo(tData.FittingVideo);}
					//if (typeof(tData.FittingVideoPreview)    		!= "undefined") {this.FittingVideoPreview(tData.FittingVideoPreview);}
					if (typeof (tData.SkippedVideo) != "undefined") { this.SkippedVideo(tData.SkippedVideo); }
					if (typeof (tData.SkippedVideoNotes) != "undefined") { this.SkippedVideoNotes(tData.SkippedVideoNotes); }
					if (typeof (tData.NoVideo) != "undefined") { this.NoVideo(tData.NoVideo); }
					if (typeof (tData.SkippedFrontTags) != "undefined") { this.SkippedFrontTags(tData.SkippedFrontTags); }
					if (typeof (tData.SkippedSideTags) != "undefined") { this.SkippedSideTags(tData.SkippedSideTags); }
					if (typeof (tData.SkippedBackTags) != "undefined") { this.SkippedBackTags(tData.SkippedBackTags); }
					if (typeof (tData.NoFrontTags) != "undefined") { this.NoFrontTags(tData.NoFrontTags); }
					if (typeof (tData.NoSideTags) != "undefined") { this.NoSideTags(tData.NoSideTags); }
					if (typeof (tData.NoBackTags) != "undefined") { this.NoBackTags(tData.NoBackTags); }
					if (typeof (tData.isActive) != "undefined") { this.isActive(tData.isActive); }
					if (typeof (tData.isCompleted) != "undefined") { this.isCompleted(tData.isCompleted); }
					//if (typeof(tData.FittingVideoNotes)   != "undefined") {this.FittingVideoNotes(tData.FittingVideoNotes);}
					if (typeof (tData.AdditionalImagesStepComplete) != "undefined") { this.AdditionalImagesStepComplete(tData.AdditionalImagesStepComplete); }
					if (typeof (tData.FittingCloned) != "undefined") { this.FittingCloned(tData.FittingCloned); }

				}
			} catch (e) {
				;
			}

			this.checkSteps();
		},

		/** Copy all fitting data from a shirt into other shirts
		 * @deprecated because after clone, when change one garment the changes is applied to the others
		*/
		cloneFittingForAllShirt_deprecated : function(){
			console.log("cloneFittingForAllShirt...");
			// console.log("this.selectedVariantFitting()", this.selectedVariantFitting());
			// console.log("this.FittingData", this.FittingData);

			const confirmAction = () => {
				// fitting to clone
				var fittingToClone = this.selectedVariantFitting().id;

				for (var fittingIndex in this.FittingData)
				{
					// cases to ignore:
					// current fitting; fitting is not a shirt
					if (fittingToClone == fittingIndex || this.FittingData[fittingIndex].FittingGarment != "Shirt"){
						continue;
					}

					// Clone front image
					this.FittingData[fittingIndex].frontImage = this.FittingData[fittingToClone].frontImage;

					// Clone side image
					this.FittingData[fittingIndex].sideImage = this.FittingData[fittingToClone].sideImage;

					// Clone back image
					this.FittingData[fittingIndex].backImage = this.FittingData[fittingToClone].backImage;

					// clone custom images
					this.FittingData[fittingIndex].customImages = this.FittingData[fittingToClone].customImages;

					// Clone videos
					this.FittingData[fittingIndex].FittingVideo = this.FittingData[fittingToClone].FittingVideo;
					this.FittingData[fittingIndex].FittingVideoNotes = this.FittingData[fittingToClone].FittingVideo;
					// No need to clone tailor assigned. Change first == change all
				}

				$.jGrowl('Images and Videos have been successfully cloned !');
			};
			pos_confirm({msg:'Are you sure you want to clone this shirt fitting to other shirts?', confirmAction});


		},

		/** Copy all fitting data from a shirt into other shirts */
		cloneFittingForAllShirt : function(){
			console.log("cloneFittingForAllShirt...");
			// console.log("this.selectedVariantFitting()", this.selectedVariantFitting());
			// console.log("this.FittingData", this.FittingData);

			const confirmAction = () => {
				// fitting to clone
				var fittingToClone = this.selectedVariantFitting().id;

				for (var fittingIndex in this.FittingData)
				{
					// cases to ignore:
					// current fitting; fitting is not a shirt
					if (fittingToClone == fittingIndex || this.FittingData[fittingIndex].FittingGarment != "Shirt"){
						continue;
					}

					// Clone front image ==========================
					this.FittingData[fittingIndex].frontImage.preview(this.FittingData[fittingToClone].frontImage.preview());

					this.FittingData[fittingIndex].frontImage.media().preview(this.FittingData[fittingToClone].frontImage.media().preview());
					this.FittingData[fittingIndex].frontImage.media().upload(this.FittingData[fittingToClone].frontImage.media().upload());
					this.FittingData[fittingIndex].frontImage.media().tags(this.FittingData[fittingToClone].frontImage.media().tags.slice(0));

					// this.FittingData[fittingIndex].frontImage.customPreview().media(this.FittingData[fittingToClone].frontImage.customPreview().media());
					// this.FittingData[fittingIndex].frontImage.customPreview().tags(this.FittingData[fittingToClone].frontImage.customPreview().tags.slice(0));

					// Clone side image ==========================
					this.FittingData[fittingIndex].sideImage.preview(this.FittingData[fittingToClone].sideImage.preview());

					this.FittingData[fittingIndex].sideImage.media().preview(this.FittingData[fittingToClone].sideImage.media().preview());
					this.FittingData[fittingIndex].sideImage.media().upload(this.FittingData[fittingToClone].sideImage.media().upload());
					this.FittingData[fittingIndex].sideImage.media().tags(this.FittingData[fittingToClone].sideImage.media().tags.slice(0));

					// this.FittingData[fittingIndex].sideImage.customPreview().media(this.FittingData[fittingToClone].sideImage.customPreview().media());
					// this.FittingData[fittingIndex].sideImage.customPreview().tags(this.FittingData[fittingToClone].sideImage.customPreview().tags.slice(0));

					// Clone back image ==========================
					this.FittingData[fittingIndex].backImage.preview(this.FittingData[fittingToClone].backImage.preview());

					this.FittingData[fittingIndex].backImage.media().preview(this.FittingData[fittingToClone].backImage.media().preview());
					this.FittingData[fittingIndex].backImage.media().upload(this.FittingData[fittingToClone].backImage.media().upload());
					this.FittingData[fittingIndex].backImage.media().tags(this.FittingData[fittingToClone].backImage.media().tags.slice(0));

					// this.FittingData[fittingIndex].backImage.customPreview().media(this.FittingData[fittingToClone].backImage.customPreview().media());
					// this.FittingData[fittingIndex].backImage.customPreview().tags(this.FittingData[fittingToClone].backImage.customPreview().tags.slice(0));

					// clone custom images ==========================
					// this.FittingData[fittingToClone].customImages.preview([]); // clear array
					// push content from other garment
					for (let index = 0; index < this.FittingData[fittingToClone].customImages.preview().length; index++) {
						const mediaToClone = this.FittingData[fittingToClone].customImages.preview()[index];

						this.FittingData[fittingIndex].customImages.preview.push(mediaToClone);

					}

					// this.FittingData[fittingToClone].customImages.media([]); // clear array
					// push content from other garment
					for (let index = 0; index < this.FittingData[fittingToClone].customImages.media().length; index++) {
						const mediaToClone = this.FittingData[fittingToClone].customImages.media()[index];

						// CURRENT VERSION DO NOT HAVE SPREAD OPERATOR (...)
						// this.FittingData[fittingIndex].customImages.media.push({
						// 	...mediaToClone,
						// 	preview : ko.observable(mediaToClone.preview()),
						// 	upload  : ko.observable(mediaToClone.upload()),
						// 	tags    : ko.observableArray(mediaToClone.tags.slice(0)),
						// });

						// clone object without clone reference
						const newObject    = JSON.parse(JSON.stringify(mediaToClone));
						newObject.preview  = ko.observable(mediaToClone.preview()); // clone object != copy object. Change object reference
						newObject.upload   = ko.observable(mediaToClone.upload()); // clone object != copy object. Change object reference
						newObject.tags     = ko.observableArray(mediaToClone.tags.slice(0)); // clone object != copy object. Change object reference

						this.FittingData[fittingIndex].customImages.media.push(
							newObject
						);

					}


					// Clone videos ==========================
					this.FittingData[fittingIndex].FittingVideo = this.FittingData[fittingToClone].FittingVideo;
					this.FittingData[fittingIndex].FittingVideoNotes = this.FittingData[fittingToClone].FittingVideo;
					console.warn("TODO: The Video is linked: if you change one, the others will be changed as well. Maybe the user don't want this !!!");


					// No need to clone tailor assigned. Change first == change all
				}

				$.jGrowl('Images and Videos have been successfully cloned !');

			};
			pos_confirm({msg:"Are you sure you want to clone this shirt fitting to other shirts?", confirmAction});

		},

		/**
		 *
		 * Returns a boolean.
		 * True if there is more than one shirt
		 */
		mustShowCloneFittingShirtButton: function () {
			// this.FittingData[0].FittingGarment
			let numberOfShirts = 0;
			this.FittingData.forEach(fittingData => {
				if (fittingData.FittingGarment.toLowerCase().trim() == "shirt") {
					numberOfShirts += 1;
				}
			});

			if (numberOfShirts >= 2) {
				return true;
			}
			return false;
		},

		/**
		 * On complete fitting, if there is at least on completion garment, the system must ask if the user wants go to payment section
		 * @returns boolean = True if user wants go to payment section / false if user wants go to fitting list
		 */
		redirectToPayment : function() {

			// check if has at least one completion data
			let completionsGarments = [];
			for (const fittingData of this.FittingData) {
				if(fittingData.useAsCompletionImages()){
					completionsGarments.push(fittingData);
				}
			}

			// redirect POS to completion page
			if (completionsGarments.length > 0) {
				/* we can go to customer orders direct, without problems */
				return (confirm("Do you want to take a payment for this order now ?"));
			}

			// go to fitting list
			else {
				return false;
			}

		},

		/**
		 * if user upload a video for a completion garment, warn user the video will be discarted
		 *
		 * @param garment = eg: orderItem.fittingsVM.FittingData[$index()]
		 */
		rememberIsCompletion : function(garment) {
			if (garment.useAsCompletionImages() && garment.FittingVideo.hasMedia()){
				alert(`${garment.FittingGarment} #${parseInt(garment.variantId) + 1} (${garment.FittingGarmentBarcode}) was marked as "COMPLETION", the video you upload will be discarded`);
			}
		},

	});

	// ██████╗ ███████╗███████╗███████╗██╗████████╗████████╗██╗███╗   ██╗ ██████╗
	// ██╔══██╗██╔════╝██╔════╝██╔════╝██║╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝
	// ██║  ██║█████╗  █████╗  █████╗  ██║   ██║      ██║   ██║██╔██╗ ██║██║  ███╗
	// ██║  ██║██╔══╝  ██╔══╝  ██╔══╝  ██║   ██║      ██║   ██║██║╚██╗██║██║   ██║
	// ██████╔╝███████╗██║     ██║     ██║   ██║      ██║   ██║██║ ╚████║╚██████╔╝
	// ╚═════╝ ╚══════╝╚═╝     ╚═╝     ╚═╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝
	//

	defFitting = SimpleDatasource.extend({
		init: function (name, dsRegistry, data) {
			orderItem.MediaUploadVM.clear();
			console.log('Fitting medias length ini fitting page');
			console.log(orderItem.MediaUploadVM.medias().length);
			if (data == null || data == undefined) {
				//	orderItem.fittingsVM.addVariantFitting('new');

				console.log("data is null");
				var Fitting = {};
				Fitting.variantId = 0;

				Fitting.frontImage = new MediaType(1, null, false, '', true);
				Fitting.sideImage = new MediaType(1, null, false, '', true);
				Fitting.backImage = new MediaType(1, null, false, '', true);
				Fitting.customImages = new MediaType(1, null, true, '', true);
				Fitting.FittingCustomer = '';
				Fitting.FittingGarmentFabric = '';
				Fitting.FittingGarmentFabricImage = '';
				Fitting.FittingGarmentBarcode = '';
				Fitting.FittingId = ""//'';
				Fitting.FittingOrderId = '';
				Fitting.FittingOrderProductId = '';
				Fitting.FittingTailor = '';
				Fitting.FittingDeliveryDay = '';
				Fitting.FittingDeliveryMonth = '';
				Fitting.FittingTailorNotes = '';
				Fitting.FittingVideo = new MediaType(2, null, true, '', true);
				Fitting.CalendarDate = new Date();
				Fitting.CalendarDate.setDate(Fitting.CalendarDate.getDate() + 15);
				Fitting.FittingDeliveryDate = Fitting.CalendarDate;
				Fitting.CalendarDate = new Date();
				Fitting.SkippedVideo = false;
				Fitting.SkippedVideoNotes = true;
				Fitting.NoVideo = false;
				Fitting.SkippedFrontTags = false;
				Fitting.SkippedSideTags = false;
				Fitting.SkippedBackTags = false;
				Fitting.NoFrontTags = false;
				Fitting.NoSideTags = false;
				Fitting.NoBackTags = false;
				Fitting.isActive = true;
				Fitting.isCompleted = false;
				Fitting.AdditionalImagesStepComplete = false;
				Fitting.FittingCloned = 0;

				Fitting.useAsCompletionImages = ko.observable(false);

				var iFitting = {
					Fitting: []
				};
				iFitting.Fitting.push(Fitting);
				this._super(name, iFitting, dsRegistry);

			}
			else {
				//console.log("data is not null");
				var iFitting = {
					Fitting: []
				};

				var x = 0;
				for (var a in data) {
					console.log(data);
					for (var b in data[a].products) {
						if (data[a].products[b].checked == true) {
							var c = localStorage.getItem("fittingindex");
							var Fitting = {};
							Fitting.variantId = x;

							Fitting.FittingCustomer = orderItem.custSelectVM.selectedCustomer();
							Fitting.FittingGarment = data[a].products[b].garment;
							Fitting.FittingGarmentFabric = data[a].products[b].fabric;
							Fitting.FittingGarmentFabricImage = data[a].products[b].fabricImage;
							Fitting.FittingGarmentBarcode = data[a].products[b].barcode;
							Fitting.FittingOrderId = data[a].order_id;
							Fitting.FittingOrderProductId = data[a].products[b].orders_products_id;
							Fitting.useAsCompletionImages = ko.observable(false);
							Fitting.useAsCompletionImages.subscribe((newValue)=>{
								// re-render TailorAssigmentControl
								if (TailorAssigmentControl){
									TailorAssigmentControl.render();
								}

								// if user is setting this garment as completion, we must check if all alteraiosn has been submitted
								if (newValue){
									this.checkIfGarmentsAreAvailableToCompletion(orderItem.fittingsVM.FittingData[orderItem.fittingsVM.selectedVariantFitting().id]);
								}
							});


							if (c == -1) {
								Fitting.FittingId = '';
								Fitting.FittingTailor = '';
								Fitting.frontImage = new MediaType(1, null, false, '', true);
								Fitting.sideImage = new MediaType(1, null, false, '', true);
								Fitting.backImage = new MediaType(1, null, false, '', true);
								Fitting.customImages = new MediaType(1, null, true, '', true);
								Fitting.CalendarDate = new Date();
								Fitting.CalendarDate.setDate(Fitting.CalendarDate.getDate() + 15);
								Fitting.FittingDeliveryDate = Fitting.CalendarDate;
								Fitting.CalendarDate = new Date();
								Fitting.FittingDeliveryDay = '';
								Fitting.FittingDeliveryMonth = '';
								Fitting.FittingTailorNotes = '';
								Fitting.FittingVideo = new MediaType(2, null, true, '', true);
								Fitting.FittingVideoNotes = [];
								Fitting.SkippedVideo = false;
								Fitting.SkippedVideoNotes = true;
								Fitting.NoVideo = false;
								Fitting.SkippedFrontTags = false;
								Fitting.SkippedSideTags = false;
								Fitting.SkippedBackTags = false;
								Fitting.NoFrontTags = false;
								Fitting.NoSideTags = false;
								Fitting.NoBackTags = false;
								Fitting.isActive = true;
								Fitting.isCompleted = false;
								Fitting.AdditionalImagesStepComplete = false;
								Fitting.FittingCloned = 0;

							} else {
								Fitting.AdditionalImagesStepComplete = true;

								Fitting.FittingId = data[a].products[b].fittings[c].fitting.fitting_id;
								if (data[a].products[b].fittings[c].fitting.cloned) {
									Fitting.FittingCloned = data[a].products[b].fittings[c].fitting.cloned;
								}
								else {
									Fitting.FittingCloned = 0;
								}
								if (data[a].products[b].fittings[c].fitting.is_active == "1" && (!data[a].products[b].with_tailor || data[a].products[b].skipStep)) {
									Fitting.isActive = true;
								} else {
									Fitting.isActive = false;
								}
								if (data[a].products[b].completed == "1") {
									Fitting.isCompleted = true;
								} else {
									Fitting.isCompleted = false;
								}

								Fitting.frontImage = new MediaType(1, null, false, '', Fitting.isActive);
								Fitting.sideImage = new MediaType(1, null, false, '', Fitting.isActive);
								Fitting.backImage = new MediaType(1, null, false, '', Fitting.isActive);
								Fitting.customImages = new MediaType(1, null, true, '', Fitting.isActive);
								Fitting.FittingVideo = new MediaType(1, null, true, '', Fitting.isActive);

								console.log("data[a].products[b].fittings[c].tailor: " + JSON.stringify(data[a].products[b].fittings[c].tailor.tailor_id));
								console.log("data[a].products[b].fittings[c].tailor.tailor_id: " + data[a].products[b].fittings[c].tailor.tailor_id);
								var tailors = dsRegistry.getDatasource('tailorsDS').getStore();
								console.log("TAILORS: " + JSON.stringify(tailors));
								for (var ind = 0; ind < tailors.length; ind++) {
									if (tailors[ind].user_id == data[a].products[b].fittings[c].tailor.tailor_id) {
										Fitting.FittingTailor = tailors[ind];
										break;
									}
								}

								var date = data[a].products[b].fittings[c].tailor.delivery_date;
								var day = date.substring(date.lastIndexOf("-") + 1);
								var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
								var year = date.substring(0, date.indexOf("-"));
								Fitting.FittingDeliveryDay = day * 1.0;
								month = month * 1.0;
								Fitting.FittingDeliveryMonth = month * 1.0;
								var dateFormat = new Date(date);
								dateFormat.setDate(dateFormat.getDate());

								Fitting.FittingDeliveryDate = dateFormat;
								Fitting.CalendarDate = new Date();
								Fitting.FittingTailorNotes = data[a].products[b].fittings[c].tailor.tailor_notes;
								Fitting.SkippedVideo = false;
								if (data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0") {
									Fitting.SkippedFrontTags = false;
								} else {
									Fitting.SkippedFrontTags = true;
								}
								if (data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0") {
									Fitting.SkippedSideTags = false;
								} else {
									Fitting.SkippedSideTags = true;
								}
								if (data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0") {
									Fitting.SkippedBackTags = false;
								} else {
									Fitting.SkippedBackTags = true;
								}
								if (data[a].products[b].fittings[c].fitting.video_pending == "0") {
									Fitting.SkippedVideo = false;
								} else {
									Fitting.SkippedVideo = true;
								}
								if (data[a].products[b].fittings[c].fitting.video_notes_pending == "0") {
									Fitting.SkippedVideoNotes = true;
								} else {
									Fitting.SkippedVideoNotes = true;
								}
								Fitting.NoVideo = data[a].products[b].fittings[c].videos.length == 0;
								//if(data[a].products[b].fittings[c].videos.length == 0 && data[a].products[b].fittings[c].fitting.video_pending == "0"){
								for (var x in data[a].products[b].fittings[c].videos) {
									Fitting.FittingVideo.addMedia(data[a].products[b].fittings[c].videos[x].video_path, data[a].products[b].fittings[c].videos[x].video_path, data[a].products[b].fittings[c].videos[0].is_sync == 1);
									try {
										if (!data[a].products[b].fittings[c].videos[x].video_notes || data[a].products[b].fittings[c].videos[x].video_notes.length < 3) {
											console.log('Nothing to do');
										}
										else {
											var videoNotes = JSON.parse(data[a].products[b].fittings[c].videos[x].video_notes);
											for (var y in videoNotes) {
												Fitting.FittingVideo.addTag(videoNotes[y], 0);
											}
										}
									} catch (exception) {
										var videoNotes = [data[a].products[b].fittings[c].videos[x].video_notes];
										for (var y in videoNotes) {
											Fitting.FittingVideo.addTag(videoNotes[y], 0);
										}
									}

								}

								Fitting.NoFrontTags = false;
								Fitting.NoSideTags = false;
								Fitting.NoBackTags = false;

								for (var i = 0; i < data[a].products[b].fittings[c].images.length; i++) {
									try {
										var what = "";
										if (data[a].products[b].fittings[c].images[i].fitting_image_type == "1") {
											what = Fitting.frontImage;
										} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "2") {
											what = Fitting.sideImage;
										} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "3") {
											what = Fitting.backImage;
										} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "4") {
											what = Fitting.customImages;
										}



										var is_sync = data[a].products[b].fittings[c].images[i].is_sync;
										if (is_sync == '1') {
											var previewPath = data[a].products[b].fittings[c].images[i].image_local_path;
											var uploadPath = data[a].products[b].fittings[c].images[i].image_local_path;
										}
										else {
											var previewPath = data[a].products[b].fittings[c].images[i].fitting_image_path;
											var uploadPath = data[a].products[b].fittings[c].images[i].fitting_image_path;
										}
										what.addMedia(previewPath, uploadPath, is_sync == '1');
										var servertags = JSON.parse(data[a].products[b].fittings[c].images[i].fitting_image_tags);
										if (servertags != null && servertags.length > 0) {
											if (data[a].products[b].fittings[c].images[i].fitting_image_type == "4") {
												media_index = what.getMediaPreview()().length - 1;
											}
											else {
												media_index = 0;
											}
											what.addTags(servertags, 0);
										} else {
											if (data[a].products[b].fittings[c].images[i].fitting_image_type == "1") {
												if (data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0") {
													Fitting.NoFrontTags = true;
												}
											} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "2") {
												if (data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0") {
													Fitting.NoSideTags = true;
												}
											} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "3") {
												if (data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0") {
													Fitting.NoBackTags = true;
												}
											}
										}
									} catch (e) { ; }
								}

							}
							Fitting.frontImage.updateChange();
							Fitting.sideImage.updateChange();
							Fitting.backImage.updateChange();
							Fitting.customImages.updateChange();
							x++;
							//console.log("Fitting: " + JSON.stringify(Fitting));
							iFitting.Fitting.push(Fitting);
						}
					}
				}
				this._super(name, iFitting, dsRegistry);
			}
		},
		/**
		 * Before start completion, we must check if this garments are available to send completion images
		 *
		 * The garment should not have any pending alteration report
		 */
		checkIfGarmentsAreAvailableToCompletion : function (fitting) {
			console.log('checkIfGarmentsAreAvailableToCompletion fitting', fitting);
			document.getElementById('loading_jp').style.display = "block";
			const mapTypeToId = (typeString) => {
				typeString = typeString.toLowerCase();

				if (typeString.includes("pant")) {
					return 1;
				} else if (typeString.includes("jacket")) {
					return 4;
				} else if (typeString.includes("vest")) {
					return 2;
				} else if (typeString.includes("shirt")) {
					return 3;
				}
				else {
					console.error("garment not identified");
					return null;
				}
			}

			const getGarmentFromId = (garmentId, garmentType) => {
				const search = orderItem.fittingsVM.FittingData.find(fittingData => (
					fittingData.FittingOrderProductId == garmentId &&
					mapTypeToId(fittingData.FittingGarment) == garmentType
				));
				if (search){
					return search
				}else{
					console.error("garment not found", garmentId);
					return {};
				}
			};

			const url = window.BUrl + "alterations_pos/are_all_alteration_reports_submitted";
			const dataToSend = [{
				garment_id: fitting.FittingOrderProductId,
				garment_type: mapTypeToId(fitting.FittingGarment),
			}]

			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'json',
				async: true,
				data: {
					"user": authCtrl.userInfo,
					garments: JSON.stringify(dataToSend),
				},
				success: function (dataS) {
					console.log('dataS', dataS);

					const unavailableGarments = dataS.garments.filter(garments => !garments.are_all_alterations_submitted);
					console.log('unavailableGarments', unavailableGarments);

					if (unavailableGarments.length > 0) {
						pos_warning({
							msg: /* html */`
								<div>
									<span>This garment is unavailable for completion. Please check if all alterations has been submitted</span><br>
									<h3 style="margin: 20px 0;">Unavailable Garment</h3>

									${unavailableGarments.map(unavailableGarment => {
										const garment = getGarmentFromId(unavailableGarment.garment_id, unavailableGarment.garment_type);
										return /* html */`
											<span>
												${garment.FittingCustomer.customer_first_name} ${garment.FittingCustomer.customer_last_name}'s
												${garment.FittingGarment} (${garment.FittingGarmentFabric})
												- Order ${garment.FittingOrderId}
											</span>
											<br>
										`
									}).join("")}

									<p>Do you want remove this garment?</p>

								</div>
							`,
							title: 'Ops',
							confirmAction: () => {
								// remove this garment from fitting
								unavailableGarments.forEach(unavailableGarment => {
									const garment = getGarmentFromId(unavailableGarment.garment_id, unavailableGarment.garment_type);
									console.log("Clearing garment", garment);
									orderItem.fittingsVM.removeVariantFitting(garment.variantId, false);
								});

								// if we just removed the last garment from fiting,
								// back to fitting list
								if (orderItem.fittingsVM.FittingData.length == 0) {
									posChangePage('#customerGarmentsFittingsList');
								}

							},
							confirmBtn: "Yes",
							cancelAction: () => {
								// un mark this garment as completion
								orderItem.fittingsVM.FittingData[(fitting.variantId)].useAsCompletionImages(false);
							}
						})
					}

				},
				error: function (dataS) {
					console.error(dataS);
				},
				complete :()=>{
					document.getElementById('loading_jp').style.display = "none";
				}
			});
		}
	});


	// ███████╗██╗████████╗████████╗██╗███╗   ██╗ ██████╗ ██╗     ██╗███████╗████████╗
	// ██╔════╝██║╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝ ██║     ██║██╔════╝╚══██╔══╝
	// █████╗  ██║   ██║      ██║   ██║██╔██╗ ██║██║  ███╗██║     ██║███████╗   ██║
	// ██╔══╝  ██║   ██║      ██║   ██║██║╚██╗██║██║   ██║██║     ██║╚════██║   ██║
	// ██║     ██║   ██║      ██║   ██║██║ ╚████║╚██████╔╝███████╗██║███████║   ██║
	// ╚═╝     ╚═╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝╚═╝╚══════╝   ╚═╝
	//
	// View model for fittingList.htm
	FittingList = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var that = this;
			this.burl = BUrl;
			this.items = ko.observable();
			this.ordersPerPage = ko.observable(10);
			this.FittingList = ko.observableArray([]);
			this.page = ko.observable(1);
			this.hasPreviousPage = ko.observable(false);
			this.hasNextPage = ko.observable(false);

			this.customerInfoPopUp = ko.observable(false);
			this.customerData = ko.observable(false);


			loadFitting = function (data) {

				console.log("loading fitting...");

				let found = false;

				var customers = orderItem.custSelectVM.customers();
				for (x in customers) {
					var temp = customers[x];

					console.log("CUSTOMER", temp);

					if (temp.server_id == data.customer_id)
					{
						found = true;
						customersVM.systemMode = "fitting";
						orderItem.custSelectVM.selectedCustomer(temp);
						posChangePage('#customerGarmentsFittingsList');
					}
				}

				if(!found)
				{
					alert("Sorry, this customer does't belong to you or is not linked to your user. You need to link this customer to your user from ERP before accessing his data.");
				}

			};

			client_data_pop_up = function (customer_id) {

				self = this;

				console.log("customer_id", customer_id);

				changeVisibilityDiv('loading_jp');

				$.ajax({
					type: 'POST',
					url: BUrl + "alterations_pos/get_customer_data",
					dataType: 'json',
					data: { "user": authCtrl.userInfo, "customer_id": customer_id },
					async: false,
					success: function (ret) {

						console.log("ret", ret);
						changeVisibilityDiv('loading_jp');
						const customer = ret.customer;
						const cities = ret.cities;

						$html = `<div>

				      <div class="cancel-btn" onclick="cancel_customer_data()">X</div>

				      <h2><strong>Customer Info</strong></h2>
				      <h2><strong>${customer.customer_first_name + ' ' + customer.customer_last_name} </strong> <span style="font-size:18px;">CUSTOMER SINCE: <strong>${customer.customer_date_added}</strong></span> </h2>
				      <br>

				      <span class="input-name-span" ><span class="input-span">Address 1:</span> <input type="text" value="${customer.customer_address1}" class="address1 input-value"> </span> <br></br>

				      <span class="input-name-span" ><span class="input-span">Address 2:</span> <input type="text" value="${customer.customer_address2}" class="address2 input-value"> </span> <br></br>

				      <span class="input-name-span" >

				      <span class="input-span">City:</span>
				      	<select class="cities-select">
				      		${cities.map(function (e) {
								return `<option value="${e.locations_id}" ${e.locations_id == customer.customer_city ? " selected " : ""} >${e.location_name}</option>`;
							}).join("")}
				      	</select>

				      </span> <br></br>

				      <span class="input-name-span" ><span class="input-span">Postal Code:</span> <input type="text" value="${customer.customer_postal_code}" class="postal input-value"> </span> <br></br>

				      <span class="input-name-span" ><span class="input-span">Mobile Phone:</span> <input type="text" value="${customer.customer_mobile_phone}" class="phone input-value"> </span> <br></br>

				      <span class="input-name-span" ><span class="input-span">Email:</span> <input type="text" value="${customer.customer_email}" class="phone input-value"> </span> <br></br>


				      <div>
				        <button class="form-btn accept-btn" onclick="submit_customer_data()" style="background-color: green;float:right" data-customer-id="${customer.customer_id}">Update Details</button>
				      </div>

				    </div>`;


						$(".customer-info-pop-up").html($html).show();
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						changeVisibilityDiv('loading_jp');
						alert("Something wrong :(, please try again");
						console.log("fitting comments endpoint cannot be reached! ");
						console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
					}
				}); // end ajax
			};



			loadOrderGarmentFittings = function(customer_id, order_id) {
				console.log("customer_id", customer_id);

				const all_customers = orderItem.custSelectVM.customers();

				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////// Start boilerplate to go straight to fitting process (to be compactible with old version) ///////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				let foundCustomer = false;

				for (let i in all_customers) {
					const customer = all_customers[i];

					if (customer.server_id == customer_id) {

						foundCustomer = true;


						var html = $('#customerInfo').remove();
						customersVM.setObservables(customersVM.selected_item());
						//ko.applyBindings(orderItem, document.getElementById('customerGarmentsFittingsList'));

						orderItem.OrdersGarmentsForFittingVM.ordersgarments([]);
						localStorage.removeItem('OrdersGarmentsForFittingDS');

						console.log("AJAX........");
						$.ajax({
							type: 'POST',
							url: BUrl + "alterations_pos/get_fitting_customer_garments",
							dataType: 'json',
							data: { "user": authCtrl.userInfo, "customer_id": customer_id, "order_id": order_id },
							async: false,
							timeout: 45000,
							success: function (ret) {

								console.log("get_fitting_customer_garments", ret);
								orderItem.OrdersGarmentsForFittingVM.ordersgarments(ret);

							},
							error: function (XMLHttpRequest, textStatus, errorThrown) {
								console.log("fitting comments endpoint cannot be reached! ");
								console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
								customAlert("TIME OUT - there is a network issue. Please try again later");
								posChangePage("#fittingList");
							}
						});


						orderItem.OrdersGarmentsForFittingVM.subscribeTo('OrdersGarmentsForFittingDS');


						orderItem.custSelectVM.selectedCustomer(customer);

						for (var x = 0; x < 100; x++) {
							localStorage.removeItem('FittingFrontImagePhoto' + x);
							localStorage.removeItem('FittingBackImagePhoto' + x);
							localStorage.removeItem('FittingSideImagePhoto' + x);
							localStorage.removeItem('FittingCustomImagePhoto' + x);
							localStorage.removeItem('FittingFrontImageTags' + x);
							localStorage.removeItem('FittingBackImageTags' + x);
							localStorage.removeItem('FittingSideImageTags' + x);
							localStorage.removeItem('FittingCustomImageTags' + x);

							localStorage.removeItem('CompletionFrontImagePhoto' + x);
							localStorage.removeItem('CompletionBackImagePhoto' + x);
							localStorage.removeItem('CompletionSideImagePhoto' + x);
						}

						for (let i in orderItem.OrdersGarmentsForFittingVM.ordersgarments()) {
							const order = orderItem.OrdersGarmentsForFittingVM.ordersgarments()[i];

							for (let j in order.products) {
								const prod = order.products[j];
								prod.checked = true;
							}
						}


						localStorage.removeItem("fittingindex");
						localStorage.setItem("fittingindex", fittingindex = -1);
						orderItem.fittingsVM = new Fitting(orderItem.dsRegistry);
						orderItem.fittingsDS = new defFitting('fittingsDS', orderItem.dsRegistry, orderItem.OrdersGarmentsForFittingVM.ordersgarments());
						orderItem.fittingsVM.subscribeTo('fittingsDS');
						console.log("orderItem.fittingsDS.getStore(): " + JSON.stringify(orderItem.fittingsDS.getStore()));
						//console.log("orderItem.fittingsVM.FittingData: " + JSON.stringify( orderItem.fittingsVM.FittingData ) );
						orderItem.fittingsVM.addVariantsFittings();


						posChangePage("#fittings");


						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/////////////////////////////////// Ending boilerplate to go straight to fitting process ///////////////////////////////////
						////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					}
				}

				if (!foundCustomer) // custumer doest belong to this salesman or is not linked to him
				{
					alert("Sorry, this customer does't belong to you or is not linked to your user. You need to link this customer to your user from ERP before accessing his data.");
				}

			};

			this.nextPage = function () {

				if (that.items() > that.page() * that.ordersPerPage()) {
					that.hasNextPage(true);
				}
				else {
					that.hasNextPage(false);
				}
				//console.log(that.hasNextPage());
				if (that.hasNextPage()) {
					that.page(that.page() + 1);
				}

			};

			this.lastPage = function () {
				var maxPage = Math.ceil(that.items() / that.ordersPerPage());
				that.page(maxPage);
			};

			this.previousPage = function () {
				if (that.page() > 1) {
					that.hasPreviousPage(true);
				}
				else {
					that.hasPreviousPage(false);
				}
				if (that.hasPreviousPage())
					that.page(that.page() - 1);

			};

			this.firstPage = function () {
				that.page(1);
			};
			this.page.extend({ notify: 'always' });
			this.page.subscribe(function (dara) {
				that.loadFittingPage();
			});
		},


		loadFittingPage: function () {

			var that = this;
			var server = this.burl;
			server = server + 'client_fittings/get_order_fittings2';

			//collect the user data and prepare to send it.
			data = {
				items_per_page: that.ordersPerPage(),
				page_no: (that.page() - 1) * that.ordersPerPage()
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			//send the information for the endpoint and wait or an answer
			$.ajax({
				type: 'POST',
				url: server,
				timeout: 120000,
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function (dataS) {

					console.log("RES", dataS);
					//store all the new info into the array
					try {
						data = JSON.parse(dataS);
					} catch (error) {
						customAlert(`something wrong with ${server} url. Please try Again later`);
						console.log('dataS: ', dataS);
						posChangePage("#main");
						return;
					}
					that.items(data.items_no);
					temp = data.items;
					that.FittingList.removeAll();

					for (let i in temp) {
						const order = temp[i];


						// Counts the products to be able ro render in the list
						let productsMapCounter = {};


						for (let j in order.products)
						{
							const product = order.products[j];

							if( productsMapCounter[product.garment_name] == undefined )
							{
								productsMapCounter[product.garment_name] = { name : product.garment_name, qty : 0}
							}

							productsMapCounter[product.garment_name].qty += 1;
						}

						// Object.values is not supported in IOS 10.2 !!!!!!!
						order.products = Object.keys(productsMapCounter).map(e => productsMapCounter[e]).map(function (e) {
							return `${e.qty}x ${e.name}`;
						}).join(", ");

					}


					for (j in temp) {
						if (temp[j].last_fitting == "01/01/1970") {
							temp[j].last_fitting = "-";
						}

						that.FittingList.push(temp[j]);
					}

					if(temp.length == 0)
					{
						const $ERROR_HTML = `<h2 style="color:red">You have no fittings available</h2>`;
						$("#no-fittings-available-list-div").html($ERROR_HTML);
					}

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {

					const $ERROR_HTML = `<h2 style="color:red">An unexpected error has occurred, please try again...</h2>`;
					$("#no-fittings-available-list-div").html($ERROR_HTML);

					alert("Something wrong :(, please try again");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
				},
				async: false
			});
		},


	});

	//END DEFINE CLOSURE
});
