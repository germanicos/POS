define(['jquery', 'knockout', 'base'], function($, ko) {

	ErrorCategoriesList = SimpleControl.extend({
		init: function(){
			this.categories = ko.observableArray();
		},
		loadInfoCategories: function(){
			var that = this;
			var comp = '';
			var server = BUrl;
			server = server + 'error_report_endpoint/get_user_errors_categories';


			//collect the user data and prepare to send it.
			data = {
				user_id: authCtrl.userInfo.user_id
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			that.categories.removeAll();

			//send the information for the endpoint and wait or an answer
			$.ajax({
				type: 'POST',
				url: server,
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//store all the new info into the array
					that.categories.removeAll();
					temp = JSON.parse(dataS);
					//ko is a bitch
					for(i in temp){
						that.categories.push(temp[i]);
					}
				},
				async: false
			});
		},
		loadAllErrors: function(){
			errorReportListVM.loadErrorsList();
			posChangePage("#errorList");
		},
		loadQVErrors: function(){
			errorReportListVM.loadErrorsList(null,null,false,true);
			posChangePage("#errorList");
		},
		loadDeductableErrors: function(){
			errorReportListVM.loadErrorsList(null,null,true);
			posChangePage("#errorList");
		},
		loadCategorieErros: function(category){
			errorReportListVM.loadErrorsList(category);
			posChangePage("#errorList");
		},
		loadGarmentErros: function(garment){
			errorReportListVM.loadErrorsList(null, garment);
			posChangePage("#errorList");
		}
	});


	SlideShowVM = SimpleControl.extend({
		init: function(error){
			var that = this;
			this.returnToCategorie = error ? false : true;
			this.Images = ko.observableArray();
			this.Videos = ko.observableArray();
			this.ShowingMediaType = ko.observable(0);
			this.IsRowling = ko.observable(true);
			this.waiting = 0;
			this.ImagePos = ko.observable(-1);
			this.MediaNumber = ko.observable(0);
			this.imagePos = -1;
			this.videoPos = -1;
			this.Media = ko.observable();
			this.Tags = ko.observableArray();

			this.IsRowling.subscribe(function(data){
				if(data){
					that.slideAuto();
				}
			});
			this.ShowingMediaType.subscribe(function(data){
				that.IsRowling(false);
				if(that.ShowingMediaType() == 0){
					that.MediaNumber(that.Images().length);
					if(that.imagePos >= 0){
						that.ImagePos(that.imagePos);
					}
					else if(that.Images().length){
						that.ImagePos(0);
					}
				}
				else if(that.ShowingMediaType() == 1){
					that.MediaNumber(that.Videos().length);
					if(that.videoPos >= 0){
						that.ImagePos(that.imagePos);
					}
					else if(that.Videos().length){
						that.ImagePos(0);
					}
				}
				else if(that.ShowingMediaType() == 2){
					that.MediaNumber(that.Images().length + that.Videos().length);
					if(that.imagePos >= 0){
						that.ImagePos(that.imagePos);
					}
					else if(that.Videos().length){
						that.ImagePos(0);
					}
				}
			});

			this.selectImageByPath = function(path){
				that.IsRowling(false);
				that.ShowingMediaType(0);
				for(var x in that.Images()){
					if(that.Images()[x].media_path = path){
						that.ImagePos(x);
					}
				}
			};

			this.selectVideoByPath = function(path){
				that.IsRowling(false);
				that.ShowingMediaType(1);
				for(var x in that.Videos()){
					if(that.Videos()[x].media_path = path){
						that.ImagePos(x);
					}
				}
			};
			this.ImagePos.extend({ notify: 'always' });
			this.ImagePos.subscribe(function(data){
				that.Tags.removeAll();
				if(that.ShowingMediaType() == 0){
					that.imagePos = data;
					that.Media(that.Images()[data]);
					try{
						var temp = JSON.parse(that.Images()[data].media_tags);
					}catch(e){
						var temp = [];
					}

				}
				else if(that.ShowingMediaType() == 1){
					that.Media(that.Videos()[data]);
					that.videoPos = data;
					try{
						var temp = JSON.parse(that.Videos()[data].media_tags);
					}catch(e){
						var temp = [];
					}
				}
				else if(that.ShowingMediaType() == 2){
					if(data >=  that.Images().length){
						that.imagePos = data;
						that.Media(that.Videos()[data]);
						try{
							var temp = JSON.parse(that.Videos()[data].media_tags);
						}catch(e){
							var temp = [];
						}
					}
					else{
						that.Media(that.Images()[data]);
						try{
							var temp = JSON.parse(that.Images()[data].media_tags);
						}catch(e){
							var temp = [];
						}
					}
				}
				for(var y in temp){
					that.Tags.push(temp[y]);
				}
			});

			this.slideAuto = function(){
				if(that.waiting > 0)
					that.waiting--;
				if(that.IsRowling() && that.waiting === 0){
					that.nextMedia();
					that.waiting++;
					setTimeout(that.slideAuto, 5000);
				}
			};

			this.nextMedia = function(){
				var temp = Number(that.ImagePos());
				temp = temp + 1;
				var asd = Number(that.MediaNumber());
				temp = temp  % asd;
				that.ImagePos(temp);
			};

			this.previousMedia = function(){
				var temp = that.ImagePos() == 0 ? that.MediaNumber() - 1 : that.ImagePos() - 1;
				that.ImagePos(temp);
			};

			this.start = function(){
				that.IsRowling(true);
				that.nextMedia();
				that.waiting++;
				setTimeout(that.slideAuto, 5000);
			};

			this.pause = function(){
				that.IsRowling(false);
			};

		},
		addImage: function(image){
			this.Images.push(image);
			if(this.ShowingMediaType() == 0){
				this.MediaNumber(this.Images().length);
			}
			else if(this.ShowingMediaType() == 1){
				this.MediaNumber(this.Videos().length);
			}
			else if(this.ShowingMediaType() == 2){
				this.MediaNumber(this.Images().length + this.Videos().length);
			}
		},
		addVideo: function(video){
			this.Videos.push(video);
			if(this.ShowingMediaType() == 0){
				this.MediaNumber(this.Images().length);
			}
			else if(this.ShowingMediaType() == 1){
				this.MediaNumber(this.Videos().length);
			}
			else if(this.ShowingMediaType() == 2){
				this.MediaNumber(this.Images().length + this.Videos().length);
			}
		}
	});

	/**
	** This class get all the error reports on the database and report the current error the user is working with
	** Main features:
	** loadErrorsList -  function that will make an post request and all the error report info
	** selectedError -  an object that stores the error report that the user is working.
	** ErrorData - an observable array that stores all the error reports
	**/
	ErrorReportListVM = SimpleControl.extend({

		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.errorsPerPage = 10;
			this.active = ko.observable(true);
			this.notActive = ko.observable(false);
			this.page = ko.observable(1);
			this.hasNextPage = ko.observable(false);
			this.hasPreviousPage = ko.observable(false);
			this.selectedError = 0;
			this.categories = ko.observableArray();
			this.selectedCategory = ko.observable();
			this.selectedGarment = ko.observable();
			this.errorCatgory = new ErrorCategory(this.DsRegistry);

			this.errorCatgory.loadInfo();

			//variables that are harded
			this.status = ko.observableArray([
				{status_id: '1', status_text:'Open'},
				{status_id: '2', status_text:'Awaiting Reply'},
				{status_id: '3', status_text:'Holding for Later'},
				{status_id: '4', status_text:'Resolved'},
				{status_id: '5', status_text:'Closed'}
			]);
			//where the data will be stored
			this.ErrorData = ko.observableArray();
			this.ErrorVisible = ko.observableArray();
			this.burl = BUrl;

			this.LoadCategoryImages = function(){
				error_id = [];
				for(var x in that.ErrorData()){
					error_id.push(that.ErrorData()[x].error_id);
				}
				data = {
					errors_id: error_id
				}
				DataRequest(true, "error_report_endpoint/get_category_images", data ,that.loadSlideShow);
			};

			this.applyFilter = function(){
				//var category = that.selectedCategory() ? that.selectedCategory().category_id: null;
				var garment = that.selectedGarment() ? that.selectedGarment(): null;
				that.loadErrorsList(that.errorCatgory.categorieID, garment);
			};

			this.loadSlideShow = function(data){
				console.log(data);
				if(data && data.length > 0 ){
					ErrorSlideShow = new SlideShowVM(true);
					for(x in data){
						data[x].media_path = BUrl + data[x].media_path;
						if(data[x].media_type == 1){
							ErrorSlideShow.addImage(data[x]);
						}
						else if(data[x].media_type == 2){
							ErrorSlideShow.addVideo(data[x]);
						}
					}
					ErrorSlideShow.ImagePos(0);
					ErrorSlideShow.start();

					posChangePage("#errorSlideShow");
				}
				else{
					customAlert("No media to be shown");
				}
			};

			//function to load a specific error based on click
			loadSpecificError = function(errorInfo){
				that.selectedError = errorInfo.error_id;
				errorReportVM.cleanAll();
				errorReportVM.setSelectedError(that.selectedError);
				errorReportVM.loadInfo();
				errorReportVM.loadSlideShow();
				posChangePage('#errorReport');
			};

			this.addNewError = function(){
				errorReportVM.cleanAll();
				errorReportVM.prepareToInsert();
				posChangePage('#errorAdd');
			};

			this.changeList = function(){
				that.active(!that.active());
				that.loadErrorsList();
			};

			this.nextPage = function(){
				if(Math.ceil(that.ErrorData().length/that.errorsPerPage) > that.page()){
					that.hasNextPage(true);
				}
				else{
					that.hasNextPage(false);
				}
				console.log(that.hasNextPage());
				if(that.hasNextPage()){
					that.page(that.page() + 1);
				}

			};

			this.lastPage = function(){
				var maxPage = Math.ceil(that.ErrorData().length/that.errorsPerPage);
				if(maxPage == 0){
					maxPage = 1;
				}
				that.page(maxPage);
				if(maxPage < that.page()){
					that.hasNextPage(true);
				}
			};

			this.previousPage = function(){
				if(that.page() > 1){
					that.hasPreviousPage(true);
				}
				else{
					that.hasPreviousPage(false);
				}
				if(that.hasPreviousPage())
					that.page(that.page() - 1);

				//console.log(that.hasPreviousPage());

			};

			this.firstPage = function(){
				that.page(1);
			};

			this.page.extend({ notify: 'always' });
			this.page.subscribe(function(data){
				that.ErrorVisible.removeAll();
				var limit = data * that.errorsPerPage;
				//alert(i < that.ErrorData && i < limit);
				for(i = (data - 1) * that.errorsPerPage; i < that.ErrorData().length && i < limit; i++){
					that.ErrorVisible.push(that.ErrorData()[i]);
				}
				//console.log(that.ErrorVisible());
			});

			this.active.subscribe(function(data){
				that.notActive(!data);
			});

			this.ErrorData.subscribe(function(data){
				that.page(1);
				that.hasPreviousPage(false);
				if(Math.floor(that.ErrorData().length/that.errorsPerPage) > 1){
					that.hasNextPage(true);
				}
				else{
					that.hasNextPage(false);
				}
			});

		},

		//this function load Errors will send the authentication data(user/password) to the server and then receive the errorsList back
		loadErrorsList: function(category, garment, deductable, qv) {
			var that = this;
			var comp = '';
			var server = this.burl;
			if(that.active())
			{
				server = server + 'error_report_endpoint/get_user_errors';
			}else{
				server = server + 'error_report_endpoint/get_user_errors_history';
			}
			that.errorCatgory.clear();
			that.errorCatgory.setSelectedCategory(category);
			that.selectedGarment(Number(garment));


			//collect the user data and prepare to send it.
			data = {
				user_id: authCtrl.userInfo.user_id,
				category: that.errorCatgory.categorieID != 0 ? that.errorCatgory.categorieID : null,
				deductable: deductable,
				qv: qv,
				garment: garment
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			that.ErrorData.removeAll();

			//send the information for the endpoint and wait or an answer
			$.ajax({
				type: 'POST',
				url: server,
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//store all the new info into the array
					that.ErrorData.removeAll();
					temp = JSON.parse(dataS);
					for(x in temp){
						temp[x].error_status = that.statusIDtoText(temp[x].error_status);
					}
					//ko is a bitch
					for(i in temp){
						that.ErrorData.push(temp[i]);
					}
				},
				async: false
			});

		},

		statusIDtoText: function(value){
			for(x in this.status()){
				if(value == this.status()[x].status_id)
					return this.status()[x].status_text;
			}
			return '';
		}

	});

	ErrorReportVM = SimpleControl.extend({
		init: function(DsRegistry, customerVM) {
			this._super( DsRegistry );
			this.customerSelectVM = customerVM;
			var that = this;


			//variables that store the error information. Loadaded by the function load info
			that.error_id = ko.observable();
			that.error_assigned_to = ko.observable();
			that.error_cat_id = ko.observable();
			that.error_date = ko.observable();
			that.error_desc = ko.observable();
			that.error_fix_date = ko.observable();
			that.error_link_customers = ko.observable();
			that.error_link_order = ko.observable();
			that.error_link_users = ko.observableArray();
			that.error_linked_garment = ko.observable();
			that.error_media_ids = ko.observable();
			that.error_media_video = ko.observable();
			that.error_priority = ko.observable();
			that.error_reason = ko.observable();
			that.error_report_by = ko.observable();
			that.error_status = ko.observable();
			that.error_title = ko.observable();
			that.garment_text = ko.observable();
			that.order_text = ko.observable();
			that.media = ko.observableArray();
			that.replies = ko.observableArray();
			that.previewCustomImageAdd = ko.observableArray([]);
			that.previewCustomVideoAdd = ko.observableArray([]);
			that.errorImagesAdd = new MediaType();
			that.errorVideosAdd = new MediaType();
			that.previewCustomImage = ko.observableArray([]);
			that.previewCustomVideo = ko.observableArray([]);
			that.errorImages = new MediaType();
			that.errorVideos = new MediaType();
			that.deductionAmount = ko.observable('');
			that.newStatus = ko.observable();
			that.isChangingStatus = ko.observable(false);
			that.showQC = ko.observable(true);
			that.showDeduction = ko.observable(true);
			that.addDeductionList = ko.observable(false);
			that.QV = ko.observable(false);

			this.garmentMedia = new GarmentMedia(null);
			this.orderGarments = new OrderGarmentAutoComplete(this.DsRegistry);
			this.customerOrder = new CustomerOrdersAutoComplete(this.DsRegistry);
			this.customers = new CustomerAutoCompleteVM(this.DsRegistry, this.customerSelectVM);
			this.errorCatgory = new ErrorCategory(this.DsRegistry);
			this.users = new UsersAutoComplete(this.DsRegistry);
			this.ErrorReplyVM = new ErrorReplyVM(this.DsRegistry);
			this.mediaUpload = orderItem.MediaUploadVM;
			this.customerOrder.setOrderGarment(this.orderGarments);
			this.customers.setCustomerOrder(this.customerOrder);

			//indicates which ErrorReport the user select at a time
			this.selectedError = 0;
			this.errorImagesAdd = new MediaType(2, null, true, '',true);
			this.errorVideosAdd = new MediaType(1, null, true, '',true);

			that.errorImagesAdd.setCustomPreview(that.previewCustomImageAdd);
			that.errorVideosAdd.setCustomPreview(that.previewCustomVideoAdd);

			this.loadSlideShow = function(){
				ErrorSlideShow = new SlideShowVM();
				for(var ind in that.media()){
					if(that.media()[ind].media_type == 1){
						ErrorSlideShow.addImage(that.media()[ind]);
					}
					else if(that.media()[ind].media_type == 2){
						ErrorSlideShow.addVideo(that.media()[ind]);
					}
				}

				for(var c in that.replies()){
					for(var ind in that.replies()[c].medias){
						if(that.replies()[c].medias[ind].media_type == 1){
							ErrorSlideShow.addImage(that.replies()[c].medias[ind]);
						}
						else if(that.replies()[c].medias[ind].media_type == 2){
							ErrorSlideShow.addVideo(that.replies()[c].medias[ind]);
						}
					}
				}
			};

			this.sendReport = function(){
				var email = $('#ErrorEmail').val().trim();
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				var result =  re.test(email);
				if(email == "" || result == false){
					customAlert("Please enter a valid email address");
				}else{
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";
					var data = {
						error_id: that.error_id(),
						add: email
					};
					DataRequest(false, 'error_report_endpoint/generate_report', data, function(){customAlert('Report Generated');},function(){customAlert('Labels was not sent. Please try again later or cantact the admins about the issue');},function(){customAlert('Report Generated');});
					spinner.style.display = "none";
				}
			};

			this.clearCustomer = function(){

			};

			this.addImages = function(){
				var media = that.garmentMedia.getMediaselected();
				for(var i in media.images){
					that.addGarmentMedia(media.images[i], 1 );
				}
				for(var i in media.videos){
					that.addGarmentMedia(media.videos[i], 2 );
				}
			};

			this.buildError = function(customer, order, garment){
				that.cleanAll();
				that.prepareToInsert();
				that.customers.selectCustomer(customer.customer_first_name + " " + customer.customer_last_name + "(" + customer.customer_email + ")");
				that.customerOrder.setSelectedOrder(order);
				that.orderGarments.setSelectedGarment(garment);
				posChangePage('#errorAdd');
			};

			this.addImage = function(){
				that.isImage = true;
				that.mediaUpload.choosePhoto();
			};

			this.addVideo = function(){
				that.isImage = false;
				that.mediaUpload.chooseVideo();
			};

			this.mediaCallback = function(data){
				if(that.isImage){
					that.errorImagesAdd.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
				}
				else{
					that.errorVideosAdd.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
				}
			};

			manageMediaTags = function(page, submit,data){
				console.log(data);
				orderItem.MediaTagManagerVM.current(data);
				orderItem.MediaTagManagerVM.page(page);
				orderItem.MediaTagManagerVM.submit(submit);
				orderItem.MediaTagManagerVM.endpoint('error_report_endpoint/update_media_tags_endpoint');
				console.log(orderItem.MediaTagManagerVM.current());
				posChangePage('#mediaTagController');
			};

			this.dialog = function(mediaData,evt, imageIndex, id){
				this.mediaData = mediaData;
				this.evt = evt;
				this.imageIndex = imageIndex;
				if(id >= 0){
					this.idCustomImage = id;
				}
				else{
					this.idCustomImage = -1;
				}
				$("textarea.title").val('');
				var tag = this.mediaData.buildTagPicture('', this.evt, this.imageIndex);

				if(this.idCustomImage >= 0){
					$("#mapper" + this.idCustomImage).css("left", tag.left+ '%').css("top",tag.top + '%').css("width","35px").css("height","35px") .show();
				}
				else{
					$('div.mapper').css("left", tag.left+ '%').css("top",tag.top + '%').css("width","35px").css("height","35px") .show();
				}
				orderItem.openDialog();
			};
			this.changeStatus = function(){
				if(that.newStatus() && that.newStatus().status_text == that.error_status()){
					customAlert("Can not change the status of an error to the same status.");
				}
				else{
					if(that.QV()){
						that.showQC(false);
					}
					if(that.addDeductionList()){
						that.showDeduction(false);
					}
					data = {
						error_id: that.error_id(),
						status_id: that.newStatus() ? that.newStatus().status_id : null,
						qc: that.QV(),
						error_deduction: that.addDeductionList()
					};
					DataRequest(false, 'error_report_endpoint/change_error_status', data, function(){customAlert("Status Changed");that.QV(false);that.addDeductionList(false);if(that.newStatus()){that.error_status(that.newStatus().status_text);}that.newStatus(null);that.isChangingStatus(false);}, function(){customAlert("An error happened! Please try again later or contact the admins about the error.");}, function(){customAlert("Status Changed");that.QV(false);that.addDeductionList(false);if(that.newStatus()){that.error_status(that.newStatus().status_text);}that.newStatus(null);that.isChangingStatus(false);});
				}
			};
			this.closeDialog = function(){
				$("div.form_panel").hide();
				$('div.mapper').hide();
				$("textarea.title").val('');
			};

			this.addTag = function(text){
				$("div.form_panel").hide();
				$('div.mapper').hide();
				var text = $("textarea.title").val();
				this.mediaData.addTag(this.mediaData.buildTagPicture(text, this.evt, this.imageIndex),this.imageIndex);
			};

			this.addGarmentMedia = function(path, mediaType){
				if(mediaType == 1){
					var ind = that.errorImagesAdd.getMediaIndex(path);
					if(ind < 0){
						that.errorImagesAdd.addMedia(path, path, false);
					}
				}
				else{
					var ind = that.errorVideosAdd.getMediaIndex(path);
					if(ind < 0){
						that.errorVideosAdd.addMedia(path, path, false);
					}
				}
			};

			//variables that are harded
			this.status = ko.observableArray([
				{status_id: '1', status_text:'Open'},
				{status_id: '2', status_text:'Awaiting Reply'},
				{status_id: '3', status_text:'Holding for Later'},
				{status_id: '4', status_text:'Resolved'},
				{status_id: '5', status_text:'Closed'}
			]);

			this.priority = ko.observableArray([
				{priority_id: '1', priority_text: 'Lowest'},
				{priority_id: '2', priority_text: 'Low'},
				{priority_id: '3', priority_text: 'Normal'},
				{priority_id: '4', priority_text: 'High'},
				{priority_id: '5', priority_text: 'Urgent'}
			]);

			this.statusTextToID = function(value){
				for(x in this.status()){
					if(value == this.status()[x].status_text)
						return this.status()[x].status_id;
				}
				return '';

			};

			this.statusIDtoText = function(value){
				for(x in this.status()){
					if(value == this.status()[x].status_id)
						return this.status()[x].status_text;
				}
				return '';
			};

			this.priorityTextToID = function(value){
				for(x in this.priority()){
					if(value == this.priority()[x].priority_text)
						return this.priority()[x].priority_id;
				}
				return '';
			};

			this.priorityIDToText = function(value){
				for(x in this.priority()){
					if(value == this.priority()[x].priority_id)
						return this.priority()[x].priority_text;
				}
				return '';
			};

			//function that will deal with all necessary operations to fill the array media
			this.fillMedia = function(array){
				for(x in array){
					var tag = ko.observableArray();
					array[x].tag = tag;
					try{
						try{
							var temp = JSON.parse(array[x].media_tags);
						}catch(e){
							var temp = array[x].media_tags;
						}
						for(y in temp){
							array[x].tag.push(temp[y]);
						}
					}catch(err) {
						console.log(err);
					}
					if(array[x].media_is_sync == "1"){
						array[x].media_path = array[x].media_path;
					}
					else{
						array[x].media_path = BUrl + array[x].media_path;
					}
				}
				for(ind in array){
					that.media.push(array[ind]);
				}

			};

			//function that will deal with all necessary operations to fill the array replies
			this.fillReplies = function(array){
				for(var x in array){
					var medias = [];
					var ind = 0;
					while(ind < that.media().length){
						if(that.media()[ind].error_reply_id == array[x].reply_id){
							medias.push(that.media()[ind]);
							that.media.remove(that.media()[ind]);
						}
						else{
							ind++;
						}
					}
					array[x].medias = medias;

					that.replies.push(array[x]);
				}
				var temp = that.replies();
				for(var i in temp){
					var tag = ko.observableArray();
					temp[i].tag = tag;
					try{
						try{
							var tags = JSON.parse(array[x].media_tags);
						}catch(e){
							var tags = array[x].media_tags;
						}
						for(y in tags){
							temp[i].tag.push(tags[y]);
						}
					}catch(err) {
						console.log(err);
					}
				}
			};

			//clean all the values from all variables
			this.cleanAll = function(){
				var that = this;
				that.error_id(null);
				that.error_assigned_to(null);
				that.error_cat_id(null);
				that.error_date(null);
				that.error_desc(null);
				that.error_fix_date(null);
				that.error_link_customers(null);
				that.error_link_order(null);
				that.error_link_users.removeAll();
				that.error_linked_garment(null);
				that.error_media_ids(null);
				that.error_media_video(null);
				that.error_priority(null);
				that.error_reason(null);
				that.error_report_by(null);
				that.error_status(null);
				that.error_title(null);
				that.garment_text(null);
				that.media.removeAll();
				that.replies.removeAll();
				that.garment_text(null);
				that.order_text(null);
				that.newStatus(null);
				that.addDeductionList(false);
				that.QV(false);
				that.isChangingStatus(false);
				that.garmentMedia = new GarmentMedia(null);
				that.orderGarments = new OrderGarmentAutoComplete(this.DsRegistry);
				that.customerOrder = new CustomerOrdersAutoComplete(this.DsRegistry);
				that.customers = new CustomerAutoCompleteVM(this.DsRegistry, this.customerSelectVM);
				that.errorCatgory = new ErrorCategory(this.DsRegistry);
				that.users = new UsersAutoComplete(this.DsRegistry);
				that.mediaUpload = orderItem.MediaUploadVM;
				//that.ErrorReplyVM = new ErrorReplyVM(this.DsRegistry);
				//that.mediaUpload = new MediaUploadVM(null);
				that.mediaUpload.clear();
				that.ErrorReplyVM.setSelectedError(this.selectedError);
				that.ErrorReplyVM.message('');
				that.errorImages = new MediaType();
				that.errorVideos = new MediaType();
				that.errorImages.setCustomPreview(that.previewCustomImage);
				that.errorImages.setCustomPreview(that.previewCustomVideo);
				that.previewCustomImageAdd.removeAll();
				that.previewCustomVideoAdd.removeAll();
				that.errorImagesAdd = new MediaType(2, null, true, '',true);
				that.errorVideosAdd = new MediaType(1, null, true, '',true);
				that.errorImagesAdd.setCustomPreview(that.previewCustomImageAdd);
				that.errorVideosAdd.setCustomPreview(that.previewCustomVideoAdd);
				that.addDeductionList(false);
				that.deductionAmount('');
			};
		},

		//set the error that the class will work with
		setSelectedError: function(errorID){
			this.selectedError = errorID;
		},

		//load all information necessary using the selected error
		loadInfo : function(){
			var that = this;
			this.burl = BUrl;
			//this.cleanAll();
			this.loadErrorsInfo();
			//collect the user data and prepare ir to send.
			data = {
				error_id: that.selectedError
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_specific_error',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//alert(dataS);
					var temp = JSON.parse(dataS);
					console.log(temp);
					//that.ErrorReplyVM = new ErrorReplyVM(that.DsRegistry);
					that.ErrorReplyVM.message('');
					//that.mediaUpload = new MediaUploadVM(null);
					that.mediaUpload.clear();
					console.log(temp);
					that.error_id(temp.error.error_id);
					that.error_assigned_to(temp.error.error_assigned_to);
					that.error_cat_id(temp.error.error_cat_id);
					that.errorCatgory.setSelectedCategory(temp.error.error_cat_id);
					that.error_date(temp.error.error_date);
					that.error_desc(temp.error.error_desc);
					that.error_fix_date(temp.error.error_fix_date);
					that.error_link_customers(temp.error.error_link_customers);
					that.error_link_order(temp.error.error_link_order);
					that.error_link_users(temp.error.error_link_users);
					that.error_linked_garment(temp.error.error_linked_garment);
					that.error_media_ids(temp.error.error_media_ids);
					that.error_media_video(temp.error.error_media_video);
					that.error_priority(that.priorityIDToText(temp.error.error_priority));
					that.error_reason(temp.error.error_reason);
					that.order_text(temp.error.error_link_order + ' ' + temp.error.ordered_date);
					that.garment_text(temp.error.garment_type + ' ' + temp.error.linked_garment_fabric_code);
					that.error_report_by(temp.error.error_report_by);
					that.error_status(that.statusIDtoText(temp.error.error_status));
					that.error_title(temp.error.error_title);
					that.fillMedia(temp.media);
					that.fillReplies(temp.errors_replies);
					if(temp.error.deduction){
						that.showDeduction(false);
					}
					else{
						that.showDeduction(true);
					}
					if(temp.error.error_qc){
						that.showQC(false);
					}
					else{
						that.showQC(true);
					}


					that.ErrorReplyVM.setSelectedError(that.selectedError);
					if(that.error_link_customers()){
						that.customers.selectCustomer(that.error_link_customers());
						that.customerOrder.setSelectedOrder(that.error_link_order());
						console.log(that.error_linked_garment());
						if(that.error_linked_garment() > 0){
							that.orderGarments.setSelectedGarment(that.error_linked_garment());
							console.log(that.orderGarments.selectedGarment());
						}

						that.errorCatgory.setSelectedCategory(that.error_cat_id());
					}
					if(that.error_link_users())
						that.users.selectUsers(that.error_link_users());

				},
				async: false
			});

		},

		uploadMedia: function(errorID, replyID){
			var that = this;
			data = {
				error_id: errorID,
				device_id: typeof device != "undefined"? device.uuid: "",
				reply_id: replyID,
				images: that.errorImagesAdd.getSyncInfo(),
				videos: that.errorVideosAdd.getSyncInfo()
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			$.ajax({
				type: 'POST',
				url: BUrl + 'error_report_endpoint/upload_media_error2',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					console.log(dataS);
				},
				async: false
			});
		},

		addReply: function(){
			var that = this;
			if(this.mediaUpload.medias().length > 0 || this.ErrorReplyVM.message()){
				this.burl = BUrl;
				var reply_id = 0;
				var server = "";
				reply_id = this.ErrorReplyVM.addReply();
				server = BUrl + "error_report_endpoint/upload_media_reply";
				reply_id = reply_id.replace(/[^0-9]/,'');
				//this.mediaUpload.uploadAllMedias(server , {error_id: this.error_id(), reply_id: reply_id});
				that.uploadMedia(that.error_id(), reply_id);
				customAlert('Reply added!');
				this.cleanAll();
				this.loadInfo();
			}else{
				customAlert('Type a message or upload a picture to reply');
			}

		},

		loadErrorsInfo: function(){
			this.errorCatgory.loadInfo();
			this.users.loadInfo();
		},

		//this function will initialize everything needed to the add and edit error action
		prepareToInsert: function(){
			//this.cleanAll();
			this.customerOrder.setOrderGarment(this.orderGarments);
            this.customers.setCustomerOrder(this.customerOrder);
			this.orderGarments.setGarmentMedia(this.garmentMedia);

			this.loadErrorsInfo();
		},

		//cancel the function add new error and return to error list
		cancelAdd : function(){
			const confirmAction = () => {
				posChangePage('#errorList');
			};
			pos_warning({msg:'Do you want to go back? All data will be lost.', confirmAction});
		},



		//add a new error into the database
		addError : function(){
			var that = this;
			this.burl = BUrl;

			//that.mediaUpload.uploadMedias(that.burl + 'error_report_endpoint/upload_media_error', 0);

			//collect the user data and prepare ir to send.
			data = {
				error_cat_id : that.errorCatgory.categorieID,
				error_desc : that.error_desc(),
				error_link_customers : that.customers.selectedCustomer() ? that.customers.selectedCustomer().server_id: null,
				error_link_order : that.customerOrder.selectedOrder() ? that.customerOrder.selectedOrder().order_id: null,
				error_link_users : that.users.getUsers(),
				error_linked_garment : that.orderGarments.getGarments(),
				error_media_ids : [],
				error_media_video : "",
				error_priority : that.priorityTextToID(that.error_priority()),
				error_reason : that.error_reason(),
				error_report_by : authCtrl.userInfo.user_id,
				error_title : that.error_title(),
				error_deduction : that.addDeductionList() ? "deduction": null,
				error_deduction_amount : that.deductionAmount()
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			console.log(data);
			if(!that.error_desc()){
				alert('Write the error description.');
			}
			else if(!that.error_title()){
				alert('Give the error a title.');
			}
			else if(that.addDeductionList() && (that.deductionAmount() <= 0 || that.deductionAmount() == '')){
				alert('Deduction value invalid!');
			}
			else{
				$.ajax({
					type: 'POST',
					url: that.burl + 'error_report_endpoint/new_error',
					data: {
						data: datapost,
						salesman: salesmanpost,
						timestamp: timestamp
					},
					success: function(dataS) {
						var id = dataS;
						id = id.replace(/[^0-9]/,'');
						//that.mediaUpload.uploadAllMedias(that.burl + 'error_report_endpoint/upload_media_error', {error_id : id});
						that.uploadMedia(id);
					},
					async: false
				});

				posChangePage('#errorList');
			}
			/**/
		},

	});

	ErrorCategory = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.burl = BUrl;
			this.categories = ko.observableArray();
			this.optionsSelect1 = ko.observableArray();
			this.categorie1 = ko.observable();
			this.optionsSelect2 = ko.observableArray();
			this.categorie2 = ko.observable();
			this.optionsSelect3 = ko.observableArray();
			this.categorie3 = ko.observable();
			this.optionsSelect4 = ko.observableArray();
			this.categorie4 = ko.observable();
			this.willCat2Appear = ko.observable(false);
			this.willCat3Appear = ko.observable(false);
			this.willCat4Appear = ko.observable(false);
			this.categorieID = 0;
			this.categorieName = ko.observable('');


			this.categorie1.subscribe(function(data){
				if(data){
					that.optionsSelect2.removeAll();
					that.getChilds(data, that.optionsSelect2);
					if(that.optionsSelect2().length > 0){
						that.willCat2Appear(true);
					}
					else {
						that.willCat2Appear(false);
					}
				} else{
					that.categorie2(null);
					that.willCat2Appear(false);
				}
				that.updateCatID();
			});

			this.categorie2.subscribe(function(data){
				if(data){
					that.optionsSelect3.removeAll();
					that.getChilds(data, that.optionsSelect3);
					if(that.optionsSelect3().length > 0){
						that.willCat3Appear(true);
					}
					else {
						that.willCat3Appear(false);
					}
				}
				else{
					that.categorie3(null);
					that.willCat3Appear(false);
				}
				that.updateCatID();
			});

			this.categorie3.subscribe(function(data){
				if(data){
					that.optionsSelect4.removeAll();
					that.getChilds(data, that.optionsSelect4);
					if(that.optionsSelect4().length > 0){
						that.willCat4Appear(true);
					}
					else {
						that.willCat4Appear(false);
					}
				}else {
					that.categorie4(null);
					that.willCat4Appear(false);
				}
				that.updateCatID();
			});

			this.categorie4.subscribe(function(data){
				that.updateCatID();
			});

			this.clear = function(){
				that.categorie1(null);
			};
		},


		getChilds: function(cat, observableArray){
			for(ind in this.categories()){
				if(this.categories()[ind].parent_category == cat.category_id){
					observableArray.push(this.categories()[ind]);
				}
			}
		},

		getParent: function(cat){
			if(cat.parent_category == '1' )
				return false;
			for(ind in this.categories()){
				if(this.categories()[ind].category_id == cat.parent_category ){
					return this.categories()[ind];
				}
			}
			return false;
		},

		getNoParentsOptions: function(observableArray){
			for(i = 0; i < this.categories().length; i++){
				if(this.categories()[i].parent_category == '1'){
					observableArray.push(this.categories()[i]);
				}
			}
		},

		setSelectedCategory: function(catID){
			for(ind in this.categories()){
				if(this.categories()[ind].category_id == catID){
					var parent = this.categories()[ind];
					var nivel = 0;
					var cats = [];
					do{
						cats.unshift(parent);
						parent = this.getParent(parent);
						nivel++;
					}while(parent);
					this.categorie1(cats.shift());
					if(nivel > 1)
						this.categorie2(cats.shift());
					if(nivel > 2)
						this.categorie3(cats.shift());
					if(nivel > 3)
						this.categorie4(cats.shift());
					break;
				}
			}
		},

		updateCatID: function(){
			if(this.categorie4()){
				this.categorieID = this.categorie4().category_id;
				this.categorieName(this.categorie4().category_name);
			}else if(this.categorie3()){
				this.categorieID = this.categorie3().category_id;
				this.categorieName(this.categorie3().category_name);
			}else if(this.categorie2()){
				this.categorieID = this.categorie2().category_id;
				this.categorieName(this.categorie2().category_name);
			}else if(this.categorie1()){
				this.categorieID = this.categorie1().category_id;
				this.categorieName(this.categorie1().category_name);
			}else{
				this.categorieID = 0;
			}

		},

		loadInfo: function(){
			var that = this;
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			salesmanpost = JSON.stringify(salesman);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_all_error_categories',
				data: {
					salesman: salesmanpost
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//store all the users returned into the
					var temp = JSON.parse(dataS);
					that.categories.removeAll();
					for(x in temp){
						that.categories.push(temp[x]);
					}
				},
				async: false
			});
			this.optionsSelect1.removeAll();
			this.getNoParentsOptions(this.optionsSelect1);
		}

	});

/*****************************************************************************************************************************************************************************************************
********                                                                      Auto-complete section.                                                                                   ***************
********  This section contains all the auto-complete functions used by the error report feature. All of them gets its data via an post request and it allows the user to select more  ***************
********  than one value at the same time.                                                                                                                                             ***************
*****************************************************************************************************************************************************************************************************/
//perhaps it can be done in a more general way, but the solution was not designed this way due to lack of time

	UsersAutoComplete = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.users = ko.observableArray();
			this.usersAutocompleteList = ko.observableArray();	// subset of customers
			this.userAutocomplete = ko.observable();		// text input
			this.selectedUser = ko.observable({});
			this.selectedUsers = ko.observableArray();	// subset of customers

			this.burl = BUrl;

			this.userAutocomplete.subscribe(function(data){
				that.usersAutocompleteList.removeAll();
				if(data.length > 2){
					for(u in that.users()){
						var ufull = that.users()[u].first_name + ' ' + that.users()[u].last_name + ' ' + that.users()[u].username;
						ufull = ufull.toLowerCase();
						if (ufull.indexOf(data.toLowerCase()) != -1) {
							that.usersAutocompleteList.push(that.users()[u]);
						}
					}
				}
			});

			this.selectedUser.subscribe(function(data){
				that.userAutocomplete('');
				that.usersAutocompleteList.removeAll();
				that.selectedUsers.push(data);
			});

			this.removeUser = function(data){
				that.selectedUsers.remove(data);
			};

		},

		selectUsers : function(usersInfo){
			for(x in usersInfo){
				for(u in this.users()){
					if(this.users()[u].user_id == usersInfo[x].user_id){
						this.selectedUser(this.users()[u]);
					}
				}
			}
		},

		loadInfo: function(){
			var that = this;

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			salesmanpost = JSON.stringify(salesman);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_all_users',
				data: {
					salesman: salesmanpost
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//store all the users returned into the
					var temp = JSON.parse(dataS);
					that.users.removeAll();
					for(x in temp){
						that.users.push(temp[x]);
					}
				},
				async: false
			});
		},

		getUsers: function(){
			var users = '';
			var add = '';
			for(var y in this.selectedUsers()){
				if(users != ''){
					add= ',';
				}
				users = users + add + this.selectedUsers()[y].user_id;
			}
			return users;
		}

	});

	CustomerAutoCompleteVM = SimpleControl.extend({
		init: function(DsRegistry, csVM) {
			this._super( DsRegistry );
			var that = this;
			this.custOrder = null;
			this.customerSelectVM = csVM;
			this.customerAutocompleteList = ko.observableArray();	// subset of customers
			this.customerAutocomplete = ko.observable();		// text input
			this.selectedCustomer = ko.observable({});

			this.customerAutocomplete.subscribe(function(data){
				that.customerAutocompleteList.removeAll();
				if(data.length > 2){
					for(ind in that.customerSelectVM.customers()){
						var cfull = that.customerSelectVM.customers()[ind].customer_last_name + ' ' +  that.customerSelectVM.customers()[ind].customer_first_name + ' ' + that.customerSelectVM.customers()[ind].customer_email;
						cfull = cfull.toLowerCase();
						if (cfull.indexOf(data.toLowerCase()) != -1) {
							that.customerAutocompleteList.push(that.customerSelectVM.customers()[ind]);
						}
					}
				}

			});

			this.selectedCustomer.subscribe(function(data){
				console.log(data);
				that.customerAutocompleteList.removeAll();
				that.customerAutocomplete('');
				if(that.custOrder){
					that.custOrder.setCustomerID(data.server_id);

				}
			});

			this.removeCustomer = function(data){
				that.selectedCustomer({});
			};

		},

		setCustomerOrder : function(CustomerOrder){
			this.custOrder = CustomerOrder;
		},

		setCustomerSelectVM : function(CustomerSelectVM){
			this.customerSelectVM = CustomerSelectVM;
		},

		selectCustomer : function(customerInfo){
			for(ind in this.customerSelectVM.customers()){
				var cfull = this.customerSelectVM.customers()[ind].customer_first_name + ' ' +  this.customerSelectVM.customers()[ind].customer_last_name + '(' + this.customerSelectVM.customers()[ind].customer_email + ')';
				cfull = cfull.toLowerCase();
				if (cfull.indexOf(customerInfo.toLowerCase()) != -1) {
					this.selectedCustomer(this.customerSelectVM.customers()[ind]);
				}
			}
		}

	});

	CustomerOrdersAutoComplete = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.orGarment = null;
			this.orders = ko.observableArray();
			this.ordersAutocompleteList = ko.observableArray();	// subset of customers
			this.ordersAutocomplete = ko.observable();		// text input
			this.selectedOrder = ko.observable({});

			this.ordersAutocomplete.subscribe(function(data){
				that.ordersAutocompleteList.removeAll();
				if(data.length > 0){
					for(ind in that.orders()){
						var ufull = that.orders()[ind].order_id + ' ' + that.orders()[ind].ordered_date;
						if (ufull.indexOf(data.toLowerCase()) != -1) {
							that.ordersAutocompleteList.push(that.orders()[ind]);
						}
					}
				}
			});

			this.selectedOrder.subscribe(function(data){
				if(data){
					that.ordersAutocompleteList.removeAll();
					if(that.orGarment){
						that.orGarment.setOrderID(data.order_id);
					}
				}
				else{
					if(that.orGarment)
						that.orGarment.setOrderID(null);
				}
			});

			this.removeOrder = function(data){
				that.selectedOrder(null);
			};

		},

		setOrderGarment: function(OrderGarment){
			this.orGarment = OrderGarment;
		},

		setCustomerID: function(custID){
			this.ordersAutocompleteList.removeAll();
			this.orders.removeAll();
			this.removeOrder();
			if(custID){
				this.getAllOrders(custID);
			}
		},

		getAllOrders: function(custID){
			var that = this;
			this.burl = BUrl;
			data = {
				customer_id: custID
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_customer_orders',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//var temp = JSON.parse(dataS);
					var temp = JSON.parse(dataS);
					console.log(temp);
					that.ordersAutocompleteList.removeAll();
					that.orders.removeAll();
					var garmentsString = '';
					var add = '';
					for(var x in temp.orders){
						garmentsString = '';
						add = '';
						for(var i in temp.orders[x].garments){
							if(garmentsString.length > 0){
								add = ', ';
							}
							garmentsString = garmentsString + add + temp.orders[x].garments[i].garment_name;
						}
						temp.orders[x].order_string = temp.orders[x].order_id + " - " + temp.orders[x].ordered_date + ' - ' + garmentsString ;
						that.orders.push(temp.orders[x]);
					}
				},
				async: false
			});

		},

		setSelectedOrder: function(orderID){
			for(ind in this.orders()){
				if(this.orders()[ind].order_id == orderID){
					this.selectedOrder(this.orders()[ind]);
				}
			}
		},


	});

	OrderGarmentAutoComplete = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.garmentMedia = null;
			this.fittingsList = null;
			this.garments = ko.observableArray();
			this.garmentsAutocompleteList = ko.observableArray();	// subset of customers
			this.garmentsAutocomplete = ko.observable();		// text input
			this.selectedGarment = ko.observable();
			this.selectedGarments = ko.observableArray();

			this.garmentsAutocomplete.subscribe(function(data){
				that.garmentsAutocompleteList.removeAll();
				if(data.length > 0){
					for(ind in that.garments()){
						var ufull = that.garments()[ind].garment_name + ' ' + that.garments()[ind].garment_fabric;
						ufull = ufull.toLowerCase();
						if (ufull.indexOf(data.toLowerCase()) != -1 && that.selectedGarments().indexOf(that.garments()[ind]) < 0) {
							that.garmentsAutocompleteList.push(that.garments()[ind]);
						}
					}
				}
			});



			this.selectedGarment.subscribe(function(data){
				if(data){
					console.log(data);
					that.garmentsAutocompleteList.removeAll();
					that.selectedGarments.push(data);
					if(that.garmentMedia){
						that.garmentMedia.setGarmentInfo({garment_id: data.garment_id, garment_type: data.garment_type });
					}
				}
				else{
					if(that.garmentMedia){
						that.garmentMedia.setGarmentInfo(null);
					}
				}
			});

			this.removeGarments = function(data){
				that.selectedGarments.remove(data);
			};

		},

		setGarmentMedia: function(GarmentMedia){
			this.garmentMedia = GarmentMedia;
		},

		setFittingList: function(FittingsList){
			this.fittingsList = FittingsList;
		},

		setOrderID: function(orderID){
			this.garmentsAutocompleteList.removeAll();
			this.garments.removeAll();
			this.selectedGarment("");
			if(orderID){
				this.getAllGarments(orderID);
			}
		},

		getAllGarments: function(orderID){
			var that = this;
			this.burl = BUrl;
			data = {
				order_id: orderID
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_order_garments',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					var temp = JSON.parse(dataS);
					console.log(temp);
					that.garmentsAutocompleteList.removeAll();
					that.garments.removeAll();
					for(var x in temp){
						if(temp[x].garment_type == '6'){
							var jacket = {};
							var pants = {};
							jacket.garment_id = temp[x].garment_id;
							jacket.garment_name =  "Jacket/S";
							jacket.garment_type = '4';
							jacket.garment_fabric = temp[x].garment_fabric;
							jacket.garment_text = jacket.garment_name + " "+ temp[x].garment_fabric + " " + temp[x].barcodes[4].barcode_id;
							pants.garment_id = temp[x].garment_id;
							pants.garment_name =  "Pant/S";
							pants.garment_type = '1';
							pants.garment_fabric = temp[x].garment_fabric;
							pants.garment_text = pants.garment_name + " "+ temp[x].garment_fabric + " " + temp[x].barcodes[1].barcode_id;
							that.garments.push(jacket);
							that.garments.push(pants);
						}
						else{
							temp[x].garment_text = (temp[x].garment_name + " " + temp[x].garment_fabric + " " + temp[x].barcode_id);
							that.garments.push(temp[x]);
						}


					}
				},
				async: false
			});
		},

		getGarments: function(){
			var garments = '';
			var add = '';
			for(x in this.selectedGarments()){
				if(garments != '' && add == '')
					add = ','
					garments = garments + add + this.selectedGarments()[x].garment_id;
			}
			return garments;
		},

		setSelectedGarment: function(garment){
			for(ind in this.garments()){
				if(this.garments()[ind].garment_id == garment.garment_id && this.garments()[ind].garment_type == garment.garment_type){
					this.selectedGarment(this.garments()[ind]);
				}
			}
		}
	});

	GarmentMedia = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.images = ko.observableArray([]);
			this.videos = ko.observableArray([]);
			this.hasMedia = ko.observable(false);
			this.selectedMediaType = ko.observable();
			this.selectedMediaGroup = ko.observableArray([]);
		},

		setGarmentInfo: function(garmentInfo){

			this.images([]);
			this.videos([]);
			this.hasMedia(false);
			this.clearSelected();
			if(garmentInfo){
				this.getAllMedias(garmentInfo);
			}
		},

		getMediaselected: function(){
			var media = {};
			media.images = [];
			media.videos = [];

			if(this.selectedMediaType() == 1){
				for(var j in this.selectedMediaGroup()){
					if(this.selectedMediaGroup()[j].checked()){
						media.images.push(this.selectedMediaGroup()[j].path);
					}
					this.selectedMediaGroup()[j].checked(false);
				}
			}

			else{
				for(var j in this.selectedMediaGroup()){
					if(this.selectedMediaGroup()[j].checked()){
						media.videos.push(this.selectedMediaGroup()[j].path);
					}
					this.selectedMediaGroup()[j].checked(false);
				}
			}
			return media;
		},

		clearSelected : function(){
			this.selectedMediaGroup.removeAll();
			this.selectedMediaType(null);
		},

		selectMediaGroup: function(mediaType, index){
			this.selectedMediaGroup.removeAll();
			this.selectedMediaType(mediaType);
			if(mediaType == 1){
				var media = this.images()[index].images;
			}
			else{
				var media = this.videos()[index].videos;
			}
			for(var i in media){
				media[i].checked = ko.observable(false);
				this.selectedMediaGroup.push(media[i]);
			}
		},

		getAllMedias: function(garmentInfo){
			var that = this;
			this.burl = BUrl;
			data = {
				garment_info: garmentInfo
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_garment_media',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//var temp = JSON.parse(dataS);
					var temp = JSON.parse(dataS);
					console.log(temp);
					that.images([]);
					that.videos([]);
					that.hasMedia(false);
					for(var vid in temp.videos){
						that.videos.push(temp.videos[vid]);
					}
					for(var img in temp.images){
						that.images.push(temp.images[img]);
					}
					that.hasMedia(temp.videos.length > 0 || temp.images.length > 0);
				},
				async: false
			});

		}

	});

	GarmentFittings = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.fittingMedia = null;
			this.alterationMedia = null
			this.fittings = ko.observableArray();
			this.selectedFitting = ko.observable({});

		},

		setFittingMedia: function(FittingMedia){
			this.fittingMedia = FittingMedia;
		},

		setAlterationMedia:  function(AlterationMedia){
			this.alterationMedia = AlterationMedia;
		},

		setGarment: function(garmentInfo){
			this.getAllOrders(garmentInfo);
		},

		getAllOrders: function(garmentInfo){
			var that = this;
			this.burl = BUrl;
			data = {
				garment_info: garmentInfo
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_garment_fittings',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//var temp = JSON.parse(dataS);
					var temp = JSON.parse(dataS);
					that.fittings.removeAll();
					for(x in temp.fittings){
						that.fittings.push(temp.fittings[x]);
					}
				},
				async: false
			});

		},

	});

	FittingMedia = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.images = ko.observableArray([]);
			this.videos = ko.observableArray([]);
			this.hasMedia = ko.observable(false);
		},

		setFittingInfo: function(fittingInfo){
			this.getAllMedias(fittingInfo);
		},

		getAllMedias: function(fittingInfo){
			var that = this;
			this.burl = BUrl;
			data = {
				fittingInfo : fittingInfo
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/get_fitting_media',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					//if it succeed, it stores the values.
					//var temp = JSON.parse(dataS);
					var temp = JSON.parse(dataS);
					that.images([]);
					that.videos([]);
					that.hasMedia(false);
					for(var vid in temp.videos){
						that.videos.push(temp.videos[vid]);
					}
					for(var img in temp.images){
						that.images.push(temp.images[img]);
					}
					that.hasMedia(temp.videos.length > 0 || temp.images.length > 0);
				},
				async: false
			});

		}
	});

/*****************************************************************************************************************************************************************************************************
********                                                                  End of auto-complete section.                                                                                ***************
*****************************************************************************************************************************************************************************************************/

/**
** This class handles the creation of replies for an error report
** Main features:
** addReply - method that will submit everything of an reply(message and media).
**
**/

	ErrorReplyVM = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.selectedError = 0;
			this.message = ko.observable('');
		},

		cancelAdd: function(){
			posChangePage('#errorReport');
		},

		addReply: function(){
			var that = this;
			this.id = 0;
			this.burl = BUrl;
			//collect the user data and prepare ir to send.
			data = {
				error_id: that.selectedError,
				message: this.message()
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			//do a post request to store the message
			$.ajax({
				type: 'POST',
				url: that.burl + 'error_report_endpoint/new_reply_for_error',
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp,
				},
				success: function(dataS) {
					that.id = dataS;
					that.addDeductionList(false);
					that.QV(false);
				},
				async: false
			});
			return that.id;

		},

		//set the error that the class will work with
		setSelectedError: function(errorID){
			this.selectedError = errorID;
		}

	});

});