define(['jquery', 'knockout', 'base'], function($, ko) {

    /**
     * main class to handle outside customer fitting
     * There will be some internal classes to make easier to understand everything
     */
    OutsideCustomerFitting = class{
        constructor(){
            // holds each step of fitting
            this.mediaImages = new this.ImageStep();
            this.videoStep = new this.VideoStep();

            this.availableSteps = ko.observableArray([
                {name: "UPLOAD IMAGE",  id: "upload_image",  isComplete: this.getStepRule("upload_image"),  classIcon: 'fimage'  },
                {name: "UPLOAD VIDEO",  id: "upload_video",  isComplete: this.getStepRule("upload_video"),  classIcon: 'fvideo'  },
                {name: "ADD NOTES",     id: "add_notes",     isComplete: this.getStepRule("add_notes"),     classIcon: 'fnote'   },
                {name: "ASSIGN TAILOR", id: "assign_tailor", isComplete: this.getStepRule("assign_tailor"), classIcon: 'ftailor' },
            ]);

            this.selectedStep = ko.observable(this.availableSteps()[0]);

            this.garmentAsCompletionData = ko.observable(false);

        }

        /**
         * Changes the current step
         * @param {string} step_id
         */
        selectStep(step_id){
            this.selectedStep(this.availableSteps().find(step => step.id === step_id));
        }

        /**
         * For each step, return a ko.compute that says if the step is complete or not
         * @param {string} step_id
         */
        getStepRule(step_id){
            return ko.computed(() => {
                if (step_id === 'upload_image'){
                    // front / back / side image
                    if (
                        this.mediaImages.frontImage().src() != "" &&
                        this.mediaImages.backImage().src() != "" &&
                        this.mediaImages.sideImage().src() != ""
                    ){
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else if(step_id === 'upload_video'){
                    if (this.videoStep.videos().length > 0){
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else if(step_id === 'add_notes'){
                    // all images must have notes
                    for (const image of this.mediaImages.allImagesInArray()) {
                        if (image.tags().length === 0){
                            return false;
                        }
                    }

                    return true;
                }

                return false;
            });
        }

        ImageStep = class {
            constructor() {
                // enum to allowed image types
                this.imageTypes = Object.freeze({
                    FRONT: "front",
                    BACK: "back",
                    SIDE: "side",
                    ADDITIONAL: "additional",
                });

                this.frontImage       = ko.observable(new this.Image( this.imageTypes.FRONT));
                this.backImage        = ko.observable(new this.Image( this.imageTypes.BACK));
                this.sideImage        = ko.observable(new this.Image( this.imageTypes.SIDE));
                this.additionalImages = ko.observableArray([]);

                // a quicker way to get all images once
                this.allImagesInArray = ko.computed(() => {
                    return [
                        this.frontImage(),
                        this.backImage(),
                        this.sideImage(),
                        ...this.additionalImages()
                    ]
                });

            }

            removeImage(imageType, index=0){
                switch (imageType) {
                    case this.imageTypes.FRONT:
                        this.frontImage().src('');
                        break;
                    case this.imageTypes.BACK:
                        this.backImage().src('');
                        break;
                    case this.imageTypes.SIDE:
                        this.sideImage().src('');
                        break;
                    case this.imageTypes.ADDITIONAL:
                        // remove that index position
                        this.additionalImages.splice(index, 1);
                        break;
                }
            }

            addImage(imageType, imageUrl){
                switch (imageType) {
                    case this.imageTypes.FRONT:
                        this.frontImage().src(imageUrl);
                        break;
                    case this.imageTypes.BACK:
                        this.backImage().src(imageUrl);
                        break;
                    case this.imageTypes.SIDE:
                        this.sideImage().src(imageUrl);
                        break;
                    case this.imageTypes.ADDITIONAL:
                        const newImage = new this.Image(this.imageTypes.ADDITIONAL);
                        newImage.src(imageUrl);

                        this.additionalImages.push(newImage);
                        break;
                }


            }

            choosePicSource(imageType) {
                const successMove = (entry) => {
                    this.addImage(imageType, entry.toURL());
                };

                const takePic = (buttonIndex) => {

                    const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

                    navigator.camera.getPicture(onSuccess, onFail,
                    {
                        quality: 10,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: source,
                        encodingType: Camera.EncodingType.JPEG,
                        mediaType: Camera.MediaType.PICTURE,
                        saveToPhotoAlbum: true,
                        correctOrientation : true,
                    });

                    const onSuccess = (imageURI) => {
                        window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
                    }

                    const onFail = (message) => {
                        pos_error({ msg:'Failed because: ' + message, title:'Error'});
                    }

                    const resolveOnSuccess = (entry) => {

                        const folderName = "ClientMediaStorage";
                        const imageName = (new Date()).getTime() + '.jpg';

                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                            //The folder is created if doesn't exist

                            fileSys.root.getDirectory(folderName,
                                { create: true, exclusive: false },
                                function (directory) {
                                    entry.moveTo(directory, imageName, successMove, resOnError);
                                },
                                resOnError);
                            },

                        resOnError);
                    };

                    const resOnError = (error) => {
                        pos_error({ msg:"Error Writing the file....", title:'Error'});
                        console.log("Failed... check console");
                        console.log(error);
                    };


                }

                navigator.notification.confirm(
                    `${imageType} Picture Source`, // message
                    function (btnIndex) {
                        takePic(btnIndex);
                    },            // callback to invoke with index of button pressed
                    '',           // title
                    ['Camera', 'Gallery']         // buttonLabels
                );

                // while developing
                if (ConfigGlobalVarsVM.env.includes('luiz')) {
                    successMove({
                        toURL: () => {
                            return ImageUtils.getRandomImage()
                        }
                    });
                }
            }

            Image = class {
                constructor(type){
                    this.type = type;
                    this.src = ko.observable("");
                    this.tags = ko.observableArray([]);
                }

                handleClickImage(evt){
                    console.log('evt', evt);

                    const width = $(evt.target).width();

                    const height = $(evt.target).height();

                    var relativeX = evt.pageX - $(evt.currentTarget).offset().left;
                    var relativeY = evt.pageY - $(evt.currentTarget).offset().top;

                    const x_percent = relativeX * 100 / width;
                    const y_percent = relativeY * 100 / height;

                    this.tags.push(new this.Tag(x_percent, y_percent));

                }

                addImageTag(posX, posY, text){
                    const newTag = {
                        posX,
                        posY,
                        text,
                    }

                    this.tags.push(newTag);
                }

                removeImageTag(tagIndex){
                    this.tags.splice(tagIndex, 1);
                }

                Tag = class{
                    constructor(posX, posY){
                        this.posX = posX;
                        this.posY = posY;
                        this.text = ko.observable('');
                    }
                }
            }

        }

        VideoStep = class {
            constructor() {
                this.videos = ko.observableArray();
            }

            uploadVideo(videoUrl){
                this.videos.push(new this.Video(videoUrl));
            }

            removeVideo(videoIndex){
                // remove that index position
                this.videos.splice(videoIndex, 1);
            }

            chooseVideoSource() {

                navigator.notification.confirm(
                    'Video Source', // message
                    function (btnIndex) {
                        takeVideo(btnIndex);
                    },            // callback to invoke with index of button pressed
                    '',           // title
                    ['Camera', 'Gallery']         // buttonLabels
                );

                const takeVideo = (buttonIndex) => {

                    if (buttonIndex == 1) // from camera
                    {
                        navigator.device.capture.captureVideo(onSuccess, onFail,
                            {
                                quality: 1,
                                saveToPhotoAlbum: true
                            });
                    }
                    else // From gallery
                    {
                        navigator.camera.getPicture(onGalleryDataSuccess, onFail, {
                            quality: 1,
                            destinationType: Camera.DestinationType.FILE_URI,
                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                            mediaType: Camera.MediaType.VIDEO,
                            correctOrientation: true
                        });
                    }

                    const onSuccess = (mediaFiles) => {

                        var i, path, len;

                        for (i = 0, len = mediaFiles.length; i < len; i += 1) {

                            console.log(mediaFiles[i].localURL);
                            console.log(mediaFiles[i].fullPath);
                            window.resolveLocalFileSystemURL(mediaFiles[i].localURL, resolveOnSuccess, resOnError);
                        }
                    }

                    const onGalleryDataSuccess = (videoURI) => {
                        window.resolveLocalFileSystemURL(videoURI, successMove, resOnError);
                    };

                    const onFail = (message) => {
                        alert('Failed because: ' + message);
                    }

                    const resolveOnSuccess = (entry) => {

                        const folderName = "ClientMediaStorage";
                        const imageName = (new Date()).getTime() + '.mov';

                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                            //The folder is created if doesn't exist

                            fileSys.root.getDirectory(folderName,
                                { create: true, exclusive: false },
                                function (directory) {
                                    entry.moveTo(directory, imageName, successMove, resOnError);
                                },
                                resOnError);
                        },
                            resOnError);
                    };

                    const successMove = (entry) => {
                        console.log('After move');
                        console.log("entry.toURL()", entry.toURL());
                        this.uploadVideo( entry.toURL())
                    };

                    const resOnError = (error) => {
                        console.error(error);
                    };

                }

                // while developing
                if (ConfigGlobalVarsVM.env.includes('luiz')) {
                    this.uploadVideo(ImageUtils.getRandomVideo());
                }
            }

            Video = class {
                constructor(url) {
                    this.url = ko.observable(url);
                    this.comments = ko.observableArray();

                    // hold comment string while commenting
                    this.tempComment = ko.observable('');
                }

                addComment(commentText){
                    this.comments.push(commentText)
                }

                removeComment(index){
                    this.comments.splice(index, 1);
                }
            }
        }

    }
});