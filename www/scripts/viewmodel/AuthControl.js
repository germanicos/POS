define(['jquery', 'knockout', 'base', 'jqm'], function($, ko) {
    AuthControl = function AuthController() {
        var self = this;
        self.clickcount = 0;
        self.userInfo    =  {};
        self.username    =  ko.observable("");
        self.password    =  ko.observable("");
        self.offlineMode =  ko.observable(false);
        self.device_id	 = 	ko.observable("");
        self.login       =  function() {
            if (!self.showSpinner()) {
				changeVisibilityDiv('loading_jp');
                //var spinner = document.getElementById('loading_jp');
				//spinner.style.display = "block";
					

				const d_id = localStorage.getItem('device_id') ? localStorage.getItem('device_id') : device.uuid;
                self.device_id( d_id );
 //customAlert("device_id: " + self.device_id());
 
                $.post(BUrl + 'client/ajax_login', {
                        username: self.username(), 
                        password: self.password(),
                        device_id: self.device_id() }, 
                        function(dataS) {
console.log("dataS:[" + dataS + "]");
						changeVisibilityDiv('loading_jp');

                        if (dataS != '0'  && dataS != '' && dataS != null && dataS != '\n' && dataS.length > 5) {
                            self.userInfo = JSON.parse(dataS);
console.log("isactive: " + self.userInfo.is_active);
                            if(self.userInfo.is_active == '1'){
	                            setTimeout(function() {

										tailorsDS = new SyncableDatasource('tailorsDS', 'tailorsDS', dsRegistry, 'tailors_endpoint');
										fitlinesJacketDS = new SyncableDatasource('fitlinesJacketDS', 'fitlinesJacketDS', dsRegistry, 'jacket_fitlines_endpoint');
										fitlinesPantsDS = new SyncableDatasource('fitlinesPantsDS', 'fitlinesPantsDS', dsRegistry, 'pants_fitlines_endpoint');

									    customersVM = new CustomerCntClass(dsRegistry);
	                            	     
	                            	    custData = new SyncableDatasource('customersDS_' + authCtrl.userInfo.user_id, 'customersDS_' + authCtrl.userInfo.user_id, dsRegistry, 'customer_endpoint');
	                           	        customersVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
	                           	         //customerOrdersVM = new OrdersClass(dsRegistry);
	                           	        orderItem  = new OrderItem(dsRegistry);
	                           	         /*
	                           	         if(localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id) == 'undefined'){
	                           	         	this.lastUpdate = '0';
	                           	         }else{                           	         	
	                           	         	this.lastUpdate = localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id);
	                           	         }
	                           	         */
                           	         	currenciesDS = new SyncableDatasource('currenciesDS', 'currenciesDS', dsRegistry, 'currencies_endpoint');
										pricerangeDS = new SyncableDatasource('pricerangeDS', 'pricerangeDS', dsRegistry, 'price_range_endpoint');
										ErrorCategoriesListVM = new ErrorCategoriesList();
										errorReportListVM = new ErrorReportListVM(dsRegistry);
										errorReportVM = new ErrorReportVM(dsRegistry, orderItem.custSelectVM);
										$('#login').val('');
										$('#password').val(''); 
	                                    posChangePage('#main');
	                                    //$.mobile.pageContainer.pagecontainer("change", "fittings.htm");
	                            },500);
	                        }else{
	                        	//$.jGrowl("User is not active");
	                        	customAlert("User is not active");
	                        	localStorage.clear();
								changeVisibilityDiv('loading_jp', "none");
	                        }   
	                            
                        } else {
                        	  customAlert("Login failed");
							  changeVisibilityDiv('loading_jp', "none");
                              
                        }
                        changeVisibilityDiv('loading_jp', "none");
                });
            }
        };
        self.resetApp = function() {
        	self.clickcount += 1;
console.log("self.clickcount " + self.clickcount);        	
        	if(self.clickcount == 5){
        		alert("App data has been reset.");
        		localStorage.clear();  
				window.location = "index.htm";
				self.clickcount = 0;
			}	
        };
        self.showSpinner =  ko.observable(false);
        self.showButton  =  ko.computed(function() {
                if (self.username() !== "" && self.password() !== "") return true;
                return false;
        }, this);


        /**
         * LOOP-
         * 	Checks the number of active requests to the ERP
         * 	and tells the user in the main screen if the APP is 100% ready to use.
         */
        setInterval(function() {
        	
        	// if num active conections > 2 => show loader
            // 2 is a good number, sometimes the app doest complete the and 2 requests always remain 
        	if( $.active > 2 )
        	{
        		$('#main-requests-loader').show();
        	}
        	else // else => hide loader
        	{
        		$('#main-requests-loader').hide();
        	}

        }, 1500);



    };
});