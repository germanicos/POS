define(['jquery', 'knockout', 'base'], function ($, ko) {

    UniformGarmentsStepVM = SimpleControl.extend({

        init: function (uniformTemplateVM) {
            console.log("Init UniformGarmentsStepVM")
            var self = this;
            this.uniformTemplateVM = uniformTemplateVM;

            this.garments = ko.observableArray(this.buildGarments());

            // for UI purposes. if this counter of garments is > 0, consider step as complet
        },

        //  There will be no SUIT in the uniform processes.
        //      Cause: Sometimes the customers will only need on part of the uniform, therefore
        //              its best if we keep the suit uniform separetly.
        //
        buildGarments: function () {

            return [
                {
                    'name': 'jacket',
                    'garments': ko.observableArray([]),
                    'id': '4',
                    'image': 'img/order_process/wetransfer-b60a96/jacket.png'
                },
                {
                    'name': 'pants',
                    'garments': ko.observableArray([]),
                    'id': '1',
                    'image': 'img/order_process/wetransfer-b60a96/pant.png'
                },
                {
                    'name': 'vest',
                    'garments': ko.observableArray([]),
                    'id': '3',
                    'image': 'img/order_process/wetransfer-b60a96/vest.png'
                },
                {
                    'name': 'shirt',
                    'garments': ko.observableArray([]),
                    'id': '2',
                    'image': 'img/order_process/wetransfer-b60a96/shirt.png'
                }
            ]

        },

        // Increases the count of garments
        incrementGarment(garmentId) {

            console.log("Incrementing " + garmentId + " for... ");

            let createdGarment = false;

            for (let garment of this.garments())
            {
                if (garment.id == garmentId) {
                    // gets the object from the design VM
                    createdGarment = this.uniformTemplateVM.garmentsDesignStep.getGarmentObject(garmentId)
                    garment.garments.push(createdGarment);

                    break;
                }
            }


            return createdGarment;
        },

        // Decreasese the number of garments
        decrementGarment(garmentId) {

            for (let garment of this.garments()) {
                if (garment.id == garmentId) {
                    if (garment.garments().length > 0) {
                        garment.garments.pop(); // removes last element
                    }
                    else {
                        console.log("Cannot go below 0");
                    }

                    break;
                }
            }
        },

        /**
         * This step is complete if at least one garment has one instance
         *
         * @returns ko.computed who changes automatically
         */
        checkStepIsComplete(){
            return ko.computed(() => {
                for (const garment of this.garments()){
                    if (garment.garments().length>0){
                        return true;
                    }
                }

                return false;
            })
        }

    });
});