define(['jquery', 'knockout', 'base'], function($, ko) {
	Completion = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			this.NumberShirts = 0;
			this.shirtCloneMessage = ko.observable(true);
			this.CompletionData     = [];
			this.CompletionDataAID  = 0;

			this.previousStepEnabled = ko.observable(false);

			this.workflow    = ko.observableArray([
				{id: 0, target: "#completions",  completed: "false", caption: "Completion", title: "UPLOAD IMAGES", myclass: "cimage" },
			]);


			this.previewFrontImage = ko.observable({media: ko.observable(''), tags: ko.observableArray([])});
			this.previewSideImage = ko.observable({media: ko.observable(''), tags: ko.observableArray([])});
			this.previewBackImage = ko.observable({media: ko.observable(''), tags: ko.observableArray([])});
			this.previewExtraImage = ko.observable({media: ko.observable(''), tags: ko.observableArray([])});

			this.media = ko.observable();
			this.mediaID = ko.observable();
			this.allComplete = ko.observable(false);
			this.mediaUpload = orderItem.MediaUploadVM;
			this.step = 0;

			this.currentStep = ko.observable();
			this.currentStep(this.workflow()[0]);
			this.currentStep.subscribe( function(data) {
				posChangePage(data.target);
				$('.cloneDialog').remove();
				that.renderCompletion();
			});

			this.clickPhoto = function(step)
			{
				console.log("Clicked foto step: ", step);


				this.step = step;
				try
				{
					that.mediaUpload.choosePhoto();
				}
				catch(e)
				{
					that.mediaUpload.imageGaleryAction();
				}

			}

			this.cloneShirtInfo = function(data) {
				var whatGarment = that.selectedVariantCompletion().id;
				for(x in that.CompletionData){
					if(x != whatGarment && that.variantNameCompletion()[x].title == 'Shirt'){
						that.CompletionData[x].frontImage.copyMedia(that.CompletionData[whatGarment].frontImage.getMedia());
						that.CompletionData[x].sideImage.copyMedia(that.CompletionData[whatGarment].sideImage.getMedia());
						that.CompletionData[x].extraImage.copyMedia(that.CompletionData[whatGarment].extraImage.getMedia());
						that.CompletionData[x].backImage.copyMedia(that.CompletionData[whatGarment].backImage.getMedia());
					}
				}
				that.checkSteps();
			};

			this.isStepCompleted = ko.computed(function() {
				for ( var ind in that.workflow() ) {
					if ( that.workflow()[ind].id == that.currentStep().id ) {
						if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
							return true;
						}else{
							return false;
						}
					}
				}
				return false;
			});

			this.addImageFromGallery = function(image){
				if(that.step == 1){
					that.frontImage.addMedia(image, image, false);
				}
				else if(that.step == 2){
					that.sideImage.addMedia(image, image, false);
				}
				else if(that.step == 3){
					that.backImage.addMedia(image, image, false);
				}
				else if(that.step == 4){
					that.extraImage.addMedia(image, image, false);
				}
			};

			this.mediaCallback = function(data){
				console.log('camera add', data);
				console.log(data);
				if(data)
				{
					console.log(that.step);
					if(that.step == 1){

						console.log("STEP 1");

						that.frontImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, true);


						console.log(that.mediaUpload.imagPath);
						console.log(that.mediaUpload.upPath);
						console.log(frontImage.getMediaPreview()());
					}
					else if(that.step == 2){
						that.sideImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, true);
					}
					else if(that.step == 3){
						that.backImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, true);
					}
					else if(that.step == 4){
						that.extraImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, true);
					}
				}
				that.checkSteps();
			};

			this.mediaUpload.setMediaCallback(this.mediaCallback);

			this.checkSteps = function(){
				var complete = true;

				console.log("Checking steps...");

				for(x in that.variantNameCompletion())
				{
					if(that.CompletionData[x].frontImage.hasMedia() && that.CompletionData[x].sideImage.hasMedia() && that.CompletionData[x].backImage.hasMedia())
					{
						console.log(that.variantNameCompletion()[x]);
						that.variantNameCompletion()[x].completed(true);
					}
					else
					{
						complete = false;
					}
				}
				that.allComplete(complete);
			};

			this.stepCaption = ko.computed(function() {
				return that.currentStep().caption;
			});

			this.stepTitle = ko.computed(function() {
				return that.currentStep().title;
			});


			this.completion       = ko.observable(0);
			this.variantNameCompletion  = ko.observableArray([{id: 0, title: "Completion 1", completed: ko.observable(false)}]);
			this.selectedVariantCompletion  = ko.observable(this.variantNameCompletion()[0]);
			this.selectedVariantCompletion.subscribe(function(data) {
				that.selectedVariantCompletion();
			//	posChangePage("#completions");
			//	$('.cloneDialog').remove();
				that.renderCompletion();
				var cs = that.currentStep().id;
				that.currentStep(that.workflow()[cs] );
			});

			this.nextStepCaption = ko.computed(function() {
				var tWork = that.workflow();
				var tInd  = -1;
				for ( var ind in tWork ) {
						if (tWork[ind].completed !== "true") {
						tInd = ind;
						break;
					}
				}
				if (tInd != -1 ) {
					return tWork[tInd].title;
				} else {
					return " Not available ";
				}
			});

			this.previousStepCaption = ko.computed(function() {
				if ( that.currentStep().id !== 0 ) {
					return that.workflow()[that.currentStep().id  - 1].title;
				} else {
					return " Not available ";
				}
			});


			this.CompletionCustomer = ko.observable('');
			this.CompletionCustomer.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionCustomer = data;
				that.flushModel();
			});

			this.CompletionGarment = ko.observable('');
			this.CompletionGarment.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionGarment = data;
				that.flushModel();
			});

			this.CompletionGarmentFabric = ko.observable('');
			this.CompletionGarmentFabric.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionGarmentFabric = data;
				that.flushModel();
			});

			this.CompletionGarmentFabricImage = ko.observable('');
			this.CompletionGarmentFabricImage.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionGarmentFabricImage = data;
				that.flushModel();
			});

			this.CompletionId = ko.observable('');
			this.CompletionId.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionId = data;
				that.flushModel();
			});

			this.CompletionOrderId = ko.observable('');
			this.CompletionOrderId.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionOrderId = data;
				that.flushModel();
			});

			this.CompletionOrderProductId = ko.observable('');
			this.CompletionOrderProductId.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].CompletionOrderProductId = data;
				that.flushModel();
			});

			this.SkippedImages = ko.observable(false);
			this.SkippedImages.subscribe(function(data) {
				that.CompletionData[ that.getRow(that.selectedVariantCompletion().id)  ].SkippedImages = data;
				that.flushModel();
			});

			this.NoImagesConfirm = function() {

				   text = "By skipping uploading images, you agree that any fault with this garment is no longer in Germanicos control and you are in full responsibility";
				document.getElementById("warningdialogtext").innerHTML = text;
				dialog = document.getElementById('warningdialog');
				dialog.style.display = "block";
				return true;
			};

			this.buttonNoAccept = function(){
				that.SkippedImages(false);
				dialog = document.getElementById('warningdialog');
				dialog.style.display = "none";
			}

			this.buttonYesAccept = function(){
				that.SkippedImages(true);
				that.sync();
				dialog = document.getElementById('warningdialog');
				dialog.style.display = "none";
			}

			this.CompletionDialog = function() {
				dialog = document.getElementById('completiondialog');
				dialog.style.display = "block";
				return true;
			};

			this.buttonToFittings = function(){
				posChangePage('#customerGarmentsFittingsList');
			}

			this.buttonToMain = function(){
				posChangePage('#main');
			}

			this.allCompletedCheck = function()
			{
				var found = true;
				   for(var x = 0; x < that.variantNameCompletion().length; x++){
					   if( that.variantNameCompletion[ x ].completed()){
						   found = false;
						   break;
					   }
				   }
				   return found;
			}

			   this.submitText = function(){
				   if(this.allCompletedCheck() == true){
					   return "SUBMIT & COMPLETE";
				   }else{
					   return "SUBMIT & NEXT GARMENT";
				   }

			}

			this.goToNextNotCompleted = function(){
				that.checkSteps();
				for(var x = 0; x < that.variantNameCompletion().length; x++){
					   if( !that.variantNameCompletion()[ x ].completed()){
						   that.selectedVariantCompletion(that.variantNameCompletion()[x]);
						   break;
					   }
					else if(that.variantNameCompletion()[ x ].title == "Shirt" && that.shirtCloneMessage() && that.NumberShirts > 1){
						const confirmAction = () => {
							that.cloneShirtInfo();
						}

						const finallyAction = () => {
							that.shirtCloneMessage(false);
						}

						pos_confirm({msg: "Do you wish to clone the images for all Shirts?", confirmAction, finallyAction})
					}
				   }
			}

			this.goToNextNotCompletedGarment = function(){
				for(var x = 0; x < that.variantNameCompletion().length; x++){
					   if( that.CompletionData[ x ].CompletionId == ''){
						   that.selectedVariantCompletion(that.variantNameCompletion()[x]);
						   break;
					   }
				   }
			}



	///////////////////////////		SYNC PART	///////////////////////////
	//////////////////////////////////////////////////////////////////////

			this.sync = function() {

				const checkMandatoryFields = () => {
					let mandatoryFieldsStatus = true;

					for(x in that.CompletionData) {
						var whichGarment = that.variantNameCompletion()[x].id;
						var frontImage  = that.CompletionData[whichGarment].frontImage.getSyncInfo();
						var sideImage  = that.CompletionData[whichGarment].sideImage.getSyncInfo();
						var backImage  = that.CompletionData[whichGarment].backImage.getSyncInfo();
						const completion_with_no_images = that.CompletionData[whichGarment].shouldUseWithoutImage;

						if(
							// need images
							completion_with_no_images == false &&

							// have at least only one image for each BSF
							(	frontImage.filter(image => image.path == "").length != 0 ||
							sideImage.filter(image => image.path == "").length != 0 ||
							backImage.filter(image => image.path == "").length != 0   ) &&

							// does not skip images
							this.SkippedImages() == false
							) {
								mandatoryFieldsStatus = false;
							}

					}

					return mandatoryFieldsStatus;
				}

				if (!checkMandatoryFields()){
					customAlert("Please upload all required images");
					return;
				}

				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";

				var allOK = true;
				for(x in that.CompletionData)
				{
					var whichGarment = that.variantNameCompletion()[x].id;
					var customerString = that.CompletionCustomer();
					var completionIdPost = that.CompletionData[x].CompletionId;

					var dateToPost = '';
					var date = new Date();
					var yearnow = date.getFullYear();
					var monthnow = date.getMonth() + 1;
					var daynow = date.getDate();
					dateToPost = daynow + "-" + monthnow + "-" + yearnow;

					var frontImage  = that.CompletionData[whichGarment].frontImage.getSyncInfo();
					var sideImage  = that.CompletionData[whichGarment].sideImage.getSyncInfo();
					var backImage  = that.CompletionData[whichGarment].backImage.getSyncInfo();
					var extraImage  = that.CompletionData[whichGarment].extraImage.getSyncInfo();
					const completion_with_no_images = that.CompletionData[whichGarment].shouldUseWithoutImage;


					data = {
							device_id: typeof device != "undefined" ? device.uuid: "",
							user_id: authCtrl.userInfo.user_id,
							customer: customerString,
							//completion_id: completionIdPost,
							order_id: that.CompletionData[whichGarment].CompletionOrderId,//this.selected_order.order_id,
							orders_products_id: that.CompletionData[whichGarment].CompletionOrderProductId,
							garment: that.CompletionData[whichGarment].CompletionGarment,
							//skipped_images: this.SkippedImages(),
							front_image: frontImage,
							side_image: sideImage,
							back_image: backImage,
							extra_image: extraImage ? extraImage : "",
							completion_with_no_images
						};


					console.log(data);
					var datatopost = JSON.stringify(data);
					datatopost = datatopost.replace("\"isDirty\":true,", "");
					console.log("datatopost: " + datatopost);


					const _order_id = that.CompletionData[whichGarment].CompletionOrderId;
					const _garment = that.CompletionData[whichGarment].CompletionGarment;


					$.ajax({
						type: 'POST',
						url: BUrl + 'client_fittings/completion_endpoint2',
						data: {
							data: datatopost
						},
						success: function(dataS)
						{
							try
							{
								dataS = JSON.parse(dataS);
								if (dataS.valid == '1')
								{
									customAlert("Completion successfully added");

									/**
									 * Send manufacturer the email about this completion, so he can perform the PATTERN UPDATE
									 */
									$.post(window.BUrl + "alterations_pos/send_manufacturer_new_pattern_to_update_email", { "user": authCtrl.userInfo, 'order_id' : _order_id, "garment_type_name" : _garment }, (ret) => { console.log('manu pattern to update email:', ret); }, "json");
								}
								else
								{
									console.log('fail');
									$.jGrowl(dataS.msg);
									allOK = false;
								}
							}
							catch(e)
							{
								console.log('fail catch', e);
								allOK = false;
							}
						},
						async:false
					});
				}

				spinner.style.display = "none";
				if(allOK){
					customAlert("Completion succesfully added");
					that.CompletionDialog();
				}
				else{
					customAlert("The completions was not succesfully completed. Please try again later or contact the admins.");
				}

			}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

			this.getFabric = function(){
				return this.getVariant(this.selectedVariantCompletion().id)
			}

		}, // end of init


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		previousStep:  function() {
			if (this.currentStep().id !== 0) {
				this.previousStepEnabled(true);
				this.currentStep( this.workflow()[this.currentStep().id  - 1]);
			}
		},

		flushModel: function() {
			this.dsRegistry.getDatasource(this.subscribed).setStore({
				"Completion"  : this.CompletionData
			}, true);
		},

		getVariant: function(id) {
			var toreturn = this.CompletionData[0];
			for (var ind in this.CompletionData) {
				if ( this.CompletionData[ind].variantId == id  ) {
					toreturn = this.CompletionData[ind];
				}
			}
			return toreturn;
		},

		getRow: function(id) {
			for (var ind in this.CompletionData) {
				if ( this.CompletionData[ind].variantId == id  ) {
					return ind;
				}
			}
			return -1;
		},

		addVariantCompletion: function(name) {
	//console.log(this.variantNameCompletion);
			if(name == "Shirt"){
				this.NumberShirts++;
			}
			if(this.variantNameCompletion()[0].title != "Completion 1"){
				this.CompletionDataAID += 1;
				var newid = this.CompletionDataAID + 1;
				var vname = name;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantCompletion().id ) )  ); //CLONE Object
				this.variantNameCompletion.push({title: vname, id: this.CompletionDataAID, completed: ko.observable(false)});
				tObj.variantId = this.CompletionDataAID;
				this.flushModel();
			}else{
				this.variantNameCompletion()[0].title = name;
			}
		},

		addVariantsCompletions: function() {
			for(var x in orderItem.completionsVM.CompletionData){
			//	console.log("addVariantsCompletions " + x);
				if(orderItem.completionsVM.CompletionData[x].CompletionGarment == "Shirt"){
					this.NumberShirts++;
				}
				if( x != 0){
					this.CompletionDataAID += 1;
					var vname = orderItem.completionsVM.CompletionData[x].CompletionGarment;
					var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantCompletion().id ) )  ); //CLONE Object
					this.variantNameCompletion.push({title: vname, id: this.CompletionDataAID, completed: ko.observable(false)});
					tObj.variantId = this.CompletionDataAID;
					this.flushModel();
				}else{
					var vname = orderItem.completionsVM.CompletionData[x].CompletionGarment;
					var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantCompletion().id ) )  ); //CLONE Object
					this.variantNameCompletion()[x].title = vname;
					tObj.variantId = this.CompletionDataAID;
					this.flushModel();
				}

			}
		},



		digestData: function(data) {
			this.CompletionData  = data.Completion.map(completion => {
				completion.shouldUseWithoutImage = false;
				return completion;
			});

			this.checkIfGarmentsAreAvailableToCompletion();

			this.renderView();

		},

		renderView: function() {
			this.renderCompletion();
		},

		renderCompletion: function() {

			var whichGarment = this.selectedVariantCompletion().id;


			//Get selected Variant
			try{
				var tData = this.getVariant(this.selectedVariantCompletion().id);
				if (tData != null) {
					//Update observables
					if (typeof (tData.frontImage) != "undefined") {
						this.frontImage = tData.frontImage;
						this.frontImage.setCustomPreview(this.previewFrontImage);
						this.frontImage.setCustomPreviewValue();

					}
					if (typeof (tData.sideImage) != "undefined") {
						this.sideImage = tData.sideImage;
						this.sideImage.setCustomPreview(this.previewSideImage);
						this.sideImage.setCustomPreviewValue();
					}
					if (typeof (tData.backImage) != "undefined") {
						this.backImage = tData.backImage;
						this.backImage.setCustomPreview(this.previewBackImage);
						this.backImage.setCustomPreviewValue();
					}
					if (typeof (tData.extraImage) != "undefined") {
						this.extraImage = tData.extraImage;
						this.extraImage.setCustomPreview(this.previewextraImage);
						this.extraImage.setCustomPreviewValue();
					}
					if (typeof(tData.CompletionCustomer)		!= "undefined") {this.CompletionCustomer(tData.CompletionCustomer);}
					if (typeof(tData.CompletionGarment)		!= "undefined") {this.CompletionGarment(tData.CompletionGarment);}
					if (typeof(tData.CompletionGarmentFabric)	!= "undefined") {this.CompletionGarmentFabric(tData.CompletionGarmentFabric);}
					if (typeof(tData.CompletionGarmentFabricImage)	!= "undefined") {this.CompletionGarmentFabricImage(tData.CompletionGarmentFabricImage);}
					if (typeof(tData.CompletionId)    			!= "undefined") {this.CompletionId(tData.CompletionId);}
					if (typeof(tData.CompletionOrderId)    	!= "undefined") {this.CompletionOrderId(tData.CompletionOrderId);}
					if (typeof(tData.CompletionOrderProductId)    	!= "undefined") {this.CompletionOrderProductId(tData.CompletionOrderProductId);}
					if (typeof(tData.SkippedImages)   != "undefined") {this.SkippedImages(tData.SkippedImages);}
				}
			}catch(e){
				;
			}
		},

		/**
		 * Before start completion, we must check if this garments are available to send completion images
		 *
		 * The garment should not have any pending alteration report
		 */
		checkIfGarmentsAreAvailableToCompletion : function () {
			document.getElementById('loading_jp').style.display = "block";
			const mapTypeToId = (typeString) => {
				typeString = typeString.toLowerCase();

				if (typeString.includes("pant")) {
					return 1;
				} else if (typeString.includes("jacket")) {
					return 4;
				} else if (typeString.includes("vest")) {
					return 2;
				} else if (typeString.includes("shirt")) {
					return 3;
				}
				else {
					console.error("garment not identified");
					return null;
				}
			}

			const getGarmentFromId = (garmentId) => {
				const search = this.CompletionData.find(completionData => completionData.CompletionOrderProductId == garmentId);
				if (search){
					return search
				}else{
					console.error("garment not found", garmentId);
					return {};
				}
			};

			// in development
			const removeUnavailableGarments = (unavailableGarments) => {
				console.log("Removing unavailable garments", unavailableGarments);

				for (const unavailableGarment of unavailableGarments) {
					const garment = getGarmentFromId(unavailableGarment.unavailableGarment.garment_id);

				}

			}

			const url = window.BUrl + "alterations_pos/are_all_alteration_reports_submitted";
			const dataToSend = this.CompletionData.map(completionData => {
				return {
					garment_id: completionData.CompletionOrderProductId,
					garment_type: mapTypeToId(completionData.CompletionGarment),
				}
			});

			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'json',
				async: true,
				data: {
					"user": authCtrl.userInfo,
					garments: JSON.stringify(dataToSend),
				},
				success: function (dataS) {
					console.log('dataS', dataS);

					const unavailableGarments = dataS.garments.filter(garments => !garments.are_all_alterations_submitted);

					if (unavailableGarments.length > 0) {
						pos_warning({
							msg: /* html */`
								<div>
									<span>There are some garments unavailable for completion. Please check if all alterations has been submitted</span><br>
									<h3 style="margin: 20px 0;">Unavailable Garments</h3>
									${unavailableGarments.map(unavailableGarment => {
										const garment = getGarmentFromId(unavailableGarment.garment_id);
										return /* html */`
											<span>
												${garment.CompletionCustomer.customer_first_name} ${garment.CompletionCustomer.customer_last_name}'s
												${garment.CompletionGarment}
												- Order ${garment.CompletionOrderId}
											</span>
											<br>
										`;
									}).join("")}
								</div>
							`,
							title:'Ops',
							finallyAction : () => {
								// go to fitting list
								// posChangePage('#fittingList');
								posChangePage('#customerGarmentsFittingsList');

							},
						})
					}

				},
				error: function (dataS) {
					console.error(dataS);
				},
				complete :()=>{
					document.getElementById('loading_jp').style.display = "none";
				}
			});
		}


	});

	defCompletion = SimpleDatasource.extend({
		init: function( name, dsRegistry, data ) {
			if(data == null || data == undefined){
			//	orderItem.completionsVM.addVariantCompletion('new');

	console.log("data is null");
				var Completion = {};
				Completion.variantId        = 0;

				Completion.CompletionCustomer = '';
				Completion.CompletionGarment = '';
				Completion.CompletionGarmentFabric = '';
				Completion.CompletionGarmentFabricImage = '';
				Completion.CompletionId = '';
				Completion.CompletionOrderId = '';
				Completion.CompletionOrderProductId = '';
				Completion.SkippedImages = false;

				var iCompletion = {
					Completion: []
				};
				iCompletion.Completion.push(Completion);
				this._super(name, iCompletion, dsRegistry);

			}else{
	//console.log("data is not null");
				var iCompletion = {
					Completion: []
				};

				var x = 0;
				for (var a in data) {
					for (var b in data[a].products){
						if( data[a].products[b].checked == true ){
						//	orderItem.completionsVM.addVariantCompletion(data[a].products[b].garment);
						var c = localStorage.getItem("completionindex");
	//console.log("data[a].products[b]: " + JSON.stringify(data[a].products[b]) );
	//console.log("completion index: " + c);
							var Completion = {};
							Completion.variantId = x;

							Completion.CompletionId = '';
							Completion.frontImage = new MediaType(1, null, false, '',true);
							Completion.sideImage = new MediaType(1, null, false, '',true);
							Completion.backImage = new MediaType(1, null, false, '',true);
							Completion.extraImage = new MediaType(1, null, false, '',true);
							Completion.CompletionCustomer = orderItem.custSelectVM.selectedCustomer();
							Completion.CompletionGarment = data[a].products[b].garment;
							Completion.CompletionGarmentFabric = data[a].products[b].fabric;
							Completion.CompletionGarmentFabricImage = data[a].products[b].fabricImage;
							Completion.CompletionOrderId = data[a].order_id;
							Completion.CompletionOrderProductId = data[a].products[b].orders_products_id;
							Completion.SkippedImages = false;

							x++;
	//console.log("Completion: " + JSON.stringify(Completion));
							iCompletion.Completion.push(Completion);
						}
					}
				}
				this._super(name, iCompletion, dsRegistry);
			}
		}
	});


	//END DEFINE CLOSURE
	});
