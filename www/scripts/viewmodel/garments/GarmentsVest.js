define(['jquery', 'knockout', 'base'], function($, ko) {
GarmentsVest = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;

		this.fabricsVM   = new FabricsCntClass(DsRegistry);
		this.fabricsVM.subscribeTo('fabricsDS');


		this.vestData     = [];
		this.vestDataAID  = 0;
		
		this.mediaUpload = orderItem.MediaUploadVM;
		this.indexGarment = -1;
		this.indexOption = -1;
		this.isImage = false;
		this.customCount = ko.observable(0);
		this.isExternal = false;
		this.isFabric = false;
		
		this.galleryImages = function(){
			that.isExternal = true;
			$('#files').trigger('click');
		};

		this.addCustomImage = function(indexGarment, indexOption, isFabric){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = true;
			that.isExternal = false;
			that.isFabric = isFabric;
			that.mediaUpload.choosePhoto();
		};
		
		this.addCustomVideo = function(indexGarment, indexOption){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = false;
			that.isExternal = false;
			that.mediaUpload.chooseVideo();
		};
		
		this.mediaCallback = function(data){
			if(that.isFabric){
				orderItem.FabricImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
				that.isFabric = false;
			}
			else if(that.isImage){
				that.vestData[that.indexGarment].custom.getCustomImages(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
			else{
				that.vestData[that.indexGarment].custom.getCustomVideos(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
		};

		this.previousStepEnabled = ko.observable(false);
		
		this.nextStepOLD = function() {
			var cs = that.currentStep().id;			
			if(cs == 0){
				if(that.vestFabric().title != "none"){
					that.workflow()[cs].completed = true;
				}	
			}else{
				that.workflow()[cs].completed = true;	
			}
			var foundnotcompletedstep = false;
			for(var x = cs+1; x < that.workflow().length; x++){
				if(that.workflow()[x].completed == false){
					that.currentStep(that.workflow()[x] );
					foundnotcompletedstep = true;
					break;
				}
			}				
			if(foundnotcompletedstep == false){
				for(var x = 0; x <= cs ; x++){
					if(that.workflow()[x].completed == false){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}
			}
			if(foundnotcompletedstep == false){
				posChangePage('#orderItemSelectGarment');
			}
		}
		
this.nextStep = function() {
			
			var cs = that.currentStep().id;
				
			var notcompletedstep = false;						
			if(cs == 0){
				for(var x = 0; x < that.vestData.length; x++){								
					if (that.vestData[x].vestFabric.title == 'none' && !that.vestData[x].VestHasCustomerFabric) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 1){
				for(var x = 0; x < that.vestData.length; x++){
					if (that.vestData[x].vestNecklineStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 2){
				for(var x = 0; x < that.vestData.length; x++){									
					if (that.vestData[x].vestBottomStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 3){
				for(var x = 0; x < that.vestData.length; x++){
					if (that.vestData[x].vestButtonNumber.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 4){
				for(var x = 0; x < that.vestData.length; x++){
					if (that.vestData[x].vestPocketStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 5){
				for(var x = 0; x < that.vestData.length; x++){
					if (that.vestData[x].vestBackStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 6){
				;
			}else if(cs == 'Custom'){
				for(var x = 0; x < that.vestData.length; x++){
					if(!that.vestData[x].custom.isFinished()){
						notcompletedstep = true;
						break;
					}
				}
			}
			
			if(notcompletedstep == false){
				if(cs == 'Custom'){
					that.customWorkflow.completed = true;
				}
				else{
					that.workflow()[cs].completed = true;
				}
				var foundnotcompletedstep = false;
				for(var x = 0; x < that.workflow().length; x++){
					if(that.workflow()[x].completed == false && that.workflow()[x] !== that.customWorkflow){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}			
				if(notcompletedstep == false){
					for(var x = 0; x <= cs ; x++){
						if(that.workflow()[x].completed == false){
							that.currentStep(that.workflow()[x] );
							foundnotcompletedstep = true;
							break;
						}
					}
					if(foundnotcompletedstep == false && !that.customWorkflow.completed && that.customCount() > 0 ){
						that.currentStep(that.customWorkflow);
						foundnotcompletedstep = true;
					}
				}
				if(foundnotcompletedstep == false){
					posChangePage('#orderItemSelectGarment');
				}				
				
			}else{
				if(cs == 'Custom'){
					that.customWorkflow.completed = false;
					that.currentStep(that.customWorkflow);
				}
				else{
					that.workflow()[cs].completed = false;
					that.currentStep(that.workflow()[cs] );
				}
				
				alert("Please fill in all necessary fields");
			}
			
		}		
		
		

		
		this.workflow    = ko.observableArray([
			{id: 0, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Fabric & Fit" },
		//	{id: 8, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Design" },
			{id: 1, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Lapel" },
			{id: 2, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Bottom" },
			{id: 3, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Buttons" },
			{id: 4, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Pockets" },
			{id: 5, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Back Style" },
			{id: 6, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Linning & Piping" },
			{id: 7, target: "#garmentsVest",  completed: false, caption: "Vest", title: "Notes" },
            {id: 'Custom', target: "#garmentsVest",  completed: false, caption: "Pant", title: "Custom Options" }
		]);
		this.customWorkflow = this.workflow()[this.workflow().length - 1];
		
		this.customStep = function(){
			that.currentStep(that.customWorkflow);
		};

		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			
			if(data.id == 5){
				that.PreviewVisible('back');
			}else if(data.id == 6){
				that.PreviewVisible('monogram');
			}else{
				that.PreviewVisible('front');
			}			
			
			posChangePage(data.target);
		});

		this.isStepCompleted = ko.computed(function() {
			
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					return that.workflow()[ind].completed;
				}
			}
			console.log('isStepCompleted bo');
			return false;
		});
		


		this.vestStepACompletion = function() {
			/*
            for ( var ind in that.vestData ) {
				if (that.vestData[ind].vestFabric.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.vestData ) {
				if (that.vestData[ind].vestBottomStyle.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			for (     ind in that.vestData ) {
				if (that.vestData[ind].vestPocketStyle.id == 0 ) {
					that.setAsCompleted(0, false);
					return;
				}
			}
			that.setAsCompleted(0, true);
			*/
		};

		this.setAsCompleted =  function(id, status) {
		//	console.log('setAdCompleted ' + status);
			var tWork = that.workflow();
			tWork[id].completed = status;
			//that.workflow(tWork);
		};


		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});
		
		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});
		
		this.reviewItem = ko.computed(function() {
			return "A Vest";
		});


		this.completion   = ko.observable(0);
		this.variantNameVest      = ko.observableArray([{id: 0, title: "Vest 1"}]);
		
		this.selectedVariantVest  = ko.observable(this.variantNameVest()[0]);
		this.selectedVariantVest.subscribe(function(data) {
			that.selectVariantVest();
		});
		
		//Prev/Next computedz

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== true) {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return 'not';
				//return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});
 
		this.VestNotes = ko.observable('');
		this.VestNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestNotes = data;
			that.flushModel();
			////////////////////////////////////////////// that.vestStepACompletion();
		});
		this.VestFabricNotes = ko.observable('');
		this.VestFabricNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestFabricNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.VestFitAndLapelNotes = ko.observable('');
		this.VestFitAndLapelNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestFitAndLapelNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.VestBottomNotes = ko.observable('');
		this.VestBottomNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestBottomNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.VestButtonsNotes = ko.observable('');
		this.VestButtonsNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestButtonsNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.VestPocketsNotes = ko.observable('');
		this.VestPocketsNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestPocketsNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.VestBackStyleNotes = ko.observable('');
		this.VestBackStyleNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestBackStyleNotes = data;
			that.flushModel();
			// that.vestStepACompletion(); 
		});
		this.VestContrastNotes = ko.observable('');
		this.VestContrastNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].VestContrastNotes = data;
			that.flushModel();
			// that.vestStepACompletion(); 
		});		

		this.vestCustomButtonsNotes = ko.observable('');
		this.vestCustomButtonsNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomButtonsNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.vestCustomButtonsImage = ko.observable('');
		this.vestCustomButtonsImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomButtonsImage = json;
			that.flushModel();
			// that.vestStepACompletion();
		});
		
		this.vestCustomLapelNotes = ko.observable('');
		this.vestCustomLapelNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomLapelNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.vestCustomLapelImage = ko.observable('');
		this.vestCustomLapelImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomLapelImage = json;
			that.flushModel();
			// that.vestStepACompletion();
		});				

		//vestFabric
		this.vestFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.vestFabric.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestFabric = data;
			that.flushModel();
			// that.vestStepACompletion();
		});


		this.vestDesignList = ko.observable(that.dsRegistry.getDatasource('vestDesignDS').getStore());
		this.vestDesign     = ko.observable(that.dsRegistry.getDatasource('vestDesignDS').getStore()[0]);
		this.vestDesign.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestDesign = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		this.vestCustomDesignNotes = ko.observable('');
		this.vestCustomDesignNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomDesignNotes = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.vestCustomDesignImage = ko.observable('');
		this.vestCustomDesignImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomDesignImage = json;
			that.flushModel();
			// that.vestStepACompletion();
		});

		//vestNeckline
		this.vestNecklineStyleList = ko.observable(that.dsRegistry.getDatasource('vestLapelStyleDS').getStore());
		this.vestNecklineStyle     = ko.observable(that.dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0]);
		this.vestNecklineStyle.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestNecklineStyle = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		//vestButtonNumber
		this.vestButtonNumberList = ko.observable(that.dsRegistry.getDatasource('vestButtonNumberDS').getStore());
		this.vestButtonNumber     = ko.observable(that.dsRegistry.getDatasource('vestButtonNumberDS').getStore()[1]);
		this.vestButtonNumber.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestButtonNumber = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		
		//vestPocketStyle
		this.vestPocketStyleList  = ko.observable(that.dsRegistry.getDatasource('vestPocketStyleDS').getStore());
		this.vestPocketStyle      = ko.observable(that.dsRegistry.getDatasource('vestPocketStyleDS').getStore()[0]);
		this.vestPocketStyle.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestPocketStyle = data;
			that.flushModel();
			// that.vestStepACompletion();
		});


		this.VestBreastPocket = ko.observable(false);
		this.VestBreastPocket.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].VestBreastPocket = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		this.VestDoubleBreastPocket = ko.observable(false);
		this.VestDoubleBreastPocket.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].VestDoubleBreastPocket = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		//vestBottom style
		this.vestBottomStyleList  = ko.observable(that.dsRegistry.getDatasource('vestBottomStyleDS').getStore());
		this.vestBottomStyle      = ko.observable(that.dsRegistry.getDatasource('vestBottomStyleDS').getStore()[0]);
		this.vestBottomStyle.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestBottomStyle = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		
		//vestBack style
		this.vestBackStyleList  = ko.observable(that.dsRegistry.getDatasource('vestBackStyleDS').getStore());
		this.vestBackStyle      = ko.observable(that.dsRegistry.getDatasource('vestBackStyleDS').getStore()[0]);
		this.vestBackStyle.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestBackStyle = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		

		//vestContrast
		this.vestContrastPocket = ko.observable(false);
		this.vestContrastPocket.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestContrastPocket = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		this.vestContrastFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.vestContrastFabric.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestContrastFabric = data;
			that.flushModel();
			// that.vestStepACompletion();
		});	


		//vestPiping
		this.vestPipingList  = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.vestPiping      = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.vestPiping.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestPiping = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		//vestLining
		this.vestLiningList  = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore());
		this.vestLining      = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore()[0]);
		this.vestLining.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestLining = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

//vestMonogram 
		this.vestMonogram = ko.observable('');
        this.vestMonogram.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestMonogram = data;
			that.flushModel();
			// that.vestStepDCompletion();
		});
		
		this.vestMonogramNotes = ko.observable('');
		this.vestMonogramNotes.subscribe(function(data) {				
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestMonogramNotes = data;
			that.flushModel();
			// that.vestStepBCompletion();
		});
		
		this.vestMonogramExtraLine = ko.observable('');
        this.vestMonogramExtraLine.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestMonogramExtraLine = data;
			that.flushModel();
			// that.vestStepDCompletion();
		});

		this.vestDefaultMonogramHeader = ko.observable(true);
		this.vestDefaultMonogramHeader.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestDefaultMonogramHeader = data;
			that.flushModel();
			// that.vestStepDCompletion();
		});
		this.vestCustomMonogramHeader = ko.observable('');
		this.vestCustomMonogramHeader.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomMonogramHeader = data;
			that.flushModel();
			// that.vestStepDCompletion();
		});

		this.vestMonogramStitchDifferentFromPiping = ko.observable(false);
		this.vestMonogramStitchDifferentFromPiping.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestMonogramStitchDifferentFromPiping = data;
			that.flushModel();
			// that.vestStepDCompletion();
		});
		
		this.vestMonogramStitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.vestMonogramStitchColour.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestMonogramStitchColour = data;
			that.flushModel();
			// that.vestStepDCompletion();
		});

		this.vestCustomMonogram = ko.observable(false);
		this.vestCustomMonogram.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomMonogram = data;
			that.flushModel();
			// that.vestStepBCompletion();
		});
		
		this.vestCustomMonogramImage = ko.observable('');
		this.vestCustomMonogramImage.subscribe(function(data) { 
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomMonogramImage = json;
			that.flushModel();
			// that.vestStepBCompletion();
		});
		this.vestCustomMonogramNotes = ko.observable('');
		this.vestCustomMonogramNotes.subscribe(function(data) {
			that.vestData[ that.getRow(that.selectedVariantVest().id)  ].vestCustomMonogramNotes = data;
			that.flushModel();
			// that.vestStepBCompletion();
		});
		
		
		//vestButtons
		this.vestButtonsList  = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore());
		this.vestButtons      = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
		this.vestButtons.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestButtons = data;
			that.flushModel();
			// that.vestStepACompletion();
		});


		//vestThread
		this.vestThreadsList  = ko.observable(that.dsRegistry.getDatasource('vestThreadsDS').getStore());
		this.vestThreads      = ko.observable(that.dsRegistry.getDatasource('vestThreadsDS').getStore()[0]);
		this.vestThreads.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestThreads = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

		this.vestFitList =  ko.observableArray(["Fitted", "Semi Fitted", "Standard Fit"]);//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore());
		this.vestFit = ko.observable('Semi Fitted');//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore()[2]);
        this.vestFit.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].vestFit = data;
			that.flushModel();
			// that.vestStepACompletion();
		});


		this.urgent = ko.observable(0);
		this.urgent.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].urgent = data;
			that.flushModel();
			// that.vestStepACompletion();
		}); 
		/*
		this.urgentDate = ko.observable('');
		this.urgentDate.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].urgentDate = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		*/
		this.DOP_day = ko.observable('');
		this.DOP_day.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].DOP_day = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.DOP_month = ko.observable('');
		this.DOP_month.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].DOP_month = data;
			that.flushModel();
			// that.vestStepACompletion();
		});
		this.DOP_date = ko.observable('');
		this.DOP_date.subscribe(function(data) {
			that.vestData[that.getRow(that.selectedVariantVest().id)].DOP_date = data;
			that.flushModel();
			// that.vestStepACompletion();
		});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////


		this.PreviewVisible = ko.observable('front');

		this.vestFabricSelect = function() {
			//console.log('vestF');
                        
                      //  alert("vestFabricSelect");
			that.dsRegistry.setContext({
				targetModel:     'garmentsVestDS',
				targetGarment:   'vest',
				targetDelta:      this.getRow(this.selectedVariantVest().id),
				targetAttribute: 'vestFabric',
				callbackPage:    '#garmentsVestFabric'
			});
			var fab = that.dsRegistry.getDatasource('garmentsVestDS').getStore().vest[that.getRow(that.selectedVariantVest().id)].vestFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};

		this.vestContrastFabricSelect = function() {
			//console.log('vestCF');
			this.dsRegistry.setContext({
				targetModel:     'garmentsVestDS',
				targetGarment:   'vest',
				targetDelta:      this.getRow(this.selectedVariantVest().id),
				targetAttribute: 'vestContrastFabric',
				callbackPage:    '#garmentsVestContrast'
			});
			var fab = that.dsRegistry.getDatasource('garmentsVestDS').getStore().vest[that.getRow(that.selectedVariantVest().id)].vestContrastFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};


		this.fnCloneVest = function(e) {             
                fe = this;
				var parentOffset = $('.content-wrapper').offset(); 
				//or $(this).offset(); if you really just want the current element's offset
				relX = e.pageX - parentOffset.left;
				relY = e.pageY - parentOffset.top;

				//need testing
				if (relY > 500) relY -= 200;
				targetAttr = $(this).parent().parent().parent().attr('data-target-attr');
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().parent().attr('data-target-attr');	
				}	
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().attr('data-target-attr');	
				}	

				currentVariant = that.selectedVariantVest();//cloneVestAttributes
				var vnames = that.variantNameVest(); 
				if (vnames.length == 1) return;

				var fa  = 'Select Vest changes! ';
			    fa += '<div class="cloneDialogBtnClose" data-role="button"><img src="http://shirt-tailor.net/thepos/appimg/template/topmenu/close.png"/></div><div  data-role="fieldcontain"><fieldset data-role="controlgroup">';
				for (var ind in vnames) {
					//if (vnames[ind].id != currentVariant.id) {
						check = '';
						if (vnames[ind].id == currentVariant.id) check = ' checked="checked" disabled';
						fa += '<input type="checkbox" '+ check +' data-id="' + vnames[ind].id + '" id="cSuit-'+ vnames[ind].id +'" name="cSuit-'+ vnames[ind].id +'"/>'
						fa += '<label for ="cSuit-'+ vnames[ind].id +'">' + vnames[ind].title + '</label>';
					//}
				}
				fa += "</fieldset></div><div class='cloneDialogBtn' data-role='button'>Apply</div>";
				$('.cloneDialog').remove();
				$('.content-wrapper').append("<div class='cloneDialog' id='cloneSuitDialog' style='left: " + relX + "px !important; top: " + relY + "px !important;'>" + fa + "</div>");
				
				$('#cloneSuitDialog input').checkboxradio();
				$('.cloneDialogBtn').on('click', function() {
					cI = [];
					$('#cloneSuitDialog :checked').each(function(thet,c) {cI.push($(c).attr('data-id'));});
					that.cloneVestAttributes(currentVariant.id, targetAttr, cI);
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
					//$.mobile.sdCurrentDialog.close();
				});
				$('.cloneDialogBtnClose').on('click', function() {
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
				});
		}

		this.attachCloneVestAttribute = function() {
		
	//		$('#textCloneBtn1').click('click', that.fnCloneVest);           
	//		$('#garmentsVest .gAttr img').on('click', that.fnCloneVest);
	//		$('#garmentsVest .fbSelectBtn').on('click', that.fnCloneVest);
	//		$('#garmentsVest .gAttr .mycheck').on('click', that.fnCloneVest);
	//		$('#garmentsVest .textCloneBtn').on('click', that.fnCloneVest);
	//		$('#garmentsVest .selectlist').on('change', that.fnCloneVest);
                      
		};




		this.cloneVestAttributes = function( pvId, pvTA, cvI)
		{
			var tAD = that.getVariant(pvId)[pvTA];
			for (var ind in that.vestData) {
				if ( cvI.indexOf(that.vestData[ind].variantId.toString()) != -1) {
					//console.log('cloning to ' + that.suitData[ind].variantId);
					that.vestData[ind][pvTA] = tAD; 
				}
			}
			that.flushModel();
		};

		this.deleteVariantVest = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							
							var currentcount = orderItem.garmentsList()[3].count();
							orderItem.garmentsList()[3].count( currentcount - 1);
							
							var tvestData = [];
							var tvarsData = [];
							for (var ind in that.variantNameVest() ) {
								if ( that.variantNameVest()[ind].id != data.id ) {
									tvarsData.push(that.variantNameVest()[ind]);
									tvestData.push(that.getVariant( that.variantNameVest()[ind].id ));
								}
							}							
							that.vestData = tvestData;							
							that.flushModel();
							that.variantNameVest(tvarsData);
							that.selectedVariantVest(that.variantNameVest()[0]);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};
		
		this.deleteVariantVest2 = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							
							var currentcount = orderItem.garmentsList()[3].count();
							orderItem.garmentsList()[3].count( currentcount - 1);
							
							var tvestData = [];
							var tvarsData = [];
							for (var ind in that.variantNameVest() ) {
								if ( that.variantNameVest()[ind].id != data.id ) {
									tvarsData.push(that.variantNameVest()[ind]);
									tvestData.push(that.getVariant( that.variantNameVest()[ind].id ));
								}
								else{
									orderItem.removeFabric('Vest', 3, 'Vest ' + (that.vestData[ ind ].variantId + 1), that.vestData[ ind ].VestCustomerFabric, that.vestData[ ind ].variantId, 1);
								}
							}							
							that.vestData = tvestData;							
							that.flushModel();
							that.variantNameVest(tvarsData);
							that.selectedVariantVest(that.variantNameVest()[0]);
							
							
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "block";
							//posChangePage('#bodyshape');
							//posChangePage('#orderItemSelectGarment');
							orderItem.prepareGarments();
							orderItem.populateGarments();
							setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },2000);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};
		
		

		this.deleteSuitVests = function(suitTimestampID) { 
			var tVestData = [];
			var tvarsData = [];
			var extrascount = 0;
			for (var ind in that.variantNameVest() ) {
try{				
	console.log("parentSuitTimestampID " + that.getVariant( that.variantNameVest()[ind].id ).parentSuitTimestampID); 
}catch(e){
	;
}				
				if ( that.getVariant( that.variantNameVest()[ind].id ).parentSuitTimestampID != suitTimestampID ) {
					tvarsData.push(that.variantNameVest()[ind]);
					tVestData.push(that.getVariant( that.variantNameVest()[ind].id ));
				}else{
					extrascount++;
				}
			}
			var currentcount = orderItem.garmentsList()[3].count();
			orderItem.garmentsList()[3].count( currentcount - extrascount);
			that.vestData = tVestData; 
			that.flushModel();
			that.variantNameVest(tvarsData);
			that.selectedVariantVest(that.variantNameVest()[0]);
		};	
		
		
		this.addVariantVestFromSuit = function(suitindex, initializationByExtra) { 
			
			var currentcount = orderItem.garmentsList()[3].count();	
			orderItem.garmentsList()[3].count( currentcount + 1);
			this.vestDataAID += 1; 
			var newid = this.vestDataAID + 1;
			var vname = "Vest " + newid;
	
			var tObj = jQuery.extend(true, {}, this.getVariantFromSuit( suitindex )  ); //CLONE some Object options 
			 
//console.log("PANT DATA BEFORE: " + JSON.stringify(this.vestData));
			this.variantNameVest.push({title: vname, id: this.vestDataAID});
			tObj.variantId = this.vestDataAID;
			tObj.custom = new CustomProperties();
			if(initializationByExtra == true){				
				this.vestData = [];
			}
			this.initializationByExtra = false;
			this.vestData.push(tObj);                    //Push to internal		
			this.flushModel();
//console.log("PANT DATA AFTER: " + JSON.stringify(this.vestData));			
			var spinner = document.getElementById('loading_jp');
			spinner.style.display = "block";
			//posChangePage('#orderItemPopulateGarments');
			//posChangePage('#measurements');
			//orderItem.currentBigStep(orderItem.bigSteps()[5]);	// more correct way for page changing than the one above
			//orderItem.currentBigStep(orderItem.bigSteps()[6]);	// more correct way for page changing than the one above
			orderItem.populateGarments();
			orderItem.prepareGarments();
			customAlert("Vest added succesfully");
			setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },2200);
		//	this.currentStep(this.workflow()[0] );
		//	var lastelemindex = this.variantNameVest().length -1;
		//	this.selectedVariantVest( this.variantNameVest()[lastelemindex] );
		};		
		

		
		
		ko.bindingHandlers.vestCloneMonogram = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var values = valueAccessor();
					for(var x = 0; x < that.vestData.length; x++){
						that.vestData[ x ].vestMonogram = values[0];
						that.vestData[ x ].vestDefaultMonogramHeader = values[1];
						that.vestData[ x ].vestCustomMonogramHeader = values[2];
						//that.vestData[ x ].vestMonogramNotes = values[3];
						that.vestData[ x ].vestMonogramExtraLine = values[4];
						//that.vestData[ x ].vestMonogramStitchColour = values[5]; 
					}
		        });
		    }
		}; 	
		
		
				
		ko.bindingHandlers.modifiedVestListOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var wantedValue = value[1];	
					var list = document.getElementsByName(value[2]);
					var checkboxes = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;
					var elemdivs = document.getElementsByName(value[5]);

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						if(wantedValue.id != undefined){
							checkValue = wantedValue.id; 
						}else{
							checkValue = wantedValue;
						}	
						if(spanvalue == checkValue){
							checkboxes[ index ].classList.add('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('showtrigger');
								elemdivs[ index ].classList.remove('offtrigger');
							}	
						}else{
							checkboxes[ index ].classList.remove('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('offtrigger');
								elemdivs[ index ].classList.remove('showtrigger');
							}	
						}	
					}
					
					if(value[4] != 'none'){
						//var customdivs = document.getElementsByName(value[4]);
						//if(customdivs[value[0]] != undefined){
							if(wantedValue.id == '123'){
								//customdivs[value[0]].style.display = 'block';
								that.vestData[value[0]].custom.addCustomInfo(value[7]);
								that.vestData[value[0]].custom.custom0 = 100;
								that.customCount(that.customCount() + 1);
							}else{
								//customdivs[value[0]].style.display = 'none';
								if(that.vestData[value[0]].custom.removeCustomInfo(value[7])){
									that.customCount(that.customCount() - 1);
								}
							}
						//}	
					}
					
					if(value[6] != undefined && value[6] != 'none'){
						var mainoptionsdivs = document.getElementsByName(value[6]);
						var mainoptionscount = mainoptionsdivs.length;
						
						for(var a = 0; a < mainoptionscount; a++){
							var haschecked = false;
							for(var b = 0; b < garmentscount; b++){
								var index = a*garmentscount + b;
								if(checkboxes[index].classList.contains('selecteds')){
									haschecked = true;
									break;
								}
							}
							if(haschecked == true){
								mainoptionsdivs[a].classList.add('activate');
							}else{
								mainoptionsdivs[a].classList.remove('activate');
							}
						}
					}
					
					that.selectedVariantVest(that.variantNameVest()[value[0]]);
		        });
		    }
		};    

		ko.bindingHandlers.modifiedVestListBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( !checkboxes[ index ].classList.contains(value[3]) ){
							checkboxes[ index ].classList.add(value[3]);
							// for doubleBreastPocket only
							if(value[4] != undefined && value[4] != 'none'){
								document.getElementsByName(value[4])[index].classList.remove('invisible');
							}
						}else{
							checkboxes[ index ].classList.remove(value[3]);
							// for doubleBreastPocket only
							if(value[4] != undefined && value[4] != 'none'){
								document.getElementsByName(value[4])[index].classList.add('invisible');
							}						
						}	
					}
					
					if(value[4] != undefined && value[6] != 'none'){
						var mainoptionsdivs = document.getElementsByName(value[6]);
						var mainoptionscount = mainoptionsdivs.length;
						
						for(var a = 0; a < mainoptionscount; a++){
							var haschecked = false;
							for(var b = 0; b < garmentscount; b++){
								var index = a*garmentscount + b;
								if(checkboxes[index].classList.contains('selecteds')){
									haschecked = true;
									break;
								}
							}
							if(haschecked == true){
								mainoptionsdivs[a].classList.add('activate');
							}else{
								mainoptionsdivs[a].classList.remove('activate');
							}
						}
					}
					
					that.selectedVariantVest(that.variantNameVest()[value[0]]);					
		        });
		    }
		};   
		
		
		this.vestBreastPocketDependencies = function( value0, value1, theselectedclass, theopacityclass ){
						
			var VestBreastPockets = document.getElementsByName(value0);
			var VestDoubleBreastPocket = document.getElementsByName(value1);			
			for(var a = 0; a < VestBreastPockets.length; a++){
				if(VestBreastPockets[a].classList.contains(theselectedclass)){
					VestDoubleBreastPocket[a].classList.remove(theopacityclass);
				}else{
					VestDoubleBreastPocket[a].classList.add(theopacityclass);
				}
			}
							
			return false;
		};
		
		this.extraVestFabricDependencies = function(){
			var notExtra = false;			
			for(var x = 0; x < that.vestData.length; x++){
				if(that.vestData[ x ].parentSuitTimestampID == ''){
					notExtra = true;
					break
				}	
			}
			return notExtra;
		};
		


		ko.bindingHandlers.modifiedVestBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var checkboxes = document.getElementsByName(value[1]);
					var index = value[0];
					var cssclass = "" + value[2];

					if( !checkboxes[ index ].classList.contains(cssclass) ){
						checkboxes[ index ].classList.add(cssclass);
					}else{
						checkboxes[ index ].classList.remove(cssclass);
					}	
					if( value[1] != 'FabricCheck' ){
						that.selectedVariantVest(that.variantNameVest()[value[0]]);
					}	
		        });
		    }
		};


		ko.bindingHandlers.switchpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);

					for(var x = 0; x < popups.length; x++){
						if(x != value[0]){
							popups[x].style.display = "none";	
						}
					}
					if(popups[value[0]].style.display == "block"){
						popups[value[0]].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}else{
						popups[value[0]].style.display = "block";		
					}

		        });
		    }
		}; 
		

		ko.bindingHandlers.vestswitchcustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantVest(that.variantNameVest()[value[0]]);
						
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.vestData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		}; 		

		ko.bindingHandlers.vestopencustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantVest(that.variantNameVest()[value[0]]);
						
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.vestData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					popup[0].style.display = "block";		
		        });
		    }
		}; 			
		

		ko.bindingHandlers.openpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);			
					popups[value[0]].style.display = "block";		//.show(), .dialog( "open" ) : not working
		        });
		    }
		}; 

		ko.bindingHandlers.closepopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[0]);
					for(var x = 0; x < popups.length; x++){			
						popups[x].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}	
		        });
		    }
		}; 


		ko.bindingHandlers.modifiedVestThreads = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var images = document.getElementsByName(value[3]);
					var texts = document.getElementsByName(value[3] + "Text");
					var options = document.getElementsByName(value[4]);
					var options2 = document.getElementsByName(value[5]);
					images[value[0]].src = value[2].image;
					if(texts[value[0]] != undefined){
						texts[value[0]].innerHTML = value[2].title;
					}	

					var garmentssize = options.length/images.length;
					for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');
						try{
							options2[x].classList.remove('selected');
						}catch(e){ ; }		
					}
					options[garmentssize*value[0] + value[1]].classList.add('selected');
					try{
						options2[garmentssize*value[0] + value[1]].classList.add('selected');
					}catch(e){ ; }	

					that.selectedVariantVest(that.variantNameVest()[value[0]]);
					/*
					if(value[5] != undefined && value[6] != undefined && value[7] != undefined && value[8] != undefined){
						if(that.VestInsidePocketsDifferentFromPiping() == false){
							var images2 = document.getElementsByName(value[5]);
							var options2 = document.getElementsByName(value[6]);
							images2[value[0]].src = value[2].image;
							var garmentssize = options2.length/images2.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options2[x].classList.remove('selected');	
							}
							options2[garmentssize*value[0] + value[1]].classList.add('selected');
						}
						if(that.VestMonogramStitchDifferentFromPiping() == false){
							var images3 = document.getElementsByName(value[7]);
							var options3 = document.getElementsByName(value[8]);
							images3[value[0]].src = value[2].image;
							var garmentssize = options3.length/images3.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options3[x].classList.remove('selected');	
							}
							options3[garmentssize*value[0] + value[1]].classList.add('selected');						
						}		
					}
					*/
		        });
		    }
		}; 
		
		
		ko.bindingHandlers.modifiedVestColors = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var listdivs = document.getElementsByName(value[3]);					
					var listimages = document.getElementsByName(value[4]);
					var listtexts = document.getElementsByName(value[5]);
					var options = document.getElementsByName(value[6]);
					var garmentssize = document.getElementById(value[7]).innerHTML;
					var vestoptionsnumber = listdivs.length/garmentssize;
					var coloroptionsnumber = options.length/garmentssize;
					
					for(var x = 0; x < vestoptionsnumber; x++){
						var y = value[0] + x*garmentssize;
						if(listimages.length > 0){	 				
							listimages[y].src = value[2].image;
						}
						if(listtexts.length > 0){
							listtexts[y].innerHTML = value[2].title;
						}
					}					
					for(var x = coloroptionsnumber*value[0]; x < coloroptionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[coloroptionsnumber*value[0] + value[1]].classList.add('selected');
		        });
		    }
		};		
		
		
		ko.bindingHandlers.modifiedVestFit = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					for(var x = 0; x < that.vestData.length; x++){
						that.vestData[ x ].vestFit = value;	
					}
					that.flushModel();
		        });
		    }
		}; 		
		

		ko.bindingHandlers.modifiedVestDropdown = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					that.selectedVariantVest(that.variantNameVest()[value[0]]);
		        });
		    }
		}; 
		
		
		ko.bindingHandlers.vestslider = {
		    init: function(element, valueAccessor, allBindingsAccessor) {
		        //initialize the control
		        var options = allBindingsAccessor().vestslideroptions || {};
		        $(element).slider(options);
		
		        //handle the value changing in the UI
		        ko.utils.registerEventHandler(element, "slidechange", function() {
		            //would need to do some more work here, if you want to bind against non-observables
		            var values = valueAccessor(); 
		            var observable = values[0];
		            observable($(element).slider("value"));
		        });
		
		    },
		    //handle the model value changing
		    update: function(element, valueAccessor) {
		    	var values = valueAccessor();
		        var value = ko.utils.unwrapObservable(values[0]);
		       // $(element).slider("value", value);
				var maindivs = document.getElementsByName(values[1]);
				var alltexts = document.getElementsByName(values[2]);
				var garmentscount = alltexts.length/maindivs.length;
				var optionscount = maindivs.length;
				
				for(var a = 0; a < maindivs.length; a++){
					for(var b = 0; b < garmentscount; b++){
						var index = a*garmentscount + b;						
						alltexts[index].innerHTML = that.vestData[ b ].VestLapelWidth; 
					}	
				}
				
				try{
					$(element).val(value).slider('refresh');
				}catch(e){
					;
				} 

		    }
		};		
		
		
		
		ko.bindingHandlers.modifyClassOfDiv = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {	        	  		        	      	
					var value = valueAccessor();
					var index = value[0];
					var divs = document.getElementsByName(value[1]);
					var classname = 'activated'; //value[2];
					var activeclassname = 'activate';//value[3];
					//var notactiveclassname = value[4];
					
					var addtheclass = true;
					if( divs[ index ].classList.contains(classname) ){
						addtheclass = false;
					}
					for(var x = 0; x < divs.length; x++){
						divs[ x ].classList.remove(classname);
					}		
					if(addtheclass == true){			
						divs[ index ].classList.add(classname);
					}	
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.vestFabricApplier = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
			//		for(var x = 0; x < value.length; x++){
			//			console.log("VALUE " + x + ": " + JSON.stringify(value[x]) );
			//		}
					
					var checkboxes = document.getElementsByName(value[0]);
					var texts = document.getElementsByName(value[3]);
					var images = document.getElementsByName(value[4]);
					var cssclass = "" + value[2]; 
					var tomakegreenelements = document.getElementsByClassName("tomakegreen");
					
					var garmentsnumber = checkboxes.length;
					var textsnumber = texts.length;
					var differenttypesnumber = textsnumber/garmentsnumber;

					var anychecked = false;
					
					for(var x = 0; x < checkboxes.length; x++){
						if(checkboxes[x].classList.contains(cssclass)){
							orderItem.removeFabric('Vest', 3, 'Vest ' + (x + 1), that.vestData[ x ].VestCustomerFabric, that.vestData[ x ].variantId, 1);
							
							if(!orderItem.ClientFabric() && document.getElementById("selectedfabric").innerHTML != "none"){
								checkboxes[x].classList.remove(cssclass);
								tomakegreenelements[x].classList.add('makegreen');
								anychecked = true;
								that.vestData[ x ].vestFabric = value[1];
								that.vestData[ x ].VestCustomerFabric = {};
								that.vestData[ x ].VestHasCustomerFabric = false;
								if(texts.length > 0){
									//texts[x].innerHTML = value[1].title;
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = value[1].title;
											texts[pos].style.display = "block";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = '';
											texts[pos+1].style.display = "none";
										}
									}
								}
								if(images.length > 0){
									images[x].src = value[1].fabricImage;
								}
							}
							else if(orderItem.ClientFabric()){
								if(orderItem.addFabricToGarment(that.vestData[ x ], 'Vest', 3,'Vest ' + (x + 1), 1)){
									checkboxes[x].classList.remove(cssclass);
									tomakegreenelements[x].classList.add('makegreen');
									anychecked = true;
									that.vestData[ x ].vestFabric = dsRegistry.getDatasource('fabricsDS').getStore()[1];
									that.vestData[ x ].VestCustomerFabric = orderItem.SelectedCustomerFabric();
									that.vestData[ x ].VestHasCustomerFabric = true;
									
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = '';
											texts[pos].style.display = "none";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = orderItem.SelectedCustomerFabric().FabricCode;
											texts[pos+1].style.display = "block";
										}
									}
								}
								else if(that.vestData[ x ].VestCustomerFabric.FabricCode){
									var temp = orderItem.SelectedCustomerFabric();
									orderItem.SelectedCustomerFabric(that.vestData[ x ].VestCustomerFabric);
									orderItem.addFabricToGarment(that.vestData[ x ], 'Vest', 3,'Vest ' + (x + 1), 1);
									orderItem.SelectedCustomerFabric(temp);
								}
							}
						}
							
					}					
					if(anychecked == true){
						document.getElementById("selectedfabric").innerHTML = "none";
						document.getElementById("fabricrange").innerHTML = "0";
						document.getElementById("composition").innerHTML = "0";
						document.getElementById("fabricinput").value = "";
						document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
						document.getElementById("fabricinfo").style.display = "none";
						orderItem.ClientFabric(false);
						orderItem.SelectedCustomerFabric({});
					}
		        });
		    }
		}; 
		
	
	/*
		ko.bindingHandlers.vestFabricApplier = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					for(var x = 0; x < value.length; x++){
						console.log("VALUE " + x + ": " + value[x]);
					}
					var elems = document.getElementsByName(value[0]);
		        });
		    }
		}; 		
	*/	
				
	},

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
		/*
	nextStep: function() {
		var tWork = this.workflow();
		for (var ind in tWork ) {
				if (!tWork[ind].completed) {
				this.currentStep( tWork[ind] );
				return;
			}
		}
	},*/

	flushModel: function() {		
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"vest"  : this.vestData
		}, true);
	},
	
	selectVariantVest: function() {
		this.renderVest();
	},
	

	getRow: function(id) {
		for (var ind in this.vestData) {
			if ( this.vestData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	
	getVariant: function(id) {
		var toreturn = this.vestData[0];
		for (var ind in this.vestData) {
			if ( this.vestData[ind].variantId == id  ) {
				toreturn = this.vestData[ind];
			}
		}
		return toreturn;
		
	},	
	
	
	getVariantFromSuit: function(index) {
		 // CODE FOR GETTING VEST DATA FROM SUIT		
		var df = this.dsRegistry.getDatasource('fabricsDS').getStore()[1];
		var obj = {};
		obj.variantId         = 0;	// this will change by the caller function
		obj.parentSuitTimestampID = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].timestampID;	// set parent suit id
		obj.VestNotes	= '';
		obj.VestFabricNotes = '';
		obj.VestFitAndLapelNotes = '';
		obj.VestBottomNotes = '';
		obj.VestButtonsNotes = '';
		obj.VestPocketsNotes = '';
		obj.VestBackStyleNotes = '';
		obj.VestContrastNotes = '';
		obj.vestCustomButtonsNotes	= '';
		obj.vestCustomButtonsImage	= '';
		obj.vestCustomLapelNotes	= '';
		obj.vestCustomLapelImage	= '';
		obj.vestCustomDesignNotes	= ''; 
		obj.vestCustomDesignImage	= '';		
		obj.vestFabric        = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].suitFabric;
		obj.vestDesign 	   = this.dsRegistry.getDatasource('vestDesignDS').getStore()[0];
		obj.vestNecklineStyle = this.dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0];
		//	obj.vestPocketStyle   = dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0];
		obj.vestButtonNumber  = this.dsRegistry.getDatasource('vestButtonNumberDS').getStore()[0];
		obj.vestPocketStyle   = this.dsRegistry.getDatasource('vestPocketStyleDS').getStore()[0];
		obj.vestBottomStyle   = this.dsRegistry.getDatasource('vestBottomStyleDS').getStore()[0];
		obj.vestBackStyle   = this.dsRegistry.getDatasource('vestBackStyleDS').getStore()[0];
		obj.vestContrastPocket = false;
		obj.vestContrastFabric = df;
		
		obj.vestPiping  = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].suitJacketPiping;
		obj.vestLining  = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].suitJacketLining;
		obj.vestMonogram  = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketMonogram;				// 9a
		obj.vestMonogramExtraLine        = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketMonogramExtraLine;				
		obj.vestDefaultMonogramHeader  = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketDefaultMonogramHeader;		// 9b
		obj.vestCustomMonogramHeader  = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketCustomMonogramHeader;	// 9b
		obj.vestMonogramStitchDifferentFromPiping = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketMonogramStitchDifferentFromPiping;	// 9d
		obj.vestMonogramStitchColour = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketMonogramStitchColour;//'';		// 9d
		obj.vestCustomMonogram			= this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketCustomMonogram;	// 9 optional A
		obj.vestCustomMonogramImage = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketCustomMonogramImage;			// 9 optional A
		obj.vestCustomMonogramNotes = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].JacketCustomMonogramNotes;			// 9 optional A
		obj.vestButtons = this.dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
		obj.vestThreads = this.dsRegistry.getDatasource('vestThreadsDS').getStore()[0];
		obj.vestFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2];
		obj.VestBreastPocket = false;
		obj.VestDoubleBreastPocket = false;
		obj.VestCustomerFabric = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].SuitCustomerFabric;
		obj.VestHasCustomerFabric = this.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[index].SuitHasCustomerFabric;
		obj.GarmentPrice = 0;

		obj.urgent = false;
		//	obj.urgentDate = '';
		obj.DOP_day = '';
		obj.DOP_month = '';
		obj.DOP_date = '';

		return obj;

	},	

	getRow: function(id) {
		for (var ind in this.vestData) {
			if ( this.vestData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},

	
	
	
	addVariantVest: function() {
		//var vname = "Vest " + (this.vestDataAID+1);this.
		this.vestDataAID += 1;
		var newid = this.vestDataAID + 1;
		var vname = "Vest " + newid; 
		
		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantVest().id )  ); //CLONE Object
		this.variantNameVest.push({title: vname, id: this.vestDataAID});
		if(tObj.parentSuitTimestampID == undefined){
			tObj.parentSuitTimestampID = '';
		}
		tObj.custom = new CustomProperties();
		tObj.variantId = this.vestDataAID;
		if(orderItem.vestfirsttime == true){
			this.vestData.push(tObj);                                                       //Push to internal
		}	
		this.flushModel();
	},
	
	
	addVariantVest2: function() {
		
		var currentcount = orderItem.garmentsList()[3].count();	
		orderItem.garmentsList()[3].count( currentcount + 1);
		
		//var vname = "Vest " + (this.vestDataAID+1);
		this.vestDataAID += 1;
		var newid = this.vestDataAID + 1;
		var vname = "Vest " + newid;
		
		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantVest().id )  ); //CLONE Object
		this.variantNameVest.push({title: vname, id: this.vestDataAID});

		if(tObj.parentSuitTimestampID == undefined){
			tObj.parentSuitTimestampID = '';
		}
		tObj.variantId = this.vestDataAID;
		tObj.custom = new CustomProperties();
		this.vestData.push(tObj);                                                       //Push to internal
		this.flushModel();
		this.currentStep(this.workflow()[0] );
		var lastelemindex = this.variantNameVest().length -1;
		this.selectedVariantVest( this.variantNameVest()[lastelemindex] );
	},


	digestData: function(data) {
		this.vestData  = data.vest;
		this.renderView();

	},

	renderView: function() {
		this.renderVest();
	},
	
	renderVest: function() {
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantVest().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.VestNotes)    != "undefined") {this.VestNotes(tData.VestNotes);}
				if (typeof(tData.VestFabricNotes)    	!= "undefined") {this.VestFabricNotes(tData.VestFabricNotes);}
				if (typeof(tData.VestFitAndLapelNotes)  != "undefined") {this.VestFitAndLapelNotes(tData.VestFitAndLapelNotes);}
				if (typeof(tData.VestBottomNotes)   	!= "undefined") {this.VestBottomNotes(tData.VestBottomNotes);}
				if (typeof(tData.VestButtonsNotes)    	!= "undefined") {this.VestButtonsNotes(tData.VestButtonsNotes);}
				if (typeof(tData.VestPocketsNotes)    	!= "undefined") {this.VestPocketsNotes(tData.VestPocketsNotes);}
				if (typeof(tData.VestBackStyleNotes)    != "undefined") {this.VestBackStyleNotes(tData.VestBackStyleNotes);}
				if (typeof(tData.VestContrastNotes)    != "undefined") {this.VestContrastNotes(tData.VestContrastNotes);}
				
				if (typeof(tData.vestFabric)         != "undefined") {this.vestFabric(tData.vestFabric);}
				if (typeof(tData.vestCustomButtonsNotes)    != "undefined") {this.vestCustomButtonsNotes(tData.vestCustomButtonsNotes);}
				if (typeof(tData.vestCustomButtonsImage)    != "undefined") {this.vestCustomButtonsImage(tData.vestCustomButtonsImage);}
				if (typeof(tData.vestCustomLapelNotes)    != "undefined") {this.vestCustomLapelNotes(tData.vestCustomLapelNotes);}
				if (typeof(tData.vestCustomLapelImage)    != "undefined") {this.vestCustomLapelImage(tData.vestCustomLapelImage);}
				if (typeof(tData.vestDesign)  		 != "undefined") {this.vestDesign(tData.vestDesign);}
				if (typeof(tData.vestCustomDesignNotes)    != "undefined") {this.vestCustomDesignNotes(tData.vestCustomDesignNotes);}
				if (typeof(tData.vestCustomDesignImage)    != "undefined") {this.vestCustomDesignImage(tData.vestCustomDesignImage);}
				if (typeof(tData.vestNecklineStyle)  != "undefined") {this.vestNecklineStyle(tData.vestNecklineStyle);}
				if (typeof(tData.vestButtonNumber)   != "undefined") {this.vestButtonNumber(tData.vestButtonNumber);}
				if (typeof(tData.vestPocketStyle)    != "undefined") {this.vestPocketStyle(tData.vestPocketStyle);}
				if (typeof(tData.vestBottomStyle)    != "undefined") {this.vestBottomStyle(tData.vestBottomStyle);}
				if (typeof(tData.vestContrastPocket) != "undefined") {this.vestContrastPocket(tData.vestContrastPocket);}
				if (typeof(tData.vestContrastFabric) != "undefined") {this.vestContrastFabric(tData.vestContrastFabric);}
				if (typeof(tData.vestBackStyle)      != "undefined") {this.vestBackStyle(tData.vestBackStyle);}
				if (typeof(tData.vestPiping) 		!= "undefined") {this.vestPiping(tData.vestPiping);}
				if (typeof(tData.vestLining) 		!= "undefined") {this.vestLining(tData.vestLining);}
				if (typeof(tData.vestMonogram)         		!= "undefined") {this.vestMonogram(tData.vestMonogram);}
				if (typeof(tData.vestMonogramExtraLine)         		!= "undefined") {this.vestMonogramExtraLine(tData.vestMonogramExtraLine);}
				if (typeof(tData.vestDefaultMonogramHeader)   != "undefined") {this.vestDefaultMonogramHeader(tData.vestDefaultMonogramHeader);}
				if (typeof(tData.vestCustomMonogramHeader)    != "undefined") {this.vestCustomMonogramHeader(tData.vestCustomMonogramHeader);}
				if (typeof(tData.vestMonogramStitchDifferentFromPiping) != "undefined") {this.vestMonogramStitchDifferentFromPiping(tData.vestMonogramStitchDifferentFromPiping);}
				if (typeof(tData.vestMonogramStitchColour)	!= "undefined") {this.vestMonogramStitchColour(tData.vestMonogramStitchColour);}
				if (typeof(tData.vestMonogramNotes)    	!= "undefined") {this.vestMonogramNotes(tData.vestMonogramNotes);}
				if (typeof(tData.vestCustomMonogram)          != "undefined") {this.vestCustomMonogram(tData.vestCustomMonogram);}
				if (typeof(tData.vestCustomMonogramImage)     != "undefined") {this.vestCustomMonogramImage(tData.vestCustomMonogramImage);}
				if (typeof(tData.vestCustomMonogramNotes)     != "undefined") {this.vestCustomMonogramNotes(tData.vestCustomMonogramNotes);}
				if (typeof(tData.vestButtons) 		!= "undefined") {this.vestButtons(tData.vestButtons);}
				if (typeof(tData.vestThreads) 		!= "undefined") {this.vestThreads(tData.vestThreads);}
				if (typeof(tData.vestFit) 			!= "undefined") {this.vestFit(tData.vestFit);}
				if (typeof(tData.VestBreastPocket) 	!= "undefined") {this.VestBreastPocket(tData.VestBreastPocket);}
				if (typeof(tData.VestDoubleBreastPocket) 	!= "undefined") {this.VestDoubleBreastPocket(tData.VestDoubleBreastPocket);}
				
				if (typeof(tData.urgent) 	!= "undefined") {this.urgent(tData.urgent);}
				//if (typeof(tData.urgentDate) 	!= "undefined") {this.urgentDate(tData.urgentDate);}
				if (typeof(tData.DOP_day)		!= "undefined") {this.DOP_day(tData.DOP_day);}
				if (typeof(tData.DOP_month)		!= "undefined") {this.DOP_month(tData.DOP_month);}
				if (typeof(tData.DOP_date)		!= "undefined") {this.DOP_date(tData.DOP_date);}
				
				
			//this.vestContrastFabricSelect();
	            if ( this.currentStep().id == 0) this.vestFabricSelect();
		        if ( this.currentStep().id == 4) this.vestContrastFabricSelect();
			}
		}catch(e){
			;
		}		
}
	
	

});

defVest = SimpleDatasource.extend({
	init: function(name, dsRegistry, olddata, count) {
		if(olddata == null || olddata == undefined){
			var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
			var vest = {};
			vest.variantId         = 0;
			vest.parentSuitTimestampID = '';
			vest.VestNotes	= '';
			vest.VestFabricNotes = '';
			vest.VestFitAndLapelNotes = '';
			vest.VestBottomNotes = '';
			vest.VestButtonsNotes = '';
			vest.VestPocketsNotes = '';
			vest.VestBackStyleNotes = '';
			vest.VestContrastNotes = '';
			vest.vestCustomButtonsNotes	= '';
			vest.vestCustomButtonsImage	= '';
			vest.vestCustomLapelNotes	= '';
			vest.vestCustomLapelImage	= '';
			vest.vestCustomDesignNotes	= ''; 
			vest.vestCustomDesignImage	= '';		
			vest.vestFabric        = df;
			vest.vestDesign 	   = dsRegistry.getDatasource('vestDesignDS').getStore()[0];
			vest.vestNecklineStyle = dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0];
		//	vest.vestPocketStyle   = dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0];
			vest.vestButtonNumber  = dsRegistry.getDatasource('vestButtonNumberDS').getStore()[0];
			vest.vestPocketStyle   = dsRegistry.getDatasource('vestPocketStyleDS').getStore()[0];
			vest.vestBottomStyle   = dsRegistry.getDatasource('vestBottomStyleDS').getStore()[0];
			vest.vestBackStyle   = dsRegistry.getDatasource('vestBackStyleDS').getStore()[0];
			vest.vestContrastPocket = false;
			vest.vestContrastFabric = df;
			
			vest.vestPiping  = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			vest.vestLining  = dsRegistry.getDatasource('liningFabricDS').getStore()[0];
			
			vest.vestMonogram        = '';				// 9a
			vest.vestMonogramNotes			= '';
			vest.vestMonogramExtraLine        = '';				
			vest.vestDefaultMonogramHeader  = true;		// 9b
			vest.vestCustomMonogramHeader  = '';	// 9b
			vest.vestMonogramStitchDifferentFromPiping = false;	// 9d
			vest.vestMonogramStitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';		// 9d
			vest.vestCustomMonogram			= false;	// 9 optional A
			vest.vestCustomMonogramImage = '';			// 9 optional A
			vest.vestCustomMonogramNotes = '';			// 9 optional A
			
			vest.vestButtons = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			vest.vestThreads = dsRegistry.getDatasource('vestThreadsDS').getStore()[0];
			vest.vestFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2];
			vest.custom     =    new CustomProperties();
			vest.VestBreastPocket = false;
			vest.VestDoubleBreastPocket = false;
			vest.VestCustomerFabric = {};
			vest.VestHasCustomerFabric = false;
			vest.GarmentPrice = 0;
			
			vest.urgent = false;
		//	vest.urgentDate = '';
			vest.DOP_day = '';
			vest.DOP_month = '';
			vest.DOP_date = '';
	
			var iVest = {
				vest: []
			};
			
			iVest.vest.push(vest);
			this._super(name, iVest, dsRegistry);
		}else{
			var iVest = {
				vest: []
			};		
			if(olddata.length > count){
				for(var x = count; x < olddata.length; x++){
					orderItem.removeFabric('Vest', 3, 'Vest ' + (x + 1), olddata[ x ].VestCustomerFabric, olddata[ x ].variantId, 1);
				}
			}
			for(var x = 0; x < count; x++){
				
				if(olddata[x] != undefined){
					var vest = {};
					vest.variantId         = x;
					vest.parentSuitTimestampID = olddata[x].parentSuitTimestampID;
					vest.VestNotes	= olddata[x].VestNotes;
					vest.VestFabricNotes = olddata[x].VestFabricNotes;
					vest.VestFitAndLapelNotes = olddata[x].VestFitAndLapelNotes;
					vest.VestBottomNotes = olddata[x].VestBottomNotes;
					vest.VestButtonsNotes = olddata[x].VestButtonsNotes;
					vest.VestPocketsNotes = olddata[x].VestPocketsNotes;
					vest.VestBackStyleNotes = olddata[x].VestBackStyleNotes;
					vest.VestContrastNotes = olddata[x].VestContrastNotes;
					vest.vestCustomButtonsNotes	= olddata[x].vestCustomButtonsNotes;
					vest.vestCustomButtonsImage	= olddata[x].vestCustomButtonsImage;
					vest.vestCustomLapelNotes	= olddata[x].vestCustomLapelNotes;
					vest.vestCustomLapelImage	= olddata[x].vestCustomLapelImage;
					vest.vestCustomDesignNotes	= olddata[x].vestCustomDesignNotes;
					vest.vestCustomDesignImage	= olddata[x].vestCustomDesignImage;
					vest.vestFabric        = olddata[x].vestFabric;
					vest.vestDesign 	   = olddata[x].vestDesign;
					vest.vestNecklineStyle = olddata[x].vestNecklineStyle;
				//	vest.vestPocketStyle   = olddata[x].vestPocketStyle;
					vest.vestButtonNumber  = olddata[x].vestButtonNumber;
					vest.vestPocketStyle   = olddata[x].vestPocketStyle;
					vest.vestBottomStyle   = olddata[x].vestBottomStyle;
					vest.vestBackStyle   = olddata[x].vestBackStyle;
					vest.vestContrastPocket = olddata[x].vestContrastPocket;
					vest.vestContrastFabric = olddata[x].vestContrastFabric;
					
					vest.vestPiping  = olddata[x].vestPiping;
					vest.vestLining  = olddata[x].vestLining;
					
					vest.vestMonogramNotes			= olddata[x].vestMonogramNotes;
					vest.vestMonogram        			= olddata[x].suitvestMonogram;
					vest.vestMonogramExtraLine        			= olddata[x].suitvestMonogramExtraLine;
					vest.vestDefaultMonogramHeader  	= olddata[x].suitvestDefaultMonogramHeader;
					vest.vestCustomMonogramHeader  	= olddata[x].suitvestCustomMonogramHeader;
					vest.vestMonogramStitchDifferentFromPiping = olddata[x].suitvestMonogramStitchDifferentFromPiping;
					vest.vestMonogramStitchColour 	= olddata[x].suitvestMonogramStitchColour;
					
					vest.vestCustomMonogram			= olddata[x].suitvestCustomMonogram;
					vest.vestCustomMonogramImage 		= olddata[x].suitvestCustomMonogramImage;
					vest.vestCustomMonogramNotes 		= olddata[x].suitvestCustomMonogramNotes;
					
					vest.VestCustomerFabric = olddata[x].VestCustomerFabric;
					vest.VestHasCustomerFabric = olddata[x].VestHasCustomerFabric;
					vest.GarmentPrice = olddata[x].GarmentPrice;
					
					vest.vestButtons = olddata[x].vestButtons;
					vest.vestThreads = olddata[x].vestThreads;
					vest.vestFit = olddata[x].vestFit;
					vest.VestBreastPocket = olddata[x].VestBreastPocket;
					vest.VestDoubleBreastPocket = olddata[x].VestDoubleBreastPocket;		
					if(olddata[x].custom){
						vest.custom      		= olddata[x].custom;
					}
					else{
						vest.custom     =    new CustomProperties();
					}
					
					vest.urgent = olddata[x].urgent;
				//	vest.urgentDate = olddata[x].urgentDate;
					vest.DOP_day = olddata[x].DOP_day;
					vest.DOP_month = olddata[x].DOP_month;
					vest.DOP_date = olddata[x].DOP_date;
										
					iVest.vest.push(vest);
					
				}else{
					
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var vest = {};
					vest.variantId         = x;
					vest.parentSuitTimestampID = '';
					vest.VestNotes	= '';
					vest.VestFabricNotes = '';
					vest.VestFitAndLapelNotes = '';
					vest.VestBottomNotes = '';
					vest.VestButtonsNotes = '';
					vest.VestPocketsNotes = '';
					vest.VestBackStyleNotes = '';
					vest.VestContrastNotes = '';
					vest.vestCustomButtonsNotes	= '';
					vest.vestCustomButtonsImage	= '';
					vest.vestCustomLapelNotes	= '';
					vest.vestCustomLapelImage	= '';
					vest.vestCustomDesignNotes	= '';
					vest.vestCustomDesignImage	= '';		
					vest.vestFabric        = df;
					vest.vestDesign 	   = dsRegistry.getDatasource('vestDesignDS').getStore()[0];
					vest.vestNecklineStyle = dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0];
				//	vest.vestPocketStyle   = dsRegistry.getDatasource('vestLapelStyleDS').getStore()[0];
					vest.vestButtonNumber  = dsRegistry.getDatasource('vestButtonNumberDS').getStore()[0];
					vest.vestPocketStyle   = dsRegistry.getDatasource('vestPocketStyleDS').getStore()[0];
					vest.vestBottomStyle   = dsRegistry.getDatasource('vestBottomStyleDS').getStore()[0];
					vest.vestBackStyle   = dsRegistry.getDatasource('vestBackStyleDS').getStore()[0];
					vest.vestContrastPocket = false;
					vest.vestContrastFabric = df;
					
					vest.vestPiping  = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					vest.vestLining  = dsRegistry.getDatasource('liningFabricDS').getStore()[0];
					vest.vestMonogramNotes			= '';
					vest.vestMonogram        = '';				// 9a
					vest.vestMonogramExtraLine        = '';
					vest.vestDefaultMonogramHeader  = true;		// 9b
					vest.vestCustomMonogramHeader  = '';	// 9b
					vest.vestMonogramStitchDifferentFromPiping = false;	// 9d
					vest.vestMonogramStitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';		// 9d
					vest.vestCustomMonogram			= false;	// 9 optional A
					vest.vestCustomMonogramImage = '';			// 9 optional A
					vest.vestCustomMonogramNotes = '';			// 9 optional A
					
					vest.vestButtons = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					vest.vestThreads = dsRegistry.getDatasource('vestThreadsDS').getStore()[0];
					vest.vestFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2];
					vest.VestBreastPocket = false;
					vest.VestDoubleBreastPocket = false;	
					vest.VestCustomerFabric = {};
					vest.VestHasCustomerFabric = false;
					vest.GarmentPrice = 0;
						
					vest.urgent = 0;
				//	vest.urgentDate = '';
					vest.DOP_day = '';
					vest.DOP_month = '';	
					vest.DOP_date = '';	
					vest.custom     =    new CustomProperties();
					iVest.vest.push(vest);			
				}	
			}
			this._super(name, iVest, dsRegistry);
		}	
	}
});


//END DEFINE CLOSURE
});










