define(['jquery', 'knockout', 'base',
        'model/image3D/Image3D_v1', 'model/image3D/Image3D_v2', 'viewmodel/ColorsPopUpTemplate', 'viewmodel/ImageTagging'],
    function ($, ko) {

    GarmentsDesignStepVM = SimpleControl.extend({

        init: function (customer, designs, ordersVM) {
            var self = this;

            this.customer = customer;
            this.designs = designs;

            this.ordersVM = ordersVM;

            this.selectedGarment = ko.observable("");
            this.selectedCategory = ko.observable(false);

            this.isCopyDesignPopUpVisible = ko.observable(false);
            this.isRecoverDesignPopUpVisible = ko.observable(false);

            // garment who will be copied to other garments
            // This is a temporary variable
            this.garmentToCopyFrom = ko.observable(false);
            this.witchGarmentRecoverDesignPopUpVisible = ko.observable(false);


            this.copyToManyPopUp = ko.observable(false);

            console.log("Init garments Desgin Step VM");

            this.hasCustomDesign = this.setHasCustomDesign();
            this.anyGarmentHasCustomDesign = this.setAnyGarmentHasCustomDesign();
            this.customTagsHandlers = this._setCustomTagsHandlers();

            // dynamics function that can be changed from VIEW
            this.copyFromSuitFunction = null;

            // class to help colors popup , so we can avoid create a popup to each options.
            // This way we can build the popup via es6 and increase speed of page initialization
            this.colorsPopUpTemplate = null; // instance of ColorsPopUpTemplate

            window.debug_garmentDesignStep = this;
        },


        /**
         * Gets the garment JSON and remove all ko object. Then remove unnecessary data for the ERP
         * (maybe only the IDs will remain)
         *
         * For performance proposers we will get necessary data manually without any external library
         */
        getSubmissionData: function () {

            const designData = [];

            for (const garments_type of this.customer.garmentsStep.garments()) {
                const new_current_garments = Object.assign([], garments_type.garments()); // shallow copy

                // copy deeper level of object
                for (const garment_index in new_current_garments) {
                    const garment = new_current_garments[garment_index];

                    // get only necessary data
                    const current_garment = {
                        categories             : Object.assign([], garment.categories),
                        garment_id             : garment.garment_id,
                        garment_name           : garment.garment_name,
                        unique_id              : garment.unique_id,
                        selectedFabric         : garment.selectedFabric(),
                        isUrgent               : garment.isUrgent(),
                        urgentDate             : garment.urgentDate(),
                        useCustomerFabric      : garment.useCustomerFabric(),
                        customerFabricImage    : ko.mapping.toJS(garment.customerFabricImage()),
                        customerFabricMeterage : garment.customerFabricMeterage(),

                    };


                    // get only necessary data
                    if (typeof current_garment.selectedFabric !== "undefined"){
                        current_garment.selectedFabric = {
                            id: current_garment.selectedFabric.id,
                            tittle: current_garment.selectedFabric.tittle,
                        };
                    }

                    // copy deeper level of object
                    for (const category_index in current_garment.categories) {
                        const category = current_garment.categories[category_index];

                        // get only necessary data
                        const new_category = {
                            id         : category.id,
                            name       : category.name,
                            garment_id : category.garment_id,
                            options    : Object.assign([], category.options),
                        }

                        // copy deeper level of object
                        for (const option_index in new_category.options) {
                            const option = new_category.options[option_index];

                            // get only necessary data
                            const new_option = {
                                cat_id        : option.cat_id,
                                id            : option.id,
                                selectedValue : option.selectedValue ? ko.mapping.toJS(option.selectedValue) : false,
                                name          : option.name,
                            }

                            // replace KO-object by JS-object -> replace reference
                            new_category.options[option_index] = new_option;

                        }

                        // replace KO-object by JS-object -> replace reference
                        current_garment.categories[category_index] = new_category;
                    }

                    // replace KO-object by JS-object -> replace reference
                    new_current_garments[garment_index] = current_garment;

                }

                designData.push({
                    garments : new_current_garments,
                    id       : garments_type.id,
                    image    : garments_type.image,
                    name     : garments_type.name,
                });
            }

            return designData;
        },

        /**
         * what to do when a customer fabric is (de)selected
         * -> set custom fabric to selected value
         * -> ask for a customer fabric picture
         * -> in case of shirt, add extra cost to editable cost
         *
         * @param new_value boolean
         * @param garment
         */
        _useCustomerFabricEvent (new_value, garment ) {
            console.log("VALUE", new_value);
            const SHIRT_EXTRA_COST_DESCRIPTION = 'Shirt CUSTOM FABRIC';

            if(new_value) // if using customer fabric
            {
                // sets the garment fabric as CUSTOMERFABRIC (id == 6470)
                const customFabric = fabricsData.store.filter(el => el.id == '6470')[0];

                garment.selectedFabric(customFabric);
                garment.fabricsInList([])
                garment.fabricInsearch("Custom Fabric");

                // sets the fabric option
                const fabric_category = garment.categories.filter(function (el) { return el.name.toLowerCase() == 'fabric'})[0];
                const options = fabric_category.options.filter(function(el){return el.name.toLowerCase() == 'fabric'})[0];
                options.selectedValue('6470');

                // do not call takeCustomerFabricImage while coping garments
                // while coping, we set custom_image before.
                if (!garment.customerFabricImage()){
                    this.takeCustomerFabricImage(garment);
                }

                // if garments is a shirt, automatically add extra cost
                // now we aren't no more using the default values 264.00 / 190.00
                if (garment.garment_id=="2"){
                    const new_extra_cost = this.customer.paymentsStep.getExtraCostObj(SHIRT_EXTRA_COST_DESCRIPTION, 0.0);
                    this.customer.paymentsStep.addExtraCost(new_extra_cost);
                }

            }
            else {
                garment.selectedFabric(false);
                garment.fabricsInList([])
                garment.fabricInsearch("");
                garment.customerFabricImage(false);
                garment.customerFabricMeterage(false);

                // if is shirt
                // search for a custom fabric extra cost and remove it
                if (garment.garment_id == "2"){
                    const extra_cost_to_remove = this.customer.paymentsStep.extraCosts().find(extraCost => extraCost.description() == SHIRT_EXTRA_COST_DESCRIPTION);
                    if (extra_cost_to_remove){
                        this.customer.paymentsStep.removeExtraCost(extra_cost_to_remove);
                    }
                }

            }
        },

        getGarmentObject(garmentId) {

            var self = this;

            let garment = JSON.parse(JSON.stringify(this.designs[garmentId])); //

            // Unique ID to identify each garment during the order process
            garment.unique_id = new Date().valueOf();

            // not slow
            for(let category of garment.categories)
            {
                // Boolean if this is selected or not in the UI
                category.selected = ko.observable(false);

                for (let option of category.options) {

                    option.selected = ko.observable(false);

                    // setting default values
                    // if option is a boolean
                    if (option.type_id == '4') {
                        option.selectedValue = ko.observable(false);
                    }
                    // option is number (int or float)
                    else if (['1', '2'].includes(option.type_id)) {
                        option.selectedValue = ko.observable(0);
                    }
                    // option is string
                    else if (option.type_id == '3') {
                        option.selectedValue = ko.observable('');
                    }

                    // color
                    else if (['5','8','9'].includes(option.type_id)){


                        // colors no long stay in design_values table.
                        // get from liningFabricDS, pipingColorDS and buttonholethreadDS !!!!

                        // if liningFabric
                        if (option.type_id == '8'){
                            option.values = [];
                            let print_order = 0;

                            for (const lining of liningFabricDS.store) {
                                option.values.push({
                                    id          : lining.id,
                                    image       : lining.image,
                                    name        : lining.title,
                                    opt_id      : option.id,
                                    print_order : print_order,
                                    thai_name   : "",
                                    preview     : lining.preview,
                                    previews    : lining.previews,
                                    price       : lining.price
                                });

                                print_order += 1;
                            }
                        }

                        // if piping
                        else if (option.type_id == '5'){

                            option.values = [];
                            let print_order = 0;

                            for (const piping of pipingColorDS.store) {
                                option.values.push({
                                    id          : piping.id,
                                    image       : piping.image,
                                    inpocket    :piping.inpocket,
                                    lapel1      : piping.lapel1,
                                    name        : piping.title,
                                    opt_id      : option.id,
                                    preview     : piping.preview,
                                    preview1    : piping.preview1,
                                    preview2    : piping.preview2,
                                    preview3    : piping.preview3,
                                    preview4    : piping.preview4,
                                    preview5    : piping.preview5,
                                    print_order : print_order,
                                    thai_name   : "",
                                    tpreview1   : piping.tpreview1,
                                    tpreview2   : piping.tpreview2,
                                    tpreview3   : piping.tpreview3,
                                    tpreview4   : piping.tpreview4,
                                    tpreview5   : piping.tpreview5
                                });

                                print_order += 1;
                            }
                        }

                        // if button
                        else if (option.type_id == '9'){

                            // images on ERP are crashed

                            // option.values = [];
                            // let print_order = 0;

                            // for (const button of buttonholethreadDS.store) {
                            //     option.values.push({
                            //         id          : button.id,
                            //         image       : button.image,
                            //         name        : button.title,
                            //         opt_id      : option.id,
                            //         print_order : print_order,
                            //         thai_name : ""
                            //     });

                            //     print_order += 1;
                            // }
                        }

                        else {
                            console.log("unknown type");
                            option.selectedValue = ko.observable(null);
                            continue;
                        }

                        // get first value by print_order and set as selected.
                        // ERP already returns an sorted array
                        let first = option.values[0];
                        option.selectedValue = ko.observable(first);

                        option.values[0].selected = ko.observable(true);
                        option.values[0].clicked = ko.observable(true); // open details options

                        for (let valueIndex = 1; valueIndex < option.values.length; valueIndex++) {
                            option.values[valueIndex].selected = ko.observable(false);
                            option.values[valueIndex].clicked = ko.observable(false); // open details options
                        }


                    }

                    // radio button
                    else if(option.type_id == '6'){

                        option.selectedValue = ko.observable(null);

                        if (option.values) {
                            for (let valueIndex = 0; valueIndex < option.values.length; valueIndex++) {
                                option.values[valueIndex].selected = ko.observable(false);
                                option.values[valueIndex].clicked = ko.observable(false); // open details options
                            }
                        }

                    }

                    // image
                    else if(option.type_id == '7'){
                        // option.selectedValue = ko.observable({image: null, tags: []});
                        option.selectedValue = ko.observableArray([]);
                    }

                    // unknown
                    else{
                        option.selectedValue = ko.observable(null);
                    }
                }

                category.visited = ko.observable(false); // to the next button functionality

                // Condition for category to be COMPLETED OR NOT
                // each category has its own particularity
                category.completed = this.conditionToBeComplete(category);

            }


            // Builds the map of categories, we will use IDS to have quicker access to the categories.
            // This is just a reference, NOT A COPY !!!!
            garment.categories_map = {};

            for (let category of garment.categories)
            {
                garment.categories_map[category.id] = category;

                category.options_map = {};

                for( let option of category.options)
                {
                    category.options_map[option.id] = option;

                    if( option.values != undefined )
                    {
                        option.values_map = {};

                        for( let value of option.values )
                        {
                            option.values_map[value.id] = value;
                        }
                    }

                }
            }

            // end building the map


            if (window.should_use_new_3D){
                garment.image3D = new Image3D_v2(garment);
            }else{
                garment.image3D = new Image3D_v1(garment);
            }

            // Sets the observables required to select the fabric
            garment.fabricInsearch = ko.observable(""); // What the salesman has typed in the fabric search
            garment.fabricsInList = ko.observableArray([]); // fabric list for the salesman to choose one
            garment.selectedFabric = ko.observable(false);
            garment.fabricCheckbox = ko.observable(false);


            garment.fabricInsearch.subscribe(function(searchString) { // filter the shirt/not shirt fabrics in the list

                //console.log("searching ", searchString);

                garment.fabricsInList.removeAll();

                //console.log("fabrics", orders.fabricsData);


                if(searchString.length > 2)
                {
                    for (let fabric of self.ordersVM.fabricsData)
                    {
                        if( typeof fabric == "undefined") { continue; }

                        // Maybe problem with ipad < 9.3
                        if( !fabric.title.toLowerCase().trim().includes(searchString.toLowerCase().trim()) )
                        {
                            continue;
                        }

                        //console.log("Adding " + fabric.title + " for ", garment);

                        if( garment.garment_name === "Shirt" )
                        {
                            if( fabric.for_shirt == "1" || fabric.for_shirt == undefined)
                            {
                                garment.fabricsInList.push(fabric)
                            }
                        }
                        else
                        {
                            if( fabric.for_shirt == "0" || fabric.for_shirt == undefined)
                            {
                                garment.fabricsInList.push(fabric)
                            }
                        }

                    }
                }
            });

            // Customer fabric obseravbles:

            garment.useCustomerFabric = ko.observable(false);
            garment.customerFabricImage = ko.observable(false);
            garment.customerFabricMeterage = ko.observable(false);

            garment.useCustomerFabric.subscribe(function(new_value) {
                self._useCustomerFabricEvent(new_value, garment);
            });
            // End observables for fabric


            // Start observables for garment urgency
            garment.isUrgent = ko.observable(false);

            garment.selected_day = ko.observable();
            garment.selected_month = ko.observable();
            garment.selected_year = ko.observable();

            // garment.urgentDate is a string in "DD/MM/YYYT" format
            // garment.urgentDate = ko.observable(false);
            garment.urgentDate = ko.computed(function(){
                return `${garment.selected_month()}/${garment.selected_day()}/${garment.selected_year()}`;
            }, this);

            // end observables for garment urgency


            // variables that controls the copy design.

            // if this variable is false, when other garment change your fit, this garment will change too
            garment.fitHasChanged = ko.observable(false);
            // ...

            // end variables that controls the copy design.


            // pre select values
            this.preSelectValue(garment);

            return garment;
        },

        selectGarment(garment) {

            if (this.selectedGarment() == garment){
                return;
            }

            console.log("selecting garment...", garment);

            var self = this;

            // This is necessary to show the loader while knockout notifies all observables
            $('#loading_jp').show( () => {
                self.selectedGarment(garment);
                document.getElementById('loading_jp').style.display = "none";
            });

        },

        selectCategory(category) {

            if (this.selectedCategory() == category){
                return;
            }

            console.log("selecting...", category);

            // unselect all
            for( let cat of this.selectedGarment().categories )
            {
                cat.selected(false);
            }


            // select just this one
            category.selected(true);
            this.selectedCategory(category);

            // If category is fabric => reinstantiate the UI for the datepicker
            if(category.name.toLowerCase() === 'fabric')
            {
                // Recreate datepicker object
                $(".datepicker").datepicker();
            }

            this._change3DView(category);
        },


        /**
         * In a category selection, this function changes the 3D view.
         * For exemple 1: in suit's monogram category, this method changes de 3d to monogram view
         *
         *
         * Another way to do this, is put a data-bind:click in each category tab
         * @deprecated
         */
        _change3DView(category) {
            // monogram step ID for vest, jacket, suit respectively
            if (['27', '46', '63'].includes(category.id)) {
                // suit
                if (category.id == '63') {
                    this.selectedGarment().image3D.side('jacket_monogram');

                // vest and jacket
                } else {
                    this.selectedGarment().image3D.side('monogram');

                }

            // sleeve
            } else if (category.id == '7') {
                // '7' is ID fot jacket Sleeve
                this.selectedGarment().image3D.side('sleeve');
            } else if (category.id == '54') {
                // '54' is ID fot suit Sleeve
                this.selectedGarment().image3D.side('jacket_sleeve');
            }

            // back
            else if (["36", "5", "17", "18", "26", "51", "61", "62"].includes(category.id)) {
                // suit
                if (category.garment_id == "6"){

                    if (["51", "62"].includes(category.id)) {
                        this.selectedGarment().image3D.side('jacket_back');
                    } else {
                        this.selectedGarment().image3D.side('pant_back');
                    }
                }

                // others
                else{
                    this.selectedGarment().image3D.side('back');
                }
            }

            // shirt contrast
            else if (category.id == "38"){
                this.selectedGarment().image3D.side('contrast');

            }

            // front
            else {
                // suit
                if (category.garment_id == '6') {
                    // pants category
                    if (['58', '59', '60', '61', '63', '64', '70'].includes(category.id)){
                        this.selectedGarment().image3D.side('pant_front');
                    }else {
                        this.selectedGarment().image3D.side('jacket_front');
                    }

                // others
                } else {
                    this.selectedGarment().image3D.side('front');
                }
            }


        },

        /**
         * One way of setting the value to an option
         * WARNING:
         *          Be aware that changing values inside the for each can cause the system to be slow.
         *          Most of the cases this will not happen, but be aware.
         *          This is caused by Knockout trying to notify all dependencies for every interation.
         */
        setOptionValue(option, value, garment = null) {

            if (garment){
                // change garment to show in 3D
                this.ordersVM.currentGarmentInstance(garment);

                // Set selected garment as the garment who will be updated
                this.ordersVM.selectedCustomer().garmentsDesignStep.selectedGarment(garment.garment);
            }

            console.log("Setting ", ko.toJSON(option.name));
            console.log("to ", ko.toJSON(value));

            try {
                // replicate value changes to other options
                const category = this.ordersVM.selectedCustomer().garmentsDesignStep.selectedGarment().categories.filter(el => el.id == option.cat_id)[0];
                this._replicateValue(category, option, value);
            } catch (error) {
                console.error("Error in _replicateValue", error);
            }


            option.selectedValue(value);

            if(option.values){
                for( let val of option.values )
                {
                    val.selected(false);
                }
            }

            if (typeof value !== "boolean"){
                value.selected(true);
            }

            // if we set a custom option, ask if user wants to take custom picture now
            if (value.name.includes("Custom")){
                const confirmAction = () => {
                    this.takeOptionCustomImage(this.getCustomOptionFromCustomValueId(value.id));
                };
                pos_confirm({
                    msg:'Do you want to take the custom picture now?',
                    confirmAction,
                    confirmBtn:'YES',
                    cancelBtn: 'NO',
                });
            }

            // save garment design
            try {

                if (this.ordersVM.selectedCustomer() == ""){
                    throw "this.ordersVM.selectedCustomer() is empty";
                } else if (this.ordersVM.selectedCustomer().garmentsDesignStep.selectedGarment() == ""){
                    throw "this.ordersVM.selectedCustomer().garmentsDesignStep.selectedGarment() is empty";
                }

                // save garment to recover in case of bug:
                this.saveGarmentDesign(this.ordersVM.selectedCustomer().garmentsDesignStep.selectedGarment());

            } catch (error) {
                 console.error("Error in saveGarmentDesign", error);
            }


        },

        /**
         * For usability purposes, when an option is selected, the value can be replicated to others.
         * This happens mostly for buttons and colors.
         *
         * For example, in a sleeve of a jacket, instead of putting a color for each of the 5 buttons,
         * we can do for one and the value is replicated to others
         *
         *
         *
         * @param {Object} option
         * @param {Object} value
         */
        _replicateValue(category, option, value){

            // for jacket's sleeve
            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗███████╗    ███████╗██╗     ███████╗███████╗██╗   ██╗███████╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝██╔════╝    ██╔════╝██║     ██╔════╝██╔════╝██║   ██║██╔════╝
            //      ██║███████║██║     █████╔╝ █████╗     ██║   ███████╗    ███████╗██║     █████╗  █████╗  ██║   ██║█████╗
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   ╚════██║    ╚════██║██║     ██╔══╝  ██╔══╝  ╚██╗ ██╔╝██╔══╝
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║   ███████║    ███████║███████╗███████╗███████╗ ╚████╔╝ ███████╗
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝    ╚══════╝╚══════╝╚══════╝╚══════╝  ╚═══╝  ╚══════╝
            //
            // button collor
           if (option.id == "39") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "40")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "40") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "41")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "41") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "42")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "42") {
                // nothing
            }


            // button hole
          if (option.id == "44") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "45")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "45") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "46")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "46") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "47")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "47") {
                // nothing
            }

            // button thread
           if (option.id == "49") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "50")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "50") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "51")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "51") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "52")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "52") {
                // nothing
            }

            // for suit's sleeve =======================
            // ███████╗██╗   ██╗██╗████████╗███████╗    ███████╗██╗     ███████╗███████╗██╗   ██╗███████╗
            // ██╔════╝██║   ██║██║╚══██╔══╝██╔════╝    ██╔════╝██║     ██╔════╝██╔════╝██║   ██║██╔════╝
            // ███████╗██║   ██║██║   ██║   ███████╗    ███████╗██║     █████╗  █████╗  ██║   ██║█████╗
            // ╚════██║██║   ██║██║   ██║   ╚════██║    ╚════██║██║     ██╔══╝  ██╔══╝  ╚██╗ ██╔╝██╔══╝
            // ███████║╚██████╔╝██║   ██║   ███████║    ███████║███████╗███████╗███████╗ ╚████╔╝ ███████╗
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝    ╚══════╝╚══════╝╚══════╝╚══════╝  ╚═══╝  ╚══════╝
            //
            // button collor
            if (option.id == "174") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "175")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "175") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "176")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "176") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "177")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "177") {
                // nothing
            }


            // button hole
            if (option.id == "179") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "180")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "180") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "224")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "224") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "181")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "181") {
                // nothing
            }

            // button thread
            if (option.id == "183") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "184")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "184") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "185")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "185") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "186")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "186") {
                // nothing
            }

            // for jacket's monogram ========================
            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗███████╗    ███╗   ███╗ ██████╗ ███╗   ██╗ ██████╗  ██████╗ ██████╗  █████╗ ███╗   ███╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝██╔════╝    ████╗ ████║██╔═══██╗████╗  ██║██╔═══██╗██╔════╝ ██╔══██╗██╔══██╗████╗ ████║
            //      ██║███████║██║     █████╔╝ █████╗     ██║   ███████╗    ██╔████╔██║██║   ██║██╔██╗ ██║██║   ██║██║  ███╗██████╔╝███████║██╔████╔██║
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   ╚════██║    ██║╚██╔╝██║██║   ██║██║╚██╗██║██║   ██║██║   ██║██╔══██╗██╔══██║██║╚██╔╝██║
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║   ███████║    ██║ ╚═╝ ██║╚██████╔╝██║ ╚████║╚██████╔╝╚██████╔╝██║  ██║██║  ██║██║ ╚═╝ ██║
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝    ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝
            //

            // jacket piping
            if (option.id == "125") {
                // change pocket color
                var nextOption = category.options.filter(el => el.id == "126")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

                // change stitch color
                nextOption = category.options.filter(el => el.id == "239")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }


            // for suit's monogram =========================
            // ███████╗██╗   ██╗██╗████████╗███████╗    ███╗   ███╗ ██████╗ ███╗   ██╗ ██████╗  ██████╗ ██████╗  █████╗ ███╗   ███╗
            // ██╔════╝██║   ██║██║╚══██╔══╝██╔════╝    ████╗ ████║██╔═══██╗████╗  ██║██╔═══██╗██╔════╝ ██╔══██╗██╔══██╗████╗ ████║
            // ███████╗██║   ██║██║   ██║   ███████╗    ██╔████╔██║██║   ██║██╔██╗ ██║██║   ██║██║  ███╗██████╔╝███████║██╔████╔██║
            // ╚════██║██║   ██║██║   ██║   ╚════██║    ██║╚██╔╝██║██║   ██║██║╚██╗██║██║   ██║██║   ██║██╔══██╗██╔══██║██║╚██╔╝██║
            // ███████║╚██████╔╝██║   ██║   ███████║    ██║ ╚═╝ ██║╚██████╔╝██║ ╚████║╚██████╔╝╚██████╔╝██║  ██║██║  ██║██║ ╚═╝ ██║
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝    ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝
            //

             // jacket piping
             if (option.id == "204") {
                // change pocket color
                var nextOption = category.options.filter(el => el.id == "205")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

                // change stitch color
                nextOption = category.options.filter(el => el.id == "241")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }


            // all fit
            // fit for shirt = 88 / fit for vest = 73 / fit for Jacket = 6 / fit for suit = 158 / fit for pants = 56
            if (["6", "56", "158", "73", "88"].includes(option.id)){
                // replicate for other garments

                // get other garments list
                const garmentsTypes = ["jacket", "suit", "pants", "vest", "shirt"];
                var otherGarments = [] ;
                garmentsTypes.forEach(garmentType => {

                    for (const current_garment of this.ordersVM.globalGarments[garmentType]()) {
                        otherGarments.push(current_garment.garment);
                    }

                });

                for (const otherGarment of otherGarments) {
                    // change if fit was not changed and if customers are the same
                    if (!otherGarment.fitHasChanged() && this.currentCustomerHasGarment(otherGarment.unique_id) ){

                        // get fit option
                        let current_option;

                        switch (otherGarment.garment_id) {
                            // jacket
                            case "4":
                                current_option = otherGarment.categories_map[3].options_map[6];
                                break;
                            // suit
                            case "6":
                                current_option = otherGarment.categories_map[48].options_map[158];
                                break;
                            // pants
                            case "1":
                                current_option = otherGarment.categories_map[13].options_map[56];
                                break;
                            // vest
                            case "3":
                                current_option = otherGarment.categories_map[21].options_map[73];
                                break;
                            // shirt
                            case "2":
                                current_option = otherGarment.categories_map[30].options_map[88];
                                break;
                        }


                        // get corresponding value
                        var correspondingValue = current_option.values.filter(el => el.name == value.name)[0];

                        current_option.selectedValue(correspondingValue);
                        for( let val of current_option.values )
                        {
                            val.selected(false);
                        }
                        correspondingValue.selected(true);



                        // if is suit, replicate for pants too
                        if (otherGarment.garment_id == "6"){
                            current_option = otherGarment.categories_map[70].options_map[243];

                            // get corresponding value
                            var correspondingValue = current_option.values.filter(el => el.name == value.name)[0];

                            current_option.selectedValue(correspondingValue);
                            for (let val of current_option.values) {
                                val.selected(false);
                            }
                            correspondingValue.selected(true);
                        }

                        otherGarment.fitHasChanged(true);

                    }
                }

            }



        },

        /**
         * Check if the customer of the current garment has another garment by other garment's id
         * @param {Number} garmentId of the other garment
         */
        currentCustomerHasGarment(garmentId){


                for (const garmentTypes of  this.ordersVM.currentGarmentInstance().customer.garmentsStep.garments()) {
                    for (const currentGarment of garmentTypes.garments()) {

                        if (currentGarment.unique_id == garmentId){
                            return true;
                        }
                    }

                }


            return false;
        },

        /**
         * To hide and show details of a option
         * @param {*} option
         */
        setValueClicked(option, value) {

            console.log("Setting ", ko.toJSON(option));
            console.log("clicked ", ko.toJSON(value));


            for( let val of option.values )
            {
                val.clicked(false);
            }

            value.clicked(true);
        },

        takeCustomerFabricImage : function(garment) {

            alert("Please Take a picture of the customer fabric.");


            console.log("Taking customer fabric image...");

            const source = Camera.PictureSourceType.CAMERA;

            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            });

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                console.log("entry.toURL()", entry.toURL());
                garment.customerFabricImage(entry.toURL());
            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };


        },

        /**
         * Copies the selected design options from one garment to another.
         *
         * we need to make the copy option by option not to destroy the existing KO bindings.
         *
         * @param  {[type]} fromGarment [description]
         * @param  {[type]} toGarment   [description]
         * @return {[type]}             [description]
         */
        copyDesign : function(fromGarment, toGarment, is_reorder = false) {


            const _copy = () => {
                if( fromGarment.garment_id != toGarment.garment_id ) { console.log("CAN ONLY COPY DESIGN BETWEEN SAME TYPE GARMENTS"); return; }

                console.log("Copying", fromGarment);
                console.log("to", toGarment);

                for( let category of fromGarment.categories )
                {
                    // Do not Clone the monogram
                    if (category.name.toLowerCase().trim() === 'monogram') {
                        continue;
                    }

                    // fabric has a different treatment
                    if (category.name.toLowerCase().trim() === 'fabric') {

                        // in case of non-reorder fromGarment has KO.object
                        if (typeof fromGarment.selectedFabric == 'function') {

                            // do not copy in design step
                            if (is_reorder){

                                if (fromGarment.selectedFabric())
                                {
                                    this.ordersVM.selectGarmentFabric(fromGarment.selectedFabric(), toGarment);
                                }

                                toGarment.isUrgent(fromGarment.isUrgent());
                                // urgent date is now a computed. we must update garment's day,month and year instead
                                if (fromGarment.urgentDate) {
                                    // toGarment.urgentDate(fromGarment.urgentDate());
                                    const dateParsed = fromGarment.urgentDate().split(new RegExp('[/-]'));
                                    toGarment.selected_day(dateParsed[1]);
                                    toGarment.selected_month(dateParsed[0]);
                                    toGarment.selected_year(dateParsed[2]);
                                }

                                toGarment.customerFabricImage(fromGarment.customerFabricImage());
                                toGarment.customerFabricMeterage(fromGarment.customerFabricMeterage());
                                toGarment.useCustomerFabric(fromGarment.useCustomerFabric());
                            }

                        }

                        // in case of reorder fromGarment has no KO.object
                        else {
                            // get fabric
                            if (fromGarment.selectedFabric){
                                let fabric = this.ordersVM.fabricsData.filter(fabric => fabric.id == fromGarment.selectedFabric.id )[0];

                                if (fabric){
                                    this.ordersVM.selectGarmentFabric(fabric, toGarment);
                                }
                            }

                            // fabric from SAVED DESIGN from ERP
                            else {

                                const fabricCategory = category.options.filter(el => el.name.toLowerCase().trim() == 'fabric')[0];
                                let fabric = this.ordersVM.fabricsData.filter(fabric => fabric.id == fabricCategory.selectedValue)[0];

                                if (fabric){
                                    this.ordersVM.selectGarmentFabric(fabric, toGarment);
                                }
                            }

                            toGarment.isUrgent(fromGarment.isUrgent);

                            // urgent date is now a computed. we must update garment's day,month and year instead
                            if (fromGarment.urgentDate){
                                // toGarment.urgentDate(fromGarment.urgentDate);
                                const dateParsed = fromGarment.urgentDate.split(new RegExp('[/-]'));
                                toGarment.selected_day(dateParsed[0]);
                                toGarment.selected_month(dateParsed[1]);
                                toGarment.selected_year(dateParsed[2]);
                            }

                            toGarment.customerFabricImage = ko.observable(fromGarment.customerFabricImage);
                            toGarment.customerFabricMeterage = ko.observable(fromGarment.customerFabricMeterage);
                            toGarment.useCustomerFabric = ko.observable(fromGarment.useCustomerFabric);

                            toGarment.useCustomerFabric.subscribe((new_value) => {
                                this._useCustomerFabricEvent(new_value, toGarment);
                            });
                        }

                        continue;
                    }


                    const toGarmentCategory = toGarment.categories.filter( e => e.id == category.id )[0];

                    for( let option of category.options )
                    {
                        var selectedVal;

                        // case isReorder == false
                        if (typeof  option.selectedValue == 'function'){
                            selectedVal = option.selectedValue();
                        }
                        else {
                            selectedVal = option.selectedValue;
                        }


                        // Find the same option in the toGarment garment
                        const toGarmentOption = toGarmentCategory.options.filter( e => e.id == option.id )[0];

                        // console.log("toGarmentOption", toGarmentOption);

                        // If there are values to selected in the option and the selectedVal is a available option:
                        if( toGarmentOption.values != undefined && selectedVal && selectedVal.id != undefined )
                        {
                            // first: unselect all
                            toGarmentOption.values.map(el => el.selected(false));


                            const toGarmentSelectedValue = toGarmentOption.values.filter( e => e.id == selectedVal.id)[0];
                            toGarmentSelectedValue.selected(true);
                            toGarmentOption.selectedValue(toGarmentSelectedValue);
                        }

                        /*
                        * Else => the option has no multiple choice (boolean, string, ...)
                        */
                        else
                        {
                            // only copy note if toGarment's note is null
                            if (option.name.toLowerCase().trim().includes("note") && toGarmentOption.selectedValue()) {
                                // do not copy !!!
                            }
                            else{
                                const selectedValClone = JSON.parse(JSON.stringify(selectedVal));
                                toGarmentOption.selectedValue(selectedValClone);
                            }
                        }
                    }
                }

                if (is_reorder) $.jGrowl(`Garment design succefully loaded !`);
                else $.jGrowl(`Garment design succefully cloned !`);

                this.isCopyDesignPopUpVisible(false);
            }



            if (!is_reorder) {
                pos_confirm({msg: "Do you really want to copy the current garment design to the selected one ?", confirmAction: _copy});
            }else{
                // copy without ask
                _copy();
            }

        },

        /**
         * Copy design to many garments once
         * @param {Garment} fromGarment
         * @param {Array} toGarments
         *
         * @deprecated
         */
        copyToMany(){
            var self = this;

            // get selected garment (is the fromGarment)
            const fromGarment = this.selectedGarment();

            // get selected garments in the "copy to many" pop-up  (are the to garments)
            // get selected by html

            let toGarments = [];

            $.each($("input[name='must_copy']:checked"), function () {

                let garmentUniqueId = $(this).attr('data-garment');

                // find garment by unique ID
                for (const customer of self.ordersVM.customers()) {
                    for (const garmentType of customer.garmentsStep.garments()) {
                        for (const garment of garmentType.garments()) {
                            if (garment.unique_id.toString() == garmentUniqueId){
                                toGarments.push(garment);
                            }
                        }
                    }
                }
            });


            for (const toGarment of toGarments) {
                this.copyDesign(fromGarment, toGarment, true);
            }

            // close popup
            this.copyToManyPopUp(false);
        },

        /**
         * Copy design to many garments once
         * @param {Garment} fromGarment
         */
        copyToMany2(fromGarment){
            var self = this;

            // get selected garment (is the fromGarment)
            if (!fromGarment){
                fromGarment = this.selectedGarment();
            }

            // get selected garments in the "copy to many" pop-up  (are the to garments)
            // get selected by html

            let toGarments = [];

            $.each($("input[name='garmentSelectedToCopy']:checked"), function () {

                let garmentUniqueId = $(this).attr('data-garment');

                // find garment by unique ID
                for (const customer of self.ordersVM.customers()) {
                    for (const garmentType of customer.garmentsStep.garments()) {
                        for (const garment of garmentType.garments()) {
                            if (garment.unique_id.toString() == garmentUniqueId){
                                toGarments.push(garment);
                            }
                        }
                    }
                }
            });


            for (const toGarment of toGarments) {
                this.copyDesign(fromGarment, toGarment, true);
            }

            // close popup
            this.witchGarmentRecoverDesignPopUpVisible(false);
        },


        /**
         * Stores a garment in LOCAL STORE of browser.
         *
         * This functionality allow the user retrieve data if a error (bug) occurs
         *
         * how this garments are save:
         *      in a list like this:
         *
         *      storedDesigns = [
         *         design={...}, date={...}, customer = {}, device_id = STRING, salesman = {}    },
         *         design={...}, date={...}, customer = {}, device_id = STRING, salesman = {}    }
         *
         *      ]
         * @param {*} garment
         */
        saveGarmentDesign(garment) {
            console.log("saving data");

            var newDesign = {};

            // get current date (attention to summer time)
            const currentDate   = new Date();
            const day           = currentDate.getDate().toString()
            const dayF          = (day.length == 1) ? '0' + day : day
            const month         = (currentDate.getMonth() + 1).toString() //+1 pois no getMonth Janeiro começa com zero.
            const monthF        = (month.length == 1) ? '0' + month : month
            const yearF         = currentDate.getFullYear();
            newDesign.date      = dayF + "-" + monthF + "-" + yearF;

            // get device_id
            newDesign.device_id = device.uuid;


            // Getting the customer that have the current garment
            const customer = this.ordersVM.globalGarments[garment.garment_name.toLowerCase()]().filter( gar => gar.garment.unique_id == garment.unique_id )[0].customer;


            // get customer info
            // newDesign.customer = this.ordersVM.selectedCustomer();
            // get only specific datas
            newDesign.customer = {
                customer_first_name             : customer.customer_first_name,
                customer_id                     : customer.customer_id,
                customer_last_name              : customer.customer_last_name,
                full_name                       : customer.full_name/* ,
                garmentsDesignStep      : {
                    designs                     : this.ordersVM.selectedCustomer().garmentsDesignStep.designs,
                    hasCustomDesign             : this.ordersVM.selectedCustomer().garmentsDesignStep.hasCustomDesign,
                    isCopyDesignPopUpVisible    : this.ordersVM.selectedCustomer().garmentsDesignStep.isCopyDesignPopUpVisible,
                    isRecoverDesignPopUpVisible : this.ordersVM.selectedCustomer().garmentsDesignStep.isRecoverDesignPopUpVisible,
                    selectedCategory            : this.ordersVM.selectedCustomer().garmentsDesignStep.selectedCategory,
                    selectedGarment             : this.ordersVM.selectedCustomer().garmentsDesignStep.selectedGarment
                }, */
            };


            // get salesman info
            newDesign.user = authCtrl.userInfo;


            // get design of garment
            const garment_json =  ko.mapping.toJS(garment)

            newDesign.design = {
                garment_name              : garment_json.garment_name,
                garment_id                : garment_json.garment_id,
                categories                : garment_json.categories,
                unique_id                 : garment_json.unique_id,
                image3D                   : garment_json.image3D,
                selectedFabric            : garment_json.selectedFabric,
                useCustomerFabric         : garment_json.useCustomerFabric,
                customerFabricImage       : garment_json.customerFabricImage,
                customerFabricMeterage    : garment_json.customerFabricMeterage,
                isUrgent                  : garment_json.isUrgent,
                urgentDate                : garment_json.urgentDate
            }

            // remove unnecessary data
            for (const category of newDesign.design.categories) {

                // remove unnecessary data
                Object.keys(category).forEach(function (itm) {
                    if (!["id","name", "garment_id", "options"].includes(itm)) {
                        delete category[itm];
                    }
                });

                for (const option of category.options) {

                    // remove unnecessary data
                    Object.keys(option).forEach(function (itm) {
                        if (!["cat_id", "id", "selectedValue", "name"].includes(itm)) {
                            delete option[itm];
                        }
                    });

                }
            }


            // get old data to push new design
            var data_to_update = this.loadGarmentFromLocalStorage();

            // identify if garment already is stored
            // each garment has an unique_id

            let isUpdate = false;

            for(const savedGarment of data_to_update()){
                //  already is stored. Need to update
                if (savedGarment.design.unique_id == garment.unique_id){

                    console.log("update garment");

                    isUpdate = true;

                    savedGarment.design = newDesign.design;

                    break;
                }
            }

            // is a new design
            if (!isUpdate){
                console.log("new garment");
                data_to_update.push(newDesign);

            }


        },

        loadGarmentFromLocalStorage() {
            try {
                savedDesigns // will throw an exception if savedDesign is undefined
            } catch (error) {
                // if savedDesign is undefined, init with an empty array
                savedDesigns = ko.observableArray();
            }
            return savedDesigns;
        },

        /**
         * filter the savedDesigns by garment type
         * @param {String} garmentType must be ['jacket', 'pants', 'suit', 'vest' or 'shirt']
         * @returns {ko.observableArray}
         */
        loadSpecificGarmentFromStorage(garmentType){
            // if garment type is undefined
            try {
                garmentType
            } catch (error) {
                return ko.observableArray([]);
            }

            // invalid garment name
            if (!garmentType || !['jacket', 'pants', 'suit', 'vest','shirt'].includes(garmentType.toLowerCase().trim())){
                return ko.observableArray([]);
            }

            var savedDesigns = this.loadGarmentFromLocalStorage();

            const response = savedDesigns().filter(el => el.design.garment_name.toLowerCase().trim() == garmentType.toLowerCase().trim());

            return ko.observableArray(response);

        },

        /**
         * filter garments by a specific type
         * @param {String} garmentType must be ['jacket', 'pants', 'suit', 'vest' or 'shirt']
         * @param {Customer} customer - Optional. if no customer is specified , return garment for all customers
         * @returns {ko.observableArray}
         */
        loadGarmentsByType(garmentType, customer = null){
            // invalid garment name
            if (!garmentType || !['jacket', 'pants', 'suit', 'vest', 'shirt'].includes(garmentType.toLowerCase().trim())) {
                return ko.observableArray([]);
            }

            var allGarments = [];

            // get all garment of this customer
            if (customer){
                console.error("not tested !!!");
                return ko.observableArray([]);;
                allGarments = this.ordersVM.selectedCustomer().garmentsStep.garments();

            // get all garment of all customers
            } else {
                const keys = Object.keys(this.ordersVM.globalGarments);
                for (const key of keys) {
                    allGarments = allGarments.concat(this.ordersVM.globalGarments[key]());
                }
            }

            // get garments of the same type
            const garmentsByType = allGarments.filter(el => {
                return el.garmentType.toLowerCase().trim() == garmentType.toLowerCase().trim();
            });

            return ko.observableArray(garmentsByType);
        },

        copyFromSavedDesign(fromGarment, toGarment){
            this.copyDesign(fromGarment, toGarment, false);

            // close pop-ups
            this.isCopyDesignPopUpVisible(false);
            this.isRecoverDesignPopUpVisible(false);
        },


        /**
         * Pre select some default values
         *
         * ex: default fit => 'SEMI FITTED'
         * @param {Object} garment
         */
        preSelectValue(garment){
            console.log('pre select value for garment:', garment);

            // pants
            if (garment.garment_id == '1') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '13')[0];
                let fitOption = fitCat.options.filter(option => option.id == '56')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '1252')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);

                // pant style => Normal Straight
                let styleCat = garment.categories.filter(category => category.id == '14')[0];
                let styleOption = styleCat.options.filter(option => option.id == '57')[0];
                let styleDefaultValue = styleOption.values.filter(value => value.id == '1256')[0];

                styleOption.selectedValue(styleDefaultValue);
                styleDefaultValue.selected(true);

            }

            // vest
            else if (garment.garment_id == '3') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '21')[0];
                let fitOption = fitCat.options.filter(option => option.id == '73')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '1289')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);

                // lapel V
                let lapelCat = garment.categories.filter(category => category.id == '22')[0];
                let lapelOption = lapelCat.options.filter(option => option.id == '74')[0];
                let lapelDefaultValue = lapelOption.values.filter(value => value.id == '1295')[0];

                lapelOption.selectedValue(lapelDefaultValue);
                lapelDefaultValue.selected(true);
            }

            // shirt
            else if (garment.garment_id == '2') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '30')[0];
                let fitOption = fitCat.options.filter(option => option.id == '88')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '1312')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);

                // pocket position => left
                let pocketCat = garment.categories.filter(category => category.id == '37')[0];
                let positionOption = pocketCat.options.filter(option => option.id == '105')[0];
                let pocketDefaultValue = positionOption.values.filter(value => value.id == '1366')[0];

                positionOption.selectedValue(pocketDefaultValue);
                pocketDefaultValue.selected(true);

                // font monogram => block
                let monogramtCat = garment.categories.filter(category => category.id == '39')[0];
                let fontOption = monogramtCat.options.filter(option => option.id == '122')[0];
                let fontDefaultValue = fontOption.values.filter(value => value.id == '1583')[0];

                fontOption.selectedValue(fontDefaultValue);
                fontDefaultValue.selected(true);

                // monogram position => cuff
                let monogramPositionOption = monogramtCat.options.filter(option => option.id == '119')[0];
                let monogramPositionValue = monogramPositionOption.values.filter(value => value.id == '1483')[0];

                monogramPositionOption.selectedValue(monogramPositionValue);
                monogramPositionValue.selected(true);

                // monogram Stitching => default
                let stitchingOption = monogramtCat.options.filter(option => option.id == '120')[0];
                let defaultValue = stitchingOption.values.filter(value => value.id == '1578')[0];

                stitchingOption.selectedValue(defaultValue);
                defaultValue.selected(true);

                // platinum make
                let fabricCat = garment.categories.filter(category => category.id == '29')[0];
                let platinumMakeOption = fabricCat.options.filter(option => option.id == '251')[0];

                platinumMakeOption.selectedValue(true);



            }

            // jacket
            else if (garment.garment_id == '4') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '3')[0];
                let fitOption = fitCat.options.filter(option => option.id == '6')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '22')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);


                // bh => true
                let lapelCat = garment.categories.filter(category => category.id == '6')[0];
                let bhOption = lapelCat.options.filter(option => option.id == '14')[0];

                bhOption.selectedValue(true);

                // lapel width => 3.25
                let widthOption = lapelCat.options.filter(option => option.id == '16')[0];
                widthOption.selectedValue(3.25);


                // sleeve button number => 4
                let sleeveCat = garment.categories.filter(category => category.id == '7')[0];
                let numberOption = sleeveCat.options.filter(option => option.id == '29')[0];
                let numberDefaultValue = numberOption.values.filter(value => value.id == '228')[0];

                numberOption.selectedValue(numberDefaultValue);
                numberDefaultValue.selected(true);

                // kissing => false by default
                let kissingOption = sleeveCat.options.filter(option => option.id == '30')[0];

                kissingOption.selectedValue(false);

                // working => true by default
                let workingOption = sleeveCat.options.filter(option => option.id == '31')[0];

                workingOption.selectedValue(true);




                // Breast Pockets => Standard Straight
                let breastPcktCat = garment.categories.filter(category => category.id == '9')[0];
                let styleOption = breastPcktCat.options.filter(option => option.id == '36')[0];
                let styleDefaultValue = styleOption.values.filter(value => value.id == '240')[0];

                styleOption.selectedValue(styleDefaultValue);
                styleDefaultValue.selected(true);

                // "Specially tailored for"
                let monogramCat = garment.categories.filter(category => category.id == '46')[0];
                let line1Option = monogramCat.options.filter(option => option.id == '128')[0];

                line1Option.selectedValue("Specially tailored for");

                let defaultMonogramOption = monogramCat.options.filter(option => option.id == '253')[0];
                defaultMonogramOption.selectedValue(true);


            }

            // suit
            else if (garment.garment_id == '6') {
                // fit (pant) => semi fitted
                let fitCat_pant = garment.categories.filter(category => category.id == '70')[0];
                let fitOption_pant = fitCat_pant.options.filter(option => option.id == '243')[0];
                let fitDefaultValue_pant = fitOption_pant.values.filter(value => value.id == '4562')[0];

                fitOption_pant.selectedValue(fitDefaultValue_pant);
                fitDefaultValue_pant.selected(true);


                // fit (jacket) => semi fitted
                let fitCat_jacket = garment.categories.filter(category => category.id == '48')[0];
                let fitOption_jacket = fitCat_jacket.options.filter(option => option.id == '158')[0];
                let fitDefaultValue_jacket = fitOption_jacket.values.filter(value => value.id == '2604')[0];

                fitOption_jacket.selectedValue(fitDefaultValue_jacket);
                fitDefaultValue_jacket.selected(true);


                // bh => true
                let lapelCat = garment.categories.filter(category => category.id == '52')[0];
                let bhOption = lapelCat.options.filter(option => option.id == '166')[0];

                bhOption.selectedValue(true);

                // lapel width => 3.25
                let widthOption = lapelCat.options.filter(option => option.id == '168')[0];
                widthOption.selectedValue(3.25);

                // sleeve button number => 4
                let sleeveCat = garment.categories.filter(category => category.id == '54')[0];
                let numberOption = sleeveCat.options.filter(option => option.id == '169')[0];
                let numberDefaultValue = numberOption.values.filter(value => value.id == '2715')[0];

                numberOption.selectedValue(numberDefaultValue);
                numberDefaultValue.selected(true);


                // kissing => false by default
                let kissingOption = sleeveCat.options.filter(option => option.id == '170')[0];

                kissingOption.selectedValue(false);

                // working => true by default
                let workingOption = sleeveCat.options.filter(option => option.id == '171')[0];

                workingOption.selectedValue(true);





                // Breast Pockets => Standard Straight
                let breastPcktCat = garment.categories.filter(category => category.id == '56')[0];
                let styleOption = breastPcktCat.options.filter(option => option.id == '190')[0];
                let styleDefaultValue = styleOption.values.filter(value => value.id == '3649')[0];

                styleOption.selectedValue(styleDefaultValue);
                styleDefaultValue.selected(true);

                // "Specially tailored for"
                let monogramCat = garment.categories.filter(category => category.id == '63')[0];
                let line1Option = monogramCat.options.filter(option => option.id == '207')[0];

                line1Option.selectedValue("Specially tailored for");

                let defaultMonogramOption = monogramCat.options.filter(option => option.id == '254')[0];
                defaultMonogramOption.selectedValue(true);

                // pant style => Normal Straight
                let styleCat = garment.categories.filter(category => category.id == '58')[0];
                let pantsStyleOption = styleCat.options.filter(option => option.id == '211')[0];
                let pantsStyleDefaultValue = pantsStyleOption.values.filter(value => value.id == '3842')[0];

                pantsStyleOption.selectedValue(pantsStyleDefaultValue);
                pantsStyleDefaultValue.selected(true);
            }
        },

        /**
         * each category has its own particularity to be complete or not.
         * This method receive a category and return a ko computed for this specific category
         * */
        conditionToBeComplete(category) {
            var self = this;
            switch (category.id) {
                // shirt categories:

                // ███████╗██╗  ██╗██╗██████╗ ████████
                // ██╔════╝██║  ██║██║██╔══██╗╚══██╔══
                // ███████╗███████║██║██████╔╝   ██║
                // ╚════██║██╔══██║██║██╔══██╗   ██║
                // ███████║██║  ██║██║██║  ██║   ██║
                // ╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝   ╚═╝
                //

                // shirt fabric
                case "29":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '86'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // shirt fit
                case "30":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '88'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Sleeve
                case "31":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '89'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // cuffs
                case "32":
                    return ko.computed(function () {
                        // if sleeve is short, return true
                        // if: check if current garment os shirt (id == 4)
                        if(self.ordersVM.currentGarmentInstance() && self.ordersVM.currentGarmentInstance().garment.garment_id == "2"){
                            let sleeveStyleOption =  self.ordersVM.currentGarmentInstance().garment.categories_map[31].options_map[89];
                            if (sleeveStyleOption.selectedValue() && sleeveStyleOption.selectedValue().id == '1316'){
                                return true;
                            }
                        }

                        let style = category.options.filter(function (el) { return el.id == '92'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });


                // Shirt Collar
                case "33":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '94'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Front
                case "34":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '96'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom
                case "35":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '99'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Back
                case "36":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '101'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pockets
                case "37":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '103'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '105'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Contrasts
                case "38":
                    return ko.computed(function () {

                        let buttom_colour = category.options.filter(function (el) { return el.id == '114'; })[0];
                        let thread_colour = category.options.filter(function (el) { return el.id == '115'; })[0];
                        return [null, ''].includes(buttom_colour.selectedValue()) ? false :
                            [null, ''].includes(thread_colour.selectedValue()) ? false : true;
                    });

                // Monogram
                case "39":
                    return ko.computed(function () {
                        return category.visited() ; // return true if this category has been visited
                        // let monogram = category.options.filter(function (el) { return el.id == '118'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '119'; })[0];
                        // let sitiching = category.options.filter(function (el) { return el.id == '120'; })[0];
                        // return [null, ''].includes(monogram.selectedValue()) ? false :
                        //     [null, ''].includes(position.selectedValue()) ? false :
                        //         [null, ''].includes(sitiching.selectedValue()) ? false : true;
                    });

                // Notes
                case "40":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '123'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // vest
                // ██╗   ██╗███████╗███████╗████████╗
                // ██║   ██║██╔════╝██╔════╝╚══██╔══╝
                // ██║   ██║█████╗  ███████╗   ██║
                // ╚██╗ ██╔╝██╔══╝  ╚════██║   ██║
                //  ╚████╔╝ ███████╗███████║   ██║
                //   ╚═══╝  ╚══════╝╚══════╝   ╚═╝
                //

                // Fabric
                case "20":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '71'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Fit
                case "21":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '73'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel
                case "22":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '74'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom
                case "23":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '76'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Buttons
                case "24":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '78'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pockets
                case "25":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '80'; })[0];
                        let breast = category.options.filter(function (el) { return el.id == '82'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : [null, ''].includes(breast.selectedValue()) ? false : true;
                    });

                // Back Style
                case "26":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '83'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lining  Pipping
                case "27":
                    return ko.computed(function () {
                        let lining = category.options.filter(function (el) { return el.id == '132'; })[0];
                        let pipping = category.options.filter(function (el) { return el.id == '133'; })[0];
                        return [null, ''].includes(lining.selectedValue()) ? false :
                            [null, ''].includes(pipping.selectedValue()) ? false : true;
                    });

                // Notes
                case "28":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '85'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // Jacket
                //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗
                //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝
                //      ██║███████║██║     █████╔╝ █████╗     ██║
                // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║
                // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║
                //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝
                //
                // Jacket style
                case "1":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '1'; })[0];
                        let bh = category.options.filter(function (el) { return el.id == '4'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : [null, ''].includes(bh.selectedValue()) ? false : true;
                    });

                // Fabric
                case "2":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '5'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Fit
                case "3":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '6'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom Style
                case "4":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '7'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Vent Style
                case "5":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '9'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Style
                case "6":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '12'; })[0];
                        let bh = category.options.filter(function (el) { return el.id == '14'; })[0];
                        let bh_color = category.options.filter(function (el) { return el.id == '15'; })[0];

                        if (![null, ''].includes(bh.selectedValue()) && ![null, ''].includes(bh_color.selectedValue())) {
                            return [null, ''].includes(style.selectedValue()) ? false : true;

                        } else {
                            return false;
                        }
                    });

                // Sleeves
                case "7":
                    return ko.computed(function () {


                        let numberOfButtons = category.options.filter(function (el) { return el.id == '29'; })[0];

                        if ([null, ''].includes(numberOfButtons.selectedValue())) {
                            return false;
                        }

                        console.log('numberOfButtons.selectedValue().id', numberOfButtons.selectedValue().id);

                        switch (numberOfButtons.selectedValue().id) {
                            // 5 buttons
                            case "229":

                                var sleeve = category.options.filter(function (el) { return el.id == '42'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '47'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '52'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 4 buttons
                            case "228":

                                var sleeve = category.options.filter(function (el) { return el.id == '41'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '51'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '46'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 3 buttons
                            case "227":

                                var sleeve = category.options.filter(function (el) { return el.id == '40'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '45'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '50'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 2 buttons
                            case "226":

                                var sleeve = category.options.filter(function (el) { return el.id == '39'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '44'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '49'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 1 buttom
                            case "225":

                                var sleeve = category.options.filter(function (el) { return el.id == '38'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '43'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '48'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                                break;

                            default:
                                return false;
                        }

                        return [null, ''].includes(sleeve.selectedValue()) ? false :
                            [null, ''].includes(thread.selectedValue()) ? false :
                                [null, ''].includes(hole.selectedValue()) ? false : true;
                    });

                // Pocket Style
                case "8":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '33'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Breastpocket Style
                case "9":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '36'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General notes
                case "10":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '37'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Details
                case "11":
                    return ko.computed(function () {
                        let tuxedo_colour = category.options.filter(function (el) { return el.id == '20'; })[0];
                        let stitch_color = category.options.filter(function (el) { return el.id == '27'; })[0];
                        // return [null, ''].includes(tuxedo_colour.selectedValue()) ? false : [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                        return [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                    });

                // Monogram
                case "46":
                    return ko.computed(function () {
                        let lining = category.options.filter(function (el) { return el.id == '124'; })[0];
                        let pipping = category.options.filter(function (el) { return el.id == '125'; })[0];
                        let pocket_color = category.options.filter(function (el) { return el.id == '126'; })[0];
                        return [null, ''].includes(lining.selectedValue()) ? false :
                            [null, ''].includes(pipping.selectedValue()) ? false :
                                [null, ''].includes(pocket_color.selectedValue()) ? false : true;
                    });

                // Vest style
                case "71":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '251'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });



                // Pants
                // ██████╗  █████╗ ███╗   ██╗████████╗███████╗
                // ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
                // ██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
                // ██╔═══╝ ██╔══██║██║╚██╗██║   ██║   ╚════██║
                // ██║     ██║  ██║██║ ╚████║   ██║   ███████║
                // ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
                //
                // fabric
                case "12":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '54'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // fit
                case "13":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '56'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pant Syle
                case "14":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '57'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pleats
                case "15":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '59'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Front Pockets
                case "16":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '61'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Back Pockets
                case "17":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '63'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '65'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Belt & Cuffs
                case "18":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '66'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General Notes
                case "19":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '70'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });






                // Suit
                // ███████╗██╗   ██╗██╗████████╗
                // ██╔════╝██║   ██║██║╚══██╔══╝
                // ███████╗██║   ██║██║   ██║
                // ╚════██║██║   ██║██║   ██║
                // ███████║╚██████╔╝██║   ██║
                // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝
                //

                // Jacket style
                case '49':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '148'; })[0];
                        let b_color = category.options.filter(function (el) { return el.id == '151'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : [null, ''].includes(b_color.selectedValue()) ? false : true;
                    });

                // Fabric
                case '47':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '156'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Fit
                case '48':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '158'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom Style
                case '50':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '159'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Vent Style
                case '51':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '161'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Style
                case '52':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '164'; })[0];
                        let bh = category.options.filter(function (el) { return el.id == '166'; })[0];
                        let bh_color = category.options.filter(function (el) { return el.id == '167'; })[0];

                        if (![null, ''].includes(bh.selectedValue()) && ![null, ''].includes(bh_color.selectedValue())) {
                            return [null, ''].includes(style.selectedValue()) ? false : true;

                        } else {
                            return false;
                        }
                    });

                // Sleeves
                case '54':
                    return ko.computed(function () {


                        let numberOfButtons = category.options.filter(function (el) { return el.id == '169'; })[0];

                        if ([null, ''].includes(numberOfButtons.selectedValue())) {
                            return false;
                        }

                        console.log('numberOfButtons.selectedValue().id', numberOfButtons.selectedValue().id);

                        switch (numberOfButtons.selectedValue().id) {
                            // 5 buttons
                            case '2716':

                                var sleeve = category.options.filter(function (el) { return el.id == '177'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '181'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '186'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 4 buttons
                            case '2715':

                                var sleeve = category.options.filter(function (el) { return el.id == '176'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '224'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '185'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 3 buttons
                            case '2714':

                                var sleeve = category.options.filter(function (el) { return el.id == '175'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '180'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '184'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 2 buttons
                            case '2713':

                                var sleeve = category.options.filter(function (el) { return el.id == '174'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '179'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '183'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 1 buttom
                            case '2712':

                                var sleeve = category.options.filter(function (el) { return el.id == '173'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '178'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '182'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                                break;

                            default:
                                return false;
                        }

                        return [null, ''].includes(sleeve.selectedValue()) ? false :
                            [null, ''].includes(thread.selectedValue()) ? false :
                                [null, ''].includes(hole.selectedValue()) ? false : true;
                    });

                // Pocket Style
                case '55':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '187'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Breastpocket Style
                case '56':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '190'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General notes
                case '64':
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '210'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Details
                case '53':
                    return ko.computed(function () {
                        let tuxedo_colour = category.options.filter(function (el) { return el.id == '194'; })[0];
                        let stitch_color = category.options.filter(function (el) { return el.id == '201'; })[0];
                        // return [null, ''].includes(tuxedo_colour.selectedValue()) ? false : [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                        return [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                    });

                // Monogram
                case '63':
                    return ko.computed(function () {
                        let lining = category.options.filter(function (el) { return el.id == '203'; })[0];
                        let pipping = category.options.filter(function (el) { return el.id == '204'; })[0];
                        let pocket_color = category.options.filter(function (el) { return el.id == '205'; })[0];
                        return [null, ''].includes(lining.selectedValue()) ? false :
                            [null, ''].includes(pipping.selectedValue()) ? false :
                                [null, ''].includes(pocket_color.selectedValue()) ? false : true;
                    });



                // suit Pants
                // fabric: same as jacket suit
                // case ''____'':
                //     return ko.computed(function () {
                //         let style = category.options.filter(function (el) { return el.id == ''____''; })[0];
                //         return [null, ''].includes(style.selectedValue()) ? false : true;
                //     });

                // fit
                case '70':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '243'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pant Syle
                case '58':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '211'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pleats
                case '59':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '213'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Front Pockets
                case '60':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '215'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Back Pockets
                case '61':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '217'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '219'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Belt & Cuffs
                case '62':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '220'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General Notes
                // same as jacket
                // case ''____'':
                //     return ko.computed(function () {
                //         let style = category.options.filter(function (el) { return el.id == ''____''; })[0];
                //         return [null].includes(style.selectedValue()) ? false : true;
                //     });

                default:
                    return ko.observable(true);
            }
        },

        /**
         * This function set a design step as 'visited'. This is only for the next btn functionality.
         *
         * Called in a step changing
         *
         * @param selectedCategory
         * @param selectedGarment
         * @param status [boolean]
         */
        setOptionsVisited : function (selectedCategory, selectedGarment, status){
            selectedGarment.categories.filter(function(el){ return el.id == selectedCategory.id})[0].visited(status);
        },

        /**
         * This method will recover the garment information for each garment
         * and each design
         *
         * @param  {[type]} reorderGarments [description]
         * @return {[type]}             [description]
         */
        recoverGarments : function(reorderGarments) {

            console.log("reorderGarments", reorderGarments);

            for(let type of reorderGarments)
            {

                for(let garment of type.garments)
                {

                    // Gets the correct garment Type
                    const custGarments = this.customer.garmentsStep.garments().filter( garType => garType.id == type.id )[0].garments();

                    // Creates a 'blank' garment
                    const recentlyCreatedGarment = this.customer.garmentsDesignStep.getGarmentObject(type.id);

                    // Push to the garment type
                    custGarments.push(recentlyCreatedGarment);

                    // gets fabric
                    if (typeof garment.selectedFabric !== "undefined"){
                        const selectedFabric = fabricsData.store.filter( e => e.id == garment.selectedFabric.id )[0];
                        recentlyCreatedGarment.selectedFabric(selectedFabric);
                    }

                    recentlyCreatedGarment.unique_id = garment.unique_id;

                    // fix urgent_date from ERP (ERP sen a date in DD/MM/YYYY, must be in MM/DD/YYYY )
                    const date_split = garment.urgentDate.split(new RegExp("[/-]"));
                    garment.urgentDate = `${date_split[1]}/${date_split[0]}/${date_split[2]}`;
                    this.copyDesign(garment, recentlyCreatedGarment, is_reorder = true);
                }
            }


        },

        /**
         * Takes a custom image for designs
         * @return {[type]} [description]
         */
        takeOptionCustomImage : function(option) {

            self = this;

            console.log("Taking Pic...", option);

            // fake_successMove();

            navigator.notification.confirm(
                'Choose Picture Source', // message
                function(btnIndex) {

                    const source = btnIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

                    takeImg(source);

                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

            function takeImg(source)
            {
                navigator.camera.getPicture(onSuccess, onFail,
                {
                    quality: 10,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: source,
                    encodingType: Camera.EncodingType.JPEG,
                    mediaType: Camera.MediaType.PICTURE,
                    saveToPhotoAlbum: true,
                    correctOrientation : true,
                });
            }

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                console.log("entry.toURL()", entry.toURL());
                const valueUpdated = {
                    image : entry.toURL(),
                    tags : []
                }
                option.selectedValue.push(valueUpdated);

                self.customTagsHandlers = self._setCustomTagsHandlers();

                self.renderCustomTagsHandlers();

            };

            /**
             * Firefox can not use navigator object
             *
             * This is used only while developing
             */
            function fake_successMove(entry = {}){
                console.log('fake_successMove');
                entry.toURL = function(){
                    return Utils.getRandomImage();
                }

                successMove(entry);
            }

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        },

        clearCustomOptionImage : function(option) {

            // navigator.notification.confirm(
            //     'Do you really want to delete this image ?', // message
            //     function(btnIndex) {

            //         if(btnIndex == 1)
            //         {
            //             option.selectedValue(false);
            //         }

            //     },            // callback to invoke with index of button pressed
            //     'Confirmation',           // title
            //     ['Yes', 'No']         // buttonLabels
            // );
            const confirmAction = () => {
                option.selectedValue([]);

                this.customTagsHandlers = this._setCustomTagsHandlers();

                this.renderCustomTagsHandlers();

            };
            pos_warning({msg:'Do you really want to delete this image ?', confirmAction});

        },

        /**
         * This method maps a custom value id to the respective custom option to take the
         * custom picture
         *
         * for example: maps "custom bottom style" to option where the picture of the bottom is taken
         * @param {*} customValueId
         */
        getCustomOptionFromCustomValueId: function(customValueId){
            const map = [
                { "value_id": "1261", "custom_option": "226" },
                { "value_id": "1343", "custom_option": "229" },
                { "value_id": "1329", "custom_option": "249" },
                { "value_id": "4567", "custom_option": "252" },
                { "value_id": "1298", "custom_option": "227" },
                { "value_id": "1291", "custom_option": "228" },
                { "value_id": "25",   "custom_option": "230" },
                { "value_id": "1",    "custom_option": "231" },
                { "value_id": "31",   "custom_option": "232" },
                { "value_id": "27",   "custom_option": "233" },
                { "value_id": "4585", "custom_option": "257" },
                { "value_id": "2607", "custom_option": "235" },
                { "value_id": "2583", "custom_option": "236" },
                { "value_id": "2613", "custom_option": "237" },
                { "value_id": "2609", "custom_option": "238" },
                { "value_id": "3847", "custom_option": "234" },
                { "value_id": "4586", "custom_option": "258" }
            ];

            const custom_option_id = map.find(el => el.value_id == customValueId).custom_option;

            // now we know the option id, we must get the hole object
            const custom_option = (
                this.selectedGarment()
                .categories.find(cat => cat.name == "Custom Data")
                .options_map[parseInt(custom_option_id)]
            );

            return custom_option;

        },

        /**
         * Return an option list to show contents of the 'custom data' category of each garment type.
         * example: if custom collar is true for shirt ==> show UPLOAD CUSTOM IMAGE for collar in CUSTOM DATA
         */
        setHasCustomDesign :  function(){

            var self = this;

            return ko.computed(function(){
                var garment = self.ordersVM.currentGarmentInstance().garment;

                console.log('self.selectedGarment()', garment);

                if (!garment){
                    return false;
                }

                switch (garment.garment_id) {
                    // Search for selected custom data in PANTS (front pockets)
                    case '1':

                        // get front pocket value (if is custom front pocket return true)
                        var frontCategory = garment.categories.filter(function (el) { return el.id == '16' })[0];
                        var frontOption = frontCategory.options.filter(function (el) { return el.id == '61' })[0];

                        return frontOption.selectedValue() != null && frontOption.selectedValue().id == '1261' ? ['226'] : [];

                    // Search for selected custom data in SHIRT (front and collar and cuff)
                    case '2':
                        var customToShow = [];

                        var frontCategory = garment.categories.filter(function (el) { return el.id == '34' })[0];
                        var frontOption = frontCategory.options.filter(function (el) { return el.id == '96' })[0];

                        if (frontOption.selectedValue() != null && frontOption.selectedValue().id == '1343'){
                            customToShow.push('229');
                        }

                        var collarCategory = garment.categories.filter(function (el) { return el.id == '33' })[0];
                        var collarOption = collarCategory.options.filter(function (el) { return el.id == '94' })[0];
                        if (collarOption.selectedValue() != null && collarOption.selectedValue().id == '1329'){
                            customToShow.push('249');
                        }

                        var cuffCategory = garment.categories.filter(function (el) { return el.id == '32' })[0];
                        var cuffOption = cuffCategory.options.filter(function (el) { return el.id == '92' })[0];
                        if (cuffOption.selectedValue() != null && cuffOption.selectedValue().id == '4567'){
                            customToShow.push('252');
                        }

                        return customToShow;

                    // Search for selected custom data in VEST (custom button image and lapel image )
                    case '3':
                        var customToShow = [];
                        var buttonCategory = garment.categories.filter(function (el) { return el.id == '24' })[0];
                        var buttonOption = buttonCategory.options.filter(function (el) { return el.id == '78' })[0];

                        if (buttonOption.selectedValue() != null && buttonOption.selectedValue().id == '1298'){
                            customToShow.push('227');
                        }

                        var lapelCategory = garment.categories.filter(function (el) { return el.id == '22' })[0];
                        var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '74' })[0];

                        if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '1291'){
                            customToShow.push('228');
                        }

                        return customToShow;



                    // Search for selected custom data in JACKET (Custom Bottom Image; Custom Design Image; Custom Lapel Image; Custom Vent Image; Custom Monogram Image;)
                    case '4':

                        var customToShow = [];
                        var bottomCategory = garment.categories.filter(function (el) { return el.id == '4' })[0];
                        var bottomOption = bottomCategory.options.filter(function (el) { return el.id == '7' })[0];

                        if (bottomOption.selectedValue() != null && bottomOption.selectedValue().id == '25'){
                            customToShow.push('230');
                        }

                        var designCategory = garment.categories.filter(function (el) { return el.id == '1' })[0];
                        var designOption = designCategory.options.filter(function (el) { return el.id == '1' })[0];

                        if (designOption.selectedValue() != null && designOption.selectedValue().id == '1'){
                            customToShow.push('231');
                        }

                        var lapelCategory = garment.categories.filter(function (el) { return el.id == '6' })[0];
                        var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '12' })[0];

                        if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '31'){
                            customToShow.push('232');
                        }

                        var ventCategory = garment.categories.filter(function (el) { return el.id == '5' })[0];
                        var ventOption = ventCategory.options.filter(function (el) { return el.id == '9' })[0];

                        if (ventOption.selectedValue() != null && ventOption.selectedValue().id == '27'){
                            customToShow.push('233');
                        }

                        var breastpocketCategory = garment.categories.filter(function (el) { return el.id == '9' })[0];
                        var breastpocketOption = breastpocketCategory.options.filter(function (el) { return el.id == '36' })[0];

                        if (breastpocketOption.selectedValue() != null && breastpocketOption.selectedValue().id == '4585' ) {
                            customToShow.push('257');
                        }















                        // var monogramCategory = garment.categories.filter(function (el) { return el.id == '46' })[0];
                        // var monogramOption = monogramCategory.options.filter(function (el) { return el.id == '' })[0];

                        // if (monogramOption.selectedValue() != null && monogramOption.selectedValue().id == ''){
                        //     customToShow.push('244');
                        // }


                        return customToShow;



                    // Search for selected custom data in SUIT (Custom Bottom Image; Custom Design Image; Custom Lapel Image; Custom Vent Image; Custom Monogram Image;front pockets)
                    case '6':
                        var customToShow = [];
                        var bottomCategory = garment.categories.filter(function (el) { return el.id == '50' })[0];
                        var bottomOption = bottomCategory.options.filter(function (el) { return el.id == '159' })[0];

                        if (bottomOption.selectedValue() != null && bottomOption.selectedValue().id == '2607'){
                            customToShow.push('235');
                        }

                        var designCategory = garment.categories.filter(function (el) { return el.id == '49' })[0];
                        var designOption = designCategory.options.filter(function (el) { return el.id == '148' })[0];

                        if (designOption.selectedValue() != null && designOption.selectedValue().id == '2583'){
                            customToShow.push('236');
                        }

                        var lapelCategory = garment.categories.filter(function (el) { return el.id == '52' })[0];
                        var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '164' })[0];

                        if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '2613'){
                            customToShow.push('237');
                        }

                        var ventCategory = garment.categories.filter(function (el) { return el.id == '51' })[0];
                        var ventOption = ventCategory.options.filter(function (el) { return el.id == '161' })[0];

                        if (ventOption.selectedValue() != null && ventOption.selectedValue().id == '2609'){
                            customToShow.push('238');
                        }

                        // var monogramCategory = garment.categories.filter(function (el) { return el.id == '' })[0];
                        // var monogramOption = monogramCategory.options.filter(function (el) { return el.id ==  })[0];

                        // if (monogramOption.selectedValue() != null && monogramOption.selectedValue().id == ''){
                        //     customToShow.push('245');
                        // }

                        // get front pocket value (if is custom front pocket return true)
                        var frontCategory = garment.categories.filter(function (el) { return el.id == '60' })[0];
                        var frontOption = frontCategory.options.filter(function (el) { return el.id == '215' })[0];

                        if (frontOption.selectedValue() != null && frontOption.selectedValue().id == '3847'){
                            customToShow.push('234');
                        }


                        var breastpocketCategory = garment.categories.filter(function (el) { return el.id == '56' })[0];
                        var breastpocketOption = breastpocketCategory.options.filter(function (el) { return el.id == '190' })[0];

                        if (breastpocketOption.selectedValue() != null && breastpocketOption.selectedValue().id == '4586' ) {
                            customToShow.push('258');
                        }









                        return customToShow;

                    default:
                        break;
                }


                return false;
            });
        },

        /**
         * Do the same as method above, but for every garment of the current type
         */
        setAnyGarmentHasCustomDesign : function(){
            var self = this;

            /**
             * push element or array if not in original
             * Avoid duplicates
             */
            function pushUnique(array0, array1=[]) {
                if (array1!=[]){
                    for (const iterator of array1) {
                        if (!array0.includes(iterator)){
                            array0.push(iterator);
                        }
                    }
                }

                return array0;
            }

            return ko.computed(function(){

                if (!self.ordersVM.currentDesigingGarmentType()){
                    return false;
                }

                var response = [];

                for (const garment_object of self.ordersVM.globalGarments[self.ordersVM.currentDesigingGarmentType().garmentType]()) {
                    var garment = garment_object.garment;
                    if (!garment){
                        return false;
                    }

                    switch (garment.garment_id) {
                        // Search for selected custom data in PANTS (front pockets)
                        case '1':

                            // get front pocket value (if is custom front pocket return true)
                            var frontCategory = garment.categories.filter(function (el) { return el.id == '16' })[0];
                            var frontOption = frontCategory.options.filter(function (el) { return el.id == '61' })[0];

                            response = frontOption.selectedValue() != null && frontOption.selectedValue().id == '1261' ? pushUnique(response, ['226']) : response;
                            break;

                        // Search for selected custom data in SHIRT (front and collar and cuff)
                        case '2':
                            var customToShow = [];

                            var frontCategory = garment.categories.filter(function (el) { return el.id == '34' })[0];
                            var frontOption = frontCategory.options.filter(function (el) { return el.id == '96' })[0];

                            if (frontOption.selectedValue() != null && frontOption.selectedValue().id == '1343'){
                                customToShow.push('229');
                            }

                            var collarCategory = garment.categories.filter(function (el) { return el.id == '33' })[0];
                            var collarOption = collarCategory.options.filter(function (el) { return el.id == '94' })[0];
                            if (collarOption.selectedValue() != null && collarOption.selectedValue().id == '1329'){
                                customToShow.push('249');
                            }

                            var cuffCategory = garment.categories.filter(function (el) { return el.id == '32' })[0];
                            var cuffOption = cuffCategory.options.filter(function (el) { return el.id == '92' })[0];
                            if (cuffOption.selectedValue() != null && cuffOption.selectedValue().id == '4567'){
                                customToShow.push('252');
                            }

                            response = pushUnique(response, customToShow);
                            break;

                        // Search for selected custom data in VEST (custom button image and lapel image )
                        case '3':
                            var customToShow = [];
                            var buttonCategory = garment.categories.filter(function (el) { return el.id == '24' })[0];
                            var buttonOption = buttonCategory.options.filter(function (el) { return el.id == '78' })[0];

                            if (buttonOption.selectedValue() != null && buttonOption.selectedValue().id == '1298'){
                                customToShow.push('227');
                            }

                            var lapelCategory = garment.categories.filter(function (el) { return el.id == '22' })[0];
                            var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '74' })[0];

                            if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '1291'){
                                customToShow.push('228');
                            }

                            response = pushUnique(response, customToShow);
                            break;



                        // Search for selected custom data in JACKET (Custom Bottom Image; Custom Design Image; Custom Lapel Image; Custom Vent Image; Custom Monogram Image;)
                        case '4':

                            var customToShow = [];
                            var bottomCategory = garment.categories.filter(function (el) { return el.id == '4' })[0];
                            var bottomOption = bottomCategory.options.filter(function (el) { return el.id == '7' })[0];

                            if (bottomOption.selectedValue() != null && bottomOption.selectedValue().id == '25'){
                                customToShow.push('230');
                            }

                            var designCategory = garment.categories.filter(function (el) { return el.id == '1' })[0];
                            var designOption = designCategory.options.filter(function (el) { return el.id == '1' })[0];

                            if (designOption.selectedValue() != null && designOption.selectedValue().id == '1'){
                                customToShow.push('231');
                            }

                            var lapelCategory = garment.categories.filter(function (el) { return el.id == '6' })[0];
                            var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '12' })[0];

                            if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '31'){
                                customToShow.push('232');
                            }

                            var ventCategory = garment.categories.filter(function (el) { return el.id == '5' })[0];
                            var ventOption = ventCategory.options.filter(function (el) { return el.id == '9' })[0];

                            if (ventOption.selectedValue() != null && ventOption.selectedValue().id == '27'){
                                customToShow.push('233');
                            }

                            var breastpocketCategory = garment.categories.filter(function (el) { return el.id == '9' })[0];
                            var breastpocketOption = breastpocketCategory.options.filter(function (el) { return el.id == '36' })[0];

                            if (breastpocketOption.selectedValue() && breastpocketOption.selectedValue().id == '4585' ) {
                                customToShow.push('257');
                            }


                            // var monogramCategory = garment.categories.filter(function (el) { return el.id == '46' })[0];
                            // var monogramOption = monogramCategory.options.filter(function (el) { return el.id == '' })[0];

                            // if (monogramOption.selectedValue() != null && monogramOption.selectedValue().id == ''){
                            //     customToShow.push('244');
                            // }


                            response = pushUnique(response, customToShow);
                            break;



                        // Search for selected custom data in SUIT (Custom Bottom Image; Custom Design Image; Custom Lapel Image; Custom Vent Image; Custom Monogram Image;front pockets)
                        case '6':
                            var customToShow = [];
                            var bottomCategory = garment.categories.filter(function (el) { return el.id == '50' })[0];
                            var bottomOption = bottomCategory.options.filter(function (el) { return el.id == '159' })[0];

                            if (bottomOption.selectedValue() != null && bottomOption.selectedValue().id == '2607'){
                                customToShow.push('235');
                            }

                            var designCategory = garment.categories.filter(function (el) { return el.id == '49' })[0];
                            var designOption = designCategory.options.filter(function (el) { return el.id == '148' })[0];

                            if (designOption.selectedValue() != null && designOption.selectedValue().id == '2583'){
                                customToShow.push('236');
                            }

                            var lapelCategory = garment.categories.filter(function (el) { return el.id == '52' })[0];
                            var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '164' })[0];

                            if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '2613'){
                                customToShow.push('237');
                            }

                            var ventCategory = garment.categories.filter(function (el) { return el.id == '51' })[0];
                            var ventOption = ventCategory.options.filter(function (el) { return el.id == '161' })[0];

                            if (ventOption.selectedValue() != null && ventOption.selectedValue().id == '2609'){
                                customToShow.push('238');
                            }

                            var breastpocketCategory = garment.categories.filter(function (el) { return el.id == '56' })[0];
                            var breastpocketOption = breastpocketCategory.options.filter(function (el) { return el.id == '190' })[0];

                            if (breastpocketOption.selectedValue() != null && breastpocketOption.selectedValue().id == '4586' ) {
                                customToShow.push('258');
                            }


                            // var monogramCategory = garment.categories.filter(function (el) { return el.id == '' })[0];
                            // var monogramOption = monogramCategory.options.filter(function (el) { return el.id ==  })[0];

                            // if (monogramOption.selectedValue() != null && monogramOption.selectedValue().id == ''){
                            //     customToShow.push('245');
                            // }

                            // get front pocket value (if is custom front pocket return true)
                            var frontCategory = garment.categories.filter(function (el) { return el.id == '60' })[0];
                            var frontOption = frontCategory.options.filter(function (el) { return el.id == '215' })[0];

                            if (frontOption.selectedValue() != null && frontOption.selectedValue().id == '3847'){
                                customToShow.push('234');
                            }

                            response = pushUnique(response, customToShow);
                            break;

                        default:
                            break;
                    }

                }

                return response;
            });
        },

        /**
         *
         * If all design of all garments of all clients are complete, set status of the step as true.
         *
         * This will set the step tab green
         */
        toggleStepComplete : function() {


            // enter into all customers, all garment and all categories
            const stepFalse = (reason) => {
                console.log("step false. reason: ", reason);
                orders.steps.filter(function (el) { return el.id == '6' })[0].complete(false);
                return false;
            }


            for(let customer of this.ordersVM.customers())
            {
                for(let garmentType of customer.garmentsStep.garments() )
                {
                    for(let garment of garmentType.garments() )
                    {
                        // checks if the garment has fabric
                        if( !garment.selectedFabric() && !garment.useCustomerFabric() )
                        {
                            stepFalse("garment has no fabric");
                            orders.steps.filter(function (el) { return el.id == '6' })[0].complete(false);
                            return false;
                        }

                        if (garment.useCustomerFabric()){

                            // check "custom fabric make"
                            // fabric category for jacket: 2
                            // fabric category for suit  : 47
                            const fabricCategory = garment.categories.filter(cat => ["2", "47"].includes(cat.id))[0];
                            if (fabricCategory){
                                // custom make option for jacket : 255
                                // custom make option for suit   : 256
                                const fabricCustomMakeOption = fabricCategory.options.filter(opt => ["255","256"].includes(opt.id))[0];
                                if(fabricCustomMakeOption && !fabricCustomMakeOption.selectedValue()){
                                    stepFalse("No custom make option");
                                    orders.steps.filter(function (el) { return el.id == '6' })[0].complete(false);
                                    return false;
                                }
                            }

                            // check custom fabric picture
                            if (!garment.customerFabricImage()){
                                stepFalse("No customer fabric image");
                                orders.steps.filter(function (el) { return el.id == '6' })[0].complete(false);
                                return false;
                            }


                            // check fabric meterage
                            if (!garment.customerFabricMeterage()){
                                stepFalse("no metarage");
                                orders.steps.filter(function (el) { return el.id == '6' })[0].complete(false);
                                return false;
                            }
                        }

                        // check all categories of garment are complete
                        garment.categories.forEach(category => {
                            if (!category.completed() && !category.name.toLowerCase().includes("note")){
                                stepFalse("Category incomplete: ", category);
                                orders.steps.filter(function (el) { return el.id == '6' })[0].complete(false);
                                return false;
                            }
                        })

                    }
                }
            }

            // if leave without returning == all steps are complete
            orders.steps.filter(function (el) { return el.id == '6' })[0].complete(true);
            return true;


        },

        /**
        * For UI purposes, Check if all garments for one customer is complete
        * @returns {Boolean}
        */
        isAllGarmentComplete(client) {
            // enter into a all garment and all categories

            for (const garmentType of client.garmentsStep.garments()) {
                for (const garment of garmentType.garments()) {
                    for (const category of garment.categories) {
                        if (!category.completed()) {
                            return false;
                        }
                    }
                }
            }

            return true;

        },

        /**
        * For UI purposes, Check if ONE garment for one customer is complete
        * @returns {Boolean}
        */
        isGarmentComplete(garment) {

            for (const category of garment.categories) {
                if (!category.completed()) {
                    return false;
                }
            }

            return true;
        },

        /**
         * Checks if all [jakcet/pants/suit/vest/shirt] of all clients are complete
         *
         * Checks garment complete by garmentType
         */
        isGarmentsComplete(garmentType){
            var self = this;

            return ko.computed(function(){
                // wrong garment type
                if  (! Object.keys(self.ordersVM.globalGarments).includes(garmentType)){
                    return false;
                }

                const categories = self.ordersVM.globalDesignCategoriesArray().filter(el => el.garmentType == garmentType)[0].categories;
                for (const category of categories) {
                    if (!category.completed() && !category.name.toLowerCase().trim().includes("custom data")) {
                        // console.log('INCOMPLETE', category);
                        return false;
                    }
                }

                // console.log('COMPLETE');
                return true;

            });
        },

        /**
         * For a jacket and suit, return the sleeve number to hide/show contents in UI
         *
         *
         */
        getSleeveNumber : function (garment = null) {
            var self = this;

            if (garment == null){
                garment = self.selectedGarment();
            }


            return ko.computed(function () {
                // for jacket
                try {
                    var sleeveCategory = garment.categories.filter(function (el) { return el.id == '7' })[0];
                    var sleeveNumberOption = sleeveCategory.options.filter(function (el) { return el.id == '29' })[0];
                    return parseInt(sleeveNumberOption.selectedValue().name);

                } catch (TypeError) {
                    // not a jacket
                    // return 0;
                }

                // for suit
                try {
                    var sleeveCategory = garment.categories.filter(function (el) { return el.id == '54' })[0];
                    var sleeveNumberOption = sleeveCategory.options.filter(function (el) { return el.id == '169' })[0];
                    return parseInt(sleeveNumberOption.selectedValue().name);

                } catch (TypeError) {
                    // not a suit
                    // return 0;
                }

                return 0;

            });
        },


        /**
         * Returns a ko.computed that says if button tuxedo is ON or OFF
         */
        checkButtonsTuxedo : function(){
            var self = this;
            return ko.computed(function(){
                // check type of current garment
                const garmentType = self.ordersVM.currentGarmentInstance().garmentType;
                // const garmentType = self.ordersVM.currentDesigingGarmentType().garmentType;

                var lapelDetailsCategory;
                var buttonsTuxedoOption;
                if (garmentType == "jacket"){
                    lapelDetailsCategory = 11;
                    buttonsTuxedoOption = 23;
                }
                else if (garmentType == "suit"){
                    lapelDetailsCategory = 53;
                    buttonsTuxedoOption = 197;
                }
                else {
                    // end function
                    return;
                }

                return self.ordersVM.currentGarmentInstance().garment.categories_map[lapelDetailsCategory].options_map[buttonsTuxedoOption].selectedValue();

            });
        },

        /**
         * auxiliary function to crate a new extra garment.
         * An extra garment can be a pants or a vest.
         *
         * @param {garment [type:suit]} fromSuit
         * @param {String} garmentTypeId ["1" or "3"]
         * @param {garment [type: pants or vest]} toGarment
         * @param {List} optionsMap required keys: [{"suit_category_id" "suit_option_id" "option_name" "new_garment_cat_id" "new_garment_option_id"}]
         */
        _addExtraGarment(fromSuit, toGarment, optionsMap, garmentTypeId) {
            let garmentName;
            switch (garmentTypeId) {
                case '1': garmentName = 'pants'; break;
                case '2': garmentName = 'shirt'; break;
                case '3': garmentName = 'vest'; break;
                case '4': garmentName = 'jacket'; break;
                case '6': garmentName = 'suit'; break;
            }

            // copy categories from suit
            for (const to_garments_category of toGarment.categories) {
                for (const to_garments_option of to_garments_category.options) {
                    const map = optionsMap.filter(el => el.new_garment_option_id == to_garments_option.id)[0];

                    // if category was not mapped
                    if (!map) { continue; }

                    const suit_category = map.suit_category_id;
                    const suit_option = map.suit_option_id;

                    const suitSelectedValue = fromSuit.categories_map[suit_category].options_map[suit_option].selectedValue();

                    // if option has values, like radio_button type or colours
                    if (suitSelectedValue && to_garments_option.values) {
                        const analogue_value_new_garment = to_garments_option.values.filter(el => el.name == suitSelectedValue.name)[0];
                        analogue_value_new_garment.selected(true);
                        this.setValueClicked(to_garments_option, analogue_value_new_garment);
                        this.setOptionValue(to_garments_option, analogue_value_new_garment, null);
                    }

                    // if option is a string, or int, or boolean
                    else {
                        to_garments_option.selectedValue(suitSelectedValue);
                    }

                }
                to_garments_category.visited(true);
            }

            // fabric is different
            toGarment.isUrgent(fromSuit.isUrgent());
            toGarment.selectedFabric(fromSuit.selectedFabric());
            toGarment.selected_day(fromSuit.selected_day());
            toGarment.selected_month(fromSuit.selected_month());
            toGarment.selected_year(fromSuit.selected_year());
            toGarment.useCustomerFabric(fromSuit.useCustomerFabric());
            toGarment.fabricInsearch(fromSuit.fabricInsearch());

            this.ordersVM.selectGarmentFabric(fromSuit.selectedFabric(), toGarment, false)

            // push new garment into garments list
            for (let garment of this.ordersVM.selectedCustomer().garmentsStep.garments()) {
                if (garment.id == garmentTypeId) {
                    garment.garments.push(toGarment);

                    // Adds the garment to the global garments obj
                    const globalGarmentObj = {
                        "customer": this.ordersVM.selectedCustomer(),
                        "garmentType": garmentName,
                        "garment": toGarment,
                    }

                    this.ordersVM.globalGarments[garment.name].push(globalGarmentObj)
                    this.ordersVM.globalGarments[garment.name].sort(function (a, b) {
                        return a.customer.full_name > b.customer.full_name;
                    });
                }
            }
        },

        /**
         * Build a extra pants from a suit
         *
         * copy => fabric and all options of a pants
         */
        addExtraPants(fromSuit) {

            const confirmAction = () => {
                // respective options of a suit's pants to pant
                // map options and categories ids from pants to suit's pants
                const optionsMap = [
                    { suit_category_id: '58',  suit_option_id: '211',  option_name: 'Pant Style',              new_garment_cat_id : '14',    new_garment_option_id : '57'   },
                    { suit_category_id: '58',  suit_option_id: '212',  option_name: 'Pant Style Notes',        new_garment_cat_id : '14',    new_garment_option_id : '58'   },
                    { suit_category_id: '59',  suit_option_id: '213',  option_name: 'Pleats Style',            new_garment_cat_id : '15',    new_garment_option_id : '59'   },
                    { suit_category_id: '59',  suit_option_id: '214',  option_name: 'Pleats Notes',            new_garment_cat_id : '15',    new_garment_option_id : '60'   },
                    { suit_category_id: '60',  suit_option_id: '215',  option_name: 'Front Pockets Style',     new_garment_cat_id : '16',    new_garment_option_id : '61'   },
                    { suit_category_id: '60',  suit_option_id: '216',  option_name: 'Front Pockets Notes',     new_garment_cat_id : '16',    new_garment_option_id : '62'   },
                    { suit_category_id: '61',  suit_option_id: '217',  option_name: 'Back Pockets Style',      new_garment_cat_id : '17',    new_garment_option_id : '63'   },
                    { suit_category_id: '61',  suit_option_id: '218',  option_name: 'Back Pockets Notes',      new_garment_cat_id : '17',    new_garment_option_id : '64'   },
                    { suit_category_id: '61',  suit_option_id: '219',  option_name: 'Back Pockets Position',   new_garment_cat_id : '17',    new_garment_option_id : '65'   },
                    { suit_category_id: '62',  suit_option_id: '220',  option_name: 'Belt Loop' ,              new_garment_cat_id : '18',    new_garment_option_id : '66'   },
                    { suit_category_id: '62',  suit_option_id: '221',  option_name: 'Belt Notes',              new_garment_cat_id : '18',    new_garment_option_id : '67'   },
                    { suit_category_id: '62',  suit_option_id: '222',  option_name: 'Belt Cuffs',              new_garment_cat_id : '18',    new_garment_option_id : '68'   },
                    { suit_category_id: '62',  suit_option_id: '223',  option_name: 'Braces',                  new_garment_cat_id : '18',    new_garment_option_id : '69'   },
                    { suit_category_id: '64',  suit_option_id: '210',  option_name: 'General notes',           new_garment_cat_id : '19',    new_garment_option_id : '70'   },
                    { suit_category_id: '69',  suit_option_id: '234',  option_name: 'Custom Pocket Image',     new_garment_cat_id : '65',    new_garment_option_id : '226'  },
                    { suit_category_id: '70',  suit_option_id: '243',  option_name: 'Pant Fit',                new_garment_cat_id : '13',    new_garment_option_id : '56'   },
                ];

                // build new pants object
                const newPants = this.getGarmentObject(1);

                this._addExtraGarment(fromSuit, newPants, optionsMap, "1");
            }

            pos_confirm({msg: "Are you sure you want to make another pants?", confirmAction})


        },

        /**
         * Build a extra vest from a suit
         *
         * copy => only fabric and fit
         */
        addExtraVest(fromSuit) {
            const confirmAction = () => {
                const optionsMap = [
                    { suit_category_id: '48', suit_option_id: '158', option_name: 'Fit', new_garment_cat_id: '21', new_garment_option_id: '73' },
                    { suit_category_id: '63', suit_option_id: '203', option_name: 'Lining', new_garment_cat_id: '27', new_garment_option_id: '132' },
                    { suit_category_id: '63', suit_option_id: '204', option_name: 'Piping', new_garment_cat_id: '27', new_garment_option_id: '133' },
                ];

                // build new pants object
                const newVest = this.getGarmentObject(3);

                this._addExtraGarment(fromSuit, newVest, optionsMap, "3");
            };

            pos_confirm({ msg: "Are you sure you want to make another vest?", confirmAction });


        },

        _setCustomTagsHandlers(){
            var self = this;

            return ko.computed(function() {

                // get list before update
                const old_handlers = (self.customTagsHandlers && self.customTagsHandlers()) ? self.customTagsHandlers() : [];
                // will be the new list
                const imagesTagsHandles = [];

                if (!self.hasCustomDesign || !self.hasCustomDesign()){
                    return [];
                }

                for (const customOptionId of self.hasCustomDesign()) {

                    const customCat = self.ordersVM.currentGarmentInstance().garment.categories.find(cat => cat.name.toLowerCase().includes("custom"));
                    if (!customCat){ continue; }

                    const customOption = customCat.options.find(option => option.id == customOptionId);
                    if (!customOption){ continue; }

                    for (const image_index in customOption.selectedValue()) {
                        const image = customOption.selectedValue()[image_index];
                        if (!image){ continue; }

                        const path = JSON.parse(JSON.stringify(image.image));
                        // if (!path){ continue; }


                            const newHandler = new DesignImageTags(`image-tag-wrapper-${customOptionId}-${image_index}`,
                                `image-tags-comment-wrapper-${customOptionId}-${image_index}`,
                                "popupNewTag-wrapper",
                                {"id" : `${customOptionId}-${image_index}`, "path": path },
                                customCat,
                                customOption,
                                image_index
                            );
                            imagesTagsHandles.push( newHandler );


                    }



                }

                return imagesTagsHandles;
            });
        },

        renderCustomTagsHandlers(){
            for (const customImageHandler of this.customTagsHandlers()) {
                customImageHandler.render();
            }
        },

        cloneShirtMonogram(currentShirt){

            const category = currentShirt.garment.categories_map[39];

            // get monogram
            const monogram = category.options_map[118].selectedValue();

            // get monogram position
            const monogramPosition = category.options_map[119].selectedValue();

            // get monogram font
            const monogramFont = category.options_map[122].selectedValue();

            // replicate this values for all other shirts
            // get only shirts of this customer
            const shirtsOfThisUser = this.ordersVM.globalGarments.shirt().filter(
                shirt =>
                    shirt.customer.customer_id === this.customer.customer_id
            )

            for (const shirt of shirtsOfThisUser) {
                const currentShirtCategory = shirt.garment.categories_map[39];

                // apply to monogram
                currentShirtCategory.options_map[118].selectedValue(monogram);

                // apply to monogram position
                // get value for this garment
                const currentShirtPosition = currentShirtCategory.options_map[119].values.find(value => value.id == monogramPosition.id);
                this.setOptionValue(currentShirtCategory.options_map[119], currentShirtPosition);

                // apply to monogram font
                // get value for this garment
                const currentShirtFont = currentShirtCategory.options_map[122].values.find(value => value.id == monogramFont.id);
                this.setOptionValue(currentShirtCategory.options_map[122], currentShirtFont);

            }

            // ok popup
            pos_ok({
                msg:"",
                title: "Monogram copied successfully"
            });
        }


    });









});