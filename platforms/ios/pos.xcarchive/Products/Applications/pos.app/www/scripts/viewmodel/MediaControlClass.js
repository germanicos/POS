define(['jquery', 'knockout', 'base'], function ($, ko) {
	
	/**
	** Class that will control the media that will be uploaded. The main features are
	** media: is an observable object used to get the media that was selected last and add it to the medias already selected.
	** medias: is an observable array which contains all medias selected to upload 
	** upload: method that will upload the information into the server. However this method will not upload the file, only the file info.
	** TODO:
	** 1 - Modify the function to chose an existing image. Now, as it is, it does not work properly.
	** 2 - Modify the upload function to be less specific
	** 3 - Save the information about the device ID
	**/
	MediaUploadVM = SimpleControl.extend({
		init: function (DsRegistry) {
			//Will store the full path of the image to be written on the json
			var that = this;
			this.ID = 0;
			//this.tag = ko.observableArray();
			this.current = ko.observable();
			this.picSource = '';
			this.imagPath = '';
			this.upPath = '';
			this.file = '';
			this.preview = ko.observable();
			this.pictureTaken = ko.observable();
			this.medias = ko.observableArray();
			this.media = ko.observable();
			this.mediaID = ko.observable();
			this.mediaType = ko.observable();
			this.imageGaleryAction = '';
			this.videoGaleryAction = '';
			this.callbackMedia = '';
			//get the necessary information to exhibit the media to the application and the info needed to upload it.
			this.media.subscribe(function (data) {
				if (data) {
					if (that.mediaType() == 1) {
						var tag = ko.observable();
					} else {
						var tag = ko.observableArray();
					}
					that.medias.push({ media_type: that.mediaType(), media_path: that.imagPath.replace(/file:\/\//, ''), media_file: data, upload_path: that.upPath, tag: tag, ID: that.ID });
					that.preview(that.imagPath.replace(/file:\/\//, ''));
					that.ID += 1;
					if (that.callbackMedia) {
						that.callbackMedia(data);
					}
					//alert(errorReportVM.mediaUpload.medias().length);
				}
			});
			
			this.setMediaCallback = function (callback) {
				that.callbackMedia = callback;
			};
			
			//Method that allows the user chose between taking a new photo and 
			chooseGalleryCamera = function () {
				that.mediaType('2');
				that.pictureTaken(false);
				navigator.notification.confirm(
					'Source', // message
					onConfirmCamera,            // callback to invoke with index of button pressed
					'',           // title
					['Camera', 'Gallery']         // buttonLabels
				);
			};
			
			chooseGalleryVideo = function () {
				that.pictureTaken(false);
				that.mediaType('1');
				navigator.notification.confirm(
					'Source', // message
					onConfirmVideo,            // callback to invoke with index of button pressed
					'',           // title
					['Camera', 'Gallery']         // buttonLabels
				);
			};
			
			
			/****************************Camera Photo Functions*************************************/
			onConfirmCamera = function (buttonIndex) {
				if (buttonIndex === 1) {
					capturePhoto("camera");
				}
				else if (buttonIndex === 2) {
					capturePhoto("gallery");
					//that.imageGaleryAction();
				}
			};
			
			callGalleryAction = function () {
				that.imageGaleryAction();
			};
			
			capturePhoto = function (source) {

				console.log("Capturing PHOTO MediaControlCalss.js FUNCTION = capturePhoto");
				
				if (source === "camera") {
					that.picSource = Camera.PictureSourceType.CAMERA;
					navigator.device.capture.captureImage(onNewPhotoDataSuccess, onFail, {quality: 10} );
				}
				else {
					that.picSource = Camera.PictureSourceType.PHOTOLIBRARY;
					navigator.camera.getPicture(onPhotoDataSuccess, onFail,
						{
							quality: 10,
							destinationType: Camera.DestinationType.FILE_URI,
							sourceType: that.picSource,
							saveToPhotoAlbum: true,
							correctOrientation : true,

						});
					}
					
					
				};
				
				//Resolving the URL the system returned for an existing file
				onPhotoDataSuccess = function (imageURI) {
					that.pictureTaken(true);
					window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
				};
				
				//getting the file info from the picture taken
				onNewPhotoDataSuccess = function (images) {
					that.pictureTaken(true);
					for (var x in images) {
						that.imagPath = images[x].localURL;
						window.resolveLocalFileSystemURL(images[x].localURL, resolveOnSuccess, resOnError);
					}
				};
				
				mediaFileDataSuccess = function (data) {
					console.log(JSON.stringify(data));
				};
				
				failFileData = function (data) {
					alert('fail');
				};
				
				//Callback function when the file system uri has been resolved
				resolveOnSuccess = function (entry) {
					var d = new Date();
					var n = d.getTime();
					//new file name
					var newFileName = n;
					if (that.mediaType() == '1') {
						newFileName = newFileName + '.mov';
					}
					else {
						newFileName = newFileName + '.jpg';
					}
					var myFolderApp = "ClientMediaStorage";
					
					imageName = n;
					
					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
						//The folder is created if doesn't exist
						fileSys.root.getDirectory(myFolderApp,
							{ create: true, exclusive: false },
							function (directory) {
								entry.moveTo(directory, newFileName, successMove, resOnError);
							},
							resOnError);
						},
						resOnError);
				};




					function successMove(entry) {
						console.log('After move');
						console.log(entry.toURL());
						console.log(entry.toNativeURL());
						console.log(entry.fullPath);
						that.imagPath = entry.toURL();
						that.upPath = entry.toNativeURL();
						that.media(entry);
						imageURI = entry.fullPath;
					}
					
					/***********************************Video Camera functions***************************************/
					onConfirmVideo = function (buttonIndex) {
						if (buttonIndex === 1) {
							captureVideo("camera");
						}
						else if (buttonIndex === 2) {
							captureVideo("gallery");
						}
					};
					
					captureVideo = function (source) {


						if (source === "camera") {
							navigator.device.capture.captureVideo(onVideoDataSuccess, onFail, { quality: 10 });
						}
						//take video from gallery
						else {
							console.log("Entering video Gallery");
							
							navigator.camera.getPicture(onGalleryDataSuccess, onFail, {
								quality: 10,
								destinationType: Camera.DestinationType.FILE_URI,
								sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
								mediaType: Camera.MediaType.VIDEO,
								correctOrientation : true
							});
						}
					};
					
					onVideoDataSuccess = function (mediaFiles) {
						that.pictureTaken(true);
						console.log("onVideoDataSuccess");
						var i, path, len;
						for (i = 0, len = mediaFiles.length; i < len; i += 1) {
							//that.imagPath = mediaFiles[i].fullPath.replace("/private","");
							console.log(mediaFiles[i].localURL);
							console.log(mediaFiles[i].fullPath);
							//that.imagPath = mediaFiles[i].fullPath.replace("/private","file:/");
							window.resolveLocalFileSystemURL(mediaFiles[i].localURL, resolveOnSuccess, resOnError);
							//console.log(that.imagPath);
							//that.upPath = mediaFiles[i].localURL;
							//that.media(mediaFiles[i]);
						}
					};
					
					onGalleryDataSuccess = function (videoURI) {
						that.pictureTaken(true);
						console.log("onGalleryDataSuccess");
						window.resolveLocalFileSystemURL(videoURI, resolveVideoOnSuccess, resOnError);
					};
					
					resolveVideoOnSuccess = function (entry) {
						that.imagPath = entry.toURL();
						that.upPath = entry.toNativeURL();
						that.media(entry);
					};
					
					/****************Error logging******************/
					onFail = function (message) {
						console.log('Failed because: ' + message);
					};
					
					resOnError = function (error) {
						console.log("Failed... check console");
						console.log(error);
					};
					/*********************************************/
					
					uploadMediaInfo = function (url, mediaType, mediaLocalPath, mediaTags, additionalData) {
							
						console.log("Uploading Media info...", mediaLocalPath);

						mediaData = {
							media_type: mediaType,
							media_local_path: mediaLocalPath,
							media_tags: JSON.stringify(mediaTags),
							device_id: typeof device != "undefined" ? device.uuid : ""
						};
						
						salesman = {
							id: authCtrl.userInfo.user_id,
							username: authCtrl.username(),
							password: authCtrl.password()
							
						};
						
						//prepare the autentification data
						if (additionalData)
						additionalDatapost = JSON.stringify(additionalData);
						else
						additionalDatapost = null;
						mediaDatapost = JSON.stringify(mediaData);
						salesmanpost = JSON.stringify(salesman);
						timestamp = Math.round(new Date().getTime() / 1000);
						

						$.ajax({
							type: 'POST',
							url: url,
							data: {
								additional_data: additionalDatapost,
								media_data: mediaDatapost,
								salesman: salesmanpost,
								timestamp: timestamp
							},
							success: function (dataS) {

								that.mediaID(dataS.replace(/[^0-9]/, ''));
								
							},
							async: false
						});
					};
					
					//upload all the media file to the server additional 
					uploadMediasInfo = function (url, additionalData) {

						for (var ind in that.medias()) 
						{
							try
							{
								sleep(1000).then(() => {
									
									uploadMediaInfo(url, that.medias()[ind].media_type, that.medias()[ind].upload_path, that.medias()[ind].tag(), additionalData);

								});
							}
							catch(err)
							{
								console.log("err", err);
								uploadMediaInfo(url, that.medias()[ind].media_type, that.medias()[ind].upload_path, that.medias()[ind].tag(), additionalData);
							}
							
						}
						
					};
					
					reset = function () {
						that.media(null);
						that.medias.removeAll();
					};
					
					uploadCurrentMediaInfo = function (serverPath, additionalData) {
						uploadMediaInfo(serverPath, that.mediaType(), that.upPath, that.medias()[that.medias().length - 1].tag(), additionalData);
					};
					
					uploadCurrentMediaInfoFittings = function (serverPath, additionalData) {
						uploadMediaInfo(serverPath, that.mediaType(), that.upPath, null, additionalData);
					};
					
				},
				
				/*******************************************Call the main functions*********************************************************/
				
				clear: function () {
					reset();
				},
				
				choosePhoto: function () {
					chooseGalleryCamera();
				},
				
				choosePhoto2: function () {
					capturePhoto("camera");
				},
				
				manageTags: function (data) {
					
				},
				
				chooseVideo: function () {
					chooseGalleryVideo();
				},
				
				uploadAllMedias: function (serverPath, additionalData) {
					uploadMediasInfo(serverPath, additionalData);
				},
				
				uploadCurrentMedia: function (serverPath, additionalData) {
					uploadCurrentMediaInfo(serverPath, additionalData);
				},
				
				uploadCurrentMediaFittings: function (serverPath, additionalData) {
					uploadCurrentMediaInfoFittings(serverPath, additionalData);
				},
				
			});
			
			
			/**
			** This class is in charge of getting the info of the files that need to be uploaded and then upload it to the server
			** It will consult the database with post request and then upload all the files
			** It needs to be improved in order to work with features other then error report
			** The main features are:
			** syncFiles - function that will request from the server all the information needed to upload files, if necessary
			** uploadFile - function that will upload the file to the server and then
			** TODO:
			** 1 - Delete all the files after the upload finishes.
			** 2 - Use device ID to identify where the media is from
			** 3 - Modify how the function gets the info and the URL it uploads. This needs to be done after an endpoint only for media is created.
			**/
			SynchronizeFiles = SimpleControl.extend({
				init: function (DsRegistry) {
					
					var that = this;
					this.medias = ko.observableArray();
					this.checked = ko.observable();
					this.progress = ko.observable(0);
					this.max = ko.observable();
					this.step = ko.observable();
					this.currentFile = ko.observable(0);
					this.currentFileText = ko.observable('');
					this.currentFileIndex = ko.observable();
					this.isWorking = ko.observable(false);
					this.isBackground = ko.observable(false);
					this.backgroundControl = true;
					this.viewd = ko.observable(false);
					this.overwrite = true;
					this.redirect = function () {
						
						posChangePage('#mediaUploadCenter');
						
						// if (orderItem.SyncFilesVM.isWorking()) {
						// 	alert('The system is working on the background!');
						// } else {
						// 	posChangePage('#mediaUploadCenter');
						// }
					};

					this.isBackground.subscribe(function (data) {
						that.backgroundControl = data;
					});
					this.getNewInfo = function () {
						if (that.isBackground() && !that.viewd() && that.medias().length > 0) {
							if (!that.isWorking()) {
								that.viewd(true);
							}
							return false;
						}
						else {
							return true;
						}
					};
					
					background = function (data) {
						that.isBackground(!that.isBackground());
						var checkbox = document.getElementById('backgroundbtn');
						
						if (that.isBackground()) {
							checkbox.classList.add('selecteds');
						}
						else {
							checkbox.classList.remove('selecteds');
						}
					};
					
					test = function (data) { // 0: element name
						
						console.log("data", data);

						data.selected(!data.selected());
						var checkboxes = document.getElementsByName('simpleclick');
						var x = 0;
						
						for (var a in that.medias()) {
							if (that.medias()[a].selected() == true) {
								checkboxes[x].classList.add('selecteds');
							} else {
								checkboxes[x].classList.remove('selecteds');
							}
							x++;
						}
					};
					
					//This function is placed here because it was getting an error, accusing that uploadFile was not an function. 
					//AS parameter this function will receive the file's path, the server URL and the media ID and will do the upload
					uploadFile = function (server, filePath, mediaID) {
						//plugin FileTransfer needed. It will be used to upload the picture. It sends the file and all the parameters to the server that are put into the object FileUploadOptions.params
						that.isWorking(true);
						that.step("Preparing info");
						var ft = new FileTransfer();
						var temp = filePath.substr(filePath.lastIndexOf('/') + 1);
						temp = temp.substr(temp.lastIndexOf('.') + 1);
						
						var options = new FileUploadOptions();
						options.fileKey = "file";
						options.fileName = "upload";
						
						var params = {};
						params.salesman = JSON.stringify({
							id: authCtrl.userInfo.user_id,
							username: authCtrl.username(),
							password: authCtrl.password()
							
						});
						
						//data needed to update the database to sync files
						params.data = JSON.stringify({
							media_id: mediaID,
							media_extension: temp
						});
						
						options.params = params;
						//options.chunkedMode = false;
						console.log("sending");
						
						
						that.step("Finding file.");
						//get the file and upload it
						window.resolveLocalFileSystemURL(filePath, function (file) {
							that.step("Uploading file.");
							ft.upload(file.nativeURL, encodeURI(server), win, fail, options);
							//ft.upload(file.toURL(), encodeURI(server), win, fail, options);//for android
						}, failFile);
						
						
						
					};
					win = function (r) {
						if (that.isWorking()) {
							that.step('File Uploaded successfully');
							//that.isWorking(false);
							that.progress(that.progress() + 1);
							that.currentFile().status = 'Uploaded successfully';
							that.currentFile().upload = false;
							uploadNextMedia();
						}
						//alert("upload finished successfully!");
						console.log("Response = " + r.response);
						console.log("Response code = " + r.responseCode);
						console.log("Bytes sent = " + r.bytesSent);
						console.log(r.response);
					};
					
					failFile = function (error) {
						if (that.isWorking()) {
							that.step('File not found');
							//that.isWorking(false);
							that.progress(that.progress() + 1);
							that.currentFile().status = 'File not found';
							that.currentFile().error(true);
							uploadNextMedia();
						}
						//alert("file not found!");
						console.log("error = " + error.code);
					};
					
					fail = function (error) {
						//alert("upload finished unsuccessfully!");
						if (that.isWorking()) {
							$.ajax({
								type: 'POST',
								url: that.currentFile().check,
								data: {
									media: that.currentFile().media_id,
									salesman: salesmanpost,
									timestamp: timestamp
								},
								success: function (dataS) {
									if (dataS == "0") {
										that.step('upload unsuccessfully!');
										//that.isWorking(false);
										that.progress(that.progress() + 1);
										that.currentFile().status = 'upload unsuccessfully';
										that.currentFile().error(true);
										uploadNextMedia();
									}
									else {
										that.step('File not found');
										//that.isWorking(false);
										that.progress(that.progress() + 1);
										that.currentFile().status = 'File not found';
										that.currentFile().error(true);
										uploadNextMedia();
									}
								},
								error: function () {
									that.step('upload unsuccessfully!');
									//that.isWorking(false);
									that.progress(that.progress() + 1);
									that.currentFile().status = 'upload unsuccessfully';
									that.currentFile().error(true);
									uploadNextMedia();
								},
								async: false
							});
							
						}
						console.log("target: " + error.target + " source: " + error.source);
						console.log("error = " + error.code + " " + error.body + " " + error.exception);
						console.log("error = " + error.code + " " + error.body + " " + error.exception);
					};
					
					uploadNextMedia = function () {
						if (that.currentFileIndex() < that.medias().length) {
							that.currentFile(that.medias()[that.currentFileIndex()]);
							that.currentFileIndex(that.currentFileIndex() + 1);
							if (that.currentFile().upload) {
								if ((that.currentFile().selected() && that.checked()) || !that.checked()) {
									that.currentFileText(that.currentFile().media_id);
									console.log(JSON.stringify(that.currentFile()));
									uploadFile(that.currentFile().endpoint, that.currentFile().local_path, that.currentFile().media_id);
								}
								else {
									uploadNextMedia();
								}
							}
							
						}
						else {
							that.checked(false);
							that.isWorking(false);
							if (!that.isBackground()) {
								orderItem.SyncFilesVM.overwrite = false;
								posChangePage('#mediaUploadCenter');
							}
							else {
								orderItem.SyncFilesVM.overwrite = true;
								$.jGrowl('Sync process complete!');
								alert('Sync process complete!');
							}
						}
					};
					
					manageMedia = function (data) {
						orderItem.MediaManagerVM = new MediaManagerVM(DsRegistry);
						orderItem.MediaManagerVM.setInfo(data.section, data);
						posChangePage('#mediaManager');
					};
					
				},
				
				deleteChecked: function () {
					var that = this;
					var server = BUrl;
					if (confirm('Do you want to delete all the selected files?')) {


						for (var x in this.medias()) {
							if (this.medias()[x].selected()) {
								var endpoint = that.medias()[x].delete;
								switch (that.medias()[x].section) {
									case "Error":
									endpoint = 'error_report_endpoint/delete_media_info';
									break;
									
									case "Fittings":
									if (that.medias()[x].media_type == 'Video') {
										endpoint = 'client_fittings/delete_video_info';
									} else {
										endpoint = 'client_image_upload/delete_image_info';
									}
									break;
								}
								
								mediaData = {
									media_type: that.medias()[x].media_type,
									media_id: that.medias()[x].media_id
								};
								
								salesman = {
									id: authCtrl.userInfo.user_id,
									username: authCtrl.username(),
									password: authCtrl.password()
									
								};
								
								mediaDatapost = JSON.stringify(mediaData);
								salesmanpost = JSON.stringify(salesman);
								timestamp = Math.round(new Date().getTime() / 1000);
								$.ajax({
									type: 'POST',
									url: server + endpoint,
									data: {
										media_data: mediaDatapost,
										salesman: salesmanpost,
										timestamp: timestamp
									},
									success: function (dataS) {
										//alert(dataS);
									},
									async: false
								});
								
								
							}
						}

						alert('Files deleted.');
						
						that.getInfo(true);
					}
				},
				
				uploadChecked: function () {
					var that = this;
					that.viewd(false);
					that.checked(true);
					that.currentFileIndex(0)
					that.progress(0);
					for (var x in this.medias()) {
						if (this.medias()[x].selected()) {
							that.max(that.max() + 1);
						}
					}
					if (confirm("Are you sure you want to upload the selected files?")) {
						alert("Please do not turn off the device before the process has been finished!");
						if (!that.isBackground()) {
							posChangePage('#mediaUploadProgress');
						}
						else {
							alert('Upload Started');
							that.uploadMedias();
						}
					}
				},
				
				uploadMedias: function () {
					//alert("aqui");
					//that.checked(false);
					uploadNextMedia();
				},
				
				selectAll: function () {
					var that = this;

					for (var x in this.medias()) 
					{
						this.medias()[x].selected(true);
					}
				},
				
				uploadAll: function () {
					var that = this;
					that.progress(0);
					that.currentFileIndex(0);
					that.viewd(false);
					if (confirm("Are you sure you want to upload all files?")) {
						that.max(that.medias().length);
						alert("Please do not turn off the device before the process has been finished!");
						if (!that.isBackground()) {
							posChangePage('#mediaUploadProgress');
						}
						else {
							alert('Upload Started');
							that.uploadMedias();
						}
					}
				},
				
				getInfo: function (overwirte) {
					var that = this;
					this.burl = BUrl;
					this.progress(0);
					this.currentFileIndex(0);
					
					
					if (device.uuid != null && device.uuid.length > 5){
						data = {
							device_id : device.uuid
						};
					} else{
						data = {
							device_id: "NODEVICEID" //hardcoded one --- FAKE
						};
					}
					
					salesman = {
						id: authCtrl.userInfo.user_id,
						username: authCtrl.username(),
						password: authCtrl.password()
						
					};
					
					//prepare the autentification data
					datapost = JSON.stringify(data);
					salesmanpost = JSON.stringify(salesman);
					timestamp = Math.round(new Date().getTime() / 1000);
					
					if (overwirte && !that.isWorking()) {
						that.medias.removeAll();
						//do a post request to get all the information it needs
						$.ajax({
							type: 'POST',
							
							url: that.burl + 'medias_endpoint/get_unsync_medias2',
							data: {
								data: datapost,
								salesman: salesmanpost,
								timestamp: timestamp
							},
							success: function (dataS) {
								//if it succeed, it stores the values.
								//alert(dataS);
								console.log(dataS);
								
								var temp = JSON.parse(dataS);
								console.log(temp);
								//temp = [{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732507635.jpg","media_id":"9075","date":"10\/11\/2016","customer_first_name":"IGor","customer_last_name":"Leal","customer_id":"1274","endpoint":"client_image_upload\/upload_image_file_completion","delete":"client_image_upload\/delete_image_info","section":"Completion","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732456531.jpg","media_id":"9074","date":"10\/11\/2016","customer_first_name":"IGor","customer_last_name":"Leal","customer_id":"1274","endpoint":"client_image_upload\/upload_image_file_completion","delete":"client_image_upload\/delete_image_info","section":"Completion","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732442167.jpg","media_id":"9073","date":"10\/11\/2016","customer_first_name":"IGor","customer_last_name":"Leal","customer_id":"1274","endpoint":"client_image_upload\/upload_image_file_completion","delete":"client_image_upload\/delete_image_info","section":"Completion","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732507635.jpg","media_id":"9075","date":"01\/01\/1970","customer_first_name":null,"customer_last_name":null,"customer_id":null,"endpoint":"client\/upload_image_file","delete":"client\/delete_order_image_info","section":"Order","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732456531.jpg","media_id":"9074","date":"01\/01\/1970","customer_first_name":null,"customer_last_name":null,"customer_id":null,"endpoint":"client\/upload_image_file","delete":"client\/delete_order_image_info","section":"Order","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732442167.jpg","media_id":"9073","date":"01\/01\/1970","customer_first_name":null,"customer_last_name":null,"customer_id":null,"endpoint":"client\/upload_image_file","delete":"client\/delete_order_image_info","section":"Order","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732507635.jpg","media_id":"9075","date":"01\/01\/1970","customer_first_name":null,"customer_last_name":null,"customer_id":null,"endpoint":"client\/upload_custom_image_file","delete":"client\/delete_custom_image_info","section":"Order Custom Images","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732456531.jpg","media_id":"9074","date":"01\/01\/1970","customer_first_name":null,"customer_last_name":null,"customer_id":null,"endpoint":"client\/upload_custom_image_file","delete":"client\/delete_custom_image_info","section":"Order Custom Images","media_type":2},{"local_path":"file:\/\/\/var\/mobile\/Containers\/Data\/Application\/7D9D11AF-8233-4856-84BF-E317C323B18A\/Documents\/ClientMediaStorage\/1478732442167.jpg","media_id":"9073","date":"01\/01\/1970","customer_first_name":null,"customer_last_name":null,"customer_id":null,"endpoint":"client\/upload_custom_image_file","delete":"client\/delete_custom_image_info","section":"Order Custom Images","media_type":2}];
								
								//console.log("There are " + temp.length + " to synchronize");
								
								for (var x in temp) {
									
									
									if (temp[x].media_type == '1') {
										temp[x].media_type = 'Image';
									} else {
										temp[x].media_type = 'Video';
									}
									
									
									
									temp[x].customer = temp[x].customer_first_name + ' ' + temp[x].customer_last_name;
									temp[x].selected = ko.observable(false);
									temp[x].endpoint = that.burl + temp[x].endpoint;
									//temp[x].date = "";
									temp[x].status = "";
									temp[x].upload = true;
									temp[x].error = ko.observable(false);
									temp[x].imgPath = ko.observable(false);
								}
								
								for (var i in temp) {
									that.medias.push(temp[i]);
								}
								
								that.currentFileIndex(0);
								that.currentFile(that.medias()[that.currentFileIndex()]);
								for (var j in that.medias()) {
									try {
										window.resolveLocalFileSystemURL(that.medias()[j].local_path, function (file) {
											that.currentFile().imgPath(file.toURL().replace(/file:\/\//, ''));
											that.currentFileIndex(that.currentFileIndex() + 1);
											that.currentFile(that.medias()[that.currentFileIndex()]);
										}, failFile);
									} catch (e) { ; }
								}
								that.currentFileIndex(0);
								
							},
							async: false
						});
					}
					else if (!that.isWorking()) {
						that.overwrite = true;
					}
				},
				
				
			});
			
			MediaManagerVM = SimpleControl.extend({
				init: function (DsRegistry) {
					//Will store the full path of the image to be written on the json
					var that = this;
					this.mu = new MediaUploadVM(DsRegistry);
					this.section = ko.observable();
					this.media = ko.observable();
					this.server = BUrl;
					this.endpoint = ko.observable();
					this.error = ko.observable(false);
					this.imgPath = ko.observable();
					this.show = ko.observable(true);
					
					this.mu.media.subscribe(function (data) {
						that.show(false);
					});
					
					failFileMediaManagerVM = function (error) {
						that.error(true);
					};
					
					cancelChanges = function () {
						orderItem.SyncFilesVM.overwrite = false;
						posChangePage('#mediaUploadCenter');
					};
					
					deleteMedia = function () {
						switch (that.media().section) {
							case "Error":
							that.endpoint('error_report_endpoint/delete_media_info');
							break;
							
							case "Fittings":
							if (that.media().media_type == 'Video') {
								that.endpoint('client_fittings/delete_video_info');
							} else {
								that.endpoint('client_image_upload/delete_image_info');
							}
							break;
						}
						
						mediaData = {
							media_type: that.media().media_type,
							media_id: that.media().media_id
						};
						
						salesman = {
							id: authCtrl.userInfo.user_id,
							username: authCtrl.username(),
							password: authCtrl.password()
							
						};
						
						mediaDatapost = JSON.stringify(mediaData);
						salesmanpost = JSON.stringify(salesman);
						timestamp = Math.round(new Date().getTime() / 1000);
						
						$.ajax({
							type: 'POST',
							url: that.server + that.endpoint(),
							data: {
								media_data: mediaDatapost,
								salesman: salesmanpost,
								timestamp: timestamp
							},
							success: function (dataS) {
							},
							async: false
						});
						orderItem.SyncFilesVM.overwrite = true;
						posChangePage('#mediaUploadCenter');
					};
					
					changeMedia = function () {
						switch (that.media().section) {
							case "Error":
							that.endpoint('error_report_endpoint/update_file_info');
							break;
							
							case "Fittings":
							if (that.media().media_type == 'Video') {
								that.endpoint('client_fittings/update_fitting_video');
							} else {
								that.endpoint('client_image_upload/update_image_info');
							}
							break;
						}
						that.mu.uploadCurrentMedia(that.server + that.endpoint(), { media_id: that.media().media_id });
						orderItem.SyncFilesVM.overwrite = true;
						posChangePage('#mediaUploadCenter');
					};
				},
				
				setInfo: function (section, media) {
					var that = this;
					that.section(section);
					that.media(media);
					window.resolveLocalFileSystemURL(media.local_path, function (file) {
						that.imgPath(file.toURL().replace(/file:\/\//, ''));
					}, failFileMediaManagerVM);
				},
				
			});
			
			MediaTagManagerVM = SimpleControl.extend({
				init: function (DsRegistry) {
					var that = this;
					this.current = ko.observable();
					this.page = ko.observable();
					this.submit = ko.observable();
					this.endpoint = ko.observable();
					
					goBack = function () {
						posChangePage(that.page());
					};
					
					submitTags = function () {
						var that = this;
						data = {
							media_id: that.current().media_id,
							media_tags: JSON.stringify(that.current().tag())
						};
						
						salesman = {
							id: authCtrl.userInfo.user_id,
							username: authCtrl.username(),
							password: authCtrl.password()
							
						};
						
						//prepare the autentification data
						datapost = JSON.stringify(data);
						salesmanpost = JSON.stringify(salesman);
						timestamp = Math.round(new Date().getTime() / 1000);
						$.ajax({
							type: 'POST',
							url: BUrl + that.endpoint(),
							data: {
								data: datapost,
								salesman: salesmanpost,
								timestamp: timestamp
							},
							success: function (dataS) {
								console.log(dataS);
							},
							async: false
						});
						//posChangePage(that.page());
					};
				},
				
			});
			
			MediaType = Class.extend({
				init: function (mediaType, mediaControler, multiple, path, editable) {
					this.mediaType = mediaType;
					this.multiple = multiple;
					this.mediaControler = mediaControler;
					this.hasCustomPreview = false;
					this.ready = false;
					this.hasImage = false;
					this.hasTagsforImages = true;
					this.customPreview = null;
					if (!this.multiple) {
						this.preview = ko.observable('');
						if (path) {
							this.media = ko.observable(path);
							this.preview(this.media().preview());
						}
						else {
							data = this.generateMediaInfo();
							this.media = ko.observable(data);
						}
						
					} else {
						this.preview = ko.observableArray([]);
						if (path.length > 0) {
							this.media = ko.observableArray(path);
							for (var x in this.media()) {
								this.preview.push(this.media()[x].preview());
							}
						}
						else {
							this.media = ko.observableArray([]);
						}
					}
					if (!editable) {
						this.editable = false;
					}
					else {
						this.editable = true;
					}
				},
				setCustomPreview: function (preview) {
					//alert(preview());
					this.customPreview = preview;
					if (!this.multiple) {
						if (!this.customPreview().media) {
							this.customPreview({ media: ko.observable(''), tags: ko.observableArray([]) });
						}
					}
					
					this.hasCustomPreview = true;
				},
				setCustomPreviewValue: function () {
					if (!this.multiple) {
						//alert(this.preview());
						//alert(this.customPreview());
						this.customPreview().media(this.preview());
						this.customPreview().tags.removeAll();
						for (var x in this.media().tags()) {
							this.customPreview().tags.push(this.media().tags()[x]);
						}
						//alert(this.customPreview());
					}
					else {
						//alert(this.customPreview());
						this.customPreview.removeAll();
						for (var x in this.preview()) {
							this.customPreview.push(this.preview()[x]);
						}
						//alert(this.customPreview());
					}
				},
				updateChange: function () {
					this.ready = true;
				},
				getMediaPreview: function () {
					if (this.customPreview) {
						if (this.multiple) {
							return this.customPreview;
						}
						else {
							return this.customPreview().media;
						}
					}
					else {
						return this.preview;
					}
				},
				getTags: function (imageIndex) {

					console.log("media:", this.media());
					
					if (this.multiple) {
						this.currentTag = this.media()[imageIndex].tags();
						return this.media()[imageIndex].tags;
					}
					else {
						if (this.customPreview) {
							return this.customPreview().tags;
						}
						else {
							return this.media().tags;
						}
					}
					
				},
				getTagAttrib: function (attrib, imageIndex, tagIndex) {
					if (this.multiple) {
						data = this.media()[imageIndex].tags()[tagIndex];;
					}
					else {
						data = this.media().tags()[tagIndex];
					}
					switch (attrib) {
						case 'width%':
						return '' + (105 - data.left) + '%';
						break;
						case 'left%':
						return '' + (data.left) + '%';
						break;
						case 'top%':
						return '' + (data.top) + '%';
						break;
						case 'text':
						return data.text;
						break;
					}
				},
				addMedia: function (previewPath, uploadPath, internal) {
					if (!internal) {
						if (previewPath.indexOf(BUrl) === -1 && previewPath.indexOf('http://shirt-tailor.net/thepos/') === -1) {
						previewPath = BUrl + previewPath;
					}
					if (uploadPath.indexOf('http://shirt-tailor.net/thepos/') >= 0) {
					uploadPath = uploadPath.replace('http://shirt-tailor.net/thepos/', '');
				}
			}
			if (this.editable || !this.ready) {
				if (this.multiple) {
					this.data = this.generateMediaInfo();
					
					data = this.generateMediaInfo(uploadPath, internal);
					data = { preview: ko.observable(previewPath), upload: ko.observable(uploadPath), change: false, delete: false, tags: ko.observableArray([]), internal: internal };
					if (this.ready) {
						data.change = true;
					}
					this.media.unshift(data);
					this.preview.unshift(previewPath);
					if (this.customPreview) {
						this.customPreview.unshift(previewPath);
					}
				}
				else {
					this.media().upload(uploadPath);
					this.media().internal = internal;
					this.media().delete = false;
					this.media().tags([]);
					this.media().preview(previewPath);
					this.preview(previewPath);
					if (this.customPreview) {
						this.customPreview().media(previewPath);
					}
					if (this.ready) {
						this.media().change = true;
					}
				}
			}
		},
		removeMedia: function (index) {
			if (this.editable && confirm("Do you wish to remove the media?")) {
				if (this.multiple) {
					try {
						var fy = this.media()[index];
						this.media.remove(fy);
					} catch (e) {
					}
					//this.media.remove(this.media()[index]);
					this.preview.remove(this.preview()[index]);
					if (this.customPreview) {
						this.customPreview.remove(this.customPreview()[index]);
					}
				}
				else {
					this.media().preview('');
					this.media().upload('');
					this.media().change = false;
					this.media().delete = true;
					this.preview('');
					if (this.customPreview) {
						this.customPreview().media('');
						this.customPreview().tags([]);
					}
					this.media().tags([]);
				}
			}
		},
		getMediaIndex: function (path) {
			if (this.multiple) {
				var index = -1;
				for (var ind in this.media()) {
					if (this.media()[ind].upload() == path)
					index = ind;
				}
				return index;
			} else {
				return path == this.media().upload() ? 0 : -1;
			}
			
		},
		addTags: function (tags, mediaIndex) {
			if (this.multiple) {
				mediaTags = this.media()[mediaIndex].tags;
				data = this.media()[mediaIndex];
			}
			else {
				mediaTags = this.media().tags;
				data = this.media();
			}
			for (var x in tags) {
				mediaTags.push(tags[x]);
				if (this.customPreview && !this.multiple) {
					this.customPreview().tags.push(tags[x]);
				}
			}
			if (this.ready) {
				data.change = true;
			}
		},
		addTag: function (tag, mediaIndex) {

			console.log("Adding tag... ");

			if (this.editable || !this.ready) {
				if (tag != "") {
					if (this.multiple) {
						mediaTags = this.media()[mediaIndex].tags;
						data = this.media()[mediaIndex];
					}
					else {
						mediaTags = this.media().tags;
						data = this.media();
					}
					mediaTags.push(tag);
					try {
						if (this.customPreview) {
							this.customPreview().tags.push(tag);
						}
					} catch (e) { ; }
					if (this.ready) {
						data.change = true;
					}
				}
				else {
					customAlert("Please type your comment.");
				}
			}
		},
		buildTagPicture: function (text, event) {
			
			if (text) {
				target = event.target;
				var image_left = $(target).offset().left;
				var click_left = event.pageX;
				var left_distance = click_left - image_left;
				var image_top = $(target).offset().top;
				var click_top = event.pageY;
				var top_distance = click_top - image_top;
				var imagemap_width = $(target).width();
				var imagemap_height = $(target).height();
				var relative_left = (left_distance / imagemap_width) * 100;
				var relative_top = (top_distance / imagemap_height) * 100;
				
				const returnObj = { left: relative_left, top: relative_top, text: text };
				
				console.log("TAG META", returnObj);
				
				return returnObj;
			}
			else {
				return "";
			}
		},
		removeTag: function (imageIndex, tagIndex) {
			if (this.editable) {
				if (confirm("Do you wish to remove the comment?")) {
					if (this.multiple) {
						mediaTags = this.media()[imageIndex].tags;
						data = this.media()[imageIndex];
					}
					else {
						mediaTags = this.media().tags;
						data = this.media();
						if (this.customPreview) {
							this.customPreview().tags.remove(this.customPreview().tags()[tagIndex]);
						}
					}
					mediaTags.remove(mediaTags()[tagIndex]);
					
					if (this.ready) {
						data.change = true;
					}
				}
			}
		},
		generateMediaInfo: function (upPath, internal, mediaID) {
			data = { preview: ko.observable(''), upload: ko.observable(''), change: true, delete: false, tags: ko.observableArray([]), internal: false, mediaID: 0 };
			if (upPath) {
				data.upload(upPath);
			}
			if (internal) {
				data.internal = true;
			}
			if (mediaID > 0) {
				data.mediaID = mediaID;
			}
			return data;
		},
		allMediaHavetags: function () {
			if (this.multiple) {
				var has = true;
				for (var x in this.media()) {
					has = has && this.media()[x].tags().length > 0;
				}
				return has;
			}
			else {
				return this.media().tags().length > 0;
			}
		},
		anyMediaHavetags: function () {
			if (this.multiple) {
				for (var x in this.media()) {
					if (this.media()[x].tags().length > 0)
					return true;
				}
				return false;
			}
			else {
				return this.media().tags().length > 0;
			}
		},
		hasMedia: function () {
			if (this.multiple) {
				this.hasImage = this.media().length > 0;
			}
			else {
				this.hasImage = this.media().upload().length > 0;
			}
			return this.hasImage;
		},
		getSyncInfo: function () {
			var info = [];
			if (this.multiple) {
				for (var x in this.media()) {
					info.push({ path: this.media()[x].upload(), tags: JSON.stringify(this.media()[x].tags()), is_sync: this.media()[x].internal });
				}
			}
			else {
				info.push({ path: this.media().upload(), tags: JSON.stringify(this.media().tags()), is_sync: this.media().internal });
			}
			return info;
		},
		getMedia: function () {
			return this.media;
		},
		copyMedia: function (media) {
			if (this.editable || !this.ready) {
				if (this.multiple) {
					for (var x in media()) {
						this.data = this.generateMediaInfo();
						
						data = { preview: ko.observable(media()[x].preview()), upload: ko.observable(media()[x].upload()), change: false, delete: false, tags: ko.observableArray([]), internal: media()[x].internal };
						if (this.ready) {
							data.change = true;
						}
						this.media.push(data);
						this.preview.push(media()[x].preview());
						if (this.customPreview) {
							this.customPreview.push(media()[x].preview());
						}
					}
				}
				else {
					this.media().upload(media().upload());
					this.media().internal = media().internal;
					this.media().delete = false;
					this.media().tags([]);
					this.preview(media().preview());
					if (this.customPreview) {
						this.customPreview().media(media().preview());
					}
					if (this.ready) {
						this.media().change = true;
					}
				}
			}
		},
		mapperID: function (index) {
			return "mapper" + index;
		}
	});
});
