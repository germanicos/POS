define(['jquery', 'knockout', 'base'], function($, ko) {
	CustomProperties = Class.extend({
		init: function(){
			this.custom = ko.observableArray([]);
			this.hasCustomInfo = ko.observable(false);
			this.custom0 = 0;
		},
		addCustomInfo: function(customInfoName){
			this.custom.push({name:customInfoName, image: new MediaType(1, null, true, '',true), video: new MediaType(2, null, true, '',true)});
			this.hasCustomInfo(true);
		},
		removeCustomInfo: function(customInfoName){
			for(var x in this.custom()){
				if(this.custom()[x].name == customInfoName){
					this.custom.splice(x,1);
					if(this.custom().length == 0){
						this.hasCustomInfo(false);
					}
					return true;
				}
			}
			return false;
		},
		getCustomImages: function(customInfoName){
			for(var x in this.custom()){
				if(this.custom()[x].name == customInfoName){
					return this.custom()[x].image;
				}
			}
			return null;
		},
		getCustomVideos: function(index){
			return this.custom()[index].video;
		},
		getCustomImages: function(index){
			return this.custom()[index].image;
		},
		getCustomVideoInfo: function(customInfoName){
			for(var x in this.custom()){
				if(this.custom()[x].name == customInfoName){
					return this.custom()[x].video;
				}
			}
			return null;
		},
		getAllNames: function(){
			names = ko.observableArray([]);
			for(var x in this.custom()){
				names.push(this.custom()[x]);
			}
			return names;
		},
		isFinished: function(){
			for(var x in this.custom()){
				if(!this.custom()[x].image.hasMedia() && !this.custom()[x].video.hasMedia())
					return false;
			}
			return true;
		},
		getSyncInfo: function(){
			var data = [];
			for(var x in this.custom()){
				data.push({name:this.custom()[x].name, image: this.custom()[x].image.getSyncInfo(), video: this.custom()[x].video.getSyncInfo()});
			}
			return data;
		}
	});
});