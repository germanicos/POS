define(['jquery', 'knockout', 'base'], function ($, ko) {
	ActiveAlterationsVM = SimpleControl.extend({
		init: function () {
			self = this;

			this.erp_date = null; // get date from ERP to avoid erros by timezone
			this.requestCurrentDateFromERP();

			this.alterationsForMan = ko.observableArray(); // alterations for manufactory

			// A list for each city
			this.alterationsForPerth = ko.observableArray();
			this.alterationsForAdelaide = ko.observableArray();
			this.alterationsForMelbourne = ko.observableArray();
			this.alterationsForTasmania = ko.observableArray();
			this.alterationsForCanberra = ko.observableArray();
			this.alterationsForSydney = ko.observableArray();
			this.alterationsForBrisbane = ko.observableArray();

			this.requestAlterationsData();

			// cities to put on australian map
			this.cities = ko.observableArray([
				{ city: "Perth", coordinateY: "242px", coordinateX: "-15px", toShow: this.alterationsForPerth },
				{ city: "Adelaide", coordinateY: "280px", coordinateX: "172px", toShow: this.alterationsForAdelaide },
				{ city: "Melbourne", coordinateY: "304px", coordinateX: "183px", toShow: this.alterationsForMelbourne },
				{ city: "Tasmania", coordinateY: "352px", coordinateX: "209px", toShow: this.alterationsForTasmania },
				{ city: "Canberra", coordinateY: "271px", coordinateX: "265px", toShow: this.alterationsForCanberra },
				{ city: "Sydney", coordinateY: "247px", coordinateX: "303px", toShow: this.alterationsForSydney },
				{ city: "Brisbane", coordinateY: "200px", coordinateX: "306px", toShow: this.alterationsForBrisbane }
			]);

			this.variantCityName = ko.observable("Melbourne");
			this.variantAlterations = ko.observableArray(this.alterationsForMelbourne()); //alterations for non-manufactory (deprecated, but in use. Waiting for update in ERP)
		},

		/** request to ERP all necessary data to this page 
		 * New method.
		*/
		requestAlterationsData: function () {
			console.log("requesting data for Active request data...");

			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/get_salesman_active_alterations",
				dataType: 'json',
				data: { "user": authCtrl.userInfo },
				async: false,
				success: function (ret) {

					console.log(ret);

					// changeVisibilityDiv('loading_jp');
					for (let i = 0; i < ret.length; i++) {
						if (ret[i].tailor_id == '37') {
							//manufacture's id is 37
							self.alterationsForMan.push(ret[i]);
						} else {
							// if (ret[i].tailor_city_name == "Perth") {
							if (ret[i].tailor_city_id == 15) {
								self.alterationsForPerth.push(ret[i]);

								// } else if (ret[i].tailor_city_name == "Adelaide") {
							} else if (ret[i].tailor_city_id == 23) {
								self.alterationsForAdelaide.push(ret[i]);

								// } else if (ret[i].tailor_city_name == "Melbourne") {
							} else if (ret[i].tailor_city_id == 6) {
								self.alterationsForMelbourne.push(ret[i]);

								// } else if (ret[i].tailor_city_name == "Tasmania") {
							} else if (ret[i].tailor_city_id == "Tasmania") {
								self.alterationsForTasmania.push(ret[i]);

								// } else if (ret[i].tailor_city_name == "Canberra") {
							} else if (ret[i].tailor_city_id == 17) {
								self.alterationsForCanberra.push(ret[i]);

								// } else if (ret[i].tailor_city_name == "Sydney") {
							} else if (ret[i].tailor_city_id == 16) {
								self.alterationsForSydney.push(ret[i]);

								// } else if (ret[i].tailor_city_name == "Brisbane") {
							} else if (ret[i].tailor_city_id == 14) {
								self.alterationsForBrisbane.push(ret[i]);
							}
						}
					}

					console.log("alterationsForMan.alterationsForMan", self.alterationsForMan());
					console.log("alterationsForPerth", self.alterationsForPerth());
					console.log("alterationsForAdelaide", self.alterationsForAdelaide());
					console.log("alterationsForMelbourne", self.alterationsForMelbourne());
					console.log("alterationsForTasmania", self.alterationsForTasmania());
					console.log("alterationsForCanberra", self.alterationsForCanberra());
					console.log("alterationsForSydney", self.alterationsForSydney());
					console.log("alterationsForBrisbane", self.alterationsForBrisbane());


				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {

					console.log("XMLHttpRequest", XMLHttpRequest);
					console.log("textStatus", textStatus);
					console.log("errorThrown", errorThrown);

					// remove loader
					changeVisibilityDiv('loading_jp');

					alert("Something went wrong, please try again.")
					posChangePage("#activeAlterations");


				}
			});


		},
		/*
		 *
		 * In active alterations, set red dates when request date is 5 days away from current date
		 * The current date must be made in ERP because of the time zone
		 *
		 *
		 * This is used to choose css style in the view (set <span> as red)
		 */
		compareDate: function (alteration) {


			// var parts_order_date = alteration.order_date.split("-");
			var erp_date_split = this.erp_date.split("-");
			var parts_request_date = alteration.request_date.split("-");


			// let order_date =new Date(parts_order_date[0], parts_order_date[1], parts_order_date[2]);
			let erp_date_object = new Date(erp_date_split[2], erp_date_split[1], erp_date_split[0]);
			let request_date = new Date(parts_request_date[2], parts_request_date[1], parts_request_date[0]);


			// Has 5 days of tolerance
			return this.addDays(erp_date_object, 5).getTime() > request_date.getTime();
		},

		// Always goes to 'error' even when successful
		requestCurrentDateFromERP: function () {
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/get_current_date",
				dataType: 'json',
				data: { "user": authCtrl.userInfo },
				async: false,
				success: function (ret) {

					console.log("requestCurrentDateFromERP");
					console.log(ret);

					self.erp_date = ret.date;


				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {

					console.log("XMLHttpRequest", XMLHttpRequest);
					console.log("textStatus", textStatus);
					console.log("errorThrown", errorThrown);


					alert("Something went wrong, please try again.")
					posChangePage("#alterationsDashBoard")



				}
			});


		},

		/** Add day into a date.
		 * Used to add a tolerance for example
		 * 
		 * Params:
		 *    old date
		 *    day to add
		 * 
		 * return:
		 *    new Date
		 */
		addDays(date, days) {
			var result = new Date(date);
			result.setDate(result.getDate() + days);
			return result;
		},

		/** On click city, it's possible change the list of alterations on the rigth side of screen */
		changeAlterationsList(city) {
			console.log("change alterations list on the right side");
			self.variantAlterations(city.toShow());
			self.variantCityName(city.city);
			console.log("new self.variantAlterations is", self.variantAlterations());

		}
	});
});