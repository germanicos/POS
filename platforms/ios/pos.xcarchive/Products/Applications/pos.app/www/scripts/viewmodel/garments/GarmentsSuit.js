define(['jquery', 'knockout', 'base'], function($, ko) {
GarmentsSuit = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;

		this.fabricsVM   = new FabricsCntClass(DsRegistry);
		this.fabricsVM.subscribeTo('fabricsDS');


		this.suitData     = [];
		this.suitDataAID  = 0;
		
		this.mediaUpload = orderItem.MediaUploadVM;
		this.indexGarment = -1;
		this.indexOption = -1;
		this.isImage = false;
		this.customCount = ko.observable(0);
		this.isExternal = false;
		this.isFabric = false;
		
		this.galleryImages = function(){
			that.isExternal = true;
			$('#files').trigger('click');
		};

		this.addCustomImage = function(indexGarment, indexOption, isFabric){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = true;
			that.isExternal = false;
			that.isFabric = isFabric;
			that.mediaUpload.choosePhoto();
		};
		
		this.addCustomVideo = function(indexGarment, indexOption){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = false;
			that.isExternal = false;
			that.mediaUpload.chooseVideo();
		};
		
		this.mediaCallback = function(data){
			if(that.isFabric){
				orderItem.FabricImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
				that.isFabric = false;
			}
			else if(that.isImage){
				that.suitData[that.indexGarment].custom.getCustomImages(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
			else{
				that.suitData[that.indexGarment].custom.getCustomVideos(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
		};

		this.previousStepEnabled = ko.observable(false);
		

		this.workflow    = ko.observableArray([
			
			{id: 0, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Fabric" },
			{id: 1, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Style" },
			{id: 2, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Bottom" },
			{id: 3, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Vent" },
			{id: 4, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Lapel" },
			{id: 15, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Lapel Details" },
			{id: 5, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Sleeves" },
			{id: 6, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Pockets" },
			{id: 7, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Breast Pockets" },
			{id: 8, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Linning & Piping" }/*,
			{id: 9, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Monogram" }*/,
			{id: 9, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Pant Style" },
			{id: 10, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Pleats" },
			{id: 11, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Front Pockets" },
			{id: 12, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Back Pockets" },
			{id: 13, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Belt Loop" },
			{id: 14, target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Notes" },
			{id: 'Custom', target: "#garmentsSuit",  completed: false, caption: "Suit", title: "Custom Options" }
		]);
		
		this.customWorkflow = this.workflow()[this.workflow().length - 1];
		
		this.customStep = function(){
			that.currentStep(that.customWorkflow);
		};
		
		this.isWorkflowVisible = function(data){
			if(data.id === 'Custom'){
				if(that.customCount() > 0)
					return true;
				else
					return false;
			}
			else
				return true;
		};

		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			for(var pos in that.workflow()){
				if(that.workflow()[pos].id == data.id)
					break;
			}
			if(pos < 10){
				that.PreviewVisibleGarment('jacket');
			}else{
				that.PreviewVisibleGarment('pant');
			}
			
			if(pos == 3){		
				that.PreviewVisibleJacket('back');
			}else if(pos == 6){
				that.PreviewVisibleJacket('buttons');
			}else if(pos == 9){
				that.PreviewVisibleJacket('monogram');
			}else if(pos == 10){
				that.PreviewVisibleJacket('monogram');
			}else if(pos == 13){
				that.PreviewVisiblePant('back');
			}else{
				that.PreviewVisibleJacket('front');
				that.PreviewVisiblePant('front');
			}			
			
			posChangePage(data.target);
			//isGarments.scrollTo(0,0,0);
			$('.cloneDialog').remove();
			that.renderSuit();
		});

		this.isStepCompleted = ko.computed(function() {

			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					return that.workflow()[ind].completed;
				}
			}
			console.log('isStepCompleted bo');
			return false;
		});
		
		this.nextStep = function() {
			
			var cs = that.currentStep().id;
			for(var pos in that.workflow()){
				if(that.workflow()[pos].id == cs)
					break;
			}
				
			var notcompletedstep = false;						
			if(cs == 0){
				for(var x = 0; x < that.suitData.length; x++){								
					if (that.suitData[x].suitFabric.title == 'none'  && !that.suitData[x].SuitHasCustomerFabric) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 1){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].suitJacketStructure.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 2){
				for(var x = 0; x < that.suitData.length; x++){									
					if (that.suitData[x].suitJacketBottomStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 3){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].suitJacketVentStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 4){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].suitJacketLapelStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 5){
				// nothing to check here
				;
			}else if(cs == 6){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].suitJacketPocketStyle.id == 0 ) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 7){
				// nothing to check here
				;				
			}else if(cs == 9){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].pantFit.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}			
			}else if(cs == 10){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].pantPleats.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 11){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].pantPockets.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 12){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].bpantPockets.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 13){
				for(var x = 0; x < that.suitData.length; x++){
					if (that.suitData[x].beltLoopStyle.id == 0) {
						notcompletedstep = true;
						break;
					}
				}
			}
			
			if(notcompletedstep == false){
				if(cs == 'Custom'){
					that.customWorkflow.completed = true;
				}
				else{
					that.workflow()[pos].completed = true;
				}
				var foundnotcompletedstep = false;
				for(var x = 0; x < that.workflow().length; x++){
					if(that.workflow()[x].completed == false && that.workflow()[x] !== that.customWorkflow){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}			
				if(notcompletedstep == false){
					for(var x = 0; x <= cs ; x++){
						if(that.workflow()[x].completed == false){
							that.currentStep(that.workflow()[x] );
							foundnotcompletedstep = true;
							break;
						}
					}
					if(foundnotcompletedstep == false && !that.customWorkflow.completed && that.customCount() > 0 ){
						that.customStep();
						foundnotcompletedstep = true;
					}
				}
				if(foundnotcompletedstep == false){
					posChangePage('#orderItemSelectGarment');
				}				
				
			}else{
				if(cs == 'Custom'){
					that.customWorkflow.completed = false;
					that.customStep();
				}
				else{
					that.workflow()[pos].completed = false;
					that.currentStep(that.workflow()[pos] );
				}
				
				customAlert("Please fill in all necessary fields");
			}
			
		}
		
		

		this.setAsCompleted =  function(id, status) {
		//	if (status) {console.log(id + ' setAdCompleted ' + status);}
			var tWork = that.workflow();
			tWork[id].completed = status;
			//that.workflow(tWork);
		};





		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});


		this.completion       = ko.observable(0);
		this.variantNameSuit  = ko.observableArray([{id: 0, title: "Suit 1"}]);
		this.selectedVariantSuit  = ko.observable(this.variantNameSuit()[0]);
		this.selectedVariantSuit.subscribe(function(data) {
			$('.cloneDialog').remove();			
			that.selectVariantSuit();
//console.log("that.selectVariantSuit(): " + JSON.stringify(that.selectVariantSuit) );
//console.log("that.variantNameSuit(): " + JSON.stringify(that.variantNameSuit()) );			
		});

		//Prev/Next computedz

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== true) {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				 return 'asd';
				//return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});



		//this.ExtraPants = ko.observable(false);
		this.ExtraPants = ko.observable(0);
		this.ExtraPants.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].ExtraPants = data;
			that.flushModel();
			
		});

		//this.ExtraJacket = ko.observable(false);
		this.ExtraJacket = ko.observable(0);
		this.ExtraJacket.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].ExtraJacket = data;
			that.flushModel();
			
		});

		this.SuitNotes = ko.observable('');
		this.SuitNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].SuitNotes = data;
			that.flushModel();
			
		});


		this.JacketFabricNotes = ko.observable('');
		this.JacketFabricNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketFabricNotes = data;
			that.flushModel();
			
		});
		this.JacketStructureNotes = ko.observable('');
		this.JacketStructureNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketStructureNotes = data;
			that.flushModel();
			
		});
		this.JacketBottomNotes = ko.observable('');
		this.JacketBottomNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketBottomNotes = data;
			that.flushModel();
			
		});
		this.JacketVentNotes = ko.observable('');
		this.JacketVentNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketVentNotes = data;
			that.flushModel();
			
		});
		this.JacketLapelStyleNotes = ko.observable('');
		this.JacketLapelStyleNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketLapelStyleNotes = data;
			that.flushModel();
			
		});
		this.JacketButtonsNotes = ko.observable('');
		this.JacketButtonsNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketButtonsNotes = data;
			that.flushModel();
			
		});
		this.JacketPocketsNotes = ko.observable('');
		this.JacketPocketsNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketPocketsNotes = data;
			that.flushModel();
			
		});
		this.JacketLiningColoursNotes = ko.observable('');
		this.JacketLiningColoursNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketLiningColoursNotes = data;
			that.flushModel();
			
		});
		this.JacketMonogramNotes = ko.observable('');
		this.JacketMonogramNotes.subscribe(function(data) {				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].JacketMonogramNotes = data;
			that.flushModel();
			
		});

		this.PantFabricNotes = ko.observable('');
		this.PantFabricNotes.subscribe(function(data) {		
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantFabricNotes = data;
			that.flushModel();
			
		});
		this.PantStyleNotes = ko.observable('');
		this.PantStyleNotes.subscribe(function(data) {		
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantStyleNotes = data;
			that.flushModel();
			
		});
		this.PantPleatsNotes = ko.observable('');
		this.PantPleatsNotes.subscribe(function(data) {		
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantPleatsNotes = data;
			that.flushModel();
			
		});
		this.PantFrontPocketsNotes = ko.observable('');
		this.PantFrontPocketsNotes.subscribe(function(data) {		
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantFrontPocketsNotes = data;
			that.flushModel();
			
		});
		this.PantBackPocketsNotes = ko.observable('');
		this.PantBackPocketsNotes.subscribe(function(data) {		
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantBackPocketsNotes = data;
			that.flushModel();
			
		});
		this.PantBeltAndCuffsNotes = ko.observable('');
		this.PantBeltAndCuffsNotes.subscribe(function(data) {		
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantBeltAndCuffsNotes = data;
			that.flushModel();
			
		});		



		this.suitJacketCustomStructureNotes = ko.observable('');
		this.suitJacketCustomStructureNotes.subscribe(function(data) {
console.log("ADDING DATA TO suitJacketCustomStructureNotes");
console.log(data);				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomStructureNotes = data;
			that.flushModel();
			
		});
		this.suitJacketCustomStructureImage = ko.observable('');
		this.suitJacketCustomStructureImage.subscribe(function(data) {
console.log("ADDING DATA TO suitJacketCustomStructureImage");
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomStructureImage = json;
			
			that.flushModel();
			
		});
		
		
		this.suitJacketCustomVentNotes = ko.observable('');
		this.suitJacketCustomVentNotes.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomVentNotes = data;
			that.flushModel();
			
		});
		this.suitJacketCustomVentImage = ko.observable('');
		this.suitJacketCustomVentImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomVentImage = json;
			that.flushModel();
			
		});		
		
		
		this.suitJacketCustomLapelNotes = ko.observable('');
		this.suitJacketCustomLapelNotes.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomLapelNotes = data;
			that.flushModel();
			
		});
		this.suitJacketCustomLapelImage = ko.observable('');
		this.suitJacketCustomLapelImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomLapelImage = json;
			that.flushModel();
			
		});
		
		this.suitJacketCustomBottomNotes = ko.observable('');
		this.suitJacketCustomBottomNotes.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomBottomNotes = data;
			that.flushModel();
			
		});
		this.suitJacketCustomBottomImage = ko.observable('');
		this.suitJacketCustomBottomImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomBottomImage = json;
			that.flushModel();
			
		});

		//suitFabric
		this.suitFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.suitFabric.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitFabric = data;
			that.flushModel();
			
		});


		//suitJacketBottom
		this.suitJacketBottomStyleList = ko.observable(that.dsRegistry.getDatasource('suitBottomStyleDS').getStore());
		this.suitJacketBottomStyleListDoubleBreasted = ko.observable(that.dsRegistry.getDatasource('suitBottomStyleDS').getStore().slice(2,4));
		this.suitJacketBottomStyle     = ko.observable(that.dsRegistry.getDatasource('suitBottomStyleDS').getStore()[0]);
		this.suitJacketBottomStyle.subscribe(function(data) {
//console.log(data);			
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBottomStyle = data;
			that.flushModel();
			
		});
		
		this.selectSuitJacketButtonColor = function(data){
			that.suitJacketButtonColor(data.image);
		};

//suitJacketStructure
		this.suitJacketStructureList = ko.observable(that.dsRegistry.getDatasource('suitJacketStructureDS').getStore());
		this.suitJacketStructure     = ko.observable(that.dsRegistry.getDatasource('suitJacketStructureDS').getStore()[0]);
		this.suitJacketStructure.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketStructure = data;
			that.flushModel();
			
		});


		//suitJacketVent
		this.suitJacketVentStyleList = ko.observable(that.dsRegistry.getDatasource('suitVentStyleDS').getStore());
		this.suitJacketVentStyle     = ko.observable(that.dsRegistry.getDatasource('suitVentStyleDS').getStore()[0]);
		this.suitJacketVentStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketVentStyle = data;
			that.flushModel();
			
		});

		//suitJacketLapelStyle 
		this.suitJacketLapelStyleList = ko.observable(that.dsRegistry.getDatasource('suitLapelStyleDS').getStore());
		this.suitJacketLapelStyle     = ko.observable(that.dsRegistry.getDatasource('suitLapelStyleDS').getStore()[0]);
		this.suitJacketLapelStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketLapelStyle = data;
			that.flushModel();
			
		});



		//this.suitJacketBreastPocketStyleList = ko.observableArray(["Standard Pocket","Patch Pocket","Welt Pocket"]);               
		this.suitJacketBreastPocketStyleList = ko.observableArray(that.dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore());
		this.suitJacketBreastPocketStyle     = ko.observable(that.dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore()[0]);
		this.suitJacketBreastPocketStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBreastPocketStyle = data;
			that.flushModel();
			
		});

		this.suitJacketTicketPocketsDifferent = ko.observable(false);
		this.suitJacketTicketPocketsDifferent.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTicketPocketsDifferent = data;
			that.flushModel();
			
		});
		
		//suitJacketLapelWidth
		this.suitJacketLapelWidth = ko.observable(0);
		this.suitJacketLapelWidth.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketLapelWidth = data;
			that.flushModel();
			
		});
		
		//suitJacketPocketStyle 
		this.suitJacketPocketStyleList = ko.observable(that.dsRegistry.getDatasource('suitPocketStyleDS').getStore());
		this.suitJacketPocketStyle     = ko.observable(that.dsRegistry.getDatasource('suitPocketStyleDS').getStore()[0]);
		this.suitJacketPocketStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketPocketStyle = data;
			that.flushModel();
			
		});

		//suitJacketSleeveButtonNumber 
		this.suitJacketSleeveButtonNumberList = ko.observable(that.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore());
		this.suitJacketSleeveButtonNumber     = ko.observable(that.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore()[3]);
		this.suitJacketSleeveButtonNumber.subscribe(function(data) {
//console.log(JSON.stringify(data));			
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonNumber = data;
			that.flushModel();
			
		});
		
		this.suitJacketKissingButtons = ko.observable(false);
		this.suitJacketKissingButtons.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketKissingButtons = data;
			that.flushModel();
			
		});


		this.suitJacketButtonholeOnLapel = ko.observable(true);
		this.suitJacketButtonholeOnLapel.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketButtonholeOnLapel = data;
			that.flushModel();
			
		});
		

		this.suitJacketButtonholeOnLapelColorList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.suitJacketButtonholeOnLapelColor = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketButtonholeOnLapelColor.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketButtonholeOnLapelColor = data;
			that.flushModel();
			
		});
		
		
		this.suitJacketSleeveButtonType     = ko.observable('Working');
		this.suitJacketSleeveButtonType.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonType = data;
			that.flushModel();
			
		});

		

        //suitJacket Designer Vent 		
		this.suitJacketDesignerVent  = ko.observable(false);														 
		this.suitJacketDesignerVent.subscribe(function(data) {													 
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketDesignerVent = data;		 
			that.flushModel();																			 
			
		});
		
		this.suitJacketTail  = ko.observable(false);														 
		this.suitJacketTail.subscribe(function(data) {													 
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTail = data;		 
			that.flushModel();																			 
			
		});		
		
        //Dinner suit 		
		this.suitJacketDinner = ko.observable(false);														 
		this.suitJacketDinner.subscribe(function(data) {													 
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketDinner = data;		 
			that.flushModel();																			 
			
		});																								 

		this.suitJacketCollarAndLapelTrimming = ko.observable(false);													 
		this.suitJacketCollarAndLapelTrimming.subscribe(function(data) {													 
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCollarAndLapelTrimming = data;	 
			that.flushModel();																						 
			
		});																											 

		
		this.suitJacketContrastTrimming = ko.observable(false);
		this.suitJacketContrastTrimming.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketContrastTrimming = data;
			that.flushModel();
			
		});
		
		
		this.suitJacketCollarTrimming = ko.observable(false);												 
		this.suitJacketCollarTrimming.subscribe(function(data) {											
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCollarTrimming = data;	
			that.flushModel();																				
			
		});																									
		
		this.suitJacketLapelTrimming = ko.observable(false);												 
		this.suitJacketLapelTrimming.subscribe(function(data) {												
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketLapelTrimming = data;	
			that.flushModel();																				
			
		});																									
		
		this.suitJacketCoverButtons = ko.observable(false);												 
		this.suitJacketCoverButtons.subscribe(function(data) {												
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCoverButtons = data;	
			that.flushModel();																				
			
		});		
		
		this.suitJacketPocketTrimming = ko.observable(false);									 
		this.suitJacketPocketTrimming.subscribe(function(data) {												
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketPocketTrimming = data;	
			that.flushModel();																				
			
		});						

		this.suitJacketBreastPocketTrimming = ko.observable(false);												 
		this.suitJacketBreastPocketTrimming.subscribe(function(data) {												
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBreastPocketTrimming = data;	
			that.flushModel();																				
			
		});	

//suitContrastPockets 		
		this.suitJacketContrastPockets = ko.observable(false);
		this.suitJacketContrastPockets.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketContrastPockets = data;
			that.flushModel();
			
		});



		this.suitJacketBreastPocketAngledStyle = ko.observable('Straight');
		this.suitJacketBreastPocketAngledStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBreastPocketAngledStyle = data;
			that.flushModel();
			
		});


	//suitContrastLapelLower 		
		this.suitJacketContrastLapelLower = ko.observable(false);
		this.suitJacketContrastLapelLower.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketContrastLapelLower = data;
			that.flushModel();
			
		});

                //suitLapelSleeveButtonHole 		
		this.suitJacketLapelButtonHole = ko.observable(false);
		this.suitJacketLapelButtonHole.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketLapelButtonHole = data;
			that.flushModel();
			
		});

                //suitLapelSleeveButtonHole 		
		this.suitJacketTicketPocket = ko.observable(false);
		this.suitJacketTicketPocket.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTicketPocket = data;
			that.flushModel();
			
		});

//suitContrastLapelUpper 		
		this.suitJacketContrastLapelUpper = ko.observable(false);
		this.suitJacketContrastLapelUpper.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketContrastLapelUpper = data;
			that.flushModel();
			
		});

                //suitContrastCheckPocket 		
		this.suitJacketContrastCheckPocket = ko.observable(false);
		this.suitJacketContrastCheckPocket.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketContrastCheckPocket = data;
			that.flushModel();
			
		});



		//suitJacketTopStitch
		this.suitJacketTopStitch = ko.observable(false);
		this.suitJacketTopStitch.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTopStitch = data;
			that.flushModel();
			
		});

		//suitJacketTopStitchPocket
		this.suitJacketTopStitchPocket = ko.observable(false);
		this.suitJacketTopStitchPocket.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTopStitchPocket = data;
			that.flushModel();
			
		});
		
		//suitJacketTopStitchLapel
		this.suitJacketTopStitchLapel = ko.observable(false);
		this.suitJacketTopStitchLapel.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTopStitchLapel = data;
			that.flushModel();
			
		});		
		
		//suitJacketTopStitchPiping 
		this.suitJacketTopStitchPipingList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.suitJacketTopStitchPiping     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketTopStitchPiping.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTopStitchPiping = data;
			that.flushModel();
			
		});

		//suitJacketPiping 
		this.suitJacketPipingList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.suitJacketPiping     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketPiping.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketPiping = data;
			that.flushModel();
			
		});
		
		this.suitJacketInsidePocketsDifferentFromPiping = ko.observable(false);
		this.suitJacketInsidePocketsDifferentFromPiping.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketInsidePocketsDifferentFromPiping = data;
			that.flushModel();
			
		});
		this.suitJacketInsidePocketsColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketInsidePocketsColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketInsidePocketsColour = data;
			that.flushModel();
			
		});


		//suitJacketLining 
		this.suitJacketLiningList = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore());
		this.suitJacketLining     = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore()[0]);
		this.suitJacketLining.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketLining = data;
			that.flushModel();
			
		});

		//suitJacketButtonColor
		this.suitJacketButtonColorList = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore());
		this.suitJacketButtonColor     = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0].image);
		this.suitJacketButtonColor.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketButtonColor = data;
			that.flushModel();
			
		});

		//suitJacketThreadColor
		this.suitJacketThreadColorList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.suitJacketThreadColor     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketThreadColor.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketThreadColor = data;
			that.flushModel();
			
		});


//suitJacketMonogram 
		this.suitJacketMonogram = ko.observable('');
        this.suitJacketMonogram.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketMonogram = data;
			that.flushModel();
			
		});
		
		this.suitJacketMonogramExtraLine = ko.observable('');
        this.suitJacketMonogramExtraLine.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketMonogramExtraLine = data;
			that.flushModel();
			
		});

		this.suitJacketDefaultMonogramHeader = ko.observable(true);
		this.suitJacketDefaultMonogramHeader.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketDefaultMonogramHeader = data;
			that.flushModel();
			
		});
		this.suitJacketCustomMonogramHeader = ko.observable('');
		this.suitJacketCustomMonogramHeader.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomMonogramHeader = data;
			that.flushModel();
			
		});

		this.suitJacketMonogramStitchDifferentFromPiping = ko.observable(false);
		this.suitJacketMonogramStitchDifferentFromPiping.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketMonogramStitchDifferentFromPiping = data;
			that.flushModel();
			
		});
		
		this.suitJacketMonogramStitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketMonogramStitchColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketMonogramStitchColour = data;
			that.flushModel();
			
		});

		this.suitJacketCustomMonogram = ko.observable(false);
		this.suitJacketCustomMonogram.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomMonogram = data;
			that.flushModel();
			
		});
		this.suitJacketCustomMonogramImage = ko.observable('');
		this.suitJacketCustomMonogramImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomMonogramImage = json;
			that.flushModel();
			
		});
		this.suitJacketCustomMonogramNotes = ko.observable('');
		this.suitJacketCustomMonogramNotes.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketCustomMonogramNotes = data;
			that.flushModel();
			
		});
		
		
		this.suitJacketEmbroidery = ko.observable(false);
		this.suitJacketEmbroidery.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketEmbroidery = data;
			that.flushModel();
			
		});
		
		this.suitJacketEmbroideryImage = ko.observable('');
		this.suitJacketEmbroideryImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketEmbroideryImage = json;
			that.flushModel();
			
		});
		this.suitJacketEmbroideryNotes = ko.observable(false);
		this.suitJacketEmbroideryNotes.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketEmbroideryNotes = data;
			that.flushModel();
			
		});
		

              //suitTopStitchCode
		//this.suitJacketTopStitchCode = ko.observable('');
		this.suitJacketTopStitchCode = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.suitJacketTopStitchCode.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketTopStitchCode = data;
			that.flushModel();
			
		});

		
		this.suitJacketContrastTrimmingColour = ko.observable('');//(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketContrastTrimmingColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketContrastTrimmingColour = data;			
			that.flushModel();
			
		});
		

		this.suitJacketFitList =  ko.observableArray(["Fitted", "Semi Fitted", "Standard Fit"]);//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore());
		this.suitJacketFit = ko.observable('Semi Fitted'); //ko.observable(that.dsRegistry.getDatasource('fitDS').getStore()[2]);
        this.suitJacketFit.subscribe(function(data) {
console.log(data);        	
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketFit = data;
console.log( JSON.stringify( that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketFit ));     		
			that.flushModel();
			
		});
		
		
		this.urgent = ko.observable(0);
		this.urgent.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].urgent = data;
			that.flushModel();
			
		});
		
		this.DOP_day = ko.observable('');
		this.DOP_day.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].DOP_day = data;
			that.flushModel();
			
		});	
		this.DOP_month = ko.observable('');
		this.DOP_month.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].DOP_month = data;
			that.flushModel();
			
		});		
		this.DOP_date = ko.observable('');
		this.DOP_date.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].DOP_date = data;
			that.flushModel();
			
		});					
		

//////////////////////////////////////////////////////////////////////////////////////////////////////////


		this.suitJacketSleeveButton1Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.suitJacketSleeveButton1Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton1Colour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButton2Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.suitJacketSleeveButton2Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton2Colour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButton3Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.suitJacketSleeveButton3Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton3Colour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButton4Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.suitJacketSleeveButton4Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton4Colour = data;
			that.flushModel();
			
		});				
		this.suitJacketSleeveButton5Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.suitJacketSleeveButton5Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton5Colour = data;
			that.flushModel();
			
		});
		
		
		this.suitJacketbuttonholeList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		
		this.suitJacketSleeveButtonHole1Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButtonHole1Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonHole1Colour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButtonHole2Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButtonHole2Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonHole2Colour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButtonHole3Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButtonHole3Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonHole3Colour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButtonHole4Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButtonHole4Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonHole4Colour = data;
			that.flushModel();
			
		});				
		this.suitJacketSleeveButtonHole5Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButtonHole5Colour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButtonHole5Colour = data;
			that.flushModel();
			
		});
		
		this.suitJacketSleeveButton1StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButton1StitchColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton1StitchColour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButton2StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButton2StitchColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton2StitchColour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButton3StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButton3StitchColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton3StitchColour = data;
			that.flushModel();
			
		});
		this.suitJacketSleeveButton4StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButton4StitchColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton4StitchColour = data;
			that.flushModel();
			
		});				
		this.suitJacketSleeveButton5StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.suitJacketSleeveButton5StitchColour.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketSleeveButton5StitchColour = data;
			that.flushModel();
			
		});
		
		this.checkVisibility = ko.observable(true);
		this.checkVisibility.subscribe(function(data) {
            if(that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBottomStyle.id == 0){
				that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].checkVisibility = false;
			}else if( that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketStructure.id == 'Double_Breasted' ||  
					  that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketStructure.id == 'Double_Breasted_One_to_close' ||  
					  that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketStructure.id == 'Double_Breasted_six_Buttons' 
					  ){
                                             		if(that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBottomStyle.id === 1 || 
                                             			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].suitJacketBottomStyle.id === 2
                                             			){
                                                    	that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].checkVisibility = false;
                                                    }else{
                                                    	that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].checkVisibility = true;
                                                	}
             }else{
             	that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].checkVisibility = true;
             }   
                                             
		});
		
		this.pantsCustomPocketsNotes = ko.observable('');
		this.pantsCustomPocketsNotes.subscribe(function(data) {
console.log("ADDING DATA TO pantsCustomPocketsNotes");
console.log(data);				
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantsCustomPocketsNotes = data;
			that.flushModel();
			
		});
		this.pantsCustomPocketsImage = ko.observable('');
		this.pantsCustomPocketsImage.subscribe(function(data) {
console.log("ADDING DATA TO pantsCustomPocketsImage");
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantsCustomPocketsImage = json;
			
			that.flushModel();
			
		});


		//PantContrastFabric
		this.PantContrastFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.PantContrastFabric.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantContrastFabric = data;
			that.flushModel();
			
		});


                //we have to delete this its being replace
		//beltLoopStyle
		this.beltLoopStyleList   = ko.observable(that.dsRegistry.getDatasource('beltLoopStyleDS').getStore());
		this.beltLoopStyle       = ko.observable(that.dsRegistry.getDatasource('beltLoopStyleDS').getStore()[0]);
		this.beltLoopStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].beltLoopStyle = data;
			that.flushModel();
			
		});


		this.pantCuffs        = ko.observable(false);
		this.pantCuffs.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantCuffs = data;
			that.flushModel();
			
		});
		
		this.PantsBracesProvisions        = ko.observable(false);
		this.PantsBracesProvisions.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantsBracesProvisions = data;
			that.flushModel();
			
		});
		
		//cuffsStyle
		this.cuffsStyleList   = ko.observable(that.dsRegistry.getDatasource('cuffsStyleDS').getStore());
		this.cuffsStyle       = ko.observable(that.dsRegistry.getDatasource('cuffsStyleDS').getStore()[0]);
		this.cuffsStyle.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].cuffsStyle = data;
			that.flushModel();
			
		});

		
		//pantFit
		this.pantFitList   = ko.observable(that.dsRegistry.getDatasource('pantFitDS').getStore());
		this.pantFit       = ko.observable(that.dsRegistry.getDatasource('pantFitDS').getStore()[0]);
		this.pantFit.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantFit = data;
			that.flushModel();
			
		});


		//pantPleats
		this.pantPleatsList   = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore());
		this.pantPleats       = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore()[0]);
		this.pantPleats.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantPleats = data;
			that.flushModel();
			
		});



        this.pantPleatsList   = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore());
		this.pantPleats       = ko.observable(that.dsRegistry.getDatasource('pantPleatsDS').getStore()[0]);
		this.pantPleats.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantPleats = data;
			that.flushModel();
			
		});



		this.bpantPocketsList   = ko.observable(that.dsRegistry.getDatasource('bpantPocketsDS').getStore());
		this.bpantPockets       = ko.observable(that.dsRegistry.getDatasource('bpantPocketsDS').getStore()[2]);
		this.bpantPockets.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].bpantPockets = data;
			that.flushModel();
			
		});

     	this.pbackpocketMethodList    = ko.observableArray(["Both","Right","Left"]);
		this.pbackpocketMethod     = ko.observable('');
		this.pbackpocketMethod.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pbackpocketMethod = data;
			that.flushModel();
			
		});


        this.pantPocketsList   = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore());
        this.pantPocketsListNoPleats   = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore().slice(0, 2));
        
        
        this.pantPockets       = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore()[1]);
		this.pantPockets.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantPockets = data;
			that.flushModel();
			
		});
		this.pantPockets1       = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore()[1]);
		this.pantPockets1.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantPockets1 = data;
			that.flushModel();
			
		});
		this.pantPockets2       = ko.observable(that.dsRegistry.getDatasource('pantPocketsDS').getStore()[1]);
		this.pantPockets2.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].pantPockets2 = data;
			that.flushModel();
			
		});


		//PantLining 
		this.PantLiningList = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore());
		this.PantLining     = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore()[0]);
		this.PantLining.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantLining = data;
			that.flushModel();
			
		});
		
		
		this.PantFitList = ko.observableArray(["Fitted", "Semi Fitted", "Standard Fit"]);//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore());
		this.PantFit = ko.observable('Semi Fitted');//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore()[2]);
        this.PantFit.subscribe(function(data) {
			that.suitData[ that.getRow(that.selectedVariantSuit().id)  ].PantFit = data;
			that.flushModel();

		});


		this.PreviewVisibleGarment = ko.observable('jacket');
		this.PreviewVisibleJacket = ko.observable('front');
		this.PreviewVisiblePant = ko.observable('front');
 
		this.suitFabricSelect = function() {
                        // customAlert("22 shuit" + this.getRow(this.selectedVariantSuit().id)); 
			that.dsRegistry.setContext({
				targetModel:     'garmentsSuitDS',
				targetGarment:   'suit',
				targetDelta:      this.getRow(this.selectedVariantSuit().id),
				targetAttribute: 'suitFabric',
				callbackPage:    '#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[that.getRow(that.selectedVariantSuit().id)].suitFabric;
			that.fabricsVM.setSelectedFabric(fab);
                        
                    //  customAlert("fab " + fab);
		};

		this.pantContrastFabricSelect = function() {
                        //  customAlert("11 shuit");
			that.dsRegistry.setContext({
				targetModel:     'garmentsSuitDS',
				targetGarment:   'suit',
				targetDelta:      this.getRow(this.selectedVariantSuit().id),
				targetAttribute: 'pantContrastFabric',
				callbackPage:    '#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsSuitDS').getStore().suit[that.getRow(that.selectedVariantSuit().id)].pantContrastFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};
		

		this.fnCloneSuit = function(e) {
				
                fe = this;
				var parentOffset = $('.content-wrapper').offset(); 
				//or $(this).offset(); if you really just want the current element's offset
				relX = e.pageX - parentOffset.left;
				relY = e.pageY - parentOffset.top;

				//need testing
				if (relY > 500) relY -= 200;
				targetAttr = $(this).parent().parent().parent().attr('data-target-attr');
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().parent().attr('data-target-attr');	
				}	
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().attr('data-target-attr');	
				}	

				currentVariant = that.selectedVariantSuit();
				var vnames = that.variantNameSuit(); 
				if (vnames.length == 1) return;

				var fa  = 'Select Suit changes! ';
			    fa += '<div class="cloneDialogBtnClose" data-role="button"><img src="http://shirt-tailor.net/thepos/appimg/template/topmenu/close.png"/></div><div  data-role="fieldcontain"><fieldset data-role="controlgroup">';
				for (var ind in vnames) {
					//if (vnames[ind].id != currentVariant.id) {
						check = '';
						if (vnames[ind].id == currentVariant.id) check = ' checked="checked" disabled ';
						fa += '<input type="checkbox" '+ check +' data-id="' + vnames[ind].id + '" id="cSuit-'+ vnames[ind].id +'" name="cSuit-'+ vnames[ind].id +'"/>'
						fa += '<label for ="cSuit-'+ vnames[ind].id +'">' + vnames[ind].title + '</label>';
					//}
				}
				fa += "</fieldset></div><div class='cloneDialogBtn' data-role='button'>Apply</div>";
				$('.cloneDialog').remove();
				$('.content-wrapper').append("<div class='cloneDialog' id='cloneSuitDialog' style='left: " + relX + "px !important; top: " + relY + "px !important;'>" + fa + "</div>");
				
				$('#cloneSuitDialog input').checkboxradio();
				$('.cloneDialogBtn').on('click', function() {
					cI = [];
					$('#cloneSuitDialog :checked').each(function(thet,c) {cI.push($(c).attr('data-id'));});
					that.cloneSuitAttributes(currentVariant.id, targetAttr, cI);
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
					//$.mobile.sdCurrentDialog.close();
				});
				$('.cloneDialogBtnClose').on('click', function() {
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
				});
		}


		this.cloneSuitAttributes = function( pvId, pvTA, cvI)
		{
			var tAD = that.getVariant(pvId)[pvTA];			
			for (var ind in that.suitData) {
				if ( cvI.indexOf(that.suitData[ind].variantId.toString()) != -1) {
					//console.log('cloning to ' + that.suitData[ind].variantId);

					that.suitData[ind][pvTA] = tAD; 
				}
			}
			that.flushModel();
		};


		this.deleteVariantSuit = function(data) {
			
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							var currentcount = orderItem.garmentsList()[2].count();
							orderItem.garmentsList()[2].count( currentcount - 1);
							
							var tsuitData = [];
							var tvarsData = [];
							for (var ind in that.variantNameSuit() ) {
								if ( that.variantNameSuit()[ind].id != data.id ) {
									tvarsData.push(that.variantNameSuit()[ind]);
									tsuitData.push(that.getVariant( that.variantNameSuit()[ind].id ));
								}
							}
							that.suitData = tsuitData;
console.log(JSON.stringify(tvarsData));							
							that.flushModel();
							that.variantNameSuit(tvarsData);
							that.selectedVariantSuit(that.variantNameSuit()[0]);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});

		};
		
		this.deleteVariantSuit2 = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							var currentcount = orderItem.garmentsList()[2].count();
							orderItem.garmentsList()[2].count( currentcount - 1);
							
							var tsuitData = [];
							var tvarsData = [];
							for (var ind in that.variantNameSuit() ) {
								if ( that.variantNameSuit()[ind].id != data.id ) {
									tvarsData.push(that.variantNameSuit()[ind]);
									tsuitData.push(that.getVariant( that.variantNameSuit()[ind].id ));
								}else{
								//	console.log("TO DELETE SUIT " + JSON.stringify( that.getVariant( that.variantNameSuit()[ind].id ) ) );
								//	console.log("TimestampID: " + that.getVariant(that.variantNameSuit()[ind].id).timestampID);
									orderItem.removeFabric('Suit', 2, 'Suit ' + (that.suitData[ ind ].variantId + 1), that.suitData[ ind ].SuitCustomerFabric, that.suitData[ ind ].variantId, 4);
									if(orderItem.garmentsPantVM)
										orderItem.garmentsPantVM.deleteSuitExtraPants( that.getVariant(that.variantNameSuit()[ind].id).timestampID );
									if(orderItem.garmentsVestVM)
										orderItem.garmentsVestVM.deleteSuitVests( that.getVariant(that.variantNameSuit()[ind].id).timestampID );
								}
							}
							that.suitData = tsuitData;
							that.flushModel();
							that.variantNameSuit(tvarsData);
							that.selectedVariantSuit(that.variantNameSuit()[0]);
							
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "block";
							//posChangePage('#bodyshape');
							//posChangePage('#orderItemSelectGarment');
							orderItem.prepareGarments();
							orderItem.populateGarments();
							setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },1000);
						}
					},
					'Cancel': {
						click: function () {return 0;},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};
		
		
		
		
		
		this.compareFunction = function(data1, data2) {
			if(JSON.stringify(data1) == JSON.stringify(data2)){
				return true;
			}else{
				return false;
			}
		};
		
		this.toggle_visibility_array = function (id, index) {		
			var e = document.getElementsByName(id); 
			for(var x = 0; x < e.length; x++){
				if(x != index){
					e[x].style.display = "none";	
				}
			}
			if(e[index].style.display == 'block'){
			   	e[index].style.display = 'none';
			}else{
				e[index].style.display = 'block';
			}	
		};
	
		
		
		ko.bindingHandlers.modifiedSuitListOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var wantedValue = value[1];	
					var list = document.getElementsByName(value[2]);
					var checkboxes = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;
					var elemdivs = document.getElementsByName(value[5]);

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						if(wantedValue.id != undefined){
							checkValue = wantedValue.id; 
						}else{
							checkValue = wantedValue;
						}	
						if(spanvalue == checkValue){
							checkboxes[ index ].classList.add('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('showtrigger');
								elemdivs[ index ].classList.remove('offtrigger');
							}	
						}else{
							checkboxes[ index ].classList.remove('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('offtrigger');
								elemdivs[ index ].classList.remove('showtrigger');
							}	
						}	
					}
					
					if(value[4] != 'none'){
						//var customdivs = document.getElementsByName(value[4]);
						//if(customdivs[value[0]] != undefined){
							if(wantedValue.id == '123'){
								//customdivs[value[0]].style.display = 'block';
								that.suitData[value[0]].custom.addCustomInfo(value[7]);
								that.suitData[value[0]].custom.custom0 = 100;
								that.customCount(that.customCount() + 1);
							}else{
								//customdivs[value[0]].style.display = 'none';
								if(that.suitData[value[0]].custom.removeCustomInfo(value[7])){
									that.customCount(that.customCount() - 1);
								}
							}
						//}	
					}
					
					
					if(value[6] != undefined && value[6] != 'none'){
						var mainoptionsdivs = document.getElementsByName(value[6]);
						var mainoptionscount = mainoptionsdivs.length;
						
						for(var a = 0; a < mainoptionscount; a++){
							var haschecked = false;
							for(var b = 0; b < garmentscount; b++){
								var index = a*garmentscount + b;
								if(checkboxes[index].classList.contains('selecteds')){
									haschecked = true;
									break;
								}
							}
							if(haschecked == true){
								mainoptionsdivs[a].classList.add('activate');
							}else{
								mainoptionsdivs[a].classList.remove('activate');
							}
						}
					}
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);
		        });
		    }
		};    

		ko.bindingHandlers.modifiedSuitListBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( !checkboxes[ index ].classList.contains(value[3]) ){
							checkboxes[ index ].classList.add(value[3]);
						}else{
							checkboxes[ index ].classList.remove(value[3]);						
						}	
					}
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);					
		        });
		    }
		};   

		ko.bindingHandlers.modifiedSuitListSelectOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var selects = document.getElementsByName(value[2]);
					var garmentscount = selects.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						
						selects[ index ].value = that.suitData[ value[0] ][value[2]];

						if ("createEvent" in document) {
							var evt = document.createEvent("HTMLEvents");
		    				evt.initEvent("change", false, true);
							selects[ index ].dispatchEvent(evt);
						}else{
							selects[ index ].fireEvent("onchange");
						}												
						
					}
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);					
		        });
		    }
		}; 

		this.modifiedSuitListSelectOptionsFunction = function( value0, value1, value2 ){
					var list = document.getElementsByName(value1);
					var selects = document.getElementsByName(value2);
					var garmentscount = selects.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value0;
						var checkValue = '';
						
						selects[ index ].value = that.suitData[ value0 ][value2];
						
					}
					that.selectedVariantSuit(that.variantNameSuit()[value0]);					
		        					
		}; 	



		ko.bindingHandlers.modifiedSuitBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var checkboxes = document.getElementsByName(value[1]);
					var index = value[0];
					var cssclass = "" + value[2];

					if( !checkboxes[ index ].classList.contains(cssclass) ){
						checkboxes[ index ].classList.add(cssclass);
					}else{
						checkboxes[ index ].classList.remove(cssclass);
					}	

					if(value[1] == 'suitJacketCoverButtons'){
						if(that.suitData[ index ].suitJacketCoverButtons == true && that.suitData[ index ].suitJacketDinner == true){
							document.getElementsByName(value[3])[index].style.display = "block";
						}else{
							document.getElementsByName(value[3])[index].style.display = "none";
						}
					}
					
					if(value[1] == 'suitJacketKissingButtons'){
						if(that.suitData[ index ].suitJacketKissingButtons == true){
							document.getElementsByName(value[3])[index].style.display = "block";
						}else{
							document.getElementsByName(value[3])[index].style.display = "none";
						}
					}

					if( value[1] != 'FabricCheck' ){
						that.selectedVariantSuit(that.variantNameSuit()[value[0]]);
					}	
		        });
		    }
		};


		ko.bindingHandlers.switchpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);

					for(var x = 0; x < popups.length; x++){
						if(x != value[0]){
							popups[x].style.display = "none";	
						}
					}
					if(popups[value[0]].style.display == "block"){
						popups[value[0]].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}else{
						popups[value[0]].style.display = "block";		
					}

		        });
		    }
		}; 
		

		ko.bindingHandlers.suitswitchcustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);		
					
					try{	
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.suitData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.suitcustomMonogramEmbroideryPopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					var tmp = that.selectedVariantSuit();
					that.selectedVariantSuit(tmp);
					try{		
						if(value[1] != undefined && value[2] != undefined){
							var link = "";		
							if(value[1].indexOf("Monogram") !=-1 ){						
								link = BUrl + that.suitJacketCustomMonogramImage().image;
							}else if(value[1].indexOf("Embroidery") !=-1 ){
								link = BUrl + that.suitJacketEmbroideryImage().image;
							}
							if(link != ""){
								document.getElementById(value[1]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
							}	
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[0]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		};		
					
		
		
		ko.bindingHandlers.suitopencustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);		
					
					try{	
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.suitData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					popup[0].style.display = "block";		
		        });
		    }
		}; 			
		
		

		ko.bindingHandlers.openpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);			
					popups[value[0]].style.display = "block";		//.show(), .dialog( "open" ) : not working
		        });
		    }
		}; 

		ko.bindingHandlers.closepopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[0]);
					for(var x = 0; x < popups.length; x++){			
						popups[x].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}	
		        });
		    }
		}; 


		ko.bindingHandlers.modifiedSuitThreads = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var images = document.getElementsByName(value[3]);
					var texts = document.getElementsByName(value[3] + "Text");
					var options = document.getElementsByName(value[4]);
					images[value[0]].src = value[2].image;
					if(texts[value[0]] != undefined){
						texts[value[0]].innerHTML = value[2].title;
					}	

					var garmentssize = options.length/images.length;
					for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
						
						// FOR LINING OPTIONS
						if(value[5] != undefined && value[6] == undefined /*&& value[7] != undefined && value[8] != undefined && value[9] != undefined && value[10] != undefined*/){
							document.getElementsByName(value[5])[x].classList.remove('selected');	
						}
					}
					options[garmentssize*value[0] + value[1]].classList.add('selected');
					
					// FOR LINING OPTIONS
					if(value[5] != undefined && value[6] == undefined /*&& value[7] != undefined && value[8] != undefined && value[9] != undefined && value[10] != undefined*/){
						document.getElementsByName(value[5])[garmentssize*value[0] + value[1]].classList.add('selected');	
					}
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);
					
					if(value[5] != undefined && value[6] != undefined && value[7] != undefined && value[8] != undefined && value[9] != undefined && value[10] != undefined){
						if(that.suitJacketInsidePocketsDifferentFromPiping() == false){
							var images2 = document.getElementsByName(value[5]);
							var texts2 = document.getElementsByName(value[6]);
							var options2 = document.getElementsByName(value[7]);
							images2[value[0]].src = value[2].image;
							texts2[value[0]].innerHTML = value[2].title;
							var garmentssize = options2.length/images2.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options2[x].classList.remove('selected');	
							}
							options2[garmentssize*value[0] + value[1]].classList.add('selected');
						}
					}
		        });
		    }
		}; 
		
		ko.bindingHandlers.modifiedSuitJacketColors = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var listdivs = document.getElementsByName(value[3]);					
					var listimages = document.getElementsByName(value[4]);
					var listtexts = document.getElementsByName(value[5]);
					var options = document.getElementsByName(value[6]);
					var garmentssize = document.getElementById(value[7]).innerHTML;
					var jacketoptionsnumber = listdivs.length/garmentssize;
					var coloroptionsnumber = options.length/garmentssize;
					
					for(var x = 0; x < jacketoptionsnumber; x++){
						var y = value[0] + x*garmentssize;
						if(listimages.length > 0){	 				
							listimages[y].src = value[2].image;
						}
						if(listtexts.length > 0){
							listtexts[y].innerHTML = value[2].title;
						}
					}					
					for(var x = coloroptionsnumber*value[0]; x < coloroptionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[coloroptionsnumber*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);	
		        });
		        
		    }
		};	
		
		ko.bindingHandlers.modifiedSuitJacketButtonNumber = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					//	[$parentContext.$index(), $index(), $data, 'suitJacketSleeveButtonNumber', 'suitJacketSleeveButtonNumberList']
					var maindivs = document.getElementsByName(value[3]);
					var options = document.getElementsByName(value[4]);
					
					//var garmentssize = maindivs.length;
					var optionsnumber = options.length / maindivs.length;
					for(var x = optionsnumber*value[0]; x < optionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[optionsnumber*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);
					
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.modifiedSuitColors = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var listdivs = document.getElementsByName(value[3]);					
					var listimages = document.getElementsByName(value[4]);
					var listtexts = document.getElementsByName(value[5]);
					var options = document.getElementsByName(value[6]);
					var garmentssize = document.getElementById(value[7]).innerHTML;
					var suitoptionsnumber = listdivs.length/garmentssize;
					var coloroptionsnumber = options.length/garmentssize;
					
					for(var x = 0; x < suitoptionsnumber; x++){
						var y = value[0] + x*garmentssize;
						if(listimages.length > 0){	 				
							listimages[y].src = value[2].image;
						}
						if(listtexts.length > 0){
							listtexts[y].innerHTML = value[2].title;
						}
					}					
					for(var x = coloroptionsnumber*value[0]; x < coloroptionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[coloroptionsnumber*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);
		        });
		    }
		};		
		
		
		ko.bindingHandlers.modifiedSuitJacketFit = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					for(var x = 0; x < that.suitData.length; x++){
						that.suitData[ x ].suitJacketFit = value;	
					}
					that.flushModel();
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.modifiedSuitPantFit = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					for(var x = 0; x < that.suitData.length; x++){
						that.suitData[ x ].PantFit = value;	
					}
					that.flushModel();			
		        });
		    }
		}; 		
				
		

		ko.bindingHandlers.modifiedSuitDropdown = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					that.selectedVariantSuit(that.variantNameSuit()[value[0]]);
		        });
		    }
		}; 
		
		ko.bindingHandlers.modifiedSuitJacketLapelWidth = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var values = valueAccessor();	
					var maindivs = document.getElementsByName(values[1]);
					var alltexts = document.getElementsByName(values[2]);
					var garmentscount = alltexts.length/maindivs.length;
					var optionscount = maindivs.length;
					
					for(var a = 0; a < maindivs.length; a++){
						for(var b = 0; b < garmentscount; b++){
							var index = a*garmentscount + b;						
							alltexts[index].innerHTML = that.suitData[ b ].suitJacketLapelWidth; 
						}	
					}
		        });
		    }
		}; 				
		
		
		ko.bindingHandlers.modifyClassOfDiv = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {	        	  		        	      	
					var value = valueAccessor();
					var index = value[0];
					var divs = document.getElementsByName(value[1]);
					var classname = 'activated'; //value[2];
					var activeclassname = 'activate';//value[3];
					//var notactiveclassname = value[4];
					
					var addtheclass = true;
					if( divs[ index ].classList.contains(classname) ){
						addtheclass = false;
					}
					for(var x = 0; x < divs.length; x++){
						divs[ x ].classList.remove(classname);
					}		
					if(addtheclass == true){			
						divs[ index ].classList.add(classname);
					}	
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.suitFabricApplier = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
			//		for(var x = 0; x < value.length; x++){
			//			console.log("VALUE " + x + ": " + JSON.stringify(value[x]) );
			//		}
					
					var checkboxes = document.getElementsByName(value[0]);
					var texts = document.getElementsByName(value[3]);
					var images = document.getElementsByName(value[4]);
					var cssclass = "" + value[2];
					var tomakegreenelements = document.getElementsByClassName("tomakegreen"); 
					
					var garmentsnumber = checkboxes.length;
					var textsnumber = texts.length;
					var differenttypesnumber = textsnumber/garmentsnumber;

					var anychecked = false;
					
					for(var x = 0; x < checkboxes.length; x++){
						if( checkboxes[x].classList.contains(cssclass)){
							
							orderItem.removeFabric('Suit', 2, 'Suit ' + (x + 1), that.suitData[ x ].SuitCustomerFabric, that.suitData[ x ].variantId, 4);
							if(!orderItem.ClientFabric() && document.getElementById("selectedfabric").innerHTML != "none"){
								checkboxes[x].classList.remove(cssclass);
								tomakegreenelements[x].classList.add('makegreen');
								var timestampid = that.suitData[ x ].timestampID;
								anychecked = true;
								that.suitData[ x ].suitFabric = value[1];
								that.suitData[ x ].SuitCustomerFabric = {};
								that.suitData[ x ].SuitHasCustomerFabric = false;
				
								try{
									for(var a = 0; a < dsRegistry.getDatasource('garmentsVestDS').getStore().vest.length; a++ ){
										if(dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].parentSuitTimestampID == timestampid  ){
											dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].vestFabric = value[1];
											dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].VestCustomerFabric = {};
											dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].VestHasCustomerFabric = false;
										}
									}
								}catch(e){ ; }
								try{	
									for(var a = 0; a < dsRegistry.getDatasource('garmentsPantDS').getStore().Pant.length; a++ ){
										if(dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].parentSuitTimestampID == timestampid  ){
											dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantFabric = value[1];
											dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantCustomerFabric = {};
											dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantHasCustomerFabric = false;
										}
									}
								}catch(e){ ; }
								
								
								if(texts.length > 0){
									//texts[x].innerHTML = value[1].title;
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = value[1].title;
											texts[pos].style.display = "block";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = '';
											texts[pos+1].style.display = "none";
										}
									}
								}
								if(images.length > 0){
									images[x].src = value[1].fabricImage;
								}
							}
							else if(orderItem.ClientFabric()) {
								if(orderItem.addFabricToGarment(that.suitData[ x ], 'Suit', 2,'Suit ' + (x + 1), 4)){
									checkboxes[x].classList.remove(cssclass);
									tomakegreenelements[x].classList.add('makegreen');
									var timestampid = that.suitData[ x ].timestampID;
									anychecked = true;
									
									that.suitData[ x ].suitFabric = dsRegistry.getDatasource('fabricsDS').getStore()[1];
									that.suitData[ x ].SuitCustomerFabric = orderItem.SelectedCustomerFabric();
									that.suitData[ x ].SuitHasCustomerFabric = true;
									
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = '';
											texts[pos].style.display = "none";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = orderItem.SelectedCustomerFabric().FabricCode;
											texts[pos+1].style.display = "block";
										}
									}
									try{
										for(var a = 0; a < dsRegistry.getDatasource('garmentsVestDS').getStore().vest.length; a++ ){
											if(dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].parentSuitTimestampID == timestampid  ){
												dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].vestFabric = dsRegistry.getDatasource('fabricsDS').getStore()[1];
												dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].VestCustomerFabric = orderItem.SelectedCustomerFabric();
												dsRegistry.getDatasource('garmentsVestDS').getStore().vest[a].VestHasCustomerFabric = true;
											}
										}
									}catch(e){ ; }
									try{	
										for(var a = 0; a < dsRegistry.getDatasource('garmentsPantDS').getStore().Pant.length; a++ ){
											if(dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].parentSuitTimestampID == timestampid  ){
												dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantFabric = dsRegistry.getDatasource('fabricsDS').getStore()[1];
												dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantCustomerFabric = orderItem.SelectedCustomerFabric();
												dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantHasCustomerFabric = true;
											}
										}
									}catch(e){ ; }
								}
								else if(that.suitData[ x ].SuitCustomerFabric.FabricCode){
									var temp = orderItem.SelectedCustomerFabric();
									orderItem.SelectedCustomerFabric(that.suitData[ x ].SuitCustomerFabric);
									orderItem.addFabricToGarment(that.suitData[ x ], 'Suit', 2,'Suit ' + (x + 1), 4);
									orderItem.SelectedCustomerFabric(temp);
								}
								
							}
						}
					}	
					if(anychecked == true){
						document.getElementById("selectedfabric").innerHTML = "none";
						document.getElementById("fabricrange").innerHTML = "0";
						document.getElementById("composition").innerHTML = "0";
						document.getElementById("fabricinput").value = "";
						document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
						document.getElementById("fabricinfo").style.display = "none";
						orderItem.ClientFabric(false);
						orderItem.SelectedCustomerFabric({});
					}
		        });
		    }
		}; 
		
		
		
		ko.bindingHandlers.modifiedSuitListBooleanDependencies = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var divs = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( checkboxes[ index ].classList.contains(value[4]) ){
							divs[ index ].classList.remove(value[5]);
						}else{
							divs[ index ].classList.add(value[5]);						
						}	
					}
								
		        });
		    }
		};   			

		this.suitJacketButtonsBottomDependencies = function( value0, value1 ){
			var bottomsList = document.getElementsByName(value0);
			var BottomsEachGarment = document.getElementsByName(value1);
			var garmentsnumber = BottomsEachGarment.length/bottomsList.length;
			
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < bottomsList.length; y++){
					var pos = y*garmentsnumber + x;
					if(pos < garmentsnumber*2){
						if(	that.suitData[ x ].suitJacketStructure.title.indexOf("Double Breasted") !=-1 || that.suitData[ x ].suitJacketStructure.title.indexOf("Double breasted") !=-1){			
							BottomsEachGarment[pos].style.display = "none";
						}else{
							BottomsEachGarment[pos].style.display = "";
						}		
					}	
				}
			}					
		}; 	
		
		this.suitJacketButtonsLapelDependencies = function( value0, value1 ){
			var lapelsList = document.getElementsByName(value0);
			var lapelsEachGarment = document.getElementsByName(value1);
			var garmentsnumber = lapelsEachGarment.length/lapelsList.length;
			
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < lapelsList.length; y++){
					var pos = y*garmentsnumber + x;
					if(pos == garmentsnumber*1 + x || pos == garmentsnumber*3 + x){
						if(	that.suitData[ x ].suitJacketStructure.title.indexOf("Double Breasted") !=-1 || that.suitData[ x ].suitJacketStructure.title.indexOf("Double breasted") !=-1){			
							lapelsEachGarment[pos].style.display = "none";
						}else{
							lapelsEachGarment[pos].style.display = "";
						}		
					}	
				}
			}					
		}; 			
		
		
		this.suitJacketDinnerTrimmingDependencies = function( value0, value1 ){	
			var TrimmingMain = document.getElementsByName(value0);
			var TrimmingElement = document.getElementsByName(value1);
			var garmentsnumber = TrimmingElement.length;
			
			var anydinner = false;
			for(var x = 0; x < garmentsnumber; x++){
				if(that.suitData[x].suitJacketDinner == true){
					anydinner = true;
					TrimmingElement[x].style.display = "block";
				}else{
					TrimmingElement[x].style.display = "none";
				}
			}
					
			if(anydinner == true){
				TrimmingMain[0].style.display = "block";
			}else{
				TrimmingMain[0].style.display = "none";
			}
		}; 		
		

		this.suitpantPleatsPocketsDependencies = function( value0, value1 ){
			var pantPocketsList = document.getElementsByName(value0);
			var pantPocketsEachGarment = document.getElementsByName(value1);
			var garmentsnumber = pantPocketsEachGarment.length/pantPocketsList.length;
			
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < pantPocketsList.length; y++){
					var pos = y*garmentsnumber + x;
					if(pos >= garmentsnumber*3){
						if(that.suitData[ x ].pantPleats.title != 'No Pleats' && that.suitData[ x ].pantPleats.title !== 'Single Pleat' && that.suitData[ x ].pantPleats.title !== 'Select Pleats'){
							pantPocketsEachGarment[pos].style.display = "none";
							
							if( that.suitData[ x ].pantPockets.title != 'Select Pocket' && that.suitData[ x ].pantPockets.title != 'No Pocket' && that.suitData[ x ].pantPockets.title != 'Slanted'){
								that.suitData[ x ].pantPockets = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
								if ("createEvent" in document) {
				    				var evt = document.createEvent("HTMLEvents");
				    				evt.initEvent("click", true, true);
				    				document.getElementsByName('pantPockets')[x].dispatchEvent(evt);
				    			}else{
				    				document.getElementsByName('pantPockets')[x].fireEvent("onclick");			
				    			}		
							}
						}else{
							pantPocketsEachGarment[pos].style.display = "block";
						}		
					}	
				}
			}					
		}; 				
		
			
		ko.bindingHandlers.suitCloneMonogram = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var values = valueAccessor();
					for(var x = 0; x < that.suitData.length; x++){
						that.suitData[ x ].suitJacketMonogram = values[0];	
						that.suitData[ x ].suitJacketDefaultMonogramHeader = values[1];
						that.suitData[ x ].suitJacketCustomMonogramHeader = values[2];
						//that.suitData[ x ].JacketMonogramNotes = values[3];
						that.suitData[ x ].suitJacketMonogramExtraLine = values[4];
						//that.suitData[ x ].suitJacketMonogramStitchColour = values[5]; 
					}
					that.flushModel();
		        });
		    }
		}; 	


	},
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},


	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"suit"  : this.suitData
		}, true);
	},

	selectVariantSuit: function() {
		this.renderSuit();
	},

	getVariant: function(id) {
		var toreturn = this.suitData[0];
		for (var ind in this.suitData) {
			if ( this.suitData[ind].variantId == id  ) {
				toreturn = this.suitData[ind];
			}
		}
		return toreturn;

	},

	getRow: function(id) {
		for (var ind in this.suitData) {
			if ( this.suitData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},

	addVariantSuit: function() {
		this.suitDataAID += 1;
		//var vname = "Suit " + (this.variantNameSuit().length + 1);
		var newid = this.suitDataAID + 1;
		var vname = "Suit " + newid;

		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantSuit().id )  ); //CLONE Object
		this.variantNameSuit.push({title: vname, id: this.suitDataAID});
		tObj.timestampID = new Date().getTime();
		tObj.variantId = this.suitDataAID;
		tObj.custom = new CustomProperties();
		if(orderItem.suitfirsttime == true){
			this.suitData.push(tObj);                                                       //Push to internal
		}	
		this.flushModel();
	},

	addVariantSuit2: function() {
		
		var currentcount = orderItem.garmentsList()[2].count();
//console.log('addVariantPant2 currentcount: ' + currentcount);	
		orderItem.garmentsList()[2].count( currentcount + 1);
//console.log('addVariantPant2 newcount: ' + orderItem.garmentsList()[2].count());
		
		//var vname = "Suit " + (this.variantNameSuit().length + 1);
		this.suitDataAID += 1;
		var newid = this.suitDataAID + 1;
		var vname = "Suit " + newid;

		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantSuit().id )  ); //CLONE Object
		this.variantNameSuit.push({title: vname, id: this.suitDataAID});
		tObj.timestampID = new Date().getTime();
		tObj.variantId = this.suitDataAID;
		tObj.custom = new CustomProperties();
		this.suitData.push(tObj);                                                       //Push to internal
		this.flushModel();
		this.currentStep(this.workflow()[0] );
		var lastelemindex = this.variantNameSuit().length -1;
		this.selectedVariantSuit( this.variantNameSuit()[lastelemindex] );
	},

	digestData: function(data) {
		this.suitData  = data.suit;
		this.renderView();

	},

	renderView: function() {
		this.renderSuit();
	},

	renderSuit: function() {
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantSuit().id);
			if (tData != null) {
	
				//Update observables
				
				if (typeof(tData.checkVisibility)    != "undefined") {this.checkVisibility(tData.checkVisibility);}
				if (typeof(tData.ExtraPants)    != "undefined") {this.ExtraPants(tData.ExtraPants);}
				if (typeof(tData.ExtraJacket)    != "undefined") {this.ExtraJacket(tData.ExtraJacket);}
				if (typeof(tData.SuitNotes)    != "undefined") {this.SuitNotes(tData.SuitNotes);}
				if (typeof(tData.JacketFabricNotes)    		!= "undefined") {this.JacketFabricNotes(tData.JacketFabricNotes);}
				if (typeof(tData.JacketStructureNotes)  != "undefined") {this.JacketStructureNotes(tData.JacketStructureNotes);}
				if (typeof(tData.JacketBottomNotes)    		!= "undefined") {this.JacketBottomNotes(tData.JacketBottomNotes);}
				if (typeof(tData.JacketVentNotes)  			!= "undefined") {this.JacketVentNotes(tData.JacketVentNotes);}
				if (typeof(tData.JacketLapelStyleNotes)     != "undefined") {this.JacketLapelStyleNotes(tData.JacketLapelStyleNotes);}
				if (typeof(tData.JacketButtonsNotes) != "undefined") {this.JacketButtonsNotes(tData.JacketPocketsNotes);}
				if (typeof(tData.JacketPocketsNotes) != "undefined") {this.JacketPocketsNotes(tData.JacketPocketsNotes);}
				if (typeof(tData.JacketLiningColoursNotes)  != "undefined") {this.JacketLiningColoursNotes(tData.JacketLiningColoursNotes);}
				if (typeof(tData.JacketMonogramNotes)    	!= "undefined") {this.JacketMonogramNotes(tData.JacketMonogramNotes);}
				
				if (typeof(tData.PantFabricNotes)    	!= "undefined") {this.PantFabricNotes(tData.PantFabricNotes);}
				if (typeof(tData.PantStyleNotes)    	!= "undefined") {this.PantStyleNotes(tData.PantStyleNotes);}
				if (typeof(tData.PantPleatsNotes)    	!= "undefined") {this.PantPleatsNotes(tData.PantPleatsNotes);}
				if (typeof(tData.PantFrontPocketsNotes) != "undefined") {this.PantFrontPocketsNotes(tData.PantFrontPocketsNotes);}
				if (typeof(tData.PantBackPocketsNotes)  != "undefined") {this.PantBackPocketsNotes(tData.PantBackPocketsNotes);}
				if (typeof(tData.PantBeltAndCuffsNotes) != "undefined") {this.PantBeltAndCuffsNotes(tData.PantBeltAndCuffsNotes);}
				
				
				if (typeof(tData.suitJacketCustomStructureNotes)    != "undefined") {this.suitJacketCustomStructureNotes(tData.suitJacketCustomStructureNotes);}
				if (typeof(tData.suitJacketCustomStructureImage)    != "undefined") {this.suitJacketCustomStructureImage(tData.suitJacketCustomStructureImage);}
				if (typeof(tData.suitJacketCustomVentNotes)       	!= "undefined") {this.suitJacketCustomVentNotes(tData.suitJacketCustomVentNotes);}
				if (typeof(tData.suitJacketCustomVentImage)        != "undefined") {this.suitJacketCustomVentImage(tData.suitJacketCustomVentImage);}
				if (typeof(tData.suitJacketCustomLapelNotes)       	!= "undefined") {this.suitJacketCustomLapelNotes(tData.suitJacketCustomLapelNotes);}
				if (typeof(tData.suitJacketCustomLapelImage)        != "undefined") {this.suitJacketCustomLapelImage(tData.suitJacketCustomLapelImage);}
				if (typeof(tData.suitJacketCustomBottomNotes)       != "undefined") {this.suitJacketCustomBottomNotes(tData.suitJacketCustomBottomNotes);}
				if (typeof(tData.suitJacketCustomBottomImage)       != "undefined") {this.suitJacketCustomBottomImage(tData.suitJacketCustomBottomImage);}
				//////////////
				if (typeof(tData.suitJacketMonogram)         		!= "undefined") {this.suitJacketMonogram(tData.suitJacketMonogram);}
				if (typeof(tData.suitJacketMonogramExtraLine)         		!= "undefined") {this.suitJacketMonogramExtraLine(tData.suitJacketMonogramExtraLine);}
				if (typeof(tData.suitJacketDefaultMonogramHeader)   != "undefined") {this.suitJacketDefaultMonogramHeader(tData.suitJacketDefaultMonogramHeader);}
				if (typeof(tData.suitJacketCustomMonogramHeader)    != "undefined") {this.suitJacketCustomMonogramHeader(tData.suitJacketCustomMonogramHeader);}
				if (typeof(tData.suitJacketMonogramStitchDifferentFromPiping) != "undefined") {this.suitJacketMonogramStitchDifferentFromPiping(tData.suitJacketMonogramStitchDifferentFromPiping);}
				if (typeof(tData.suitJacketMonogramStitchColour)	!= "undefined") {this.suitJacketMonogramStitchColour(tData.suitJacketMonogramStitchColour);}
				if (typeof(tData.suitJacketCustomMonogram)          != "undefined") {this.suitJacketCustomMonogram(tData.suitJacketCustomMonogram);}
				if (typeof(tData.suitJacketCustomMonogramImage)     != "undefined") {this.suitJacketCustomMonogramImage(tData.suitJacketCustomMonogramImage);}
				if (typeof(tData.suitJacketCustomMonogramNotes)     != "undefined") {this.suitJacketCustomMonogramNotes(tData.suitJacketCustomMonogramNotes);}
				if (typeof(tData.suitJacketEmbroidery)        		!= "undefined") {this.suitJacketEmbroidery(tData.suitJacketEmbroidery);}
				if (typeof(tData.suitJacketEmbroideryImage)        	!= "undefined") {this.suitJacketEmbroideryImage(tData.suitJacketEmbroideryImage);}
				if (typeof(tData.suitJacketEmbroideryNotes)        	!= "undefined") {this.suitJacketEmbroideryNotes(tData.suitJacketEmbroideryNotes);}
				//////////////
				if (typeof(tData.suitFabric)          			!= "undefined") {this.suitFabric(tData.suitFabric);}
				if (typeof(tData.suitJacketBottomStyle)    			!= "undefined") {this.suitJacketBottomStyle(tData.suitJacketBottomStyle);}
				if (typeof(tData.suitJacketVentStyle)     			!= "undefined") {this.suitJacketVentStyle(tData.suitJacketVentStyle);}
	
				if (typeof(tData.suitJacketLapelStyle)      		!= "undefined") {this.suitJacketLapelStyle(tData.suitJacketLapelStyle);}
				
				if (typeof(tData.suitJacketBreastPocketStyle)       != "undefined") {this.suitJacketBreastPocketStyle(tData.suitJacketBreastPocketStyle);}	 
	//			if (typeof(tData.suitJacketTicketPocketStyle)       != "undefined") {this.suitJacketTicketPocketStyle(tData.suitJacketTicketPocketStyle);}	 
				if (typeof(tData.suitJacketLapelWidth)      		!= "undefined") {this.suitJacketLapelWidth(tData.suitJacketLapelWidth);}
		//		if (typeof(tData.suitJacketLapelSatin)    			!= "undefined") {this.suitJacketLapelSatin(tData.suitJacketLapelSatin);}
	
				if (typeof(tData.suitJacketPocketStyle)    		    != "undefined") {this.suitJacketPocketStyle(tData.suitJacketPocketStyle);}
				if (typeof(tData.suitJacketSleeveButtonNumber)      != "undefined") {this.suitJacketSleeveButtonNumber(tData.suitJacketSleeveButtonNumber);}
				if (typeof(tData.suitJacketSleeveButtonType)    	!= "undefined") {this.suitJacketSleeveButtonType(tData.suitJacketSleeveButtonType);}
				if (typeof(tData.suitJacketTopStitch)        		!= "undefined") {this.suitJacketTopStitch(tData.suitJacketTopStitch);}
				if (typeof(tData.suitJacketTopStitchPocket) 		!= "undefined") {this.suitJacketTopStitchPocket(tData.suitJacketTopStitchPocket);}
				if (typeof(tData.suitJacketTopStitchLapel) 			!= "undefined") {this.suitJacketTopStitchLapel(tData.suitJacketTopStitchLapel);}
		//		if (typeof(tData.suitJacketTopStitchDarts) 			!= "undefined") {this.suitJacketTopStitchDarts(tData.suitJacketTopStitchDarts);}
		//		if (typeof(tData.suitJacketTopStitchAroundBottom) 	!= "undefined") {this.suitJacketTopStitchAroundBottom(tData.suitJacketTopStitchAroundBottom);}
		//		if (typeof(tData.suitJacketTopStitchContrast)		!= "undefined") {this.suitJacketTopStitchContrast(tData.suitJacketTopStitchContrast);}
				if (typeof(tData.suitJacketTopStitchCode)			!= "undefined") {this.suitJacketTopStitchCode(tData.suitJacketTopStitchCode);}			 
				if (typeof(tData.suitJacketTopStitchPiping)  		!= "undefined") {this.suitJacketTopStitchPiping(tData.suitJacketTopStitchPiping);}
				if (typeof(tData.suitJacketLining)           		!= "undefined") {this.suitJacketLining(tData.suitJacketLining);}
				if (typeof(tData.suitJacketButtonColor)      		!= "undefined") {this.suitJacketButtonColor(tData.suitJacketButtonColor);}
				if (typeof(tData.suitJacketThreadColor)      		!= "undefined") {this.suitJacketThreadColor(tData.suitJacketThreadColor);}
				
				
	            if (typeof(tData.suitJacketStructure) 				!= "undefined") {this.suitJacketStructure(tData.suitJacketStructure);}
	            if (typeof(tData.suitJacketDesignerVent)			!= "undefined") {this.suitJacketDesignerVent(tData.suitJacketDesignerVent);}				 
	            if (typeof(tData.suitJacketTail)					!= "undefined") {this.suitJacketTail(tData.suitJacketTail);}				 
	  //          if (typeof(tData.suitJacketCustomVent)			 		!= "undefined") {this.suitJacketCustomVent(tData.suitJacketCustomVent);}								 
	            if (typeof(tData.suitJacketDinner)			 		!= "undefined") {this.suitJacketDinner(tData.suitJacketDinner);}								 
	            if (typeof(tData.suitJacketCollarAndLapelTrimming)	!= "undefined") {this.suitJacketCollarAndLapelTrimming(tData.suitJacketCollarAndLapelTrimming);}	 
	            if (typeof(tData.suitJacketContrastTrimming)        != "undefined") {this.suitJacketContrastTrimming(tData.suitJacketContrastTrimming);}
	            if (typeof(tData.suitJacketCollarTrimming)        	!= "undefined") {this.suitJacketCollarTrimming(tData.suitJacketCollarTrimming);}				 
	            if (typeof(tData.suitJacketLapelTrimming)        	!= "undefined") {this.suitJacketLapelTrimming(tData.suitJacketLapelTrimming);}					 
	            if (typeof(tData.suitJacketCoverButtons)        	!= "undefined") {this.suitJacketCoverButtons(tData.suitJacketCoverButtons);}					 
	            if (typeof(tData.suitJacketPocketTrimming)        	!= "undefined") {this.suitJacketPocketTrimming(tData.suitJacketPocketTrimming);}
	            if (typeof(tData.suitJacketBreastPocketTrimming)        	!= "undefined") {this.suitJacketBreastPocketTrimming(tData.suitJacketBreastPocketTrimming);}					 
				if (typeof(tData.suitJacketContrastPockets)         != "undefined") {this.suitJacketContrastPockets(tData.suitJacketContrastPockets);}//suitJacketContrastPockets
				if (typeof(tData.suitJacketContrastCheckPocket)     != "undefined") {this.suitJacketContrastCheckPocket(tData.suitJacketContrastCheckPocket);}
				if (typeof(tData.suitJacketContrastLapelLower)   	!= "undefined") {this.suitJacketContrastLapelLower(tData.suitJacketContrastLapelLower);}
				if (typeof(tData.suitJacketContrastLapelUpper)      != "undefined") {this.suitJacketContrastLapelUpper(tData.suitJacketContrastLapelUpper);}
				if (typeof(tData.suitJacketLapelButtonHole)         != "undefined") {this.suitJacketLapelButtonHole(tData.suitJacketLapelButtonHole);}
				if (typeof(tData.suitJacketTicketPocket)            != "undefined") {this.suitJacketTicketPocket(tData.suitJacketTicketPocket);}
				if (typeof(tData.suitJacketContrastTrimmingColour)     		!= "undefined") {this.suitJacketContrastTrimmingColour(tData.suitJacketContrastTrimmingColour);}
		//		if (typeof(tData.suitJacketBreastPocket)            != "undefined") {this.suitJacketBreastPocket(tData.suitJacketBreastPocket);}
				if (typeof(tData.suitJacketKissingButtons)          != "undefined") {this.suitJacketKissingButtons(tData.suitJacketKissingButtons);}
				if (typeof(tData.suitJacketButtonholeOnLapel)       != "undefined") {this.suitJacketButtonholeOnLapel(tData.suitJacketButtonholeOnLapel);}
		//		if (typeof(tData.suitJacketButtonholeOnLapelContrastCheck)       != "undefined") {this.suitJacketButtonholeOnLapelContrastCheck(tData.suitJacketButtonholeOnLapelContrastCheck);}		// REMOVED
				if (typeof(tData.suitJacketButtonholeOnLapelColor)  != "undefined") {this.suitJacketButtonholeOnLapelColor(tData.suitJacketButtonholeOnLapelColor);}
				if (typeof(tData.suitJacketBreastPocketAngledStyle)      != "undefined") {this.suitJacketBreastPocketAngledStyle(tData.suitJacketBreastPocketAngledStyle);}
				if (typeof(tData.suitJacketPiping)		            != "undefined") {this.suitJacketPiping(tData.suitJacketPiping);}
				if (typeof(tData.suitJacketInsidePocketsColour)		!= "undefined") {this.suitJacketInsidePocketsColour(tData.suitJacketInsidePocketsColour);}
				if (typeof(tData.suitJacketFit)              		!= "undefined") {this.suitJacketFit(tData.suitJacketFit);}
				
				if (typeof(tData.suitJacketInsidePocketsDifferentFromPiping)  != "undefined") {this.suitJacketInsidePocketsDifferentFromPiping(tData.suitJacketInsidePocketsDifferentFromPiping);}
				if (typeof(tData.suitJacketTicketPocketsDifferent)            != "undefined") {this.suitJacketTicketPocketsDifferent(tData.suitJacketTicketPocketsDifferent);}
		//		if (typeof(tData.suitJacketButtonContrast)        		  != "undefined") {this.suitJacketButtonContrast(tData.suitJacketButtonContrast);}
				if (typeof(tData.suitJacketSleeveButton1Colour)           != "undefined") {this.suitJacketSleeveButton1Colour(tData.suitJacketSleeveButton1Colour);}
				if (typeof(tData.suitJacketSleeveButton2Colour)           != "undefined") {this.suitJacketSleeveButton2Colour(tData.suitJacketSleeveButton2Colour);}
				if (typeof(tData.suitJacketSleeveButton3Colour)           != "undefined") {this.suitJacketSleeveButton3Colour(tData.suitJacketSleeveButton3Colour);}
				if (typeof(tData.suitJacketSleeveButton4Colour)           != "undefined") {this.suitJacketSleeveButton4Colour(tData.suitJacketSleeveButton4Colour);}
				if (typeof(tData.suitJacketSleeveButton5Colour)           != "undefined") {this.suitJacketSleeveButton5Colour(tData.suitJacketSleeveButton5Colour);}
				if (typeof(tData.suitJacketSleeveButtonHole1Colour)       != "undefined") {this.suitJacketSleeveButtonHole1Colour(tData.suitJacketSleeveButtonHole1Colour);}
				if (typeof(tData.suitJacketSleeveButtonHole2Colour)       != "undefined") {this.suitJacketSleeveButtonHole2Colour(tData.suitJacketSleeveButtonHole2Colour);}
				if (typeof(tData.suitJacketSleeveButtonHole3Colour)       != "undefined") {this.suitJacketSleeveButtonHole3Colour(tData.suitJacketSleeveButtonHole3Colour);}
				if (typeof(tData.suitJacketSleeveButtonHole4Colour)       != "undefined") {this.suitJacketSleeveButtonHole4Colour(tData.suitJacketSleeveButtonHole4Colour);}
				if (typeof(tData.suitJacketSleeveButtonHole5Colour)       != "undefined") {this.suitJacketSleeveButtonHole5Colour(tData.suitJacketSleeveButtonHole5Colour);}
				if (typeof(tData.suitJacketSleeveButton1StitchColour)     != "undefined") {this.suitJacketSleeveButton1StitchColour(tData.suitJacketSleeveButton1StitchColour);}
				if (typeof(tData.suitJacketSleeveButton2StitchColour)     != "undefined") {this.suitJacketSleeveButton2StitchColour(tData.suitJacketSleeveButton2StitchColour);}
				if (typeof(tData.suitJacketSleeveButton3StitchColour)     != "undefined") {this.suitJacketSleeveButton3StitchColour(tData.suitJacketSleeveButton3StitchColour);}
				if (typeof(tData.suitJacketSleeveButton4StitchColour)     != "undefined") {this.suitJacketSleeveButton4StitchColour(tData.suitJacketSleeveButton4StitchColour);}
				if (typeof(tData.suitJacketSleeveButton5StitchColour)     != "undefined") {this.suitJacketSleeveButton5StitchColour(tData.suitJacketSleeveButton5StitchColour);}			
				
				if (typeof(tData.pantsCustomPocketsNotes)    != "undefined") {this.pantsCustomPocketsNotes(tData.pantsCustomPocketsNotes);}
				if (typeof(tData.pantsCustomPocketsImage)    != "undefined") {this.pantsCustomPocketsImage(tData.pantsCustomPocketsImage);}			
				if (typeof(tData.PantContrastFabric)	!= "undefined") {this.PantContrastFabric(tData.PantContrastFabric);}
				if (typeof(tData.beltLoopStyle)			!= "undefined") {this.beltLoopStyle(tData.beltLoopStyle);}
				if (typeof(tData.cuffsStyle)			!= "undefined") {this.cuffsStyle(tData.cuffsStyle);}
				if (typeof(tData.pantFit)				!= "undefined") {this.pantFit(tData.pantFit);}
				if (typeof(tData.pantPleats)			!= "undefined") {this.pantPleats(tData.pantPleats);}
				if (typeof(tData.bpantPockets)			!= "undefined") {this.bpantPockets(tData.bpantPockets);}
				if (typeof(tData.pbackpocketMethod)		!= "undefined") {this.pbackpocketMethod(tData.pbackpocketMethod);}
				if (typeof(tData.pantCuffs)				!= "undefined") {this.pantCuffs(tData.pantCuffs);}
				if (typeof(tData.PantsBracesProvisions)	!= "undefined") {this.PantsBracesProvisions(tData.PantsBracesProvisions);}
		//		if (typeof(tData.PantsBackPocketButton)	!= "undefined") {this.PantsBackPocketButton(tData.PantsBackPocketButton);}
		//		if (typeof(tData.PantsAngledBeltLoop)	!= "undefined") {this.PantsAngledBeltLoop(tData.PantsAngledBeltLoop);}
				if (typeof(tData.pantPockets)			!= "undefined") {this.pantPockets(tData.pantPockets);}               
				if (typeof(tData.pantPockets1)			!= "undefined") {this.pantPockets1(tData.pantPockets1);}
				if (typeof(tData.pantPockets2)			!= "undefined") {this.pantPockets2(tData.pantPockets2);}
				if (typeof(tData.PantFit)				!= "undefined") {this.PantFit(tData.PantFit);}
	
				if (typeof(tData.urgent)			!= "undefined") {this.urgent(tData.urgent);}
				//if (typeof(tData.urgentDate)		!= "undefined") {this.urgentDate(tData.urgentDate);}
				if (typeof(tData.DOP_day)		!= "undefined") {this.DOP_day(tData.DOP_day);}
				if (typeof(tData.DOP_month)		!= "undefined") {this.DOP_month(tData.DOP_month);}
				if (typeof(tData.DOP_date)		!= "undefined") {this.DOP_date(tData.DOP_date);}
				
	
				if ( this.currentStep().id == 0) this.suitFabricSelect();
				if ( this.currentStep().id == 4) this.pantContrastFabricSelect();
	
			}
		}catch(e){
			;
		}	
	}

});

defSuit = SimpleDatasource.extend({
	init: function(name, dsRegistry, olddata, count) {
		if(olddata == null || olddata == undefined){
			
			var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
			var suit = {};
			suit.variantId        = 0;
			suit.timestampID = new Date().getTime();
			
			suit.checkVisibility	= true;
			suit.ExtraPants = 0;	//false
			suit.ExtraJacket = 0;	//false
			suit.SuitNotes	= '';
			suit.JacketFabricNotes			= '';
			suit.JacketStructureNotes	= '';
			suit.JacketBottomNotes			= '';
			suit.JacketVentNotes			= '';
			suit.JacketLapelStyleNotes		= '';
			suit.JacketButtonsNotes	= '';
			suit.JacketPocketsNotes	= '';
			suit.JacketLiningColoursNotes	= '';
			suit.JacketMonogramNotes		= '';
			suit.PantFabricNotes		= '';
			suit.PantStyleNotes			= '';
			suit.PantPleatsNotes		= '';
			suit.PantFrontPocketsNotes	= '';
			suit.PantBackPocketsNotes	= '';
			suit.PantBeltAndCuffsNotes	= '';
			
			suit.suitJacketCustomStructureNotes	= '';
			suit.suitJacketCustomStructureImage	= '';
			suit.suitJacketCustomVentNotes		= '';
			suit.suitJacketCustomVentImage		= '';
			suit.suitJacketCustomLapelNotes		= '';
			suit.suitJacketCustomLapelImage		= '';
			suit.suitJacketCustomBottomNotes		= '';
			suit.suitJacketCustomBottomImage		= ''; 
			
			suit.suitJacketMonogram        = '';				// 9a
			suit.suitJacketMonogramExtraLine        = '';
			suit.suitJacketDefaultMonogramHeader  = true;		// 9b
			suit.suitJacketCustomMonogramHeader  = '';	// 9b
			suit.suitJacketMonogramStitchDifferentFromPiping = false;	// 9d
			suit.suitJacketMonogramStitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';		// 9d
			
			suit.suitJacketCustomMonogram			= false;	// 9 optional A
			suit.suitJacketCustomMonogramImage = '';			// 9 optional A
			suit.suitJacketCustomMonogramNotes = '';			// 9 optional A
			
			suit.suitJacketEmbroidery	= false;			// 9 optional B
			suit.suitJacketEmbroideryImage = '';			// 9 optional B
			suit.suitJacketEmbroideryNotes = '';			// 9 optional B
	
			suit.suitFabric 	 	 = df;
			suit.suitJacketBottomStyle	 = dsRegistry.getDatasource('suitBottomStyleDS').getStore()[0];
			suit.suitJacketVentStyle   	 = dsRegistry.getDatasource('suitVentStyleDS').getStore()[0];
	        suit.suitJacketStructure = dsRegistry.getDatasource('suitJacketStructureDS').getStore()[0];	
	        
			suit.suitJacketLapelStyle  	 = dsRegistry.getDatasource('suitLapelStyleDS').getStore()[0];
			suit.suitJacketBreastPocketStyle = dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore()[1]; //"Standard Pocket";
			suit.suitJacketTicketPocketsDifferent = false;
	//		suit.suitJacketTicketPocketStyle = dsRegistry.getDatasource('tickPocketStyleDS').getStore()[0];
			suit.suitJacketLapelWidth   	 = 0;
	//		suit.suitJacketLapelSatin  	 = false;
			suit.suitJacketPocketStyle  	 = dsRegistry.getDatasource('suitPocketStyleDS').getStore()[0];
			suit.suitJacketSleeveButtonNumber	= dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore()[3];
			suit.suitJacketSleeveButtonType	= 'Working';
			suit.suitJacketTopStitch         = false;
			suit.suitJacketTopStitchPocket   = false;
			suit.suitJacketTopStitchLapel   = false;
		//	suit.suitJacketTopStitchDarts   = false;
		//	suit.suitJacketTopStitchAroundBottom   = false;
		//	suit.suitJacketTopStitchContrast = false;
			suit.suitJacketTopStitchCode      = dsRegistry.getDatasource('pipingColorDS').getStore()[0];	
	    //    suit.suitJacketBreastPocket        = true;
	        suit.suitJacketKissingButtons      = false;
	        suit.suitJacketButtonholeOnLapel      = true;
	    //    suit.suitJacketButtonholeOnLapelContrastCheck      = false;		// REMOVED
	        suit.suitJacketButtonholeOnLapelColor = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
	        suit.suitJacketBreastPocketAngledStyle  = 'Straight';
	        suit.suitJacketLapelButtonHole	 = true;
	        suit.suitJacketTicketPocket		 = false;
		    suit.suitJacketContrastLapelUpper  = false;
	        suit.suitJacketContrastLapelLower  = false;
	        suit.suitJacketContrastPockets     = false; 
	        suit.suitJacketContrastCheckPocket = false;
	        suit.suitJacketCollarAndLapelTrimming	 = false;
	        suit.suitJacketContrastTrimming	 = false;	
	        suit.suitJacketCollarTrimming		 = false;
	        suit.suitJacketLapelTrimming	 	= false;
	        suit.suitJacketCoverButtons	 	= false;
	        suit.suitJacketPocketTrimming	 	= false;
	        suit.suitJacketBreastPocketTrimming	 	= false;
	        suit.suitJacketDesignerVent		= false;
	        suit.suitJacketTail		= false;     			
	  //      suit.suitJacketCustomVent				= false;   			
	        suit.suitJacketDinner				= false;   			
			suit.suitJacketTopStitchPiping   	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketPiping            	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketInsidePocketsColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0]; // ='';
			suit.suitJacketLining              = dsRegistry.getDatasource('liningFabricDS').getStore()[0];
			suit.suitJacketButtonColor         = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			suit.suitJacketThreadColor         = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2];
			
			suit.suitJacketInsidePocketsDifferentFromPiping = false;
			
	//		suit.suitJacketLiningCode = dsRegistry.getDatasource('liningFabricDS').getStore()[0];//'';
			suit.suitJacketContrastTrimmingColour = '';//dsRegistry.getDatasource('pipingColorDS').getStore()[0];
	//		suit.suitJacketPipingCode = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';
	//		suit.suitJacketButtonContrast	= false;			// 9 optional B
			suit.suitJacketSleeveButton1Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			suit.suitJacketSleeveButton2Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			suit.suitJacketSleeveButton3Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			suit.suitJacketSleeveButton4Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			suit.suitJacketSleeveButton5Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			
			suit.suitJacketSleeveButtonHole1Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButtonHole2Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButtonHole3Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButtonHole4Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButtonHole5Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			
			suit.suitJacketSleeveButton1StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButton2StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButton3StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButton4StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			suit.suitJacketSleeveButton5StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
	
			suit.PantContrastFabric = df;
			suit.pantsCustomPocketsNotes	= '';
			suit.pantsCustomPocketsImage	= '';		
			suit.beltLoopStyle      = dsRegistry.getDatasource('beltLoopStyleDS').getStore()[0];
	//		suit.PantsAngledBeltLoop = false;
			suit.pantCuffs          = false;
			suit.PantsBracesProvisions         = false;
			suit.pbackpocketMethod  = "Both";
	//		suit.PantsBackPocketButton = true;
			suit.cuffsStyle         = dsRegistry.getDatasource('cuffsStyleDS').getStore()[0];
			suit.pantFit            = dsRegistry.getDatasource('pantFitDS').getStore()[0];
			suit.pantPleats         = dsRegistry.getDatasource('pantPleatsDS').getStore()[0];
			suit.bpantPockets      = dsRegistry.getDatasource('bpantPocketsDS').getStore()[0];
			suit.pantPockets       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
	        suit.pantPockets1       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
	        suit.pantPockets2       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
	        suit.PantFit      		= 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2]
	        suit.custom     =    new CustomProperties();
	
			suit.urgent = false;
			//suit.urgentDate = '';
			suit.DOP_day = '';
			suit.DOP_month = '';
			suit.DOP_date = '';
			suit.SuitCustomerFabric = {};
			suit.SuitHasCustomerFabric = false;
			suit.GarmentPrice = 0;
	
			var iSuit = {
				suit: []
			};
	
			iSuit.suit.push(suit);
			this._super(name, iSuit, dsRegistry);
					
		}else{
			
			var iSuit = {
				suit: []
			};	
			if(olddata.length > count){
				for(var x = count; x < olddata.length; x++){
					orderItem.removeFabric('Suit', 2, 'Suit ' + (x + 1), olddata[ x ].SuitCustomerFabric, olddata[ x ].variantId, 4);
				}
			}
			for(var x = 0; x < count; x++){
				
				if(olddata[x] != undefined){
					var suit = {};
					suit.variantId        = x; //olddata[x].variantId; // <- the commented code can cause garments with the same variantId
					suit.timestampID 	  = olddata[x].timestampID;
			
					suit.checkVisibility	= olddata[x].checkVisibility;
					suit.ExtraPants 		= olddata[x].ExtraPants;
					suit.ExtraJacket 		= olddata[x].ExtraJacket;
					suit.SuitNotes			= olddata[x].SuitNotes;
					suit.JacketFabricNotes			= olddata[x].JacketFabricNotes;
					suit.JacketStructureNotes		= olddata[x].JacketStructureNotes;
					suit.JacketBottomNotes			= olddata[x].JacketBottomNotes;
					suit.JacketVentNotes			= olddata[x].JacketVentNotes;
					suit.JacketLapelStyleNotes		= olddata[x].JacketLapelStyleNotes;
					suit.JacketButtonsNotes			= olddata[x].JacketButtonsNotes;
					suit.JacketPocketsNotes			= olddata[x].JacketPocketsNotes;
					suit.JacketLiningColoursNotes	= olddata[x].JacketLiningColoursNotes;
					suit.JacketMonogramNotes		= olddata[x].JacketMonogramNotes;
					suit.PantFabricNotes			= olddata[x].PantFabricNotes;
					suit.PantStyleNotes				= olddata[x].PantStyleNotes;
					suit.PantPleatsNotes			= olddata[x].PantPleatsNotes;
					suit.PantFrontPocketsNotes		= olddata[x].PantFrontPocketsNotes;
					suit.PantBackPocketsNotes		= olddata[x].PantBackPocketsNotes;
					suit.PantBeltAndCuffsNotes		= olddata[x].PantBeltAndCuffsNotes;
					
					suit.suitJacketCustomStructureNotes	= olddata[x].suitJacketCustomStructureNotes;
					suit.suitJacketCustomStructureImage	= olddata[x].suitJacketCustomStructureImage;
					suit.suitJacketCustomVentNotes		= olddata[x].suitJacketCustomVentNotes;
					suit.suitJacketCustomVentImage		= olddata[x].suitJacketCustomVentImage;
					suit.suitJacketCustomLapelNotes		= olddata[x].suitJacketCustomLapelNotes;
					suit.suitJacketCustomLapelImage		= olddata[x].suitJacketCustomLapelImage;
					suit.suitJacketCustomBottomNotes	= olddata[x].suitJacketCustomBottomNotes;
					suit.suitJacketCustomBottomImage	= olddata[x].suitJacketCustomBottomImage; 
					
					suit.suitJacketMonogram        			= olddata[x].suitJacketMonogram;
					suit.suitJacketMonogramExtraLine        			= olddata[x].suitJacketMonogramExtraLine;
					suit.suitJacketDefaultMonogramHeader  	= olddata[x].suitJacketDefaultMonogramHeader;
					suit.suitJacketCustomMonogramHeader  	= olddata[x].suitJacketCustomMonogramHeader;
					suit.suitJacketMonogramStitchDifferentFromPiping = olddata[x].suitJacketMonogramStitchDifferentFromPiping;
					suit.suitJacketMonogramStitchColour 	= olddata[x].suitJacketMonogramStitchColour;
					
					suit.suitJacketCustomMonogram			= olddata[x].suitJacketCustomMonogram;
					suit.suitJacketCustomMonogramImage 		= olddata[x].suitJacketCustomMonogramImage;
					suit.suitJacketCustomMonogramNotes 		= olddata[x].suitJacketCustomMonogramNotes;
					
					suit.suitJacketEmbroidery		= olddata[x].suitJacketEmbroidery;
					suit.suitJacketEmbroideryImage  = olddata[x].suitJacketEmbroideryImage;
					suit.suitJacketEmbroideryNotes  = olddata[x].suitJacketEmbroideryNotes;
			
					suit.suitFabric 	 	 	 = olddata[x].suitFabric;
					suit.suitJacketBottomStyle	 = olddata[x].suitJacketBottomStyle;
					suit.suitJacketVentStyle   	 = olddata[x].suitJacketVentStyle;
			        suit.suitJacketStructure 	 = olddata[x].suitJacketStructure;	
			        
					suit.suitJacketLapelStyle  	 = olddata[x].suitJacketLapelStyle;
					suit.suitJacketBreastPocketStyle = olddata[x].suitJacketBreastPocketStyle;
					suit.suitJacketTicketPocketsDifferent = olddata[x].suitJacketTicketPocketsDifferent;
			//		suit.suitJacketTicketPocketStyle = olddata[x].suitJacketTicketPocketStyle;
					suit.suitJacketLapelWidth   	 = olddata[x].suitJacketLapelWidth;
			//		suit.suitJacketLapelSatin  	 	 = olddata[x].suitJacketLapelSatin;
					suit.suitJacketPocketStyle  	 = olddata[x].suitJacketPocketStyle;
					suit.suitJacketSleeveButtonNumber	= olddata[x].suitJacketSleeveButtonNumber;
					suit.suitJacketSleeveButtonType	 = olddata[x].suitJacketSleeveButtonType;
					suit.suitJacketTopStitch         = olddata[x].suitJacketTopStitch;
					suit.suitJacketTopStitchPocket   = olddata[x].suitJacketTopStitchPocket;
					suit.suitJacketTopStitchLapel    = olddata[x].suitJacketTopStitchLapel;
				//	suit.suitJacketTopStitchDarts    = olddata[x].suitJacketTopStitchDarts;
				//	suit.suitJacketTopStitchAroundBottom   = olddata[x].suitJacketTopStitchAroundBottom;
				//	suit.suitJacketTopStitchContrast = olddata[x].suitJacketTopStitchContrast;
					suit.suitJacketTopStitchCode     = olddata[x].suitJacketTopStitchCode;	
			    //    suit.suitJacketBreastPocket      = olddata[x].suitJacketBreastPocket;
			        suit.suitJacketKissingButtons    = olddata[x].suitJacketKissingButtons;
			        suit.suitJacketButtonholeOnLapel = olddata[x].suitJacketButtonholeOnLapel;
			  //      suit.suitJacketButtonholeOnLapelContrastCheck      = olddata[x].suitJacketButtonholeOnLapelContrastCheck;		// REMOVED
			        suit.suitJacketButtonholeOnLapelColor  = olddata[x].suitJacketButtonholeOnLapelColor;
			        suit.suitJacketBreastPocketAngledStyle = olddata[x].suitJacketBreastPocketAngledStyle;
			        suit.suitJacketLapelButtonHole	   = olddata[x].suitJacketLapelButtonHole;
			        suit.suitJacketTicketPocket		   = olddata[x].suitJacketTicketPocket;
				    suit.suitJacketContrastLapelUpper  = olddata[x].suitJacketContrastLapelUpper;
			        suit.suitJacketContrastLapelLower  = olddata[x].suitJacketContrastLapelLower;
			        suit.suitJacketContrastPockets     = olddata[x].suitJacketContrastPockets; 
			        suit.suitJacketContrastCheckPocket = olddata[x].suitJacketContrastCheckPocket;
			        suit.suitJacketCollarAndLapelTrimming	 = olddata[x].suitJacketCollarAndLapelTrimming;
			        suit.suitJacketContrastTrimming	 = olddata[x].suitJacketContrastTrimming;
			        suit.suitJacketCollarTrimming	 = olddata[x].suitJacketCollarTrimming;
			        suit.suitJacketLapelTrimming	 = olddata[x].suitJacketLapelTrimming;
			        suit.suitJacketCoverButtons	  	 = olddata[x].suitJacketCoverButtons;
			        suit.suitJacketPocketTrimming	 = olddata[x].suitJacketPocketTrimming;
			        suit.suitJacketBreastPocketTrimming	 = olddata[x].suitJacketBreastPocketTrimming;
			        suit.suitJacketDesignerVent		 = olddata[x].suitJacketDesignerVent;
			        suit.suitJacketTail				 = olddata[x].suitJacketTail;     			
			  //    suit.suitJacketCustomVent	 	 = olddata[x].suitJacketCustomVent;   			
			        suit.suitJacketDinner			 = olddata[x].suitJacketDinner;   			
					suit.suitJacketTopStitchPiping   = olddata[x].suitJacketTopStitchPiping;
					suit.suitJacketPiping            = olddata[x].suitJacketPiping;
					suit.suitJacketInsidePocketsColour = olddata[x].suitJacketInsidePocketsColour;
					suit.suitJacketLining              = olddata[x].suitJacketLining;
					suit.suitJacketButtonColor         = olddata[x].suitJacketButtonColor;
					suit.suitJacketThreadColor         = olddata[x].suitJacketThreadColor;
					suit.suitJacketFit = olddata[x].suitJacketFit;
					
					suit.suitJacketInsidePocketsDifferentFromPiping = olddata[x].suitJacketInsidePocketsDifferentFromPiping;
					
			//		suit.suitJacketLiningCode = olddata[x].suitJacketLiningCode;
					suit.suitJacketContrastTrimmingColour = olddata[x].suitJacketContrastTrimmingColour;
			//		suit.suitJacketPipingCode = olddata[x].suitJacketPipingCode;
			//		suit.suitJacketButtonContrast	   = olddata[x].suitJacketButtonContrast;
					suit.suitJacketSleeveButton1Colour = olddata[x].suitJacketSleeveButton1Colour;
					suit.suitJacketSleeveButton2Colour = olddata[x].suitJacketSleeveButton2Colour;
					suit.suitJacketSleeveButton3Colour = olddata[x].suitJacketSleeveButton3Colour;
					suit.suitJacketSleeveButton4Colour = olddata[x].suitJacketSleeveButton4Colour;
					suit.suitJacketSleeveButton5Colour = olddata[x].suitJacketSleeveButton5Colour;
					
					suit.suitJacketSleeveButtonHole1Colour = olddata[x].suitJacketSleeveButtonHole1Colour;
					suit.suitJacketSleeveButtonHole2Colour = olddata[x].suitJacketSleeveButtonHole2Colour;
					suit.suitJacketSleeveButtonHole3Colour = olddata[x].suitJacketSleeveButtonHole3Colour;
					suit.suitJacketSleeveButtonHole4Colour = olddata[x].suitJacketSleeveButtonHole4Colour;
					suit.suitJacketSleeveButtonHole5Colour = olddata[x].suitJacketSleeveButtonHole5Colour;
					
					suit.suitJacketSleeveButton1StitchColour = olddata[x].suitJacketSleeveButton1StitchColour;
					suit.suitJacketSleeveButton2StitchColour = olddata[x].suitJacketSleeveButton2StitchColour;
					suit.suitJacketSleeveButton3StitchColour = olddata[x].suitJacketSleeveButton3StitchColour;
					suit.suitJacketSleeveButton4StitchColour = olddata[x].suitJacketSleeveButton4StitchColour;
					suit.suitJacketSleeveButton5StitchColour = olddata[x].suitJacketSleeveButton5StitchColour;
					suit.PantContrastFabric 		= olddata[x].PantContrastFabric;
					suit.pantsCustomPocketsNotes	= olddata[x].pantsCustomPocketsNotes;
					suit.pantsCustomPocketsImage	= olddata[x].pantsCustomPocketsImage;		
					suit.beltLoopStyle      = olddata[x].beltLoopStyle;
			//		suit.PantsAngledBeltLoop = olddata[x].PantsAngledBeltLoop;
					suit.pantCuffs          = olddata[x].pantCuffs;
					suit.PantsBracesProvisions	= olddata[x].PantsBracesProvisions;
					suit.pbackpocketMethod  = olddata[x].pbackpocketMethod;
			//		suit.PantsBackPocketButton = olddata[x].PantsBackPocketButton;
					suit.cuffsStyle         = olddata[x].cuffsStyle;
					suit.pantFit            = olddata[x].pantFit;
					suit.pantPleats         = olddata[x].pantPleats;
					suit.bpantPockets       = olddata[x].bpantPockets;
					suit.pantPockets       = olddata[x].pantPockets;
			        suit.pantPockets1       = olddata[x].pantPockets1;
			        suit.pantPockets2       = olddata[x].pantPockets2;
			        suit.PantFit      		= olddata[x].PantFit;
					if(olddata[x].custom){
						suit.custom      		= olddata[x].custom;
					}
					else{
						suit.custom     =    new CustomProperties();
					}
					suit.SuitCustomerFabric = olddata[x].SuitCustomerFabric;;
					suit.SuitHasCustomerFabric = olddata[x].SuitHasCustomerFabric;;
					suit.GarmentPrice = olddata[x].GarmentPrice;;
			
					suit.urgent = olddata[x].urgent;
					//suit.urgentDate = olddata[x].urgentDate;
					suit.DOP_day = olddata[x].DOP_day;
					suit.DOP_month = olddata[x].DOP_month;
					suit.DOP_date = olddata[x].DOP_date;
			
					iSuit.suit.push(suit);
					
				}else{
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var suit = {};
					suit.variantId        = x;
					suit.timestampID = new Date().getTime();
					
					suit.checkVisibility	= true;
					suit.ExtraPants = 0;	//false
					suit.ExtraJacket = 0;	//false
					suit.SuitNotes	= '';
					suit.JacketFabricNotes			= '';
					suit.JacketStructureNotes	= '';
					suit.JacketBottomNotes			= '';
					suit.JacketVentNotes			= '';
					suit.JacketLapelStyleNotes		= '';
					suit.JacketButtonsNotes	= '';
					suit.JacketPocketsNotes	= '';
					suit.JacketLiningColoursNotes	= '';
					suit.JacketMonogramNotes		= '';
					suit.PantFabricNotes		= '';
					suit.PantStyleNotes			= '';
					suit.PantPleatsNotes		= '';
					suit.PantFrontPocketsNotes	= '';
					suit.PantBackPocketsNotes	= '';
					suit.PantBeltAndCuffsNotes	= '';
					
					suit.suitJacketCustomStructureNotes	= '';
					suit.suitJacketCustomStructureImage	= '';
					suit.suitJacketCustomVentNotes		= '';
					suit.suitJacketCustomVentImage		= '';
					suit.suitJacketCustomLapelNotes		= '';
					suit.suitJacketCustomLapelImage		= '';
					suit.suitJacketCustomBottomNotes		= '';
					suit.suitJacketCustomBottomImage		= ''; 
					
					suit.suitJacketMonogram        = '';				// 9a
					suit.suitJacketMonogramExtraLine        = '';	
					suit.suitJacketDefaultMonogramHeader  = true;		// 9b
					suit.suitJacketCustomMonogramHeader  = '';	// 9b
					suit.suitJacketMonogramStitchDifferentFromPiping = false;	// 9d
					suit.suitJacketMonogramStitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';		// 9d
					
					suit.suitJacketCustomMonogram			= false;	// 9 optional A
					suit.suitJacketCustomMonogramImage = '';			// 9 optional A
					suit.suitJacketCustomMonogramNotes = '';			// 9 optional A
					
					suit.suitJacketEmbroidery	= false;			// 9 optional B
					suit.suitJacketEmbroideryImage = '';			// 9 optional B
					suit.suitJacketEmbroideryNotes = '';			// 9 optional B
			
					suit.suitFabric 	 	 = df;
					suit.suitJacketBottomStyle	 = dsRegistry.getDatasource('suitBottomStyleDS').getStore()[0];
					suit.suitJacketVentStyle   	 = dsRegistry.getDatasource('suitVentStyleDS').getStore()[0];
			        suit.suitJacketStructure = dsRegistry.getDatasource('suitJacketStructureDS').getStore()[0];	
			        
					suit.suitJacketLapelStyle  	 = dsRegistry.getDatasource('suitLapelStyleDS').getStore()[0];
					suit.suitJacketBreastPocketStyle = dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore()[1]; //"Standard Pocket";
					suit.suitJacketTicketPocketsDifferent = false;
			//		suit.suitJacketTicketPocketStyle = dsRegistry.getDatasource('tickPocketStyleDS').getStore()[0];
					suit.suitJacketLapelWidth   	 = 0;
			//		suit.suitJacketLapelSatin  	 = false;
					suit.suitJacketPocketStyle  	 = dsRegistry.getDatasource('suitPocketStyleDS').getStore()[0];
					suit.suitJacketSleeveButtonNumber	= dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore()[3];
					suit.suitJacketSleeveButtonType	= 'Working';
					suit.suitJacketTopStitch         = false;
					suit.suitJacketTopStitchPocket   = false; 
					suit.suitJacketTopStitchLapel   = false;
				//	suit.suitJacketTopStitchDarts   = false;
				//	suit.suitJacketTopStitchAroundBottom   = false;
				//	suit.suitJacketTopStitchContrast = false;
					suit.suitJacketTopStitchCode      = dsRegistry.getDatasource('pipingColorDS').getStore()[0];	
			    //    suit.suitJacketBreastPocket        = true;
			        suit.suitJacketKissingButtons      = false;
			        suit.suitJacketButtonholeOnLapel      = true;
			    //    suit.suitJacketButtonholeOnLapelContrastCheck      = false;		// REMOVED
			        suit.suitJacketButtonholeOnLapelColor = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			        suit.suitJacketBreastPocketAngledStyle  = 'Straight';
			        suit.suitJacketLapelButtonHole	 = true;
			        suit.suitJacketTicketPocket		 = false;
				    suit.suitJacketContrastLapelUpper  = false;
			        suit.suitJacketContrastLapelLower  = false;
			        suit.suitJacketContrastPockets     = false; 
			        suit.suitJacketContrastCheckPocket = false;
			        suit.suitJacketCollarAndLapelTrimming	 = false;
			        suit.suitJacketContrastTrimming	 = false;
			        suit.suitJacketCollarTrimming		 = false;
			        suit.suitJacketLapelTrimming	 	= false;
			        suit.suitJacketCoverButtons	 	= false;
			        suit.suitJacketPocketTrimming	 	= false;
			        suit.suitJacketBreastPocketTrimming	 	= false;
			        suit.suitJacketDesignerVent		= false;
			        suit.suitJacketTail		= false;     			
			  //      suit.suitJacketCustomVent				= false;   			
			        suit.suitJacketDinner				= false;   			
					suit.suitJacketTopStitchPiping   	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketPiping            	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketInsidePocketsColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0]; // ='';
					suit.suitJacketLining              = dsRegistry.getDatasource('liningFabricDS').getStore()[0];
					suit.suitJacketButtonColor         = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					suit.suitJacketThreadColor         = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2];
					
					suit.suitJacketInsidePocketsDifferentFromPiping = false;
					
			//		suit.suitJacketLiningCode = dsRegistry.getDatasource('liningFabricDS').getStore()[0];//'';
					suit.suitJacketContrastTrimmingColour = '';//dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			//		suit.suitJacketPipingCode = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';
			//		suit.suitJacketButtonContrast	= false;			// 9 optional B
					suit.suitJacketSleeveButton1Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					suit.suitJacketSleeveButton2Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					suit.suitJacketSleeveButton3Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					suit.suitJacketSleeveButton4Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					suit.suitJacketSleeveButton5Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					
					suit.suitJacketSleeveButtonHole1Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButtonHole2Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButtonHole3Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButtonHole4Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButtonHole5Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					
					suit.suitJacketSleeveButton1StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButton2StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButton3StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButton4StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.suitJacketSleeveButton5StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					suit.PantContrastFabric = df;
					suit.pantsCustomPocketsNotes	= '';
					suit.pantsCustomPocketsImage	= '';		
					suit.beltLoopStyle      = dsRegistry.getDatasource('beltLoopStyleDS').getStore()[0];
			//		suit.PantsAngledBeltLoop = false;
					suit.pantCuffs          = false;
					suit.PantsBracesProvisions         = false;
					suit.pbackpocketMethod  = "Both";
			//		suit.PantsBackPocketButton = true;
					suit.cuffsStyle         = dsRegistry.getDatasource('cuffsStyleDS').getStore()[0];
					suit.pantFit            = dsRegistry.getDatasource('pantFitDS').getStore()[0];
					suit.pantPleats         = dsRegistry.getDatasource('pantPleatsDS').getStore()[0];
					suit.bpantPockets      = dsRegistry.getDatasource('bpantPocketsDS').getStore()[0];
					suit.pantPockets       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
			        suit.pantPockets1       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
			        suit.pantPockets2       = dsRegistry.getDatasource('pantPocketsDS').getStore()[0];
			        suit.PantFit      		= 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2]
					suit.custom     =    new CustomProperties();
					suit.urgent = 0;
					//suit.urgentDate = '';
					suit.DOP_day = '';
					suit.DOP_month = '';
					suit.DOP_date = '';
					suit.custom     =    new CustomProperties();
					suit.SuitCustomerFabric = {};
					suit.SuitHasCustomerFabric = false;
					suit.GarmentPrice = 0;
					iSuit.suit.push(suit);
			
				}
			}	
			this._super(name, iSuit, dsRegistry);
		}	
		
	}
});


//END DEFINE CLOSURE
});
