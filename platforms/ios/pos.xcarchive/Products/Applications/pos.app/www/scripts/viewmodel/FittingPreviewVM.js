define(['jquery', 'knockout', 'base'], function($, ko) {
    FittingPreviewVM = SimpleControl.extend({

        init: function() {
            var that = this;
            this.images = [];
            this.videos = [];
            this.selectedImage = ko.observable();

            this.setFittingInfo = function(data) {
                console.log(data)
                that.selectedImage(null);
                for (var i in data.images) {
                    var is_sync = data.images[i].is_sync;
                    if (is_sync == '1') {
                        data.images[i].path = data.images[i].image_local_path;
                    } else {
                        data.images[i].path = data.images[i].fitting_image_path;
                    }
                    data.images[i].tags = JSON.parse(data.images[i].fitting_image_tags);
                }
                for (var i in data.videos) {
                    data.videos[i].path = data.videos[i].video_path;
                    data.videos[i].tags = JSON.parse(data.videos[i].video_notes);
                }
                that.images = data.images;
                that.videos = data.videos;
                that.Customer = data.customer;
                that.DeliveryDate = data.tailor.delivery_date;
                that.TailorNotes = data.tailor.tailor_notes;
                that.Garment = data.garment;
                that.Barcode = data.barcode;
                that.Fabric = data.fabric;
                that.Tailor = data.tailor.tailor_name;

                //TODO missing in data
                // that.orderDate = data.orderDate;
                // that.orderID = data.orderID;
                that.orderDate = "temporary content";
                that.orderID = "temporary content";


            };

        },

        loadInfo: function(ID) {
            data = {
                fitting_id: ID
            };
            DataRequest(true, 'client_fittings/get_fitting', data, this.setFittingInfo, function() { customAlert('Could not load the list. Please try again later or contact the admins!'); });
        },

        loadPage: function(callback) {
            this.callback = callback;
            posChangePage("#fittingView");
        },

        backToLastStep: function() {
            if (this.callback)
                this.callback.loadPage();
        },


        selectImage: function(image) {
            this.selectedImage(image);
        },
    });
});