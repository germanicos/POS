define(['jquery', 'knockout', 'base'], function ($, ko) {
	AlterationsListVM = SimpleControl.extend({
		init: function () {
			var that = this;
			// callback can be a fittingPreview or an alterationReport object
			this.callback = "";
			this.mode = "";
			this.selectedGarment = ko.observable(false);
			this.customerList = ko.observableArray();
			this.customerGarments = ko.observableArray();
			this.endpoint = "client_alterations_new/alterations_endpoint2";
			this.setCustomersInfo = function (data) {
				that.customerList.removeAll();
				for (var x in data) {
					that.customerList.push(data[x])
				}
			};
			this.activeClient = ko.observable(null);
		},

		/**
		 * Shows customer's garments and set client as 'active'
		 * @param customer a knockout object that contains orders and client information
		 */
		setCustomerGarments: function (customer) {
			this.customerGarments.removeAll();
			for (var x in customer.orders) {
				this.customerGarments.push(customer.orders[x]);
			}
			this.selectedGarment(true);

			// set selected client as active
			this.activeClient(customer.customer);


		},

		redirectPage: function (data) {

			// Isso nao se faz, use uma string mais clara (nao use numeros nesses casos)
			if (this.mode == 1) {
				id = data.fitting_id;
			} else {
				id = data.alteration_id;
			}
			this.callback.loadInfo(id);
			this.callback.loadPage(this);
		},

		loadInfo: function () {
			DataRequest(true, this.endpoint, null, this.setCustomersInfo, function () { customAlert('Could not load the list. Please try again later or contact the admins!'); });
		},

		loadPage: function () {
			posChangePage("#alterationsList");
		}
	});

});