{
	store: [
		{
			title: "FabricA", 
			fabricImage: "gfx/fabrics/fabric1.jpg", 
			price: 50,
		},
		{
			title: "FabricB", 
			fabricImage: "gfx/fabrics/fabric2.jpg", 
			price: 100,
		},
		{
			title: "FabricC", 
			fabricImage: "gfx/fabrics/fabric3.jpg", 
			price: 150,
		},
		{
			title: "FabricD", 
			fabricImage: "gfx/fabrics/fabric4.jpg", 
			price: 200,
		},
		{
			title: "FabricE", 
			fabricImage: "gfx/fabrics/fabric5.jpg", 
			price: 250,
		},
	],
	timestamp: 0
}