define(['jquery', 'knockout', 'base'], function($, ko) {
    
    UniformOrderGarmentsStepVM = SimpleControl.extend({

    	init: function(uniformVM, templates) {

            var self = this;

            this.uniformOrderVM = uniformVM;
            console.log("Init UniformOrderGarmentsStepVM...");

            this.templates = ko.observableArray(this.buildTemplateObject(templates));
        },

        /**
         * TODO : filter only the necessary data
         * @return {[type]} [description]
         */
        getSubmissionData: function () {

            var data = {};

            // get fit data
            var templatesData = [];
            templatesData = ko.toJS(this.templates);

            // remove unnecessary data
            templatesData.map(el => {

                Object.keys(el).forEach(function (itm) {
                    if (!["id",
                        "garment_type",
                        "garment_type_name",
                        "instances",
                        "logo_img",
                        "name",
                        "notes",
                        "price"
                    ].includes(itm)) {
                        delete el[itm];
                    } // endif

                });

                // remove unnecessary data of 'instance'
                el.instances.map((instance) => {
                    Object.keys(instance).forEach(function (instance_key) {
                        if (!["selectedFit",
                            "image3D"
                        ].includes(instance_key)) {
                            delete instance[instance_key];
                        } // endif
                    })

                    // remove unnecessary data of 'image3D'
                    if (instance.image3D){
                        Object.keys(instance.image3D).forEach(function (image3D_key) {
                            if (![
                                // to shirt
                                "shirtMonogram",
                                "monogramNotes",
                                "shirtSelectedPosition",
                                "shirtSelectedColor",
                                "shirtSelectedStyle",
    
                                // to vest, jacket en suit
                                "monogramLine1",
                                "monogramLine2",
                                "jacketSelectedStyle"
                            ].includes(image3D_key)) {
                                delete instance.image3D[image3D_key];
                            } // endif
                        })
                    }


                });






            });

            // MONOGRAM DATA IS INSIDE IMAGE3D

            data = templatesData;
            return data;
        },

        /**
         * Adds more variables and observables to each template
         */
        buildTemplateObject : function(templates) {

        	for(let template of templates)
        	{
        		// Instances represent one instance of the template
        		// Each instance should have unique attributes, like: FIT, MONOGRAM, ETC
        		template.instances = ko.observableArray([]);
        	}

        	return templates;
        },


        incrementGarment : function(template) {

        	console.log("Adding template instance...");
        	const garment = this.uniformOrderVM.customerDetailsStep.getGarmentObject(template);
        	template.instances.push(garment);

        },

        decrementGarment : function(template) {

        	if(template.instances().length > 0)
        	{
        	    template.instances.pop(); // removes last element
        	}
        	else
        	{
        	    console.log("Cannot go below 0");
        	}

        },

        /**
         * Get a design json and return a user friendly array [{option : "" , value : ""}]
         * @param {Object} TranslatedDesign 
         * @returns {Array}
         */
        getBackwardsDesignList : function(design){
            var response = [];
            for (const optionObject of design) {
                const name = optionObject.option.name;
                var value;

                if (name.toLowerCase().trim() == "fabric"){
                    response.push({'name':name, 'value':optionObject.value.code});
                    continue;

                }

                if (typeof(optionObject.value) === "object" && (optionObject.value != null) ){
                    value = optionObject.value.name;
                }else {
                    value = optionObject.value;
                }

                response.push({'name':name, 'value':value});
            }

            return response;
        }


    });

});