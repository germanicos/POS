define(['jquery', 'knockout', 'base'], function ($, ko) {

    /**
     * 2nd step for uniform order
     * This class will be responsible for the creation of a new template (default design)
     * of an existing company
     *     For example: Template: 'Mercedes Black Salesman', 'Victoria Fire Brigade Shirt'
     */
    UniformTemplateVM = SimpleControl.extend({

        init: function (companyId) {

            var self = this;

            console.log("Init UniformTemplateVM...");

            this.companyId = companyId;
            this.steps = this.buildSteps();
            this.selectedStep = ko.observable(this.steps[0]);

            /* start Steps definitions */
            this.garmentsStep = new UniformGarmentsStepVM(this);
            this.garmentsDesignStep = null; // get from the server
            this.garmentsPortfolioStep = new UniformPortfolioVM(this);
            
            this.finalizeStep = new UniformFinalizeStepVM(this); // get from the server
            /* end Step definition */

            this.company = ko.observable(false);

            this.selectedGarmentType = ko.observable(false);
            this.selectedGarment = ko.observable(false);

            // Contains all the fabrics
            this.fabricsData = fabricsData.store;

            // Should be in the last line
            this.getUniformCompanyData(companyId);

            this.kissingButtonsExtraCost = ko.observable('30');
        },


        getUniformCompanyData: function (companyId) {

            console.log("Fetching data from server...");
            var self = this;

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/prepare_uniform_template_process',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "company_id": companyId
                },
                success: function (dataS) {


                    console.log(dataS);

                    self.company(dataS.company);
                    self.garmentsDesignStep = new UniformGarmentsDesignStepVM(self, dataS.garments_design, dataS.logo_positon_shapes);

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: false
            });


        },

        buildSteps: function () {

            return [
                {
                    'name': 'Select Garments',
                    'id': '1',
                    'complete': false
                },
                {
                    'name': 'Design Template',
                    'id': '2',
                    'complete': false
                },
                {
                    'name': 'Portfolio',
                    'id': '3',
                    'complete': false
                },
                {
                    'name': 'Finalize',
                    'id': '4',
                    'complete': false
                }
            ];
        },


        stepBack: function () {

            console.log("Steping back...");

            const currentStep = this.selectedStep();
            const currentStepId = parseInt(currentStep.id);

            if (currentStepId <= 1) {
                posChangePage("#main");
                return;
            }

            this.changeStep(currentStepId - 1);
        },


        nextStep: function () {

            const currentStep = this.selectedStep();
            const currentStepId = parseInt(currentStep.id);

            if (currentStepId >= 4) { alert('You are in the last step'); return; }

            /**
             * If step equals to 3, checks if the POS has sent the 
             * base64 images to the server, and proceed only if it has
             */
            if( currentStepId == 3)
            {
                if(!this.garmentsPortfolioStep.hasSentBase64Images())
                {
                    customAlert("Please, send the template images to the server first ! (Use SEND SELECTED GARMENTS TO CUSTOMER BTN) ");
                    return;
                }
            }


            // DESIGN TEMPLATE
            if (currentStepId == 2)
            {
                this.nextBtnDesignStep(currentStepId);
            }
            else
            {
                var nextStep = this.steps.filter(e => e.id == currentStepId + 1)[0];
                
                console.log('Next BTN...', nextStep);

                this.changeStep(nextStep.id);
            }


            // Back to top except in design
            if (currentStepId != '2') {
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            }

        },

        /**
         * Special case: in design template step.
         * 
         * before change step, go through categories of the garments
         * @param {Number} currentStepId 
         */
        nextBtnDesignStep: function (currentStepId) {
            // set current step as 'visited'
            var selectedGarment = this.garmentsDesignStep.selectedGarment();
            var selectedCategory = this.garmentsDesignStep.selectedCategory()
            
            // avoid error
            if (selectedGarment == "") {
                // $.jGrowl(`please, select a garment`);

                // go to next step:
                this.changeStep(String(currentStepId+1));
                return;
            }
            if (selectedCategory == "") {
                $.jGrowl(`please, select a category`);
                return;
            }

            try {
                selectedGarment.categories.filter(function(el){ return el.id == selectedCategory.id})[0].visited(true);
            } catch (TypeError) {
                console.log("error in filter in GarmentDesignStepVM.js");
                $.jGrowl("Please, select a category");
            }

            // for usability purposes search for unvisited steps in current garment first:
            for (const category_index in selectedGarment.categories) {
                let category = selectedGarment.categories[category_index];

                // check for unvisited and incomplete category
                if (category.visited() == false || category.completed() == false) {
                    if (category.visited()){
                        $.jGrowl(`please, check ${selectedGarment.garment_name}'s ${category.name}`);
                    }

                    // change category
                    this.garmentsDesignStep.selectCategory(category);
                    
                    // set new category as visited
                    category.visited(true);
                    return;
                }


                // go to next step (deprecated)
                if (false && category == this.garmentsDesignStep.selectedCategory()) {

                    // change garment
                    this.selectGarment(selectedGarment);

                    // change category
                    let nextCategory = selectedGarment.categories[parseInt(category_index)+1];
                    if (nextCategory){

                        this.garmentsDesignStep.selectCategory(nextCategory);
                    }
                    else {
                        break;
                    }
                    
                    // set new category as visited
                    category.visited(true);
                    return;
                }
            }

            // if we're in the last category, open LOGO popup
            if (selectedCategory == selectedGarment.categories[selectedGarment.categories.length - 1]){
                this.garmentsDesignStep.showLogoSection();

                // change to first category
                this.garmentsDesignStep.selectCategory(selectedGarment.categories[0]);
                return;
            }


            // now we can search in other garments
            for (const garmentType of this.garmentsStep.garments()) {
                for (const garment of garmentType.garments()) {
                    for (const category of garment.categories) {

                        // NO NEED TO CHECK FABRIC ANYMORE


                        if (category.visited() == false) {
                            // $.jGrowl(`please, finish ${garment.garment_name}'s ${category.name} for ${customer.full_name}`);

                            // change garment
                            this.selectGarmentType(garmentType);
                            this.selectGarment(garment);

                            // change category
                            this.garmentsDesignStep.selectCategory(category);

                            // set new category as visited
                            category.visited(true);
                            return;
                        }
                    }
                }


            }
           

            // go to next step
            // const nextStep = this.steps.filter( e => e.id == String(currentStepId+1) )[0];
            // this.changeStep(nextStep.id);
            this.changeStep(String(currentStepId+1));

        },

        changeStep: function (stepId) {

            var self = this;

            $('#loading_jp').show( () => {
            
                console.log("changing step: ", stepId);

                // Get first element whose id equals to stepId
                const newStep = this.steps.filter(e => e.id == stepId)[0];

                console.log("new step", newStep);

                // this.selectedCustomer().selectedStep(newStep);
                this.selectedStep(newStep);

           
                // Ui sugar
                $(".order-step:visible").effect("slide", { direction: 'right' }, 500);

                document.getElementById('loading_jp').style.display = "none";

                switch(stepId)
                {
                    case '2':
                        
                        this.garmentsDesignStep.autoSelectGarmentType();

                    break;

                    case '3':

                        // set "front" as default view in portfolio
                        uniformTemplate.garmentsPortfolioStep.change3DView();
                        
                        if( !this.garmentsPortfolioStep.hasSentBase64Images() )
                        {
                            // waits 3 seconds for the page to load
                            setTimeout(self.garmentsPortfolioStep.autoSendBase64images, 3000);
                        }

                    break;

                    case '4':

                        if( !this.finalizeStep.canvasBase64() ) // if there is no signature yet...
                        {
                            this.finalizeStep.prepareCanvas(); // prepare the canvas to sign
                        }
                    break;
                }

                document.getElementById('loading_jp').style.display = "none";
            }); // end loader

        },


        submitTemplate: function () {

            // Replace with notification later
            if( !confirm('Do you really want to submit the template ? ') )
            {
                return;
            }

            const submissionData = {
                'company_id': this.company().company_id,
                'garments': this.garmentsDesignStep.getSubmissionData(),
            }

            const device_id = device.uuid;
            
            $('#loading_jp').show( () => { 

                $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/submit_uniform_template',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "submission_data": JSON.stringify(submissionData),
                    "device_id" : device_id
                },
                success: function (dataS) {

                    console.log(dataS);

                    if( dataS.result == 'success' )
                    {
                        $.jGrowl("Template successfully created !`");
                        posChangePage("#main");
                    }
                    else
                    {
                        customAlert("Something wrong happned : ( please try again...");
                        customAlert(JSON.stringify(dataS));
                    }

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: false
                });

            });
        },


        selectGarmentType: function (garmentType) {

            if (this.selectedGarmentType() == garmentType){
                return;
            }

            console.log("Selecting garment type", garmentType);

            $('#loading_jp').show( () => { 
                
                this.selectedGarmentType(garmentType);

                // select first garment of this type if exists
                if (garmentType.garments().length) 
                {
                    this.selectedGarment(garmentType.garments()[0]);

                    // init first category of first garment if is empty
                    if (!this.garmentsDesignStep.selectedCategory()) {
                        this.garmentsDesignStep.selectCategory(this.selectedGarment().categories[0]);
                    }
                }
                else 
                {
                    this.selectedGarment(false);
                }

                document.getElementById('loading_jp').style.display = "none";
            });



        },

        /**
         * Returns the index of a garment in its type
         * @return {[type]} [description]
         */
        getGarmentIndex: function (garment) {

            const garmentType = this.garmentsStep.garments().filter(garmentType => garmentType.id == garment.id)[0];

            if (garmentType.garments().length > 0) {
                return garmentType.garments().indexOf(garment);
            }
            else {
                return false;
            }
        },


        selectGarment: function (garment) {

            if (this.selectedGarment() == garment){
                return;
            }

            console.log("Selecting garment", garment);
            // This is necessary to show the loader while knockout notifies all observables
            $('#loading_jp').show( () => { 
               this.selectedGarment(garment);
                
               // init first category of first garment if is empty
                if (!this.garmentsDesignStep.selectedCategory()) {
                    this.garmentsDesignStep.selectCategory(this.selectedGarment().categories[0]);
                }

                
                document.getElementById('loading_jp').style.display = "none";
            });

        },

        exit: function () {

            if (!confirm("Do you really want to cancel this order ?")) { return; }

            posChangePage('#main');
            $.jGrowl('Order cancelled');

        },


        selectGarmentFabric : function(fabric, garment, alertStockLevel = true) {
            console.log("<<<<<<<<<<<<<< Selecting fabric" + fabric.title + " for garment: ", garment);
            garment.selectedFabric(fabric);
            garment.fabricInsearch(fabric.title);
            garment.fabricsInList([]);

            // Unset customer fabrics...
            garment.useCustomerFabric(false);
            garment.customerFabricImage(false);
            garment.customerFabricMeterage(false);

            const fabric_category = garment.categories.filter(function (el) { return el.name.toLowerCase() == 'fabric'})[0];
            const options = fabric_category.options.filter(function(el){return el.name.toLowerCase() == 'fabric'})[0];
            options.selectedValue(fabric.id);

            if( alertStockLevel && fabric.in_stock == '0' )
            {
                navigator.notification.alert("WARNING: THIS FABRIC IS CURRENTLY OUT OF STOCK !!");
            }
        },


        /**
         * Generates a base64 image from a html element.
         *
         *  
         * @param  {[type]} $uiElement [ Should be a UI element from the jquery plugin 
         *                   example:
         *                       const $UiEL = $("#myId");
         * 
         *                       generateBase64Img($UiEL);
         * 
         *     ]
         */
        generateBase64Img : function($uiElement) {

            html2canvas($uiElement, {
                            onrendered: function(canvas) {
                                theCanvas = canvas;
                    
                                let datauri = theCanvas.toDataURL("image/png");;
                                console.log("datauri", datauri);
                                
                        

                            }
                        });

        }         








    });


});