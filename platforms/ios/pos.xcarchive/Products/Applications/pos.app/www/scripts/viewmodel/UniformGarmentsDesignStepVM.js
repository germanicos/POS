define(['jquery', 'knockout', 'base'], function ($, ko) {

    UniformGarmentsDesignStepVM = SimpleControl.extend({

        init: function (uniformTemplateVM, designs, logoPositionShapes) {
            var self = this;

            console.log("Init Garment Design Step VM");
            this.uniformTemplateVM = uniformTemplateVM;
            this.logoPositionShapes = logoPositionShapes;

            // Design tree from server
            this.designs = designs;

            this.selectedGarment = this.uniformTemplateVM.selectedGarment;
            this.selectedCategory = ko.observable(false);

            this.hasCustomDesign = this.setHasCustomDesign();
        },

        getSubmissionData: function () {

            const designData = ko.mapping.toJS(this.uniformTemplateVM.garmentsStep.garments());

            for (const garments_type of designData) {

                for (const garment of garments_type.garments) {

                    // remove unnecessary data
                    Object.keys(garment).forEach(function (itm) {
                        if (!["categories",
                            "garment_id",
                            "garment_name",
                            "selectedFabric",
                            "unique_id",
                            "templateName",
                            "price",
                            "portifolioNotes",
                            "logoData",
                            "base64",
                            "base64_back"
                        ].includes(itm)) {
                            delete garment[itm];
                        } // endif

                    });

                    // remove unnecessary data
                    Object.keys(garment.selectedFabric).forEach(function (itm) {
                        if (!["id", "title"].includes(itm)) {
                            delete garment.selectedFabric[itm];
                        }
                    });

                    for (const category of garment.categories) {

                        // remove unnecessary data
                        Object.keys(category).forEach(function (itm) {
                            if (!["id", "name", "garment_id", "options"].includes(itm)) {
                                delete category[itm];
                            }
                        });

                        for (const option of category.options) {

                            // remove unnecessary data
                            Object.keys(option).forEach(function (itm) {
                                if (!["cat_id", "id", "selectedValue", "name"].includes(itm)) {
                                    delete option[itm];
                                }
                            });

                        }
                    }
                }
            }

            return designData;
        },

        /**
         * Returns an empty logo data object
         *     image : the image to be used in the logo (can only choose between logo or text)
         *     text : the text to be used as the logo (can only choose between logo or text)
         *     textColour : color of the stich if user selected text
         *     logoImageShapes : from ERP => uniform_companies_logo_shapes
         */
        getLogoObject: function (garmentId) {

            // clone the originals
            const possibleShapes = JSON.parse(JSON.stringify(this.logoPositionShapes.shapes));

            //  adds the selected property as an observable
            for (let shape of possibleShapes) {
                shape.selected = ko.observable(false);
            }

            const logoData = {
                "image": ko.observable(false), // temporary placeholder
                "text": ko.observable(""),
                "textColor": ko.observable(""),
                "logoImageShapes": ko.observableArray(possibleShapes),
                "currentView": ko.observable('front'), // this can be front / back
                "positions": ko.observableArray(this.getPositionsArray(garmentId)),
                "notes" : ko.observable('')
            };

            return logoData;
        },


        /**
         * Changes the view of the current logo 3d 
         *     If current is front, set back and vice versa
         * @return {[type]} [description]
         */
        changeCurrentView: function () {

            console.log("changing view...");

            const currentView = this.uniformTemplateVM.selectedGarment().logoData.currentView();

            if (currentView === 'front') {
                this.uniformTemplateVM.selectedGarment().logoData.currentView('back');
                this.uniformTemplateVM.selectedGarment().image3D.side('back');
                // TODO: suit. Suit can be [jacket_front, jacket_back, pant_front, pant_back]
            }
            else {
                this.uniformTemplateVM.selectedGarment().logoData.currentView('front');
                this.uniformTemplateVM.selectedGarment().image3D.side('front');
                // TODO: suit. Suit can be [jacket_front, jacket_back, pant_front, pant_back]
            }

        },

        /**
         * Returns the positions for each type of garment
         * @param  {[type]} garment_id [1, 2, 3, 4]
         */
        getPositionsArray: function (garmentId) {

            if (this.logoPositionShapes.positions[garmentId] == undefined) {
                console.log("%c THERE IS NO POSITIONS FOR THIS GARMENT YET, PUT IN DB", 'color: white;background red;');
                return [];
            }

            const positions = JSON.parse(JSON.stringify(this.logoPositionShapes.positions[garmentId]));

            //  adds properties as observables
            for (let position of positions) {
                position.selected = ko.observable(false);
                position.width_3d = ko.observable(13); // in % to show in 3d (this can come from the database)
                position.width = ko.observable(10); // in cm or inches, to show in form
                position.height = ko.observable(10); // in cm or inches, to show in form
                position.rotate = ko.observable(0);
            }

            return positions;
        },

        getGarmentObject(garmentId) {

            var self = this;

            let garment = JSON.parse(JSON.stringify(this.designs[garmentId]));

            // Unique ID to identify each garment during the order process
            garment.unique_id = new Date().valueOf();

            console.log(this.uniformTemplateVM.garmentsStep.garments().find(type => type.id == garmentId));



            // Name of the template => ex. 'Mercedez Black'
            const garmentType = this.uniformTemplateVM.garmentsStep.garments().find(type => type.id == garmentId);
            const defaultGarmentName = `${garmentType.name} #${garmentType.garments().length + 1}`;
            garment.templateName = ko.observable(defaultGarmentName);



            // Price of the garment -> selected by the salesman
            garment.price = ko.observable(0.0);

            // Notes added in portifolio step
            garment.portifolioNotes = ko.observable("");

            garment.selectedFabric = ko.observable("");

            // Logo data for the garment
            garment.logoData = this.getLogoObject(garmentId);

            // not slow
            for (let category of garment.categories) {
                // Boolean if this is selected or not in the UI
                category.selected = ko.observable(false);

                for (let option of category.options) {
                    option.selected = ko.observable(false);

                    // setting default values
                    // if option is a boolean
                    if (option.type_id == '4') {
                        option.selectedValue = ko.observable(false);
                    }
                    // option is number (int or float)
                    else if (['1', '2'].includes(option.type_id)) {
                        option.selectedValue = ko.observable(0);
                    }
                    // option is string
                    else if (option.type_id == '3') {
                        option.selectedValue = ko.observable('');
                    }
                     // color 
                     else if (['5','8','9'].includes(option.type_id)){
                        
                            
                        // colors no long stay in design_values table.
                        // get from liningFabricDS, pipingColorDS and buttonholethreadDS !!!!
                        
                        
                        // if liningFabric
                        if (option.type_id == '8'){
                            option.values = [];
                            let print_order = 0;

                            for (const lining of liningFabricDS.store) {
                                option.values.push({
                                    id          : lining.id,
                                    image       : lining.image,
                                    name        : lining.title,
                                    opt_id      : option.id,
                                    print_order : print_order,
                                    thai_name   : "",
                                    preview     : lining.preview,
                                    previews    : lining.previews,
                                    price       : lining.price
                                });
                                
                                print_order += 1;
                            }
                        }

                        // if piping
                        else if (option.type_id == '5'){

                            option.values = [];
                            let print_order = 0;

                            for (const piping of pipingColorDS.store) {
                                option.values.push({
                                    id          : piping.id,
                                    image       : piping.image,
                                    inpocket    :piping.inpocket,
                                    lapel1      : piping.lapel1,
                                    name        : piping.title,
                                    opt_id      : option.id,
                                    preview     : piping.preview,
                                    preview1    : piping.preview1,
                                    preview2    : piping.preview2,
                                    preview3    : piping.preview3,
                                    preview4    : piping.preview4,
                                    preview5    : piping.preview5,
                                    print_order : print_order,
                                    thai_name   : "",
                                    tpreview1   : piping.tpreview1,
                                    tpreview2   : piping.tpreview2,
                                    tpreview3   : piping.tpreview3,
                                    tpreview4   : piping.tpreview4,
                                    tpreview5   : piping.tpreview5
                                });
                                
                                print_order += 1;
                            }
                        }

                        // if button
                        else if (option.type_id == '9'){

                            // images on ERP are crashed
                            
                            // option.values = [];
                            // let print_order = 0;

                            // for (const button of buttonholethreadDS.store) {
                            //     option.values.push({
                            //         id          : button.id,
                            //         image       : button.image,
                            //         name        : button.title,
                            //         opt_id      : option.id,
                            //         print_order : print_order,
                            //         thai_name : ""
                            //     });
                                
                            //     print_order += 1;
                            // }
                        }

                        else {
                            console.log("unknown type");
                            option.selectedValue = ko.observable(null);
                            continue;
                        }

                        // get first value by print_order and set as selected.
                        // ERP already returns an sorted array
                        let first = option.values[0];
                        option.selectedValue = ko.observable(first);
                        
                        option.values[0].selected = ko.observable(true);
                        option.values[0].clicked = ko.observable(true); // open details options

                        for (let valueIndex = 1; valueIndex < option.values.length; valueIndex++) {
                            option.values[valueIndex].selected = ko.observable(false);
                            option.values[valueIndex].clicked = ko.observable(false); // open details options
                        }

                        
                    }

                    // radio button
                    else if (option.type_id == '6') {

                        option.selectedValue = ko.observable(null);

                        if (option.values) {
                            for (let valueIndex = 0; valueIndex < option.values.length; valueIndex++) {
                                option.values[valueIndex].selected = ko.observable(false);
                                option.values[valueIndex].clicked = ko.observable(false); // open details options
                            }
                        }

                    }

                    // unknown
                    else {
                        option.selectedValue = ko.observable(null);
                    }
                }

                // Ignore 'monogram' for shirt
                // Ignore 'fit' of all garments too
                if (category.id == '39' || ['30','21','13','3'].includes(category.id)){
                    category.visited = ko.observable(true); // to the next button functionality
                    category.completed = ko.observable(true);

                } else {
                    
                    // Condition for category to be COMPLETED OR NOT
                    // each category has its own particularity
                    category.visited = ko.observable(false); // to the next button functionality
                    category.completed = this.conditionToBeComplete(category);
                }

            }

            garment.image3D = new Image3DVMUniform(garment);

            // Sets the observables required to select the fabric
            garment.fabricInsearch = ko.observable(""); // What the salesman has typed in the fabric search
            garment.fabricsInList = ko.observableArray([]); // fabric list for the salesman to choose one
            garment.selectedFabric = ko.observable(false);

            garment.fabricInsearch.subscribe(function (searchString) { // filter the shirt/not shirt fabrics in the list

                //console.log("searching ", searchString);

                garment.fabricsInList.removeAll();

                //console.log("fabrics", orders.fabricsData);


                if (searchString.length > 2) {
                    for (let fabric of self.uniformTemplateVM.fabricsData) {
                        if (typeof fabric == "undefined") { continue; }

                        // Maybe problem with ipad < 9.3 
                        if (!fabric.title.toLowerCase().trim().includes(searchString.toLowerCase().trim())) {
                            continue;
                        }

                        //console.log("Adding " + fabric.title + " for ", garment);

                        if (garment.garment_name === "Shirt") {
                            if (fabric.for_shirt == "1" || fabric.for_shirt == undefined) {
                                garment.fabricsInList.push(fabric)
                            }
                        }
                        else {
                            if (fabric.for_shirt == "0" || fabric.for_shirt == undefined) {
                                garment.fabricsInList.push(fabric)
                            }
                        }

                    }
                }
            });

            // Customer fabric obseravbles:

            garment.useCustomerFabric = ko.observable(false);
            garment.customerFabricImage = ko.observable(false);
            garment.customerFabricMeterage = ko.observable(false);

            garment.useCustomerFabric.subscribe(function (value) {

                console.log("VALUE", value);

                if (value) // if using customer fabric
                {
                    // sets the garment fabric as CUSTOMERFABRIC (id == 6470)
                    const customFabric = fabricsData.store.filter(el => el.id == '6470')[0];

                    // sets the garment fabric as false
                    garment.selectedFabric(customFabric);
                    garment.fabricsInList([])
                    garment.fabricInsearch("Custom Fabric");

                    // sets the fabric option as CUSTOMERFABRIC (id == 6470)
                    const fabric_category = garment.categories.filter(function (el) { return el.name.toLowerCase() == 'fabric' })[0];
                    const options = fabric_category.options.filter(function (el) { return el.name.toLowerCase() == 'fabric' })[0];
                    options.selectedValue('6470');

                    self.takeCustomerFabricImage(garment.selectedFabric());
                }

            });
            // End observables for fabric


            // Start observables for garment urgency
            garment.isUrgent = ko.observable(false);
            garment.urgentDate = ko.observable(false);
           // end observables for garment urgency

           // pre select values
           this.preSelectValue(garment);

            return garment;
        },

        /**
         * Clear the logo image if logo text is set
         */
        setLogoText: function () {


            console.log("clearing logo img...");
            const selectedGarment = this.uniformTemplateVM.selectedGarment();
            selectedGarment.logoData.image(false);

        },

        /**
         * [setLogoPosition description]
         */
        setLogoPosition: function (logoObject) {

            console.log('toggling ', logoObject);
            logoObject.selected(!logoObject.selected());

        },

        /**
         * Sets the logoObject as assigned for the selected Garment
         * @param {[type]} logoObject [description]
         */
        setLogoImageShape: function (logoObject) {

            console.log("setting logo image shape to selected garment...");
            const selectedGarment = this.uniformTemplateVM.selectedGarment();

            // unselect all 
            for (let shape of selectedGarment.logoData.logoImageShapes()) {
                shape.selected(false);
            }


            // selected the clicked one
            logoObject.selected(true);

        },


        /**
         * Uploads the logo image for the selected garment
         * @return {[type]} [description]
         */
        uploadLogoBtn: function () {

            var self = this;

            console.log("Uploading logo...");

            navigator.notification.confirm(
                'Choose Picture Source', // message
                function (btnIndex) {

                    // const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;

                    takeImg(btnIndex);

                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

            function takeImg(source) {
                navigator.camera.getPicture(onSuccess, onFail,
                    {
                        quality: 10,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: source,
                        encodingType: Camera.EncodingType.JPEG,
                        mediaType: Camera.MediaType.PICTURE,
                        saveToPhotoAlbum: true,
                        correctOrientation: true,
                    });
            }

            function onSuccess(imageURI) {
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError);
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }

            function resolveOnSuccess(entry) {

                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';

                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist

                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                },

                    resOnError);
            };

            function successMove(entry) {
                console.log('After move');

                console.log("entry.toURL()", entry.toURL());
                const selectedGarment = self.uniformTemplateVM.selectedGarment();

                selectedGarment.logoData.image(entry.toURL());
                selectedGarment.logoData.text(""); // clear the text
            };

            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        },


        selectCategory(category) {

            console.log("selecting...", category);

            // unselect all
            for (let cat of this.selectedGarment().categories) {
                cat.selected(false);
            }


            // select just this one
            category.selected(true);
            this.selectedCategory(category);

            // If category is fabric => reinstantiate the UI for the datepicker
            if (category.name.toLowerCase() === 'fabric') {
                // Recreate datepicker object
                $(".datepicker").datepicker();
            }


            this._change3DView(category);
        },

    
         /**
         * In a category selection, this function changes the 3D view.
         * For exemple 1: in suit's monogram category, this method changes de 3d to monogram view
         * 
         * 
         * Another way to do this, is put a data-bind:click in each category tab
         */
        _change3DView(category) {
            // monogram step ID for vest, jacket, suit respectively
            if (['27', '46', '63'].includes(category.id)) {
                // suit
                if (category.id == '63') {
                    this.selectedGarment().image3D.side('jacket_monogram');
                    
                // vest and jacket
                } else {
                    this.selectedGarment().image3D.side('monogram');
                    
                }
            
            // sleeve
            } else if (category.id == '7') {
                // '7' is ID fot jacket Sleeve
                this.selectedGarment().image3D.side('sleeve');
            } else if (category.id == '54') {
                // '54' is ID fot suit Sleeve
                this.selectedGarment().image3D.side('jacket_sleeve');
            }

            // back
            else if (["36", "5", "17", "26", "51", "61"].includes(category.id)) {
                // suit
                if (category.garment_id == "6"){

                    if ("51" == category.id) {
                        this.selectedGarment().image3D.side('jacket_back');
                    } else {
                        this.selectedGarment().image3D.side('pant_back');
                    }
                }

                // others
                else{
                    this.selectedGarment().image3D.side('back');
                }
            }
            
            // shirt contrast
            else if (category.id == "38"){
                this.selectedGarment().image3D.side('contrast');
                
            }

            // front
            else {
                // suit
                if (category.garment_id == '6') {
                    // pants category
                    if (['58', '59', '60', '61', '62', '63', '64', '70'].includes(category.id)){
                        this.selectedGarment().image3D.side('pant_front');
                    }else {
                        this.selectedGarment().image3D.side('jacket_front');
                    }

                // others
                } else {
                    this.selectedGarment().image3D.side('front');
                }
            }

           
        },

        /**
        * This function set a design step as 'visited'. This is only for the next btn functionality.
        * 
        * Called in a step changing
        * 
        * @param selectedCategory
        * @param selectedGarment
        * @param status [boolean]
        */
        setOptionsVisited: function (selectedCategory, selectedGarment, status) {
            console.log("setOptionsVisited");
            selectedGarment.categories.filter(function (el) { return el.id == selectedCategory.id })[0].visited(status);
        },





        /**
         * Takes a custom image for designs
         * @return {[type]} [description]
         */
        takeOptionCustomImage : function(option) {

            self = this;
             
            console.log("Taking Pic...", option);
        
            navigator.notification.confirm(
                'Choose Picture Source', // message
                function(btnIndex) {
        
                    // const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;
                    
                    takeImg(btnIndex);
                     
                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

            function takeImg(source)
            {
                navigator.camera.getPicture(onSuccess, onFail,
                {
                    quality: 10,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: source,
                    encodingType: Camera.EncodingType.JPEG,
                    mediaType: Camera.MediaType.PICTURE,
                    saveToPhotoAlbum: true,
                    correctOrientation : true,
                }); 
            }
        
            function onSuccess(imageURI) { 
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError); 
            }  
        
            function onFail(message) { 
                alert('Failed because: ' + message); 
            } 
        
            function resolveOnSuccess(entry) {
                 
                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';
                 
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist
                     
                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };
        
            function successMove(entry) {
                console.log('After move');
                 
                console.log("entry.toURL()", entry.toURL());
                option.selectedValue(entry.toURL());
            
            };
        
            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        },

        clearCustomOptionImage : function(option) {

            navigator.notification.confirm(
                'Do you really want to delete this image ?', // message
                function(btnIndex) {
        
                    if(btnIndex == 1)
                    {
                        option.selectedValue(false);
                    }
                     
                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['Yes', 'No']         // buttonLabels
            );


        },


        
         /**
         * Return an option list to show contents of the 'custom data' category of each garment type.
         * example: if custom collar is true for shirt ==> show UPLOAD CUSTOM IMAGE for collar in CUSTOM DATA
         */
        setHasCustomDesign :  function(){
            
            var self = this;
            return ko.computed(function(option){
                console.log('self.selectedGarment()', self.selectedGarment());

                switch (self.selectedGarment().garment_id) {
                    // Search for selected custom data in PANTS (front pockets)
                    case '1':

                        // get front pocket value (if is custom front pocket return true)
                        var frontCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '16' })[0];
                        var frontOption = frontCategory.options.filter(function (el) { return el.id == '61' })[0];

                        return frontOption.selectedValue() != null && frontOption.selectedValue().id == '1261' ? ['226'] : [];

                    // Search for selected custom data in SHIRT (front and collar and cuff)
                    case '2':
                        var customToShow = [];
                        
                        var frontCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '34' })[0];
                        var frontOption = frontCategory.options.filter(function (el) { return el.id == '96' })[0];
                        
                        if (frontOption.selectedValue() != null && frontOption.selectedValue().id == '1343'){
                            customToShow.push('229');
                        }

                        var collarCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '33' })[0];
                        var collarOption = collarCategory.options.filter(function (el) { return el.id == '94' })[0];
                        if (collarOption.selectedValue() != null && collarOption.selectedValue().id == '1329'){
                            customToShow.push('249');
                        }

                        var cuffCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '32' })[0];
                        var cuffOption = cuffCategory.options.filter(function (el) { return el.id == '92' })[0];
                        if (cuffOption.selectedValue() != null && cuffOption.selectedValue().id == '4567'){
                            customToShow.push('252');
                        }

                        return customToShow;

                    // Search for selected custom data in VEST (custom button image and lapel image )
                    case '3':
                        var customToShow = [];
                        var buttonCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '24' })[0];
                        var buttonOption = buttonCategory.options.filter(function (el) { return el.id == '78' })[0];

                        if (buttonOption.selectedValue() != null && buttonOption.selectedValue().id == '1298'){
                            customToShow.push('227');
                        }
                        
                        var lapelCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '22' })[0];
                        var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '74' })[0];

                        if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '1291'){
                            customToShow.push('228');
                        }

                        return customToShow;



                    // Search for selected custom data in JACKET (Custom Bottom Image; Custom Design Image; Custom Lapel Image; Custom Vent Image; Custom Monogram Image;)
                    case '4':
                        
                        var customToShow = [];
                        var bottomCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '4' })[0];
                        var bottomOption = bottomCategory.options.filter(function (el) { return el.id == '7' })[0];

                        if (bottomOption.selectedValue() != null && bottomOption.selectedValue().id == '25'){
                            customToShow.push('230');
                        }

                        var designCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '1' })[0];
                        var designOption = designCategory.options.filter(function (el) { return el.id == '1' })[0];

                        if (designOption.selectedValue() != null && designOption.selectedValue().id == '1'){
                            customToShow.push('231');
                        }

                        var lapelCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '6' })[0];
                        var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '12' })[0];

                        if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '31'){
                            customToShow.push('232');
                        }

                        var ventCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '5' })[0];
                        var ventOption = ventCategory.options.filter(function (el) { return el.id == '9' })[0];

                        if (ventOption.selectedValue() != null && ventOption.selectedValue().id == '27'){
                            customToShow.push('233');
                        }


                        var breastpocketCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '9' })[0];
                        var breastpocketOption = breastpocketCategory.options.filter(function (el) { return el.id == '36' })[0];

                        if (breastpocketOption.selectedValue() != null && breastpocketOption.selectedValue().id == '4585' ) {
                            customToShow.push('257');
                        }


                        // var monogramCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '46' })[0];
                        // var monogramOption = monogramCategory.options.filter(function (el) { return el.id == '' })[0];

                        // if (monogramOption.selectedValue() != null && monogramOption.selectedValue().id == ''){
                        //     customToShow.push('244');
                        // }


                        return customToShow;



                    // Search for selected custom data in SUIT (Custom Bottom Image; Custom Design Image; Custom Lapel Image; Custom Vent Image; Custom Monogram Image;front pockets)
                    case '6':
                        var customToShow = [];
                        var bottomCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '50' })[0];
                        var bottomOption = bottomCategory.options.filter(function (el) { return el.id == '159' })[0];

                        if (bottomOption.selectedValue() != null && bottomOption.selectedValue().id == '2607'){
                            customToShow.push('235');
                        }

                        var designCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '49' })[0];
                        var designOption = designCategory.options.filter(function (el) { return el.id == '148' })[0];

                        if (designOption.selectedValue() != null && designOption.selectedValue().id == '2583'){
                            customToShow.push('236');
                        }

                        var lapelCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '52' })[0];
                        var lapelOption = lapelCategory.options.filter(function (el) { return el.id == '164' })[0];

                        if (lapelOption.selectedValue() != null && lapelOption.selectedValue().id == '1613'){
                            customToShow.push('237');
                        }

                        var ventCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '51' })[0];
                        var ventOption = ventCategory.options.filter(function (el) { return el.id == '161' })[0];

                        if (ventOption.selectedValue() != null && ventOption.selectedValue().id == '2609'){
                            customToShow.push('238');
                        }

                        var breastpocketCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '56' })[0];
                        var breastpocketOption = breastpocketCategory.options.filter(function (el) { return el.id == '190' })[0];

                        if (breastpocketOption.selectedValue() != null && breastpocketOption.selectedValue().id == '4586' ) {
                            customToShow.push('258');
                        }

                        // var monogramCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '' })[0];
                        // var monogramOption = monogramCategory.options.filter(function (el) { return el.id ==  })[0];

                        // if (monogramOption.selectedValue() != null && monogramOption.selectedValue().id == ''){
                        //     customToShow.push('245');
                        // }

                        // get front pocket value (if is custom front pocket return true)
                        var frontCategory = self.selectedGarment().categories.filter(function (el) { return el.id == '60' })[0];
                        var frontOption = frontCategory.options.filter(function (el) { return el.id == '215' })[0];

                        if (frontOption.selectedValue() != null && frontOption.selectedValue().id == '3847'){
                            customToShow.push('234');
                        }

                        return customToShow;

                    default:
                        break;
                }


                return true;
            });
        },

         /**
         * To hide and show details of a option
         * @param {*} option 
         */
        setValueClicked(option, value) {


            for( let val of option.values )
            {
                val.clicked(false);
            }

            value.clicked(true);
        },

          // One way of setting the value to an option
        // WARNING: 
        //          Be aware that changing values inside the for each can cause the system to be slow.
        //          Most of the cases this will not happend, but be aware. 
        //          This is caused by Knockout trying to notify all dependencies for every interation.
        setOptionValue(option, value) {

            console.log("Setting ", ko.toJSON(option.name));
            console.log("to ", ko.toJSON(value));

            try {
                // replicate value changes to other options
                const category = this.selectedGarment().categories.filter(el => el.id == option.cat_id)[0];
                this._replicateValue(category, option, value);
            } catch (error) {
                console.error("Error in _replicateValue", error);
            }

            option.selectedValue(value);

            for( let val of option.values )
            {
                val.selected(false);
            }

            if (typeof value !== "boolean"){
                value.selected(true);
            }
        },


        showLogoSection : function() {

            $(".logo-section-wrapper").show();
            this.showUploadLogoPopUp();

        },

        showUploadLogoPopUp : function() {

            $("#upload-logo-step").show();
            $("#logo-details-step").hide();

        },


        /**
         * Open a popup for logo details
         * @param {ko.observable} garment like the selectedGarment
         */
        showLogoDetailsPopUp : function(garment) {
            
            if (garment){
                console.log('logo for garment:', garment());
                // set default view for logo
                // suit
                if (garment().garment_id == '6') {
                    garment().image3D.side('jacket_front');
                }
                
                // others
                else {
                    garment().image3D.side('front');
                    
                }
            }


            $("#logo-details-step").show();
            $("#upload-logo-step").hide();

        },


        autoSelectGarmentType : function() {

            // if there is no selected garment => triggers a click with jquery to select one
            if( !this.uniformTemplateVM.selectedGarmentType() )
            {
                const garmentTypeWithInstances = this.uniformTemplateVM.garmentsStep.garments().filter( gar => gar.garments().length > 0 );

                if( garmentTypeWithInstances.length )
                {
                    this.uniformTemplateVM.selectGarmentType(garmentTypeWithInstances[0]);
                }

            }




        },


         /**
         * Pre select some default values
         * 
         * ex: default fit => 'SEMI FITTED' 
         * @param {Object} garment 
         */
        preSelectValue(garment){
            console.log('pre select value for garment:', garment);

            // pants
            if (garment.garment_id == '1') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '13')[0];
                let fitOption = fitCat.options.filter(option => option.id == '56')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '1252')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);

            }

            // vest
            else if (garment.garment_id == '3') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '21')[0];
                let fitOption = fitCat.options.filter(option => option.id == '73')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '1289')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);
            }

            // shirt
            else if (garment.garment_id == '2') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '30')[0];
                let fitOption = fitCat.options.filter(option => option.id == '88')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '1312')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);

                // pocket position => left
                let pocketCat = garment.categories.filter(category => category.id == '37')[0];
                let positionOption = pocketCat.options.filter(option => option.id == '105')[0];
                let pocketDefaultValue = positionOption.values.filter(value => value.id == '1366')[0];

                positionOption.selectedValue(pocketDefaultValue);
                pocketDefaultValue.selected(true);

                // font monogram => block
                let monogramtCat = garment.categories.filter(category => category.id == '39')[0];
                let fontOption = monogramtCat.options.filter(option => option.id == '122')[0];
                let fontDefaultValue = fontOption.values.filter(value => value.id == '1583')[0];

                fontOption.selectedValue(fontDefaultValue);
                fontDefaultValue.selected(true);

                // platinum make
                let fabricCat = garment.categories.filter(category => category.id == '29')[0];
                let platinumMakeOption = fabricCat.options.filter(option => option.id == '251')[0];

                platinumMakeOption.selectedValue(true);


            }

            // jacket
            else if (garment.garment_id == '4') {
                // fit => semi fitted
                let fitCat = garment.categories.filter(category => category.id == '3')[0];
                let fitOption = fitCat.options.filter(option => option.id == '6')[0];
                let fitDefaultValue = fitOption.values.filter(value => value.id == '22')[0];

                fitOption.selectedValue(fitDefaultValue);
                fitDefaultValue.selected(true);


                // bh => true
                let lapelCat = garment.categories.filter(category => category.id == '6')[0];
                let bhOption = lapelCat.options.filter(option => option.id == '14')[0];
                
                bhOption.selectedValue(true);

                // sleeve button number => 4
                let sleeveCat = garment.categories.filter(category => category.id == '7')[0];
                let numberOption = sleeveCat.options.filter(option => option.id == '29')[0];
                let numberDefaultValue = numberOption.values.filter(value => value.id == '228')[0];

                numberOption.selectedValue(numberDefaultValue);
                numberDefaultValue.selected(true);


                // kissing => FALSE by default
                let kissingOption = sleeveCat.options.filter(option => option.id == '30')[0];
                kissingOption.selectedValue(false);

                // Working => true by default
                let workingOption = sleeveCat.options.filter(option => option.id == '31')[0];
                workingOption.selectedValue(true);
                

                // Breast Pockets => Standard Straight
                let breastPcktCat = garment.categories.filter(category => category.id == '9')[0];
                let styleOption = breastPcktCat.options.filter(option => option.id == '36')[0];
                let styleDefaultValue = styleOption.values.filter(value => value.id == '240')[0];

                styleOption.selectedValue(styleDefaultValue);
                styleDefaultValue.selected(true);

                // "Specially tailored for" 
                let monogramCat = garment.categories.filter(category => category.id == '46')[0];
                let line1Option = monogramCat.options.filter(option => option.id == '128')[0];

                line1Option.selectedValue("");

            }

            // suit
            else if (garment.garment_id == '6') {
                // fit (pant) => semi fitted
                let fitCat_pant = garment.categories.filter(category => category.id == '70')[0];
                let fitOption_pant = fitCat_pant.options.filter(option => option.id == '243')[0];
                let fitDefaultValue_pant = fitOption_pant.values.filter(value => value.id == '4562')[0];

                fitOption_pant.selectedValue(fitDefaultValue_pant);
                fitDefaultValue_pant.selected(true);


                // fit (jacket) => semi fitted
                let fitCat_jacket = garment.categories.filter(category => category.id == '48')[0];
                let fitOption_jacket = fitCat_jacket.options.filter(option => option.id == '158')[0];
                let fitDefaultValue_jacket = fitOption_jacket.values.filter(value => value.id == '2604')[0];

                fitOption_jacket.selectedValue(fitDefaultValue_jacket);
                fitDefaultValue_jacket.selected(true);


                // bh => true
                let lapelCat = garment.categories.filter(category => category.id == '52')[0];
                let bhOption = lapelCat.options.filter(option => option.id == '166')[0];

                bhOption.selectedValue(true);

                // sleeve button number => 4
                let sleeveCat = garment.categories.filter(category => category.id == '54')[0];
                let numberOption = sleeveCat.options.filter(option => option.id == '169')[0];
                let numberDefaultValue = numberOption.values.filter(value => value.id == '2715')[0];

                numberOption.selectedValue(numberDefaultValue);
                numberDefaultValue.selected(true);


                // kissing => true
                let kissingOption = sleeveCat.options.filter(option => option.id == '170')[0];

                kissingOption.selectedValue(true);

                // Breast Pockets => Standard Straight
                let breastPcktCat = garment.categories.filter(category => category.id == '56')[0];
                let styleOption = breastPcktCat.options.filter(option => option.id == '190')[0];
                let styleDefaultValue = styleOption.values.filter(value => value.id == '3649')[0];

                styleOption.selectedValue(styleDefaultValue);
                styleDefaultValue.selected(true);

                // "Specially tailored for" 
                let monogramCat = garment.categories.filter(category => category.id == '63')[0];
                let line1Option = monogramCat.options.filter(option => option.id == '207')[0];

                line1Option.selectedValue("");
            }
        },

         /**
         * For usability purposes, when an option is selected, the value can be replicated to others.
         * This happens mostly for buttons and colors.
         * 
         * For example, in a sleeve of a jacket, instead of putting a color for each of the 5 buttons,
         * we can do for one and the value is replicated to others
         *
         * 
         * @param {Object} option 
         * @param {Object} value 
         */
        _replicateValue(category, option, value){

            // for jacket's sleeve
            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗███████╗    ███████╗██╗     ███████╗███████╗██╗   ██╗███████╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝██╔════╝    ██╔════╝██║     ██╔════╝██╔════╝██║   ██║██╔════╝
            //      ██║███████║██║     █████╔╝ █████╗     ██║   ███████╗    ███████╗██║     █████╗  █████╗  ██║   ██║█████╗  
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   ╚════██║    ╚════██║██║     ██╔══╝  ██╔══╝  ╚██╗ ██╔╝██╔══╝  
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║   ███████║    ███████║███████╗███████╗███████╗ ╚████╔╝ ███████╗
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝    ╚══════╝╚══════╝╚══════╝╚══════╝  ╚═══╝  ╚══════╝
            //                                                                                                               
            // button collor
            if (option.id == "39") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "40")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "40") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "41")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "41") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "42")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "42") {
                // nothing
            }
            

            // button hole
            if (option.id == "44") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "45")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "45") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "46")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "46") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "47")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "47") {
                // nothing
            }

            // button thread
            if (option.id == "49") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "50")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "50") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "51")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "51") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "52")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "52") {
                // nothing
            }

            // for suit's sleeve =======================
            // ███████╗██╗   ██╗██╗████████╗███████╗    ███████╗██╗     ███████╗███████╗██╗   ██╗███████╗
            // ██╔════╝██║   ██║██║╚══██╔══╝██╔════╝    ██╔════╝██║     ██╔════╝██╔════╝██║   ██║██╔════╝
            // ███████╗██║   ██║██║   ██║   ███████╗    ███████╗██║     █████╗  █████╗  ██║   ██║█████╗  
            // ╚════██║██║   ██║██║   ██║   ╚════██║    ╚════██║██║     ██╔══╝  ██╔══╝  ╚██╗ ██╔╝██╔══╝  
            // ███████║╚██████╔╝██║   ██║   ███████║    ███████║███████╗███████╗███████╗ ╚████╔╝ ███████╗
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝    ╚══════╝╚══════╝╚══════╝╚══════╝  ╚═══╝  ╚══════╝
            //                                                                                           
            // button collor
            if (option.id == "174") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "175")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "175") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "176")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "176") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "177")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "177") {
                // nothing
            }


            // button hole
            if (option.id == "179") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "180")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "180") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "224")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "224") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "181")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "181") {
                // nothing
            }

            // button thread
            if (option.id == "183") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "184")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "184") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "185")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            else if (option.id == "185") {
                // if next button has the same value
                const nextOption = category.options.filter(el => el.id == "186")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }

            }
            else if (option.id == "186") {
                // nothing
            }

            // for jacket's monogram ========================
            //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗███████╗    ███╗   ███╗ ██████╗ ███╗   ██╗ ██████╗  ██████╗ ██████╗  █████╗ ███╗   ███╗
            //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝██╔════╝    ████╗ ████║██╔═══██╗████╗  ██║██╔═══██╗██╔════╝ ██╔══██╗██╔══██╗████╗ ████║
            //      ██║███████║██║     █████╔╝ █████╗     ██║   ███████╗    ██╔████╔██║██║   ██║██╔██╗ ██║██║   ██║██║  ███╗██████╔╝███████║██╔████╔██║
            // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   ╚════██║    ██║╚██╔╝██║██║   ██║██║╚██╗██║██║   ██║██║   ██║██╔══██╗██╔══██║██║╚██╔╝██║
            // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║   ███████║    ██║ ╚═╝ ██║╚██████╔╝██║ ╚████║╚██████╔╝╚██████╔╝██║  ██║██║  ██║██║ ╚═╝ ██║
            //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝    ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝
            //                                                                                                                                         

            // jacket piping
            if (option.id == "125") {
                // change pocket color
                var nextOption = category.options.filter(el => el.id == "126")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
                
                // change stitch color
                nextOption = category.options.filter(el => el.id == "239")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }


            // for suit's monogram =========================
            // ███████╗██╗   ██╗██╗████████╗███████╗    ███╗   ███╗ ██████╗ ███╗   ██╗ ██████╗  ██████╗ ██████╗  █████╗ ███╗   ███╗
            // ██╔════╝██║   ██║██║╚══██╔══╝██╔════╝    ████╗ ████║██╔═══██╗████╗  ██║██╔═══██╗██╔════╝ ██╔══██╗██╔══██╗████╗ ████║
            // ███████╗██║   ██║██║   ██║   ███████╗    ██╔████╔██║██║   ██║██╔██╗ ██║██║   ██║██║  ███╗██████╔╝███████║██╔████╔██║
            // ╚════██║██║   ██║██║   ██║   ╚════██║    ██║╚██╔╝██║██║   ██║██║╚██╗██║██║   ██║██║   ██║██╔══██╗██╔══██║██║╚██╔╝██║
            // ███████║╚██████╔╝██║   ██║   ███████║    ██║ ╚═╝ ██║╚██████╔╝██║ ╚████║╚██████╔╝╚██████╔╝██║  ██║██║  ██║██║ ╚═╝ ██║
            // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝    ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝
            //                                                                                                                     

             // jacket piping
             if (option.id == "204") {
                // change pocket color
                var nextOption = category.options.filter(el => el.id == "205")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
                
                // change stitch color
                nextOption = category.options.filter(el => el.id == "241")[0];
                if (nextOption && nextOption.selectedValue().name == option.selectedValue().name) {
                    // get corresponding value
                    var correspondingValue = nextOption.values.filter(el => el.name == value.name)[0];
                    this.setOptionValue(nextOption, correspondingValue);
                }
            }
            

        },

















         /**
         * each category has its own particularity to be complete or not.
         * This method receive a category and return a ko computed for this specific category
         * */
        conditionToBeComplete(category) {
            switch (category.id) {
                // shirt categories:

                // ███████╗██╗  ██╗██╗██████╗ ████████
                // ██╔════╝██║  ██║██║██╔══██╗╚══██╔══
                // ███████╗███████║██║██████╔╝   ██║  
                // ╚════██║██╔══██║██║██╔══██╗   ██║  
                // ███████║██║  ██║██║██║  ██║   ██║  
                // ╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝   ╚═╝  
                //                                     

                // shirt fabric
                case "29":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '86'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // shirt fit
                case "30":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '88'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Sleeve
                case "31":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '89'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // cuffs
                case "32":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '92'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });


                // Shirt Collar
                case "33":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '94'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Front
                case "34":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '96'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom
                case "35":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '99'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Back
                case "36":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '101'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pockets
                case "37":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '103'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '105'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Contrasts
                case "38":
                    return ko.computed(function () {
                        
                        let buttom_colour = category.options.filter(function (el) { return el.id == '114'; })[0];
                        let thread_colour = category.options.filter(function (el) { return el.id == '115'; })[0];
                        return [null, ''].includes(buttom_colour.selectedValue()) ? false :
                            [null, ''].includes(thread_colour.selectedValue()) ? false : true;
                    });

                // Monogram
                case "39":
                    return ko.computed(function () {
                        return category.visited() ; // return true if this category has been visited
                        // let monogram = category.options.filter(function (el) { return el.id == '118'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '119'; })[0];
                        // let sitiching = category.options.filter(function (el) { return el.id == '120'; })[0];
                        // return [null, ''].includes(monogram.selectedValue()) ? false :
                        //     [null, ''].includes(position.selectedValue()) ? false :
                        //         [null, ''].includes(sitiching.selectedValue()) ? false : true;
                    });

                // Notes
                case "40":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '123'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // vest
                // ██╗   ██╗███████╗███████╗████████╗
                // ██║   ██║██╔════╝██╔════╝╚══██╔══╝
                // ██║   ██║█████╗  ███████╗   ██║   
                // ╚██╗ ██╔╝██╔══╝  ╚════██║   ██║   
                //  ╚████╔╝ ███████╗███████║   ██║   
                //   ╚═══╝  ╚══════╝╚══════╝   ╚═╝   
                //                                   

                // Fabric
                case "20":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '71'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Fit
                case "21":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '73'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel
                case "22":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '74'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom
                case "23":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '76'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Buttons
                case "24":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '78'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pockets
                case "25":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '80'; })[0];
                        let breast = category.options.filter(function (el) { return el.id == '82'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : [null, ''].includes(breast.selectedValue()) ? false : true;
                    });

                // Back Style
                case "26":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '83'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lining  Pipping
                case "27":
                    return ko.computed(function () {
                        let lining = category.options.filter(function (el) { return el.id == '132'; })[0];
                        let pipping = category.options.filter(function (el) { return el.id == '133'; })[0];
                        return [null, ''].includes(lining.selectedValue()) ? false :
                            [null, ''].includes(pipping.selectedValue()) ? false : true;
                    });

                // Notes
                case "28":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '85'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // Jacket
                //      ██╗ █████╗  ██████╗██╗  ██╗███████╗████████╗
                //      ██║██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝
                //      ██║███████║██║     █████╔╝ █████╗     ██║   
                // ██   ██║██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   
                // ╚█████╔╝██║  ██║╚██████╗██║  ██╗███████╗   ██║   
                //  ╚════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   
                //                                                  
                // Jacket style
                case "1":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '1'; })[0];
                        let bh = category.options.filter(function (el) { return el.id == '4'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : [null, ''].includes(bh.selectedValue()) ? false : true;
                    });

                // Fabric
                case "2":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '5'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Fit
                case "3":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '6'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom Style
                case "4":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '7'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Vent Style
                case "5":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '9'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Style
                case "6":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '12'; })[0];
                        let bh = category.options.filter(function (el) { return el.id == '14'; })[0];
                        let bh_color = category.options.filter(function (el) { return el.id == '15'; })[0];

                        if (![null, ''].includes(bh.selectedValue()) && ![null, ''].includes(bh_color.selectedValue())) {
                            return [null, ''].includes(style.selectedValue()) ? false : true;

                        } else {
                            return false;
                        }
                    });

                // Sleeves
                case "7":
                    return ko.computed(function () {


                        let numberOfButtons = category.options.filter(function (el) { return el.id == '29'; })[0];

                        if ([null, ''].includes(numberOfButtons.selectedValue())) {
                            return false;
                        }

                        console.log('numberOfButtons.selectedValue().id', numberOfButtons.selectedValue().id);

                        switch (numberOfButtons.selectedValue().id) {
                            // 5 buttons
                            case "229":

                                var sleeve = category.options.filter(function (el) { return el.id == '42'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '47'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '52'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 4 buttons
                            case "228":

                                var sleeve = category.options.filter(function (el) { return el.id == '41'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '51'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '46'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 3 buttons
                            case "227":

                                var sleeve = category.options.filter(function (el) { return el.id == '40'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '45'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '50'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 2 buttons
                            case "226":

                                var sleeve = category.options.filter(function (el) { return el.id == '39'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '44'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '49'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 1 buttom
                            case "225":

                                var sleeve = category.options.filter(function (el) { return el.id == '38'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '43'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '48'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                                break;

                            default:
                                return false;
                        }

                        return [null, ''].includes(sleeve.selectedValue()) ? false :
                            [null, ''].includes(thread.selectedValue()) ? false :
                                [null, ''].includes(hole.selectedValue()) ? false : true;
                    });

                // Pocket Style
                case "8":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '33'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Breastpocket Style
                case "9":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '36'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General notes
                case "10":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '37'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Details
                case "11":
                    return ko.computed(function () {
                        let tuxedo_colour = category.options.filter(function (el) { return el.id == '20'; })[0];
                        let stitch_color = category.options.filter(function (el) { return el.id == '27'; })[0];
                        // return [null, ''].includes(tuxedo_colour.selectedValue()) ? false : [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                        return [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                    });

                // Monogram
                case "46":
                    return ko.computed(function () {
                        let lining = category.options.filter(function (el) { return el.id == '124'; })[0];
                        let pipping = category.options.filter(function (el) { return el.id == '125'; })[0];
                        let pocket_color = category.options.filter(function (el) { return el.id == '126'; })[0];
                        return [null, ''].includes(lining.selectedValue()) ? false :
                            [null, ''].includes(pipping.selectedValue()) ? false :
                                [null, ''].includes(pocket_color.selectedValue()) ? false : true;
                    });

                // Vest style
                case "71":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '251'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });



                // Pants
                // ██████╗  █████╗ ███╗   ██╗████████╗███████╗
                // ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
                // ██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
                // ██╔═══╝ ██╔══██║██║╚██╗██║   ██║   ╚════██║
                // ██║     ██║  ██║██║ ╚████║   ██║   ███████║
                // ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
                //                                            
                // fabric
                case "12":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '54'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // fit
                case "13":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '56'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pant Syle
                case "14":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '57'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pleats
                case "15":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '59'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Front Pockets
                case "16":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '61'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Back Pockets
                case "17":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '63'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '65'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Belt & Cuffs
                case "18":
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '66'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General Notes
                case "19":
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '70'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });






                // Suit
                // ███████╗██╗   ██╗██╗████████╗
                // ██╔════╝██║   ██║██║╚══██╔══╝
                // ███████╗██║   ██║██║   ██║   
                // ╚════██║██║   ██║██║   ██║   
                // ███████║╚██████╔╝██║   ██║   
                // ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   
                //                              

                // Jacket style
                case '49':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '148'; })[0];
                        let b_color = category.options.filter(function (el) { return el.id == '151'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : [null, ''].includes(b_color.selectedValue()) ? false : true;
                    });

                // Fabric
                case '47':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '156'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Fit
                case '48':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '158'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Bottom Style
                case '50':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '159'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Vent Style
                case '51':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '161'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Style
                case '52':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '164'; })[0];
                        let bh = category.options.filter(function (el) { return el.id == '166'; })[0];
                        let bh_color = category.options.filter(function (el) { return el.id == '167'; })[0];

                        if (![null, ''].includes(bh.selectedValue()) && ![null, ''].includes(bh_color.selectedValue())) {
                            return [null, ''].includes(style.selectedValue()) ? false : true;

                        } else {
                            return false;
                        }
                    });

                // Sleeves
                case '54':
                    return ko.computed(function () {


                        let numberOfButtons = category.options.filter(function (el) { return el.id == '169'; })[0];

                        if ([null, ''].includes(numberOfButtons.selectedValue())) {
                            return false;
                        }

                        console.log('numberOfButtons.selectedValue().id', numberOfButtons.selectedValue().id);

                        switch (numberOfButtons.selectedValue().id) {
                            // 5 buttons
                            case '2716':

                                var sleeve = category.options.filter(function (el) { return el.id == '177'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '181'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '186'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 4 buttons
                            case '2715':

                                var sleeve = category.options.filter(function (el) { return el.id == '176'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '224'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '185'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 3 buttons
                            case '2714':

                                var sleeve = category.options.filter(function (el) { return el.id == '175'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '180'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '184'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 2 buttons
                            case '2713':

                                var sleeve = category.options.filter(function (el) { return el.id == '174'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '179'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '183'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                            // 1 buttom
                            case '2712':

                                var sleeve = category.options.filter(function (el) { return el.id == '173'; })[0];
                                var thread = category.options.filter(function (el) { return el.id == '178'; })[0];
                                var hole = category.options.filter(function (el) { return el.id == '182'; })[0];

                                if ([null, ''].includes(sleeve.selectedValue()) || [null, ''].includes(hole.selectedValue()) || [null, ''].includes(thread.selectedValue())) {
                                    return false;
                                }

                                break;

                            default:
                                return false;
                        }

                        return [null, ''].includes(sleeve.selectedValue()) ? false :
                            [null, ''].includes(thread.selectedValue()) ? false :
                                [null, ''].includes(hole.selectedValue()) ? false : true;
                    });

                // Pocket Style
                case '55':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '187'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Breastpocket Style
                case '56':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '190'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General notes
                case '64':
                    return ko.computed(function () {
                        return category.visited() ;
                        // let style = category.options.filter(function (el) { return el.id == '210'; })[0];
                        // return [null].includes(style.selectedValue()) ? false : true;
                    });

                // Lapel Details
                case '53':
                    return ko.computed(function () {
                        let tuxedo_colour = category.options.filter(function (el) { return el.id == '194'; })[0];
                        let stitch_color = category.options.filter(function (el) { return el.id == '201'; })[0];
                        // return [null, ''].includes(tuxedo_colour.selectedValue()) ? false : [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                        return [null, ''].includes(stitch_color.selectedValue()) ? false : true;
                    });

                // Monogram
                case '63':
                    return ko.computed(function () {
                        let lining = category.options.filter(function (el) { return el.id == '203'; })[0];
                        let pipping = category.options.filter(function (el) { return el.id == '204'; })[0];
                        let pocket_color = category.options.filter(function (el) { return el.id == '205'; })[0];
                        return [null, ''].includes(lining.selectedValue()) ? false :
                            [null, ''].includes(pipping.selectedValue()) ? false :
                                [null, ''].includes(pocket_color.selectedValue()) ? false : true;
                    });



                // suit Pants
                // fabric: same as jacket suit
                // case ''____'':
                //     return ko.computed(function () {
                //         let style = category.options.filter(function (el) { return el.id == ''____''; })[0];
                //         return [null, ''].includes(style.selectedValue()) ? false : true;
                //     });

                // fit
                case '70':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '243'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pant Syle
                case '58':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '211'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Pleats
                case '59':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '213'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Front Pockets
                case '60':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '215'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Back Pockets
                case '61':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '217'; })[0];
                        // let position = category.options.filter(function (el) { return el.id == '219'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // Belt & Cuffs
                case '62':
                    return ko.computed(function () {
                        let style = category.options.filter(function (el) { return el.id == '220'; })[0];
                        return [null, ''].includes(style.selectedValue()) ? false : true;
                    });

                // General Notes
                // same as jacket
                // case ''____'':
                //     return ko.computed(function () {
                //         let style = category.options.filter(function (el) { return el.id == ''____''; })[0];
                //         return [null].includes(style.selectedValue()) ? false : true;
                //     });

                default:
                    return ko.observable(true);
            }
        },

         /**
         * For a jacket and suit, return the sleeve number to hide/show contents in UI
         * 
         * 
         * working for jacket. Do this for suit as well
         */
        getSleeveNumber : function () {
            var self = this;
            return ko.computed(function(){
                // for jacket
                try {
                    var sleeveCategory = self.selectedGarment().categories.filter(function(el) {return el.id == '7' })[0];
                    var sleeveNumberOption = sleeveCategory.options.filter(function(el) {return el.id =='29'})[0];
                    return parseInt(sleeveNumberOption.selectedValue().name);
                    
                } catch (TypeError) {
                    // not a jacket
                    // return 0;
                }
                
                // for suit
                try {
                    var sleeveCategory = self.selectedGarment().categories.filter(function(el) {return el.id == '54' })[0];
                    var sleeveNumberOption = sleeveCategory.options.filter(function(el) {return el.id =='169'})[0];
                    return parseInt(sleeveNumberOption.selectedValue().name);

                } catch (TypeError) {
                    // not a suit
                    // return 0;
                }

                return 0;
                
            });
        },


        /**
         * Returns a ko.computed that says if button tuxedo is ON or OFF
         */
        checkButtonsTuxedo : function(){
            var self = this;
            return ko.computed(function(){
                // check type of current garment
                const garmentType = self.uniformTemplateVM.selectedGarmentType().name;

                var lapelDetailsCategory;
                var buttonsTuxedoOption;
                if (garmentType == "jacket"){
                    lapelDetailsCategory = "11";
                    buttonsTuxedoOption = "23";
                }
                else if (garmentType == "suit"){
                    lapelDetailsCategory = "53";
                    buttonsTuxedoOption = "197";
                }
                else {
                    // end function
                    return;
                }

                return self.uniformTemplateVM.selectedGarment().categories.filter(el=>el.id == lapelDetailsCategory)[0].options.filter(el=>el.id == buttonsTuxedoOption)[0].selectedValue();

            });
        }


    });




});