define(['jquery', 'knockout', 'base'], function ($, ko) {


    /**
     * This will be responsible to show the statistics of a given company.
     */
    UniformStatisticsVM = SimpleControl.extend({

        init: function (companyId) {

            var self = this;
            this.company_id = companyId;
            this.company = ko.observable(false);

            console.log("Init UniformStatisticsVM...");

            this.finishedLoading = ko.observable(true);

            this.garments_split = ko.observableArray([]);
            this.salesman_split = ko.observableArray([]);
            this.order_history = ko.observableArray([]);

            this.total_sales_amount = ko.observable(0.00);
            this.getStatisticsData();
        },


        getStatisticsData: function () {

            console.log("fecthing company statistics... data");
            var self = this;

            $.ajax({
                type: 'POST',
                timeout: 120000, // sets timeout to 120 seconds
                url: BUrl + 'orders_pos/get_company_statistics_data',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "company_id": self.company_id
                },
                success: function (dataS) {

                    console.log(dataS);

                    self.company(dataS.company);

                    for (let garment of dataS.statistics.garments_split) 
                    {
                        let img = "";

                        switch (garment.garment_name) {
                            case 'Jacket': img = "img/order_process/wetransfer-b60a96/jacket.png"; break;
                            case 'Pants': img = "img/order_process/wetransfer-b60a96/pant.png"; break;
                            case 'Vest': img = "img/order_process/wetransfer-b60a96/vest.png"; break;
                            case 'Shirt': img = "img/order_process/wetransfer-b60a96/shirt.png"; break;
                        }

                        garment.img = img;
                    }

                    self.garments_split(dataS.statistics.garments_split);

                    self.salesman_split(dataS.statistics.salesman_split);

                    const total = dataS.statistics.salesman_split.length ? dataS.statistics.salesman_split.map(e => e.sales).reduce((accumulator, currentValue) => accumulator + parseFloat(currentValue)) : 0.00;

                    self.total_sales_amount(total);

                    // Build the garments description beautiful HTML
                    for( let order of dataS.statistics.order_history)
                    {   
                        let garmentsMap = {};
                        let $HTML = '';

                        // Counts the garments by its type
                        for( let garment of order.garments )
                        {
                            if( garmentsMap[garment.garment_type] == undefined )
                            {
                                garmentsMap[garment.garment_type] = { 'name' : garment.garment_type, 'qty' : 0 }
                            }

                            garmentsMap[garment.garment_type].qty += 1;
                        }

                        console.log("garmentsMap", garmentsMap);

                        for( let garmentType in garmentsMap)
                        {   
                            $HTML += `${garmentsMap[garmentType].name.substring(0,2)}x${garmentsMap[garmentType].qty} `; 
                        }
                        
                        order.garmentsHTML = $HTML;
                    }



                    self.order_history(dataS.statistics.order_history);

                    self.finishedLoading(true);

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: true
            });
        },



        // to build pie chart ==============================================
        /**
         * Return a list of all available cities
         */
        getCities() {
            var availableCities = [];

            for (const order of this.order_history()) {
                if (!availableCities.includes(order.order_city)) {
                    availableCities.push(order.order_city);
                }
            }


            return availableCities;
        },

        /**
         * get an amount of orders from one city
         * @param {String} city 
         */
        getOrdersByCity(city) {

            return this.order_history().filter(el => el.order_city == city).length;
        },

        /**
         * @returns {Array} [qnt_for_cities[0], qnt_for_cities[1], qnt_for_cities[2], ...]
         * @param {Array} cities 
         */
        getDataSet_pieChart(cities) {
            var response = [];
            for (const index in cities) {
                if (cities.hasOwnProperty(index)) {
                    const city = cities[index];
                    response.push(this.getOrdersByCity(city));
                }
            }

            return response;
        },

        buildPieChart() {

            var self = this;

            var ctx = document.getElementById("CitySalesChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: self.getCities(),
                    datasets: [{
                        label: '# of Orders',
                        data: self.getDataSet_pieChart(self.getCities()),
                        backgroundColor: [
                            'rgba(255, 99, 132)',
                            'rgba(54, 162, 235)',
                            'rgba(255, 206, 86)',
                            'rgba(75, 192, 192)',
                            'rgba(153, 102, 255)',
                            'rgba(255, 159, 64)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {

                }
            });
        },

        // end  build pie chart ==============================================

        // to build bar chart ==============================================
        /**
         * @returns {Array} years
         */
        getYears() {
            var years = [];

            for (const order of this.order_history()) {

                // get year of order (last 4 digits)
                let orderYear = order.order_date.slice(Math.max(order.order_date.length - 4, 1));

                // check if already not in array
                if (!years.includes(orderYear)) {
                    years.push(orderYear);
                }
            }


            return years;

        },

        /**
         * total amount of an Year
         * @param {String} year
         * @returns {Number} amount
         */
        getAmountByYear(year) {
            let amount = 0;

            for (const order of this.order_history()) {

                // get year of order (last 4 digits)
                let orderYear = order.order_date.slice(Math.max(order.order_date.length - 4, 1));

                if (orderYear == year) {
                    amount += order.order_total_amount;
                }
            }

            return amount;

        },


        /**
         * @returns {Array} [amount_for_years[0], amount_for_years[1], amount_for_years[2], ...]
         * @param {Array} years 
         */
        getDataSet_barChart(years) {
            var response = [];
            for (const index in years) {
                if (years.hasOwnProperty(index)) {
                    const year = years[index];
                    response.push(this.getAmountByYear(year));
                }
            }

            return response;
        },

        buildBarChart() {
            var self = this;
            var ctx = document.getElementById("salesChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: self.getYears(),
                    datasets: [{
                        label: 'Total amount by year',
                        data: self.getDataSet_barChart( self.getYears()),
                        backgroundColor: [
                            'rgba(255, 99, 132)',
                            'rgba(54, 162, 235)',
                            'rgba(255, 206, 86)',
                            'rgba(75, 192, 192)',
                            'rgba(153, 102, 255)',
                            'rgba(255, 159, 64)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
        // end build bar chart ==============================================

    });

});