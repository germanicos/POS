define(['jquery', 'knockout', 'base'], function($, ko) {
GarmentsShirt = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;

		this.fabricsVM   = new FabricsCntClass(DsRegistry);
		this.fabricsVM.subscribeTo('fabricsDS');


		this.shirtData     = [];
		this.shirtDataAID  = 0;

		this.previousStepEnabled = ko.observable(false);

		this.mediaUpload = orderItem.MediaUploadVM;
		this.indexGarment = -1;
		this.indexOption = -1;
		this.isImage = false;
		this.customCount = ko.observable(0);
		this.isExternal = false;;
		this.isFabric = false;
		
		this.galleryImages = function(){
			that.isExternal = true;
			$('#files').trigger('click');
		};

		this.addCustomImage = function(indexGarment, indexOption, isFabric){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = true;
			that.isExternal = false;
			that.isFabric = isFabric;
			that.mediaUpload.choosePhoto();
		};
		
		this.addCustomVideo = function(indexGarment, indexOption){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = false;
			that.isExternal = false;
			that.mediaUpload.chooseVideo();
		};
		
		this.mediaCallback = function(data){
			if(that.isFabric){
				orderItem.FabricImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
				that.isFabric = false;
			}
			else if(that.isImage){
				that.shirtData[that.indexGarment].custom.getCustomImages(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
			else{
				that.shirtData[that.indexGarment].custom.getCustomVideos(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
		};

		this.nextStepOLD = function() {			
			var cs = that.currentStep().id;
			if(cs == 0){
				if(that.shirtFabric().title != "none"){
					that.workflow()[cs].completed = true;
				}	
			}else{
				that.workflow()[cs].completed = true;	
			}			
			var foundnotcompletedstep = false;
			for(var x = cs+1; x < that.workflow().length; x++){
				if(that.workflow()[x].completed == false){
					that.currentStep(that.workflow()[x] );
					foundnotcompletedstep = true;
					break;
				}
			}				
			if(foundnotcompletedstep == false){
				for(var x = 0; x <= cs ; x++){
					if(that.workflow()[x].completed == false){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}
			}
			if(foundnotcompletedstep == false){
				posChangePage('#orderItemSelectGarment');
			}
		}


		this.nextStep = function() {
			
			var cs = that.currentStep().id;
				
			var notcompletedstep = false;						
			if(cs == 0){
				for(var x = 0; x < that.shirtData.length; x++){								
					if (that.shirtData[x].shirtFabric.title == 'none' && !that.shirtData[x].ShirtHasCustomerFabric) { 
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 1){
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtSleeveStyle.id == 0) {
						notcompletedstep = true;
						break; 
					}	
				}		
			}else if(cs == 2){ 
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtSleeveStyle.id == 1 && that.shirtData[x].shirtCuffsStyle.id == 0 ) {
						notcompletedstep = true;
						break; 
					}
					if (that.shirtData[x].shirtSleeveStyle.id == 2 && ( that.shirtData[x].shirtCuffsStyle.id == 0 || that.shirtData[x].shirtCuffsStyle.id > 6)  ) {
						notcompletedstep = true;
						break; 
					}
				}					
			}else if(cs == 3){
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtCollarStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 4){
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtFrontStyle.id == 0) {
						notcompletedstep = true;
						break;
					}
				}
			}else if(cs == 5){
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtBottomStyle.id == 0) {
						notcompletedstep = true;
						break;
					}
				}				
			}else if(cs == 6){
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtBackStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 7){
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtPocketStyle.id == 0) {
						notcompletedstep = true;
						break;
					}
				}
			}else if(cs == 8){
				// nothing here
				;
			}else if(cs == 9){
				/*
				for(var x = 0; x < that.shirtData.length; x++){
					if (that.shirtData[x].shirtMonogram.trim() == "") {
						notcompletedstep = true;
						break;
					} 
				}
				*/
			}else if(cs == 'Custom'){
				for(var x = 0; x < that.shirtData.length; x++){
					if(!that.shirtData[x].custom.isFinished()){
						notcompletedstep = true;
						break;
					}
				}
			}	
			
			if(notcompletedstep == false){
				if(cs == 'Custom'){
					that.customWorkflow.completed = true;
				}
				else{
					that.workflow()[cs].completed = true;
				}
				var foundnotcompletedstep = false;
				for(var x = 0; x < that.workflow().length; x++){
					if(that.workflow()[x].completed == false && that.workflow()[x] !== that.customWorkflow){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}			
				if(notcompletedstep == false){
					for(var x = 0; x <= cs ; x++){
						if(that.workflow()[x].completed == false){
							that.currentStep(that.workflow()[x] );
							foundnotcompletedstep = true;
							break;
						}
					}
					if(foundnotcompletedstep == false && !that.customWorkflow.completed && that.customCount() > 0 ){
						that.currentStep(that.customWorkflow);
						foundnotcompletedstep = true;
					}
				}
				if(foundnotcompletedstep == false){
					posChangePage('#orderItemSelectGarment');
				}				
				
			}else{
				if(cs == 'Custom'){
					that.customWorkflow.completed = false;
					that.currentStep(that.customWorkflow);
				}
				else{
					that.workflow()[cs].completed = false;
					that.currentStep(that.workflow()[cs] );
				}
				customAlert("Please fill in all necessary fields");
			}
			
		}



		
		this.workflow    = ko.observableArray([
			{id: 0, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Shirt Fabric"},
			{id: 1, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Sleeves"},
			{id: 2, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Cuffs"},
			{id: 3, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Shirt Collar"},
			{id: 4, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Front"},
			{id: 5, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Bottom"},
			{id: 6, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Back"},
			{id: 7, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Pockets"},
			{id: 8, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Contrasts"},
			{id: 9, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Monogram"},
			{id: 10, target: "#garmentsShirt", completed: false, caption: "Shirt", title: "Notes"},
			{id: 'Custom', target: "#garmentsShirt",  completed: false, caption: "Suit", title: "Custom Options" }
		]);
		this.customWorkflow = this.workflow()[this.workflow().length - 1];
		
		this.customStep = function(){
			that.currentStep(that.workflow()[that.workflow().length]);
		};
		
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			
//console.log("shirtMonogramPosition: " + that.shirtMonogramPosition() );			
			if(data.id == 8){
				that.PreviewVisible('contrast');
			}else if(data.id == 6){
				that.PreviewVisible('back');
			}else if(data.id == 9 && that.shirtMonogramPosition() == 'Collar'){
				that.PreviewVisible('back');	
			}else{
				that.PreviewVisible('front');
			}
			
			posChangePage(data.target);
			$('.cloneDialog').remove();
			that.renderShirt();
		});


		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});
		
		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});
		
		this.reviewItem = ko.computed(function() {
			return "A Shirt";
		});


		this.completion   = ko.observable(0);
		this.variantNameShirt      = ko.observableArray([{id: 0, title: "Shirt 1"}]);

		this.selectedVariantShirt  = ko.observable(this.variantNameShirt()[0]);
		this.selectedVariantShirt.subscribe(function(data) {
			that.selectVariantShirt();
		});

		//Prev/Next computedz

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== true) {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				 return 'asd';
				//return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});






		


		this.ShirtPlatinumTreatment = ko.observable(true);
		this.ShirtPlatinumTreatment.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtPlatinumTreatment = data;
			that.flushModel();
		});


		this.ShirtNotes = ko.observable('');
		this.ShirtNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtNotes = data;
			that.flushModel();
		});
		this.ShirtFabricNotes = ko.observable('');
		this.ShirtFabricNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtFabricNotes = data;
			that.flushModel();
		});
		this.ShirtSleevesAndCuffsNotes = ko.observable('');
		this.ShirtSleevesAndCuffsNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtSleevesAndCuffsNotes = data;
			that.flushModel();
		});
		this.ShirtCollarNotes = ko.observable('');
		this.ShirtCollarNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtCollarNotes = data;
			that.flushModel();
		});
		this.ShirtFrontBottomNotes = ko.observable('');
		this.ShirtFrontBottomNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtFrontBottomNotes = data;
			that.flushModel();
		});
		this.ShirtBackNotes = ko.observable('');
		this.ShirtBackNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtBackNotes = data;
			that.flushModel();
		});
		this.ShirtPocketsNotes = ko.observable('');
		this.ShirtPocketsNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtPocketsNotes = data;
			that.flushModel();
		});
		this.ShirtContrastsNotes = ko.observable('');
		this.ShirtContrastsNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtContrastsNotes = data;
			that.flushModel();
		});
		this.ShirtMonogramNotes = ko.observable('');
		this.ShirtMonogramNotes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtMonogramNotes = data;
			that.flushModel();
			
		});		


		//shirtFabric
		this.shirtFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.shirtFabric.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtFabric = data;
			that.flushModel();
			
		});



		//shirtEpaulletes
		this.shirtEpaulletes = ko.observable(false);
		this.shirtEpaulletes.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtEpaulletes = data;
			that.flushModel();
			
		});

		this.shirtShortCuffs = ko.observable(false); 
		this.shirtShortCuffs.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtShortCuffs = data;
			that.flushModel();
			
		});

		//shirtSleeveStyle
		this.shirtSleeveStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtSleeveStyleDS').getStore());
		this.shirtSleeveStyle       = ko.observable(that.dsRegistry.getDatasource('shirtSleeveStyleDS').getStore()[0]);
		this.shirtSleeveStyle.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtSleeveStyle = data;
			that.flushModel();
			
		});



		//shirtCollarStyle
		this.shirtCollarStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtCollarStyleDS').getStore());
		this.shirtCollarStyle       = ko.observable(that.dsRegistry.getDatasource('shirtCollarStyleDS').getStore()[0]);
		this.shirtCollarStyle.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtCollarStyle = data;
			that.flushModel();
			
		});

		//shirtCuffsStyle
		this.shirtCuffsStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtCuffsStyleDS').getStore());
		this.shirtCuffsStyleListRolled   = ko.observable(that.dsRegistry.getDatasource('shirtCuffsStyleDS').getStore().slice(0, 7));
		this.shirtCuffsStyle       = ko.observable(that.dsRegistry.getDatasource('shirtCuffsStyleDS').getStore()[0]);
		this.shirtCuffsStyle.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtCuffsStyle = data;
			that.flushModel();
			
		
		});



		//shirtSeams
		this.shirtSeams = ko.observable(false);
		this.shirtSeams.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtSeams = data;
			that.flushModel();
			
		});
		
		//shirtFrontStyle
		this.shirtFrontStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtFrontStyleDS').getStore());
		this.shirtFrontStyle       = ko.observable(that.dsRegistry.getDatasource('shirtFrontStyleDS').getStore()[0]);
		this.shirtFrontStyle.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtFrontStyle = data;
			that.flushModel();
			
		
		});

		//shirtDarts
		this.shirtDarts = ko.observable(false);
		this.shirtDarts.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtDarts = data;
			that.flushModel();
			
		});
		
		//shirtBackStyle
		this.shirtBackStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtBackStyleDS').getStore());
		this.shirtBackStyle       = ko.observable(that.dsRegistry.getDatasource('shirtBackStyleDS').getStore()[0]);
		this.shirtBackStyle.subscribe(function(data) {
			that.shirtData[that.getRow(that.selectedVariantShirt().id)].shirtBackStyle = data;
			that.flushModel();
			
		
		});

		//shirtBottomStyle
		this.shirtBottomStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtBottomStyleDS').getStore());
		this.shirtBottomStyle       = ko.observable(that.dsRegistry.getDatasource('shirtBottomStyleDS').getStore()[0]);
		this.shirtBottomStyle.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtBottomStyle = data;
			that.flushModel();
			
		
		});
		

		//shirtPocketStyle
		this.shirtPocketStyleList   = ko.observable(that.dsRegistry.getDatasource('shirtPocketStyleDS').getStore());
		this.shirtPocketStyle       = ko.observable(that.dsRegistry.getDatasource('shirtPocketStyleDS').getStore()[0]);
		this.shirtPocketStyle.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtPocketStyle = data;
			that.flushModel();
			
		
		});
		
		
     	this.shirtPocketPositionList    = ko.observableArray(["Right","Left","Both"]);
		this.shirtPocketPosition     = ko.observable('');
		this.shirtPocketPosition.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtPocketPosition = data;
			that.flushModel();
			
		});


		//shirtContrastFabric
		this.shirtContrastFabric = ko.observable('');
		this.shirtContrastFabric.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastFabric = data;
			that.flushModel();
			
		});

		/*
		this.shirtContrastCheck = ko.observable(false);
		this.shirtContrastCheck.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id) ].shirtContrastCheck = data;
			that.flushModel();
			
		});
		*/

		this.shirtButtonsAndThreadsContrastCheck = ko.observable(false);
		this.shirtButtonsAndThreadsContrastCheck.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id) ].shirtButtonsAndThreadsContrastCheck = data;
			that.flushModel();
			
		});


		this.shirtContrastInsideCollar = ko.observable(false);
		this.shirtContrastInsideCollar.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id) ].shirtContrastInsideCollar = data;
			that.flushModel();
			
		});
		
		this.shirtContrastOutsideCollar = ko.observable(false);
		this.shirtContrastOutsideCollar.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id) ].shirtContrastOutsideCollar = data;
			that.flushModel();
			
		});
		
		this.shirtContrastOutsideCuff = ko.observable(false);
		this.shirtContrastOutsideCuff.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastOutsideCuff = data;
			that.flushModel();
			
		});
		
		this.shirtContrastInsideCuff = ko.observable(false);
		this.shirtContrastInsideCuff.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastInsideCuff = data;
			that.flushModel();
			
		});
		
		this.shirtContrastOutsidePlacket = ko.observable(false);
		this.shirtContrastOutsidePlacket.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastOutsidePlacket = data;
			that.flushModel();
			
		});
		
		this.shirtContrastInsidePlacket = ko.observable(false);
		this.shirtContrastInsidePlacket.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastInsidePlacket = data;
			that.flushModel();
			
		});
		
		this.shirtContrastBackOfCuff = ko.observable(false);
		this.shirtContrastBackOfCuff.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastBackOfCuff = data;
			that.flushModel();
			
		});
		
		this.shirtContrastCollarStand = ko.observable(false);
		this.shirtContrastCollarStand.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtContrastCollarStand = data;
			that.flushModel();
			
		});
		
		//suitButtonColor
		this.shirtButtonColorList = ko.observable(that.dsRegistry.getDatasource('shirtButtonsDS').getStore());
		this.shirtButtonColor     = ko.observable(that.dsRegistry.getDatasource('shirtButtonsDS').getStore()[0]);
		this.shirtButtonColor.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtButtonColor = data;
			that.flushModel();
			
		});

		//shirtThreadColor
		this.shirtThreadColorList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.shirtThreadColor     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.shirtThreadColor.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtThreadColor = data;
			that.flushModel();
			
		});

		this.shirtFitList =  ko.observableArray(["Fitted", "Semi Fitted", "Standard Fit"]);//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore());
		this.shirtFit = ko.observable('Semi Fitted');//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore()[2]);
		this.shirtFit.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtFit = data;
			that.flushModel();
			
		});


		//shirtMonogram 
		this.shirtMonogram = ko.observable('');
		this.shirtMonogram.subscribe(function(data) {			
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtMonogram = data;
			that.flushModel();
			
		});



		this.shirtMonogramPosition = ko.observable(0);
		this.shirtMonogramPosition.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].shirtMonogramPosition = data;
			
			if(data == 'Collar'){
				that.PreviewVisible('back');
			}else{
				that.PreviewVisible('front');
			}	
			
			that.flushModel();
			
		});


		this.ShirtMonogramStitchColourList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.ShirtMonogramStitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[1]);
		this.ShirtMonogramStitchColour.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtMonogramStitchColour = data;
			that.flushModel();
			
		});
		
		this.ShirtMonogramFontList = ko.observable(that.dsRegistry.getDatasource('shirtMonogramFontDS').getStore());
		this.ShirtMonogramFont = ko.observable(that.dsRegistry.getDatasource('shirtMonogramFontDS').getStore()[0]);
		this.ShirtMonogramFont.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].ShirtMonogramFont = data;
			that.flushModel();
			
		});		
		
		
		this.urgent = ko.observable(0);
		this.urgent.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].urgent = data;
			that.flushModel();
			
		});		
				
		/*		
		this.urgentDate = ko.observable('');
		this.urgentDate.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].urgentDate = data;
			that.flushModel();
			
		});		
		*/
		this.DOP_day = ko.observable('');
		this.DOP_day.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].DOP_day = data;
			that.flushModel();
			
		});			
		this.DOP_month = ko.observable('');
		this.DOP_month.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].DOP_month = data;
			that.flushModel();
			
		});				
		this.DOP_date = ko.observable('');
		this.DOP_date.subscribe(function(data) {
			that.shirtData[ that.getRow(that.selectedVariantShirt().id)  ].DOP_date = data;
			that.flushModel();

		});				


		this.setAsCompleted =  function(id, status) {
		//	console.log('setAdCompleted ' + status);
			var tWork = that.workflow();
			tWork[id].completed = status;
			//that.workflow(tWork);
		};


		this.PreviewVisible = ko.observable('front');

		this.shirtFabricSelect = function() {
			that.dsRegistry.setContext({
				targetModel:     'garmentsShirtDS',
				targetGarment:   'Shirt',
				targetDelta:      this.getRow(this.selectedVariantShirt().id),
				targetAttribute: 'shirtFabric',
				callbackPage:    '#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsShirtDS').getStore().Shirt[that.getRow(that.selectedVariantShirt().id)].shirtFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};

		this.shirtContrastFabricSelect = function() {
			console.log('cfs');
			this.dsRegistry.setContext({
				targetModel:     'garmentsShirtDS',
				targetGarment:   'Shirt',
				targetDelta:      this.getRow(this.selectedVariantShirt().id),
				targetAttribute: 'shirtContrastFabric',
				callbackPage:    '#garmentsVestContrast'
			});
			var fab = that.dsRegistry.getDatasource('garmentsShirtDS').getStore().Shirt[that.getRow(that.selectedVariantShirt().id)].shirtContrastFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};

		this.fnCloneShirt = function(e) {
				fe = this;
				var parentOffset = $('.content-wrapper').offset(); 
				//or $(this).offset(); if you really just want the current element's offset
				relX = e.pageX - parentOffset.left;
				relY = e.pageY - parentOffset.top;

				//need testing
				if (relY > 500) relY -= 200;
				targetAttr = $(this).parent().parent().parent().attr('data-target-attr');
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().parent().attr('data-target-attr');	
				}
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().attr('data-target-attr');	
				}	

				currentVariant = that.selectedVariantShirt();
				var vnames = that.variantNameShirt(); 
				if (vnames.length == 1) return;

				var fa  = 'Select shirt changes! ';
			    fa += '<div class="cloneDialogBtnClose" data-role="button"><img src="http://shirt-tailor.net/thepos/appimg/template/topmenu/close.png"/></div><div  data-role="fieldcontain"><fieldset data-role="controlgroup">';
				for (var ind in vnames) {
					//if (vnames[ind].id != currentVariant.id) {
						check = '';
						if (vnames[ind].id == currentVariant.id) check = ' disabled checked="checked" ';
						fa += '<input type="checkbox" '+ check +' data-id="' + vnames[ind].id + '" id="cSuit-'+ vnames[ind].id +'" name="cSuit-'+ vnames[ind].id +'"/>'
						fa += '<label for ="cSuit-'+ vnames[ind].id +'">' + vnames[ind].title + '</label>';
					//}
				}
				fa += "</fieldset></div><div class='cloneDialogBtn' data-role='button'>Apply</div>";
				$('.cloneDialog').remove();
				$('.content-wrapper').append("<div class='cloneDialog' id='cloneSuitDialog' style='left: " + relX + "px !important; top: " + relY + "px !important;'>" + fa + "</div>");
				
				$('#cloneSuitDialog input').checkboxradio();
				$('.cloneDialogBtn').on('click', function() {
					cI = [];
					$('#cloneSuitDialog :checked').each(function(thet,c) {cI.push($(c).attr('data-id'));});
					that.cloneShirtAttributes(currentVariant.id, targetAttr, cI);
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
					//$.mobile.sdCurrentDialog.close();
				});
				$('.cloneDialogBtnClose').on('click', function() {
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
				});
		}


		this.cloneShirtAttributes = function( pvId, pvTA, cvI)
		{
			var tAD = that.getVariant(pvId)[pvTA];
			for (var ind in that.shirtData) {
				if ( cvI.indexOf(that.shirtData[ind].variantId.toString()) != -1) {
					//console.log('cloning to ' + that.shirtData[ind].variantId);
					that.shirtData[ind][pvTA] = tAD; 
				}
			}
			that.flushModel();
		};


		this.deleteVariantShirt = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {		 						
							var currentcount = orderItem.garmentsList()[4].count();
							orderItem.garmentsList()[4].count( currentcount - 1);							
							var tShirtData = [];
							var tvarsData = [];
							for (var ind in that.variantNameShirt() ) {
								if ( that.variantNameShirt()[ind].id != data.id ) {
									tvarsData.push(that.variantNameShirt()[ind]);
									tShirtData.push(that.getVariant( that.variantNameShirt()[ind].id ));
								}
							}						
							that.shirtData = tShirtData;				
							that.flushModel();
							that.variantNameShirt(tvarsData);
							that.selectedVariantShirt(that.variantNameShirt()[0]);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};
		
		this.deleteVariantShirt2 = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {		 						
							var currentcount = orderItem.garmentsList()[4].count();
							orderItem.garmentsList()[4].count( currentcount - 1);							
							var tShirtData = [];
							var tvarsData = [];
							for (var ind in that.variantNameShirt() ) {
								if ( that.variantNameShirt()[ind].id != data.id ) {
									tvarsData.push(that.variantNameShirt()[ind]);
									tShirtData.push(that.getVariant( that.variantNameShirt()[ind].id ));
								}
								else{
									orderItem.removeFabric('Shirt', 4, 'Shirt ' + (that.shirtData[ ind ].variantId + 1), that.shirtData[ ind ].ShirtCustomerFabric, that.shirtData[ ind ].variantId, 2);
								}
							}						
							that.shirtData = tShirtData;				
							that.flushModel();
							that.variantNameShirt(tvarsData);
							that.selectedVariantShirt(that.variantNameShirt()[0]);
							
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "block";
							//posChangePage('#bodyshape');
							//posChangePage('#orderItemSelectGarment');
							orderItem.prepareGarments();
							orderItem.populateGarments();
							setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },1000);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};		



		
		
		
				
		ko.bindingHandlers.modifiedShirtListOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var wantedValue = value[1];	
					var list = document.getElementsByName(value[2]);
					var checkboxes = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;
					var elemdivs = document.getElementsByName(value[5]);

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						if(wantedValue.id != undefined){
							checkValue = wantedValue.id; 
						}else{
							checkValue = wantedValue;
						}	
						if(spanvalue == checkValue){
							checkboxes[ index ].classList.add('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('showtrigger');
								elemdivs[ index ].classList.remove('offtrigger');
							}	
						}else{
							checkboxes[ index ].classList.remove('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('offtrigger');
								elemdivs[ index ].classList.remove('showtrigger');
							}	
						}	
					}
					
					if(value[4] != 'none'){
						var customdivs = document.getElementsByName(value[4]);
						//if(customdivs[value[0]] != undefined){
							if(wantedValue.id == '123'){
								//customdivs[value[0]].style.display = 'block';
								that.shirtData[value[0]].custom.addCustomInfo(value[7]);
								that.shirtData[value[0]].custom.custom0 = 100;
								that.customCount(that.customCount() + 1);
							}else{
								if(that.shirtData[value[0]].custom.removeCustomInfo(value[7])){
									that.customCount(that.customCount() - 1);
								}
							}
						//}	
					}
					
					if(value[6] != undefined && value[6] != 'none'){
						var mainoptionsdivs = document.getElementsByName(value[6]);
						var mainoptionscount = mainoptionsdivs.length;
						
						for(var a = 0; a < mainoptionscount; a++){
							var haschecked = false;
							for(var b = 0; b < garmentscount; b++){
								var index = a*garmentscount + b;
								if(checkboxes[index].classList.contains('selecteds')){
									haschecked = true;
									break;
								}
							}
							if(haschecked == true){
								mainoptionsdivs[a].classList.add('activate');
							}else{
								mainoptionsdivs[a].classList.remove('activate');
							}
						}
					}
					
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);
		        });
		    }
		};    

		ko.bindingHandlers.modifiedShirtListBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( !checkboxes[ index ].classList.contains(value[3]) ){
							checkboxes[ index ].classList.add(value[3]);
						}else{
							checkboxes[ index ].classList.remove(value[3]);						
						}	
					}
					
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);					
		        });
		    }
		};   
		
		ko.bindingHandlers.modifiedShirtListSelectOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var selects = document.getElementsByName(value[2]);
					var garmentscount = selects.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						
						selects[ index ].value = that.shirtData[ value[0] ][value[2]];

						if ("createEvent" in document) {
							var evt = document.createEvent("HTMLEvents");
		    				evt.initEvent("change", false, true);
							selects[ index ].dispatchEvent(evt);
						}else{
							selects[ index ].fireEvent("onchange");
						}												
						
					}
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);					
		        });
		    }
		};   			


		ko.bindingHandlers.modifiedShirtBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var checkboxes = document.getElementsByName(value[1]);
					var index = value[0];
					var cssclass = "" + value[2];

					if( !checkboxes[ index ].classList.contains(cssclass) ){
						checkboxes[ index ].classList.add(cssclass);
					}else{
						checkboxes[ index ].classList.remove(cssclass);
					}	
					if( value[1] != 'FabricCheck' ){
						that.selectedVariantShirt(that.variantNameShirt()[value[0]]);
					}	
		        });
		    }
		};
		
		ko.bindingHandlers.modifiedShirtSleevesCuffsText = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "change", function() { 
		        	var value = valueAccessor(); 	
console.log("element.value: " + element.value );		        	
					document.getElementsByName('ShirtSleevesAndCuffsNotes2')[0].innerHTML = element.value;
		            if ("createEvent" in document) {
		    			var evt = document.createEvent("HTMLEvents");
		    			evt.initEvent("change", false, true);
		    			document.getElementsByName('ShirtSleevesAndCuffsNotes2')[0].dispatchEvent(evt);
					}else{
		   				document.getElementsByName('ShirtSleevesAndCuffsNotes2')[0].fireEvent("onchange");
	            	}		        	
		        });
		    }
		};
		ko.bindingHandlers.modifiedShirtSleevesCuffs2Text = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "change", function() { 
		        	var value = valueAccessor(); 	
console.log("element.value: " + element.value );		        	
					document.getElementsByName('ShirtSleevesAndCuffsNotes')[0].innerHTML = element.value;
		            if ("createEvent" in document) {
		    			var evt = document.createEvent("HTMLEvents");
		    			evt.initEvent("change", false, true);
		    			document.getElementsByName('ShirtSleevesAndCuffsNotes')[0].dispatchEvent(evt);
					}else{
		   				document.getElementsByName('ShirtSleevesAndCuffsNotes')[0].fireEvent("onchange");
	            	}		        	
		        });
		    }
		}; 			
		


		ko.bindingHandlers.switchpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);

					for(var x = 0; x < popups.length; x++){
						if(x != value[0]){
							popups[x].style.display = "none";	
						}
					}
					if(popups[value[0]].style.display == "block"){
						popups[value[0]].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}else{
						popups[value[0]].style.display = "block";		
					}

		        });
		    }
		}; 
		

		ko.bindingHandlers.shirtswitchcustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);
					
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.shirtData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		};

		ko.bindingHandlers.shirtopencustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);
					
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.shirtData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					popup[0].style.display = "block";		
		        });
		    }
		};		
		

		ko.bindingHandlers.openpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);			
					popups[value[0]].style.display = "block";		//.show(), .dialog( "open" ) : not working
		        });
		    }
		}; 

		ko.bindingHandlers.closepopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[0]);
					for(var x = 0; x < popups.length; x++){			
						popups[x].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}	
		        });
		    }
		}; 


		ko.bindingHandlers.modifiedShirtThreads = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var images = document.getElementsByName(value[3]);
					var texts = document.getElementsByName(value[3] + "Text");
					var options = document.getElementsByName(value[4]);
					images[value[0]].src = value[2].image;
					if(texts[value[0]] != undefined){
						texts[value[0]].innerHTML = value[2].title;
					}	

					var garmentssize = options.length/images.length;
					for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[garmentssize*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);
		        });
		    }
		}; 
		
		
		ko.bindingHandlers.modifiedShirtColors = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var listdivs = document.getElementsByName(value[3]);					
					var listimages = document.getElementsByName(value[4]);
					var listtexts = document.getElementsByName(value[5]);
					var options = document.getElementsByName(value[6]);
					var garmentssize = document.getElementById(value[7]).innerHTML;
					var shirtoptionsnumber = listdivs.length/garmentssize;
					var coloroptionsnumber = options.length/garmentssize;
					
					for(var x = 0; x < shirtoptionsnumber; x++){
						var y = value[0] + x*garmentssize;
						if(listimages.length > 0){	 				
							listimages[y].src = value[2].image;
						}
						if(listtexts.length > 0){
							listtexts[y].innerHTML = value[2].title;
						}
					}					
					for(var x = coloroptionsnumber*value[0]; x < coloroptionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[coloroptionsnumber*value[0] + value[1]].classList.add('selected');
		        });
		    }
		};		
		
		
		ko.bindingHandlers.modifiedShirtFit = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					for(var x = 0; x < that.shirtData.length; x++){
						that.shirtData[ x ].shirtFit = value;	
					}
					that.flushModel();
		        });
		    }
		}; 		
		

		ko.bindingHandlers.modifiedShirtDropdown = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					that.selectedVariantShirt(that.variantNameShirt()[value[0]]);
		        });
		    }
		}; 
		
		
		ko.bindingHandlers.shirtslider = {
		    init: function(element, valueAccessor, allBindingsAccessor) {
		        //initialize the control
		        var options = allBindingsAccessor().shirtslideroptions || {};
		        $(element).slider(options);
		
		        //handle the value changing in the UI
		        ko.utils.registerEventHandler(element, "slidechange", function() {
		            //would need to do some more work here, if you want to bind against non-observables
		            var values = valueAccessor(); 
		            var observable = values[0];
		            observable($(element).slider("value"));
		        });
		
		    },
		    //handle the model value changing
		    update: function(element, valueAccessor) {
		    	var values = valueAccessor();
		        var value = ko.utils.unwrapObservable(values[0]);
		       // $(element).slider("value", value);
				var maindivs = document.getElementsByName(values[1]);
				var alltexts = document.getElementsByName(values[2]);
				var garmentscount = alltexts.length/maindivs.length;
				var optionscount = maindivs.length;
				
				for(var a = 0; a < maindivs.length; a++){
					for(var b = 0; b < garmentscount; b++){
						var index = a*garmentscount + b;						
						alltexts[index].innerHTML = that.shirtData[ b ].ShirtLapelWidth; 
					}	
				}
				
				try{
					$(element).val(value).slider('refresh');
				}catch(e){
					;
				} 

		    }
		};		
		
		
		
		ko.bindingHandlers.modifyClassOfDiv = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {	        	  		        	      	
					var value = valueAccessor();
					var index = value[0];
					var divs = document.getElementsByName(value[1]);
					var classname = 'activated'; //value[2];
					var activeclassname = 'activate';//value[3];
					//var notactiveclassname = value[4];
					
					var addtheclass = true;
					if( divs[ index ].classList.contains(classname) ){
						addtheclass = false;
					}
					for(var x = 0; x < divs.length; x++){
						divs[ x ].classList.remove(classname);
					}		
					if(addtheclass == true){			
						divs[ index ].classList.add(classname);
					}	
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.shirtFabricApplier = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
				//	for(var x = 0; x < value.length; x++){
				//		console.log("VALUE " + x + ": " + JSON.stringify(value[x]) );
				//	}
					
					var checkboxes = document.getElementsByName(value[0]);
					var texts = document.getElementsByName(value[3]);
					var images = document.getElementsByName(value[4]);
					var cssclass = "" + value[2];
					var tomakegreenelements = document.getElementsByClassName("tomakegreen"); 
					
					var garmentsnumber = checkboxes.length;
					var textsnumber = texts.length;
					var differenttypesnumber = textsnumber/garmentsnumber;

					var anychecked = false;
					
					for(var x = 0; x < checkboxes.length; x++){
						if( checkboxes[x].classList.contains(cssclass)){
							
							orderItem.removeFabric('Shirt', 4, 'Shirt ' + (x + 1), that.shirtData[ x ].ShirtCustomerFabric, that.shirtData[ x ].variantId, 2);
							
							if(!orderItem.ClientFabric() && document.getElementById("selectedfabric").innerHTML != "none"){
								checkboxes[x].classList.remove(cssclass);
								tomakegreenelements[x].classList.add('makegreen');
								anychecked = true;
								that.shirtData[ x ].shirtFabric = value[1];
								that.shirtData[ x ].ShirtCustomerFabric = {};
								that.shirtData[ x ].ShirtHasCustomerFabric = false;
								if(texts.length > 0){
									//texts[x].innerHTML = value[1].title;
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = value[1].title;
											texts[pos].style.display = "block";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = '';
											texts[pos+1].style.display = "none";
										}
									}
								}
								if(images.length > 0){
									images[x].src = value[1].fabricImage;
								}
							}
							else if(orderItem.ClientFabric()){
								if(orderItem.addFabricToGarment(that.shirtData[ x ], 'Shirt', 4,'Shirt ' + (x + 1), 2)){
									checkboxes[x].classList.remove(cssclass);
									tomakegreenelements[x].classList.add('makegreen');
									anychecked = true;
									that.shirtData[ x ].shirtFabric = dsRegistry.getDatasource('fabricsDS').getStore()[1];
									that.shirtData[ x ].ShirtCustomerFabric = orderItem.SelectedCustomerFabric();
									that.shirtData[ x ].ShirtHasCustomerFabric = true;
									
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = '';
											texts[pos].style.display = "none";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = orderItem.SelectedCustomerFabric().FabricCode;
											texts[pos+1].style.display = "block";
										}
									}
								}
								else if(that.shirtData[ x ].ShirtCustomerFabric.FabricCode){
									var temp = orderItem.SelectedCustomerFabric();
									orderItem.SelectedCustomerFabric(that.shirtData[ x ].ShirtCustomerFabric);
									orderItem.addFabricToGarment(that.shirtData[ x ], 'Shirt', 4,'Shirt ' + (x + 1), 2)
									orderItem.SelectedCustomerFabric(temp);
								}
							}
						}
					}							
					if(anychecked == true){
						document.getElementById("selectedfabric").innerHTML = "none";
						document.getElementById("fabricrange").innerHTML = "0";
						document.getElementById("composition").innerHTML = "0";
						document.getElementById("fabricinput").value = "";
						document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
						document.getElementById("fabricinfo").style.display = "none";
						orderItem.ClientFabric(false);
						orderItem.SelectedCustomerFabric({});
					}
		        });
		    }
		}; 
			
		
		ko.bindingHandlers.shirtContrastDependencies = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
						
					var index = value[0];
					var elem =  "" + value[1];
					if( that.shirtData[ index ][elem] == true ){							
						for(var x = 2; x < value.length; x ++){						
							document.getElementsByName(value[x])[index].classList.remove('invisible');
						}
					}else{
						for(var x = 2; x < value.length; x ++){						
							document.getElementsByName(value[x])[index].classList.add('invisible');
						}
					}
					

		        });
		    }
		};
		
		
		 this.shirtSleevesCuffsDependencies = function( value0, value1 ){
						
			var shirtCuffsStyleList = document.getElementsByName(value0);
			var shirtCuffsStyleEachGarment = document.getElementsByName(value1);
			var garmentsnumber = shirtCuffsStyleEachGarment.length/shirtCuffsStyleList.length;
			
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < shirtCuffsStyleList.length; y++){
					var pos = y*garmentsnumber + x;
					
					if(that.shirtData[ x ].shirtSleeveStyle.id == 2){
						if(pos >= garmentsnumber*7){
							shirtCuffsStyleEachGarment[pos].style.display = "none"; 
						}else{
							shirtCuffsStyleEachGarment[pos].style.display = "";
						}	
					}else if(that.shirtData[ x ].shirtSleeveStyle.id == 3){
						shirtCuffsStyleEachGarment[pos].style.display = "none";
					}else{
						shirtCuffsStyleEachGarment[pos].style.display = "";
					}
				}
			}
		}; 	
		
		
		ko.bindingHandlers.shirtCloneMonogram = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var values = valueAccessor();
					for(var x = 0; x < that.shirtData.length; x++){
						that.shirtData[ x ].shirtMonogram = values[0];
						that.shirtData[ x ].shirtMonogramPosition = values[1];
						
					}  
					that.flushModel();
		        });
		    }
		}; 	

	},
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},

	
	flushModel: function() {
console.log("this.subscribed: " + JSON.stringify(this.subscribed) );		
		this.dsRegistry.getDatasource(this.subscribed).setStore({			
			"Shirt"  : this.shirtData
		}, true);
	},

	selectVariantShirt: function() {
		this.renderShirt();
	},

	getVariant: function(id) {
		var toreturn = this.shirtData[0];
		for (var ind in this.shirtData) {			
			if ( this.shirtData[ind].variantId == id  ) {				
				toreturn = this.shirtData[ind];
			}
		}
		return toreturn;

	},

	getRow: function(id) {
		for (var ind in this.shirtData) {
			if ( this.shirtData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},

	addVariantShirt: function() {
//console.log("AT addVariantShirt");
//console.log("SHIRTDATA BEFORE: " + JSON.stringify(this.shirtData));
//console.log("this.shirtDataAID: " + this.shirtDataAID);	
//console.log("this.variantNameShirt().length: " + this.variantNameShirt().length);		
		this.shirtDataAID += 1;
		//var vname = "Shirt " + (this.variantNameShirt().length + 1);
		var newid = this.shirtDataAID + 1;
		var vname = "Shirt " + newid;


		var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShirt().id ) )  ); //CLONE Object
		this.variantNameShirt.push({title: vname, id: this.shirtDataAID});
		tObj.custom = new CustomProperties();
		tObj.variantId = this.shirtDataAID;
		if(orderItem.shirtfirsttime == true){
			this.shirtData.push(tObj);                                                       //Push to internal
		}	

//console.log("ERROR IS HERE: " + JSON.stringify(this.shirtData));		
		this.flushModel();
	},


	addVariantShirt2: function() {
		
		var currentcount = orderItem.garmentsList()[4].count();
//console.log('addVariantShirt2 currentcount: ' + currentcount);	
		orderItem.garmentsList()[4].count( currentcount + 1);
//console.log('addVariantShirt2 newcount: ' + orderItem.garmentsList()[4].count());
		
		this.shirtDataAID += 1;
		//var vname = "Shirt " + (this.variantNameShirt().length + 1);
		var newid = this.shirtDataAID + 1;
		var vname = "Shirt " + newid;
		
console.log( "this.selectedVariantShirt():" +  JSON.stringify(this.selectedVariantShirt()) );		
		
		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantShirt().id )  ); //CLONE Object
		//var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow( this.selectedVariantShirt().id ) ) ); // PROBLEMATIC
		this.variantNameShirt.push({title: vname, id: this.shirtDataAID});
		tObj.variantId = this.shirtDataAID;
		tObj.custom = new CustomProperties();
		this.shirtData.push(tObj);                                                       //Push to internal
		this.flushModel();
		this.currentStep(this.workflow()[0] );
		var lastelemindex = this.variantNameShirt().length -1;
		this.selectedVariantShirt( this.variantNameShirt()[lastelemindex] );
	},

	digestData: function(data) {
		this.shirtData  = data.Shirt;
		this.renderView();

	},

	renderView: function() {
		this.renderShirt();
	},

	renderShirt: function() {
		//Get selected Variant

		try{
			var tData = this.getVariant(this.selectedVariantShirt().id);
			if (tData != null) {
	
				//Update observables
				if (typeof(tData.ShirtNotes)                != "undefined") {this.ShirtNotes(tData.ShirtNotes);}
				if (typeof(tData.ShirtPlatinumTreatment)    != "undefined") {this.ShirtPlatinumTreatment(tData.ShirtPlatinumTreatment);}
				if (typeof(tData.ShirtFabricNotes)          != "undefined") {this.ShirtFabricNotes(tData.ShirtFabricNotes);}
				if (typeof(tData.ShirtSleevesAndCuffsNotes) != "undefined") {this.ShirtSleevesAndCuffsNotes(tData.ShirtSleevesAndCuffsNotes);}
				if (typeof(tData.ShirtCollarNotes)          != "undefined") {this.ShirtCollarNotes(tData.ShirtCollarNotes);}
				if (typeof(tData.ShirtFrontBottomNotes)     != "undefined") {this.ShirtFrontBottomNotes(tData.ShirtFrontBottomNotes);}
				if (typeof(tData.ShirtBackNotes)            != "undefined") {this.ShirtBackNotes(tData.ShirtBackNotes);}
				if (typeof(tData.ShirtPocketsNotes)         != "undefined") {this.ShirtPocketsNotes(tData.ShirtPocketsNotes);}
				if (typeof(tData.ShirtContrastsNotes)       != "undefined") {this.ShirtContrastsNotes(tData.ShirtContrastsNotes);}
				if (typeof(tData.ShirtMonogramNotes)        != "undefined") {this.ShirtMonogramNotes(tData.ShirtMonogramNotes);}
				
				if (typeof(tData.shirtFabric)               != "undefined") {this.shirtFabric(tData.shirtFabric);}
				if (typeof(tData.shirtContrastFabric)       != "undefined") {this.shirtContrastFabric(tData.shirtContrastFabric);}		
				if (typeof(tData.shirtSleeveStyle)          != "undefined") {this.shirtSleeveStyle(tData.shirtSleeveStyle);}
				if (typeof(tData.shirtBackStyle)            != "undefined") {this.shirtBackStyle(tData.shirtBackStyle);}
				if (typeof(tData.shirtBottomStyle)          != "undefined") {this.shirtBottomStyle(tData.shirtBottomStyle);}
				if (typeof(tData.shirtCollarStyle)          != "undefined") {this.shirtCollarStyle(tData.shirtCollarStyle);}
				if (typeof(tData.shirtCuffsStyle)           != "undefined") {this.shirtCuffsStyle(tData.shirtCuffsStyle);}
				if (typeof(tData.shirtFrontStyle)           != "undefined") {this.shirtFrontStyle(tData.shirtFrontStyle);}
				if (typeof(tData.shirtPocketStyle)          != "undefined") {this.shirtPocketStyle(tData.shirtPocketStyle);}
				if (typeof(tData.shirtPocketPosition)       != "undefined") {this.shirtPocketPosition(tData.shirtPocketPosition);}
				if (typeof(tData.shirtSeams)                != "undefined") {this.shirtSeams(tData.shirtSeams);}
				if (typeof(tData.shirtDarts)                != "undefined") {this.shirtDarts(tData.shirtDarts);}
				if (typeof(tData.shirtEpaulletes)           != "undefined") {this.shirtEpaulletes(tData.shirtEpaulletes);}
				if (typeof(tData.shirtShortCuffs)           != "undefined") {this.shirtShortCuffs(tData.shirtShortCuffs);}
				if (typeof(tData.shirtMonogram )            != "undefined") {this.shirtMonogram(tData.shirtMonogram);}
				if (typeof(tData.shirtContrastOutsideCuff )        != "undefined") {this.shirtContrastOutsideCuff(tData.shirtContrastOutsideCuff);}
				if (typeof(tData.shirtContrastInsideCuff )        != "undefined") {this.shirtContrastInsideCuff(tData.shirtContrastInsideCuff);}
				if (typeof(tData.shirtContrastOutsideCollar )      != "undefined") {this.shirtContrastOutsideCollar(tData.shirtContrastOutsideCollar);}
				if (typeof(tData.shirtContrastInsideCollar )      != "undefined") {this.shirtContrastInsideCollar(tData.shirtContrastInsideCollar);}
				if (typeof(tData.shirtContrastInsidePlacket )     != "undefined") {this.shirtContrastInsidePlacket(tData.shirtContrastInsidePlacket);}
				if (typeof(tData.shirtContrastOutsidePlacket )     != "undefined") {this.shirtContrastOutsidePlacket(tData.shirtContrastOutsidePlacket);}
				if (typeof(tData.shirtContrastBackOfCuff )     != "undefined") {this.shirtContrastBackOfCuff(tData.shirtContrastBackOfCuff);}
				if (typeof(tData.shirtContrastCollarStand )     != "undefined") {this.shirtContrastCollarStand(tData.shirtContrastCollarStand);}
				if (typeof(tData.shirtButtonColor )    		!= "undefined") {this.shirtButtonColor(tData.shirtButtonColor);}
				if (typeof(tData.shirtMonogramPosition )    != "undefined") {this.shirtMonogramPosition(tData.shirtMonogramPosition);}
				if (typeof(tData.ShirtMonogramStitchColour )    != "undefined") {this.ShirtMonogramStitchColour(tData.ShirtMonogramStitchColour);}
				if (typeof(tData.ShirtMonogramFont )    != "undefined") {this.ShirtMonogramFont(tData.ShirtMonogramFont);}			
				if (typeof(tData.shirtThreadColor )    		!= "undefined") {this.shirtThreadColor(tData.shirtThreadColor);}
				if (typeof(tData.shirtFit )   				!= "undefined") {this.shirtFit(tData.shirtFit);}
		//		if (typeof(tData.shirtContrastCheck )   	!= "undefined") {this.shirtContrastCheck(tData.shirtContrastCheck);}
				if (typeof(tData.shirtButtonsAndThreadsContrastCheck )   	!= "undefined") {this.shirtButtonsAndThreadsContrastCheck(tData.shirtButtonsAndThreadsContrastCheck);}
				if (typeof(tData.urgent )   	!= "undefined") {this.urgent(tData.urgent);}
				//if (typeof(tData.urgentDate )   	!= "undefined") {this.urgentDate(tData.urgentDate);}
				if (typeof(tData.DOP_day)		!= "undefined") {this.DOP_day(tData.DOP_day);}
				if (typeof(tData.DOP_month)		!= "undefined") {this.DOP_month(tData.DOP_month);}
				if (typeof(tData.DOP_date)		!= "undefined") {this.DOP_date(tData.DOP_date);}
				
				if ( this.currentStep().id == 0) this.shirtFabricSelect();
				//if ( this.currentStep().id == 4) this.shirtContrastFabricSelect();
			}
		}catch(e){
			;
		}
	}


});

defShirt = SimpleDatasource.extend({
	init: function(name, dsRegistry, olddata, count) {
		if(olddata == null || olddata == undefined){
			var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
			var Shirt = {};
			Shirt.variantId         = 0;
			Shirt.ShirtNotes                = '';
			Shirt.ShirtPlatinumTreatment	= true;
			Shirt.ShirtFabricNotes			= '';
			Shirt.ShirtSleevesAndCuffsNotes	= '';
			Shirt.ShirtCollarNotes			= '';
			Shirt.ShirtFrontBottomNotes		= '';
			Shirt.ShirtBackNotes			= '';
			Shirt.ShirtPocketsNotes			= '';
			Shirt.ShirtContrastsNotes		= '';
			Shirt.ShirtMonogramNotes		= '';
			
			Shirt.shirtFabric                = df;
	
			Shirt.shirtContrastFabric	= '';//df;
			Shirt.shirtContrastInsideCollar    = false;
	        Shirt.shirtContrastOutsideCollar    = false;
	        Shirt.shirtContrastInsideCuff        = false;
	        Shirt.shirtContrastOutsideCuff        = false;
	        Shirt.shirtContrastInsidePlacket    = false;
	        Shirt.shirtContrastOutsidePlacket    = false;
			Shirt.shirtContrastBackOfCuff    = false;
	        Shirt.shirtContrastCollarStand	    = false;
			
		//	Shirt.shirtContrastCheck	= false;
			Shirt.shirtButtonsAndThreadsContrastCheck	= false;
	
			Shirt.shirtBackStyle          = dsRegistry.getDatasource('shirtBackStyleDS').getStore()[0];
			Shirt.shirtBottomStyle        = dsRegistry.getDatasource('shirtBottomStyleDS').getStore()[0];
			Shirt.shirtCollarStyle        = dsRegistry.getDatasource('shirtCollarStyleDS').getStore()[0];
			Shirt.shirtCuffsStyle         = dsRegistry.getDatasource('shirtCuffsStyleDS').getStore()[0];
			Shirt.shirtSleeveStyle        = dsRegistry.getDatasource('shirtSleeveStyleDS').getStore()[0];
			Shirt.shirtFrontStyle         = dsRegistry.getDatasource('shirtFrontStyleDS').getStore()[0];
			Shirt.shirtPocketStyle        = dsRegistry.getDatasource('shirtPocketStyleDS').getStore()[0];
			Shirt.shirtPocketPosition     = "Left";
			Shirt.shirtDarts              = false;
			Shirt.shirtSeams              = false;
			Shirt.shirtEpaulletes         = false;
			Shirt.shirtShortCuffs         = false;
			
			Shirt.shirtButtonColor        = dsRegistry.getDatasource('shirtButtonsDS').getStore()[0];
			Shirt.shirtThreadColor        = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Shirt.shirtFit        		  = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2]
			Shirt.shirtMonogram           = '';
			Shirt.shirtMonogramPosition   = '';
			Shirt.ShirtMonogramStitchColour   = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Shirt.ShirtMonogramFont   = dsRegistry.getDatasource('shirtMonogramFontDS').getStore()[0];
			Shirt.custom     =    new CustomProperties();
			
			Shirt.urgent = 0; 
		//	Shirt.urgentDate = '';
			Shirt.DOP_day = '';
			Shirt.DOP_month = '';
			Shirt.DOP_date = '';
			
			Shirt.ShirtCustomerFabric = {};
			Shirt.ShirtHasCustomerFabric = false;
			Shirt.GarmentPrice = 0;
	
			var iShirt = {
				Shirt: []
			};
	
			iShirt.Shirt.push(Shirt);
			this._super(name, iShirt, dsRegistry);
			
		}else{
			var iShirt = {
				Shirt: []
			};
//console.log("NUMBER OF SHIRTS: " + count);
			
			if(olddata.length > count){
				for(var x = count; x < olddata.length; x++){
					orderItem.removeFabric('Shirt', 4, 'Shirt ' + (x + 1), olddata[ x ].ShirtCustomerFabric, olddata[ x ].variantId, 2);
				}
			}
			for(var x = 0; x < count; x++){
				//var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
				if(olddata[x] != undefined /*&& olddata[x].ShirtPlatinumTreatment == undefined*/){
					var Shirt = {};
					Shirt.variantId         = x;
					Shirt.ShirtNotes                = olddata[x].ShirtNotes;
					if(olddata[x].ShirtPlatinumTreatment != undefined){
						Shirt.ShirtPlatinumTreatment	= olddata[x].ShirtPlatinumTreatment;
					}else{
						Shirt.ShirtPlatinumTreatment = true;
					}	
					Shirt.ShirtFabricNotes			= olddata[x].ShirtFabricNotes;
					Shirt.ShirtSleevesAndCuffsNotes	= olddata[x].ShirtSleevesAndCuffsNotes;
					Shirt.ShirtCollarNotes			= olddata[x].ShirtCollarNotes;
					Shirt.ShirtFrontBottomNotes		= olddata[x].ShirtFrontBottomNotes;
					Shirt.ShirtBackNotes			= olddata[x].ShirtBackNotes;
					Shirt.ShirtPocketsNotes			= olddata[x].ShirtPocketsNotes;
					Shirt.ShirtContrastsNotes		= olddata[x].ShirtContrastsNotes;
					Shirt.ShirtMonogramNotes		= olddata[x].ShirtMonogramNotes;
					Shirt.shirtFabric               = olddata[x].shirtFabric;
					Shirt.shirtContrastFabric		= olddata[x].shirtContrastFabric;//df;
					Shirt.shirtContrastInsideCollar   = olddata[x].shirtContrastInsideCollar;
			        Shirt.shirtContrastOutsideCollar  = olddata[x].shirtContrastOutsideCollar;
			        Shirt.shirtContrastInsideCuff     = olddata[x].shirtContrastInsideCuff;
			        Shirt.shirtContrastOutsideCuff    = olddata[x].shirtContrastOutsideCuff;
			        Shirt.shirtContrastInsidePlacket  = olddata[x].shirtContrastInsidePlacket;
			        Shirt.shirtContrastOutsidePlacket = olddata[x].shirtContrastOutsidePlacket;
					Shirt.shirtContrastBackOfCuff     = olddata[x].shirtContrastBackOfCuff;
			        Shirt.shirtContrastCollarStand	  = olddata[x].shirtContrastCollarStand;
					
			//		Shirt.shirtContrastCheck	= olddata[x].shirtContrastCheck;
					Shirt.shirtButtonsAndThreadsContrastCheck	= olddata[x].shirtButtonsAndThreadsContrastCheck;
			
					Shirt.shirtBackStyle          = olddata[x].shirtBackStyle;
					Shirt.shirtBottomStyle        = olddata[x].shirtBottomStyle;
					Shirt.shirtCollarStyle        = olddata[x].shirtCollarStyle;
					Shirt.shirtCuffsStyle         = olddata[x].shirtCuffsStyle;
					Shirt.shirtSleeveStyle        = olddata[x].shirtSleeveStyle;
					Shirt.shirtFrontStyle         = olddata[x].shirtFrontStyle;
					Shirt.shirtPocketStyle        = olddata[x].shirtPocketStyle;
					Shirt.shirtPocketPosition     = olddata[x].shirtPocketPosition;
					Shirt.shirtDarts              = olddata[x].shirtDarts;
					Shirt.shirtSeams              = olddata[x].shirtSeams;
					Shirt.shirtEpaulletes         = olddata[x].shirtEpaulletes;
					Shirt.shirtShortCuffs         = olddata[x].shirtShortCuffs;
					
					Shirt.shirtButtonColor        = olddata[x].shirtButtonColor;
					Shirt.shirtThreadColor        = olddata[x].shirtThreadColor;
					Shirt.shirtFit        		  = olddata[x].shirtFit;
					Shirt.shirtMonogram           = olddata[x].shirtMonogram;
					Shirt.shirtMonogramPosition   = olddata[x].shirtMonogramPosition;
					Shirt.ShirtMonogramStitchColour   = olddata[x].ShirtMonogramStitchColour;
					Shirt.ShirtMonogramFont   = olddata[x].ShirtMonogramFont;
					if(olddata[x].custom){
						Shirt.custom      		= olddata[x].custom;
					}
					else{
						Shirt.custom     =    new CustomProperties();
					}

					Shirt.urgent = olddata[x].urgent; 
				//	Shirt.urgentDate = olddata[x].urgentDate;
					Shirt.DOP_day = olddata[x].DOP_day;
					Shirt.DOP_month = olddata[x].DOP_month;
					Shirt.DOP_date = olddata[x].DOP_date;
					
					Shirt.ShirtCustomerFabric = olddata[x].ShirtCustomerFabric;
					Shirt.ShirtHasCustomerFabric = olddata[x].ShirtHasCustomerFabric;
					Shirt.GarmentPrice = olddata[x].GarmentPrice;
			
					//var iShirt = {
					//	Shirt: []
					//};			
					iShirt.Shirt.push(Shirt);
					//this._super(name, iShirt, dsRegistry);
				}else{
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var Shirt = {};
					Shirt.variantId         = x;
					Shirt.ShirtNotes                = '';
					Shirt.ShirtPlatinumTreatment	= true;
					Shirt.ShirtFabricNotes			= '';
					Shirt.ShirtSleevesAndCuffsNotes	= '';
					Shirt.ShirtCollarNotes			= '';
					Shirt.ShirtFrontBottomNotes		= '';
					Shirt.ShirtBackNotes			= '';
					Shirt.ShirtPocketsNotes			= '';
					Shirt.ShirtContrastsNotes		= '';
					Shirt.ShirtMonogramNotes		= '';
					
					Shirt.shirtFabric                = df;
			
					Shirt.shirtContrastFabric	= '';//df;
					Shirt.shirtContrastInsideCollar    = false;
			        Shirt.shirtContrastOutsideCollar    = false;
			        Shirt.shirtContrastInsideCuff        = false;
			        Shirt.shirtContrastOutsideCuff        = false;
			        Shirt.shirtContrastInsidePlacket    = false;
			        Shirt.shirtContrastOutsidePlacket    = false;
					Shirt.shirtContrastBackOfCuff    = false;
			        Shirt.shirtContrastCollarStand	    = false;
					
			//		Shirt.shirtContrastCheck	= false;
					Shirt.shirtButtonsAndThreadsContrastCheck	= false;
			
					Shirt.shirtBackStyle          = dsRegistry.getDatasource('shirtBackStyleDS').getStore()[0];
					Shirt.shirtBottomStyle        = dsRegistry.getDatasource('shirtBottomStyleDS').getStore()[0];
					Shirt.shirtCollarStyle        = dsRegistry.getDatasource('shirtCollarStyleDS').getStore()[0];
					Shirt.shirtCuffsStyle         = dsRegistry.getDatasource('shirtCuffsStyleDS').getStore()[0];
					Shirt.shirtSleeveStyle        = dsRegistry.getDatasource('shirtSleeveStyleDS').getStore()[0];
					Shirt.shirtFrontStyle         = dsRegistry.getDatasource('shirtFrontStyleDS').getStore()[0];
					Shirt.shirtPocketStyle        = dsRegistry.getDatasource('shirtPocketStyleDS').getStore()[0];
					Shirt.shirtPocketPosition     = "Left";
					Shirt.shirtDarts              = false;
					Shirt.shirtSeams              = false;
					Shirt.shirtEpaulletes         = false;
					Shirt.shirtShortCuffs         = false;
					
					Shirt.shirtButtonColor        = dsRegistry.getDatasource('shirtButtonsDS').getStore()[0];
					Shirt.shirtThreadColor        = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Shirt.shirtFit        		  = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[2]
					Shirt.shirtMonogram           = '';
					Shirt.shirtMonogramPosition   = '';
					Shirt.ShirtMonogramStitchColour   = dsRegistry.getDatasource('pipingColorDS').getStore()[1];
					Shirt.ShirtMonogramFont   = dsRegistry.getDatasource('shirtMonogramFontDS').getStore()[0];
					Shirt.custom     =    new CustomProperties();
					
					Shirt.urgent = false; 
				//	Shirt.urgentDate = '';
					Shirt.DOP_day = '';
					Shirt.DOP_month = '';
					Shirt.DOP_date = '';
					Shirt.ShirtCustomerFabric = {};
					Shirt.ShirtHasCustomerFabric = false;
					Shirt.GarmentPrice = 0;
					
					//var iShirt = {
					//	Shirt: []
					//};
			
					iShirt.Shirt.push(Shirt);
					//this._super(name, iShirt, dsRegistry);
				}	
				
			}
			this._super(name, iShirt, dsRegistry);
		}	
	} 
});


//END DEFINE CLOSURE
});