define(['jquery', 'knockout', 'base'], function($, ko) {
    
    /**
     * This class will receive the customers ids that we will use for the
     * order process.
     */
    PendingGroupOrdersVM = SimpleControl.extend({


    	init: function() {

    		var self = this;

    		console.log("Init pendingGroupOrdersVM...");

    		this.orders = ko.observableArray([]);
    		this.getPendingOrdersAjax();

    		this.pendingOrderSteps = this.getPendingGroupOrderSteps();

    		// Keep track of the current step that the salesman is in at any given moment
    		this.currentStep = ko.observable(this.pendingOrderSteps[0]); 

    		// Selected order to peform the edition
    		this.currentOrder = ko.observable(false);

    		// current customer of the currentOrder for edition page
    		this.currentCustomer = ko.observable(false);

    		this.cities = dsRegistry.getDatasource('citiesDS').getStore();
    		this.states = dsRegistry.getDatasource('statesDS').getStore();
    		this.countries = dsRegistry.getDatasource('countriesDS').getStore();
    		this.referal_methods_list = ko.observableArray([{"name":"Google Search","id":"1"},{"name":"Bing Search","id":"8"},{"name":"Yahoo Search","id":"9"},{"name":"Print Advertisement","id":"2"},{"name":"TV Ad","id":"3"},{"name":"Radio Ad","id":"4"},{"name":"Billboard","id":"5"},{"name":"Other","id":"6"},{"name":"Referral","id":"7"}]);
    	
        },

        getPendingOrdersAjax : function() {

        	var self = this;

        	$.ajax({
        	    type: 'POST',
        	    timeout: 60000, // sets timeout to 60 seconds
        	    url: BUrl + 'orders_pos/get_pending_group_orders',
        	    dataType: 'json',
        	    data: { 
        	        "user": authCtrl.userInfo
        	    },
        	    success: function (dataS) {
        	        
        	    	console.log("get_pending_group_orders", dataS);

        	    	for(let order of dataS.orders)
        	    	{	
        	    		// Transform each customer in an observable
        	    		order.customers = order.customers.map( customer => { 

                            const mappedCustomer = ko.mapping.fromJS(customer);

                            // Create the NON observable value of is_present to know the initial value
                            // of the variable
                            mappedCustomer.is_present_initial_value = mappedCustomer.is_present();

                            return mappedCustomer;
                        });

        	    		order.leader = order.customers.filter( e => e.is_leader())[0];


        	    		self.orders.push(order);
        	    	}

        	    },
        	    error: function (error) {
        	        console.log(JSON.stringify(error));
        	        customAlert("TIME OUT - there is a network issue. Please try again later"); 
        	    },
        	    async: false
        	});
        },

        getPendingGroupOrderSteps : function() {

    		return [
    			 	{
                        'name' : 'orderSelection',
                        'id' : '1'
                    },
                    {
                        'name' : 'customerList',
                        'id' : '2'
                    },
                    {
                        'name' : 'customerDetails',
                        'id' : '3'
                    }
    			];
    	},

    	/**
    	 * Selects an order from the order list and goes to the seccond step
    	 * @return {[type]} [description]
    	 */
    	selectOrder : function(order) {

    		console.log("selecting order");
    		this.currentOrder(order);
    		this.currentCustomer(order.customers.filter( e => !e.is_present() && !e.is_leader() )[0]);
    		// Goes to the second step
    		this.currentStep(this.pendingOrderSteps.filter(e => e.id == '2')[0]);


    		$(".pre-order-step:visible").effect("slide", { direction: 'right' }, 500);
    	},

    	nextStep : function() {

    		let nextStep;

    		let requirements = null;

    		switch(this.currentStep().id)
    		{
    			case '1':
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '2')[0];
    			break;

    			case '2':
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '3')[0];
    			break;
    		}

    		this.currentStep(nextStep);

    		$(".pre-order-step:visible").effect("slide", { direction: 'right' }, 500);
    	},

    	lastStep : function() {

    		let nextStep;

    		let requirements = null;

    		switch(this.currentStep().id)
    		{
    			case '2':
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '1')[0];
    			break;

    			case '3':
    				nextStep = this.pendingOrderSteps.filter(e => e.id == '2')[0];
    			break;
    		}

    		this.currentStep(nextStep);

    		$(".pre-order-step:visible").effect("slide", { direction: 'left' }, 500);
    	},

    	selectDetailInfoStepCurrentCustomer : function(customer) {

    		if(!customer.is_present())
    		{
    			customAlert("You cannot select this customer -- He is not present !");
    			return;
    		}

    		console.log("customer", customer);
    		this.currentCustomer(customer);
    		$(".datepicker").datepicker();

    	},

    	proceedToOrder : function() {
    		
    		if(!confirm('Proceed to order ?')) { return ;}

            const customers_ids = this.currentOrder().customers.map( customer => customer.customer_id() );
            const leader = this.currentOrder().customers.filter( customer => customer.is_leader() )[0];
            const order_ids = this.currentOrder().customers.map( customer => customer.order_id() );

            /**
             * Information to be sent to ERP and recover the order data in order to 
             * recreate the order instance and finish the order.
             * @type {Object}
             */
            const reorder_data = {
                    'orders_ids' : order_ids,
                    'customers_data' : ko.mapping.toJS(this.currentOrder().customers)
                };


            const groupOrdersData = {   
                    'customers_ids'  : customers_ids,
                    'leader_id'      : leader.customer_id(),
                    'customers_data' : undefined, // no need to send this here, the information is all in ERP already
                    'leaderPic'      : leader.image(),
                    'group_name'     : leader.group_name(),
                    'is_reorder'     : true,
                    'reorder_data'   : reorder_data
                };
            /**
             * Writing the data in the local store, so we can retrieve it when we initiate the group orders 
             * process
             */
            localStorage.setItem('groupOrdersData' , JSON.stringify(groupOrdersData) );
            posChangePage('#orders');


            // Sends the new customer data for the selcted order to ERP
            this.updateCustomerData()
    	},

        /**
         * Updates the customer data for the forms
         * @return {[type]} [description]
         */
        updateCustomerData : function() {

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/update_customers_data',
                dataType: 'json',
                data: { 
                    "user": authCtrl.userInfo,
                    "customers": ko.mapping.toJSON(this.currentOrder().customers)
                },
                success: function (ret) {
                    
                    console.log(ret);
               
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                },
                async: true
            });


        }


    });

});