define(['jquery', 'knockout', 'base'], function($, ko) {
    AlterationMainPageVM = SimpleControl.extend({
        init: function() {
            alterationsList = new AlterationsListVM();
        },

        loadBrieffingPage() {
            fittingPreview = new FittingPreviewVM();
            alterationsList.callback = fittingPreview;
            alterationsList.mode = 1;
            alterationsList.endpoint = "client_alterations_new/alterations_brieffing_endpoint";
            alterationsList.loadInfo();
            alterationsList.loadPage();
        },

        loadAlterationReportPage() {
            alterationReport = new AlterationReportVM();
            alterationsList.callback = alterationReport;
            alterationsList.mode = 2;
            alterationsList.endpoint = "client_alterations_new/alterations_endpoint2";
            alterationsList.loadInfo();
            alterationsList.loadPage();
        }
    });
});