/**
 * uma função global que pode ser usada para pegar o preview do current garment no group order
*/
__getPreview = function(garment_uniqueId=orders.selectedCustomer().garmentsDesignStep.selectedGarment().unique_id) {


    console.log('garment_uniqueId', garment_uniqueId);

    var allGarments = orders.selectedCustomer().garmentsStep.garments();

    
    var result = ko.observableArray();



    // search garment in all garments array
    for (const garmentTypes of allGarments) {
        for (const garment of garmentTypes.garments()) {
            if (garment.unique_id == garment_uniqueId) {
                for (const category of garment.categories) {
                    for (const option of category.options) {
                        let optionName = option.name;

                        // Custom data (images)
                        if (category.name.toLowerCase() == "custom data") {
                            // do not show in portfolio
                            continue;
                        }

                        // special case: fabric is stored in another place
                        if (optionName.toLowerCase() == 'fabric') {

                            result.push([optionName, garment.fabricInsearch()]);

                        } else {
                            // if value was selected
                            if (option.selectedValue()) {
                                // if selected value is object
                                let optionValue = option.selectedValue().name == null ? option.selectedValue() : option.selectedValue().name;
                                result.push([optionName, optionValue]);

                            }

                        }


                    }
                }
                return result;
            }
        }
    }
}