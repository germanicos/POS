

define(['jquery', 'knockout', 'base', 'login'], function ($, ko) {



	OrderItem = SimpleControl.extend({
		init: function (DsRegistry) {
			console.log('OrderItem init');
			console.log("authCtrl.userInfo.user_id: " + authCtrl.userInfo.user_id);
			var that = this;


			this._super(DsRegistry);



			this.customersVMmirror = customersVM;
			
			//this.customerOrdersVMmirror = customerOrdersVM;
			// review stuff

			this.garment_comments_jacket = ko.observableArray([]);
			this.garment_comments_shirt = ko.observableArray([]);
			this.garment_comments_pants = ko.observableArray([]);
			this.garment_comments_vest = ko.observableArray([]);
			var that = this;

			this.companyinvoice = ko.observable(false);
			this.invoicecompanyname = ko.observable('');
			this.invoiceproductdescription = ko.observable('');

			this.send_invoice_email_checkbox = ko.observable(false);

			this.urgent = ko.observable(false);
			this.future = ko.observable(false);
			this.acceptTerms = ko.observable(false);
			this.sample = ko.observable(false);
			this.sampletext = ko.observable('');
			this.reviewNotes = ko.observable('');
			this.deposit = ko.observable(0);
			this.alterDeposit = ko.observable(true);
			this.deposit.subscribe(function (data) {
				try {
					that.alterDeposit(false);
					modifyDepositValue(data);
				} catch (e) {

				}
			});
			this.discountCode = ko.observable('');
			this.extracost = ko.observable(0);
			this.subtotal = ko.observable(0);
			this.tax = ko.observable(0);
			this.purchaseTotal = ko.observable(0);
			this.purchaseTotal.subscribe(function () {
				if (that.alterDeposit()) {
					var deposit = Number(data) / 2;
					that.deposit(deposit.round(2));
					that.alterDeposit(true);
				}
			});
			this.extracosttext = ko.observable('');
			this.cities = ko.observableArray([]);//dsRegistry.getDatasource('citiesDS').getStore());
			this.states = ko.observableArray([]);//(dsRegistry.getDatasource('statesDS').getStore());
			this.fitting_city = ko.observable('');
			this.order_city = ko.observable('');
			this.paymentMethodList = ko.observableArray(["Credit card V/M", "Credit card Amex", "Bank transfer", "Eftpos", "Cheque", "Cash"]);
			this.bankList = ko.observableArray(["ANZ", "COM", "STG"]);
			this.paymentMethod = ko.observable('');
			this.bank = ko.observable('');
			this.referer = ko.observable('');
			this.creditCardImage = ko.observable('');
			this.jacketfirsttime = 0;
			this.pantsfirsttime = 0;
			this.suitfirsttime = 0;
			this.vestfirsttime = 0;
			this.shirtfirsttime = 0;
			//this.fitlinesJacket	= ko.observableArray(dsRegistry.getDatasource('fitlinesJacketDS').getStore());
			//this.fitlinesPants	= ko.observableArray(dsRegistry.getDatasource('fitlinesPantsDS').getStore());
			this.newcustomerorder = false;
			this.signature = "";
			this.costanalysis = ko.observable('');
			this.fitlinevideo = ko.observable('');
			//this.fitlinevideonotes = ko.observable('');
			this.garmentvideo = ko.observable('');
			//this.garmentvideonotes = ko.observable('');
			this.customervideo = ko.observable('');
			//this.customervideonotes = ko.observable('');
			this.videonotes = ko.observable('');

			this.measurementserrorcheck = 0;
			this.mediaerrorcheck = 0;
			this.lockselects = true;	// THIS IS USED TO "LOCK" THE SELECTS OF THE EXTRA PANTS DURING INITIALISATION 

			this.PendingDigestDataCount = 0; // this is used when going in shipments->pending, so that it won't load the page multiple times.
			this.ShippingNotificationsTotal = ko.observable(this.dsRegistry.getDatasource('ShippingNotificationsDS').getStore()[0]);

			$.ajax({
				type: 'POST',
				url: BUrl + 'fittings/get_garment_comments_endpoint',
				dataType: 'text',
				async: true,
				success: function (dataS) {

					var json = JSON.parse(dataS);

					that.garment_comments_jacket(json["jacket_comments"]);
					that.garment_comments_shirt(json["shirt_comments"]);
					that.garment_comments_pants(json["pants_comments"]);
					that.garment_comments_vest(json["vest_comments"]);

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log("fitting comments endpoint cannot be reached! ");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
				}
			});

			this.ExtraCostViewModel = function () {
				var self = this;
				self.extracosttext = ko.observable('');
				self.extracost = ko.observable();
			};

			this.extracosts = ko.observableArray();// ko.observableArray([new this.ExtraCostViewModel()]); 

			this.extracoststotal = function () {
				var total = 0;
				for (var pos = 0; pos < this.extracosts().length; pos++) {
					if (this.extracosts()[pos].extracosttext().length > 0 && (this.extracosts()[pos].extracost() * 1.0) >= 0) {
						total += this.extracosts()[pos].extracost() * 1.0;
					}
				}
				return total;
			};

			this.addExtraCost = function () {
				var checkfailed = false;
				for (var pos = 0; pos < this.extracosts().length; pos++) {
					if (this.extracosts()[pos].extracosttext().length == 0 || (this.extracosts()[pos].extracost() * 1.0) <= 0) {
						checkfailed = true;
						break;
					}
				}
				if (checkfailed == false) {
					this.extracosts.push(new this.ExtraCostViewModel());
				}
			};

			this.removeExtraCost = function (pos) {
				this.extracosts.remove(that.extracosts()[pos]);
			};



			this.acceptTermsUpdate = function () {

				text = "<div><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Terms and Conditions </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">&ldquo;</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Customer&rdquo; </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">means any person who is purchasing items from the Supplier. </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">&ldquo;</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Supplier&rdquo; </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">means </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Germanicos Bespoke Tailors</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">, Australia. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Germanicos may at any time modify any relevant terms and conditions, policies or notices. You acknowledge that by visiting the website from time to time, you shall become bound to the current version of the relevant terms and conditions (the \"current version\") and, unless stated in the current version, all previous versions shall be superseded by the current version. You shall be responsible for reviewing the then current version each time you visit the website. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Lead Time &amp; Delivery of Product </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">It is understood that turnaround and dispatch of the orders placed at the Supplier may vary from time to time depending on current order volumes, work schedules, fabric availability or a particular characteristic of the order placed. The Supplier will try its best to comply with the average turnaround of between 4 and 6 weeks. This is an estimated time frame of delivery, the supplier is not liable for any delays. For guaranteed delivery time lines, we have the </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Express Service</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Express Service - </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Supplier guarantees delivery if a customer opts for this service. The Express Service has an additional fee of $300 AUD. This service will guarantee delivery between 2.5 to 4 weeks. If a customer chooses to pay the express fee, supplier guarantees delivery in the said time frame. We try our best to get the products in time while maintaining the quality, however if for whatever reason we are unable to deliver the product in time, a full refund can be applied upon request. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">Express Service </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">has a single fitting policy due to time constrain</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">ts from the customer&rsquo;s side. If </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">there is any alteration to be done to the garments, the customer can always bring the garments at a later stage for any alteration. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Payment Terms </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">As we have to order fabrics for the orders placed with us, we will take a 50% deposit of the total amount and the balance to be paid by the customer on completion or when he/she picks up the garments and/or before we deliver/ship the garments to the customer. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Modes of Payment </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">We accept the following credit cards (Visa, MasterCard &amp; AMEX), EFTPOS and Debit card payments. Customers can also do Bank Transfers (Applicable only for deposits and not for final payments) For all Bank Transfer payments the lead time commences from the time we have clear funds in our account and not from the time of order or when a customer sees us or transfers the money from their account. </span></p></div><div title=\"Page 2\"><div><div><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Design and Style </span></p></div></div><div><div><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">By accepting these terms and conditions on the POS system or otherwise, the customer agrees that he/she is happy with all the design options chosen while placing the order with us. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Once Design &amp; Style options are chosen they cannot be changed after production commences - Approximately 7 days after initial order. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Adjustments </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS,Bold';\">100% fit and satisfaction policy </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">After your first appointment with Germanicos, a fitting will be organised once the garment is constructed. During this fitting the tailor will determine with you, whether any adjustments are required. Any adjustments that need to be made are conducted in-house at no cost to the client. We will do as many fittings as needed to get the garments 100% fit. In the rare case that a garment cannot be adjusted, Germanicos will organise for the client to be re-measured and the garment will be re-made. Once again, there is no cost to the client undergoing this process. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Change in Body Measurement </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">As all our products are bespoke, we take various body measurements at the time of new order and make the garments accordingly. However, if the customer fluctuates more than 2 inches (from the time he has placed the order to 1st fitting); Germanicos will try its best to accommodate such changes and offer the best fit for the customer. Germanicos cannot be held liable for garments not fitting perfectly if severe body fluctuations occur. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">GST &amp; Taxes </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">The Australian GST tax rate applies to all orders delivered within Australia. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">International orders are exempt from Australian GST (10%); however they may incur taxes and duties applied by customs in the country where the order is delivered. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Any duties and taxes incurred in the country of destination are the responsibility of the customer. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">If you require fur</span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">ther information about your country&rsquo;s taxes and duties you will need to contact </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">your local customs office directly. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">To comply with Australian export regulations, we are required to declare the exact value of all </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">items ordered and to mark them as dutiable &lsquo;merchandise&rsquo;. We are also prohibited by law from marking the order as a &lsquo;gift&rsquo;, even if order is placed with the intention of sending to a gift </span><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">recipient. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Cancellation </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">As all our products are bespoke, we do not refund or replace for Change of Mind. However, if the order is cancelled within 24 hours from the time order placed and before the supplier has ordered fabric / started the process of making the product, a cancellation fee of $ 100 AUD is applicable which will be charged / deducted from the deposit paid by the customer. </span></p></div></div></div><div title=\"Page 3\"><div><div><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Terms and Conditions of Gift Voucher Purchase &amp; Use </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">1. Germanicos gift vouchers can be exchanged for goods in all Germanicos studios. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">2. Gift vouchers may not be exchanged for cash. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">3. Gift vouchers are valid for 12 months from date of purchase. </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">4. If goods purchased value is less than the value of the gift voucher, any balance will be left as a credit for you with Germanicos, and will be redeemed against subsequent future orders. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Trade Marks </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">The trade marks, names, logos and service marks (collectively \"trademarks\") displayed on this website are registered and unregistered trademarks of the Website Owner. Nothing contained on this website should be construed as granting any licence or right to use any trade mark without the prior written permission of the Website Owner. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Trebuchet MS,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Legal Jurisdiction </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">All our customer contracts are governed by courts of Victoria, Australia and shall fall under the exclusive jurisdiction of Victorian Courts in Australia. English is the official language offered for the conclusion of the contract. </span></p><p><span style=\"font-size: 12.000000pt; font-family: 'Times New Roman,Bold'; color: rgb(100.000000%,0.000000%,0.000000%);\">Disclaimer </span></p><p><span style=\"font-size: 10.000000pt; font-family: 'Trebuchet MS';\">Germanicos strives to give you accurate content, including product information, policies, pricing and visual displays. Policies, pricing, and item availability are subject to change without prior notice. Prices displayed are subject to change and final approval at the time of order fulfilment. Images are shown for representational purposes only. In the event Germanicos is unable to provide an actual image of a product, they reserve the right to substitute a similar image for the one described in the product information. While Germanicos tries in good faith to make sure that the information displayed is accurate, they are not responsible for typographical errors or technical inaccuracies.&nbsp;</span></p></div></div></div>";
				document.getElementById("termstext").innerHTML = text;

				terms = document.getElementById('terms');
				terms.style.display = "block";
				/*
				if (!confirm(text)) {
					that.acceptTerms(false);
					return false;
				}
				that.acceptTerms(true);
				*/
				return true;
			};

			this.buttonNoAcceptTerms = function () {
				that.acceptTerms(false);
				terms = document.getElementById('terms');
				terms.style.display = "none";
			}
			this.buttonYesAcceptTerms = function () {
				that.acceptTerms(true);
				terms = document.getElementById('terms');
				terms.style.display = "none";
			}


			this.prepareGarments = function () {

				// defined in index.htm => this will bind the pop up with the click in the fabric image
				bindFabricImageClickEvent();



				//  this.numberOfPant = ko.observable(true);
				numberOfPant = ko.observable("");
				numberOfJacket = ko.observable("");
				numberOfSuit = ko.observable("");
				numberOfShirt = ko.observable("");
				numberOfVest = ko.observable("");
				//if((typeof orderItem.garmentsJacketVM.JacketData[0].JacketBottomStyle.id!='undefined')){
				//  }else{
				// if (typeof(window[orderItem.garmentsJacketVM]) == 'undefined') {
				if (typeof orderItem.garmentsJacketVM === "undefined" || orderItem.garmentsJacketVM == undefined || orderItem.garmentsJacketVM.jacketData == undefined || orderItem.garmentsJacketVM.jacketData[0] == undefined) {
					;
				} else {
					if (orderItem.garmentsJacketVM.JacketData[0].JacketFabric.id != 0 || orderItem.garmentsJacketVM.JacketData[0].JacketJacketStructure.id != 0 || orderItem.garmentsJacketVM.JacketData[0].JacketBottomStyle.id != 0
						|| orderItem.garmentsJacketVM.JacketData[0].JacketVentStyle.id != 0 || orderItem.garmentsJacketVM.JacketData[0].JacketLapelStyle.id != 0 || orderItem.garmentsJacketVM.JacketData[0].JacketPocketStyle.id != 0) {
						// numberOfJacket = ko.observable(1);  
						if (orderItem.garmentsJacketVM.JacketDataAID != 0) {
							var jacketclone = orderItem.garmentsJacketVM.JacketDataAID + 1;
							numberOfJacket = ko.observable(jacketclone);
						} else {
							numberOfJacket = ko.observable(1);
						}
					} else {
						numberOfJacket = ko.observable("");
					}
				}
				if (typeof orderItem.garmentsPantVM === "undefined" || orderItem.garmentsPantVM == undefined || orderItem.garmentsPantVM.pantData == undefined || orderItem.garmentsPantVM.pantData[0] == undefined) {
					;
				} else {
					if (orderItem.garmentsPantVM.PantData[0].pantPleats.id != 0 || orderItem.garmentsPantVM.PantData[0].PantFabric.id != 0  /* || orderItem.garmentsPantVM.PantData[0].pantPockets.id!=0 */ || orderItem.garmentsPantVM.PantData[0].beltLoopStyle.id != 0
						|| orderItem.garmentsPantVM.PantData[0].pantFit.id != 0 || orderItem.garmentsPantVM.PantData[0].pantPleats.id != 0 || orderItem.garmentsPantVM.PantData[0].bpantPockets.id != 0 || orderItem.garmentsPantVM.PantData[0].beltLoopStyle.id != 0) {
						//  numberOfPant = ko.observable(1);     
						//  this.numberOfPant = ko.observable(1);     
						if (orderItem.garmentsPantVM.PantDataAID != 0) {
							var pantclone = orderItem.garmentsPantVM.PantDataAID + 1;
							numberOfPant = ko.observable(pantclone);
						} else {
							numberOfPant = ko.observable(1);
						}
					} else {
						numberOfPant = ko.observable("");
					}
				}
				//  orderItem.OrderItemSelectGarmentVM.OrderItemSelectGarmentload(orderItem);
				if (typeof orderItem.garmentsSuitVM === "undefined" || orderItem.garmentsSuitVM == undefined || orderItem.garmentsSuitVM.suitData == undefined || orderItem.garmentsSuitVM.suitData[0] == undefined) {
					;
				} else {
					if (orderItem.garmentsSuitVM.suitData[0].suitFabric.id != 0 || orderItem.garmentsSuitVM.suitData[0].suitJacketStructure.id != 0 || orderItem.garmentsSuitVM.suitData[0].BottomStyle.id != 0 || orderItem.garmentsSuitVM.suitData[0].VentStyle.id != 0
						|| orderItem.garmentsSuitVM.suitData[0].LapelStyle.id != 0 || orderItem.garmentsSuitVM.suitData[0].PocketStyle.id != 0 || orderItem.garmentsSuitVM.suitData[0].ButtonStyle.id != 0 || orderItem.garmentsSuitVM.suitData[0].pantFit.id != 0
						|| orderItem.garmentsSuitVM.suitData[0].pantPleats.id != 0 /*|| orderItem.garmentsSuitVM.suitData[0].pantPockets.id!=0*/ || orderItem.garmentsSuitVM.suitData[0].bpantPockets.id != 0 || orderItem.garmentsSuitVM.suitData[0].beltLoopStyle.id != 0) {
						// numberOfSuit = ko.observable(1);      
						if (orderItem.garmentsSuitVM.suitDataAID != 0) {
							var Suitclone = orderItem.garmentsSuitVM.suitDataAID + 1;
							numberOfSuit = ko.observable(Suitclone);
						} else {
							numberOfSuit = ko.observable(1);
						}
					} else {
						numberOfSuit = ko.observable("");
					}
				}
				if (typeof orderItem.garmentsVestVM === "undefined" || orderItem.garmentsVestVM == undefined || orderItem.garmentsVestVM.vestData == undefined || orderItem.garmentsVestVM.vestData[0] == undefined) {
					;
				} else {
					if (orderItem.garmentsVestVM.vestData[0].vestFabric.id != 0 || orderItem.garmentsVestVM.vestData[0].vestNecklineStyle.id != 0 || orderItem.garmentsVestVM.vestData[0].vestBottomStyle.id != 0
						|| orderItem.garmentsVestVM.vestData[0].vestPocketStyle.id != 0) {
						// numberOfVest = ko.observable(1); 
						if (orderItem.garmentsVestVM.vestDataAID != 0) {
							var Vestclone = orderItem.garmentsVestVM.vestDataAID + 1;
							numberOfVest = ko.observable(Vestclone);
						} else {
							numberOfVest = ko.observable(1);
						}
					} else {
						numberOfVest = ko.observable("");
					}
				}

				if (typeof orderItem.garmentsShirtVM === "undefined" || orderItem.garmentsShirtVM == undefined || orderItem.garmentsShirtVM.shirtData == undefined || orderItem.garmentsShirtVM.shirtData[0] == undefined) {
					;
				} else {
					//if(orderItem.garmentsShirtVM.shirtData[0] == undefined){             
					if (orderItem.garmentsShirtVM.shirtData[0].shirtFabric.id != 0 || orderItem.garmentsShirtVM.shirtData[0].shirtCollarStyle.id != 0 || orderItem.garmentsShirtVM.shirtData[0].shirtBottomStyle.id != 0
						|| orderItem.garmentsShirtVM.shirtData[0].shirtPocketStyle.id != 0) {
						// numberOfVest = ko.observable(1); 
						if (orderItem.garmentsShirtVM.shirtDataAID != 0) {
							var Shirtclone = orderItem.garmentsShirtVM.shirtDataAID + 1;
							numberOfShirt = ko.observable(Shirtclone);
						} else {
							numberOfShirt = ko.observable(1);
						}
					} else {
						numberOfShirt = ko.observable("");
					}
				}
				//orderItem.populateGarments();
				var spinner = document.getElementById('loading_jp');
				var myVar = setTimeout(function () { spinner.style.display = "none"; }, 1500);

			};

			this.syncmeasurementsVM = function () {
				console.log("syncmeasurementsVM " + customersVM.systemMode);
				this.measurementsVM = new Measurements(dsRegistry, this);

				if (customersVM.systemMode == "shipping_copy_garment" || customersVM.systemMode == "shipping_remake_garments") {	// AFTER MEASUREMENTS HAVE BEEN LOADED AND systemMode == "shipping_copy_garment", CHANGE TO shipping_copy_garment
					document.getElementById('loading_jp').style.display = "block";
					if (customersVM.systemMode == "shipping_copy_garment") {
						$.mobile.pageContainer.pagecontainer("change", "shipping_copy_garment.htm");
						posChangePage('#shipping_copy_garment');
					} else {
						$.mobile.pageContainer.pagecontainer("change", "shipping_faulty_remake_garments.htm");
					}
					setTimeout(function () {
						document.getElementById('loading_jp').style.display = "none";
					}, 750);

				}
			};

			this.syncbodyshapeVM = function () {
				this.bodyshapeVM = new Bodyshape(dsRegistry);
			};

			this.syncfitlinesJacket = function () {
				this.fitlinesJacket = new FitlinesJacket(dsRegistry, this);
			};

			this.syncfitlinesPants = function () {
				this.fitlinesPants = new FitlinesPants(dsRegistry, this);
			};


			this.months = ko.observableArray([{
				"name": "January",
				"id": "1"
			}, {
				"name": "February",
				"id": "2"
			}, {
				"name": "March",
				"id": "3"
			}, {
				"name": "April",
				"id": "4"
			}, {
				"name": "May",
				"id": "5"
			}, {
				"name": "June",
				"id": "6"
			}, {
				"name": "July",
				"id": "7"
			}, {
				"name": "August",
				"id": "8"
			}, {
				"name": "September",
				"id": "9"
			}, {
				"name": "October",
				"id": "10"
			}, {
				"name": "November",
				"id": "11"
			}, {
				"name": "December",
				"id": "12"
			}]);
			this.days = ko.observableArray([]);
			for (i = 1; i <= 31; i++) this.days.push(i.toString());
			this.DOP_day = ko.observable('');
			this.DOP_month = ko.observable('');
			this.customer_DOP = ko.computed({                // date of payment
				read: function () {
					return this.DOP_month() + "-" + this.DOP_day();
				},
				write: function (DOP) {
					if (DOP) {
						DOP = DOP.split('-');
						this.DOP_month(DOP[1]);
						$('select[name="DOP_month"]').selectmenu('refresh');
						this.DOP_day(DOP[2]);
						$('select[name="DOP_day"]').selectmenu('refresh');
					}
				},
				owner: this
			});
			////////////////////
			this.depositinput = ko.computed({
				read: function () {
					return this.deposit();
				},
				write: function (value) {
					//if(this.deposit() < value){
					if (that.alterDeposit()) {
						this.deposit(value);
						that.alterDeposit(true);
					}

					//}	 
				},
				owner: this
			});

			this.FittingsListVM = new FittingList(DsRegistry);
			if (typeof orderItem === 'undefined' || orderItem.SyncFilesVM == undefined) {
				this.SyncFilesVM = new SynchronizeFiles(DsRegistry);
			}
			else {
				this.SyncFilesVM = orderItem.SyncFilesVM;
			}
			this.MediaManagerVM = new MediaManagerVM(DsRegistry);
			this.Notifications = new NotificationsVM(null);
			this.MediaTagManagerVM = new MediaTagManagerVM(null);

			console.log("instanciating customer class", dsRegistry);
			
			this.custSelectVM = new CustomerSelectClass(dsRegistry);
			this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
			this.custSelectVM.setParentVM(this);

			this.custOrdersVM = new CustomerOrdersClass(DsRegistry);
			this.custOrdersVM.subscribeTo('customerOrdersDS');
			this.custOrdersVM.setParentVM(this);

			this.custOrderVM = new CustomerOrderClass(DsRegistry);
			this.custOrderVM.subscribeTo('customerOrderDS');
			this.custOrderVM.setParentVM(this);

			this.custOrderPaymentsVM = new CustomerOrderPaymentsClass(DsRegistry);
			this.custOrderPaymentsVM.subscribeTo('customerOrderPaymentsDS');
			this.custOrderPaymentsVM.setParentVM(this);

			this.OrderPaymentVM = new OrderPaymentClass(DsRegistry);
			this.OrderPaymentVM.subscribeTo('OrderPaymentDS');
			this.OrderPaymentVM.setParentVM(this);

			// old way of fitting
			this.OrderFittingsVM = new OrderFittingsClass(DsRegistry);
			this.OrderFittingsVM.subscribeTo('FittingsDS');
			this.OrderFittingsVM.setParentVM(this);

			this.FittingVM = new FittingClass(DsRegistry);
			this.FittingVM.subscribeTo('FittingDS');
			this.FittingVM.setParentVM(this);

			// new way of fitting
			this.OrdersGarmentsForFittingVM = new OrdersGarmentsForFittingClass(DsRegistry);
			this.OrdersGarmentsForFittingVM.subscribeTo('OrdersGarmentsForFittingDS');
			this.OrdersGarmentsForFittingVM.setParentVM(this);
			////////
			this.AlterationsVM = new AlterationsClass(DsRegistry);
			this.AlterationsVM.subscribeTo('AlterationsDS');
			this.AlterationsVM.setParentVM(this);

			this.ShippingFabricVM = new ShippingFabric(DsRegistry);
			this.ShippingFabricVM.subscribeTo('ShippingFabricDS');
			this.ShippingFabricVM.setParentVM(this);

			this.ShippingCopyGarmentVM = new ShippingCopyGarment(DsRegistry);
			this.ShippingCopyGarmentVM.subscribeTo('ShippingCopyGarmentDS');
			this.ShippingCopyGarmentVM.setParentVM(this);

			this.ShippingFaultyRemakeGarmentsVM = new ShippingFaultyRemakeGarments(DsRegistry);
			this.ShippingFaultyRemakeGarmentsVM.subscribeTo('ShippingFaultyRemakeGarmentsDS');
			this.ShippingFaultyRemakeGarmentsVM.setParentVM(this);

			this.ShippingOtherVM = new ShippingOther(DsRegistry, this.custSelectVM);
			this.ShippingOtherVM.subscribeTo('ShippingOtherDS');
			this.ShippingOtherVM.setParentVM(this);

			this.ShippingDataVM = new ShippingData(DsRegistry);
			this.ShippingDataVM.subscribeTo('ShippingDataDS');
			this.ShippingDataVM.setParentVM(this);

			this.ShippingBoxVM = new ShippingBox(DsRegistry);
			this.ShippingBoxVM.subscribeTo('ShippingBoxDS');
			this.ShippingBoxVM.setParentVM(this);

			this.ShippingNewBoxVM = new ShippingBox(DsRegistry);
			this.ShippingNewBoxVM.subscribeTo('ShippingNewBoxDS');
			this.ShippingNewBoxVM.setParentVM(this);

			//media control
			this.MediaUploadVM = new MediaUploadVM(DsRegistry);
			this.callbackMedia = function (data) {
				that.mediaController.addMedia(that.MediaUploadVM.imagPath, that.MediaUploadVM.upPath, true);
			};
			//this.media = this.MediaUploadVM.media;
			// master cities do show first in list
			this.masterCities = [
				"melbourne",
				"sydney",
				"brisbane",
				"perth",
				"adelaide",
				"canberra"
				];

			this.cities_display = ko.computed(function () {
				this.cities = ko.observableArray(dsRegistry.getDatasource('citiesDS').getStore());

				// sort cities first by master city then by alfabetical order
				this.cities.sort((a, b) => {
					if (that.masterCities.includes(a.location_name.trim().toLowerCase())) {
						return -1;
					}
					else if (that.masterCities.includes(b.location_name.trim().toLowerCase())) {
						return 1;
					}
			
					else {
						return a.location_name.trim().toLowerCase() < b.location_name.trim().toLowerCase() ? -1 : 1;
					}
				});


				this.states = ko.observableArray(dsRegistry.getDatasource('statesDS').getStore());
				var stateslist = [];
				var newlist = [];
				if (this.custSelectVM.selectedCustomer().customer_country == undefined) {
					return this.cities();
				} else {
					for (i in this.states()) {
						if (this.states()[i].parent_location_id == this.custSelectVM.selectedCustomer().customer_country) {
							stateslist.push(this.states()[i]);
						}
					}
					for (i in this.cities()) {
						for (j in stateslist) {
							if (this.cities()[i].parent_location_id == stateslist[j].locations_id) {
								newlist.push(this.cities()[i]);
							}
						}
					}
					if (newlist.length == 0) {
						return this.cities();
					} else {
						return newlist;
					}
				}
			}, this);


			this.orderMediaNotes = ko.observable('');

			this.currentBigStep = ko.observable();

			this.bigSteps = ko.computed(function () {
				var sC = this.custSelectVM.selectedCustomer();
				console.log('bigsteps');
				customerTitle = ko.computed(function () {
					if (($.isEmptyObject(sC))) {
						return "Customer info";
					} else {
						if (customersVM.systemMode == "shipping_alterations" || customersVM.systemMode == "shipping_garments" || customersVM.systemMode == "shipping_fabrics") {
							;
						} else if (customersVM.systemMode == "shipping_faulty_remake_garments") {
							; // SYNC A NEW ENDPOINT FOR THE ORDERS' GARMENTS
						} else if (customersVM.systemMode == "shipping_copy_garment" || customersVM.systemMode == "shipping_remake_garments") {
							measurementsDS = new SyncableDatasource('measurementsDS', 'measurementsDS', dsRegistry, 'customer_measurements_endpoint');
							measurementsDS.sync();
						} else {
							measurementsDS = new SyncableDatasource('measurementsDS', 'measurementsDS', dsRegistry, 'customer_measurements_endpoint');
							measurementsDS.sync();

							bodyshapeDS = new SyncableDatasource('bodyshapeDS', 'bodyshapeDS', dsRegistry, 'customer_bodyshape_endpoint');
							bodyshapeDS.sync();
						}
						//		fitlinesJacketDS.sync();
						//		fitlinesPantsDS.sync();
						return sC.customer_first_name + ' ' + sC.customer_last_name;
					}
					// return ($.isEmptyObject(sC))?"Customer info":sC.customer_first_name + ' ' + sC.customer_last_name;
				}, this);

				return [
					{
						title: customerTitle(),
						id: 0
					},

					{
						title: "Select Garments",
						id: 1
					},

					{
						title: "Copy Garments",
						id: 8
					},

					{
						title: "Measurements",
						id: 2
					},

					{
						title: "Images",
						id: 3
					},

					{
						title: "Video",
						id: 7
					},

					{
						title: "Body Shape",
						id: 4
					},

					{
						title: "DESIGN",
						id: 5
					},

					{
						title: "Payment",
						id: 6
					}
				];
			}, this);



			this.garmentsList = new ko.observableArray([
				{
					id: 0,
					title: "Jacket",
					image: "http://shirt-tailor.net/thepos/appimg/measurements/jacket.png",
					active: false,
					count: ko.observable(0)
				},
				{
					id: 1,
					title: "Pant",
					image: "http://shirt-tailor.net/thepos/appimg//measurements/pants.png",
					active: false,
					count: ko.observable(0)
				},
				{
					id: 2,
					title: "Suit",
					image: "http://shirt-tailor.net/thepos/appimg/measurements/suit.png",
					active: false,
					count: ko.observable(0)
				},

				{
					id: 3,
					title: "Vest",
					image: "http://shirt-tailor.net/thepos/appimg/measurements/vest.png",
					active: false,
					count: ko.observable(0)
				},

				{
					id: 4,
					title: "Shirt",
					image: "http://shirt-tailor.net/thepos/appimg/measurements/shirt.png",
					active: false,
					count: ko.observable(0)
				}
			]);

			this.garments_choice = new ko.observableArray([
				{
					id: 0,
					pant: ko.observable()
				},

				{
					id: 1,
					pant: "Jacket"
				},
				{
					id: 2,
					pant: "Jacket"
				},
				{
					id: 3,
					pant: "Jacket"
				},
				{
					id: 4,
					pant: "Jacket"
				}

			]);


			this.addGarment = function (data) {

				//console.log('addGarment ' + JSON.stringify(data));			
				var ti = that.garmentsList.indexOf(data);
				//console.log('old count: ' + that.garmentsList()[ti].count());
				that.garmentsList()[ti].count(data.count() + 1);
				//console.log('new count: ' + that.garmentsList()[ti].count());        
			};

			this.removeGarment = function (data) {
				//console.log('removeGarment');						
				//console.log("data: " + data);
				//console.log("data.count():" + data.count());

				var ti = that.garmentsList.indexOf(data);
				//console.log(that.garmentsList()[ti].count());
				if (that.garmentsList()[ti].count() != 0) {
					that.garmentsList()[ti].count(data.count() - 1);
				}
				//console.log(that.garmentsList()[ti].count());
			};



			this.atLeastOneGarment = ko.computed(function () {
				//console.log('atLeastOneGarment');									
				for (var ind in that.garmentsList()) {
					if (that.garmentsList()[ind].count() != 0) {
						return true;
					}
				}
				return false;
			}, this);

			this.nextButtonMeasurements = function () {
				that.currentBigStep(that.bigSteps()[4]);
			};

			this.nextButtonBodyshape = function () {
				that.currentBigStep(that.bigSteps()[5]);
			}
			this.nextButtonMedia = function () {
				that.currentBigStep(that.bigSteps()[7]);
			};
			this.nextButtonCustomer = function () {
				that.currentBigStep(that.bigSteps()[1]);
			}

			this.nextBigStep = function () {
				var cur = jQuery.extend({}, that.currentBigStep());
				console.log("cur.id: " + cur.id);

				var check = 0;

				if (cur.id == 1) {

					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";

				} else if (cur.id == 2) {
					if (that.measurementserror().length > 0 && that.measurementserrorcheck == 0) {
						check = 1;
						that.measurementserrorcheck = 1;
						customAlert("Measurements missing: \n" + that.measurementserror());
					} else {
						check = 0;
						that.updateMeasurements();
					}
				} else if (cur.id == 3) {
					if (that.mediaerrorcheck == 0) {
						var counter = that.customerImage.getMedia()().length;
						if (counter < 3) {
							check = 1;
							that.mediaerrorcheck = 1;
							customAlert("You must upload at least 3 images!");
						}
					} else {
						check = 0;
					}
				}




				if (check == 0) {
					if (cur.id == 3) {
						cur.id = 7;
					}
					else if (cur.id == 7) {
						cur.id = 4;
					}
					else if (cur.id == 1 && that.copyGarment()) {
						cur.id = 8;
					}
					else if (cur.id == 8) {
						cur.id = 2;
					}
					else {
						cur.id = cur.id + 1;
					}
					that.currentBigStep(cur);
				}
			}


			this.selectGarmentYesCallbackFunction = null;
			this.selectGarmentNoCallbackFunction = null;
			this.openDialogSelectGarment = function (message, yesCallback, noCallback) {
				document.getElementById("dialogtext").innerHTML = message;
				document.getElementById("confirmDialog").style.display = "block";
				that.selectGarmentYesCallbackFunction = yesCallback;
				selectGarmentNoCallbackFunction = noCallback;
			};

			this.dialogSelectGarmentYesBtn = function () {
				document.getElementById("confirmDialog").style.display = "none";
				if (that.selectGarmentYesCallbackFunction) {
					that.selectGarmentYesCallbackFunction();
				}
			};

			this.dialogSelectGarmentNoBtn = function () {
				document.getElementById("confirmDialog").style.display = "none";
				if (that.selectGarmentNoCallbackFunction) {
					that.selectGarmentNoCallbackFunction();
				}
			};


			this.openpopup = function (index, element) {
				var popups = document.getElementsByName(element);
				for (var x = 0; x < popups.length; x++) {
					popups[x].style.display = "none";
				}
				popups[index].style.display = "block";
			}

			this.closepopup = function (index, element) {
				var popups = document.getElementsByName(element);
				for (var x = 0; x < popups.length; x++) {
					popups[x].style.display = "none";
				}
			}




			this.getSellerCurrency = function () {

				var cityid = authCtrl.userInfo.city;
				//console.log("citycode: " + cityid);
				var stateid = '';
				var countryid = '';
				var currencyid = '';
				var currency;


				cities = that.dsRegistry.getDatasource('citiesDS');
				if (cities != null) {
					for (var i = 0; i < cities.getStore().length; i++) {
						if (cities.getStore()[i].locations_id == cityid) {
							stateid = cities.getStore()[i].parent_location_id;
							break;
						}
					}
				}

				states = that.dsRegistry.getDatasource('statesDS');
				if (states != null) {
					for (var i = 0; i < states.getStore().length; i++) {
						if (states.getStore()[i].locations_id == stateid) {
							countryid = states.getStore()[i].parent_location_id;
							break;
						}
					}
				}

				countries = that.dsRegistry.getDatasource('countriesDS');
				if (countries != null) {
					for (var i = 0; i < countries.getStore().length; i++) {
						if (countries.getStore()[i].locations_id == countryid) {
							currencyid = countries.getStore()[i].currency_id;
							break;
						}
					}
				}

				currencies = that.dsRegistry.getDatasource('currenciesDS');
				if (currencies != null) {
					for (var i = 0; i < currencies.getStore().length; i++) {
						if (currencies.getStore()[i].currency_id == currencyid) {
							currency = currencies.getStore()[i];
							break;
						}
					}
				}

				return currency;	// its a json {"currency_id":"5","title":"Australian Dollar","code":"AUD","symbol":"Α$","id":3}
			}


			Number.prototype.round = function (places) {
				places = Math.pow(10, places);
				return Math.round(this * places) / places;
			}


			this.checkGarmentsForFabrics = function () {
				if (that.garmentsList()[0].count() > 0) {
					for (i in that.dumpOrder().Jacket.Jacket) {
						if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.title == "none" && !that.dumpOrder().Jacket.Jacket[i].JacketHasCustomerFabric) {
							return false;
						}
					}
				}
				if (that.garmentsList()[1].count() > 0) {
					for (i in that.dumpOrder().Pant.Pant) {
						if (that.dumpOrder().Pant.Pant[i].PantFabric.title == "none" && !that.dumpOrder().Pant.Pant[i].PantHasCustomerFabric) {
							return false;
						}
					}
				}
				if (that.garmentsList()[2].count() > 0) {
					for (i in that.dumpOrder().Suit.suit) {
						if (that.dumpOrder().Suit.suit[i].suitFabric.title == "none" && !that.dumpOrder().Suit.suit[i].SuitHasCustomerFabric) {
							return false;
						}
					}
				}
				if (that.garmentsList()[3].count() > 0) {
					for (i in that.dumpOrder().Vest.vest) {
						if (that.dumpOrder().Vest.vest[i].vestFabric.title == "none" && !that.dumpOrder().Vest.vest[i].VestHasCustomerFabric) {
							return false;
						}
					}
				}
				if (that.garmentsList()[4].count() > 0) {
					for (i in that.dumpOrder().Shirt.Shirt) {
						if (that.dumpOrder().Shirt.Shirt[i].shirtFabric.title == "none" && !that.dumpOrder().Shirt.Shirt[i].ShirtHasCustomerFabric) {
							return false;
						}
					}
				}
				return true;
			}


			this.kissingButtonsExtraCost = function () {
				currency = that.getSellerCurrency();
				var pricesarray = [];
				pricerange = that.dsRegistry.getDatasource('pricerangeDS');
				if (pricerange != null) {
					for (var i = 0; i < pricerange.getStore().length; i++) {
						if (pricerange.getStore()[i].price_range_currency == currency.code) {
							pricesarray.push(pricerange.getStore()[i]);
						}
					}
				}
				for (x = 0; x < pricesarray.length; x++) {
					if (pricesarray[x].price_range_garment_name == 'Kissing Buttons') {
						return pricesarray[x].price_range_price * 1.0;
					}
				}
			}


			this.computePrice = function () {

				currency = that.getSellerCurrency();
				var pricesarray = [];
				pricerange = that.dsRegistry.getDatasource('pricerangeDS');
				if (pricerange != null) {
					for (var i = 0; i < pricerange.getStore().length; i++) {
						if (pricerange.getStore()[i].price_range_currency == currency.code) {
							pricesarray.push(pricerange.getStore()[i]);
						}
					}
				}
				var analysis = [];
				var price = 0;
				var text = "";
				if (that.garmentsList()[0].count() > 0) {
					for (i in that.dumpOrder().Jacket.Jacket) {
						jacketprice = 0;
						if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.price || that.dumpOrder().Jacket.Jacket[i].GarmentPrice) {
							//   price = price +  that.dumpOrder().Jacket.Jacket[i].JacketFabric.price * 1.0;
							if (that.dumpOrder().Jacket.Jacket[i].GarmentPrice)
								jacketprice = Number(that.dumpOrder().Jacket.Jacket[i].GarmentPrice);
							for (var x = 0; x < pricesarray.length; x++) {

								if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Jacket') {
									jacketprice = jacketprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									analysis.push('<h4  class="grid_12"><div class="price_type grid_8">1x ' + pricesarray[x].price_range_name + ' Jacket (' + that.dumpOrder().Jacket.Jacket[i].JacketFabric.title + ') </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></h4>');
									break;
								}
							}
							analysis.push('<ul class="grid_12">');
							if (that.dumpOrder().Jacket.Jacket[i].JacketVentStyle.id == '123') {
								if (that.dumpOrder().Jacket.Jacket[i].JacketTail == true) {
									//price = price * 1.30;
									for (var x = 0; x < pricesarray.length; x++) {
										if (pricesarray[x].price_range_garment_name == 'Tail Suits') {
											extracost = jacketprice * (pricesarray[x].price_range_price * 1.0) / 100.0;
											jacketprice += extracost;
											analysis.push('<li><div class="price_type grid_8">1x Jacket Tail </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
											break;
										}
									}

								}
							}

							//      if (that.dumpOrder().Jacket.Jacket[i].JacketDinner == true){
							//      	price = price + 200;
							//      }
							if (that.dumpOrder().Jacket.Jacket[i].JacketKissingButtons == true) {
								//price = price + 30;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Kissing Buttons') {
										extracost = pricesarray[x].price_range_price * 1.0;
										jacketprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}

							}
							//  if (that.dumpOrder().Jacket.Jacket[i].JacketContrastTrimming == true){
							if (that.dumpOrder().Jacket.Jacket[i].JacketCollarTrimming == true || that.dumpOrder().Jacket.Jacket[i].JacketLapelTrimming == true ||
								that.dumpOrder().Jacket.Jacket[i].JacketCoverButtons == true || that.dumpOrder().Jacket.Jacket[i].JacketPocketTrimming == true) {
								//price = price + 200;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Satin Trimming') {
										extracost = pricesarray[x].price_range_price * 1.0;
										jacketprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}
							}
							//   }	

							if (that.dumpOrder().Jacket.Jacket[i].JacketLining.price == 'yes') {
								//price = price + 100;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Silk Lining') {
										extracost = pricesarray[x].price_range_price * 1.0;
										jacketprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}
							}

							if ( /*that.dumpOrder().Jacket.Jacket[i].JacketTopStitch == true &&*/ (that.dumpOrder().Jacket.Jacket[i].JacketTopStitchPocket == true || that.dumpOrder().Jacket.Jacket[i].JacketTopStitchLapel == true)) {
								//price = price + 90;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Top Stitch') {
										extracost = pricesarray[x].price_range_price * 1.0;
										jacketprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}
							}

							if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogram == true) {
								if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage != "") {
									//console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image);
									image_already_exists = false;
									for (var j = 0; j < i; j++) {
										if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image == that.dumpOrder().Jacket.Jacket[j].JacketCustomMonogramImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//	price = price + 100;
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												extracost = pricesarray[x].price_range_price * 1.0;
												jacketprice += extracost;
												analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
												break;
											}
										}

									}
								}
							}

							if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroidery == true) {
								if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage != "") {
									//console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image);
									image_already_exists = false;
									for (var j = 0; j < i; j++) {
										if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image == that.dumpOrder().Jacket.Jacket[j].JacketEmbroideryImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//price = price + 100;	
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												extracost = pricesarray[x].price_range_price * 1.0;
												jacketprice += extracost;
												analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
												break;
											}
										}
									}
								}
							}

						}
						price += jacketprice;
						analysis.push('</ul>');
					}
				}

				if (that.garmentsList()[1].count() > 0) {
					for (i in that.dumpOrder().Pant.Pant) {
						if (that.dumpOrder().Pant.Pant[i].PantFabric.price || that.dumpOrder().Pant.Pant[i].GarmentPrice) {
							//  price = price +  that.dumpOrder().Pant.Pant[i].PantFabric.price * 1.0;
							if (that.dumpOrder().Pant.Pant[i].GarmentPrice)
								price += Number(that.dumpOrder().Pant.Pant[i].GarmentPrice);
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Pant.Pant[i].PantFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Pants') {
									price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x ' + pricesarray[x].price_range_name + ' Pants (' + that.dumpOrder().Pant.Pant[i].PantFabric.title + ') </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></h4>');
									//analysis.push('<ul style="margin-top:-20px">');
									//analysis.push('</ul>');
									break;
								}
							}
						}
					}
				}

				if (that.garmentsList()[4].count() > 0) {
					for (i in that.dumpOrder().Shirt.Shirt) {
						if ((that.dumpOrder().Shirt.Shirt[i].shirtFabric.price && that.dumpOrder().Shirt.Shirt[i].shirtFabric.title != 'none') || that.dumpOrder().Shirt.Shirt[i].GarmentPrice) {
							var platinumprice = 0;
							var silverprice = 0;
							if (that.dumpOrder().Shirt.Shirt[i].GarmentPrice)
								price += Number(that.dumpOrder().Shirt.Shirt[i].GarmentPrice);
							for (var x = 0; x < pricesarray.length; x++) {
								if (pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Platinum') {
									platinumprice = pricesarray[x].price_range_price * 1.0;
								}
								if (pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Silver') {
									silverprice = pricesarray[x].price_range_price * 1.0;
								}
							}
							count = i * 1.0 + 1;
							text += "Shirt " + count + ":\n";
							if (that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment == true) {
								price += platinumprice;
								analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x Platinum Make Shirt (' + that.dumpOrder().Shirt.Shirt[i].shirtFabric.title + ') </div><div class="price_itself grid_3"> ' + platinumprice + '.00&nbsp;' + currency.symbol + '</div></h4>');
							} else {
								price += silverprice;
								analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x Standard Make Shirt (' + that.dumpOrder().Shirt.Shirt[i].shirtFabric.title + ') </div><div class="price_itself grid_3"> ' + silverprice + '.00&nbsp;' + currency.symbol + '</div></h4>');
							}
							//   	analysis.push('<ul>');
							// 	analysis.push('</ul>');

						}

					}
				}

				if (that.garmentsList()[2].count() > 0) {
					for (i in that.dumpOrder().Suit.suit) {
						suitprice = 0;
						extrajacketprice = 0;
						extrapantsprice = 0;
						if (that.dumpOrder().Suit.suit[i].suitFabric.price || that.dumpOrder().Suit.suit[i].GarmentPrice) {
							//price = price +  that.dumpOrder().Suit.suit[i].suitFabric.price * 1.0;
							if (that.dumpOrder().Suit.suit[i].GarmentPrice)
								suitprice = Number(that.dumpOrder().Suit.suit[i].GarmentPrice);
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Suit.suit[i].suitFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Suit') {
									suitprice = suitprice + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x Bespoke ' + pricesarray[x].price_range_name + ' Suit (' + that.dumpOrder().Suit.suit[i].suitFabric.title + ') </div><div class="price_itself grid_3">' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></h4>');
									break;
								}
							}
							analysis.push('<ul class="grid_12">');
							if (that.dumpOrder().Suit.suit[i].suitJacketVentStyle.id == '123') {
								if (that.dumpOrder().Suit.suit[i].suitJacketTail == true) {
									//price = price * 1.30;
									for (var x = 0; x < pricesarray.length; x++) {
										if (pricesarray[x].price_range_garment_name == 'Tail Suits') {
											extracost = suitprice * (pricesarray[x].price_range_price * 1.0) / 100.0;
											suitprice += extracost;
											analysis.push('<li><div class="price_type grid_8">1x Suit Tail </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
											/*
											if(that.dumpOrder().Suit.suit[i].ExtraJacket >0){
												extrajacketprice += extrajacketprice * ((pricesarray[x].price_range_price * 1.0 )/100.0);
											}*/
											break;
										}
									}

								}
							}
							if (that.dumpOrder().Suit.suit[i].suitJacketKissingButtons == true) {
								//price = price + 30;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Kissing Buttons') {
										extracost = pricesarray[x].price_range_price * 1.0;
										suitprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										/*
										for(a = 0; a < that.dumpOrder().Suit.suit[i].ExtraJacket; a++){
											extrajacketprice += pricesarray[x].price_range_price * 1.0;
										}*/
										break;
									}
								}
							}
							if (that.dumpOrder().Suit.suit[i].suitJacketCollarTrimming == true || that.dumpOrder().Suit.suit[i].suitJacketLapelTrimming == true ||
								that.dumpOrder().Suit.suit[i].suitJacketCoverButtons == true || that.dumpOrder().Suit.suit[i].suitJacketPocketTrimming == true) {
								//price = price + 200;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Satin Trimming') {
										extracost = pricesarray[x].price_range_price * 1.0;
										suitprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}
							}
							//     }	

							if (that.dumpOrder().Suit.suit[i].suitJacketLining.price == 'yes') {
								//price = price + 100;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Silk Lining') {
										extracost = pricesarray[x].price_range_price * 1.0;
										suitprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}
							}

							if ((that.dumpOrder().Suit.suit[i].suitJacketTopStitchPocket == true || that.dumpOrder().Suit.suit[i].suitJacketTopStitchLapel == true)) {

								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Top Stitch') {
										extracost = pricesarray[x].price_range_price * 1.0;
										suitprice += extracost;
										analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
										break;
									}
								}
							}

							if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogram == true) {
								if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage != "") {
									image_already_exists = false;
									for (var j = 0; j < i; j++) {
										if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image == that.dumpOrder().Suit.suit[j].suitJacketCustomMonogramImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//	price = price + 100;
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												extracost = pricesarray[x].price_range_price * 1.0;
												suitprice += extracost;
												analysis.push('<li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li>');
												break;
											}
										}
									}
								}
							}

							if (that.dumpOrder().Suit.suit[i].suitJacketEmbroidery == true) {
								if (that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage != "") {
									image_already_exists = false;
									for (var j = 0; j < i; j++) {
										if (that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image == that.dumpOrder().Suit.suit[j].suitJacketEmbroideryImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//price = price + 100;
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												extracost = pricesarray[x].price_range_price * 1.0;
												suitprice += extracost;
												analysis.push('<li>1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</li>');
												break;
											}
										}
									}
								}
							}
							analysis.push('</ul>');
						}

						price = price + suitprice + extrajacketprice + extrapantsprice;
					}

				}

				if (that.garmentsList()[3].count() > 0) {
					for (i in that.dumpOrder().Vest.vest) {
						if (that.dumpOrder().Vest.vest[i].vestFabric.price || that.dumpOrder().Vest.vest[i].GarmentPrice) {
							if (that.dumpOrder().Vest.vest[i].GarmentPrice)
								price += Number(that.dumpOrder().Vest.vest[i].GarmentPrice);
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Vest.vest[i].vestFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Vest') {
									price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									analysis.push('<h4 class="grid_12"><div class="price_type grid_8">1x ' + pricesarray[x].price_range_name + ' Vest (' + that.dumpOrder().Vest.vest[i].vestFabric.title + ') </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></h4>');
									break;
								}
							}

							if (that.dumpOrder().Vest.vest[i].vestLining.price == 'yes' && that.dumpOrder().Vest.vest[i].parentSuitTimestampID == '') {
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Silk Lining') {
										extracost = pricesarray[x].price_range_price * 1.0;
										price += extracost;
										analysis.push('<ul class="grid_12"><li><div class="price_type grid_8">1x ' + pricesarray[x].price_range_garment_name + ' </div><div class="price_itself grid_3"> ' + pricesarray[x].price_range_price + '&nbsp;' + currency.symbol + '</div></li></ul>');
										break;
									}
								}
							}
						}
					}
				}

				var urgent = false;
				if (that.garmentsList()[0].count() > 0) {
					for (i in that.dumpOrder().Jacket.Jacket) {
						if (that.dumpOrder().Jacket.Jacket[i].urgent == 1) {
							urgent = true;
							break;
						}
					}
				}
				if (that.garmentsList()[1].count() > 0 && urgent == false) {
					for (i in that.dumpOrder().Pant.Pant) {
						if (that.dumpOrder().Pant.Pant[i].urgent == 1) {
							urgent = true;
							break;
						}
					}
				}
				if (that.garmentsList()[2].count() > 0 && urgent == false) {
					for (i in that.dumpOrder().Suit.suit) {
						if (that.dumpOrder().Suit.suit[i].urgent == 1) {
							urgent = true;
							break;
						}
					}
				}
				if (that.garmentsList()[3].count() > 0 && urgent == false) {
					for (i in that.dumpOrder().Vest.vest) {
						if (that.dumpOrder().Vest.vest[i].urgent == 1) {
							urgent = true;
							break;
						}
					}
				}
				if (that.garmentsList()[4].count() > 0 && urgent == false) {
					for (i in that.dumpOrder().Shirt.Shirt) {
						if (that.dumpOrder().Shirt.Shirt[i].urgent == 1) {
							urgent = true;
							break;
						}
					}
				}

				if (urgent == true) {
					price += that.urgentCost();
					analysis.push('<h4 class="grid_12"><div class="price_type grid_8">Express Delivery </div><div class="price_itself grid_3"> ' + that.urgentCost().toFixed(2) + '&nbsp;' + currency.symbol + '</div></h4>')
				}


				//console.log(that.extracost());				
				price += that.extracoststotal() * 1.0;

				analysistext = "";
				for (var x = 0; x < analysis.length; x++) {
					analysistext += analysis[x];
				}
				if (analysistext.length < 2) {
					analysistext = "-";
				}

				that.costanalysis(analysistext);
				return price.round(2);
			}

			this.computePriceWithDiscount = function () {
				var price = that.computePrice();
				if (that.computePrice() >= that.calculateDiscount()) {
					var price = that.computePrice() - that.calculateDiscount();
				}
				that.subtotal(price.round(2));
				if (that.calculateDiscount() > 0) {
					var total = that.subtotal() + that.tax();
					that.purchaseTotal(total);
				}

				return price.round(2);
			}


			/////////////////////////////////////            

			// this.computePriceForGarment = ko.computed(function(garment, index) {
			this.computePriceForGarment = function (garment, index) {

				currency = that.getSellerCurrency();

				var pricesarray = [];
				pricerange = that.dsRegistry.getDatasource('pricerangeDS');
				if (pricerange != null) {
					for (var i = 0; i < pricerange.getStore().length; i++) {
						if (pricerange.getStore()[i].price_range_currency == currency.code) {
							pricesarray.push(pricerange.getStore()[i]);
						}
					}
				}
				var price = 0;
				var extrajacketprice = 0;
				var extrapantsprice = 0;

				if (garment == "Jacket") {

					if (that.garmentsList()[0].count() > 0) {
						i = index;
						if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.price) {
							//   price = price +  that.dumpOrder().Jacket.Jacket[i].JacketFabric.price * 1.0;	
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Jacket.Jacket[i].JacketFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Jacket') {
									price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
								}
							}

							if (that.dumpOrder().Jacket.Jacket[i].JacketVentStyle.id == '123') {
								if (that.dumpOrder().Jacket.Jacket[i].JacketTail == true) {
									//price = price * 1.30;
									for (var x = 0; x < pricesarray.length; x++) {
										if (pricesarray[x].price_range_garment_name == 'Tail Suits') {
											price += price * ((pricesarray[x].price_range_price * 1.0) / 100.0);
											break;
										}
									}

								}
							}
							if (that.dumpOrder().Jacket.Jacket[i].JacketKissingButtons == true) {
								//price = price + 30;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Kissing Buttons') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}

							}
							//   if (that.dumpOrder().Jacket.Jacket[i].JacketContrastTrimming == true){
							if (that.dumpOrder().Jacket.Jacket[i].JacketCollarTrimming == true || that.dumpOrder().Jacket.Jacket[i].JacketLapelTrimming == true ||
								that.dumpOrder().Jacket.Jacket[i].JacketCoverButtons == true || that.dumpOrder().Jacket.Jacket[i].JacketPocketTrimming == true) {
								//price = price + 200;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Satin Trimming') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}
							//    }	

							if (that.dumpOrder().Jacket.Jacket[i].JacketLining.price == 'yes') {
								//price = price + 100;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Silk Lining') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}

							if ( /*that.dumpOrder().Jacket.Jacket[i].JacketTopStitch == true &&*/ (that.dumpOrder().Jacket.Jacket[i].JacketTopStitchPocket == true || that.dumpOrder().Jacket.Jacket[i].JacketTopStitchLapel == true)) {
								//price = price + 90;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Top Stitch') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}

							if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogram == true) {
								if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage != "") {
									console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image);
									image_already_exists = false;
									for (j = 0; j < i; j++) {
										if (that.dumpOrder().Jacket.Jacket[i].JacketCustomMonogramImage.image == that.dumpOrder().Jacket.Jacket[j].JacketCustomMonogramImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//	price = price + 100;
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												price += pricesarray[x].price_range_price * 1.0;
												break;
											}
										}

									}
								}
							}

							if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroidery == true) {
								if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage != "") {
									console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image);
									image_already_exists = false;
									for (j = 0; j < i; j++) {
										if (that.dumpOrder().Jacket.Jacket[i].JacketEmbroideryImage.image == that.dumpOrder().Jacket.Jacket[j].JacketEmbroideryImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//price = price + 100;	
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												price += pricesarray[x].price_range_price * 1.0;
												break;
											}
										}
									}
								}
							}
						}
					}
				} else if (garment == "Pants") {					
					if (that.garmentsList()[1].count() > 0) {
						i = index;
						if (that.dumpOrder().Pant.Pant[i].PantFabric.price) {
							//  price = price +  that.dumpOrder().Pant.Pant[i].PantFabric.price * 1.0;
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Pant.Pant[i].PantFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Pants') {
									price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
								}
							}
						}
					}
				} else if (garment == "Shirt") {
					if (that.garmentsList()[4].count() > 0) {
						i = index;
						if (that.dumpOrder().Shirt.Shirt[i].shirtFabric.price) {
							if (that.dumpOrder().Shirt.Shirt[i].shirtFabric.title != 'none') {

								var platinumprice = 0;
								var silverprice = 0;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Platinum') {
										platinumprice = pricesarray[x].price_range_price * 1.0;
									}
									if (pricesarray[x].price_range_garment_name == 'Shirt' && pricesarray[x].price_range_name == 'Silver') {
										silverprice = pricesarray[x].price_range_price * 1.0;
									}
								}
								console.log("that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment: " + that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment);
								if (that.dumpOrder().Shirt.Shirt[i].ShirtPlatinumTreatment == true) {
									price += platinumprice;
								} else {
									price += silverprice;
								}
							}
						}
					}
				} else if (garment == "Suit") {
					if (that.garmentsList()[2].count() > 0) {
						i = index;
						if (that.dumpOrder().Suit.suit[i].suitFabric.price) {
							//price = price +  that.dumpOrder().Suit.suit[i].suitFabric.price * 1.0;
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Suit.suit[i].suitFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Suit') {
									price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
								}
							}

							if (that.dumpOrder().Suit.suit[i].suitJacketVentStyle.id == '123') {
								if (that.dumpOrder().Suit.suit[i].suitJacketTail == true) {
									//price = price * 1.30;
									for (var x = 0; x < pricesarray.length; x++) {
										if (pricesarray[x].price_range_garment_name == 'Tail Suits') {
											price += price * ((pricesarray[x].price_range_price * 1.0) / 100.0);
											/*
											if(that.dumpOrder().Suit.suit[i].ExtraJacket >0){
												extrajacketprice += extrajacketprice * ((pricesarray[x].price_range_price * 1.0 )/100.0);
											}*/
											break;
										}
									}
								}
							}
							if (that.dumpOrder().Suit.suit[i].suitJacketKissingButtons == true) {
								//price = price + 30;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Kissing Buttons') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}
							//   if (that.dumpOrder().Suit.suit[i].suitJacketContrastTrimming == true){
							if (that.dumpOrder().Suit.suit[i].suitJacketCollarTrimming == true || that.dumpOrder().Suit.suit[i].suitJacketLapelTrimming == true ||
								that.dumpOrder().Suit.suit[i].suitJacketCoverButtons == true || that.dumpOrder().Suit.suit[i].suitJacketPocketTrimming == true) {
								//price = price + 200;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Satin Trimming') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}
							//     }	

							if (that.dumpOrder().Suit.suit[i].suitJacketLining.price == 'yes') {
								//price = price + 100;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Silk Lining') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}

							if ( /*that.dumpOrder().Suit.suit[i].suitJacketTopStitch == true &&*/ (that.dumpOrder().Suit.suit[i].suitJacketTopStitchPocket == true || that.dumpOrder().Suit.suit[i].suitJacketTopStitchLapel == true)) {
								//price = price + 90;
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Top Stitch') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}

							if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogram == true) {
								if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage != "") {
									console.log("CUSTOM MONOGRAM IMAGE HAS BEEN INSERTED " + that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image);
									image_already_exists = false;
									for (j = 0; j < i; j++) {
										if (that.dumpOrder().Suit.suit[i].suitJacketCustomMonogramImage.image == that.dumpOrder().Suit.suit[j].suitJacketCustomMonogramImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//	price = price + 100;
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												price += pricesarray[x].price_range_price * 1.0;
												break;
											}
										}
									}
								}
							}

							if (that.dumpOrder().Suit.suit[i].suitJacketEmbroidery == true) {
								if (that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage != "") {
									console.log("EMBROIDERY IMAGE HAS BEEN INSERTED " + that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image);
									image_already_exists = false;
									for (j = 0; j < i; j++) {
										if (that.dumpOrder().Suit.suit[i].suitJacketEmbroideryImage.image == that.dumpOrder().Suit.suit[j].suitJacketEmbroideryImage.image) {
											image_already_exists = true;
											break;
										}
									}
									if (image_already_exists == false) {
										//price = price + 100;
										for (var x = 0; x < pricesarray.length; x++) {
											if (pricesarray[x].price_range_garment_name == 'Custom Monogram') {
												price += pricesarray[x].price_range_price * 1.0;
												break;
											}
										}
									}
								}
							}
						}
					}
				} else if (garment == "Vest") {
					if (that.garmentsList()[3].count() > 0) {
						i = index;
						if (that.dumpOrder().Vest.vest[i].vestFabric.price) {
							//price = price +  that.dumpOrder().Vest.vest[i].vestFabric.price * 1.0;
							for (var x = 0; x < pricesarray.length; x++) {
								if (that.dumpOrder().Vest.vest[i].vestFabric.price_range == pricesarray[x].price_range_name && pricesarray[x].price_range_garment_name == 'Vest') {
									price = price + pricesarray[x].price_range_price * 1.0; // * 1.0 to convert from string to number
									break;
								}
							}

							if (that.dumpOrder().Vest.vest[i].vestLining.price == 'yes' && that.dumpOrder().Vest.vest[i].parentSuitTimestampID == '') {
								for (var x = 0; x < pricesarray.length; x++) {
									if (pricesarray[x].price_range_garment_name == 'Silk Lining') {
										price += pricesarray[x].price_range_price * 1.0;
										break;
									}
								}
							}

						}
					}
				}

				total = price + extrapantsprice + extrajacketprice;
				if (total > 0) {
					return total.round(2);
				}
				else {
					return '-';
				}
			}

			this.extracostFunction = function () {
				return ((0 + that.extracoststotal()) / 1);
			}

			this.calculateDiscount = function () {	//1205J0234T00  12: first 2 ammount digits,	05:	day,	A: seller name is JAdmin,	02: month,	34: next 2 ammount digits,	T: seller last name is TAdmin, 00: decimal ammount

				var code = that.discountCode();

				var discounts = discounts_code_from_server;

				for (discount_name in discounts) {
					if (code == discount_name) {
						return parseInt(discounts[code]);
					}
				}

				if (code == "Freelining") {
					return 100;
				} else if (code == "FreeShirt") {
					return 190;
				} else if (code == "FreeLinShi") {
					return 290;
				} else if (code == null || code == undefined || code.length < 12) {
					return 0;
				} else {

					var amount = code.substr(0, 2) + code.substr(7, 2) + "." + code.substr(10, 2);

					var codeday = code.substr(2, 2);
					var codemonth = code.substr(5, 2);
					var sellerFirstNameFD = code.substr(4, 1).toLowerCase();
					var sellerLastNameFD = code.substr(9, 1).toLowerCase();
					var first_nameFD = authCtrl.userInfo.first_name.substr(0, 1).toLowerCase();
					var last_nameFD = authCtrl.userInfo.last_name.substr(0, 1).toLowerCase();
					var day = new Date().getDate();
					var month = new Date().getMonth() + 1;
					if (day < 10) {
						day = "0" + day;
					} else {
						day = "" + day;
					}
					if (month < 10) {
						month = "0" + month;
					} else {
						month = "" + month;
					}
					if (sellerFirstNameFD == first_nameFD && sellerLastNameFD == last_nameFD && codeday == day && codemonth == month && code.length == 12) {

						return (amount * 1.0).round(2);
					} else {
						return 0;
					}
				}
			}

			this.checkDiscountCode = function () {
				var code = that.discountCode();

				var discounts = discounts_code_from_server;

				for (discount_name in discounts) {
					if (code == discount_name) {
						return 1;
					}
				}

				if (code == "Freelining" || code == "FreeShirt" || code == "FreeLinShi") {
					return 1;
				} else if (code == null || code == undefined || code.length < 12) {
					return -1;
				} else {

					var amount = (code.substr(0, 2) + code.substr(7, 2) + "." + code.substr(10, 2)) * 1;

					var codeday = code.substr(2, 2);
					var codemonth = code.substr(5, 2);
					var sellerFirstNameFD = code.substr(4, 1).toLowerCase();;
					var sellerLastNameFD = code.substr(9, 1).toLowerCase();;
					var first_nameFD = authCtrl.userInfo.first_name.substr(0, 1).toLowerCase();
					var last_nameFD = authCtrl.userInfo.last_name.substr(0, 1).toLowerCase();
					var day = new Date().getDate();
					var month = new Date().getMonth() + 1;
					if (day < 10) {
						day = "0" + day;
					} else {
						day = "" + day;
					}
					if (month < 10) {
						month = "0" + month;
					} else {
						month = "" + month;
					}

					console.log("amount: " + amount);
					console.log("that.computePrice(): " + that.computePrice());
					var difference = that.computePrice() - amount;
					console.log("difference: " + difference);

					if (difference < 0) {
						console.log("CASE NEGATIVE");
						return 0;
					} else if (sellerFirstNameFD == first_nameFD && sellerLastNameFD == last_nameFD && codeday == day && codemonth == month && code.length == 12) {
						return 1;
					} else {
						return 0;
					}
				}
			}


			this.calculateVat = function () {
				gstAmount = that.computePriceWithDiscount() * that.getGST();
				that.tax(gstAmount.round(2));
				var total = that.subtotal() + that.tax();
				that.purchaseTotal(total);
				return gstAmount.round(2);
			}

			this.getGST = function () {

				currency = that.getSellerCurrency();

				pricerange = that.dsRegistry.getDatasource('pricerangeDS');
				if (pricerange != null) {
					for (var i = 0; i < pricerange.getStore().length; i++) {
						if (pricerange.getStore()[i].price_range_currency == currency.code) {
							gst = (pricerange.getStore()[i].price_range_GST * 1.0) / 100;
							return gst;
						}
					}
				}
				return 0;
			}

			this.calculateTotalPrice = function () {
				var total = that.calculateVat() + that.computePriceWithDiscount();
				that.purchaseTotal(total);

				//console.log("calculateTotalPrice:" + total );				
				return total;
			}

			this.calculateTotalPriceWithDiscount = function () {
				var total = that.calculateVat() + that.computePriceWithDiscount();// - that.calculateDiscount();
				//console.log("calculateTotalPriceWithDiscount:" + total ); 
				return total;
			}

			this.getCurrencySymbol = function () {
				currency = that.getSellerCurrency();
				return currency.symbol;
			}


			this.urgentCost = function () {

				currency = that.getSellerCurrency();
				var pricesarray = [];
				pricerange = that.dsRegistry.getDatasource('pricerangeDS');
				if (pricerange) {
					for (var i = 0; i < pricerange.getStore().length; i++) {
						if (pricerange.getStore()[i].price_range_currency == currency.code) {
							pricesarray.push(pricerange.getStore()[i]);
						}
					}
				}
				var urgentcost = 0;
				for (x = 0; x < pricesarray.length; x++) {
					if (pricesarray[x].price_range_garment_name == 'Express Delivery') {
						urgentcost = pricesarray[x].price_range_price * 1.0;
						break;
					}
				}
				return urgentcost;
			}

			/*-------------------------------------------------------- COPY GARMENT -------------------------------------------------------------------------------------------*/

			this.copyGarment = ko.observable(false);
			this.CopyGarmentJacket = ko.observable(false);
			this.CopyGarmentPant = ko.observable(false);
			this.CopyGarmentShirt = ko.observable(false);
			this.CopyGarmentVest = ko.observable(false);


			this.copyGarmentImage = new MediaType(1, null, true, '', true);
			this.copyGarmentVideo = new MediaType(2, null, true, '', true);
			this.copyGarmentComments = ko.observable('');
			this.copyGarmentPreviewImage = ko.observableArray([]);
			this.copyGarmentPreviewVideo = ko.observableArray([]);
			this.copyGarmentImage.setCustomPreview(this.copyGarmentPreviewImage);
			this.copyGarmentVideo.setCustomPreview(this.copyGarmentPreviewVideo);



			/*---------------------------------------------------- END OF COPY GARMENT -----------------------------------------------------------------------------------------*/


			/*---------------------------------------------------- FABRIC ITEMS ---------------------------------------------------------------------------------------------*/

			this.FabricJacketNumber = ko.observable(0);
			this.FabricPantNumber = ko.observable(0);
			this.FabricShirtNumber = ko.observable(0);
			this.FabricVestNumber = ko.observable(0);

			this.FabricList = ko.observableArray([]);
			this.FabricPrice = ko.observableArray([]);
			this.FabricMetrage = ko.observable('');
			this.ClientFabric = ko.observable(false);
			this.AddClientFabric = ko.observable(false);
			this.FabricCode = ko.observable('');
			this.SelectedCustomerFabric = ko.observable({});
			this.FabricImage = new MediaType(1, null, false, '', true);
			this.FabricPreviewImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.FabricImage.setCustomPreview(this.FabricPreviewImage);
			this.FabricImage.setCustomPreviewValue();

			this.updateGarmentPrice = function (data, element, lol, test, what) {
				var values = document.getElementsByName("GarmentPrice");
				var currentPostion = 0;
				for (var c in that.FabricPrice()) {
					for (var y in that.FabricPrice()[c].garmentList) {
						that.FabricPrice()[c].garmentList[y].garment.GarmentPrice = that.FabricPrice()[c].garmentList[y].price();
					}
				}
				that.calculateTotalPrice();
			};

			this.removeFabric = function (GarmentName, GarmentType, Title, Fabric, garmentId, usage) {
				for (var c in that.FabricList()) {
					if (Fabric.FabricCode && that.FabricList()[c].FabricCode == Fabric.FabricCode) {
						that.FabricList()[c].usage -= usage;
						if (GarmentType == 0 && that.FabricList()[c].JacketGarments > 0) {
							that.FabricList()[c].JacketGarments = that.FabricList()[c].JacketGarments - 1;
						} else if (GarmentType == 1 && that.SelectedCustomerFabric().PantGarments > 0) {
							that.FabricList()[c].PantGarments = that.FabricList()[c].PantGarments - 1;
						}
						else if (GarmentType == 2 && that.FabricList()[c].SuitGarments > 0) {
							that.FabricList()[c].SuitGarments = that.FabricList()[c].SuitGarments - 1;
						}
						else if (GarmentType == 3 && that.FabricList()[c].VestGarments > 0) {
							that.FabricList()[c].VestGarments = that.FabricList()[c].VestGarments - 1;
						}
						else if (GarmentType == 4 && that.FabricList()[c].ShirtGarments > 0) {
							that.FabricList()[c].ShirtGarments = that.FabricList()[c].ShirtGarments - 1;
						}
					}
				}
				for (var c in that.FabricPrice()) {
					for (var y in that.FabricPrice()[c].garmentList) {
						if ((garmentId == that.FabricPrice()[c].garmentList[y].garment.variantId && GarmentName == that.FabricPrice()[c].garmentName)) {
							that.FabricPrice()[c].garmentList.splice(y, 1);
							break;
						}
					}
				}
			};

			this.addCustomerFabric = function () {
				if (that.FabricMetrage() <= 0) {
					customAlert('Digit a valid metrage!');
				} else if (!that.FabricCode()) {
					customAlert('Inform a code to the Fabric!');
				} else if (!that.FabricImage.hasMedia()) {
					customAlert('Please upload a image of the Fabric.');
				} else {
					that.FabricList.push({ FabricCode: that.FabricCode(), FabricMetrage: that.FabricMetrage(), FabricImagePreview: that.FabricImage.getMediaPreview()(), FabricImageInfo: that.FabricImage.getSyncInfo(), usage: 0, isShirtFabric: 0, SuitGarments: 0, PantGarments: 0, JacketGarments: 0, VestGarments: 0, ShirtGarments: 0 });
					that.FabricCode('');
					that.FabricMetrage('');
					that.AddClientFabric(false);
					that.FabricImage = new MediaType(1, null, false, '', true);
					that.FabricImage.setCustomPreview(this.FabricPreviewImage);
					that.FabricImage.setCustomPreviewValue();
					that.SelectedCustomerFabric(that.FabricList()[that.FabricList().length - 1]);
				}
			};

			this.addFabricToGarment = function (Garment, GarmentName, GarmentType, Title, usage) {
				if ((that.SelectedCustomerFabric().usage + usage) <= Number(that.SelectedCustomerFabric().FabricMetrage)) {
					that.SelectedCustomerFabric().usage += usage;
					var found = false;
					found = false;
					for (var c in that.FabricPrice()) {
						if (that.FabricPrice()[c].garmentName == GarmentName) {
							for (var y in that.FabricPrice()[c].garmentList) {
								if (that.FabricPrice()[c].garmentList[y].title == Title) {
									found = true;
									break;
								}
							}
						}
					}
					if (!found) {
						found = false;
						for (var c in that.FabricPrice()) {
							if (that.FabricPrice()[c].garmentName == GarmentName) {
								found = true;
								var price = ko.observable('');
								price.subscribe(that.updateGarmentPrice);
								that.FabricPrice()[c].garmentList.push({ title: Title, price: price, garment: Garment });
								break;
							}
						}
						if (!found) {
							var price = ko.observable('');
							var garmentvm = {};
							if (GarmentType == 0) {
								garmentvm = that.garmentsJacketVM;
							} else if (GarmentType == 1) {
								garmentvm = that.garmentsPantVM;

							} else if (GarmentType == 2) {
								garmentvm = that.garmentsSuitVM;
							}
							else if (GarmentType == 3) {
								garmentvm = that.garmentsVestVM;
							} else if (GarmentType == 4) {
								garmentvm = that.garmentsShirtVM;
							}
							price.subscribe(that.updateGarmentPrice);
							that.FabricPrice.push({ garmentName: GarmentName, garmentList: [], controller: garmentvm });
							that.FabricPrice()[that.FabricPrice().length - 1].garmentList.push({ title: Title, price: price, garment: Garment });
						}

					}
					if (GarmentType == 0) {
						that.SelectedCustomerFabric().JacketGarments = that.SelectedCustomerFabric().JacketGarments + 1;
						that.SelectedCustomerFabric().isShirtFabric = false;
					} else if (GarmentType == 1) {
						that.SelectedCustomerFabric().PantGarments = that.SelectedCustomerFabric().PantGarments + 1;
						that.SelectedCustomerFabric().isShirtFabric = false;
					}
					else if (GarmentType == 2) {
						that.SelectedCustomerFabric().SuitGarments = that.SelectedCustomerFabric().SuitGarments + 1;
						that.SelectedCustomerFabric().isShirtFabric = false;
					}
					else if (GarmentType == 3) {
						that.SelectedCustomerFabric().VestGarments = that.SelectedCustomerFabric().VestGarments + 1;
						that.SelectedCustomerFabric().isShirtFabric = false;
					}
					else if (GarmentType == 4) {
						that.SelectedCustomerFabric().isShirtFabric = true;
						that.SelectedCustomerFabric().ShirtGarments = that.SelectedCustomerFabric().ShirtGarments + 1;
					}
					return true;
				}
				else {
					customAlert('The amount of fabric is not enough.');
					return false;
				}
			}




			/*------------------------------------------------ END OF FABRIC ITEMS ------------------------------------------------------------------------------------------*/


			this.detectVerticalSquash = function (img) {
				var iw = img.naturalWidth, ih = img.naturalHeight;
				//				var canvas = document.createElement('canvas');
				var canvas = document.getElementById("canvas-squash");
				canvas.width = 1;
				canvas.height = ih;
				var ctx = canvas.getContext('2d');
				var t = ctx.drawImage(img, 0, 0);
				t = null;
				var data = ctx.getImageData(0, 0, 1, ih).data;
				// search image edge pixel position in case it is squashed vertically.
				var sy = 0;
				var ey = ih;
				var py = ih;
				while (py > sy) {
					var alpha = data[(py - 1) * 4 + 3];
					if (alpha === 0) {
						ey = py;
					} else {
						sy = py;
					}
					py = (ey + sy) >> 1;
				}
				var ratio = (py / ih);
				canvas.width = canvas.width;
				return (ratio === 0) ? 1 : ratio;
			}

			this.drawImageIOSFix = function (ctx, img, sx, sy, sw, sh) {  /*(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) */
				var vertSquashRatio = orderItem.detectVerticalSquash(img);
				// Works only if whole image is displayed:
				// ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
				// The following works correct also when only a part of the image is displayed:
				var t = ctx.drawImage(img, sx * vertSquashRatio, sy * vertSquashRatio, sw * vertSquashRatio, sh * vertSquashRatio);
				t = null;
			}


			this.resizeMyImage = function (url, thumbwidth, postedwidth, page, what) {
				console.log("resizeMyImage function in app.js");
				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";
				var sourceImage = new Image();
				var thumbnail;
				var topost;

				sourceImage.onload = function () {
					// Create a canvas with the desired dimensions
					var oldwidth = sourceImage.width;
					var oldheight = sourceImage.height;
					//var canvas = document.createElement("canvas");
					var canvas = document.getElementById("canvas-0");
					if (oldwidth < oldheight) {
						var width = thumbwidth;
						var height = (oldheight / oldwidth) * width;
						canvas.width = width;
						canvas.height = height;
						orderItem.drawImageIOSFix(canvas.getContext("2d"), sourceImage, 0, 0, width, height);
						thumbnail = canvas.toDataURL("image/jpeg", 0.2);
						canvas.width = canvas.width; // resets canvas

						var width2 = postedwidth;
						var height2 = (oldheight / oldwidth) * width2;
						canvas.width = width2;
						canvas.height = height2;
						orderItem.drawImageIOSFix(canvas.getContext("2d"), sourceImage, 0, 0, width2, height2);
						topost = canvas.toDataURL("image/jpeg", 0.6);
						canvas.width = canvas.width;

						orderItem.appendImage(topost, thumbnail, page, what);
					} else {
						//var tempcanvas = document.createElement("canvas");
						var tempcanvas = document.getElementById("canvas-1");
						var tempheight = thumbwidth;
						var tempwidth = (oldwidth / oldheight) * tempheight;
						tempcanvas.width = tempwidth;
						tempcanvas.height = tempheight;
						orderItem.drawImageIOSFix(tempcanvas.getContext("2d"), sourceImage, 0, 0, tempwidth, tempheight);
						var tmpimageurl = tempcanvas.toDataURL("image/jpeg", 0.2);
						tempcanvas.width = tempcanvas.width;
						//tempcanvas.remove();

						var tempimage = new Image();
						tempimage.src = tmpimageurl;
						tmpimageurl = null;
						delete tempimage;

						tempimage.onload = function () {
							var newwidth = tempimage.width;
							var newheight = tempimage.height;
							var cContext = canvas.getContext('2d');
							var cw = tempimage.width, ch = tempimage.height, cx = 0, cy = 0;
							cw = tempimage.height;
							ch = tempimage.width;
							cy = tempimage.height * (-1);
							canvas.setAttribute('width', cw);
							canvas.setAttribute('height', ch);
							cContext.rotate(90 * Math.PI / 180);
							var t = cContext.drawImage(tempimage, cx, cy);
							t = null;

							thumbnail = canvas.toDataURL("image/jpeg", 0.2);
							//var tempcanvas2 = document.createElement("canvas");
							var tempcanvas2 = document.getElementById("canvas-2");
							var tempheight2 = postedwidth;
							var tempwidth2 = (oldwidth / oldheight) * tempheight2;
							tempcanvas2.width = tempwidth2;
							tempcanvas2.height = tempheight2;
							orderItem.drawImageIOSFix(tempcanvas2.getContext("2d"), sourceImage, 0, 0, tempwidth2, tempheight2);
							var tmpimageurl2 = tempcanvas2.toDataURL("image/jpeg", 0.6);
							tempcanvas2.width = tempcanvas2.width;
							//tempcanvas2.remove();

							var tempimage2 = new Image();
							tempimage2.src = tmpimageurl2;
							tmpimageurl2 = null;
							delete tempimage2;

							tempimage2.onload = function () {
								var newwidth2 = tempimage2.width;
								var newheight2 = tempimage2.height;
								var cContext2 = canvas.getContext('2d');
								var cw2 = tempimage2.width, ch2 = tempimage.height, cx2 = 0, cy2 = 0;
								cw2 = tempimage2.height;
								ch2 = tempimage2.width;
								cy2 = tempimage2.height * (-1);
								canvas.setAttribute('width', cw2);
								canvas.setAttribute('height', ch2);
								cContext2.rotate(90 * Math.PI / 180);
								var t = cContext2.drawImage(tempimage2, cx2, cy2);
								t = null;
								topost = canvas.toDataURL("image/jpeg", 0.6);
								canvas.width = canvas.width;
								//canvas.remove();
								orderItem.appendImage(topost, thumbnail, page, what);
							}
						}
					}
				}
				sourceImage.src = url;

				topost = null;
				thumbnail = null;
				delete sourceImage;
			}

			this.appendImage = function (topost, thumbnail, page, what) {
				if (page == "fittings.htm") {
					appendItemFittingsImage(topost, thumbnail);
				} else if (page == "completions.htm") {
					appendItemCompletionImage(topost, thumbnail, what);
				} else if (page == "alterationReport.htm") {
					appendItemAlterationReportImage(topost, thumbnail, what);
				} else if (page == "fitting.htm") {	// THIS ONE MIGHT NOT BE USED
					appendItemFittingImage(topost, thumbnail);
				} else if (page == "fittingImages.htm") {	// THIS ONE MIGHT NOT BE USED
					appendItemFittingImagesImage(topost, thumbnail);
				} else if (page == "garmentsJacket.htm") {
					appendItemJacketMonogram(topost, thumbnail);
				} else if (page == "garmentsPant.htm") {
					appendItemPantPockets(topost, thumbnail);
				} else if (page == "garmentsSuit.htm") {
					appendItemSuitPockets(topost, thumbnail);
				} else if (page == "garmentsShirt.htm") {
					appendItem(topost, thumbnail);
				} else if (page == "garmentsVest.htm") {
					appendItemVestButtons(topost, thumbnail);
				} else if (page == "makePayment.htm") {
					appendItemCreditCard(topost, thumbnail);
				} else if (page == "measurements.htm") {
					appendItemMeasurements(topost, thumbnail);
				} else if (page == "orderItemCopyGarments.htm") {
					appendItemMediaCopy(topost, thumbnail);
				} else if (page == "orderItemMedia.htm") {
					appendItemMedia(topost, thumbnail);
				} else if (page == "orderItemVideo.htm") {
					appendItemVideo(topost, thumbnail);
				} else if (page == "orderItemReview.htm") {
					appendItemReviewCreditCard(topost, thumbnail);
				} else if (page == "shipping_fabrics.htm") {
					appendItemShippingFabric(topost, thumbnail);
				} else if (page == "shipping_copy_garment.htm") {
					appendItemShippingCopyGarment(topost, thumbnail);
				} else if (page == "shipping_faulty_remake_garments.htm") {
					appendItemShippingFaultyRemakeGarments(topost, thumbnail);
				} else if (page == "shipping_other.htm") {
					appendItemShippingOther(topost, thumbnail);
				} else if (page == "shipping_box_send.htm") {
					appendItemBoxSend(topost, thumbnail);
				}
			};

			this.openDialog = function () {
				document.getElementById("form_panel").style.display = "block";
			};

			this.openDialog2 = function (controller, event, index) {
				this.orderImageIndex = index;
				this.orderImageEvent = event;
				this.orderImageControler = controller;
				var tag = this.customerImage.buildTagPicture('', this.orderImageEvent, this.orderImageIndex);
				$("#mapper" + this.orderImageIndex).css("left", tag.left + '%');
				$("#mapper" + this.orderImageIndex).css("top", tag.top + '%');
				$("#mapper" + this.orderImageIndex).css("width", "35px");
				$("#mapper" + this.orderImageIndex).css("height", "35px");
				$("#mapper" + this.orderImageIndex).show();
				document.getElementById("form_panel").style.display = "block";
			};

			this.closeDialog2 = function () {
				this.orderImageIndex = -1;
				this.orderImageEvent = null;
				this.orderImageControler = null;
				$("div.form_panel").hide();
				$('div.mapper').hide();
				$("textarea.title").val('');
			};

			this.addTag = function () {
				var text = $("textarea.title").val();
				$("div.form_panel").hide();
				$('div.mapper').hide();
				$("textarea.title").val('');
				var tag = this.orderImageControler.buildTagPicture(text, this.orderImageEvent, this.orderImageIndex);
				this.orderImageControler.addTag(tag, this.orderImageIndex);
				this.orderImageIndex = -1;
				this.orderImageEvent = null;
				this.orderImageControler = null;
			};



			this.addCommentVideoFitlines = function (controller, mediaIndex, text) {
				controller.addTag(text, mediaIndex);
			};
			this.clickMedia = function (controller, mode) {
				that.mediaController = controller;
				if (mode == 2) {
					that.MediaUploadVM.choosePhoto2();
				}
				else {
					that.MediaUploadVM.chooseVideo();
				}
			}

			this.selectedGarment = new ko.observable();


			this.isEnabled = function (data) {
				console.log('isEnabled');
				for (var ind in that.garmentsList()) {
					if (that.garmentsList()[ind].id == data && that.garmentsList()[ind].count() > 0) {
						return that.garmentsList()[ind].active;
					}
				}
				return false;
			};


			this.dumpOrder = function () {

				if ((typeof (that.garmentsJacketDS) == "undefined" && that.garmentsList()[0].count() > 0) ||
					(typeof (that.garmentsPantDS) == "undefined" && that.garmentsList()[1].count() > 0) ||
					(typeof (that.garmentsSuitDS) == "undefined" && that.garmentsList()[2].count() > 0) ||
					(typeof (that.garmentsVestDS) == "undefined" && that.garmentsList()[3].count() > 0) ||
					(typeof (that.garmentsShirtDS) == "undefined" && that.garmentsList()[4].count() > 0)
				) {
					that.populateGarments();		// HOTFIX FOR FIXING THE BUG IF YOU JUMP FROM orderItemPopulateGarments DIRECTLY TO orderItemSelectGarment
				}

				var out = {};

				if (typeof (that.garmentsJacketDS) != "undefined") {
					if (that.garmentsList()[0].count() > 0) {
						out.Jacket = that.garmentsJacketDS.getStore();
						for (var counterX in out.Jacket.Jacket) {
							for (var c in that.FabricPrice()) {
								if (that.FabricPrice()[c].garmentName == "Jacket") {
									for (var y in that.FabricPrice()[c].garmentList) {
										if (that.FabricPrice()[c].garmentList[y].garment.variantId == out.Jacket.Jacket[counterX].variantId) {
											out.Jacket.Jacket[counterX].GarmentPrice = that.FabricPrice()[c].garmentList[y].price();
										}
									}
								}
							}
							out.Jacket.Jacket[counterX].customMedia = out.Jacket.Jacket[counterX].custom.getSyncInfo();
						}
					}
				}
				if (typeof (that.garmentsPantDS) != "undefined") {
					if (that.garmentsList()[1].count() > 0) {
						out.Pant = that.garmentsPantDS.getStore();
						for (var counterX in out.Pant.Pant) {
							for (var c in that.FabricPrice()) {
								if (that.FabricPrice()[c].garmentName == "Pant") {
									for (var y in that.FabricPrice()[c].garmentList) {
										if (that.FabricPrice()[c].garmentList[y].garment.variantId == out.Pant.Pant[counterX].variantId) {
											out.Pant.Pant[counterX].GarmentPrice = that.FabricPrice()[c].garmentList[y].price();
										}
									}
								}
							}
							out.Pant.Pant[counterX].customMedia = out.Pant.Pant[counterX].custom.getSyncInfo();
						}
					}
				}
				if (typeof (that.garmentsSuitDS) != "undefined") {
					if (that.garmentsList()[2].count() > 0) {
						out.Suit = that.garmentsSuitDS.getStore();
						for (var counterX in out.Suit.suit) {
							for (var c in that.FabricPrice()) {
								if (that.FabricPrice()[c].garmentName == "Suit") {
									for (var y in that.FabricPrice()[c].garmentList) {
										if (that.FabricPrice()[c].garmentList[y].garment.variantId == out.Suit.suit[counterX].variantId) {
											out.Suit.suit[counterX].GarmentPrice = that.FabricPrice()[c].garmentList[y].price();
										}
									}
								}
							}
							out.Suit.suit[counterX].customMedia = out.Suit.suit[counterX].custom.getSyncInfo();
						}
					}
				}
				if (typeof (that.garmentsVestDS) != "undefined") {
					if (that.garmentsList()[3].count() > 0) {
						out.Vest = that.garmentsVestDS.getStore();
						for (var counterX in out.Vest.vest) {
							for (var c in that.FabricPrice()) {
								if (that.FabricPrice()[c].garmentName == "Vest") {
									for (var y in that.FabricPrice()[c].garmentList) {
										if (that.FabricPrice()[c].garmentList[y].garment.variantId == out.Vest.vest[counterX].variantId) {
											out.Vest.vest[counterX].GarmentPrice = that.FabricPrice()[c].garmentList[y].price();
										}
									}
								}
							}
							out.Vest.vest[counterX].customMedia = out.Vest.vest[counterX].custom.getSyncInfo();
						}
					}
				}
				if (typeof (that.garmentsShirtDS) != "undefined") {
					if (that.garmentsList()[4].count() > 0) {
						out.Shirt = that.garmentsShirtDS.getStore();
						for (var counterX in out.Shirt.Shirt) {
							for (var c in that.FabricPrice()) {
								if (that.FabricPrice()[c].garmentName == "Shirt") {
									for (var y in that.FabricPrice()[c].garmentList) {
										if (that.FabricPrice()[c].garmentList[y].garment.variantId == out.Shirt.Shirt[counterX].variantId) {
											out.Shirt.Shirt[counterX].GarmentPrice = that.FabricPrice()[c].garmentList[y].price();
										}
									}
								}
							}
							out.Shirt.Shirt[counterX].customMedia = out.Shirt.Shirt[counterX].custom.getSyncInfo();
						}
					}
				}
				return out;
			};


			this.addVariantToGarment = function (data) {
				console.log('addVariantToGarment');
				switch (data.id) {
					case 0:
						that.garmentsJacketVM.addVariantJacket();
						break;
					case 1:
						that.garmentsPantVM.addVariantPant();
						break;
					case 2:
						that.garmentsSuitVM.addVariantSuit();
						break;
					case 3:
						that.garmentsVestVM.addVariantVest();
						break;
					case 4:
						that.garmentsShirtVM.addVariantShirt();
						break;

				}
			};

			this.alreadypopulated = false;


			this.populateGarments = function () {
				console.log('populateGarments');
				for (var ind in that.garmentsList()) {
					if (that.garmentsList()[ind].count() != 0) {
						that.enableGarment({
							id: that.garmentsList()[ind].id
						});
						for (var cc = 0; cc < that.garmentsList()[ind].count() - 1; cc++) {
							that.addVariantToGarment({ id: that.garmentsList()[ind].id });
						}
					}
				}
			};


			this.enableGarment = function (data) {
				console.log('enableGarment ' + JSON.stringify(data));
				var spinner = document.getElementById('loading_jp');
				switch (data.id) {
					case 0:
						if (that.garmentsJacketDS == undefined) {
							that.jacketfirsttime = true;
							that.garmentsJacketDS = new defJacket('garmentsJacketDS', DsRegistry, null, 0);		// resets values
							that.garmentsJacketVM = new GarmentsJacket(DsRegistry);
							that.garmentsJacketVM.subscribeTo('garmentsJacketDS');
						} else {
							that.jacketfirsttime = false;
							var currentcount = orderItem.garmentsList()[0].count();
							thegarmentsold = that.garmentsJacketDS.getStore().Jacket;
							that.garmentsJacketDS = new defJacket('garmentsJacketDS', DsRegistry, thegarmentsold, currentcount);		// resets values
							//	thegarments = that.garmentsJacketDS.getStore().Jacket;
							that.garmentsJacketVM = new GarmentsJacket(DsRegistry);
							that.garmentsJacketVM.subscribeTo('garmentsJacketDS');
						}
						break;
					case 1:
						if (that.garmentsPantDS == undefined) {
							that.pantfirsttime = true;
							that.garmentsPantDS = new defPant('garmentsPantDS', DsRegistry, null, 0);		// resets values
							that.garmentsPantVM = new GarmentsPant(DsRegistry);
							that.garmentsPantVM.subscribeTo('garmentsPantDS');
						} else {
							that.pantfirsttime = false;
							var currentcount = orderItem.garmentsList()[1].count();
							thegarmentsold = that.garmentsPantDS.getStore().Pant;
							that.garmentsPantDS = new defPant('garmentsPantDS', DsRegistry, thegarmentsold, currentcount);		// resets values
							//	thegarments = that.garmentsPantDS.getStore().Pant;
							that.garmentsPantVM = new GarmentsPant(DsRegistry);
							that.garmentsPantVM.subscribeTo('garmentsPantDS');
						}
						break;
					case 2:
						if (that.garmentsSuitDS == undefined) {
							that.suitfirsttime = true;
							that.garmentsSuitDS = new defSuit('garmentsSuitDS', DsRegistry, null, 0);		// resets values
							that.garmentsSuitVM = new GarmentsSuit(DsRegistry);
							that.garmentsSuitVM.subscribeTo('garmentsSuitDS');
						} else {
							that.suitfirsttime = false;
							var currentcount = orderItem.garmentsList()[2].count();
							thegarmentsold = that.garmentsSuitDS.getStore().suit;
							that.garmentsSuitDS = new defSuit('garmentsSuitDS', DsRegistry, thegarmentsold, currentcount);		// resets values
							//	thegarments = that.garmentsSuitDS.getStore().suit;
							that.garmentsSuitVM = new GarmentsSuit(DsRegistry);
							that.garmentsSuitVM.subscribeTo('garmentsSuitDS');
						}


						break;
					case 3:
						if (that.garmentsVestDS == undefined) {
							that.vestfirsttime = true;
							that.garmentsVestDS = new defVest('garmentsVestDS', DsRegistry, null, 0);		// resets values
							that.garmentsVestVM = new GarmentsVest(DsRegistry);
							that.garmentsVestVM.subscribeTo('garmentsVestDS');
						} else {
							that.vestfirsttime = false;
							var currentcount = orderItem.garmentsList()[3].count();
							thegarmentsold = that.garmentsVestDS.getStore().vest;
							that.garmentsVestDS = new defVest('garmentsVestDS', DsRegistry, thegarmentsold, currentcount);		// resets values
							//	thegarments = that.garmentsVestDS.getStore().vest;
							that.garmentsVestVM = new GarmentsVest(DsRegistry);
							that.garmentsVestVM.subscribeTo('garmentsVestDS');
						}
						break;
					case 4:
						if (that.garmentsShirtDS == undefined) {
							that.shirtfirsttime = true;
							that.garmentsShirtDS = new defShirt('garmentsShirtDS', DsRegistry, null, 0);		// resets values
							that.garmentsShirtVM = new GarmentsShirt(DsRegistry);
							that.garmentsShirtVM.subscribeTo('garmentsShirtDS');
						} else {
							that.shirtfirsttime = false;
							var currentcount = orderItem.garmentsList()[4].count();
							thegarmentsold = that.garmentsShirtDS.getStore().Shirt;
							that.garmentsShirtDS = new defShirt('garmentsShirtDS', DsRegistry, thegarmentsold, currentcount);		// resets values
							//thegarments = that.garmentsShirtDS.getStore().Shirt;
							that.garmentsShirtVM = new GarmentsShirt(DsRegistry);
							that.garmentsShirtVM.subscribeTo('garmentsShirtDS');
						}
						break;

				}

				var tA = that.garmentsList();
				for (var ind in that.garmentsList()) {
					if (that.garmentsList()[ind].id == data.id) {
						tA[ind].active = true;
					}
				}
				that.garmentsList(tA);
				spinner.style.display = 'none';
			};

			this.getVariantShirt = function (id) {
				for (var ind in that.garmentsShirtVM.shirtData) {
					if (that.garmentsShirtVM.shirtData[ind].variantId == id) {
						return that.garmentsShirtVM.shirtData[ind];
					}
				}
				return null;

			},


				this.disableGarment = function (data) {
					console.log('disableGarment');
					var tA = that.garmentsList();
					for (var ind in that.garmentsList()) {
						if (that.garmentsList()[ind].id == data.id) {
							tA[ind].active = false;
						}
					}
					that.garmentsList(tA);
				};


			editjacket = function (data) {
				console.log('editjacket');
				that.garmentsVM = that.garmentsJacketVM;
				posChangePage(that.garmentsVM.currentStep().target);
			};

			editPant = function (data) {
				console.log('editPant');
				that.garmentsVM = that.garmentsPantVM;
				that.MediaUploadVM.setMediaCallback(that.garmentsVM.mediaCallback);
				posChangePage(that.garmentsVM.currentStep().target);
			};

			editSuit = function (data) {
				that.garmentsVM = that.garmentsSuitVM;
				posChangePage(that.garmentsVM.currentStep().target);
			};


			editVest = function (data) {
				that.garmentsVM = that.garmentsVestVM;
				posChangePage(that.garmentsVM.currentStep().target);
			};


			this.editGarment = function (data) {
				var spinner = document.getElementById('loading_jp');
				spinner.style.display = "block";
				var myVar = setTimeout(function () {
					switch (data.id) {
						case 0:
							that.garmentsVM = that.garmentsJacketVM;
							posChangePage(that.garmentsVM.currentStep().target);
							break;
						case 1:
							that.garmentsVM = that.garmentsPantVM;
							posChangePage(that.garmentsVM.currentStep().target);
							break;
						case 2:
							that.garmentsVM = that.garmentsSuitVM;
							posChangePage(that.garmentsVM.currentStep().target);
							break;
						case 3:
							that.garmentsVM = that.garmentsVestVM;
							posChangePage(that.garmentsVM.currentStep().target);
							break;
						case 4:
							that.garmentsVM = that.garmentsShirtVM;
							posChangePage(that.garmentsVM.currentStep().target);
							break;
					}
				}, 200);
			};

			this.cancelMode = false;
			this.atmeasurements = ko.observable(false);
			this.currentBigStep = ko.observable(this.bigSteps()[0]);
			this.currentBigStep.subscribe(function (data) {
				if (that.cancelMode == true) {
					that.cancelMode = false;
					return;
				}

				switch (data.id) {
					case 0:
						if ($.isEmptyObject(that.custSelectVM.selectedCustomer()))
							posChangePage('#orderItemSelectCustomer');
						else
							posChangePage('#orderItemCustomerInfo');
						break;
					case 1:
						posChangePage('#orderItemPopulateGarments');
						break;
					case 2:
						that.measurementsVM.selectedGarment({ name: "none" });// = ko.observable({name: "none"});// FIX FOR FORCING THE POS TO FORGET THE SELECTED GARMENT IN CASE YOU GO BACK TO SELECT GARMENTS, AND RETURN TO MEASUREMENTS
						that.atmeasurements(true);
						posChangePage('#measurements');
						setTimeout(function () { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; }, 1500);
						break;
					case 3:
						posChangePage('#orderItemMedia');
						break;
					case 7:
						posChangePage('#orderItemVideo');
						break;
					case 4:
						posChangePage('#bodyshape');
						break;
					case 5:
						posChangePage('#orderItemSelectGarment');
						break;
					case 6:
						posChangePage('#orderItemReview');
						break;
					case 8:
						that.atmeasurements(true);
						posChangePage('#orderItemCopyGarments');
						break;
				}
			});

			this.currentBigStep2 = ko.observable(this.bigSteps()[0]);
			this.currentBigStep2.subscribe(function (data) {
				if (that.atmeasurements() == true) {
					that.currentBigStep(data);
				}
			});

			this.updateMeasurements = function (data) {

				var item = {
					"jacketChest": that.measurementsVM.jacketChest(),
					"jacketStomach": that.measurementsVM.jacketStomach(),
					"jacketHips": that.measurementsVM.jacketHips(),
					"jacketfullShoulder": that.measurementsVM.jacketfullShoulder(),
					"jacketsleeveL": that.measurementsVM.jacketsleeveL(),
					"jacketsleeveR": that.measurementsVM.jacketsleeveR(),
					"jacketFront": that.measurementsVM.jacketFront(),
					"jacketBack": that.measurementsVM.jacketBack(),
					"jacketBicep": that.measurementsVM.jacketBicep(),
					"jacketforeArms": that.measurementsVM.jacketforeArms(),
					"jacketWrist": that.measurementsVM.jacketWrist(),
					"jacketLength": that.measurementsVM.jacketLength(),
					"jacketLapel": that.measurementsVM.jacketLapel(),
					"jacketNeck": that.measurementsVM.jacketNeck(),
					"jacketZeropoint": that.measurementsVM.jacketZeropoint(),
					"jacketZeropointfront": that.measurementsVM.jacketZeropointfront(),
					"jacketZeropointback": that.measurementsVM.jacketZeropointback(),
					"jacketRightFrontToZero": that.measurementsVM.jacketRightFrontToZero(),
					"jacketRightBackToZero": that.measurementsVM.jacketRightBackToZero(),
					"jacketTenseBicep": that.measurementsVM.jacketTenseBicep(),
					"jacketComments": that.measurementsVM.jacketComments(),
					"pantWaist": that.measurementsVM.pantWaist(),
					"pantHips": that.measurementsVM.pantHips(),
					"thigh": that.measurementsVM.thigh(),
					"cuffs": that.measurementsVM.cuffs(),
					"pantFrontCrotch": that.measurementsVM.pantFrontCrotch(),
					"pantInseam": that.measurementsVM.pantInseam(),
					"pantLength": that.measurementsVM.pantLength(),
					"knee": that.measurementsVM.knee(),
					"calf": that.measurementsVM.calf(),
					"crotch": that.measurementsVM.crotch(),
					"zipper": that.measurementsVM.zipper(),
					"pantAngledHems": that.measurementsVM.pantAngledHems(),
					"pantAngledWaist": that.measurementsVM.pantAngledWaist(),
					"pantWaistAngle": that.measurementsVM.pantWaistAngle(),
					"pantsComments": that.measurementsVM.pantsComments(),
					"shirtChest": that.measurementsVM.shirtChest(),
					"shirtStomach": that.measurementsVM.shirtStomach(),
					"shirtHips": that.measurementsVM.shirtHips(),
					"shirtfullshoulder": that.measurementsVM.shirtfullshoulder(),
					"shirtsleeveL": that.measurementsVM.shirtsleeveL(),
					"shirtsleeveR": that.measurementsVM.shirtsleeveR(),
					"shirtNeck": that.measurementsVM.shirtNeck(),
					"shirtFront": that.measurementsVM.shirtFront(),
					"shirtBack": that.measurementsVM.shirtBack(),
					"shirtLength": that.measurementsVM.shirtLength(),
					"shirtBicep": that.measurementsVM.shirtBicep(),
					"shirtWrist": that.measurementsVM.shirtWrist(),
					"shirtZeroPoint": that.measurementsVM.shirtZeroPoint(),
					"shirtFrontToZero": that.measurementsVM.shirtFrontToZero(),
					"shirtBackToZero": that.measurementsVM.shirtBackToZero(),
					"shirtRightFrontToZero": that.measurementsVM.shirtRightFrontToZero(),
					"shirtRightBackToZero": that.measurementsVM.shirtRightBackToZero(),
					"shirtTenseBicep": that.measurementsVM.shirtTenseBicep(),
					"shirtForearm": that.measurementsVM.shirtForearm(),
					"shirtWristwatch": that.measurementsVM.shirtWristwatch(),
					"shirtComments": that.measurementsVM.shirtComments(),
					"vestChest": that.measurementsVM.vestChest(),
					"vestStomach": that.measurementsVM.vestStomach(),
					"vestNeck": that.measurementsVM.vestNeck(),
					"vestHips": that.measurementsVM.vestHips(),
					"vestLength": that.measurementsVM.vestLength(),
					"vestVLength": that.measurementsVM.vestVLength(),
					"vestfullShoulder": that.measurementsVM.vestfullShoulder(),
					"vestHalfShoulderWidth": that.measurementsVM.vestHalfShoulderWidth(),
					"vestComments": that.measurementsVM.vestComments(),
					"jacketDate": Math.round(new Date().getTime() / 1000),
					"pantsDate": Math.round(new Date().getTime() / 1000),
					"shirtDate": Math.round(new Date().getTime() / 1000),
					"vestDate": Math.round(new Date().getTime() / 1000)
				};

				var tVar = this.dsRegistry.getDatasource('measurementsDS').getStore();
				tVar[0] = item;
				this.dsRegistry.getDatasource('measurementsDS').setStore(tVar);


				measurementsDS2 = new SyncableDatasource('measurementsDS', 'measurementsDS', dsRegistry, 'update_customer_measurements_endpoint');
				measurementsDS2.sync();

				if (customersVM.systemMode != "order") {
					posChangePage('#customerInfo');
					$.jGrowl("Succesfully updated customer's  measurements", {
						header: 'Update'
					});
				}

			};


			this.measurementserror = function (data) {
				var errorstring = '';
				if (that.future() == false) {
					if (that.dumpOrder().Jacket || that.dumpOrder().Suit) {
						if (that.measurementsVM.jacketChest() == "" || that.measurementsVM.jacketChest() <= 0 || that.measurementsVM.jacketChest() > 99) {
							errorstring += 'Jacket Chest, ';
						}
						if (that.measurementsVM.jacketStomach() == "" || that.measurementsVM.jacketStomach() <= 0 || that.measurementsVM.jacketStomach() > 99) {
							errorstring += 'Jacket Stomach, ';
						}
						if (that.measurementsVM.jacketHips() == "" || that.measurementsVM.jacketHips() <= 0 || that.measurementsVM.jacketHips() > 99) {
							errorstring += 'Jacket Hips, ';
						}
						if (that.measurementsVM.jacketFront() == "" || that.measurementsVM.jacketFront() <= 0 || that.measurementsVM.jacketFront() > 99) {
							errorstring += 'Jacket Front, ';
						}
						if (that.measurementsVM.jacketBack() == "" || that.measurementsVM.jacketBack() <= 0 || that.measurementsVM.jacketBack() > 99) {
							errorstring += 'Jacket Chest, ';
						}
						if (that.measurementsVM.jacketfullShoulder() == "" || that.measurementsVM.jacketfullShoulder() <= 0 || that.measurementsVM.jacketfullShoulder() > 99) {
							errorstring += 'Jacket Full Shoulder, ';
						}
						if (that.measurementsVM.jacketsleeveL() == "" || that.measurementsVM.jacketsleeveL() <= 0 || that.measurementsVM.jacketsleeveL() > 99) {
							errorstring += 'Jacket Left Sleeve, ';
						}
						if (that.measurementsVM.jacketsleeveR() == "" || that.measurementsVM.jacketsleeveR() <= 0 || that.measurementsVM.jacketsleeveR() > 99) {
							errorstring += 'Jacket Right Sleeve, ';
						}
						if (that.measurementsVM.jacketNeck() == "" || that.measurementsVM.jacketNeck() <= 0 || that.measurementsVM.jacketNeck() > 99) {
							errorstring += 'Jacket Neck, ';
						}
						if (that.measurementsVM.jacketBicep() == "" || that.measurementsVM.jacketBicep() <= 0 || that.measurementsVM.jacketBicep() > 99) {
							errorstring += 'Jacket Bicep, ';
						}
						if (that.measurementsVM.jacketWrist() == "" || that.measurementsVM.jacketBicep() <= 0 || that.measurementsVM.jacketBicep() > 99) {
							errorstring += 'Jacket Wrist, ';
						}
						if (that.measurementsVM.jacketLength() == "" || that.measurementsVM.jacketBicep() <= 0 || that.measurementsVM.jacketBicep() > 99) {
							errorstring += 'Jacket Length, ';
						}
						if (that.measurementsVM.jacketZeropoint() == "" || that.measurementsVM.jacketBicep() <= 0 || that.measurementsVM.jacketBicep() > 99) {
							errorstring += 'Jacket Zero Point, ';
						}
						if (that.measurementsVM.jacketZeropointfront() == "" || that.measurementsVM.jacketZeropointfront() <= 0 || that.measurementsVM.jacketZeropointfront() > 99) {
							errorstring += 'Jacket Front S. to Zero, ';
						}
						if (that.measurementsVM.jacketZeropointback() == "" || that.measurementsVM.jacketZeropointback() <= 0 || that.measurementsVM.jacketZeropointback() > 99) {
							errorstring += 'Jacket Back S. to Zero, ';
						}
						if (that.measurementsVM.jacketRightFrontToZero() == "" || that.measurementsVM.jacketRightFrontToZero() <= 0 || that.measurementsVM.jacketRightFrontToZero() > 99) {
							errorstring += 'Jacket Front S. to Zero, ';
						}
						if (that.measurementsVM.jacketRightBackToZero() == "" || that.measurementsVM.jacketRightBackToZero() <= 0 || that.measurementsVM.jacketRightBackToZero() > 99) {
							errorstring += 'Jacket Back S. to Zero, ';
						}
					}
					if (that.dumpOrder().Pant || that.dumpOrder().Suit) {
						if (that.measurementsVM.pantWaist() == "" || that.measurementsVM.pantWaist() <= 0 || that.measurementsVM.pantWaist() > 99) {
							errorstring += 'Pants Waist, ';
						}
						if (that.measurementsVM.pantHips() == "" || that.measurementsVM.pantHips() <= 0 || that.measurementsVM.pantHips() > 99) {
							errorstring += 'Pants Hips, ';
						}
						if (that.measurementsVM.thigh() == "" || that.measurementsVM.thigh() <= 0 || that.measurementsVM.thigh() > 99) {
							errorstring += 'Pants Thigh, ';
						}
						if (that.measurementsVM.cuffs() == "" || that.measurementsVM.cuffs() <= 0 || that.measurementsVM.cuffs() > 99) {
							errorstring += 'Pants Cuffs, ';
						}
						if (that.measurementsVM.pantLength() == "" || that.measurementsVM.pantLength() <= 0 || that.measurementsVM.pantLength() > 99) {
							errorstring += 'Pants Length, ';
						}
						if (that.measurementsVM.knee() == "" || that.measurementsVM.knee() <= 0 || that.measurementsVM.knee() > 99) {
							errorstring += 'Knee, ';
						}
						if (that.measurementsVM.calf() == "" || that.measurementsVM.calf() <= 0 || that.measurementsVM.calf() > 99) {
							errorstring += 'Calf, ';
						}
						if (that.measurementsVM.crotch() == "" || that.measurementsVM.crotch() <= 0 || that.measurementsVM.crotch() > 99) {
							errorstring += 'Pants Cuffs, ';
						}
					}
					if (that.dumpOrder().Shirt) {
						if (that.measurementsVM.shirtChest() == "" || that.measurementsVM.shirtChest() <= 0 || that.measurementsVM.shirtChest() > 99) {
							errorstring += 'Shirt Chest, ';
						}
						if (that.measurementsVM.shirtStomach() == "" || that.measurementsVM.shirtStomach() <= 0 || that.measurementsVM.shirtStomach() > 99) {
							errorstring += 'Shirt Stomach, ';
						}
						if (that.measurementsVM.shirtfullshoulder() == "" || that.measurementsVM.shirtfullshoulder() <= 0 || that.measurementsVM.shirtfullshoulder() > 99) {
							errorstring += 'Shirt Full Shoulder, ';
						}
						if (that.measurementsVM.shirtsleeveL() == "" || that.measurementsVM.shirtsleeveL() <= 0 || that.measurementsVM.shirtsleeveL() > 99) {
							errorstring += 'Shirt Sleeve Left, ';
						}
						if (that.measurementsVM.shirtsleeveR() == "" || that.measurementsVM.shirtsleeveR() <= 0 || that.measurementsVM.shirtsleeveR() > 99) {
							errorstring += 'Shirt Sleeve Right, ';
						}
						if (that.measurementsVM.shirtNeck() == "" || that.measurementsVM.shirtNeck() <= 0 || that.measurementsVM.shirtNeck() > 99) {
							errorstring += 'Shirt Neck, ';
						}
						if (that.measurementsVM.shirtFront() == "" || that.measurementsVM.shirtFront() <= 0 || that.measurementsVM.shirtFront() > 99) {
							errorstring += 'Shirt Front, ';
						}
						if (that.measurementsVM.shirtBack() == "" || that.measurementsVM.shirtBack() <= 0 || that.measurementsVM.shirtBack() > 99) {
							errorstring += 'Shirt Back, ';
						}
						if (that.measurementsVM.shirtLength() == "" || that.measurementsVM.shirtLength() <= 0 || that.measurementsVM.shirtLength() > 99) {
							errorstring += 'Shirt Shirt Length, ';
						}
						if (that.measurementsVM.shirtBicep() == "" || that.measurementsVM.shirtBicep() <= 0 || that.measurementsVM.shirtBicep() > 99) {
							errorstring += 'Shirt Bicep, ';
						}
						if (that.measurementsVM.shirtHips() == "" || that.measurementsVM.shirtHips() <= 0 || that.measurementsVM.shirtHips() > 99) {
							errorstring += 'Shirt Hips, ';
						}
						if (that.measurementsVM.shirtWrist() == "" || that.measurementsVM.shirtWrist() <= 0 || that.measurementsVM.shirtWrist() > 99) {
							errorstring += 'Shirt Wrist, ';
						}
						if (that.measurementsVM.shirtRightFrontToZero() == "" || that.measurementsVM.shirtRightFrontToZero() <= 0 || that.measurementsVM.shirtRightFrontToZero() > 99) {
							errorstring += 'Right Front to Zero, ';
						}
						if (that.measurementsVM.shirtRightBackToZero() == "" || that.measurementsVM.shirtRightBackToZero() <= 0 || that.measurementsVM.shirtRightBackToZero() > 99) {
							errorstring += 'Right Back to Zero, ';
						}
					}
					if (that.dumpOrder().Vest) {
						if (that.measurementsVM.vestHips() == "" || that.measurementsVM.vestHips() <= 0 || that.measurementsVM.vestHips() > 99) {
							errorstring += 'Vest Hips at V length, ';
						}
						if (that.measurementsVM.vestLength() == "" || that.measurementsVM.vestLength() <= 0 || that.measurementsVM.vestLength() > 99) {
							errorstring += 'Vest Length, ';
						}
						if (that.measurementsVM.vestChest() == "" || that.measurementsVM.vestChest() <= 0 || that.measurementsVM.vestChest() > 99) {
							errorstring += 'Vest Chest, ';
						}
						if (that.measurementsVM.vestStomach() == "" || that.measurementsVM.vestStomach() <= 0 || that.measurementsVM.vestStomach() > 99) {
							errorstring += 'Vest Stomach, ';
						}
						if (that.measurementsVM.vestNeck() == "" || that.measurementsVM.vestNeck() <= 0 || that.measurementsVM.vestNeck() > 99) {
							errorstring += 'Vest Neck, ';
						}
						if (that.measurementsVM.vestHalfShoulderWidth() == "" || that.measurementsVM.vestHalfShoulderWidth() <= 0 || that.measurementsVM.vestHalfShoulderWidth() > 99) {
							errorstring += 'Vest Half Shoulder Length, ';
						}
						if (that.measurementsVM.vestfullShoulder() == "" || that.measurementsVM.vestfullShoulder() <= 0 || that.measurementsVM.vestfullShoulder() > 99) {
							errorstring += 'Vest Full Shoulder, ';
						}
					}
				}
				if (errorstring.length > 0) {
					errorstring = errorstring.trim();
					errorstring = errorstring.substring(0, errorstring.length - 1);
				}

				return errorstring;
			}



			this.errorfunction = function (data) {
				var errorstring = '';

				errorstring += that.measurementserror();

				if (errorstring.length > 0) {
					error = true;
					errorstring = "MEASUREMENTS MISSING\n" + errorstring + "\n";
				}

				if (that.future() == false) {
					if (that.checkGarmentsForFabrics() == false) {
						errorstring += '\nFABRICS ERROR:\nThe garments do not have a fabric selected.\n';
						error = true;
					}
					if (that.paymentMethod() == '' || that.paymentMethod() == undefined) {
						errorstring += '\nPAYMENT METHOD ERROR:\nYou must select a payment method.\n';
						error = true;
					}
					if (that.paymentMethod() == 'Bank transfer' && !that.bank()) {
						errorstring += '\nPAYMENT METHOD ERROR:\nYou must select a bank.\n';
						error = true;
					}
					if ((that.paymentMethod() == 'Credit card V/M' || that.paymentMethod() == 'Credit card Amex' || that.paymentMethod() == 'Credit card' || that.paymentMethod() == 'Eftpos') && that.creditCardImage() == '') {
						errorstring += '\nPAYMENT METHOD ERROR:\nYou must upload a photo of the receipt for credit card or Eftpos payments.\n';
						error = true;
					}

				}

				if (that.order_city() == null || that.order_city() == undefined) {
					errorstring += '\nORDER CITY ERROR:\nYou must select an order city.\n';
					error = true;
				}

				if (that.acceptTerms() == false) {
					errorstring += '\nTERMS & CONDITIONS ERROR:\nYou must accept the terms and conditions.\n';
					error = true;
				}

				return errorstring;
			};

			this.initializeDesignHtml = function () {
				var mainlistdisplays = document.getElementsByClassName("MainListElementDisplay");
				var mainlistelements = document.getElementsByClassName("MainListElement");
				var garmentelements = document.getElementsByClassName("triggerr");
				var checkboxes = document.getElementsByClassName("listsimpleclick");
				var garmentsnumber = garmentelements.length / mainlistelements.length;

				var zeroelementchecked = [];
				var zeroelement = true;

				var garmentpants = false;
				if (document.getElementsByName("PantFabricText").length > 0) {
					garmentpants = true;
				}


				for (var a = 0; a < mainlistelements.length; a++) {
					if (mainlistdisplays[a].style.display == 'none') {
						zeroelement = true;
					} else {
						zeroelement = false;
					}
					var haschecked = false;
					for (var b = 0; b < garmentsnumber; b++) {
						var index = a * garmentsnumber + b;
						if (checkboxes[index].classList.contains('selecteds')) {
							if (garmentpants == true) {
								if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[b].parentSuitTimestampID == '') {
									mainlistelements[a].classList.add('activate');
								}
							} else {
								mainlistelements[a].classList.add('activate');
							}
							garmentelements[index].classList.remove('offtrigger');
							garmentelements[index].classList.add('showtrigger');

							if (zeroelement == true) {
								zeroelementchecked[b] = true;
							} else {
								zeroelementchecked[b] = false;
							}
						} else {
							if (zeroelement == true) {
								zeroelementchecked[b] = false;
							}
							if (zeroelementchecked[b] == false) {
								garmentelements[index].classList.add('offtrigger');
							}
						}

					}
				}

				try {
					document.getElementById("selectedfabric").innerHTML = "none";
				} catch (e) {
					console.log(e);
				}
				try {
					document.getElementById("fabricrange").innerHTML = "0";
				} catch (e) {
					console.log(e);
				}
				try {
					document.getElementById("composition").innerHTML = "0";
				} catch (e) {
					console.log(e);
				}
				try {
					document.getElementById("fabricinput").value = "";
				} catch (e) {
					console.log(e);
				}
				try {
					document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
				} catch (e) {
					console.log(e);
				}

				return false;
			};




			this.initializeHtmlFromExtraPants = function () {

				this.lockselects = true;

				var pantsnumber = document.getElementsByName("pantFitPreview").length;

				var pantPleats = this.dsRegistry.getDatasource('pantPleatsDS').getStore();
				var pantFit = this.dsRegistry.getDatasource('pantFitDS').getStore();
				var PantFit = ["Fitted", "Semi Fitted", "Standard Fit"];
				var pantPockets = this.dsRegistry.getDatasource('pantPocketsDS').getStore();
				var bpantPockets = this.dsRegistry.getDatasource('bpantPocketsDS').getStore();
				var pbackpocketMethod = ["Both", "Right", "Left"];
				var beltLoopStyle = this.dsRegistry.getDatasource('beltLoopStyleDS').getStore();

				console.log("pantsnumber: " + pantsnumber);

				for (var a = 0; a < pantsnumber; a++) {
					console.log("CHECKING PANT " + a);

					try {
						for (var b = 0; b < PantFit.length; b++) {
							if (document.getElementsByName("PantFitSpan")[a].innerHTML == PantFit[b]) {
								document.getElementsByName("PantFit")[a].getElementsByTagName('option')[b].selected = true;
							}
						}
					} catch (e) { ; }

					try {
						for (var b = 0; b < pantPleats.length; b++) {
							if (document.getElementsByName("pantPleatsSpan")[a].innerHTML == pantPleats[b].title) {
								document.getElementsByName("pantPleats")[a].getElementsByTagName('option')[b].selected = true;
							}
						}
					} catch (e) { ; }

					try {
						for (var b = 0; b < pantFit.length; b++) {
							if (document.getElementsByName("pantFitSpan")[a].innerHTML == pantFit[b].title) {
								document.getElementsByName("pantFit")[a].getElementsByTagName('option')[b].selected = true;
							}
						}
					} catch (e) { ; }

					try {
						for (var b = 0; b < pantPockets.length; b++) {
							if (document.getElementsByName("pantPocketsSpan")[a].innerHTML == pantPockets[b].title) {
								document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[b].selected = true;
								/*  try{
									document.getElementsByName("pantPockets2")[a].getElementsByTagName('option')[b].selected = true;
								}catch(e){
									;
								}	*/
							}
						}
					} catch (e) { ; }

					try {
						for (var b = 0; b < bpantPockets.length; b++) {
							if (document.getElementsByName("bpantPocketsSpan")[a].innerHTML == bpantPockets[b].title) {
								document.getElementsByName("bpantPockets")[a].getElementsByTagName('option')[b].selected = true;
							}
						}
					} catch (e) { ; }

					try {
						for (var b = 0; b < pbackpocketMethod.length; b++) {
							if (document.getElementsByName("pbackpocketMethodSpan")[a].innerHTML == pbackpocketMethod[b]) {
								document.getElementsByName("pbackpocketMethod")[a].getElementsByTagName('option')[b].selected = true;
							}
						}
					} catch (e) { ; }

					try {
						for (var b = 0; b < beltLoopStyle.length; b++) {
							if (document.getElementsByName("beltLoopStyleSpan")[a].innerHTML == beltLoopStyle[b].title) {
								document.getElementsByName("beltLoopStyle")[a].getElementsByTagName('option')[b].selected = true;
							}
						}
					} catch (e) { ; }


					if ("createEvent" in document) {
						var evt = document.createEvent("HTMLEvents");
						evt.initEvent("change", true, true);
						document.getElementsByName('pantPleats')[a].dispatchEvent(evt);
						document.getElementsByName('PantFit')[a].dispatchEvent(evt);
						document.getElementsByName('pantFit')[a].dispatchEvent(evt);
						document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
						//	document.getElementsByName('pantPockets2')[a].dispatchEvent(evt);
						document.getElementsByName('bpantPockets')[a].dispatchEvent(evt);
						document.getElementsByName('beltLoopStyle')[a].dispatchEvent(evt);
					} else {
						document.getElementsByName('pantPleats')[a].fireEvent("onchange");
						document.getElementsByName('PantFit')[a].fireEvent("onchange");
						document.getElementsByName('pantFit')[a].fireEvent("onchange");
						document.getElementsByName('pantPockets')[a].fireEvent("onchange");
						//		document.getElementsByName('pantPockets2')[a].fireEvent("onchange");
						document.getElementsByName('bpantPockets')[a].fireEvent("onchange");
						document.getElementsByName('beltLoopStyle')[a].fireEvent("onchange");
					}

				}
				setTimeout(function () { console.log("this.lockselects: " + orderItem.lockselects); console.log("RELEASING LOCKSELECTS"); orderItem.lockselects = false; console.log("this.lockselects: " + orderItem.lockselects); }, 2000);

				return false;
			};



			this.modifyHtmlFromExtraPants = function () {

				var pantsnumber = document.getElementsByName("pantFitPreview").length;
				console.log("modifyHtmlFromExtraPants lockselects " + this.lockselects);
				if (this.lockselects == false) {

					for (var a = 0; a < pantsnumber; a++) {

						document.getElementsByName("pantFitPreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.preview;
						document.getElementsByName("pantPleatsPreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.preview;
						document.getElementsByName("pantPocketsPreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.preview;
						document.getElementsByName("beltLoopStylePreview")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.preview;
						document.getElementsByName("bpantPocketsPreviewLeft")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.previewleft;
						document.getElementsByName("bpantPocketsPreviewRight")[a].src = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.previewright;

						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs == true && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id === 'Normal_Straight') {
							document.getElementsByName("pantCuffsPreviewNormal")[a].style.display = "";
							document.getElementsByName("pantCuffsPreviewBoot")[a].style.display = "none";
							document.getElementsByName("pantCuffsPreviewNarrow")[a].style.display = "none";
						} else if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs == true && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id === 'Boot_Flare') {
							document.getElementsByName("pantCuffsPreviewNormal")[a].style.display = "none";
							document.getElementsByName("pantCuffsPreviewBoot")[a].style.display = "";
							document.getElementsByName("pantCuffsPreviewNarrow")[a].style.display = "none";
						} else if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs == true && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id === 'Narrow_Slim') {
							document.getElementsByName("pantCuffsPreviewNormal")[a].style.display = "none";
							document.getElementsByName("pantCuffsPreviewBoot")[a].style.display = "none";
							document.getElementsByName("pantCuffsPreviewNarrow")[a].style.display = "";
						}

						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod == 'Both' || this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod === 'Left') {
							document.getElementsByName("bpantPocketsPreviewLeft")[a].style.display = "";
						} else {
							document.getElementsByName("bpantPocketsPreviewLeft")[a].style.display = "none";
						}

						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod == 'Both' || this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pbackpocketMethod === 'Right') {
							document.getElementsByName("bpantPocketsPreviewRight")[a].style.display = "";
						} else {
							document.getElementsByName("bpantPocketsPreviewRight")[a].style.display = "none";
						}

						document.getElementsByName("pantFitFrame")[a].classList.remove('completedgarments');
						document.getElementsByName("pantPleatsFrame")[a].classList.remove('completedgarments');
						document.getElementsByName("pantPocketsFrame")[a].classList.remove('completedgarments');
						document.getElementsByName("bpantPocketsFrame")[a].classList.remove('completedgarments');
						document.getElementsByName("beltLoopStyleFrame")[a].classList.remove('completedgarments');

						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.id != '0') {
							document.getElementsByName("pantFitFrame")[a].classList.add('completedgarments');
						}
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.id != '0') {
							document.getElementsByName("pantPleatsFrame")[a].classList.add('completedgarments');
						}
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.id != '0') {
							document.getElementsByName("pantPocketsFrame")[a].classList.add('completedgarments');
						}
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.id != '0') {
							document.getElementsByName("bpantPocketsFrame")[a].classList.add('completedgarments');
						}
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.id != '0') {
							document.getElementsByName("beltLoopStyleFrame")[a].classList.add('completedgarments');
						}


						document.getElementsByName("PantFitSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantFit;
						document.getElementsByName("pantFitSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantFit.title;
						document.getElementsByName("pantPleatsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title;
						document.getElementsByName("pantPocketsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title;
						document.getElementsByName("bpantPocketsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].bpantPockets.title;
						document.getElementsByName("beltLoopStyleSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.title;
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].beltLoopStyle.title != 'No Belt Loop') {
							document.getElementsByName("beltLoopStyleSpanVisible")[a].style.display = "";
						} else {
							document.getElementsByName("beltLoopStyleSpanVisible")[a].style.display = "none";
						}
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantCuffs != true) {
							document.getElementsByName("pantCuffsSpanVisible")[a].style.display = "";
						} else {
							document.getElementsByName("pantCuffsSpanVisible")[a].style.display = "none";
						}
						if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].PantsBracesProvisions != true) {
							document.getElementsByName("PantsBracesProvisionsSpanVisible")[a].style.display = "";
						} else {
							document.getElementsByName("PantsBracesProvisionsSpanVisible")[a].style.display = "none";
						}

						try {
							if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title != 'Select Pleats' && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title != 'No Pleats'
								&& this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPleats.title !== 'Single Pleat') {

								if (this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title != 'Select Pocket' && this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title != 'No Pocket'
									&& this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title != 'Slanted') {

									this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets = this.dsRegistry.getDatasource('pantPocketsDS').getStore()[2];
									document.getElementsByName("pantPockets")[a].getElementsByTagName('option')[2].selected = true;
									if ("createEvent" in document) {
										var evt = document.createEvent("HTMLEvents");
										evt.initEvent("change", true, true);
										document.getElementsByName('pantPockets')[a].dispatchEvent(evt);
									} else {
										document.getElementsByName('pantPockets')[a].fireEvent("onchange");
									}
									document.getElementsByName("pantPocketsSpan")[a].innerHTML = this.dsRegistry.getDatasource('garmentsPantDS').getStore().Pant[a].pantPockets.title;
								}
							}
						} catch (e) {
							console.log("EXCEPTION 1");
						}
					}

				}

				return false;
			};




			///////////////////////////////////////////////////////////////               
			this.sync = function () {
				console.log('APP.JS SYNC');
				that.alterDeposit(false);
				var errorstring = that.errorfunction();

				if (errorstring.length > 2) {
					customAlert(errorstring);


				} else {
					var spinner = document.getElementById('loading_jp');
					spinner.style.display = "block";



					var fitlinephotosarray = [];
					var fitlinephotos = localStorage.getItem('fitlinephotos');
					fitlinephotos = (fitlinephotos == null) ? [] : JSON.parse(fitlinephotos);
					if (localStorage.getItem('fitlinephotos') != null) {
						if (localStorage.getItem('fitlinephotos').length > 0) {
							for (var i = 0, len = fitlinephotos.length; i < len; i++) {
								if (fitlinephotos[i].customer_id == that.custSelectVM.selectedCustomer().server_id) {
									fitlinephotosarray.push(fitlinephotos[i].picture);
								}
							}
						}
					}

					var fitlinetagsarray = [];
					var fitlinetags = localStorage.getItem('fitlinetags');
					fitlinetags = (fitlinetags == null) ? [] : JSON.parse(fitlinetags);
					if (localStorage.getItem('fitlinetags') != null) {
						if (localStorage.getItem('fitlinetags').length > 0) {
							for (var j = 0; j < fitlinetags.length; j++) {
								for (var i = 0; i < fitlinephotos.length; i++) {
									if (fitlinetags[j].image_id == fitlinephotos[i].image_id) {
										fitlinetagsarray.push(fitlinetags[j]);
									}
								}
							}
						}
					}

					var signatureid = 0;
					var signaturephotos = localStorage.getItem('signature');
					signaturephotos = (signaturephotos == null) ? [] : JSON.parse(signaturephotos);
					if (localStorage.getItem('signature') != null) {
						if (localStorage.getItem('signature').length > 0) {
							signatureid = signaturephotos[0].image_id;
						}
					}
					//console.log("signatureid: " + signatureid);

					var creditcardid = 0;
					var creditcardphotos = localStorage.getItem('CreditCardPhoto');
					creditcardphotos = (creditcardphotos == null) ? [] : JSON.parse(creditcardphotos);
					if (localStorage.getItem('CreditCardPhoto') != null) {
						if (localStorage.getItem('CreditCardPhoto').length > 0) {
							creditcardid = creditcardphotos[0].image_id;
						}
					}

					var urgentDigit;
					if (that.urgent() == true) {
						urgentDigit = 1;
					} else {
						urgentDigit = 0;
					}

					var futureDigit;
					if (that.future() == true) {
						futureDigit = 1;
					} else {
						futureDigit = 0;
					}

					var companyinvoiceDigit;
					if (that.companyinvoice() == true) {
						companyinvoiceDigit = 1;
					} else {
						companyinvoiceDigit = 0;
					}

					var discountCodePost;
					var discountAmountPost;
					if (that.checkDiscountCode() == 1) {
						discountCodePost = that.discountCode();
						discountAmountPost = that.calculateDiscount();
					} else {
						discountCodePost = '';
						discountAmountPost = 0;
					}

					var paymethod;
					if (that.paymentMethod() == '' || that.paymentMethod() == undefined) {
						paymethod = '';
					} else {
						paymethod = that.paymentMethod();
					}

					var fittingcitypost;
					if (that.fitting_city() == '' || that.fitting_city() == undefined) {
						this.cities = ko.observableArray(dsRegistry.getDatasource('citiesDS').getStore());
						for (i in this.cities()) {
							if (this.cities()[i].location_name == 'none') {
								fittingcitypost = this.cities()[i].locations_id;
							}
						}
					} else {
						fittingcitypost = that.fitting_city();
					}

					var extracostspost = that.extracosts;
					for (var x = 0; x < extracostspost().length; x++) {
						if (extracostspost()[x].extracosttext().length == 0 || (extracostspost()[x].extracost() * 1.0) <= 0) {
							extracostspost.remove(extracostspost()[x]);
							x--;
						}
					}

					if (that.deposit() > that.calculateTotalPrice()) {
						customAlert("You can not receive more than the total cost!");
						spinner.style.display = "none";
						return;
					}
					for (var x in that.FabricPrice()) {
						console.log('asd');
						for (var y in that.FabricPrice()[x].garmentList) {
							console.log('dsa');
							try {
								console.log(Number(that.FabricPrice()[x].garmentList[y].price()));
								if (!Number(that.FabricPrice()[x].garmentList[y].price()) || Number(that.FabricPrice()[x].garmentList[y].price()) <= 0) {

									customAlert("Inform the all garments prices!");
									spinner.style.display = "none";
									return;
								}
							}
							catch (e) {
								customAlert("Inform the all garments prices!");
								spinner.style.display = "none";
								return;
							}
						}
					}
					if (that.copyGarment() && !that.CopyGarmentJacket() && !that.CopyGarmentPant() && !that.CopyGarmentVest() && !that.CopyGarmentShirt()) {
						customAlert("Inform what garments do you wish to copy.");
						spinner.style.display = "none";
						return;
					}
					data = {
						device_id: typeof device != "undefined" ? device.uuid : "",
						send_invoice_email: that.send_invoice_email_checkbox(),
						order: that.dumpOrder(),
						customer: that.custSelectVM.selectedCustomer(),
						measurements: that.measurementsVM.flushModel(),
						fitlinejacket: that.measurementsVM.fitlineJacketVM.flushModel(),
						fitlinepants: that.measurementsVM.fitlinePantsVM.flushModel(),
						fitline_images: fitlinephotosarray,
						fitlinetags: fitlinetagsarray,
						customer_images: that.customerImage.getSyncInfo(),
						customer_videos: that.customerVideo.getSyncInfo(),
						customer_images_notes: that.orderMediaNotes(),
						bodyshape: that.bodyshapeVM.flushModel(),
						user_id: authCtrl.userInfo.user_id,
						review: {
							urgent: urgentDigit,
							future: futureDigit,
							notes: that.reviewNotes(),
							totalcost: that.computePriceWithDiscount(),
							GST: that.getGST() * 100,
							deposit: that.deposit(),
							extracosts: ko.toJSON(extracostspost()),
							discount: discountAmountPost,
							discountcode: discountCodePost,
							fitting_city: fittingcitypost,
							order_city: that.order_city(),
							paymentMethod: paymethod,
							bank: that.bank(),
							currency: that.getSellerCurrency(),
							referer: that.referer(),
							DOP_day: that.DOP_day(),
							DOP_month: that.DOP_month(),
							customer_signature: signatureid,
							customer_credit_card: creditcardid,
							companyinvoice: companyinvoiceDigit,
							invoicecompanyname: that.invoicecompanyname(),
							invoiceproductdescription: that.invoiceproductdescription()
						},
						copy_garment: {
							copyJacket: that.CopyGarmentJacket(),
							copyPant: that.CopyGarmentPant(),
							copyVest: that.CopyGarmentVest(),
							copyShirt: that.CopyGarmentShirt(),
							comments: that.copyGarmentComments(),
							images: that.copyGarmentImage.getSyncInfo(),
							videos: that.copyGarmentVideo.getSyncInfo()
						},
						customer_fabric: that.FabricList()
					};

					var datatopost0 = JSON.stringify(data);
					datatopost0 = datatopost0.replace("\"isDirty\":true,", "");
					while (datatopost0.indexOf("??") >= 0) {//datatopost0.contains("??")){
						datatopost0 = datatopost0.replace("??", "?");
					}
					//datatopost0 = datatopost0.replace(/&/g,'and');
					var datatopost = 'data='.concat(datatopost0);
					console.log(datatopost);

					request = $.ajax({
						type: 'POST',
						url: BUrl + 'client/order_endpoint6',
						timeout: 60000, // in milliseconds, use 25000 out of testing
						dataType: 'json',
						data: { data: datatopost0 },
						error: function (dataS) {
							
							customAlert("Error in submitting the order. <br/>Error type: " + dataS.statusText + "<br/>Check your data and try again.");
							customAlert(dataS);

							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "none";
						},
						success: function (dataS) {
							spinner.style.display = "none";
							console.log(JSON.stringify(dataS));
							dataS = JSON.parse(JSON.stringify(dataS));

							if (dataS.valid == '1') {
								thephotos = localStorage.getItem('photos');
								thephotos = (thephotos === null) ? [] : JSON.parse(thephotos);
								for (var a = thephotos.length - 1; a >= 0; a--) {
									if (thephotos[a].customer_id == that.custSelectVM.selectedCustomer().server_id) {
										thephotos.splice(a, 1);
									}
								}
								thetags = localStorage.getItem('tags');
								thetags = (thetags == null) ? [] : JSON.parse(thetags);
								if (localStorage.getItem('tags') != null) {
									if (localStorage.getItem('tags').length > 0) {
										for (var j = thetags.length - 1; j >= 0; j--) {
											var tagimagefound = false;
											for (var i = 0; i < thephotos.length; i++) {
												if (thetags[j].image_id == thephotos[i].image_id) {
													tagimagefound = true;
													console.log("tag found");
												}
											}
											if (tagimagefound == false) {
												thetags.splice(j, 1);
											}
										}
									}
								}
								localStorage.removeItem('signature');
								localStorage.removeItem('CreditCardPhoto');
								var tmp = [];
								for (var x = 0; x < thephotos.length; x++) {
									tmp.push({ customer_id: thephotos[x].customer_id, image_id: thephotos[x].image_id, picture: thephotos[x].picture, thumbnail: thephotos[x].thumbnail });
								}
								localStorage.removeItem('photos');
								localStorage.setItem('photos', JSON.stringify(tmp));
								var tmptags = [];
								for (var x = 0; x < thetags.length; x++) {
									tmptags.push({ image_id: thetags[x].image_id, left: thetags[x].left, top: thetags[x].top, text: thetags[x].text });
								}
								localStorage.removeItem('tags');
								localStorage.setItem('tags', JSON.stringify(tmptags));
								///////////////////////////////////
								thefitlinephotos = localStorage.getItem('fitlinephotos');
								thefitlinephotos = (thefitlinephotos === null) ? [] : JSON.parse(thefitlinephotos);
								for (var a = thefitlinephotos.length - 1; a >= 0; a--) {
									if (thefitlinephotos[a].customer_id == that.custSelectVM.selectedCustomer().server_id) {
										thefitlinephotos.splice(a, 1);
									}
								}

								thefitlinetags = localStorage.getItem('fitlinetags');
								thefitlinetags = (thefitlinetags == null) ? [] : JSON.parse(thefitlinetags);
								if (localStorage.getItem('fitlinetags') != null) {
									if (localStorage.getItem('fitlinetags').length > 0) {
										for (var j = thefitlinetags.length - 1; j >= 0; j--) {
											var tagimagefound = false;
											for (var i = 0; i < thefitlinephotos.length; i++) {
												if (thefitlinetags[j].image_id == thefitlinephotos[i].image_id) {
													tagimagefound = true;
													console.log("tag found");
												}
											}
											if (tagimagefound == false) {
												thefitlinetags.splice(j, 1);
											}
										}
									}
								}

								var tmp = [];
								for (var x = 0; x < thefitlinephotos.length; x++) {
									tmp.push({ customer_id: thefitlinephotos[x].customer_id, image_id: thefitlinephotos[x].image_id, picture: thefitlinephotos[x].picture, thumbnail: thefitlinephotos[x].thumbnail });
								}
								localStorage.removeItem('fitlinephotos');
								localStorage.setItem('fitlinephotos', JSON.stringify(tmp));
								var tmpfitlinetags = [];
								for (var x = 0; x < thefitlinetags.length; x++) {
									tmpfitlinetags.push({ image_id: thefitlinetags[x].image_id, left: thefitlinetags[x].left, top: thefitlinetags[x].top, text: thefitlinetags[x].text });
								}
								localStorage.removeItem('fitlinetags');
								localStorage.setItem('fitlinetags', JSON.stringify(tmpfitlinetags));
								//that.custSelectVM.selectedCustomer().server_id	                    	
								// updates selected customer with server_id
								for (i in custData.store) {
									try {
										if (custData.store[i].id == that.custSelectVM.selectedCustomer().id) {
											custData.store[i].server_id = parseInt(dataS.customer_id);
											that.custSelectVM.selectedCustomer().server_id = parseInt(dataS.customer_id);
											custData.modelUpdate();
											break;
										}
									} catch (e) {
										console.log("plz Remind fix the customer endpoint");
									}
								}
								// msg + shipping date
								weekday = [];
								weekday[0] = "Sunday";
								weekday[1] = "Monday";
								weekday[2] = "Tuesday";
								weekday[3] = "Wednesday";
								weekday[4] = "Thursday";
								weekday[5] = "Friday";
								weekday[6] = "Saturday";
								t = dataS.shipping.split(/[- :]/);
								new_date = new Date(t[0], t[1] - 1, t[2]);
								shipping_date = new_date.getDate() + '/' + (parseInt(new_date.getMonth()) + 1) + '/' + new_date.getFullYear();
								console.log('success');
								setTimeout(function () {
									that.clearOrder()
								}, 1000);
								posChangePage('#main');
								$.jGrowl('Order has been saved!');
								//orderItem  = new OrderItem(dsRegistry);
							} else {
								console.log('fail');
								$.jGrowl(dataS.msg);
							}
							spinner.style.display = "none";
						}
					});

				}
			}

			this.selectedCustomer = ko.computed(function () {
				if ((typeof (that.custSelectVM.selectedCustomer()) != 'undefined') && (that.custSelectVM.selectedCustomer() != {})) {
					return that.custSelectVM.selectedCustomer().customer_first_name + ' ' + that.custSelectVM.selectedCustomer().customer_last_name;
				} else {
					return "";
				}
			});

			this.selectedOrder = ko.computed(function () {
				if ((typeof (that.custOrdersVM.selectedOrder()) != 'undefined') && (that.custOrdersVM.selectedOrder() != {})) {
					return that.custOrdersVM.selectedOrder().customer_id + ' ' + that.custOrdersVM.selectedOrder().order_id;
				} else {
					return "";
				}
			});

			this.selectedOrderGarment = ko.computed(function () {
				if ((typeof (that.custOrderVM.selectedOrderGarment()) != 'undefined') && (that.custOrderVM.selectedOrderGarment() != {})) {
					return that.custOrderVM.selectedOrderGarment().orders_products_id + ' ' + that.custOrderVM.selectedOrderGarment().garment_name;
				} else {
					return "";
				}
			});




		},	// END OF INIT


		clearStorageAndExit: function (data) {
			//console.log("clearStorageAndExit");
			localStorage.clear();
			//posChangePage('index.htm'); 
			window.location = "index.htm";
		},


		cancelOrder: function (data) {
			console.log('cancelOrder');
			var that = this;
			if (!confirm('Are you sure you want to cancel the current order?')) return;
			orderItem = new OrderItem(dsRegistry);
			posChangePage('#main');
			$.jGrowl('Order cancelled');
			setTimeout(function () {
				that.clearOrder()
			}, 1000);
		},

		clearOrder: function () {
			console.log('clearOrder');
			orderItem = new OrderItem(dsRegistry);
			var that = this;
			this.garmentsList = new ko.observableArray([
				{
					id: 0,
					title: "Jacket",
					image: "gfx/measurements/jacket.png",
					active: false,
					count: ko.observable(0)
				},
				{
					id: 1,
					title: "Pant",
					image: "gfx/measurements/pants.png",
					active: false,
					count: ko.observable(0)
				},
				{
					id: 2,
					title: "Suit",
					image: "gfx/measurements/suit.png",
					active: false,
					count: ko.observable(0)
				},

				{
					id: 3,
					title: "Vest",
					image: "gfx/measurements/vest.png",
					active: false,
					count: ko.observable(0)
				},

				{
					id: 4,
					title: "Shirt",
					image: "gfx/measurements/shirt.png",
					active: false,
					count: ko.observable(0)
				}
			]);
			this.custSelectVM.selectedCustomer({});
			try {
				this.bodyshapeVM.clear();
			} catch (e) { ; }
			this.cancelMode = true;
			this.currentBigStep(that.bigSteps()[0]);
		},

		setSelectedCustomer: function (data) {
			var selection = this.custSelectVM.selectedCustomer();
			customersVM.selected_item(selection);
			console.log("customersVM.systemMode: " + customersVM.systemMode);
			if (customersVM.systemMode == "order") {
				this.customerImage = new MediaType(1, null, true, '', true);
				this.customerVideo = new MediaType(2, null, true, '', true);
				this.MediaUploadVM.setMediaCallback(this.callbackMedia);
				this.customPreviewImage = ko.observableArray([]);
				this.customPreviewVideo = ko.observableArray([]);
				this.customerImage.setCustomPreview(this.customPreviewImage);
				this.customerVideo.setCustomPreview(this.customPreviewVideo);
				this.copyGarmentImage = new MediaType(1, null, true, '', true);
				this.copyGarmentVideo = new MediaType(2, null, true, '', true);
				this.MediaUploadVM.setMediaCallback(this.callbackMedia);
				this.copyGarmentPreviewImage = ko.observableArray([]);
				this.copyGarmentPreviewVideo = ko.observableArray([]);
				this.copyGarmentImage.setCustomPreview(this.copyGarmentPreviewImage);
				this.copyGarmentVideo.setCustomPreview(this.copyGarmentPreviewVideo);
				if (!$.isEmptyObject(data)) posChangePage('#orderItemCustomerInfo');
			} else if (customersVM.systemMode == "customer") {
				if (!$.isEmptyObject(data)) posChangePage('#customerInfo');
			} else if (customersVM.systemMode == "shipping_alterations" || customersVM.systemMode == "shipping_garments") {
				localStorage.removeItem('AlterationsDS');
				if (customersVM.systemMode == "shipping_alterations") {
					AlterationsDS = new SyncableDatasource('AlterationsDS', 'AlterationsDS', dsRegistry, 'customer_alterations_endpoint');
				} else {
					//AlterationsDS = new SyncableDatasource('AlterationsDS','AlterationsDS', dsRegistry, 'alterations_endpoint');
					AlterationsDS = new SyncableDatasource('AlterationsDS', 'AlterationsDS', dsRegistry, 'customer_garments_endpoint');
				}
				orderItem.AlterationsVM.subscribeTo('AlterationsDS');
				posChangePage('#shipping_alterations_list');
			} else if (customersVM.systemMode == "shipping_fabrics") {
				posChangePage('#shipping_fabrics');
			} else if (customersVM.systemMode == "shipping_copy_garment") {
			} else if (customersVM.systemMode == "shipping_faulty_garments") {
				posChangePage('#shipping_faulty_remake_garments');
			} else if (customersVM.systemMode == "shipping_remake_garments") {
				//posChangePage('#shipping_faulty_remake_garments');	 // MOVED TO syncmeasurementsVM
			} else if (customersVM.systemMode == "shipping_other") {
				//posChangePage('#shipping_other');
			} else if (customersVM.systemMode == "fitting") {
				posChangePage('#customerGarmentsFittingsList');
			}

		},

		setSelectedCustomerFromList: function (data) {
			var selection = this.custSelectVM.selectedCustomer();
			customersVM.selected_item(selection);
			if (!$.isEmptyObject(data)) posChangePage('#customerInfo');
		},

		selectedCustomerEdit: function (data) {
			customersVM.selectCustomer(this.custSelectVM.selectedCustomer().id, '#' + $('.ui-page-active').attr('id'));
		},

		setSelectedOrder: function (data) {
			var selection = this.custOrdersVM.selectedOrder();
			customersVM.selected_order(selection);
			if (!$.isEmptyObject(data)) posChangePage('#orderInfo');
		},

		setSelectedOrderGarment: function (data) {
			var selection = this.custOrderVM.selectedOrderGarment();
			customersVM.selected_ordergarment(selection);
			//if (!$.isEmptyObject(data)) posChangePage('#orderInfo');
			// if jacket view jacket, if pants view pants etc
		},

		submitOrder: function () {
			posChangePage('#main');
			$.jGrowl('Order submitted succesfully');
		}
	});




	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




	//     ##   ##    ##   ##   ########
	//     ##   ###   ##   ##      ##
	//     ##   ## #  ##   ##      ##
	//     ##   ##  # ##   ##      ##
	//     ##   ##   ###   ##      ##
	//     ##   ##    ##   ##      ##

	//authCtrl = new AuthControl();

	var vbodyshapeShouldersDS = { "store": [{ "title": "Default", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png", "id": "0" }, { "title": "Both slightly sloped", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png", "id": "1" }, { "title": "Both very sloped", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-very-sloped.png", "id": "2" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-normal.png", "id": "3" }, { "title": "Left normal Right sloped", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-normal-left-sloped.png", "id": "4" }, { "title": "Left slightly sloped Right very sloped", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-slightly-sloped-left-very-sloped.png", "id": "5" }, { "title": "Right normal Left sloped", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-sloped-left-normal.png", "id": "6" }, { "title": "Right slightly sloped Left very sloped", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-right-very-sloped-left-slightly-sloped.png", "id": "7" }, { "title": "Square", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-square.png", "id": "8" }], "timestamp": 0 };
	var vbodyshapeArmsDS = { "store": [{ "title": "Default", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_shoulders/shoulders-both-slightly-sloped.png", "id": "0" }, { "title": "Backwards arms", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/backwards_arms.png", "id": "1" }, { "title": "Balanced", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/balanced.png", "id": "2" }, { "title": "Forwards arms", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_arms/forwards_arms.png", "id": "3" }], "timestamp": 0 };
	var vbodyshapeBackDS = { "store": [{ "title": "Curved back", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-curved-back.png", "id": "1" }, { "title": "Hump back", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-hump-back.png", "id": "2" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-normal.png", "id": "3" }, { "title": "Upper back curve", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-upper-back-curve.png", "id": "4" }, { "title": "Upper back curved forward", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_back/back-silhouette-upper-back-curved-forward.png", "id": "5" }], "timestamp": 0 };
	var vbodyshapeFrontDS = { "store": [{ "title": "Caving in", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-caving-in.png", "id": "1" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-normal.png", "id": "2" }, { "title": "Slightly protruding", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-slightly-protruding.png", "id": "3" }, { "title": "Chicken", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-chicken.png", "id": "5" }, { "title": "Well padded chest", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_front/front-chest-silhouette-well-padded-chest.png", "id": "4" }], "timestamp": 0 };
	var vbodyshapeLegDS = { "store": [{ "title": "Bow legged", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/bow_legged.png", "id": "1" }, { "title": "Inwards", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/inwards.png", "id": "2" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/normal.png", "id": "3" }, { "title": "Outwards", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_leg/outwards.png", "id": "4" }], "timestamp": 0 };
	var vbodyshapeNeckHeightDS = { "store": [{ "title": "Long", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckheight/long_neck.png", "id": "1" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckheight/normal_neck.png", "id": "2" }, { "title": "Very Short", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_neckheight/very_short_neck.png", "id": "3" }], "timestamp": 0 };
	var vbodyshapeStandingDS = { "store": [{ "title": "Backward", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/backward.png", "id": "1" }, { "title": "Neutral", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/neutral.png", "id": "2" }, { "title": "Slight lean", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/slight_lean.png", "id": "3" }, { "title": "Strong lean", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_standing/strong_lean.png", "id": "4" }], "timestamp": 0 };
	var vbodyshapeStomachDS = { "store": [{ "title": "Beer belly", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-beer-belly.png", "id": "1" }, { "title": "Bulge", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-bulge.png", "id": "2" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-normal.png", "id": "3" }, { "title": "Pot belly", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-pot-belly.png", "id": "4" }, { "title": "Slight bulge", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-slight-bulge.png", "id": "5" }, { "title": "Washboard", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_stomach/stomach-silhouette-washboard.png", "id": "6" }], "timestamp": 0 };
	var vbodyshapeThighDS = { "store": [{ "title": "Heavy legs", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-heavy-legs.png", "id": "1" }, { "title": "Large muscular thighs", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-large-muscular-thighs.png", "id": "2" }, { "title": "Thick thighs", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-thick-thighs.png", "id": "3" }, { "title": "Thin legs", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-thin-legs.png", "id": "4" }, { "title": "Very erect prominent calves", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-very-erect-prominent-calves.png", "id": "5" }, { "title": "Normal", "bodyshapeImage": "http://shirt-tailor.net/thepos/appimg/bodyshape/bodyshape_thigh/thigh-stance-normal.png", "id": "6" }], "timestamp": 0 };
	var vvestLapelStyleDS = { "store": [{ "title": "Style V", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/none.png", "id": "0" }, { "title": "Style V", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/none.png", "id": "4" }, { "title": "Notch lapel", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/notch.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/notch_preview.png", "id": "1" }, { "title": "Peak", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/peak.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/peak_preview.png", "id": "2" }, { "title": "Shawl lapel V", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png", "id": "3" }, { "title": "Custom", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png", "id": "123" }], "timestamp": 0 };
	// IMAGES MUST CHANGE
	var vvestDesignDS = { "store": [{ "title": "WC 1", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/v.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/none.png", "id": "1" }, { "title": "WC 2", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/notch.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/notch_preview.png", "id": "2" }, { "title": "WC 3", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/peak.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/peak_preview.png", "id": "3" }, { "title": "WC 4", "image": "http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/lapel/shawl_lapel_preview.png", "id": "4" }], "timestamp": 0 };
	// NEEDS THREE MORE OPTIONS FOR DOUBLE BREASTED
	var vvestButtonNumberDS = { "store": [{ "title": "Select Buttons", "image": "http://shirt-tailor.net/thepos/appimg/vest/buttons/four.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/buttons/four_preview.png", "id": "0" }, { "title": "Four Buttons", "image": "http://shirt-tailor.net/thepos/appimg/vest/buttons/four.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/buttons/four_preview.png", "id": "1" }, { "title": "Five Buttons", "image": "http://shirt-tailor.net/thepos/appimg/vest/buttons/five.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/buttons/five_preview.png", "id": "2" }, { "title": "Six Buttons", "image": "http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png", "id": "3" }, { "title": "Custom Buttons", "image": "http://shirt-tailor.net/thepos/appimg/vest/buttons/six.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/buttons/six_preview.png", "id": "123" }], "timestamp": 0 };
	var vvestPocketStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/vest/pockets/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/none.png", "id": "0" }, { "title": "No ", "image": "http://shirt-tailor.net/thepos/appimg/vest/pockets/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/none.png", "id": "1" }, { "title": "Single", "image": "http://shirt-tailor.net/thepos/appimg/vest/pockets/single.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/pockets/single_preview.png", "id": "2" }, { "title": "Patch", "image": "http://shirt-tailor.net/thepos/appimg/vest/pockets/patch.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/pockets/patch_preview.png", "id": "3" }, { "title": "Flap", "image": "http://shirt-tailor.net/thepos/appimg/vest/pockets/flap.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/pockets/flap_preview.png", "id": "4" }], "timestamp": 0 };
	var vvestBottomStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/vest/bottom/notch.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/bottom/notch_preview.png", "id": "0" }, { "title": "Angle cut", "image": "http://shirt-tailor.net/thepos/appimg/vest/bottom/notch.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/bottom/notch_preview.png", "id": "1" }, { "title": "Straight cut", "image": "http://shirt-tailor.net/thepos/appimg/vest/bottom/round.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/bottom/round_preview.png", "id": "2" }], "timestamp": 0 };
	var vvestBackStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/vest/back/belt.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/back/belt_preview.png", "id": "0" }, { "title": "Belt", "image": "http://shirt-tailor.net/thepos/appimg/vest/back/belt.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/back/belt_preview.png", "id": "1" }, { "title": "Plain", "image": "http://shirt-tailor.net/thepos/appimg/vest/back/single.png", "preview": "http://shirt-tailor.net/thepos/appimg/vest/back/single_preview.png", "id": "2" }], "timestamp": 0 };
	var vvestButtonsDS = { "store": [{ "title": "DEFAULT", "image": "http://shirt-tailor.net/thepos/appimg/buttons/default.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_5.png", "id": "0" }, { "title": "White", "image": "http://shirt-tailor.net/thepos/appimg/buttons/white.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/white_5.png", "id": "1" }, { "title": "Sky Blue", "image": "http://shirt-tailor.net/thepos/appimg/buttons/sky-blue.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/sky-blue_5.png", "id": "2" }, { "title": "Light Khaki", "image": "http://shirt-tailor.net/thepos/appimg/buttons/light-khaki.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/light-khaki_5.png", "id": "3" }, { "title": "Brown", "image": "http://shirt-tailor.net/thepos/appimg/buttons/brown.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/brown_5.png", "id": "4" }, { "title": "Marron", "image": "http://shirt-tailor.net/thepos/appimg/buttons/marron.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/marron_5.png", "id": "5" }, { "title": "Purple", "image": "http://shirt-tailor.net/thepos/appimg/buttons/purple.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/purple_5.png", "id": "6" }, { "title": "Navy", "image": "http://shirt-tailor.net/thepos/appimg/buttons/navy.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/navy_5.png", "id": "7" }, { "title": "Black", "image": "http://shirt-tailor.net/thepos/appimg/buttons/black.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/black_5.png", "id": "8" }, { "title": "Pearl White", "image": "http://shirt-tailor.net/thepos/appimg/buttons/pearl-white.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/pearl-white_5.png", "id": "9" }, { "title": "Deep Brown", "image": "http://shirt-tailor.net/thepos/appimg/buttons/deep-brown.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/deep-brown_5.png", "id": "10" }, { "title": "Dull Silver", "image": "http://shirt-tailor.net/thepos/appimg/buttons/dull-silver.png", "preview1": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/dull-silver_5.png", "id": "11" }], "timestamp": 0 };
	var vvestThreadsDS = { "store": [{ "title": "DEFAULT", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/default.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/black.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png", "id": "0" }, { "title": "White", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/white.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/white.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-white-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-white-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-white-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-white-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-white-5.png", "id": "white" }, { "title": "Cream", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/cream.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/cream.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-cream-5.png", "id": "cream" }, { "title": "Yellow", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/yellow.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/yellow.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-yellow-5.png", "id": "yellow" }, { "title": "Light Green", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/green.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/green.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-green-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-green-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-green-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-green-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-green-5.png", "id": "green" }, { "title": "Light Blue", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/light_blue.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/lightblue.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightblue-5.png", "id": "light-blue" }, { "title": "Navy", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/blue.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/blue.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-blue-5.png", "id": "blue" }, { "title": "Purple", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/purple.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/purple.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-purple-5.png", "id": "purple" }, { "title": "Pink", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/pink.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/pink.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-pink-5.png", "id": "pink" }, { "title": "Light Pink", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/light_pink.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/lightpink.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-lightpink-5.png", "id": "light-pink" }, { "title": "Red", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/red.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/red.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-red-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-red-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-red-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-red-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-red-5.png", "id": "red" }, { "title": "Orange", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/orange.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/orange.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-orange-5.png", "id": "orange" }, { "title": "Bronze", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/gold.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/gold.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-gold-5.png", "id": "bronze" }, { "title": "Charcoal", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/grey.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/charcoal.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-charcoal-5.png", "id": "charcoal" }, { "title": "Black", "image": "http://shirt-tailor.net/thepos/appimg/color_thread/black.png", "lapel1": "http://shirt-tailor.net/thepos/appimg/buttonhole/black.png", "preview1": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/butstitch/w-black-5.png", "id": "black" }], "timestamp": 0 };
	var vbuttonholethreadDS = { "store": [{ "title": "DEFAULT", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/default.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png", "id": "0" }, { "title": "White", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/white.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-white-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-white-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-white-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-white-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-white-5.png", "id": "white" }, { "title": "Cream", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/cream.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-cream-5.png", "id": "cream" }, { "title": "Yellow", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/yellow.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-yellow-5.png", "id": "yellow" }, { "title": "Light Green", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/green.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-green-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-green-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-green-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-green-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-green-5.png", "id": "green" }, { "title": "Light Blue", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/light_blue.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightblue-5.png", "id": "light-blue" }, { "title": "Navy", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/blue.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-blue-5.png", "id": "blue" }, { "title": "Purple", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/purple.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-purple-5.png", "id": "purple" }, { "title": "Pink", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/pink.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-pink-5.png", "id": "pink" }, { "title": "Light Pink", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/light_pink.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-lightpink-5.png", "id": "light-pink" }, { "title": "Red", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/red.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-red-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-red-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-red-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-red-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-red-5.png", "id": "red" }, { "title": "Orange", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/orange.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-orange-5.png", "id": "orange" }, { "title": "Bronze", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/gold.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-gold-5.png", "id": "bronze" }, { "title": "Charcoal", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/grey.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-charcoal-5.png", "id": "charcoal" }, { "title": "Black", "image": "http://shirt-tailor.net/thepos/appimg/button_thread/black.png", "preview1": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-1.png", "preview2": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-2.png", "preview3": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-3.png", "preview4": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-4.png", "preview5": "http://shirt-tailor.net/thepos/appimg/button_thread/w-black-5.png", "id": "black" }], "timestamp": 0 };
	var vsuitJacketStructureDS = { "store": [{ "title": "Select Button", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/one_button.png", "id": "0" }, { "title": "One Button", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/one_button.png", "id": "One_Button" }, { "title": "Two Button", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/two_but.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/two_but_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/two_buttons.png", "id": "Two_Button" }, { "title": "Three Button", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/three_but.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/three_but_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/three_buttons.png", "id": "Three_Button" }, { "title": "Four Button", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/four_but.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/four_but_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/four_buttons.png", "id": "Four_Button" }, { "title": "Double Breasted", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/four_double.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/four_double_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/double.png", "id": "Double_Breasted" }, { "title": "Double Breasted (one to close)", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/four_open_double.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/four_open_double_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/double_one_to_close.png", "id": "Double_Breasted_One_to_close" }, { "title": "Double breasted (six buttons)", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/six_double.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/six_double_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/double_two_to_close.png", "id": "Double_Breasted_six_Buttons" }, { "title": "Custom", "image": "http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/structure/one_but_preview.png", "buttoni": "http://shirt-tailor.net/thepos/appimg/jacket/trimming/one_button.png", "id": "123" }], "timestamp": 0 };
	var vsuitLapelStyleDS = { "store": [{ "title": "Select Lapel", "image": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel_preview.png", "previewone": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/notch_lappel_preview.png", "previewtwo": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/notch_lappel_preview.png", "previewthree": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/notch_lappel_preview.png", "previewfour": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/notch_lappel_preview.png", "dpreview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/notch_lappel_preview.png", "id": "0" }, { "title": "Notch Lapel", "image": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel_preview.png", "previewone": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/notch_lappel_preview.png", "previewtwo": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/notch_lappel_preview.png", "previewthree": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/notch_lappel_preview.png", "previewfour": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/notch_lappel_preview.png", "dpreview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/notch_lappel_preview.png", "id": "1" }, { "title": "Peak Lapel", "image": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/peak_lappel.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/peak_lappel_preview.png", "previewone": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/peak_lappel_preview.png", "previewtwo": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/peak_lappel_preview.png", "previewthree": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/peak_lappel_preview.png", "previewfour": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/peak_lappel_preview.png", "dpreview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/peak_lappel_preview.png", "id": "2" }, { "title": "Shawl Lapel", "image": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/shawl_lappel.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/shawl_lappel_preview.png", "previewone": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/shawl_lappel_preview.png", "previewtwo": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/shawl_lappel_preview.png", "previewthree": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/shawl_lappel_preview.png", "previewfour": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/shawl_lappel_preview.png", "dpreview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/shawl_lappel_preview.png", "id": "3" }, { "title": "Custom Lapel", "image": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/notch_lappel_preview.png", "previewone": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/1/notch_lappel_preview.png", "previewtwo": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/2/notch_lappel_preview.png", "previewthree": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/3/notch_lappel_preview.png", "previewfour": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/4/notch_lappel_preview.png", "dpreview": "http://shirt-tailor.net/thepos/appimg/jacket/lapel/d/notch_lappel_preview.png", "id": "123" }], "timestamp": 0 };
	var vsuitBottomStyleDS = { "store": [{ "title": "Select Bottom", "image": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "id": "0" }, { "title": "Curved Bottom", "image": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "id": "Curved" }, { "title": "Straight Bottom", "image": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/straight_bottom.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/straight_bottom_preview.png", "id": "Straight" }, { "title": "Custom Bottom", "image": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "id": "123" }], "timestamp": 0 };
	var vsuitVentStyleDS = { "store": [{ "title": "Select Vent Style", "image": "http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent_preview.png", "id": "0" }, { "title": "No Vent", "image": "http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent_preview.png", "id": "1" }, { "title": "One Vent", "image": "http://shirt-tailor.net/thepos/appimg/jacket/vent/one_vent.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/vent/one_vent_preview.png", "id": "2" }, { "title": "Two Vent", "image": "http://shirt-tailor.net/thepos/appimg/jacket/vent/two_vent.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/vent/two_vent_preview.png", "id": "3" }, { "title": "Custom Vent", "image": "http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/vent/no_vent_preview.png", "id": "123" }], "timestamp": 0 };
	var vsuitPocketStyleDS = { "store": [{ "title": "NO", "titles": "Select", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_topstitch.png", "id": "0" }, { "title": "JP5", "titles": "Classic", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp5.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp5_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp5_ticket_topstitch.png", "id": "jp5" }, { "title": "JP1", "titles": "Slightly Slanted", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp1_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp1_ticket_topstitch.png", "id": "jp1" }, { "title": "JP7", "titles": "Welt Pocket", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp7.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp7_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp7_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "id": "jp7" }, { "title": "JP2", "titles": "Batman", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp2.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp2_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp2_ticket_topstitch.png", "id": "jp2" }, { "title": "JP3", "titles": "Front Curved", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp3.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp3_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp3_ticket_topstitch.png", "id": "jp3" }, { "title": "JP6", "titles": "Back Curved", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp6.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp6_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_trimmed.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp6_ticket_topstitch.png", "id": "jp6" }, { "title": "JP9", "titles": "Patch Pocket", "image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_preview.png", "ticket_image": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp9.png", "ticket_preview": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/ticket-jp9_preview.png", "trimmedpocket": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "trimmedticket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_ticket_trimmed.png", "topstitchpocket": "http://shirt-tailor.net/thepos/appimg/jacket/pocket/jp9_topstitch.png", "topstitchticketpocket": "http://shirt-tailor.net/thepos/appimg/jacket/bottom/curved_bottom_preview.png", "id": "jp9" }], "timestamp": 0 };
	var vsuitButtonStyleDS = { "store": [{ "title": "Three buttons kissing", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_kiss.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_kiss-preview.png", "id": "1" }, { "title": "Three buttons standing", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_stand.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_stand-preview.png", "id": "2" }, { "title": "Three buttons working", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_work.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/3_b_work-preview.png", "id": "3" }, { "title": "Four buttons kissing", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_kiss.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_kiss-preview.png", "id": "4" }, { "title": "Four buttons standing", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_sta.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_sta-preview.png", "id": "5" }, { "title": "Four buttons working", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_work.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/4_b_work-preview.png", "id": "6" }, { "title": "Five buttons kissing", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_kiss.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_kiss-preview.png", "id": "7" }, { "title": "Five buttons standing", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_sta.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_sta-preview.png", "id": "8" }, { "title": "Five buttons working", "image": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_work.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/5_b_work-preview.png", "id": "9" }], "timestamp": 0 };
	var vJacketSleeveButtonNumberDS = { "store": [{ "title": "One button", "id": "1", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/one_button.png", "previews": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/one_button_covered.png" }, { "title": "Two buttons", "id": "2", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/two_buttons.png", "previews": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/two_buttons_covered.png" }, { "title": "Three buttons", "id": "3", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/three_buttons.png", "previews": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/three_buttons_covered.png" }, { "title": "Four buttons", "id": "4", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/four_buttons.png", "previews": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/four_buttons_covered.png" }, { "title": "Five buttons", "id": "5", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/five_buttons.png", "previews": "http://shirt-tailor.net/thepos/appimg/jacket/buttons/five_buttons_covered.png" }], "timestamp": 0 };
	var vJacketBreastPocketStyleDS = { "store": [{ "title": "No Breastpocket", "image": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": 0 }, { "title": "Standard Straight", "image": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default_pr.png", "id": 1 }, { "title": "Standard Angled", "image": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default_angled.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/default_angled_pr.png", "id": 2 }, { "title": "Patch Pocket", "image": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/patch_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/jacket/breastpocket/patch_pocket_preview.png", "id": 3 }], "timestamp": 0 };
	var vpantBackStyleDS = { "store": [{ "title": "Undefined", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/3_b_kiss.png", "id": "0" }, { "title": "curv_flap_wi_bu_pantt.png ", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/curv_flap_wi_bu_pantt.png", "id": "1" }, { "title": "lsolsing_back.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/lsolsing_back.png", "id": "2" }, { "title": "nononopocke.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/nononopocke.png", "id": "3" }, { "title": "douddl_open_pant_ba.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/douddl_open_pant_ba.png", "id": "4" }, { "title": "mod_fl_wif_bu_pantt.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/mod_fl_wif_bu_pantt.png", "id": "5" }, { "title": "squ_ba_wi_bu_po_pan.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/squ_ba_wi_bu_po_pan.png", "id": "6" }], "timestamp": 0 };
	var vbeltLoopStyleDS = { "store": [{ "title": "Select Belt Loop", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/nonobeelttloop.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "0" }, { "title": "Single Straight", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/sinnggle_pant_llop.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/sinnggle_pant_llop_preview.png", "id": "Single_Straight" }, { "title": "Double Straight", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/dobblooe_pant_loup.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/dobblooe_pant_loup_preview.png", "id": "Double_Straight" }, { "title": "Double Angled", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/double_angle.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/double_angle_preview.png", "id": "Double_Angle" }, { "title": "Criss Cross", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/cross.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/cross_preview.png", "id": "Criss_Cross" }, { "title": "Designer", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/mode_lo_pannm.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/mode_lo_pannm_preview.png", "id": "Designer" }, { "title": "Button Side Adjusters", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/but_adj_belt_oop.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/but_adj_belt_oop_preview.png", "id": "Button_Side_Adjuster" }, { "title": "Buckle Side Adjusters", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/waist_lop.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/waist_lop_preview.png", "id": "Buckle_Side" }, { "title": "No Belt Loop", "image": "http://shirt-tailor.net/thepos/appimg/pant/belt_loop/nonobeelttloop.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "No_Beltloop" }], "timestamp": 0 };
	var vcuffsStyleDS = { "store": [{ "title": "Undefined", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/3_b_kiss.png", "id": "0" }, { "title": "cuffff_pant_po.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/cuffff_pant_po.png", "id": "1" }, { "title": "fold_ov_pant.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/fold_ov_pant.png", "id": "2" }, { "title": "singgl_pant_cuf_tab.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/singgl_pant_cuf_tab.png", "id": "3" }, { "title": "doub_pant_cuf_tab.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/doub_pant_cuf_tab.png", "id": "4" }, { "title": "reggulla_nma.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/reggulla_nma.png", "id": "5" }, { "title": "squ_ba_wi_bu_po_pan.png", "image": "http://shirt-tailor.net/thepos/appimg/pant/cuffs/squ_ba_wi_bu_po_pan.png", "id": "6" }], "timestamp": 0 };
	var vpantFitDS = { "store": [{ "title": "Select Pant Style", "image": "http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan_preview.png", "id": "0" }, { "title": "Normal Straight", "image": "http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/fit/nom_str_pan_preview.png", "id": "Normal_Straight" }, { "title": "Boot Flare", "image": "http://shirt-tailor.net/thepos/appimg/pant/fit/boot_fl_pan.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/fit/boot_fl_pan_preview.png", "id": "Boot_Flare" }, { "title": "Narrow Slim", "image": "http://shirt-tailor.net/thepos/appimg/pant/fit/narr_po_pant.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/fit/narr_po_pant_preview.png", "id": "Narrow_Slim" }], "timestamp": 0 };
	var vpantPleatsDS = { "store": [{ "title": "Select Pleats", "image": "http://shirt-tailor.net/thepos/appimg/pant/pleats/no_plll_pant.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "0" }, { "title": "No Pleats", "image": "http://shirt-tailor.net/thepos/appimg/pant/pleats/no_plll_pant.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "4" }, { "title": "Single Pleat", "image": "http://shirt-tailor.net/thepos/appimg/pant/pleats/singgle_pl_pant.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pleats/singgle_pl_pant_preview.png", "id": "1" }, { "title": "Double Pleats", "image": "http://shirt-tailor.net/thepos/appimg/pant/pleats/dobb_ple_pan.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pleats/dobb_ple_pan_preview.png", "id": "2" }, { "title": "Triple Pleats", "image": "http://shirt-tailor.net/thepos/appimg/pant/pleats/trip_ple_pan.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pleats/trip_ple_pan_preview.png", "id": "3" }], "timestamp": 0 };
	var vpantPocketsDS = { "store": [{ "title": "Select Pocket", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/no_pock_pann_t.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "0" }, { "title": "No Pocket", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/no_pock_pann_t.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "No_Pocket" }, { "title": "Slanted", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/slanted_pan_pocck.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pockets/slanted_pan_pocck_preview.png", "id": "TP1" }, { "title": "Slanted Welt", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/stan_welt_pant.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pockets/stan_welt_pant_preview.png", "id": "TP2" }, { "title": "Straight Welt", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/welt_stri_pan.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pockets/welt_stri_pan_preview.png", "id": "TP3" }, { "title": "Modern Curved", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/mod_cir_pant_p.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pockets/mod_cir_pant_p_preview.png", "id": "TP4" }, { "title": "Jeans", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc_preview.png", "id": "Jeans" }, { "title": "Custom Pocket", "image": "http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/pockets/jerans_poc_preview.png", "id": "123" }], "timestamp": 0 };
	var vbpantPocketsDS = { "store": [{ "title": "Select Pocket Opening", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "0" }, { "title": "No Pocket Opening", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/none.png", "id": "no_pocket" }, { "title": "Double w. Button", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/double_button_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/double_button_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/double_button_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/double_button_preview-left.png", "id": "double_buttons" }, { "title": "Double", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/double_pocket_preview-left.png", "id": "double" }, { "title": "Single w. Button", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/single_buttons.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/single_button_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/single_button_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/single_button_preview-left.png", "id": "single_buttons" }, { "title": "Single", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/single_pocket_preview-left.png", "id": "single" }, { "title": "Modern Flap w. Button", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_button_preview-left.png", "id": "modern_flap" }, { "title": "Square Flap w. Button", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/square_flap.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/square_flap_preview-left.png", "id": "flap" }, { "title": "Modern Flap no Button", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_no_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/modern_preview-left.png", "id": "modern_flap_no_button" }, { "title": "Square Flap no Button", "image": "http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap.png", "preview": "http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview.png", "previewright": "http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview-right.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/pant/back/square_no_flap_preview-left.png", "id": "flap_no_button" }], "timestamp": 0 };
	var vshirtButtonsDS = { "store": [{ "title": "DEFAULT", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/default.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/white-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_white-preview.png", "id": "0" }, { "title": "White", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/white.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/white-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_white-preview.png", "id": "1" }, { "title": "Cream", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/cream.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/cream-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_cream-preview.png", "id": "2" }, { "title": "Yellow", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/yellow.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/yellow-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_yellow-preview.png", "id": "3" }, { "title": "Light Green", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/green.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/green-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_green-preview.png", "id": "4" }, { "title": "Light Pink", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/light_pink.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/light_pink-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_light_pink-preview.png", "id": "5" }, { "title": "Pink", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/pink.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/pink-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_pink-preview.png", "id": "6" }, { "title": "Powder Blue", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/powder-blue.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/power-blue-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_power-blue-preview.png", "id": "7" }, { "title": "Aqua", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/aqua.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/aqua-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_aqua-preview.png", "id": "8" }, { "title": "Navy", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/navy.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/navy-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_navy-preview.png", "id": "9" }, { "title": "Orange", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/orange.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/orange-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_orange-preview.png", "id": "10" }, { "title": "Red", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/red.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/red-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_red-preview.png", "id": "11" }, { "title": "Purple", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/purple.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/purple-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_purple-preview.png", "id": "12" }, { "title": "Charcoal", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/charcoal.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/charcoal-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_charcoal-preview.png", "id": "13" }, { "title": "Black", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/black.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/black-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_black-preview.png", "id": "14" }, { "title": "Screw Back Stud", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/stud.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/studs-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_studs-preview.png", "id": "15" }, { "title": "Cufflink Stud", "image": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/stud.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/studs-preview.png", "spreview": "http://shirt-tailor.net/thepos/appimg/shirt/buttons/s_studs-preview.png", "id": "16" }], "timestamp": 0 };
	var vshirtBackStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat-preview.png", "id": 0 }, { "title": "Box Pleat", "image": "http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/box_pleat-preview.png", "id": 1 }, { "title": "Plain", "image": "http://shirt-tailor.net/thepos/appimg/shirt/back/plain.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png", "id": 3 }, { "title": "Side Pleats", "image": "http://shirt-tailor.net/thepos/appimg/shirt/back/side_pleats.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/side_pleats-preview.png", "id": 6 }, { "title": "Center Pleats", "image": "http://shirt-tailor.net/thepos/appimg/shirt/back/center_pleats.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/center_pleats-preview.png", "id": 2 }], "timestamp": 0 };
	var vshirtBottomStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__-preview.png", "id": "0" }, { "title": "Round TP", "image": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/tri_tab.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/tri_tab-preview.png", "id": "3" }, { "title": "Round", "image": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/classi.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/classi-preview.png", "id": "4" }, { "title": "Straight", "image": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight__-preview.png", "id": "1" }, { "title": "Cut Straight", "image": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/bottom/straight-preview.png", "id": "2" }], "timestamp": 0 };
	var vshirtCollarStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/cut_away_1_button.png", "id": 0 }, { "title": "French", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_1_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_1_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/french_one_button.png", "id": 3 }, { "title": "Italian", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_1_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_1_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/italian_one_button.png", "id": 14 }, { "title": "St-Germain", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_2_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/french_collar_2_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/french_two_button.png", "id": 4 }, { "title": "Button Down", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/button_down.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/button_down-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/button_down.png", "id": 6 }, { "title": "Wing Tip", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/tuxedo.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/tuxedo-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/tuxedo.png", "id": 11 }, { "title": "Cut Away", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_1_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/cut_away_1_button.png", "id": 12 }, { "title": "Cut Away Two But.", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_2_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/cut_away_2_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/cut_away_2_button.png", "id": 13 }, { "title": "Italian Two But.", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_2_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/italian_collar_2_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/italian_two_button.png", "id": 15 }, { "title": "Hidden Button", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/hidden_button.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/hidden_button-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/hidden-button.png", "id": 7 }, { "title": "Tab", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/tab.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/tab-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/tab.png", "id": 8 }, { "title": "Batman", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/batman.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/batman-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/batman.png", "id": 2 }, { "title": "Round", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/round_collar.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/round_collar-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/round_collar.png", "id": 5 }, { "title": "Modern", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/coll_modern.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/coll_modern-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/modern.png", "id": 9 }, { "title": "Band", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/band.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png", "id": 1 }, { "title": "Custom", "image": "http://shirt-tailor.net/thepos/appimg/shirt/collar/round_collar.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/collar/round_collar-preview.png", "previewcontr": "http://shirt-tailor.net/thepos/appimg/shirt/collar/contrast/round_collar.png", "id": 123 }], "timestamp": 0 };
	var vshirtCuffsStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/box_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/box_pleat-preview.png", "id": 0 }, { "title": "One Button Angled", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_angled.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_angled-preview.png", "id": 1 }, { "title": "One Button Round", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_round.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_round-preview.png", "id": 2 }, { "title": "One Button Square", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_square.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/1_button_square-preview.png", "id": 3 }, { "title": "Two Button Angled", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_angled.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_angled-preview.png", "id": 4 }, { "title": "Two Button Round", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_round.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_round-preview.png", "id": 5 }, { "title": "Two Button Square", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_square.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/2_button_square-preview.png", "id": 6 }, { "title": "French Angle", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_cut.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_cut-preview.png", "id": 7 }, { "title": "French Round", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_round.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_round-preview.png", "id": 8 }, { "title": "French Square", "image": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_square.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/cuffs/french_square-preview.png", "id": 9 }], "timestamp": 0 };
	var vshirtFitStyleDS = { "store": [{ "title": "Undefined", "image": "http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt-preview.png", "id": 0 }, { "title": "fitted_shirt.png", "image": "http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/fit/fitted_shirt-preview.png", "id": 1 }, { "title": "semi_shirt.png", "image": "http://shirt-tailor.net/thepos/appimg/shirt/fit/semi_shirt.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/fit/semi_shirt-preview.png", "id": 2 }, { "title": "standard_shirt.png", "image": "http://shirt-tailor.net/thepos/appimg/shirt/fit/standard_shirt.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/fit/standard_shirt-preview.png", "id": 3 }], "timestamp": 0 };
	var vshirtFrontStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/box.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/box-preview.png", "id": 0 }, { "title": "Box", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/box.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/box-preview.png", "id": 1 }, { "title": "Fly", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/hidden.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/hidden-preview.png", "id": 2 }, { "title": "French", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/single.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/back/plain-preview.png", "id": 3 }, { "title": "Pleats", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/pleat.png", "id": 4 }, { "title": "Full Pleats", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_full_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/full_pleat.png", "id": 5 }, { "title": "Angle Pleats", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_angled_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/angled_pleat.png", "id": 6 }, { "title": "Custom", "image": "http://shirt-tailor.net/thepos/appimg/shirt/front/thumb_angled_pleat.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/front/angled_pleat.png", "id": 123 }], "timestamp": 0 };
	var vshirtPocketStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png", "id": 0 }, { "title": "No Pocket", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/no_pocket-preview.png", "id": 1 }, { "title": "Classic", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic-preview-left.png", "id": 6 }, { "title": "Classic Square", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_square-preview-left.png", "id": 2 }, { "title": "Classic Angle", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/classic_angle-preview-left.png", "id": 3 }, { "title": "Diamond Straight", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/diamond_straight-preview-left.png", "id": 4 }, { "title": "Round Flap", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_flap-preview-left.png", "id": 5 }, { "title": "Flap Diamond", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/flap_diamond-preview-left.png", "id": 7 }, { "title": "Round with Glass", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/round_with_glass-preview-left.png", "id": 8 }, { "title": "Angle Flap", "image": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap-preview.png", "previewleft": "http://shirt-tailor.net/thepos/appimg/shirt/pocket/angle_flap-preview-left.png", "id": 9 }], "timestamp": 0 };
	var vshirtSleeveStyleDS = { "store": [{ "title": "Select", "image": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal-preview.png", "id": 0 }, { "title": "Normal", "image": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/normal-preview.png", "id": 1 }, { "title": "Rolled", "image": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/rolled.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/rolled-preview.png", "id": 2 }, { "title": "Short", "image": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/short.png", "preview": "http://shirt-tailor.net/thepos/appimg/shirt/sleeves/short-preview.png", "id": 3 }], "timestamp": 0 };
	var vshirtMonogramFontDS = { "store": [{ "title": "Block", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font1.png", "id": 1 }, { "title": "Text", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font2.png", "id": 2 }, { "title": "Fancy", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font3.png", "id": 3 }, { "title": "Script", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font4.png", "id": 4 }, { "title": "Diamond", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font5.png", "id": 5 }, { "title": "Angle Block", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font6.png", "id": 6 }, { "title": "Angle Text", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font7.png", "id": 7 }, { "title": "Angle Fancy", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font8.png", "id": 8 }, { "title": "Angle Script", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font9.png", "id": 9 }, { "title": "Small Letters", "image": "http://shirt-tailor.net/thepos/appimg/shirt/monogram/font10.png", "id": 10 }], "timestamp": 0 };
	var vfitDS = { "store": [{ "title": "Fitted", "id": "Fitted" }, { "title": "Semi Fitted", "id": "Semi_Fitted" }, { "title": "Standard Fit", "id": "Standard_Fit" }], "timestamp": 0 };


	//Getting discounts code from endPoint
	$.ajax({
		type: 'POST',
		url: BUrl + 'client/get_discounts_endpoint',
		dataType: 'text',
		async: true,
		success: function (dataS) {
			discounts_code_from_server = JSON.parse(dataS);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("Discounts endpoints cannot be reached! ");
			console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
		}
	});

	$.ajax({
		type: 'POST',
		url: BUrl + 'client/get_lining_fabric',
		dataType: 'text',
		async: true,
		success: function (dataS) {
			var vliningFabricDS = JSON.parse(JSON.parse(dataS));
			liningFabricDS = new SyncableDatasource('liningFabricDS', 'liningFabricDS', dsRegistry, null, true, vliningFabricDS);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("Discounts endpoints cannot be reached! ");
			console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
		}
	});

	$.ajax({
		type: 'POST',
		url: BUrl + 'client/get_piping_color',
		dataType: 'text',
		async: true,
		success: function (dataS) {
			var vpipingColorDS = JSON.parse(JSON.parse(dataS));
			pipingColorDS = new SyncableDatasource('pipingColorDS', 'pipingColorDS', dsRegistry, null, true, vpipingColorDS);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("Discounts endpoints cannot be reached! ");
			console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
		}
	});







	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	dsRegistry = new DatasourceRegistry();

	fabricsData = new SyncableDatasource('fabricsDS', 'fabricsDS', dsRegistry, 'fabrics_endpoint', true, null, true);


	pocketTypeDS = new SyncableDatasource('pocketTypeDS', 'pocketTypeDS', dsRegistry, null, true);
	ShippingNotificationsDS = new SyncableDatasource('ShippingNotificationsDS', 'ShippingNotificationsDS', dsRegistry, 'shipping_notifications_numbers_endpoint');
	countriesDS = new SyncableDatasource('countriesDS', 'countriesDS', dsRegistry, 'countries_endpoint', true, null, true);
	statesData = new SyncableDatasource('statesDS', 'statesDS', dsRegistry, 'states_endpoint', true, null, true);
	citiesData = new SyncableDatasource('citiesDS', 'citiesDS', dsRegistry, 'cities_endpoint', true, null, true);
	bodyshapeShouldersDS = new SyncableDatasource('bodyshapeShouldersDS', 'bodyshapeShouldersDS', dsRegistry, null, true, vbodyshapeShouldersDS);
	bodyshapeNeckStanceDS = new SyncableDatasource('bodyshapeNeckStanceDS', 'bodyshapeNeckStanceDS', dsRegistry, null, true);
	bodyshapeArmsDS = new SyncableDatasource('bodyshapeArmsDS', 'bodyshapeArmsDS', dsRegistry, null, true, vbodyshapeArmsDS);
	bodyshapeBackDS = new SyncableDatasource('bodyshapeBackDS', 'bodyshapeBackDS', dsRegistry, null, true, vbodyshapeBackDS);
	bodyshapeBeltAngleDS = new SyncableDatasource('bodyshapeBeltAngleDS', 'bodyshapeBeltAngleDS', dsRegistry, null, true);
	bodyshapeBeltHeightDS = new SyncableDatasource('bodyshapeBeltHeightDS', 'bodyshapeBeltHeightDS', dsRegistry, null, true);
	bodyshapeFrontDS = new SyncableDatasource('bodyshapeFrontDS', 'bodyshapeFrontDS', dsRegistry, null, true, vbodyshapeFrontDS);
	bodyshapeLegDS = new SyncableDatasource('bodyshapeLegDS', 'bodyshapeLegDS', dsRegistry, null, true, vbodyshapeLegDS);
	bodyshapeNeckHeightDS = new SyncableDatasource('bodyshapeNeckHeightDS', 'bodyshapeNeckHeightDS', dsRegistry, null, true, vbodyshapeNeckHeightDS);
	bodyshapeSeatDS = new SyncableDatasource('bodyshapeSeatDS', 'bodyshapeSeatDS', dsRegistry, null, true);
	bodyshapeSleeveDS = new SyncableDatasource('bodyshapeSleeveDS', 'bodyshapeSleeveDS', dsRegistry, null, true);
	bodyshapeStandingDS = new SyncableDatasource('bodyshapeStandingDS', 'bodyshapeStandingDS', dsRegistry, null, true, vbodyshapeStandingDS);
	bodyshapeStomachDS = new SyncableDatasource('bodyshapeStomachDS', 'bodyshapeStomachDS', dsRegistry, null, true, vbodyshapeStomachDS);
	bodyshapeThighDS = new SyncableDatasource('bodyshapeThighDS', 'bodyshapeThighDS', dsRegistry, null, true, vbodyshapeThighDS);
	vestLapelStyleDS = new SyncableDatasource('vestLapelStyleDS', 'vestLapelStyleDS', dsRegistry, null, true, vvestLapelStyleDS);
	vestDesignDS = new SyncableDatasource('vestDesignDS', 'vestDesignDS', dsRegistry, null, true, vvestDesignDS);
	vestButtonNumberDS = new SyncableDatasource('vestButtonNumberDS', 'vestButtonNumberDS', dsRegistry, null, true, vvestButtonNumberDS);
	vestPocketStyleDS = new SyncableDatasource('vestPocketStyleDS', 'vestPocketStyleDS', dsRegistry, null, true, vvestPocketStyleDS);
	vestBottomStyleDS = new SyncableDatasource('vestBottomStyleDS', 'vestBottomStyleDS', dsRegistry, null, true, vvestBottomStyleDS);
	vestBackStyleDS = new SyncableDatasource('vestBackStyleDS', 'vestBackStyleDS', dsRegistry, null, true, vvestBackStyleDS);
	vestButtonsDS = new SyncableDatasource('vestButtonsDS', 'vestButtonsDS', dsRegistry, null, true, vvestButtonsDS);
	vestThreadsDS = new SyncableDatasource('vestThreadsDS', 'vestThreadsDS', dsRegistry, null, true, vvestThreadsDS);
	buttonholethreadDS = new SyncableDatasource('buttonholethreadDS', 'buttonholethreadDS', dsRegistry, null, true, vbuttonholethreadDS);
	suitLapelStyleDS = new SyncableDatasource('suitLapelStyleDS', 'suitLapelStyleDS', dsRegistry, null, true, vsuitLapelStyleDS);
	suitBottomStyleDS = new SyncableDatasource('suitBottomStyleDS', 'suitBottomStyleDS', dsRegistry, null, true, vsuitBottomStyleDS);
	suitVentStyleDS = new SyncableDatasource('suitVentStyleDS', 'suitVentStyleDS', dsRegistry, null, true, vsuitVentStyleDS);
	suitPocketStyleDS = new SyncableDatasource('suitPocketStyleDS', 'suitPocketStyleDS', dsRegistry, null, true, vsuitPocketStyleDS);
	tickPocketStyleDS = new SyncableDatasource('tickPocketStyleDS', 'tickPocketStyleDS', dsRegistry, null, true);
	suitButtonStyleDS = new SyncableDatasource('suitButtonStyleDS', 'suitButtonStyleDS', dsRegistry, null, true, vsuitButtonStyleDS);
	JacketSleeveButtonNumberDS = new SyncableDatasource('JacketSleeveButtonNumberDS', 'JacketSleeveButtonNumberDS', dsRegistry, null, true, vJacketSleeveButtonNumberDS);
	JacketBreastPocketStyleDS = new SyncableDatasource('JacketBreastPocketStyleDS', 'JacketBreastPocketStyleDS', dsRegistry, null, true, vJacketBreastPocketStyleDS);
	suitJacketStructureDS = new SyncableDatasource('suitJacketStructureDS', 'suitJacketStructureDS', dsRegistry, null, true, vsuitJacketStructureDS);
	pantBackStyleDS = new SyncableDatasource('pantBackStyleDS', 'pantBackStyleDS', dsRegistry, null, true, vpantBackStyleDS);
	beltLoopStyleDS = new SyncableDatasource('beltLoopStyleDS', 'beltLoopStyleDS', dsRegistry, null, true, vbeltLoopStyleDS);
	beltLoopStyleDS = new SyncableDatasource('beltLoopStyleDS', 'beltLoopStyleDS', dsRegistry, null, true, vbeltLoopStyleDS);
	cuffsStyleDS = new SyncableDatasource('cuffsStyleDS', 'cuffsStyleDS', dsRegistry, null, true, vcuffsStyleDS);
	pantFitDS = new SyncableDatasource('pantFitDS', 'pantFitDS', dsRegistry, null, true, vpantFitDS);
	pantPleatsDS = new SyncableDatasource('pantPleatsDS', 'pantPleatsDS', dsRegistry, null, true, vpantPleatsDS);
	pantPocketsDS = new SyncableDatasource('pantPocketsDS', 'pantPocketsDS', dsRegistry, null, true, vpantPocketsDS);
	bpantPocketsDS = new SyncableDatasource('bpantPocketsDS', 'bpantPocketsDS', dsRegistry, null, true, vbpantPocketsDS);
	shirtButtonsDS = new SyncableDatasource('shirtButtonsDS', 'shirtButtonsDS', dsRegistry, null, true, vshirtButtonsDS);
	shirtBackStyleDS = new SyncableDatasource('shirtBackStyleDS', 'shirtBackStyleDS', dsRegistry, null, true, vshirtBackStyleDS);
	shirtBottomStyleDS = new SyncableDatasource('shirtBottomStyleDS', 'shirtBottomStyleDS', dsRegistry, null, true, vshirtBottomStyleDS);
	shirtCollarStyleDS = new SyncableDatasource('shirtCollarStyleDS', 'shirtCollarStyleDS', dsRegistry, null, true, vshirtCollarStyleDS);
	shirtCuffsStyleDS = new SyncableDatasource('shirtCuffsStyleDS', 'shirtCuffsStyleDS', dsRegistry, null, true, vshirtCuffsStyleDS);
	shirtFitStyleDS = new SyncableDatasource('shirtFitStyleDS', 'shirtFitStyleDS', dsRegistry, null, true, vshirtFitStyleDS);
	shirtFrontStyleDS = new SyncableDatasource('shirtFrontStyleDS', 'shirtFrontStyleDS', dsRegistry, null, true, vshirtFrontStyleDS);
	shirtPocketStyleDS = new SyncableDatasource('shirtPocketStyleDS', 'shirtPocketStyleDS', dsRegistry, null, true, vshirtPocketStyleDS);
	shirtSleeveStyleDS = new SyncableDatasource('shirtSleeveStyleDS', 'shirtSleeveStyleDS', dsRegistry, null, true, vshirtSleeveStyleDS);
	shirtMonogramFontDS = new SyncableDatasource('shirtMonogramFontDS', 'shirtMonogramFontDS', dsRegistry, null, true, vshirtMonogramFontDS);
	fitDS = new SyncableDatasource('fitDS', 'fitDS', dsRegistry, null, true, vfitDS);


	ko.bindingHandlers.updateFormOnChange = {
		update: function (element) {
			$(element).trigger('create');
		}
	};

	ko.bindingHandlers.updateSliderOnChange = {
		init: function (element, valueAccessor) {
			// use setTimeout with 0 to run this after Knockout is done
			setTimeout(function () {
				// $(element) doesn't work as that has been removed from the DOM
				var curSlider = $('#' + element.id);
				// helper function that updates the slider and refreshes the thumb location
				function setSliderValue(newValue) {
					curSlider.val(newValue).slider('refresh');
				}
				// subscribe to the bound observable and update the slider when it changes
				valueAccessor().subscribe(setSliderValue);
				// set up the initial value, which of course is NOT stored in curSlider, but the original element :\
				setSliderValue($(element).val());
				// subscribe to the slider's change event and update the bound observable
				curSlider.bind('change', function () {
					valueAccessor()(curSlider.val());
				});
			}, 0);
		}
	};

	ko.bindingHandlers.checkbox = {
		update: function (element, valueAccessor) {
			setTimeout(function () {
				var value = valueAccessor();
				var valueUnwrapped = ko.utils.unwrapObservable(value);
				$(element).attr("checked", valueUnwrapped).checkboxradio("refresh");
			}, 0);
		}
	};
	console.log('Version 085');


	ko.bindingHandlers.datepicker2 = {
	    init: function(element, valueAccessor, allBindingsAccessor) {
	        var $el = $(element);
	        
	        //initialize datepicker with some optional options
	        var options = allBindingsAccessor().datepickerOptions || {};
	        $el.datepicker(options);

	        //handle the field changing
	        ko.utils.registerEventHandler(element, "change", function() {
	            var observable = valueAccessor();
	            observable($el.datepicker("getDate"));
	        });

	        //handle disposal (if KO removes by the template binding)
	        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
	            $el.datepicker("destroy");
	        });

	    },
	    update: function(element, valueAccessor) {
	        var value = ko.utils.unwrapObservable(valueAccessor()),
	            $el = $(element),
	            current = $el.datepicker("getDate");
	        
	        if (value - current !== 0) {
	            $el.datepicker("setDate", value);   
	        }
	    }
	};




});
