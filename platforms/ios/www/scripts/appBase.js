define(['jquery', 'knockout', 'jclass'], function ($, ko) {

	function cleanArray(actual) {
		var newArray = [];
		for (var i = 0; i < actual.length; i++) {
			if (actual[i]) {
				newArray.push(actual[i]);
			}
		}
		return newArray;
	}

	function customAlert(message) {
		document.getElementById("customalertmessage").innerHTML = message;
		document.getElementById("customalert").style.display = "block";
	}

	changeVisibilityDiv = function changeVisibilityDiv(nameDiv, visibility) {
		try {
			if (visibility) {
				var div = document.getElementById(nameDiv);
				div.style.display = visibility;
			}
			else {
				var div = document.getElementById(nameDiv);
				if (div.style.display == "none") {
					div.style.display = "block";
				}
				else {
					div.style.display = "none";
				}
			}

		} catch (e) {
			console.log('Div not found');
		}
	}

	mobileInputChange = function mobileInputChange(mobile) {
		var phone = mobile.value.replace(/[^0-9]/g, '');
		if (phone.length > 7) {
			phone = phone.slice(0, 7) + '-' + phone.slice(7);
		}
		if (phone.length > 4) {
			phone = phone.slice(0, 4) + '-' + phone.slice(4);
		}
		mobile.value = phone;
	}

	numberInputHandler = function numberInputHandler(mobile) {
		if (mobile.value != '') {
			var phone = mobile.value.replace(/[^0-9\.\,]/g, '');
			mobile.value = phone;
		}
	}

	DataRequest = function DataRequest(async, endpoint, dataToPost, success, failure, parseFailure, timeout) {
		if (!timeout) {
			timeout = 60000;
		}
		dataToPost = JSON.stringify(dataToPost);
		dataToPost = dataToPost.replace("\"isDirty\":true,", "");
		var salesman = {
			id: authCtrl.userInfo.user_id,
			username: authCtrl.username(),
			password: authCtrl.password()

		};
		salesman = JSON.stringify(salesman);
		$.ajax({
			type: 'POST',
			timeout: timeout, // sets timeout to 45 seconds
			url: BUrl + endpoint,
			data: {
				timestamp: Math.round(new Date().getTime() / 1000),
				salesman: salesman,
				data: dataToPost,
			},
			success: function (dataS) {
				console.log("Data Result SUCCESS:", dataS);
				try {
					if (dataS.trim())
						var response = JSON.parse(dataS);
					else
						var response = '';
					console.log(response);
					if (success)
						success(response);
				} catch (e) {
					console.log(e);
					if (parseFailure) {
						parseFailure(dataS, e);
					}
				}
			},
			error: function (error) {
				console.log("Data Result ERROR:", error);
				if (failure) {
					failure(error);
				}
			},
			async: false
		});
	}

	DatasourceRegistry = function DatasourceRegistry() {
		var self = this;
		self.registry = [];
		self.context = '';

		self.register = function (name, datasource) {
			self.registry[name] = datasource;
		};

		self.getDatasource = function (name) {
			return self.registry[name];
		};

		self.setContext = function (context) {
			this.context = context;
		};

		self.getContext = function () {
			return this.context;
		};

		self.navigate = function (page, context) {
			self.setContext(context);
			posChangePage(page);
		};

	}

	SimpleControl = Class.extend({
		init: function (DsRegistry) {
			this.dsRegistry = DsRegistry;
			this.subscribed = "";
			var that = this;

			$(document).live('modelUpdate', function (event, data) {
				if (data == that.subscribed) {
					var ds = that.dsRegistry.getDatasource(data).getStore();
					if (typeof (ds) != 'undefined') {
						that.digestData(ds);
					}
				}
			});
			this.self = this;
			this.parentViewModel = null;
		},
		getSelf: function () {
			return this.self;
		},
		setParentVM: function (parentRef) {
			this.parentViewModel = parentRef;
		},
		subscribeTo: function (name) {
			this.subscribed = name;
			if (typeof (this.dsRegistry.getDatasource(name)) != 'undefined') {
				var ds = this.dsRegistry.getDatasource(name).getStore();
				if (typeof (ds) != 'undefined') {
					this.digestData(ds);
				}
				//console.log("subscribeTo DS:" + JSON.stringify(ds) );
			}
		}
	});


	/////////////////////////////////////////////////////////////////////////////////




	SyncableDatasource = Class.extend({
		init: function (name, dataRef, datasourceRegistry, serverEndpoint, dontUseLocalStorage, iniValue, async) {
			if (serverEndpoint != undefined) {
				console.log("RUNNING EXTENDED SyncableDatasource for " + serverEndpoint);
			}
			this.index = 0;
			this.name = name;
			this.dataRef = dataRef;
			this.endpoint = BUrl + 'client/' + serverEndpoint;
			this.serverendpoint = serverEndpoint;
			this.dsregistry = datasourceRegistry;
			if (dontUseLocalStorage) {
				this.useLocalStorage = false;
			}
			else {
				this.useLocalStorage = true;
			}


			if (this.serverendpoint == "order_fittings_endpoint") {
				this.endpoint = BUrl + "client_fittings/" + serverEndpoint;
			}
			if (this.serverendpoint == "customer_garments_fittings_endpoint") {
				this.endpoint = BUrl + "client_fittings/" + serverEndpoint;
			}
			if (this.serverendpoint == "customer_garments_fittings_endpoint2") {
				this.endpoint = BUrl + "client_fittings/" + serverEndpoint;
			}
			if (this.serverendpoint == "alterations_endpoint" || this.serverendpoint == "alterations_endpoint2") {
				this.endpoint = BUrl + "client_alterations/" + serverEndpoint;
			}
			if (this.serverendpoint == "customer_alterations_endpoint" || this.serverendpoint == "customer_garments_endpoint" || this.serverendpoint == "history_boxes_endpoint"
				|| this.serverendpoint == "incoming_boxes_endpoint" || this.serverendpoint == "pending_items_endpoint" || this.serverendpoint == "shipping_notifications_numbers_endpoint") {
				this.endpoint = BUrl + "client_logistics/" + serverEndpoint;
			}

			if (iniValue) {
				this.store = iniValue.store;
			}
			else if (this.useLocalStorage && localStorage.getItem(dataRef) != null) {
				var st = JSON.parse(localStorage.getItem(dataRef));
				this.store = st.store;
				//			this.lastUpdate = st.timestamp;			// edited by JP
				for (ind in this.store) {
					if (this.store[ind] != null) {	// this control was added because of the ids in fabricsDS not being in the right order
						if (this.store[ind].id > this.index) {
							this.index = this.store[ind].id;
						}
					}
				}
				if (this.index != 0) {
					this.index += 1;
				}
			} else {
				//			this.lastUpdate = "0"; 			// edited by JP
				this.store = [];
				if (async) {
					this.sync(true);
				}
				else {
					this.sync(false);
				}
			}
			datasourceRegistry.register(name, this);
			//this.modelUpdate();
		},
		upsert: function (item, triggerUpdate) {
			//	console.log("RUNNING EXTENDED upsert for " + JSON.stringify(item) );
			//	console.log("typeof(item)" + typeof(item));

			if (this.serverendpoint != 'customer_endpoint') {
				if (typeof (item) != "undefined" && typeof (item.id) != "undefined") {
					item.isDirty = true;
					this.store[item.id] = item;
				} else {
					this.index += 1;
					item.id = this.index; //new Date().getTime(); 
					this.store[this.index - 1] = item;
				}
			} else {
				var theid = -1;
				for (var ind in this.store) {
					if (this.store[ind] != null) {
						if (this.store[ind].server_id == item.server_id) {
							item.isDirty = true;
							this.store[ind] = item;
							theid = ind;
							break;
						}
					}
				}
				if (theid == -1) {
					this.index += 1;
					item.id = this.index; //new Date().getTime(); 
					this.store[this.index - 1] = item;
				}
			}

			if (triggerUpdate) {
				this.modelUpdate();
			}
			return this.index - 1;
		},
		deleteItem: function (id, triggerUpdate) {
			if (typeof (this.store[id]) != "undefined") {
				this.store[id].isDead = true;
			}
			if (triggerUpdate) {
				this.modelUpdate();
			}
		},
		modelUpdate: function () {
			//	console.log('updating model');
			if (this.store.length !== 0 && this.useLocalStorage) {
				var st = {};
				st.store = this.store;
				st.timestamp = Math.round(new Date().getTime() / 1000);
				//			this.lastUpdate = st.timestamp;					// edited by JP
				if (typeof (localStorage.getItem(this.dataRef)) != 'undefined') {
					localStorage.removeItem(this.dataRef);
				}
				localStorage[this.dataRef] = JSON.stringify(st);
				$(document).trigger('modelUpdate', this.name);
			}
		},
		getStore: function () {
			return this.store;
		},
		setStore: function (data) {
			this.store = data;
			this.modelUpdate();
		},

		sync: function (is_async) {
			var spinner = document.getElementById('loading_jp');
			if (this.serverendpoint != undefined) {
				// 		spinner.style.display = "block";
				if (localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id) == 'undefined' || localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id) == null) {
					this.lastUpdate = '0';
				} else {
					this.lastUpdate = localStorage.getItem('lastUpdate_' + authCtrl.userInfo.user_id);
				}
				///////////	
				if (!is_async && this.useLocalStorage) {
					is_async = true;
				}
				var that = this;
				var diffArray = [];
				var newStore = that.store.concat();   //CLONE  

				if (that.serverendpoint != 'customer_endpoint') {
					spinner.style.display = "block";
				} else {
					spinner.style.display = "none";
				}
				console.log("THE SYNC FOR:" + that.serverendpoint);

				for (var ind in that.store) {
					if (that.store[ind] != null) {
						if (that.store[ind].isDirty || typeof (that.store[ind].server_id) == "undefined") { // updated or new
							diffArray.push(that.store[ind]);
							newStore[ind] = null;
						} else if (that.store[ind].isDead) {                                                // deleted (IT IS NOT USED)
							diffArray.push({
								isDead: true,
								server_id: that.store[ind].server_id
							})
							newStore[ind] = null;
						}
					}
				}

				var salesmanString = "{username:\"" + authCtrl.username() + "\",password:\"" + authCtrl.password() + "\", id:\"" + authCtrl.userInfo.user_id + "\"}";
				var salesman = JSON.stringify(eval("(" + salesmanString + ")"));
				var customerString = "0";
				try {
					customerString = orderItem.custSelectVM.selectedCustomer().server_id;
				} catch (e) {
					customerString = "0";
				}
				if (customerString == null) {
					customerString = "0";
				}

				var order_id_string = "0";
				try {
					order_id_string = orderItem.custOrdersVM.selectedOrder().order_id;
				} catch (e) {
					order_id_string = "0";
				}
				if (order_id_string == null) {
					order_id_string = "0";
				}

				var thetimestamp = that.lastUpdate;
				if (JSON.stringify(diffArray) != "[]" && thetimestamp == 0) {
					thetimestamp = Math.round(new Date().getTime() / 1000);
				}
				if (that.serverendpoint == 'customer_endpoint') {
					var tmp = JSON.parse(localStorage.getItem('customersDS_' + authCtrl.userInfo.user_id));
					if (tmp != null) {
						thetimestamp = tmp.timestamp;
					} else {
						thetimestamp = 0;
					}
				}
				if (order_id_string == "0" && customerString == "0") {
					data1 = {
						timestamp: thetimestamp,
						data: JSON.stringify(diffArray),
						salesman: salesman //JSON.stringify(eval("(" + salesmanString + ")"))
					};
				} else if (order_id_string == "0" && customerString != "0") {
					data1 = {
						timestamp: thetimestamp,
						data: JSON.stringify(diffArray),
						customer_id: customerString,
						salesman: salesman //JSON.stringify(eval("(" + salesmanString + ")"))//(eval("(" + salesmanString + ")"))
					};
				} else if (order_id_string != "0" && customerString == "0") {
					data1 = {
						timestamp: thetimestamp,
						data: JSON.stringify(diffArray),
						salesman: salesman,//JSON.stringify(eval("(" + salesmanString + ")"))//(eval("(" + salesmanString + ")")),
						order_id: order_id_string
					};
				} else {
					data1 = {
						timestamp: thetimestamp,
						data: JSON.stringify(diffArray),
						customer_id: customerString,
						salesman: salesman,//JSON.stringify(eval("(" + salesmanString + ")"))//(eval("(" + salesmanString + ")")),
						order_id: order_id_string
					};
				}

				var timeoutTime = 35000;
				if (that.serverendpoint == "fabrics_endpoint") {
					timeoutTime = 90000;
				}

				if (that.serverendpoint == "customer_measurements_endpoint" && data1.customer_id == undefined) {
					;
				} else {

					request = $.ajax({
						type: 'POST',
						url: that.endpoint,
						dataType: 'json',
						timeout: timeoutTime,
						data: data1,
						async: is_async,
						error: function (dataS) {

							if (dataS.statusText != "OK") {
								customAlert(that.serverendpoint + "<br/>Error: " + JSON.stringify(dataS));//.statusText );
							} else {
								console.log(that.serverendpoint + "\nStatus: " + dataS.statusText);
							}
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "none";

						},
						success: function (dataS) {
							console.log(that.name);
							var data = dataS;
							if (dataS == '') {
								data = {};
							}

							var newItems = data['new'];
							that.lastUpdate = data['timestamp'];

							localStorage.setItem('lastUpdate_' + authCtrl.userInfo.user_id, that.lastUpdate);

							if (that.serverendpoint == 'customer_endpoint' || that.serverendpoint == 'jacket_fitlines_endpoint' || that.serverendpoint == 'pants_fitlines_endpoint' ||
								that.serverendpoint == 'customer_measurements_endpoint' || that.serverendpoint == 'customer_bodyshape_endpoint') {
								that.store = cleanArray(newStore);
							}
							// finds new max index
							that.index = 0;
							for (ind in that.store) {
								if (that.store[ind].id > that.index) {
									that.index = that.store[ind].id;
								}
							}
							count = 0;

							// upserts new items
							for (ind in newItems) {
								that.upsert(newItems[ind]);
								count++;
							}

							that.modelUpdate();

							console.log("ALL DONE");
							// DYMAMIC SYNCING
							if (that.serverendpoint == 'customer_measurements_endpoint') {
								orderItem.syncmeasurementsVM();
							} else if (that.serverendpoint == 'customer_bodyshape_endpoint') {
								orderItem.syncbodyshapeVM();
							} else if (that.serverendpoint == 'jacket_fitlines_endpoint') {
								if (count == 0) {
									fitlinesJacketDS.sync();
								}
							} else if (that.serverendpoint == 'customer_endpoint' && orderItem.newcustomerorder == true) {	// SETTING THE SELECTED CUSTOMER AGAIN IN CASE OF NEW CUSTOMER, SO THAT IMAGE POSTING WORKS OK. 
								orderItem.newcustomerorder = false;
								var new_cust_id = that.dsregistry.getDatasource("customersDS_" + authCtrl.userInfo.user_id).index - 1;
								var new_cust = that.dsregistry.getDatasource("customersDS_" + authCtrl.userInfo.user_id).store[new_cust_id];
								new_cust = that.dsregistry.getDatasource("customersDS_" + authCtrl.userInfo.user_id).store[new_cust_id];
								orderItem.custSelectVM.selectedCustomer(new_cust);
							}
							try {
								if (customersVM.systemMode != "shipping_copy_garment" || customersVM.systemMode != "shipping_remake_garments" || that.serverendpoint != 'customer_measurements_endpoint') {
									spinner.style.display = "none";
								}
							} catch (exception) {
								spinner.style.display = "none";
							}
							if (that.serverendpoint == 'shipping_notifications_numbers_endpoint') {
								try {
									document.getElementById("pendingnotifications").innerHTML = that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[0];
									if (that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[0] == undefined || that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[0] == 0) {
										document.getElementById("pendingnotifications").style.display = 'none';
									} else {
										document.getElementById("pendingnotifications").style.display = 'block';
									}
								} catch (e) { ; }
								try {
									document.getElementById("incomingnotifications").innerHTML = that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[1];
									if (that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[1] == undefined || that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[1] == 0) {
										document.getElementById("incomingnotifications").style.display = 'none';
									} else {
										document.getElementById("incomingnotifications").style.display = 'block';
									}
								} catch (e) { ; }
								try {
									document.getElementById("totalnotifications").innerHTML = that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[2];
									if (that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[2] == undefined || that.dsregistry.getDatasource('ShippingNotificationsDS').getStore()[2] == 0) {
										document.getElementById("totalnotifications").style.display = 'none';
									} else {
										document.getElementById("totalnotifications").style.display = 'block';
									}
								} catch (e) { ; }
							}

						}
					});
				}
			} else {
				spinner.style.display = "none";
			}

		}
	});


	SimpleDatasource = Class.extend({
		init: function (name, data, datasourceRegistry) {
			this.index = 0;
			this.name = name;
			this.store = data;
			datasourceRegistry.register(name, this);
			this.modelUpdate();
		},
		modelUpdate: function () {
			console.log("modelUpdate AT APPBASE 2");
			$(document).trigger('modelUpdate', this.name);
		},
		setStore: function (data, noupdate) {
			this.store = data;
			if (!noupdate) { this.modelUpdate(); }
		},
		getStore: function () {
			//console.log("appBase.js getStore");		
			return this.store;
		}
	});
	


	//Define closure end
});