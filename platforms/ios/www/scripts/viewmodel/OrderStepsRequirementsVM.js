define(['jquery', 'knockout', 'base'], function($, ko) {
    
    /**
     * This VM will be responsible to check if all requiremnts are met
     * before moving forward/backward in the Group orders process
     *
     * This VM will have a lot of 'dity' and 'ugly' code, so its better to separate from the 
     * more important code related to the Group orders business logic.
     *      
     * */
    OrderStepsRequirementsVM = SimpleControl.extend({

    	init: function(ordersVM) {

    		this.ordersVM = ordersVM;

            console.log("Init OrderStepRequirementsVM")
            var self = this;
            
        },

        nextBtn : function(currentStepId) {

            try 
            {
	            switch(currentStepId)
	            {
	            	case '1': 
	            		this.nextBtnSelectGarmentsStep();
	            	break;

	            	case '2': 
	            		this.nextBtnMeasurmentsStep();
	            	break;

	            	case '3': 
                        this.nextBtnBodyShapesStep();
	            	break;

	            	case '4': 
                        this.nextBtnImagesStep();
	            	break;

	            	case '5': 
                        this.nextBtnVideoStep();
	            	break;

	            	case '6': 
                        this.nextBtnDesignStep();
                    break;
                    
	            	case '7': 
                        this.nextBtnPaymentStep();
	            	break;
	            }
	        }
	        catch(errMessage) 
	        {
	        	console.log('Error ', errMessage);
	        	alert(errMessage);
	        }

        },

        /**
         * Action for the next btn inside the select garments Step
         *
         * Requirement: All customers needs to have at least one garment
         * 
         */
        nextBtnSelectGarmentsStep : function() {

        	const res = this.checkGarmentsStep();
        	/**
    		 * If one customer doest have any garments => throw error
    		 */
    		if( !res )
    		{
    			throw "All customers need to have at least one garment !!";
    		}

        	const nextStep = this.ordersVM.steps.filter( e => e.id == '2' )[0];
        	this.ordersVM.changeStep(nextStep.id); 

            // selects the first PRESENT customer
            const presentCustomer = this.ordersVM.customers().filter(el=>el.is_present)[0];
            if (presentCustomer){
                this.ordersVM.selectedCustomer(presentCustomer);
            }else{
                this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
            }
        },

        /**
         * Logic to check if garment step is complete
         * ******* Can be used outside the context of the nextBTN  => for example: to color the step in green
         * @return {[type]} [description]
         */
        checkGarmentsStep : function() {

        	const customers = this.ordersVM.customers();
        	let res = true;

        	for(let customer of customers)
        	{
        		let hasGarments = false;
        		
        		for( let garmentType of customer.garmentsStep.garments() ) // checks if there is at least one garment per customer
        		{
        			if( garmentType.garments().length > 0)
        			{
        				hasGarments = true;
        				continue;
        			}
        		}

        		if( !hasGarments )
        		{
        			res = false;
        			break;
        		}
        	}

        	return res;
        },

        checkMeasurementsSTep: function () {
            const customers = this.ordersVM.customers();

            // list of not mandatory measurments
            const notMandatoryMeaskeys = ['jacket_measurements_lapel_length',
                'pants_measurements_front_crotch',
                'pants_measurements_zipper',
                'pants_measurements_inseam',
                'shirt_measurements_watch',
                'vest_measurements_v_length',
                'jacket_measurements_notes',
                'pants_measurements_notes',
                'pants_measurements_angled_hems',
                'shirt_measurements_notes',
                'vest_measurements_notes',
                'jacket_measurements_last_edited',
                'pants_measurements_last_edited',
                'shirt_measurements_last_edited',
                'vest_measurements_last_edited'
            ];


            for (let customer of customers) {

                // check if customer is present
                if (!this.customerIsPresent(customer)) {
                    continue;
                }

                // Gets the garments types that the user selected in the select garments step
                const selectedGarmentTypesNames = customer.garmentsStep.garments().filter(e => e.garments().length > 0).map(e => e.name);

                console.log("selectedGarmentTypesNames", selectedGarmentTypesNames);

                // only check measurments for the selected garments types in the garments step
                const necessaryMeasurments = customer.measurementsStep.garments.filter(e => selectedGarmentTypesNames.includes(e.name().toLowerCase()));

                console.log("necessaryMeasurments", necessaryMeasurments);

                for (let garmentType of necessaryMeasurments) {
                    for (let meas of garmentType.measurements()) {

                        // console.log("meas.title()", meas.title() );
                        // console.log("meas.value()", meas.value() );

                        if (notMandatoryMeaskeys.includes(meas.key())) { continue; }


                        // If measurment is empty or ZERO -- Do not include anlged pants hems...
                        if ( meas.value == 'undefined' || !meas.value() || meas.value().trim() == '' || parseFloat(meas.value()) <= 0.0) {
                            // console.log('Changing meas selected customer...');
                            // Change to this customer
                            // this.ordersVM.changeCustomer(customer.customer_id);
                            // $.jGrowl(`Please fill in: ${meas.title()} for ${customer.full_name} !!`, { life: 7000 });
                            return {'success':false, 'meas':meas, 'customer':customer};
                        }
                    }
                }
            }

            return {'success':true};
        },

        /**
         * Action for the next btn inside the select garments Step
         *
         * Requirement: All mandatory measurments for all the customers needs to be filled up by the user
         * 
         */
        nextBtnMeasurmentsStep : function() {
            var checkMeasurement = this.checkMeasurementsSTep()
            if (checkMeasurement.success == false){
                console.log('Changing meas selected customer...');
                // Change to this customer
                this.ordersVM.changeCustomer(checkMeasurement.customer.customer_id);
                $.jGrowl(`Please fill in: ${checkMeasurement.meas.title()} for ${checkMeasurement.customer.full_name} !!`, { life: 7000 });
                return
            };
    
        	// If all customers have all measurments => change to next step
        	const nextStep = this.ordersVM.steps.filter( e => e.id == '4' )[0];
            this.ordersVM.changeStep(nextStep.id);

            // selects the first PRESENT customer
            const presentCustomer = this.ordersVM.customers().filter(el=>el.is_present)[0];
            if (presentCustomer){
                this.ordersVM.selectedCustomer(presentCustomer);
            }else{
                this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
            }
        },



        /**
         * requirement : All bodyshapes needs to be filled up
         * 
         *         TODO future: Only require the bodyshapes that makes sense for the selected garments
         *                         ex. If customer has only pants => show and require only the lower body bodyshape options
         *
         * 
         * @return {[type]} [description]
         */
        nextBtnBodyShapesStep : function() {

            const customers = this.ordersVM.customers();

            for(let customer of customers)
            {
                // check if customer is present
                if (!this.customerIsPresent(customer)) {
                    continue;
                }

                for( let bodyshapeCategory of customer.bodyShapesStep.bodyShapes() ) // checks if there is at least one garment per customer
                {
                    if( !bodyshapeCategory.selectedValue() )
                    {
                        // Change to this customer
                        this.ordersVM.changeCustomer(customer.customer_id);
                        //alert(`Please select ${bodyshapeCategory.cat_name} bodyshape for ${customer.full_name} !!`);
                        $.jGrowl(`Please select ${bodyshapeCategory.cat_name} bodyshape for ${customer.full_name} !!`, { life: 7000 });
                        return;
                    }
                }
            }

            // If all customers have all bodyshapes => change to next step
            const nextStep = this.ordersVM.steps.filter( e => e.id == '6' )[0];
            this.ordersVM.changeStep(nextStep.id);
            // selects the first customer
            this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
        },

        /**
         * requirement: Front/back/side images are mandatory
         *
         *          Maybe: Are tags mandatory ?
         *          
         * @return {[type]} [description]
         */
        nextBtnImagesStep : function() {

            const customers = this.ordersVM.customers();

            for(let customer of customers)
            {
                // check if customer is present
                if (!this.customerIsPresent(customer)) {
                    continue;
                }

                for( let image of customer.imageData ) // checks if there is at least one garment per customer
                {
                    // additional are not mandatory
                    if( image.type === 'additional' ) { continue; }

                    if( !image.url() || image.url().trim() == "")
                    {
                        this.ordersVM.changeCustomer(customer.customer_id);
                        $.jGrowl(`Please take ${image.type} image for ${customer.full_name} !!`, { life: 7000 });
                        return;
                    }
                }
            }

            // If all customers have all bodyshapes => change to next step
            const nextStep = this.ordersVM.steps.filter( e => e.id == '5' )[0];
            this.ordersVM.changeStep(nextStep.id);

            // selects the first PRESENT customer
            const presentCustomer = this.ordersVM.customers().filter(el=>el.is_present)[0];
            if (presentCustomer){
                this.ordersVM.selectedCustomer(presentCustomer);
            }else{
                this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);
            }
        },


        nextBtnVideoStep : function() {

            // check if all customers has video
            for(let customer of this.ordersVM.customers())
            {
                // if one does not have video. Ask if the program may continue
                if(customer.uploadVideosStep.videos() && customer.uploadVideosStep.videos().length > 0 ){
                    // has video
                }
                else {
                    // does not have video
                    if (customer.is_present) {
                        if(!confirm(`Customer ${customer.full_name} does not have Video. Are you sure you want to proceed without a video?`)){
                            return;
                        }
                    }
                }
            }

            const nextStep = this.ordersVM.steps.filter( e => e.id == '3' )[0];
            this.ordersVM.changeStep(nextStep.id);

            // selectes the first customer
            this.ordersVM.selectedCustomer(this.ordersVM.customers()[0]);

        },

        /**
         * This functions loops through the Garments and its design steps.
         *     Once it gets to the last design step of the last garment, it goes to the payments step.
         */
        nextBtnDesignStep: function () {

            var self = this;

            console.log("new next btn click");

            const currentGarmentType = this.ordersVM.currentDesigingGarmentType();
            // gets the active garment type
            const currentGarmentTypeName = currentGarmentType.garmentType;
            // gets the active garment type steps
            const currentGarmentTypeDesignSteps = this.ordersVM.globalDesignCategoriesArray().filter( e => e.garmentType == currentGarmentTypeName)[0];
            // gets the selected design step
            const currentDesignStep = this.ordersVM.currentDesignStep();
            // gets the position in the array of the current one
            var currentDesignArrayIndex = currentGarmentTypeDesignSteps.categories.indexOf(currentDesignStep) == -1 ? 0 : currentGarmentTypeDesignSteps.categories.indexOf(currentDesignStep);

            // this variable should hold the next step for design
            let nextStep = false;

            // if none is selected => select the first one
            if( !currentDesignStep )
            {
                nextStep = currentGarmentTypeDesignSteps.categories[0];
            }
            // Finds the next step and move to it
            else
            {   
                console.log("currentDesignArrayIndex i ", currentDesignArrayIndex);
                console.log("length", currentGarmentTypeDesignSteps.categories.length);


                // if current design is one of the above, change garment before change step
                // Lapel details (jacket and suit) => cat_id: [11 and 53]
                // Sleeves (jacket and suit)       => cat_id: [7 and 54]
                // monogram (jacket and suit)      => cat_id: [46 and 63]
                // Contrast (shirt)                => cat_id: [38]
                // Monogram (shirt)                => cat_id: [39]
                // notes (all)                     => cat_id: [10, 19, 64, 28, 40]
                // 65 => custom data for pants
                // 66 => custom data for shirt
                // 67 => custom data for vest
                // 68 => custom data for jacket
                // 69 => custom data for suit
                if(["11", "53", "7", "54", "46", "63", "38", "39", "10", "19", "64", "28", "40","65", "66", "67", "68", "69"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex].id)){
                    // get current garment position of array
                    let currentGarmentPosition =  this.ordersVM.globalGarments[currentGarmentTypeName]().indexOf(this.ordersVM.currentGarmentInstance())
                    
                    // select next garment
                    if (currentGarmentPosition!=-1 && currentGarmentPosition+1 < this.ordersVM.globalGarments[currentGarmentTypeName]().length){
                        let nextGarment = this.ordersVM.globalGarments[currentGarmentTypeName]()[currentGarmentPosition+1];
                        this.ordersVM.currentGarmentInstance(nextGarment);
                        return;
                    }
                }

                // if next step is "custom data" but no custom options was selected, just skip this option
                // 65 => custom data for pants
                // 66 => custom data for shirt
                // 67 => custom data for vest
                // 68 => custom data for jacket
                // 69 => custom data for suit
                if (currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1] && 
                    ["65", "66", "67", "68", "69"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1].id)){
                    // const custom_category = currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1];
                    
                    // check if any garment of current type has custom design

                    console.log('anyGarmentHasCustomDesign = ', this.ordersVM.selectedCustomer().garmentsDesignStep.anyGarmentHasCustomDesign());
                    if (!this.ordersVM.selectedCustomer().garmentsDesignStep.anyGarmentHasCustomDesign() ||
                        !this.ordersVM.selectedCustomer().garmentsDesignStep.anyGarmentHasCustomDesign().length > 0) {

                            // skip
                            currentDesignArrayIndex+=1;
                    }
                }

                // If there is next step in this garment...
                if( currentDesignArrayIndex + 1 < currentGarmentTypeDesignSteps.categories.length )
                {
                    // fabric case
                    // fabric category for pants : 12
                    // fabric category for shirt : 29
                    // fabric category for vest  : 20
                    // fabric category for jacket: 2
                    // fabric category for suit  : 41
                    // fabric category for suit  : 47
                    if (["12", "29", "20", "2", "41", "47"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex].id)) {
                       
                        // check fabrick for all garments of current type
                        for (const garment of this.ordersVM.globalGarments[currentGarmentTypeName]()) {
                            if (!garment.garment.categories[0].options[0].selectedValue()){
                                currentDesignStep.completed = ko.observable(false);
                                $.jGrowl(`Missing ${currentDesignStep.name}`, { life: 7000 });
                                return;
                            }
                            
                        }
                        currentDesignStep.completed = ko.observable(true);



                    }

                    // check if current category is completed (ignore custom data)
                    // 65 => custom data for pants
                    // 66 => custom data for shirt
                    // 67 => custom data for vest
                    // 68 => custom data for jacket
                    // 69 => custom data for suit
                    else if (currentDesignStep.completed() == false &&  !["65", "66", "67", "68", "69"].includes(currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex].id)){
                        $.jGrowl(`Missing ${currentDesignStep.name}`, { life: 7000 });
                        return;
                    }
                    
                    nextStep = currentGarmentTypeDesignSteps.categories[currentDesignArrayIndex + 1];
                }
                else // Else => change garment type
                {
                    // gets only the ones with garments count > 0
                    const globalGarmentsArray = this.ordersVM.globalGarmentsArray().filter( e => self.ordersVM.globalGarments[e.garmentType]().length > 0 );

                    // Gets the index of the selected garment
                    const currentGarmentIndex = globalGarmentsArray.indexOf(currentGarmentType);

                    console.log("globalGarmentsArray", globalGarmentsArray);

                    /**
                     * If there is NEXT garment, proceed.
                     *     We select the next garment and call this function again.
                     */
                    if( currentGarmentIndex + 1 < globalGarmentsArray.length )
                    {
                        const nextGarment = globalGarmentsArray[currentGarmentIndex + 1];
                        this.ordersVM.chooseDesignGarmentType(nextGarment);
                        this.nextBtnDesignStep();
                        return;
                    }
                    else // Else => go to last step
                    {
                        this.ordersVM.changeStep('7');
                        return;
                    }
                }
            }

            // Finally, choose the next step.
            this.ordersVM.chooseCurrentDesignStep(nextStep);
        },

        nextBtnPaymentStep:function(){
            function showAlertsFunc(alert, show=true) {
                if (show){
                    customAlert(alert);
                }
            }

            for (const customer of this.ordersVM.customers()) {
                if (customer.is_paying == true){
                    
                    // checking
                    if (!customer.paymentsStep.selectedPaymentMethod()) {
                        showAlertsFunc(`Please, fill payment method for customer ${customer.full_name} `);
                        this.ordersVM.changeCustomer(customer.customer_id);
                        return;
                    }
                    if (!customer.paymentsStep.selectedOrderCity()) {
                        showAlertsFunc(`Please, fill order city for customer ${customer.full_name} `);
                        this.ordersVM.changeCustomer(customer.customer_id);
                        return;
                    }
                    if (!customer.paymentsStep.terms.accepted()) {
                        showAlertsFunc(`Please, check if customer ${customer.full_name} accepted terms`);
                        this.ordersVM.changeCustomer(customer.customer_id);
                        return;
                    }
                    if (!customer.paymentsStep.signing()) {
                        showAlertsFunc(`Please, fill signature for customer ${customer.full_name} `);
                        this.ordersVM.changeCustomer(customer.customer_id);
                        return;
                    }
                    if (!customer.paymentsStep.selectedFittingCity()) {
                        showAlertsFunc(`Please, fill fill fitting city for customer ${customer.full_name} `);
                        this.ordersVM.changeCustomer(customer.customer_id);
                        return;
                    }

                }
            }
        },

        /**
         * Checks details for the order submission
         * 
         * @return {[type]} [description]
         */
        checkSubmissionRequirments : function() {

            console.log("Checking steps before sumission....");
            // orders.customers()[0].garmentsStep.garments()[0].garments()
            // Checks if all garments have fabrics
            //const garments = self.ordersVM.customers().map(customer => customer.garmentsStep.garments().map( garmentType => garmentType.garments().map(garment => garment ) ).concat() ).reduce( (prev, curr) => { return prev.concat(curr); } ).reduce( (prev, curr) => { return prev.concat(curr); } );

            for(let customer of this.ordersVM.customers())
            {
                for(let garmentType of customer.garmentsStep.garments() )
                {
                    for(let garment of garmentType.garments() )
                    {
                        // checks if the garment has fabric
                        if( !garment.selectedFabric() && !garment.useCustomerFabric() )
                        {
                            customAlert(`You need to add fabric for ${customer.full_name} ${garment.garment_name} !`); 
                            return false;
                        }






                    }
                }
            }





        },

        /**
         * Send the saved designs from local storage to ERP
         */
        sendSavedDesigns() {

            var self = this;
            
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/submit_saved_designs',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "savedDesigns": self.getSavedDesignStringify()
                },

                success: function (dataS) {
                    // nothing to do
                    console.log("saved designs successfully sent");
                    
                },

                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: true
            });

        },

        /**
         * self.ordersVM.selectedCustomer().garmentsDesignStep.loadGarmentFromLocalStorage()() returns a cyclic object,
         * so the method JSON.stringify crashes. With this method, the problem is solved
         */
        getSavedDesignStringify(){

            const obj = this.ordersVM.selectedCustomer().garmentsDesignStep.loadGarmentFromLocalStorage()();

            var seen = [];
            
            return JSON.stringify(obj, function(key, val) {
                if (val != null && typeof val == "object") {
                    if (seen.indexOf(val) >= 0) {
                        return;
                    }
                    seen.push(val);
                }
                return val;
            });
        },

        /**
         * 
         * @param {*} customer 
         * @returns {Boolean} true if is present, e false if is not
         */
        customerIsPresent : function (customer) {
            return customer.is_present ? true : false;
        }

    })


});