define(['jquery', 'knockout', 'base'], function($, ko) {
	
	NotificationsVM = SimpleControl.extend({
		init: function(DsRegistry) {
			this.fittings = ko.observable();
			this.errors = ko.observable();
			this.alteration = ko.observable();
			this.unsync = ko.observable();
			
		},
		loadInfo : function(){
			var that = this;
			url = BUrl + 'medias_endpoint/get_unsync_medias2';
			
			that.fittings("");
			that.errors("");
			that.alteration('');
			that.unsync('');
			mediaData = {
				user_id : authCtrl.userInfo.user_id,
				device_id: typeof device != "undefined"? device.uuid: ""
			};
			
			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()
				
			};
			
			mediaDatapost = JSON.stringify(mediaData);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);
			$.ajax({
				type: 'POST',
				url: url,
				timeout: 45000,
				data: {
					data: mediaDatapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function(dataS) {
					
					var temp = JSON.parse(dataS);
					console.log("TEMP", temp);
					
					//if(temp.not_fittings != '0')that.fittings(temp.not_fittings);
					//if(temp.not_errors != '0')that.errors(temp.not_errors);
					//if(temp.not_alterations != '0')that.alteration(temp.not_alterations);
					//if(temp.num_unsync != '0')that.unsync(temp.num_unsync);

					$(".num-unsync-media").html(temp.length);

				},
				async: true
			});
		},
	});
});