define(['jquery', 'knockout', 'base'], function($, ko) {


    MeasurementsStepVM = SimpleControl.extend({

        init: function(customer, fitlines, ordersVM) {
            console.log("Init MeasurementsStepVM")
            var self = this;
            this.customer = customer;

            this.garments = this.buildGarmentsMeasurments();
            this.fitlines = ko.observableArray(fitlines);

            this.selectedFitline = ko.observable(this.fitlines[0]);
            this.selectedGarment = ko.observable("");
            this.selectedMeasurmentHistory = ko.observable(null);

            this.measurementsHistory = ko.observableArray(this.buildMeasurementsHistory());

            this.ordersVM = ordersVM;

            this.isSuitJacketMeasurementsVisible = this.shouldShowSuitJacketMeas();
            this.isSuitPantsMeasurementsVisible = this.shouldShowSuitPantsMeas();
        },

        getSubmissionData : function() {

            var self = this;

            const measurements = [];

            // we need to know if there is suit to put the jacket and pants measurments 
            let hasSuit = this.customer.garmentsStep.garments().filter( gar => gar.name == 'suit' )[0].garments().length > 0;

            for( let garmentType of this.customer.measurementsStep.garments )
            {
                // get the number of garments for this type.
                let garmentCount = 0;

                // garment count for SUIT should be generate both for pants and jacket, thus we can have the measurments
                // even if the customer buys only a SUIT
                if( hasSuit && (garmentType.name() == 'jacket' || garmentType.name() == 'pants')  )
                {
                    garmentCount = 1;
                }   
                else
                {
                    garmentCount = this.customer.garmentsStep.garments().filter( gar => gar.name == garmentType.name() )[0].garments().length;
                }   

                // if there is no garments for this garmentType => continue 
                if( garmentCount === 0) { continue; }

                const measurement = {
                    garment : garmentType.name(),
                    fitline_id : this.selectedFitline() ? this.selectedFitline().jacket_fitline_id : 1, // 1 => no fitline
                    measurements : ko.mapping.toJS(garmentType.measurements())
                };

                measurements.push(measurement);
            }
            

            return measurements
        },


        /**
         * Sends the measurements to ERP 
         * after the measurment step is done.
         */
        syncMeasurements : function() {

            var self = this;

            const measurements = this.getSubmissionData();

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/sync_measurements',
                dataType: 'json',
                data: { 
                    "user": authCtrl.userInfo,
                    "measurements": JSON.stringify(measurements),
                    "customer_id" : self.customer.customer_id
                }, 
                success: function (dataS) {
                    
                    console.log(dataS);

                    if( dataS.result == 'success' )
                    {
                        $.jGrowl(`${self.customer.customer_first_name} measurements Sync ! `);
                    }
                   
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                },
                async: false
            });



        },



        /**
         * Computes if the page should show the Suit partes measurements => jacket/Pants
         *
         * Checks if they are already visible,
         *     If yes => do not show
         *     Else => show
         * @return {[type]} [description]
         */
        shouldShowSuitJacketMeas : function() {

            var self = this;

                console.log("claculating if should show suit jacket meas...");

                const hasJacket = self.customer.garmentsStep.garments().filter( e => e.name.toLowerCase().trim() === 'jacket')[0].garments().length > 0;
                const hasSuit = self.customer.garmentsStep.garments().filter( e => e.name.toLowerCase().trim() === 'suit')[0].garments().length > 0;
                
                console.log("hasJacket", hasJacket);

                if(!hasJacket && hasSuit)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        },

        /**
         * Computes if the page should show the Suit partes measurements => jacket/Pants
         * 
         * @return {[type]} [description]
         */
        shouldShowSuitPantsMeas : function() {

            var self = this;

                console.log("claculating if should show suit pants meas...");
                
                const hasPants = self.customer.garmentsStep.garments().filter( e => e.name.toLowerCase().trim() === 'pants')[0].garments().length > 0;
                const hasSuit = self.customer.garmentsStep.garments().filter( e => e.name.toLowerCase().trim() === 'suit')[0].garments().length > 0;

                console.log("hasPants", hasPants);

                // If the customer has SUIT and doest have PANTS => show the pants measurments for the suit part
                if(!hasPants && hasSuit)
                {
                    return true;
                }
                else
                {
                    return false;
                }

        },


        


        buildGarmentsMeasurments : function() {

            let measurements = [];

            for( let meas_name in this.customer.measurements)
            {
                const current_meas = Object.keys(this.customer.measurements[meas_name]).map(key => this.customer.measurements[meas_name][key])
 
                const meas = 
                {
                    'name': meas_name,
                    'measurements' : current_meas
                };

                // change mandatory measurements
                if (meas_name.toLowerCase() == 'jacket') {
                    for (let measurement of meas.measurements){
                        if (["jacket_measurements_last_edited", "jacket_measurements_lapel_length", "jacket_measurements_notes"].includes(measurement.key.toLowerCase())) {
                            measurement.mandatory = false;
                        }
                        else{
                            measurement.mandatory = true;
                        }
                    }
                } else if (meas_name.toLowerCase() == 'pants') {
                    for (let measurement of meas.measurements){
                        if (["pants_measurements_last_edited", "pants_measurements_front_crotch", "pants_measurements_inseam", "pants_measurements_zipper", "pants_measurements_notes"].includes(measurement.key.toLowerCase())) {
                            measurement.mandatory = false;
                        }
                        else{
                            measurement.mandatory = true;
                        }
                    }


                } else if (meas_name.toLowerCase() == 'shirt') {
                    for (let measurement of meas.measurements){
                        if (["shirt_measurements_last_edited", "shirt_measurements_wrist", "shirt_measurements_notes"].includes(measurement.key.toLowerCase())) {
                            measurement.mandatory = false;
                        }
                        else{
                            measurement.mandatory = true;
                        }
                    }


                } else if (meas_name.toLowerCase() == 'vest') {
                    for (let measurement of meas.measurements){
                        if (["vest_measurements_last_edited", "vest_measurements_v_length", "vest_measurements_notes"].includes(measurement.key.toLowerCase())) {
                            measurement.mandatory = false;
                        }
                        else{
                            measurement.mandatory = true;
                        }
                    }


                }
                
                measurements.push(ko.mapping.fromJS(meas));
            }

            return measurements;
        },

        buildMeasurementsHistory : function() {

            let history = [];

            for(let garment_name in this.customer.measurements_history)
            {
                const meas = {
                    'name' : garment_name,
                    'measurements' : this.customer.measurements_history[garment_name]
                };

                history.push(meas);
            }

            return history;
        },

        selectGarment: function(garment) {

            console.log("Selecting...", garment.name);
            this.selectedGarment(garment.name);

        },

        selectMeasurmentHistory : function(measHistory) {
            this.selectedMeasurmentHistory(measHistory);

            // 'Blink' effect
            $(".meas-hist-value-wrapper").fadeOut().fadeIn()
        },

        getMeasHistoryValue : function(meas) {

            //console.log("this.selectedMeasurmentHistory()", this.selectedMeasurmentHistory());
            //
            //console.log("Meas", meas); 
            //console.log("history", this.selectedMeasurmentHistory());


            if( this.selectedMeasurmentHistory() )
            {
                const hist_mea = this.selectedMeasurmentHistory().keys.filter( e => meas.key() == e.key);

                return hist_mea[0] ? parseFloat(hist_mea[0].value) : false;
            }
            else
            {
                return false;
            }
        },

        getFitline : function(meas) {


            if( meas.fitline_key && this.selectedFitline() && this.selectedFitline()[meas.fitline_key()] )
            {   
                const fitline_val = this.selectedFitline()[meas.fitline_key()];

                // eval to solve 45.00 + 2.00
                return eval(fitline_val) > 0 ? fitline_val : false;
            }
            else
            {
                return false;
            }

            
        },

        // orders.selectedCustomer().measurementsStep.toggleStepComplete()
        toggleStepComplete: function () {
            orders.steps.filter(function (el) { return el.id == '2' })[0].complete =
                this.ordersVM.orderStepRequirementsVM.checkMeasurementsSTep().success;

        },

        /**
         * Copy measurement value to other garments.
         * 
         * ex: chest for jacket must be equal chest for shirt
         * 
         * 
         * @param {Object} measurement 
         */
        copyMeasurement : function(measurement){
            var self = this;
            const _setMeas = function(garment_type, meas_key, meas_value){
                // find measurement by key
                const meas_garment = self.garments.filter(el => el.name() == garment_type)[0];
                
                if (!meas_garment) return ;

                const meas_to_update = meas_garment.measurements().filter(el => el.key() == meas_key)[0];

                // set value if current value is not zero or null
                if (meas_to_update && ['','0',0, undefined, null].includes(meas_to_update.value())){
                    meas_to_update.value(meas_value);
                }
            }

            // back
            if (['jacket_measurements_back', 'shirt_measurements_back'].includes(measurement.key())) {
                _setMeas('jacket', 'jacket_measurements_back', measurement.value());
                _setMeas('shirt', 'shirt_measurements_back', measurement.value());
               
            }
            
            // bicep
            else if (['jacket_measurements_bicep', 'shirt_measurements_bicep'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_bicep', measurement.value());
                _setMeas('shirt', 'shirt_measurements_bicep', measurement.value());
                
            }
            
            // chest
            else if (['jacket_measurements_chest', 'vest_measurements_chest', 'shirt_measurements_chest'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_chest', measurement.value());
                _setMeas('vest', 'vest_measurements_chest', measurement.value());
                _setMeas('shirt', 'shirt_measurements_chest', measurement.value());
            }

            // Forearm
            else if (['jacket_measurements_fore_arms', 'shirt_measurements_forearm'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_fore_arms', measurement.value());
                _setMeas('shirt', 'shirt_measurements_forearm', measurement.value());
            }

            // front
            else if (['jacket_measurements_front', 'shirt_measurements_front'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_front', measurement.value());
                _setMeas('shirt', 'shirt_measurements_front', measurement.value());
            }

            // Front
            else if (['jacket_measurements_front', 'shirt_measurements_front'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_front', measurement.value());
                _setMeas('shirt', 'shirt_measurements_front', measurement.value());
            }

            // Full Shoulder
            else if (['jacket_measurements_full_shoulder', 'vest_measurements_full_shoulder', 'shirt_measurements_full_shoulder'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_full_shoulder', measurement.value());
                _setMeas('vest', 'vest_measurements_full_shoulder', measurement.value());
                _setMeas('shirt', 'shirt_measurements_full_shoulder', measurement.value());
            }

            // Hips
            else if (['jacket_measurements_hips', 'shirt_measurements_hips'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_hips', measurement.value());
                _setMeas('shirt', 'shirt_measurements_hips', measurement.value());
            }

            // Left Back to Zero
            else if (['jacket_measurements_right_back_to_zero', 'shirt_measurements_back_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_right_back_to_zero', measurement.value());
                _setMeas('shirt', 'shirt_measurements_back_to_zero', measurement.value());
            }
            
            // Left Front to Zero
            else if (['jacket_measurements_right_front_to_zero', 'shirt_measurements_front_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_right_front_to_zero', measurement.value());
                _setMeas('shirt', 'shirt_measurements_front_to_zero', measurement.value());
            }
            
            // Left Sleeve
            else if (['jacket_measurements_sleeve_left', 'shirt_measurements_sleeve_left'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_sleeve_left', measurement.value());
                _setMeas('shirt', 'shirt_measurements_sleeve_left', measurement.value());
            }
            
            // Neck
            else if (['jacket_measurements_neck', 'vest_measurements_length', 'shirt_measurements_neck'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_neck', measurement.value());
                _setMeas('vest', 'vest_measurements_length', measurement.value());
                _setMeas('shirt', 'shirt_measurements_neck', measurement.value());
            }
            
            // Right Back to Zero
            else if (['jacket_measurements_back_shoulder_to_zero_point', 'shirt_measurements_right_back_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_back_shoulder_to_zero_point', measurement.value());
                _setMeas('shirt', 'shirt_measurements_right_back_to_zero', measurement.value());
            }
            
            // Right Front to Zero
            else if (['jacket_measurements_front_shoulder_to_zero_point', 'shirt_measurements_right_front_to_zero'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_front_shoulder_to_zero_point', measurement.value());
                _setMeas('shirt', 'shirt_measurements_right_front_to_zero', measurement.value());
            }
            
            // Right Sleeve
            else if (['jacket_measurements_sleeve_right', 'shirt_measurements_sleeve_right'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_sleeve_right', measurement.value());
                _setMeas('shirt', 'shirt_measurements_sleeve_right', measurement.value());
            }
            
            // Stomach
            else if (['jacket_measurements_stomach', 'vest_measurements_neck', 'shirt_measurements_stomach'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_stomach', measurement.value());
                _setMeas('vest', 'vest_measurements_neck', measurement.value());
                _setMeas('shirt', 'shirt_measurements_stomach', measurement.value());
            }
            
            // Tense Bicep
            else if (['jacket_measurements_tense_bicep', 'shirt_measurements_tense_bicep'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_tense_bicep', measurement.value());
                _setMeas('shirt', 'shirt_measurements_tense_bicep', measurement.value());
            }
            
            // Wrist
            else if (['jacket_measurements_wrist', 'shirt_measurements_wrist'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_wrist', measurement.value());
                _setMeas('shirt', 'shirt_measurements_wrist', measurement.value());
            }
            
            // Zero Point
            else if (['jacket_measurements_zero_point', 'shirt_measurements_zero_point'].includes(measurement.key())){
                _setMeas('jacket', 'jacket_measurements_zero_point', measurement.value());
                _setMeas('shirt', 'shirt_measurements_zero_point', measurement.value());
            }
            


        },


        /**
         * Checks if all measurments have been input by the user
         * @return {Boolean} [description]
         */
        isGarmentTypeMeasurementComplete : function(garmentName, customer = orders.selectedCustomer()) {

            console.log("checking for", garmentName);

            if( !['jacket', 'pants', 'shirt', 'vest' ].includes(garmentName.toLowerCase().trim()) )
            {
                console.log("Garment type not allowed: ", garmentName);
                return false;
            }


            let measurments = null;

            try 
            {
                // Gets the measurments for that garment name
                measurments = customer.measurementsStep.garments.filter( e => e.name().toLowerCase().trim() === garmentName.toLowerCase().trim() )[0].measurements();
            }
            catch(err)
            {
                console.log(`error checking for ${garmentName}`, err);
                return true;
            }

            let isComplete = true;

            for(let meas of measurments)
            {   
                // If measurment is mandatory and its value is empty => mark as imcomplete
                if(meas.mandatory() && ( meas.value() === "" || parseFloat(meas.value()) == 0.0) )
                {
                    isComplete = false;
                    break;
                }
            }

            return isComplete;
        },

        /**
         * For UI purposes, Check if all garments for on customer is complete
         * @returns {Boolean}
         */
        isAllGarmentTypeMeasurementComplete(customer) {

            if (!customer.is_present) { return true };

            for (const garmentsTypes of customer.garmentsStep.garments()) {
                for (const garment of garmentsTypes.garments()) {

                    if (garment.garment_name.toLowerCase().trim() == 'suit') {
                        if (!this.isGarmentTypeMeasurementComplete('jacket', customer = customer) || !this.isGarmentTypeMeasurementComplete('pants', customer = customer)){
                            return false;
                        }
                    } else {
                        if (!this.isGarmentTypeMeasurementComplete(garment.garment_name, customer = customer)) {
                            return false;
                        }
                    }
                }
            }

            return true;
        }


    });
});