define(['jquery', 'knockout', 'base'], function($, ko) {
    
    /**
     * This class will receive the customers ids that we will use for the
     * order process.
     */
    UnlockCustomersFittingsVM = SimpleControl.extend({

        init: function() {
            console.log("Init UnlockCustomersFittingsVM")
            var self = this;
            
            // Used in the customer search
            this.custSelectVM = new CustomerSelectClass(dsRegistry);
            this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
            this.custSelectVM.setParentVM(this);

            // Stored the customers found by the search
            this.selectedCustomer = ko.observable(false);
            this.customerOrders = ko.observableArray([]);
            this.selectedOrder = ko.observable(false);

            this.selectedCustomer.subscribe(this.selectCustomerSubscription);

        },

        addCustomerFromSearchBar : function(customer) {

            console.log("appending client to list.", customer);
            this.selectedCustomer(customer);
            this.selectedOrder(false);
            this.custSelectVM.customerAutocomplete("");
        },

        selectCustomerSubscription : function(selectedCustomer) {

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'alterations_pos/get_customers_orders_data',
                dataType: 'json',
                data: { 
                    "user" : authCtrl.userInfo,
                    "customer_id" : selectedCustomer.server_id
                },
                success: function (ret) {
                    
                    console.log(ret);

                    if(ret.result === 'success')
                    {
                        unlockCustomersFittings.customerOrders(ret.orders);

                        if(ret.orders.length === 0)
                        {
                            customAlert("Customer has no orders !"); 
                        }
                        else // sets the fitting status as observable in order to change later !
                        {
                            for(let order of ret.orders)
                            {
                                for(let garment of order.garments)
                                {
                                    if( !garment.fitting_status )
                                    {
                                        garment.fitting_status = { 'status' : ko.observable('NO STATUS') }
                                    }
                                    else
                                    {
                                        garment.fitting_status.status = ko.observable(garment.fitting_status.status);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        alert("Something wrong... :( please try again");
                    }
               
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later"); 
                    //posChangePage('#main');
                },
                async: true
            });

        },


        makeGarmentAvailable : function(garment) {

            const garment_id = garment.id;
            const garment_type = garment.type;

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'alterations_pos/set_new_garment_fitting_status',
                dataType: 'json',
                data: { 
                    "user" : authCtrl.userInfo,
                    "garment_id" : garment_id,
                    "garment_type" : garment_type
                },
                success: function (ret) {
                    
                    console.log("makeGarmentAvailable ret", ret);

                    if(ret.result)
                    {
                        $.jGrowl(`Garment Available in fitting list !!!`); 
                        garment.fitting_status.status('available');            
                    }
                    else
                    {
                        alert("Something wrong... :( please try again");
                    }
               
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later"); 
                    //posChangePage('#main');
                },
                async: true
            });

        }





      

        
    });
});