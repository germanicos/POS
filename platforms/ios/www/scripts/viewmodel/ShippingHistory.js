define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingHistory = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var self = this;
		this.boxes = ko.observableArray();
		this.selectedBox  = ko.observable({});
		this.loadSelectedBox = function(index){
			orderItem.shippingBoxVM = new ShippingBox(orderItem.dsRegistry);
			orderItem.shippingBoxDS = new defShippingBox('shippingBoxDS', orderItem.dsRegistry, this.boxes()[index] );
			orderItem.shippingBoxVM.subscribeTo('shippingBoxDS');
console.log("orderItem.shippingBoxDS.getStore(): " + JSON.stringify( orderItem.shippingBoxDS.getStore() ) );
 			orderItem.shippingBoxVM.addVariantsShippingBox();
			posChangePage('#shipping_box');
		};
	},
	
	digestData: function(data) {
		var newObserve = [];				
		for (var ind in data) {
			if(data[ind] != null){
				if (!data[ind].isDead){ 
					newObserve.push(data[ind]);
					newObserve[ind].checked = false;	
				}	
			}	
		}
		this.boxes(newObserve);
console.log("boxes: " + JSON.stringify(this.boxes()));		
	},
});  



//END DEFINE CLOSURE
});