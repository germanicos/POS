define(['jquery', 'knockout', 'base'], function ($, ko) {
    ShippingMissingItemsVM = Class.extend({
        init: function () {
            self = this;
            this.missingItems = ko.observableArray();
            this.salesmen = ko.observableArray();
            this.local = ko.observableArray();

            this.requestData();

            this.popUp = ko.observable(null);
            this.showPopUp = ko.observable(false);

        },

        /**
         * @description Request necessary data from ERP
         */
        requestData: function () {
            console.log("requesting data for missing items ...");

            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/get_missing_items",
                dataType: 'json',
                data: { "user": authCtrl.userInfo },
                async: false,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    // handle items
                    for (item of ret.boxes) {
                        item.sorted = ko.observable(false); // flag for "found" and "not found"
                        self.missingItems.push(item);
                    }

                    // handle salesmen
                    for (salesman of ret.salesmans) {
                        salesman.fullName = salesman.first_name + " " + salesman.last_name;
                        self.salesmen.push(salesman);
                    }

                    // handle location
                    for (local of ret.locations) {
                        local.fullAddress = local.address + " - " + local.name;
                        self.local.push(local);
                    }
                    // It is possible set a new location
                    self.local.push({
                        id: "0",
                        name: "",
                        address: "",
                        fullAddress: "Other"
                    })

                },


                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main_new");


                }

            });
        },

        /** Post to ERP sending new status of a missing item. If connection is successful, update UI
         * @param {Number} index
         * @param {Number} selectedAddress_id
         * @param {Number} selectedSalesman_id
         * @param {String} explanation
         * @param {String} other
        */
        toggleSortItem: function (index, selectedAddress_id, selectedSalesman_id, explanation, other) {
            self.sendUpdatesToERP(index, self.missingItems()[index].id, selectedAddress_id, selectedSalesman_id, explanation, other);
        },

        /**
         * 
         * @param {number} index 
         * @param {number} item_id 
         * @param {number} selectedAddress_id 
         * @param {number} selectedSalesman_id 
         * @param {string} explanation 
         * @param {string} other 
         */
        sendUpdatesToERP(index, item_id, selectedAddress_id, selectedSalesman_id, explanation, other) {
            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/sort_missing_item",
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "item_id": item_id,
                    "address_id": selectedAddress_id,
                    "other_address": other,
                    "salesman_id": selectedSalesman_id,
                    "explanation": explanation
                },
                async: false,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    console.log("debug!!!");

                    // update UI
                    self.missingItems()[index].sorted(!self.missingItems()[index].sorted())


                },


                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    console.log("Something went wrong, please try again.");


                }

            });
        },

        openSortItemPopUp: function (itemIndex) {
            self.popUp(new self.PopUp(itemIndex, self));
        },

        closeSortItemPopUp: function () {
            self.showPopUp(false);
            self.popUp(null);
        },

        // ██████╗  ██████╗ ██████╗       ██╗   ██╗██████╗ 
        // ██╔══██╗██╔═══██╗██╔══██╗      ██║   ██║██╔══██╗
        // ██████╔╝██║   ██║██████╔╝█████╗██║   ██║██████╔╝
        // ██╔═══╝ ██║   ██║██╔═══╝ ╚════╝██║   ██║██╔═══╝ 
        // ██║     ╚██████╔╝██║           ╚██████╔╝██║     
        // ╚═╝      ╚═════╝ ╚═╝            ╚═════╝ ╚═╝     
        //                                                 

        /** auxiliar object to handle popup */
        PopUp: class {
            constructor(itemIndex, VMContext) {
                this.itemIndex = itemIndex;

                this.VMContext = VMContext.showPopUp(true);

                // PopUp elements
                this.address = VMContext.local;
                this.selectedAddress = ko.observable(null);
                this.other = ko.observable("");

                this.salesman = VMContext.salesmen;
                this.selectedSalesman = ko.observable("");

                this.explanation = ko.observable("");
            }

            closePopUp() {
                // this.VMContext.showPopUp(false);
                this.VMContext.closeSortItemPopUp();
            }

            submitButton() {
                this.VMContext.toggleSortItem(this.itemIndex, this.selectedAddress().id, this.selectedSalesman().user_id, this.explanation(), this.other());
                this.closePopUp();
            }

        }
    });
});