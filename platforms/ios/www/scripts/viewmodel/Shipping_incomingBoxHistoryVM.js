define(['jquery', 'knockout', 'base'], function ($, ko) {
    Shipping_incomingBoxHistoryVM = Class.extend({
        init: function () {
            self = this;
            this.boxes = ko.observableArray();
            this.boxesToShow = ko.observableArray();
            this.historyChat = ko.observable(null);
            this.requestData();
        },

        /**
         * @description Request necessary data from ERP
         */
        requestData: function () {
            console.log("requesting data for incoming box history...");

            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/get_box_history",
                dataType: 'json',
                data: { "user": authCtrl.userInfo, "type": "incoming" },
                async: false,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    for (box of ret) {
                        box.showItemTable = ko.observable(false); // var to show/hide table in view
                        self.boxes.push(box);
                    }

                    self.boxesToShow(self.boxes());
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main_new");


                }

            });
        },
        
        openHistoryChat: function (boxIndex, itemIndex) {
            self.historyChat({
                item_name: self.boxes()[boxIndex].items[itemIndex].garment_name,
                item_chat: self.boxes()[boxIndex].items[itemIndex].chats,
                client_name: self.boxes()[boxIndex].items[itemIndex].customer_name
            });
        },

        closeHistoryChat: function () {
            self.historyChat(null);
        },

        applyFilterRange: function() {
            
            var from = document.getElementById('from').value;
            var until = document.getElementById('until').value;
            
            var fromDate = new Date (from);
            var untilDate = new Date (until);

            self.boxesToShow([]);

            for (box of self.boxes()){
                currentDatePreFormated = box.received_date.split('-');
                var currentDate = new Date(currentDatePreFormated[2],currentDatePreFormated[1]-1, currentDatePreFormated[0],0,0,0,0);
                
                if ( currentDate >= fromDate && currentDate <= untilDate){
                    self.boxesToShow.push(box);
                }
            }

        },

        clearFilterRange: function() {
          self.boxesToShow(self.boxes());

        }

    });
});