define(['jquery', 'knockout', 'base'], function ($, ko) {
	Measurements = SimpleControl.extend({
		init: function (DsRegistry, oI) {//, fromcached) {

			this.fitlinesJacket = ko.observableArray(dsRegistry.getDatasource('fitlinesJacketDS').getStore());
			this.fitlinesPants = ko.observableArray(dsRegistry.getDatasource('fitlinesPantsDS').getStore());

			var self = this;
			this._super(DsRegistry);
			this.orderItemMirror = oI;

















			//	if(fromcached == true){
			this.pantWaist = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantWaist);					//= ko.observable('');		this.pantWaist.subscribe(function() { self.flushModel(); });
			this.knee = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].knee);					//= ko.observable('');		this.pantWaist.subscribe(function() { self.flushModel(); });
			this.calf = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].calf);					//= ko.observable('');		this.pantWaist.subscribe(function() { self.flushModel(); });
			this.pantHips = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantHips);					//= ko.observable('');		this.pantHips.subscribe(function() { self.flushModel(); });
			this.thigh = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].thigh);						//= ko.observable('');		this.thigh.subscribe(function() { self.flushModel(); });
			this.cuffs = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].cuffs);						//= ko.observable('');		this.cuffs.subscribe(function() { self.flushModel(); });		
			this.pantLength = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantLength);					//= ko.observable('');		this.pantLength.subscribe(function() { self.flushModel(); });
			this.crotch = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].crotch);						//= ko.observable('');		this.crotch.subscribe(function() { self.flushModel(); });		
			this.zipper = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].zipper);						//= ko.observable('');		this.zipper.subscribe(function() { self.flushModel(); });
			this.pantFrontCrotch = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantFrontCrotch);				//= ko.observable('');		this.pantFrontCrotch.subscribe(function() { self.flushModel(); });
			this.pantInseam = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantInseam);					//= ko.observable('');		this.pantInseam.subscribe(function() { self.flushModel(); });
			this.pantAngledWaist = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantAngledWaist);					//= ko.observable('');		this.pantAngledWaist.subscribe(function() { self.flushModel(); });
			this.pantAngledHems = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantAngledHems);					//= ko.observable('');		this.pantAngledWaist.subscribe(function() { self.flushModel(); });
			this.pantWaistAngle = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantWaistAngle);					//= ko.observable('');		this.pantWaistAngle.subscribe(function() { self.flushModel(); });

			this.jacketChest = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketChest);					//= ko.observable('');		this.jacketChest.subscribe(function() { self.flushModel(); });
			this.jacketStomach = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketStomach);				//= ko.observable('');		this.jacketStomach.subscribe(function() { self.flushModel(); });
			this.jacketHips = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketHips);					//= ko.observable('');		this.jacketHips.subscribe(function() { self.flushModel(); });
			this.jacketFront = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketFront);					//= ko.observable('');		this.jacketFront.subscribe(function() { self.flushModel(); });
			this.jacketBack = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketBack);					//= ko.observable('');		this.jacketBack.subscribe(function() { self.flushModel(); });
			this.jacketfullShoulder = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketfullShoulder);			//= ko.observable('');		this.jacketfullShoulder.subscribe(function() { self.flushModel(); });
			this.jacketsleeveL = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketsleeveL);				//= ko.observable('');		this.jacketsleeveL.subscribe(function() { self.flushModel(); });
			this.jacketsleeveR = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketsleeveR);				//= ko.observable('');		this.jacketsleeveR.subscribe(function() { self.flushModel(); });
			this.jacketNeck = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketNeck);					//= ko.observable('');		this.jacketNeck.subscribe(function() { self.flushModel(); });
			this.jacketBicep = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketBicep);					//= ko.observable('');		this.jacketBicep.subscribe(function() { self.flushModel(); });
			this.jacketWrist = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketWrist);					//= ko.observable('');		this.jacketWrist.subscribe(function() { self.flushModel(); });
			//			this.jacketWristWatch			= ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketWristWatch);			//= ko.observable('');		this.jacketWristWatch.subscribe(function() { self.flushModel(); });
			this.jacketforeArms = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketforeArms);				//= ko.observable('');		this.jacketforeArms.subscribe(function() { self.flushModel(); });
			this.jacketLength = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketLength);				//= ko.observable('');		this.jacketLength.subscribe(function() { self.flushModel(); });
			this.jacketLapel = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketLapel);					//= ko.observable('');		this.jacketLapel.subscribe(function() { self.flushModel(); });
			this.jacketZeropoint = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketZeropoint);				//= ko.observable('');		this.jacketZeropoint.subscribe(function() { self.flushModel(); });
			this.jacketZeropointfront = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketZeropointfront);			//= ko.observable('');		this.jacketZeropointfront.subscribe(function() { self.flushModel(); });
			this.jacketZeropointback = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketZeropointback);			//= ko.observable('');		this.jacketZeropointback.subscribe(function() { self.flushModel(); });
			this.jacketRightFrontToZero = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketRightFrontToZero);			//= ko.observable('');		this.jacketZeropointfront.subscribe(function() { self.flushModel(); });
			this.jacketRightBackToZero = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketRightBackToZero);			//= ko.observable('');		this.jacketZeropointback.subscribe(function() { self.flushModel(); });
			this.jacketTenseBicep = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketTenseBicep);			//= ko.observable('');		this.jacketZeropointback.subscribe(function() { self.flushModel(); });
			//			this.jacketHalfzero				= ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketHalfzero);				//= ko.observable('');		this.jacketHalfzero.subscribe(function() { self.flushModel(); });
			//			this.jacketHalfstomach			= ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketHalfstomach);			//= ko.observable('');		this.jacketHalfstomach.subscribe(function() { self.flushModel(); });		

			this.shirtChest = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtChest);					//= ko.observable('');		this.shirtChest.subscribe(function() { self.flushModel(); });
			this.shirtStomach = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtStomach);				//= ko.observable('');		this.shirtStomach.subscribe(function() { self.flushModel(); });
			this.shirtfullshoulder = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtfullshoulder);			//= ko.observable('');		this.shirtfullshoulder.subscribe(function() { self.flushModel(); });
			this.shirtsleeveL = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtsleeveL);				//= ko.observable('');		this.shirtsleeveL.subscribe(function() { self.flushModel(); });
			this.shirtsleeveR = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtsleeveR);				//= ko.observable('');		this.shirtsleeveR.subscribe(function() { self.flushModel(); });
			this.shirtNeck = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtNeck);					//= ko.observable('');		this.shirtNeck.subscribe(function() { self.flushModel(); });
			this.shirtFront = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtFront);					//= ko.observable('');		this.shirtFront.subscribe(function() { self.flushModel(); });
			this.shirtBack = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtBack);					//= ko.observable('');		this.shirtBack.subscribe(function() { self.flushModel(); });
			this.shirtLength = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtLength);					//= ko.observable('');		this.shirtLength.subscribe(function() { self.flushModel(); });
			this.shirtBicep = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtBicep);					//= ko.observable('');		this.shirtBicep.subscribe(function() { self.flushModel(); });
			this.shirtHips = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtHips);					//= ko.observable('');		this.shirtHips.subscribe(function() { self.flushModel(); });
			this.shirtWrist = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtWrist);					//= ko.observable('');		this.shirtWrist.subscribe(function() { self.flushModel(); });
			this.shirtZeroPoint = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtZeroPoint);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });
			this.shirtFrontToZero = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtFrontToZero);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });
			this.shirtBackToZero = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtBackToZero);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel();
			this.shirtRightFrontToZero = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtRightFrontToZero);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });
			this.shirtRightBackToZero = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtRightBackToZero);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });
			this.shirtTenseBicep = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtTenseBicep);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });
			this.shirtForearm = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtForearm);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });
			this.shirtWristwatch = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtWristwatch);				//= ko.observable('');		this.shirtWristwatch.subscribe(function() { self.flushModel(); });

			this.vestHips = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestHips);					//= ko.observable('');		this.vestHips.subscribe(function() { self.flushModel(); });
			this.vestLength = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestLength);					//= ko.observable('');		this.vestLength.subscribe(function() { self.flushModel(); });		
			this.vestChest = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestChest);					//= ko.observable('');		this.vestChest.subscribe(function() { self.flushModel(); });
			this.vestStomach = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestStomach);					//= ko.observable('');		this.vestStomach.subscribe(function() { self.flushModel(); });
			this.vestNeck = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestNeck);					//= ko.observable('');		this.vestNeck.subscribe(function() { self.flushModel(); });
			this.vestHalfShoulderWidth = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestHalfShoulderWidth);		//= ko.observable('');		this.vestHalfShoulderLength.subscribe(function() { self.flushModel(); });
			this.vestfullShoulder = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestfullShoulder);		    //= ko.observable('');		this.vestfullShoulder.subscribe(function() { self.flushModel(); });
			this.vestVLength = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestVLength);					//= ko.observable('');		this.vestVLength.subscribe(function() { self.flushModel(); });

			this.suitComments = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].suitComments);				//= ko.observable('');		this.suitComments.subscribe(function() { self.flushModel(); });
			this.jacketComments = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketComments);				//= ko.observable('');		this.jacketComments.subscribe(function() { self.flushModel(); });
			this.pantsComments = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].pantsComments);				//= ko.observable('');		this.pantsComments.subscribe(function() { self.flushModel(); });
			this.shirtComments = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtComments);				//= ko.observable('');		this.shirtComments.subscribe(function() { self.flushModel(); });
			this.vestComments = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].vestComments);				//= ko.observable('');		this.vestComments.subscribe(function() { self.flushModel(); });
			this.overcoatComments = ko.observable(dsRegistry.getDatasource('measurementsDS').getStore()[0].overcoatComments);			//= ko.observable('');		this.overcoatComments.subscribe(function() { self.flushModel(); });

			this.jacketDate = ko.computed(function () {
				timestamp = dsRegistry.getDatasource('measurementsDS').getStore()[0].jacketDate;
				if (timestamp != '0.00') {
					var a = new Date(timestamp * 1000);
					var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
					var year = a.getFullYear();
					var month = months[a.getMonth()];
					var date = a.getDate();
					var hour = a.getHours();
					var min = a.getMinutes();
					var sec = a.getSeconds();
					var time = date + ' ' + month + ' ' + year + ', ' + hour + ':' + min + ':' + sec;
					return time;
				} else {
					return 'no date available';
				}
			}, this);

			this.pantsDate = ko.computed(function () {
				timestamp = dsRegistry.getDatasource('measurementsDS').getStore()[0].pantsDate;
				if (timestamp != '0.00') {
					var a = new Date(timestamp * 1000);
					var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
					var year = a.getFullYear();
					var month = months[a.getMonth()];
					var date = a.getDate();
					var hour = a.getHours();
					var min = a.getMinutes();
					var sec = a.getSeconds();
					var time = date + ' ' + month + ' ' + year + ', ' + hour + ':' + min + ':' + sec;
					return time;
				} else {
					return 'no date available';
				}
			}, this);

			this.shirtDate = ko.computed(function () {
				timestamp = dsRegistry.getDatasource('measurementsDS').getStore()[0].shirtDate;
				if (timestamp != '0.00') {
					var a = new Date(timestamp * 1000);
					var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
					var year = a.getFullYear();
					var month = months[a.getMonth()];
					var date = a.getDate();
					var hour = a.getHours();
					var min = a.getMinutes();
					var sec = a.getSeconds();
					var time = date + ' ' + month + ' ' + year + ', ' + hour + ':' + min + ':' + sec;
					return time;
				} else {
					return 'no date available';
				}
			}, this);

			this.vestDate = ko.computed(function () {
				timestamp = dsRegistry.getDatasource('measurementsDS').getStore()[0].vestDate;
				if (timestamp != '0.00') {
					var a = new Date(timestamp * 1000);
					var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
					var year = a.getFullYear();
					var month = months[a.getMonth()];
					var date = a.getDate();
					var hour = a.getHours();
					var min = a.getMinutes();
					var sec = a.getSeconds();
					var time = date + ' ' + month + ' ' + year + ', ' + hour + ':' + min + ':' + sec;
					return time;
				} else {
					return 'no date available';
				}
			}, this);



			if (this.pantWaist() == "0.00") { this.pantWaist(""); }
			if (this.knee() == "0.00") { this.knee(""); }
			if (this.calf() == "0.00") { this.calf(""); }
			if (this.pantHips() == "0.00") { this.pantHips(""); }
			if (this.thigh() == "0.00") { this.thigh(""); }
			if (this.cuffs() == "0.00") { this.cuffs(""); }
			if (this.pantLength() == "0.00") { this.pantLength(""); }
			if (this.crotch() == "0.00") { this.crotch(""); }
			if (this.zipper() == "0.00") { this.zipper(""); }
			if (this.pantFrontCrotch() == "0.00") { this.pantFrontCrotch(""); }
			if (this.pantInseam() == "0.00") { this.pantInseam(""); }
			if (this.pantAngledWaist() == "0.00" || this.pantAngledWaist() == undefined || this.pantAngledWaist() == null || this.pantAngledWaist() == "false") { this.pantAngledWaist(false); }
			if (this.pantAngledHems() == "0.00" || this.pantAngledHems() == undefined || this.pantAngledHems() == null || this.pantAngledHems() == "false") { this.pantAngledHems(false); }
			if (this.pantAngledWaist() == "true") { this.pantAngledWaist(true); }
			if (this.pantAngledHems() == "true") { this.pantAngledHems(true); }
			if (this.pantWaistAngle() == "0.00" || this.pantWaistAngle() == undefined || this.pantWaistAngle() == null) { this.pantWaistAngle('Standard'); }

			if (this.jacketChest() == "0.00") { this.jacketChest(""); }
			if (this.jacketStomach() == "0.00") { this.jacketStomach(""); }
			if (this.jacketHips() == "0.00") { this.jacketHips(""); }
			if (this.jacketFront() == "0.00") { this.jacketFront(""); }
			if (this.jacketBack() == "0.00") { this.jacketBack(""); }
			if (this.jacketfullShoulder() == "0.00") { this.jacketfullShoulder(""); }
			if (this.jacketsleeveL() == "0.00") { this.jacketsleeveL(""); }
			if (this.jacketsleeveR() == "0.00") { this.jacketsleeveR(""); }
			if (this.jacketNeck() == "0.00") { this.jacketNeck(""); }
			if (this.jacketBicep() == "0.00") { this.jacketBicep(""); }
			if (this.jacketWrist() == "0.00") { this.jacketWrist(""); }
			//			this.jacketWristWatch	
			if (this.jacketforeArms() == "0.00") { this.jacketforeArms(""); }
			if (this.jacketLength() == "0.00") { this.jacketLength(""); }
			if (this.jacketLapel() == "0.00") { this.jacketLapel(""); }
			if (this.jacketZeropoint() == "0.00") { this.jacketZeropoint(""); }
			if (this.jacketZeropointfront() == "0.00") { this.jacketZeropointfront(""); }
			if (this.jacketZeropointback() == "0.00") { this.jacketZeropointback(""); }
			if (this.jacketRightFrontToZero() == "0.00") { this.jacketRightFrontToZero(""); }
			if (this.jacketRightBackToZero() == "0.00") { this.jacketRightBackToZero(""); }
			if (this.jacketTenseBicep() == "0.00") { this.jacketTenseBicep(""); }
			//			if( this.jacketHalfzero			
			//			if( this.jacketHalfstomach				
			if (this.shirtChest() == "0.00") { this.shirtChest(""); }
			if (this.shirtStomach() == "0.00") { this.shirtStomach(""); }
			if (this.shirtfullshoulder() == "0.00") { this.shirtfullshoulder(""); }
			if (this.shirtsleeveL() == "0.00") { this.shirtsleeveL(""); }
			if (this.shirtsleeveR() == "0.00") { this.shirtsleeveR(""); }
			if (this.shirtNeck() == "0.00") { this.shirtNeck(""); }
			if (this.shirtFront() == "0.00") { this.shirtFront(""); }
			if (this.shirtBack() == "0.00") { this.shirtBack(""); }
			if (this.shirtLength() == "0.00") { this.shirtLength(""); }
			if (this.shirtBicep() == "0.00") { this.shirtBicep(""); }
			if (this.shirtHips() == "0.00") { this.shirtHips(""); }
			if (this.shirtWrist() == "0.00") { this.shirtWrist(""); }
			if (this.shirtWristwatch() == "0.00") { this.shirtWristwatch(""); }
			if (this.shirtZeroPoint() == "0.00") { this.shirtZeroPoint(""); }
			if (this.shirtFrontToZero() == "0.00") { this.shirtFrontToZero(""); }
			if (this.shirtBackToZero() == "0.00") { this.shirtBackToZero(""); }
			if (this.shirtRightFrontToZero() == "0.00") { this.shirtRightFrontToZero(""); }
			if (this.shirtRightBackToZero() == "0.00") { this.shirtRightBackToZero(""); }
			if (this.shirtTenseBicep() == "0.00") { this.shirtTenseBicep(""); }
			if (this.shirtForearm() == "0.00") { this.shirtForearm(""); }

			if (this.vestHips() == "0.00") { this.vestHips(""); }
			if (this.vestLength() == "0.00") { this.vestLength(""); }
			if (this.vestChest() == "0.00") { this.vestChest(""); }
			if (this.vestStomach() == "0.00") { this.vestStomach(""); }
			if (this.vestNeck() == "0.00") { this.vestNeck(""); }
			if (this.vestHalfShoulderWidth() == "0.00") { this.vestHalfShoulderWidth(""); }
			if (this.vestfullShoulder() == "0.00") { this.vestfullShoulder(""); }
			if (this.vestVLength() == "0.00") { this.vestVLength(""); }


			this.measurementsORfitlineJacket = ko.observable('measurements');
			this.measurementsORfitlinePants = ko.observable('measurements');

			this.selectedGarment = ko.observable({ name: "none" });
			this.fitlinesJacket = ko.observableArray(dsRegistry.getDatasource('fitlinesJacketDS').getStore());
			this.fitlinesPants = ko.observableArray(dsRegistry.getDatasource('fitlinesPantsDS').getStore());

			this.fitlineJacketVM = new FitlinesJacket(dsRegistry, this, 8);
			this.fitlinePantsVM = new FitlinesPants(dsRegistry, this, 0);
			
			
			ko.bindingHandlers.selectedIndexJacket = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							tableindex = element.selectedIndex - 1; // USE -1 IF optionsCaption: 'Fitline' IS USED

							if (tableindex >= 0) {
								/*
								for(x = 0; x < document.getElementsByName('readonlyinput').length; x++){
									document.getElementsByName('readonlyinput')[x].style.display = "block";
								}
								*/
								//this.fitlineJacketVM = new FitlinesJacket(dsRegistry, this, tableindex);
								self.fitlineJacketVM.update(tableindex);
								console.log("this.fitlineJacketVM.jacketName(): " + this.fitlineJacketVM.jacketName());
								/*document.getElementsByName('jacketName')[0].value = this.fitlineJacketVM.jacketName();
								document.getElementsByName('jacketId')[0].value = this.fitlineJacketVM.jacketId();
								document.getElementsByName('jacketAcrossBack')[0].value = this.fitlineJacketVM.jacketAcrossBack();//	document.getElementsByName('jacketAcrossBack')[0].onchange;
								document.getElementsByName('jacketAcrossShoulder')[0].value = this.fitlineJacketVM.jacketAcrossShoulder();
								document.getElementsByName('jacketBackPanelAtXBack')[0].value = this.fitlineJacketVM.jacketBackPanelAtXBack();
								document.getElementsByName('jacketBackPanelBottomEdge')[0].value = this.fitlineJacketVM.jacketBackPanelBottomEdge();
								document.getElementsByName('jacketBackPanelChest')[0].value = this.fitlineJacketVM.jacketBackPanelChest();
								document.getElementsByName('jacketBackPanelHip')[0].value = this.fitlineJacketVM.jacketBackPanelHip();
								document.getElementsByName('jacketBackPanelTrueWaist')[0].value = this.fitlineJacketVM.jacketBackPanelTrueWaist();
								document.getElementsByName('jacketFitlineBicep')[0].value = this.fitlineJacketVM.jacketFitlineBicep();
								document.getElementsByName('jacketCenterBackLength')[0].value = this.fitlineJacketVM.jacketCenterBackLength();
								document.getElementsByName('jacketFitlineComments')[0].value = this.fitlineJacketVM.jacketFitlineComments();
								document.getElementsByName('jacketElbow')[0].value = this.fitlineJacketVM.jacketElbow();
								document.getElementsByName('jacketFrontLength')[0].value = this.fitlineJacketVM.jacketFrontLength();
								document.getElementsByName('jacketFrontPanelChest')[0].value = this.fitlineJacketVM.jacketFrontPanelChest();
								document.getElementsByName('jacketFrontPanelHip')[0].value = this.fitlineJacketVM.jacketFrontPanelHip();
								document.getElementsByName('jacketFrontPanelTrueWaist')[0].value = this.fitlineJacketVM.jacketFrontPanelTrueWaist();
								document.getElementsByName('jacketFrontSideSeamLength')[0].value = this.fitlineJacketVM.jacketFrontSideSeamLength();
								document.getElementsByName('jacketLapelWidthAtChest')[0].value = this.fitlineJacketVM.jacketLapelWidthAtChest();
								document.getElementsByName('jacketShoulderSeam')[0].value = this.fitlineJacketVM.jacketShoulderSeam();
								document.getElementsByName('jacketSidePanelBottomEdge')[0].value = this.fitlineJacketVM.jacketSidePanelBottomEdge();
								document.getElementsByName('jacketSidePanelChest')[0].value = this.fitlineJacketVM.jacketSidePanelChest();
								document.getElementsByName('jacketSidePanelHip')[0].value = this.fitlineJacketVM.jacketSidePanelHip();
								document.getElementsByName('jacketSidePanelTrueWaist')[0].value = this.fitlineJacketVM.jacketSidePanelTrueWaist();
								document.getElementsByName('jacketSleeveCuffButtoned')[0].value = this.fitlineJacketVM.jacketSleeveCuffButtoned();
								document.getElementsByName('jacketSleeveCurve')[0].value = this.fitlineJacketVM.jacketSleeveCurve();
								document.getElementsByName('jacketSleeveCurveFront')[0].value = this.fitlineJacketVM.jacketSleeveCurveFront();
								document.getElementsByName('jacketSleeveCurveSide')[0].value = this.fitlineJacketVM.jacketSleeveCurveSide();
								document.getElementsByName('jacketSleeveLengthLeft')[0].value = this.fitlineJacketVM.jacketSleeveLengthLeft();
								document.getElementsByName('jacketSleeveLengthRight')[0].value = this.fitlineJacketVM.jacketSleeveLengthRight();
							
								document.getElementsByName('jacketChestRule')[0].value = this.fitlineJacketVM.jacketChestRule(); 
								//document.getElementById('jacketChestRule').innerHTML = this.fitlineJacketVM.jacketChestRule();
								document.getElementsByName('jacketStomachRule')[0].value = this.fitlineJacketVM.jacketStomachRule();
								document.getElementsByName('jacketJacketLengthRule')[0].value = this.fitlineJacketVM.jacketJacketLengthRule();
								document.getElementsByName('jacketFullShoulderRule')[0].value = this.fitlineJacketVM.jacketFullShoulderRule();
								document.getElementsByName('jacketHipsRule')[0].value = this.fitlineJacketVM.jacketHipsRule();
								document.getElementsByName('jacketFrontRule')[0].value = this.fitlineJacketVM.jacketFrontRule();
								document.getElementsByName('jacketBackRule')[0].value = this.fitlineJacketVM.jacketBackRule();
								document.getElementsByName('jacketNeckRule')[0].value = this.fitlineJacketVM.jacketNeckRule();
								document.getElementsByName('jacketBicepRule')[0].value = this.fitlineJacketVM.jacketBicepRule();
								document.getElementsByName('jacketLeftSleeveRule')[0].value = this.fitlineJacketVM.jacketLeftSleeveRule();
								document.getElementsByName('jacketRightSleeveRule')[0].value = this.fitlineJacketVM.jacketRightSleeveRule();
								document.getElementsByName('jacketWristRule')[0].value = this.fitlineJacketVM.jacketWristRule();
								document.getElementsByName('jacketLapelLengthRule')[0].value = this.fitlineJacketVM.jacketLapelLengthRule();
								document.getElementsByName('jacketForearmRule')[0].value = this.fitlineJacketVM.jacketForearmRule();
								document.getElementsByName('jacketZeroPointRule')[0].value = this.fitlineJacketVM.jacketZeroPointRule();
								document.getElementsByName('jacketFrontSZeroRule')[0].value = this.fitlineJacketVM.jacketFrontSZeroRule();
								document.getElementsByName('jacketSBackSZeroRule')[0].value = this.fitlineJacketVM.jacketSBackSZeroRule();               		
								console.log("this.fitlineJacketVM.jacketChestRule(): " + this.fitlineJacketVM.jacketChestRule());               		
						  	
										if ("createEvent" in document) {
									var evt = document.createEvent("HTMLEvents");
									evt.initEvent("change", false, true);
									
									document.getElementsByName('jacketName')[0].dispatchEvent(evt);
									document.getElementsByName('jacketId')[0].dispatchEvent(evt);
									document.getElementsByName('jacketAcrossBack')[0].dispatchEvent(evt);
									document.getElementsByName('jacketAcrossShoulder')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBackPanelAtXBack')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBackPanelBottomEdge')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBackPanelChest')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBackPanelHip')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBackPanelTrueWaist')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFitlineBicep')[0].dispatchEvent(evt);
									document.getElementsByName('jacketCenterBackLength')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFitlineComments')[0].dispatchEvent(evt);
									document.getElementsByName('jacketElbow')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontLength')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontPanelChest')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontPanelHip')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontPanelTrueWaist')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontSideSeamLength')[0].dispatchEvent(evt);
									document.getElementsByName('jacketLapelWidthAtChest')[0].dispatchEvent(evt);
									document.getElementsByName('jacketShoulderSeam')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSidePanelBottomEdge')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSidePanelChest')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSidePanelHip')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSidePanelTrueWaist')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSleeveCuffButtoned')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSleeveCurve')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSleeveCurveFront')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSleeveCurveSide')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSleeveLengthLeft')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSleeveLengthRight')[0].dispatchEvent(evt);
									
									
									document.getElementsByName('jacketChestRule')[0].dispatchEvent(evt);
								//	document.getElementById('jacketChestRule').dispatchEvent(evt);
									document.getElementsByName('jacketStomachRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketJacketLengthRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFullShoulderRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketHipsRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBackRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketNeckRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketBicepRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketLeftSleeveRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketRightSleeveRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketWristRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketLapelLengthRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketForearmRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketZeroPointRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketFrontSZeroRule')[0].dispatchEvent(evt);
									document.getElementsByName('jacketSBackSZeroRule')[0].dispatchEvent(evt);   								
									
								}else{
									
									document.getElementsByName('jacketName')[0].fireEvent("onchange");
									document.getElementsByName('jacketId')[0].fireEvent("onchange");						
									document.getElementsByName('jacketAcrossBack')[0].fireEvent("onchange");
									document.getElementsByName('jacketAcrossShoulder')[0].fireEvent("onchange");
									document.getElementsByName('jacketBackPanelAtXBack')[0].fireEvent("onchange");
									document.getElementsByName('jacketBackPanelBottomEdge')[0].fireEvent("onchange");
									document.getElementsByName('jacketBackPanelChest')[0].fireEvent("onchange");
									document.getElementsByName('jacketBackPanelHip')[0].fireEvent("onchange");
									document.getElementsByName('jacketBackPanelTrueWaist')[0].fireEvent("onchange");
									document.getElementsByName('jacketFitlineBicep')[0].fireEvent("onchange");
									document.getElementsByName('jacketCenterBackLength')[0].fireEvent("onchange");
									document.getElementsByName('jacketFitlineComments')[0].fireEvent("onchange");
									document.getElementsByName('jacketElbow')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontLength')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontPanelChest')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontPanelHip')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontPanelTrueWaist')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontSideSeamLength')[0].fireEvent("onchange");
									document.getElementsByName('jacketLapelWidthAtChest')[0].fireEvent("onchange");
									document.getElementsByName('jacketShoulderSeam')[0].fireEvent("onchange");
									document.getElementsByName('jacketSidePanelBottomEdge')[0].fireEvent("onchange");
									document.getElementsByName('jacketSidePanelChest')[0].fireEvent("onchange");
									document.getElementsByName('jacketSidePanelHip')[0].fireEvent("onchange");
									document.getElementsByName('jacketSidePanelTrueWaist')[0].fireEvent("onchange");
									document.getElementsByName('jacketSleeveCuffButtoned')[0].fireEvent("onchange");
									document.getElementsByName('jacketSleeveCurve')[0].fireEvent("onchange");
									document.getElementsByName('jacketSleeveCurveFront')[0].fireEvent("onchange");
									document.getElementsByName('jacketSleeveCurveSide')[0].fireEvent("onchange");
									document.getElementsByName('jacketSleeveLengthLeft')[0].fireEvent("onchange");
									document.getElementsByName('jacketSleeveLengthRight')[0].fireEvent("onchange");
									
									
									document.getElementsByName('jacketChestRule')[0].fireEvent("onchange");
								//	document.getElementById('jacketChestRule').fireEvent("onchange");
									document.getElementsByName('jacketStomachRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketJacketLengthRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketFullShoulderRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketHipsRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketBackRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketNeckRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketBicepRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketLeftSleeveRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketRightSleeveRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketWristRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketLapelLengthRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketForearmRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketZeroPointRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketFrontSZeroRule')[0].fireEvent("onchange");
									document.getElementsByName('jacketSBackSZeroRule')[0].fireEvent("onchange");   								
									
										}
								
						  	
						  	
							}else{
								/*
								for(x = 0; x < document.getElementsByName('readonlyinput').length; x++){
									document.getElementsByName('readonlyinput')[x].style.display = "none";
								}*/
							}
						}
					});
				}
			};


			ko.bindingHandlers.selectedIndexPants = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							tableindex = element.selectedIndex;// -1; // USE -1 IF optionsCaption: 'Fitline' IS USED -1;
							if (tableindex >= 0) {
								//this.fitlinePantsVM = new FitlinesPants(dsRegistry, this, tableindex);
								self.fitlinePantsVMfitlineJacketVM.update(tableindex);

								/*document.getElementsByName('pantsName')[0].value = this.fitlinePantsVM.pantsName();
								document.getElementsByName('pantsId')[0].value = this.fitlinePantsVM.pantsId();
	
								document.getElementsByName('pantsBackCrotch')[0].value = this.fitlinePantsVM.pantsBackCrotch();//	document.getElementsByName('jacketAcrossBack')[0].onchange;
								document.getElementsByName('pantsCuff')[0].value = this.fitlinePantsVM.pantsCuff();
								document.getElementsByName('pantsFrontCrotch')[0].value = this.fitlinePantsVM.pantsFrontCrotch();
								document.getElementsByName('pantsHip')[0].value = this.fitlinePantsVM.pantsHip();
								document.getElementsByName('pantsKnee')[0].value = this.fitlinePantsVM.pantsKnee();
								document.getElementsByName('pantsLength')[0].value = this.fitlinePantsVM.pantsLength();
								document.getElementsByName('pantsThigh')[0].value = this.fitlinePantsVM.pantsThigh();
								document.getElementsByName('pantsWaist')[0].value = this.fitlinePantsVM.pantsWaist();
								document.getElementsByName('pantsFitlineComments')[0].value = this.fitlinePantsVM.pantsFitlineComments();
						  	
										if("createEvent" in document) {
									var evt = document.createEvent("HTMLEvents");
									evt.initEvent("change", false, true);
									
									document.getElementsByName('pantsName')[0].dispatchEvent(evt);
									document.getElementsByName('pantsId')[0].dispatchEvent(evt);
									
									document.getElementsByName('pantsBackCrotch')[0].dispatchEvent(evt);
									document.getElementsByName('pantsCuff')[0].dispatchEvent(evt);
									document.getElementsByName('pantsFrontCrotch')[0].dispatchEvent(evt);
									document.getElementsByName('pantsHip')[0].dispatchEvent(evt);
									document.getElementsByName('pantsKnee')[0].dispatchEvent(evt);
									document.getElementsByName('pantsLength')[0].dispatchEvent(evt);
									document.getElementsByName('pantsThigh')[0].dispatchEvent(evt);
									document.getElementsByName('pantsWaist')[0].dispatchEvent(evt);
									document.getElementsByName('pantsFitlineComments')[0].dispatchEvent(evt);
								}else{
									
									document.getElementsByName('pantsName')[0].fireEvent("onchange");
									document.getElementsByName('pantsId')[0].fireEvent("onchange");
									
									document.getElementsByName('pantsBackCrotch')[0].fireEvent("onchange");
									document.getElementsByName('pantsCuff')[0].fireEvent("onchange");
									document.getElementsByName('pantsFrontCrotch')[0].fireEvent("onchange");
									document.getElementsByName('pantsHip')[0].fireEvent("onchange");
									document.getElementsByName('pantsKnee')[0].fireEvent("onchange");
									document.getElementsByName('pantsLength')[0].fireEvent("onchange");
									document.getElementsByName('pantsThigh')[0].fireEvent("onchange");
									document.getElementsByName('pantsWaist')[0].fireEvent("onchange");
									document.getElementsByName('pantsFitlineComments')[0].fireEvent("onchange");
									
										}*/



							}
						}
					});
				}
			};



			ko.bindingHandlers.modifiedJacketChest = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('vestChest')[0].value = element.value;
							document.getElementsByName('shirtChest')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('vestChest')[0].dispatchEvent(evt);
								document.getElementsByName('shirtChest')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('vestChest')[0].fireEvent("onchange");
								document.getElementsByName('shirtChest')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketStomach = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('vestStomach')[0].value = element.value;
							document.getElementsByName('shirtStomach')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('vestStomach')[0].dispatchEvent(evt);
								document.getElementsByName('shirtStomach')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('vestStomach')[0].fireEvent("onchange");
								document.getElementsByName('shirtStomach')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketNeck = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('vestNeck')[0].value = element.value;
							document.getElementsByName('shirtNeck')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('vestNeck')[0].dispatchEvent(evt);
								document.getElementsByName('shirtNeck')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('vestNeck')[0].fireEvent("onchange");
								document.getElementsByName('shirtNeck')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketFront = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtFront')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtFront')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtFront')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketZeroPoint = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtZeroPoint')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtZeroPoint')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtZeroPoint')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketFrontToZeroRight = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtRightFrontToZero')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtRightFrontToZero')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtRightFrontToZero')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketBackToZeroRight = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtRightBackToZero')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtRightBackToZero')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtRightBackToZero')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketTenseBicep = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtTenseBicep')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtTenseBicep')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtTenseBicep')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketFrontToZero = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtFrontToZero')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtFrontToZero')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtFrontToZero')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketBackToZero = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtBackToZero')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtBackToZero')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtBackToZero')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketBack = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtBack')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtBack')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtBack')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedForearm = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtForearm')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtForearm')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtForearm')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketBicep = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtBicep')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtBicep')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtBicep')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketWrist = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('shirtWrist')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('shirtWrist')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('shirtWrist')[0].fireEvent("onchange");
							}
						}
					});
				}
			};

			ko.bindingHandlers.modifiedJacketHips = {
				init: function (element, valueAccessor) {
					ko.utils.registerEventHandler(element, "change", function () {
						var value = valueAccessor();
						if (ko.isWriteableObservable(value)) {
							document.getElementsByName('pantHips')[0].value = element.value;
							if ("createEvent" in document) {
								var evt = document.createEvent("HTMLEvents");
								evt.initEvent("change", false, true);
								document.getElementsByName('pantHips')[0].dispatchEvent(evt);
							} else {
								document.getElementsByName('pantHips')[0].fireEvent("onchange");
							}
						}
					});
				}
			};


			this.jacketcompleted = ko.observable(false);
			this.pantscompleted = ko.observable(false);
			this.suitcompleted = ko.observable(false);
			this.vestcompleted = ko.observable(false);
			this.shirtcompleted = ko.observable(false);




			this.indexJacket = ko.observable("1");
			this.indexPants = ko.observable("1");
		},

		checkcompleted: function (data) {
			if (this.jacketChest() != "" && this.jacketChest() > 0 && this.jacketStomach() != "" && this.jacketStomach() > 0 && this.jacketHips() != "" && this.jacketHips() > 0 &&
				this.jacketFront() != "" && this.jacketFront() > 0 && this.jacketBack() != "" && this.jacketBack() > 0 && this.jacketfullShoulder() != "" && this.jacketfullShoulder() > 0 &&
				this.jacketsleeveL() != "" && this.jacketsleeveL() > 0 && this.jacketsleeveR() != "" && this.jacketsleeveR() > 0 && this.jacketNeck() != "" && this.jacketNeck() > 0 &&
				this.jacketBicep() != "" && this.jacketBicep() > 0 && this.jacketWrist() != "" && this.jacketBicep() > 0 && this.jacketLength() != "" && this.jacketBicep() > 0 &&
				this.jacketZeropoint() != "" && this.jacketBicep() > 0 && this.jacketZeropointfront() != "" && this.jacketZeropointfront() > 0 && this.jacketZeropointback() != "" && this.jacketZeropointback() > 0 &&
				this.jacketRightFrontToZero() != "" && this.jacketRightFrontToZero() > 0 && this.jacketRightBackToZero() != "" && this.jacketRightBackToZero() > 0) {

				this.jacketcompleted(true);
			} else {
				this.jacketcompleted(false);
			}

			// }else if (name == 'pant') {
			if (this.pantWaist() != "" && this.pantWaist() > 0 && this.pantHips() != "" && this.pantHips() > 0 && this.thigh() != "" && this.thigh() > 0 && this.cuffs() != "" && this.cuffs() > 0 &&
				this.pantLength() != "" && this.pantLength() > 0 && this.knee() != "" && this.knee() > 0 && this.calf() != "" && this.calf() > 0 && this.crotch() != "" && this.crotch() > 0) {

				this.pantscompleted(true);
			} else {
				this.pantscompleted(false);
			}

			// }else if (name == 'shirt') {
			if (this.shirtChest() != "" && this.shirtChest() > 0 && this.shirtStomach() != "" && this.shirtStomach() > 0 && this.shirtfullshoulder() != "" && this.shirtfullshoulder() > 0 &&
				this.shirtsleeveL() != "" && this.shirtsleeveL() > 0 && this.shirtsleeveR() != "" && this.shirtsleeveR() > 0 && this.shirtNeck() != "" && this.shirtNeck() > 0 && this.shirtFront() != "" &&
				this.shirtFront() > 0 && this.shirtBack() != "" && this.shirtBack() > 0 && this.shirtLength() != "" && this.shirtLength() > 0 && this.shirtBicep() != "" && this.shirtBicep() > 0 &&
				this.shirtHips() != "" && this.shirtHips() > 0 && this.shirtWrist() != "" && this.shirtWrist() > 0 && this.shirtZeroPoint() > 0 && this.shirtFrontToZero() > 0 && this.shirtBackToZero() > 0 &&
				this.shirtForearm() > 0 && this.shirtRightFrontToZero() > 0 && this.shirtRightBackToZero() > 0) {

				this.shirtcompleted(true);
			} else {
				this.shirtcompleted(false);
			}

			// }else if (name == 'vest') {
			if (this.vestHips() != "" && this.vestHips() > 0 && this.vestLength() != "" && this.vestLength() > 0 && this.vestChest() != "" && this.vestChest() > 0 && this.vestStomach() != "" && this.vestStomach() &&
				this.vestNeck() != "" && this.vestNeck() > 0 && this.vestHalfShoulderWidth() != "" && this.vestHalfShoulderWidth() > 0 && this.vestfullShoulder() != "" && this.vestfullShoulder() > 0) {

				this.vestcompleted(true);
			} else {
				this.vestcompleted(false);
			}

			if (this.pantscompleted() == true && this.jacketcompleted() == true) {
				this.suitcompleted(true);
			} else {
				this.suitcompleted(false);
			}
			if (data == 'jacket') {
				return this.jacketcompleted();
			} else if (data == 'pants') {
				return this.pantscompleted();
			} else if (data == 'suit') {
				return this.suitcompleted();
			} else if (data == 'vest') {
				return this.vestcompleted();
			} else if (data == 'shirt') {
				return this.shirtcompleted();
			} else {
				return false;
			}
		},

		// event: { change: $parent.selectionChanged }
		selectionChanged: function (event) {
			this.fitlineJacketVM = new FitlinesJacket(dsRegistry, this, 2);
		},


		timeConverter: function (timestamp) {
			if (timestamp == '0.00') {
				var a = new Date(timestamp * 1000);
				var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				var year = a.getFullYear();
				var month = months[a.getMonth()];
				var date = a.getDate();
				var hour = a.getHours();
				var min = a.getMinutes();
				var sec = a.getSeconds();
				var time = date + ',' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
				return time;
			} else {
				return 'no date available';
			}
		},


		test: function () {
			console.log(this.orderItemMirror);
		},

		enableVisibility: function () {
			var viewModel = {
				shouldShowMessage: ko.observable(false) // Message initially visible
			};
		},

		garmentsList: function () {
			return ko.computed(function () {
				var gL = this.orderItemMirror.garmentsList();
				result = [
					{
						"name": "jacket",
						"namez": "Jacket",
						"img": "gfx/measurements/jacket_m.png",
						"bgclass": "jacket_measurement",
						"count": gL[0].count()
					},
					{
						"name": "pants",
						"namez": "Pants",
						"img": "gfx/measurements/pants_m.png",
						"bgclass": "pant_measurement",
						"count": gL[1].count()
					},
					{
						"name": "suit",
						"namez": "Suit",
						"img": "gfx/measurements/suit_m.png",
						"bgclass": "suit_measurement",
						"count": gL[2].count()
					},
					{
						"name": "vest",
						"namez": "Vest",
						"img": "gfx/measurements/vest_m.png",
						"bgclass": "vest_measurement",
						"count": gL[3].count()
					},
					{
						"name": "shirt",
						"namez": "Shirt",
						"img": "gfx/measurements/shirt_m.png",
						"bgclass": "shirt_measurement",
						"count": gL[4].count()
					}
				];
				return result;
			}, this);
		},

		garmentsList2: function () {
			return ko.computed(function () {			// This one is used only for showing, at measurements.htm, jacket and pants instead of suit
				var gL = this.orderItemMirror.garmentsList();
				result = [
					{
						"name": "jacket",
						"namez": "Jacket",
						"img": "gfx/measurements/jacket_m.png",
						"bgclass": "jacket_measurement",
						"count": gL[0].count() + gL[2].count()
					},
					{
						"name": "pants",
						"namez": "Pants",
						"img": "gfx/measurements/pants_m.png",
						"bgclass": "pant_measurement",
						"count": gL[1].count() + gL[2].count()
					},
					{
						"name": "vest",
						"namez": "Vest",
						"img": "gfx/measurements/vest_m.png",
						"bgclass": "vest_measurement",
						"count": gL[3].count()
					},
					{
						"name": "shirt",
						"namez": "Shirt",
						"img": "gfx/measurements/shirt_m.png",
						"bgclass": "shirt_measurement",
						"count": gL[4].count()
					}
				];
				return result;
			}, this);
		},


		flushModel: function () {
			var tModel = {};
			tModel.pantWaist = this.pantWaist();
			tModel.knee = this.knee();
			tModel.calf = this.calf();
			tModel.pantHips = this.pantHips();
			tModel.crotch = this.crotch();
			tModel.thigh = this.thigh();
			tModel.pantLength = this.pantLength();
			tModel.cuffs = this.cuffs();
			tModel.zipper = this.zipper();
			tModel.pantFrontCrotch = this.pantFrontCrotch();
			tModel.pantInseam = this.pantInseam();
			tModel.pantAngledWaist = this.pantAngledWaist();
			tModel.pantAngledHems = this.pantAngledHems();
			tModel.pantWaistAngle = this.pantWaistAngle();

			tModel.jacketChest = this.jacketChest();
			tModel.jacketStomach = this.jacketStomach();
			tModel.jacketLength = this.jacketLength();
			tModel.jacketfullShoulder = this.jacketfullShoulder();
			tModel.jacketHips = this.jacketHips();
			tModel.jacketFront = this.jacketFront();
			tModel.jacketBack = this.jacketBack();
			tModel.jacketNeck = this.jacketNeck();
			tModel.jacketBicep = this.jacketBicep();
			tModel.jacketsleeveL = this.jacketsleeveL();
			tModel.jacketsleeveR = this.jacketsleeveR();
			tModel.jacketWrist = this.jacketWrist();
			//tModel.jacketWristWatch				= this.jacketWristWatch();
			tModel.jacketLapel = this.jacketLapel();
			tModel.jacketforeArms = this.jacketforeArms();
			tModel.jacketZeropoint = this.jacketZeropoint();
			tModel.jacketZeropointfront = this.jacketZeropointfront();
			tModel.jacketZeropointback = this.jacketZeropointback();
			tModel.jacketRightFrontToZero = this.jacketRightFrontToZero();
			tModel.jacketRightBackToZero = this.jacketRightBackToZero();
			tModel.jacketTenseBicep = this.jacketTenseBicep();
			//tModel.jacketHalfzero				= this.jacketHalfzero();
			//tModel.jacketHalfstomach			= this.jacketHalfstomach();

			tModel.shirtChest = this.shirtChest();
			tModel.shirtStomach = this.shirtStomach();
			tModel.shirtLength = this.shirtLength();
			tModel.shirtfullshoulder = this.shirtfullshoulder();
			tModel.shirtFront = this.shirtFront();
			tModel.shirtBack = this.shirtBack();
			tModel.shirtNeck = this.shirtNeck();
			tModel.shirtBicep = this.shirtBicep();
			tModel.shirtHips = this.shirtHips();
			tModel.shirtsleeveL = this.shirtsleeveL();
			tModel.shirtsleeveR = this.shirtsleeveR();
			tModel.shirtWrist = this.shirtWrist();
			tModel.shirtWristwatch = this.shirtWristwatch();
			tModel.shirtZeroPoint = this.shirtZeroPoint();
			tModel.shirtFrontToZero = this.shirtFrontToZero();
			tModel.shirtBackToZero = this.shirtBackToZero();
			tModel.shirtRightFrontToZero = this.shirtRightFrontToZero();
			tModel.shirtRightBackToZero = this.shirtRightBackToZero();
			tModel.shirtTenseBicep = this.shirtTenseBicep();
			tModel.shirtForearm = this.shirtForearm();

			tModel.vestLength = this.vestLength();
			tModel.vestHips = this.vestHips();
			tModel.vestVLength = this.vestVLength();
			tModel.vestfullShoulder = this.vestfullShoulder();
			tModel.vestHalfShoulderWidth = this.vestHalfShoulderWidth();
			tModel.vestChest = this.vestChest();
			tModel.vestStomach = this.vestStomach();
			tModel.vestNeck = this.vestNeck();
			tModel.vestComments = this.vestComments();
			tModel.suitComments = this.suitComments();
			tModel.jacketComments = this.jacketComments();
			tModel.pantsComments = this.pantsComments();
			tModel.shirtComments = this.shirtComments();
			tModel.overcoatComments = this.overcoatComments();


			tModel.measurementsORfitlineJacket = this.measurementsORfitlineJacket();
			tModel.measurementsORfitlinePants = this.measurementsORfitlinePants();

			return tModel;
		},

		flushGarment: function (garment) {
			var tModel = {};
			console.log("flushGarment: " + garment);
			if (garment == "Pant" || garment == "Pants" || garment == "Suit" || garment == "pant" || garment == "pants" || garment == "suit" || garment == "Suit/P") {
				tModel.pantWaist = this.pantWaist();
				tModel.knee = this.knee();
				tModel.calf = this.calf();
				tModel.pantHips = this.pantHips();
				tModel.crotch = this.crotch();
				tModel.thigh = this.thigh();
				tModel.pantLength = this.pantLength();
				tModel.cuffs = this.cuffs();
				tModel.zipper = this.zipper();
				tModel.pantFrontCrotch = this.pantFrontCrotch();
				tModel.pantInseam = this.pantInseam();
				tModel.pantAngledWaist = this.pantAngledWaist();
				tModel.pantWaistAngle = this.pantWaistAngle();
			}
			if (garment == "Jacket" || garment == "Suit" || garment == "jacket" || garment == "suit" || garment == "Suit/J") {
				tModel.jacketChest = this.jacketChest();
				tModel.jacketStomach = this.jacketStomach();
				tModel.jacketLength = this.jacketLength();
				tModel.jacketfullShoulder = this.jacketfullShoulder();
				tModel.jacketHips = this.jacketHips();
				tModel.jacketFront = this.jacketFront();
				tModel.jacketBack = this.jacketBack();
				tModel.jacketNeck = this.jacketNeck();
				tModel.jacketBicep = this.jacketBicep();
				tModel.jacketsleeveL = this.jacketsleeveL();
				tModel.jacketsleeveR = this.jacketsleeveR();
				tModel.jacketWrist = this.jacketWrist();
				//tModel.jacketWristWatch				= this.jacketWristWatch();
				tModel.jacketLapel = this.jacketLapel();
				tModel.jacketforeArms = this.jacketforeArms();
				tModel.jacketZeropoint = this.jacketZeropoint();
				tModel.jacketZeropointfront = this.jacketZeropointfront();
				tModel.jacketZeropointback = this.jacketZeropointback();
				//tModel.jacketHalfzero				= this.jacketHalfzero();
				//tModel.jacketHalfstomach			= this.jacketHalfstomach();
			}
			if (garment == "shirt" || garment == "Shirt") {
				tModel.shirtChest = this.shirtChest();
				tModel.shirtStomach = this.shirtStomach();
				tModel.shirtLength = this.shirtLength();
				tModel.shirtfullshoulder = this.shirtfullshoulder();
				tModel.shirtFront = this.shirtFront();
				tModel.shirtBack = this.shirtBack();
				tModel.shirtNeck = this.shirtNeck();
				tModel.shirtBicep = this.shirtBicep();
				tModel.shirtHips = this.shirtHips();
				tModel.shirtsleeveL = this.shirtsleeveL();
				tModel.shirtsleeveR = this.shirtsleeveR();
				tModel.shirtWrist = this.shirtWrist();
				tModel.shirtWristwatch = this.shirtWristwatch();
			}
			if (garment == "vest" || garment == "Vest") {
				tModel.vestLength = this.vestLength();
				tModel.vestHips = this.vestHips();
				tModel.vestVLength = this.vestVLength();
				tModel.vestfullShoulder = this.vestfullShoulder();
				tModel.vestHalfShoulderWidth = this.vestHalfShoulderWidth();
				tModel.vestChest = this.vestChest();
				tModel.vestStomach = this.vestStomach();
				tModel.vestNeck = this.vestNeck();
				tModel.vestComments = this.vestComments();
				tModel.suitComments = this.suitComments();
				tModel.jacketComments = this.jacketComments();
				tModel.pantsComments = this.pantsComments();
				tModel.shirtComments = this.shirtComments();
				tModel.overcoatComments = this.overcoatComments();
			}
			return tModel;
		},


		clear: function () {
			this.pantWaist('');
			this.knee('');
			this.calf('');
			this.pantHips('');
			this.thigh('');
			this.cuffs('');
			this.pantLength('');
			this.crotch('');
			this.zipper('');
			this.pantFrontCrotch('');
			this.pantInseam('');
			this.pantAngledWaist(false);
			this.pantAngledHems(false);
			this.pantWaistAngle('Standard');

			this.jacketChest('');
			this.jacketStomach('');
			this.jacketHips('');
			this.jacketFront('');
			this.jacketBack('');
			this.jacketfullShoulder('');
			this.jacketsleeveL('');
			this.jacketsleeveR('');
			this.jacketNeck('');
			this.jacketBicep('');
			this.jacketWrist('');
			this.jacketWristWatch('');
			this.jacketforeArms('');
			this.jacketLength('');
			this.jacketLapel('');
			this.jacketZeropoint('');
			this.jacketZeropointfront('');
			this.jacketZeropointback('');
			this.jacketRightFrontToZero('');
			this.jacketRightBackToZero('');
			this.jacketTenseBicep('');
			this.jacketHalfzero('');
			this.jacketHalfstomach('');

			this.shirtChest('');
			this.shirtStomach('');
			this.shirtfullshoulder('');
			this.shirtsleeveL('');
			this.shirtsleeveR('');
			this.shirtNeck('');
			this.shirtFront('');
			this.shirtBack('');
			this.shirtLength('');
			this.shirtBicep('');
			this.shirtHips('');
			this.shirtWrist('');
			this.shirtWristwatch('');
			this.shirtZeroPoint('');
			this.shirtFrontToZero('');
			this.shirtBackToZero('');
			this.shirtRightFrontToZero('');
			this.shirtRightBackToZero('');
			this.shirtTenseBicep('');
			this.shirtForearm('');

			this.vestHips('');
			this.vestLength('');
			this.vestChest('');
			this.vestStomach('');
			this.vestNeck('');
			this.vestHalfShoulderWidth('');
			this.vestfullShoulder('');
			this.vestVLength('');

			this.vestComments('');
			this.suitComments('');
			this.jacketComments('');
			this.pantsComments('');
			this.shirtComments('');
			this.overcoatComments('');

			this.jacketDate('');
			this.pantsDate('');
			this.shirtDate('');
			this.vestDate('');

			this.selectedGarment({ name: "none" });
			this.measurementsORfitlineJacket('measurements');
			this.measurementsORfitlinePants('measurements');
		}

	})
});