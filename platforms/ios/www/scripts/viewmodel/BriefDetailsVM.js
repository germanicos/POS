define(['jquery', 'knockout', 'base'], function ($, ko) {

	BriefDetailsVM = SimpleControl.extend({

		/**
		 * initData comes from vm father (the alterationsBriefList's vm)
		 */
		init: function (initData) {

			console.log("init brief detatils");

			self = this;

			self.fitting_id = initData.fitting_id;
			self.images = [];
			self.videos = [];

			self.tailors = ko.observableArray(dsRegistry.getDatasource('tailorsDS').getStore());

			// POST to the server to the get brief details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/get_alteration_brief_details",
				dataType: 'json',
				data: { "user": authCtrl.userInfo, "fitting_id": self.fitting_id  },
				async: false,
				timeout: 60000,

				success: function (ret) {

					console.log("BriefDetailsVM response", ret);
					// set page content
					self.setPageContent(ret);

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log("fitting comments endpoint cannot be reached! ");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
					customAlert("TIME OUT - there is a network issue. Please try again later"); 
				}
			});

			// set pop-up visible or not
			self.emailPopUpIsVisible = ko.observable(false);

			// show tags or not
			self.showImageTags = ko.observable(true);
		},

		/** Initialize view variables*/
		setPageContent: function (briefDetails) {

			console.log("briefDetails", briefDetails);

			// top of page elements
			self.Customer = briefDetails.fitting.customer_name;
			self.Garment = briefDetails.fitting.garment.garment_name;
			self.salesman = briefDetails.fitting.alteration.salesman_first_name + " " + briefDetails.fitting.alteration.salesman_last_name;

			self.orders_products_id = briefDetails.fitting.orders_products_id;
			self.garment_type_id = briefDetails.fitting.garment_id;

			self.delivery_date = briefDetails.fitting.delivery_date;
			self.intialOrderDate = briefDetails.fitting.ordered_date;
			self.fittingDate = briefDetails.fitting.fitting_date;
			self.orderID = briefDetails.fitting.order_id;

			self.selectedTailor = ko.observable(self.tailors().filter( e => e.user_id == briefDetails.fitting.alteration.tailor_id )[0]);

			self.selectedTailor.subscribe(function(newValue) {
				console.log("New tailor:", newValue)
			   	const tailor_id = newValue.user_id;

			   	$.ajax({
			   		type: 'POST',
			   		url: BUrl + "alterations_pos/update_alteration_tailor",
			   		dataType: 'json',
			   		data: { "user": authCtrl.userInfo, "fitting_id": self.fitting_id, 'tailor_id' : tailor_id  },
			   		async: true,
			   		timeout: 60000,

			   		success: function (ret) {

			   			console.log("Change tailor response", ret);
			   			
			   			if( ret.result )
			   			{
			   				$.jGrowl("Tailor Updated !");
			   			}
			   			else
			   			{
			   				customAlert("Something wrong with the server :( Please try again..."); 
			   			}

			   		},
			   		error: function (XMLHttpRequest, textStatus, errorThrown) {
			   			customAlert("Something wrong with the server :( Please try again2..."); 
			   			console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown); 
			   		}
			   	});

			});



			self.givenToTailor = ko.observable(briefDetails.fitting.alteration.given_to_tailor == 1);
			
			self.notes = briefDetails.fitting.notes;
			// video
			self.videos = briefDetails.fitting.fitting_videos;

			// fix videos path
			for (let i = 0; i < self.videos.length; i++) {
				const v = self.videos[i];

				v.path = window.BUrl + v.path;

			}	

			console.log("briefDetails.fitting.fitting_images", briefDetails.fitting.fitting_images);

			Object.keys(briefDetails.fitting.fitting_images).map(function(objectKey, index) {
			    
			    const el = briefDetails.fitting.fitting_images[objectKey];
			    console.log("el", el);
				const img_name = objectKey.replace("_", " ").toUpperCase();

			    if( objectKey === "additional_images" ) // different treatment for additional images
				{
					for (let i = 0; i < el.length; i++) // loop trough all additional images and add to the array
					{
						const additional_image = el[i];

						self.images.push({ "path" : BUrl + additional_image.path, "tags" : additional_image.fitting_image_tags == null ? [] :  additional_image.fitting_image_tags , "img_name" :  "ADDITIONAL IMAGE " + (i+1) });
					}
				}
				else // for normal images => we add one by one
				{
					self.images.push({ "path" : BUrl + el.path, "tags" : el.fitting_image_tags == null ? [] : el.fitting_image_tags, "img_name" : img_name });
				}

			});


			// TODO: other images

			// an image with "path" and "tags"
			self.selectedImage = ko.observable();
			self.selectedTagsNotes = ko.observableArray([]); // reset array

			// Holds information about the other garments in the order => to build the navigation in top of the page
			self.otherOrderGarments = briefDetails.fitting_order_details.garments;

			for(let otherGarment of self.otherOrderGarments)
			{
				// Boolean => tells the system if should send fitting PDF email as well
				otherGarment.sendEmailAsWell = ko.observable(false);
			}
		},
		
		/**
         * Init a briefDetails with the corresponding fit and go to next page
         */
        goToBriefDetails: function (nextPage, data) {

        	console.log('nextPage', nextPage);
        	console.log('data', data);
            

            /**
             * Garmbiarra.
             *
             * this is necessary to change the page when we are in the same page that we want to change.
             *
             * First we destroy the HTML and then we build it again.
             */
            $.when($("#loading_jp").show()).then(function() {

            	briefDetails = new BriefDetailsVM(data);

            	// Change to another page
            	$.when($('#briefDetails').remove()).then( function() {
            		posChangePage('#briefDetails');
            		$("#loading_jp").hide();
            	});


            });
            
            
        },


		/** changed selected image */
		selectImage(selectedImage) {
			
			self.selectedImage(selectedImage);
			self.selectedTagsNotes.removeAll();
			
			// show image tags by default
			self.showImageTags(true);

			// Concatenate notes
			let note = '';
			let cont = 1;

			// go through the entire array of tags concatenating their contents
			for (let index = 0; index < self.selectedImage().tags.length; index++) 
			{
				const tag = self.selectedImage().tags[index];
				note = cont +":  "+JSON.parse(tag.text);
				cont += 1;

				self.selectedTagsNotes.push(note);
			}

			// Not using Pinch zoom anymore => causing problems
			// console.log("zoom enabled...");
			//$.jGrowl("Pinch Zoom Enabled !");
			//////////// Enable pinch zoom using the meta tag ////////////////
			/// Be aware, this should be disable as soon as the pop up cloes -- It may cause wrong behavior in the app
			//const viewport = document.getElementById("meta-viewport-tag");
			//viewport.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=4.0, minimum-scale=1.0, user-scalable=1');
		},

		closeImgPopUp() {
			
			// Not using Pinch zoom anymore => causing problems
			// $.jGrowl("Pinch Zoom disabled");
			// console.log("zoom disabled...");
			// const viewport = document.getElementById("meta-viewport-tag");
			// viewport.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0');
			
			this.selectedImage(null);
		},

		/** Send to a fitting confirmation*/
		sendToTailor(){

			let that = this;

			if(!confirm("Confirm that the tailor got the alteration info ?"))
			{
				return;
			}

			// POST to the server to the get brief details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/set_alteration_to_tailor",
				dataType: 'json',
				data: { "user": authCtrl.userInfo, "fitting_id": self.fitting_id , "alteration_with_tailor": 1 },
				async: true,
				success: function (ret) {

					console.log("set_alteration_to_tailor success");
					
					that.givenToTailor(true);
					alert("You can now fill up and submit the alteration report when the tailor finishes the job");
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) 
				{
					console.log("fitting comments endpoint cannot be reached! ");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
				}
			});
		},

		/** Send an e-mail */
		sendEmail() {

			// make sure that email is not null or blank
			if (document.getElementById("emailTosend").value == "" || document.getElementById("emailTosend").value == null)
			{
				customAlert("E-mail can not be blank");
				return;
			}

			document.getElementById('loading_jp').style.display = "block";


			let alsoSendTo = [];

			for(let other_gar of self.otherOrderGarments)
			{
				if( other_gar.sendEmailAsWell())
				{
					alsoSendTo.push(other_gar);
				}
			}


			// Close the email pop up -- we dont need to wait the server to process
			self.toggleEmailPopUp();

			// POST to the server to the get brief details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/send_brief_email",
				dataType: 'json',
				data: {
					"user": authCtrl.userInfo,
					"fitting_id": self.fitting_id,
					"alsoSendTo" : ko.toJSON(alsoSendTo),
					"email": document.getElementById("emailTosend").value,
					"content": document.getElementById("contentToSend").value
				},
				async: true,

				success: function (ret) 
				{
					if (ret.result == "success")
					{
						$.jGrowl("Email sent");
					} 
					else 
					{
						customAlert("Something went wrong -- EMAIL NOT SENT (ERROR 1)");
					}

					if(!ret.all_images_sync)
					{
						customAlert("Please, sync your images and try to send the email again. *THE LAST ONE HAS UNSYNC IMAGES* ");
					}

					document.getElementById('loading_jp').style.display = "none";

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) 
				{
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
					customAlert("Something went wrong -- EMAIL NOT SENT (ERROR 2)");
					document.getElementById('loading_jp').style.display = "none";
				}
			});
		},

		/** set pop-up visible or not  */
		toggleEmailPopUp(){
			this.emailPopUpIsVisible(!this.emailPopUpIsVisible());
		},

		toggleShowImageTags(){
			self.showImageTags(!self.showImageTags());
		}

	});

});