define(['jquery', 'knockout', 'base'], function ($, ko) {

    UniformPortfolioVM = SimpleControl.extend({

        init: function (uniformTemplateVM) {
            console.log("Init UniformPortfolioVM")
            this.uniformTemplateVM = uniformTemplateVM;

            this.hasSentBase64Images = ko.observable(false);
        },

        /**
         * Get data to show in the view
         */
        getPortfolioData: function (garment_uniqueId) {
            console.log('garment_uniqueId', garment_uniqueId);


            var uniformGarments = this.uniformTemplateVM.garmentsStep.garments();

            var result = ko.observableArray();



            // search garment in all garments array
            for (const garmentTypes of uniformGarments) {
                for (const garment of garmentTypes.garments()) {
                    if (garment.unique_id == garment_uniqueId) {
                        for (const category of garment.categories) {
                            for (const option of category.options) {
                                let optionName = option.name;

                                // Custom data (images) and fit
                                if (["custom data", "fit", "jacket fit", "pant fit"].includes(category.name.toLowerCase())){
                                    // do not show in portfolio
                                    continue;
                                }

                                // special case: fabric is stored in another place
                                if (optionName.toLowerCase() == 'fabric') {
                                    const selectedFabric = fabricsData.store.filter(el=>el.title == garment.fabricInsearch())[0];
                                    result.push([
                                        optionName,
                                        selectedFabric.title + ', ' + (selectedFabric.composition_1 == null ? "" : selectedFabric.composition_1) + ' ' + (selectedFabric.composition_2 == null ? "" : selectedFabric.composition_2) + ' ' + (selectedFabric.composition_3 == null ? "" :selectedFabric.composition_3)]);

                                    result.push(["Fabric weight", selectedFabric.weight]);
                                } else {
                                    // if value was selected
                                    if (option.selectedValue()) {

                                        // do not show any colours if selectedValue is DEFAULT
                                        if (
                                            // type is colour
                                            ['5','8','9'].includes(option.type_id)&&
                                            
                                            // selected colour is DEFAULT
                                            option.selectedValue().name && option.selectedValue().name.toLowerCase().trim() == 'default'
                                            ){
                                            
                                                // do not show in portfolio
                                                continue;
                                        }
                                        
                                        // if selected value is object
                                        let optionValue = option.selectedValue().name == null ? option.selectedValue() : option.selectedValue().name;
                                        result.push([optionName, optionValue]);

                                    }

                                }


                            }
                        }
                        return result;
                    }
                }
            }
        },

        // sends the base 64 images to the server and stores it in the 
        // Database for future use
        autoSendBase64images : function() {

            // when email = null => we dont send it via email, only stores in the DB
            uniformTemplate.garmentsPortfolioStep.prepareBase64Images(email = null)


        },



        sendPortifolioEmail : function() {

            var self = this;

            console.log("Sending portifolio email...");

            const company_email = self.uniformTemplateVM.company().email.trim();

            const email = prompt("Please enter the destination email", company_email);

            if( email === null) { return; }

            this.prepareBase64Images(email)
        },


        /**
         * Gathers data from the templates and sends to the ERP.
         * 
         * @return {[type]} [description]
         */
        prepareBase64Images : function(email) {

            var self = this;

            const numberOfGarments = $(".garment-portifolio-el").length;
            let progress = 0;
            let progress_back = 0;


            var garments = [];
            for (const garmentTypes of this.uniformTemplateVM.garmentsStep.garments()) {
                for (const garment of garmentTypes.garments()) {
                    garments.push(garment);
                }
            }

            // generates the base64 and puts in the DOM element
            $(".garment-portifolio-el").each(function() {

                console.log("looping", $(this) );

                const $selfEl = this;
                const $uiElement = $(this).find('#image-3d');


                // ==================== get back side ========================

                // change 3d to back view
                garments[progress_back].image3D.side('back');
                var finish_back = false; // to sync
                
                try {
                    
                    html2canvas($uiElement, {
                        onrendered: function (canvas) {
                            
                            // store back as base64
                            let datauri_back = canvas.toDataURL("image/png");
                            $($selfEl).data('base64_back', datauri_back);
                            
                            if (progress_back == numberOfGarments){
                                finish_back = true;
                            }
                        }
                    });
                    
                } catch (error) {
                    alert("Error on generate image from 3D");
                    console.error(error);
                }
                
                // change 3d to front again
                garments[progress_back].image3D.side('front');
                
                // Increment progress
                progress_back += 1;

                // ================== end get back side =======================

                // ================ front side ================================
                html2canvas($uiElement, {
                                onrendered: function(canvas) {
                                    let datauri = canvas.toDataURL("image/png");
                                    
                                    $($selfEl).data('base64', datauri);

                                    // Increment progress
                                    progress += 1;

                                    // If has finished generating all base64s => send them to ERP
                                    if( progress == numberOfGarments && finish_back)
                                    {
                                        self._sendPortifolioEmail(email);
                                        self.hasSentBase64Images(true);
                                    }
                                }
                        });

                // ========= end front side ==================================

            });

        },

        /**
         * Second step to send PDF, this function should only be called after prepareBase64Images
         *     
         *     uniformTemplate is a global variable that represents the main viewmodel
         *
         *      IF toEmail == null => just generate the image and do not send the email
         */
        _sendPortifolioEmail : function(toEmail = null) {
            
            let garments = [];

            for(let garmentType of uniformTemplate.garmentsStep.garments() )
            {
                for(let garment of garmentType.garments() )
                {
                    const unique_id = garment.unique_id;

                    // ID of DOM element
                    const $el = `#garment-${unique_id}`;

                    // recover the base64 image from the HTML element
                    const base64 = $($el).data('base64');
                    const base64_back = $($el).data('base64_back');

                    // sets the base64 into the garment ko object
                    garment.base64 = base64;
                    garment.base64_back = base64_back;

                    console.log("Getting preivew");

                    const designPreview = ko.mapping.toJS(uniformTemplate.garmentsPortfolioStep.getPortfolioData(unique_id));

                    console.log("designPreview", designPreview);

                    // Creates object to be sent to ERP
                    const garment_obj = {
                            'base64' : base64,
                            'base64_back' : base64_back,
                            'unique_id' : unique_id,
                            'price' : garment.price(),
                            'template_name' : garment.templateName(),
                            'template_notes' : garment.portifolioNotes(),
                            'design_summary' : designPreview
                        }


                    garments.push(garment_obj);
                }
            }

            // Sends the data to ERP and send PDF from there
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/send_uniform_portifolio_email',
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "garments" : JSON.stringify(garments),
                    "company_id" : uniformTemplate.companyId,
                    "to_email" : toEmail
                },
                success: function (dataS) {

                    if( dataS.result === 'success')
                    {
                        if( toEmail ) // only show alert if email is set
                        {
                            $.jGrowl("Portfolio PDF successfully sent !");
                        }
                    }
                    else
                    {
                        customAlert("Something wrong.... :( The Portfolio email could not be sent");
                    }

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later");
                },
                async: true
            });

        },

        /**
         * set 'front' as default view of 3d
         * 
         * to test: uniformTemplate.garmentsPortfolioStep.change3DView();
         */
        change3DView: function () {
            var uniformGarments = this.uniformTemplateVM.garmentsStep.garments();

            // search garment in all garments array
            for (const garmentTypes of uniformGarments) {
                for (const garment of garmentTypes.garments()) {
                    garment.image3D.side('front');

                }
            }

        }


    })
});