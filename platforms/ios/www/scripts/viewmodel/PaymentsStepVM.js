define(['jquery', 'knockout', 'base'], function ($, ko) {

    PaymentsStepVM = SimpleControl.extend({

        init: function (customer, paymentMethods, pricing, discountCodesFromServer) {
            var self = this;
            	
            console.log("Init  payments VM");

            this.pricing = pricing;
            
            this.customer = customer;

            // germanicos terms --- the customer has to read and accpets the terms
            this.terms = this.getTermsReviewObject();

            // Holds the signature
            this.canvas = null;

            // Bollean => controls if the canvas is open to sign
            this.signing = ko.observable(false);

            // Base64 representation of the signature
            this.canvasBase64 = ko.observable(false);

            this.cities = dsRegistry.getDatasource('citiesDS').getStore();
    		this.states = dsRegistry.getDatasource('statesDS').getStore();
    		this.countries = dsRegistry.getDatasource('countriesDS').getStore();

    		// List of available payment methods
    		this.paymentMethods = ko.observableArray(paymentMethods);

    		// chosen by customer
    		this.selectedPaymentMethod = ko.observable(false);

    		this.ccImage = ko.observable(false);

    		this.banks = ko.observableArray(['ANZ', 'COM', 'STG']);

    		// Bank used when payment = bank transfeer
    		this.selectedBank = ko.observable(false);

    		// Value of the first payment
    		this.depositValue = ko.observable(0.0);

    		this.depositComments = ko.observable("");

    		// City in which the order has been taken
    		this.selectedOrderCity = ko.observable(false);
    		this.selectedOrderCountry = ko.observable("1"); // set Australia as default country
    		this.selectedOrderState = ko.observable(false);

    		// City in which the customer wants the fitting to happen
    		this.selectedFittingCity = ko.observable(false);
    		this.selectedFittingCountry = ko.observable(false);
    		this.selectedFittingState = ko.observable(false);

    		// General order notes
    		this.orderGeneralNotes = ko.observable("");

    		this.includeCompanyInvoice = ko.observable(false);

    		this.mailCustomerInvoice = ko.observable(false);

    		this.isFutureOrder = ko.observable(false);

    		// IF includeCompanyInvoice 
    		this.invoiceCompanyName = ko.observable("");
    		this.invoiceProductDescription = ko.observable("");
    		// End if includeCompanyInvoice
    		  
            this.currentDiscountCode = ko.observable(false);

    		this.extraCosts = ko.observableArray([]);

            // Holds a computed array of the costs of GARMENTS (NO GST)
            this.regularCosts = this.getComputedRegularCosts();
            // End regular costs

            // Holds a computed array of TOTAL COSTS = garment costs + extra costs (NO GST)
            this.totalOrderCost = this.getComputedTotalOrderCosts();

            this.discountCodesFromServer = discountCodesFromServer;
        },

        getSubmissionData : function() {

            const paymentData = {
                'payment_method' : this.selectedPaymentMethod(),
                'cc_image' : this.ccImage(),
                'selected_bank' : this.selectedBank() ? this.selectedBank() : false,
                'deposit_value' : this.depositValue(),
                'deposit_comments' : this.depositComments(),
                'order_city' : this.selectedOrderCity(),
                'fitting_city' : this.selectedFittingCity(),
                'order_notes' : this.orderGeneralNotes(),
                'include_company_invoice' : this.includeCompanyInvoice(),
                'invoice_company_name' : this.invoiceCompanyName(),
                'invoice_product_description' : this.invoiceProductDescription(),
                'mail_customer_invoice' : this.mailCustomerInvoice(),
                'is_future_order' : this.isFutureOrder(),
                'extra_costs' : ko.mapping.toJS(this.extraCosts()),
                'regular_costs' : ko.mapping.toJS(this.regularCosts()),
                'total_order_costs' : ko.mapping.toJS(this.totalOrderCost()),
                'discount_code'  : this.currentDiscountCode(),
                'discount_value' : this.getDiscountCodeValue(), 
            }

            return paymentData;
        },

        getExtraCostObj : function() {

        	return {
        		'description' : ko.observable(''),
        		'value' : ko.observable(0.0)
        	};
        },

        getRegularCostObj : function() {

            return {
                'description' : '',
                'value' : 0.0,
                'GST' : 10, // Default GST 10 % 
                'price_range_id' : "",
                'garment_unique_id' : "" // From which garment the cost is coming -- This is is artificial (just used before the regular id is created)
            };

        },

        // Retursn the computed regular costs function
        getComputedRegularCosts : function() {

            var self = this;

            return ko.computed(function() {

                /**
                 * If customer is not paying => return not costs
                 * The leader MUST pay
                 */
                if( !self.customer.is_paying && !self.customer.is_leader)
                {
                    console.log("Customer is not paying and is not the leader....");
                    return [];
                }


                /**
                 * if customer is paying, but is not the leader => return the normal costs for his garments
                 */
                if( !self.customer.is_leader && self.customer.is_paying )
                {
                    console.log("customer is not the leader, but he is paying...");
                    return self.getCustomerCostsFunc(self.customer);
                }


                /**
                 * If the customer is paying AND he is the leader => get his costs PLUS the costs for the customers that are not paying
                 */
                if( self.customer.is_leader )
                {
                    console.log("customer is the leader, he must pay");
                    let customerCosts = self.getCustomerCostsFunc(self.customer);

                    for( let notPayingCustomer of orders.customers() )
                    {
                        // If customer is paying => continue
                        if( notPayingCustomer.is_paying ) { continue; }

                        console.log("customer is not the leader, and he is not paying, incrementing the leader costs...");


                        const notPayingCustomerCost = self.getCustomerCostsFunc(notPayingCustomer);

                        console.log("notPayingCustomerCost", notPayingCustomerCost);

                        /**
                         * Adds the description from where this cost is coming from...
                         */
                        for (let cost of notPayingCustomerCost) 
                        {
                            cost.description = cost.description + ` ( FOR -- ${notPayingCustomer.full_name} --) `;
                        }

                        // Concat the costs
                        customerCosts.push.apply(customerCosts, notPayingCustomerCost); 
                    }

                    console.log("customerCosts", customerCosts);
                    return customerCosts;
                }
            });
        },


        getCostPriceRangeId : function(priceCategory, fabricRange) {

            var self = this;

            console.log("priceCategory", priceCategory);
            console.log("fabricRange", fabricRange);

            // Convert the prices strcutre in just one array of prices
            const allPricesValues = Object.keys(this.pricing).map(function(objectKey) { return self.pricing[objectKey]; });

            const flatenPrices = allPricesValues.reduce((acc, val) => acc.concat(val), []);

            // Finds the correct price
            const price = flatenPrices.filter( e => e.category.toLowerCase() === priceCategory.toLowerCase() && e.name.toLowerCase() === fabricRange.toLowerCase() );


            if( price.length )
            {
                return price[0].id;
            }
            else
            {   
                return false;
            }

        },

        /**
         * Retunrs the cost for a given customer
         * 
         * @param  {[type]} customer [ the customer object that we need the cost from ]
         */
        getCustomerCostsFunc : function(customer) {

            var self = this;
            let costsArray = [];

            for( let garmentType of customer.garmentsStep.garments() )
            {
                for( let garment of garmentType.garments() )
                {

                    const garment_type = garment.garment_name.toLowerCase().trim();

                    const fabric_title = garment.selectedFabric() ? garment.selectedFabric().title : "None";

                    // if is urgent and less than 4 week, we should add the EXPRESS FEE and it should show in the payments page.
                    // get today date
                    const todayDate = new Date();

                    // get urgentDate
                    const garmentUrgentDate = new Date(garment.selected_year(), garment.selected_month() - 1, garment.selected_day());

                    // check if is urgent and less than 4 week
                    // 4 week == 2419200000 milliseconds
                    if (garment.isUrgent() && (garmentUrgentDate.getTime() - todayDate.getTime() <= 2419200000)) {
                        // if true, add express fee

                        // Gets a 'clean' cost obj 
                        const costObj = self.getRegularCostObj();

                        costObj.description = `EXPRESS FEE for ${garment_type} (${fabric_title})`;
                        costObj.value = parseFloat(300.0);
                        costObj.garment_unique_id = garment.unique_id;


                        // Adds the cost to the costs array
                        costsArray.push(costObj);

                    }

                    if( garment.selectedFabric() && !garment.useCustomerFabric() ) // if garment has fabric already
                    {
                        const fabricObj = garment.selectedFabric();

                        // The keys are uppercase => Shirt, Vest, Jacket, ...
                        const priceKey = garment_type[0].toUpperCase() + garment_type.slice(1);

                        // description for each garment
                        var description;
                        var price;

                        // if shirt
                        if (garment_type == 'shirt'){
                            // if fabric is CUSTOMERFABRIC
                            if (['undefined', undefined].includes(fabricObj.prices[priceKey])){
                                description = `1x Bespoke ${garment_type} (${fabricObj.title})`;
                                price = 0.0 ;
                            }
                            else {
                                // 29 => fabric category
                                // 251 => platinum option
                                // platinum_make on
                                if (garment.categories_map[29].options_map[251].selectedValue()){
                                    description = `1x Platinum Make Shirt (${fabricObj.title})`;
                                    price = 264.00 ;
                                }
                                // platinum_make off
                                else {
                                    description = `1x Standard Make Shirt (${fabricObj.title}) `;
                                    price = 190.00 ;
                                }
                            }


                        }

                        // other garments
                        else {
                            description = `1x Bespoke ${fabricObj.price_range} ${garment_type} (${fabricObj.title})`;
                           
                            // gets the price for the garment
                            price = ['undefined', undefined].includes(fabricObj.prices[priceKey]) ? 0.0 : fabricObj.prices[priceKey];
                        }



                        // Gets a 'clean' cost obj 
                        const costObj = self.getRegularCostObj();

                        costObj.description = description;
                        costObj.value = parseFloat(price);
                        costObj.garment_unique_id = garment.unique_id;
                        
                        // .cal => change the THIS binding inside the function ( first parameter will become the THIS )
                        costObj.price_range_id = self.getCostPriceRangeId.call(self, garment_type, fabricObj.price_range);

                        // Adds the cost to the costs array
                        costsArray.push(costObj);
                    }
                    // If the salesman has selected customer fabric => we set the BRONZE range pricing for the garment
                    else if( garment.useCustomerFabric() )
                    {
                        let pricing = null;
                        var customFabricMake = "";
                        
                        // If price range not found => seach for silver (shirt cases)
                        if(self.pricing[garment_type].filter( e => e.name.toLowerCase() === 'bronze').length > 0)
                        {
                            pricing = self.pricing[garment_type].filter( e => e.name.toLowerCase() === 'bronze')[0];

                            // if garment is suit or jacket, check if Custom Fabric Make is MADE TO MEASURE MAKE ($ 990) or BESPOKE MAKE ($ 1895)
                            if (garment_type.trim().toLowerCase() == "suit" && 
                                garment.categories_map[47].options_map[255].selectedValue() && garment.categories_map[47].options_map[255].selectedValue().id == "4581"){
                                pricing.value = 990;
                                customFabricMake = "(MADE TO  MEASURE MAKE) ";
                            }
                            else if (garment_type.trim().toLowerCase() == "suit" && 
                                garment.categories_map[47].options_map[255].selectedValue() && garment.categories_map[47].options_map[255].selectedValue().id == "4582"){
                                pricing.value = 1895;
                                customFabricMake = " (BESPOKE MAKE) ";
                            }
                            if (garment_type.trim().toLowerCase() == "jacket" && 
                                garment.categories_map[2].options_map[256].selectedValue() && garment.categories_map[2].options_map[256].selectedValue().id == "4583"){
                                pricing.value = 990;
                                customFabricMake = "(MADE TO  MEASURE MAKE) ";
                            }
                            else if (garment_type.trim().toLowerCase() == "jacket" && 
                                garment.categories_map[2].options_map[256].selectedValue() && garment.categories_map[2].options_map[256].selectedValue().id == "4584"){
                                pricing.value = 1895;
                                customFabricMake = " (BESPOKE MAKE) ";
                            }
                        }
                        else
                        {
                            pricing = self.pricing[garment_type].filter( e => e.name.toLowerCase() === 'silver')[0];
                            try {
                                pricing.value = garment.categories_map[29].options_map[251].selectedValue() ? 264.00 : 190.00;
                            } catch (error) {
                                console.warn('error. Garment not shirt');
                            }
                        }
                        
                        console.log("Pricing", pricing);

                        // description for each garment
                        const description = `1x Bespoke CUSTOMER FABRIC ${customFabricMake} ${garment_type}`;

                        // Gets a 'clean' cost obj 
                        const costObj = self.getRegularCostObj();

                        costObj.description = description;
                        costObj.value = parseFloat(pricing.value);
                        costObj.garment_unique_id = garment.unique_id;
                        
                        costObj.price_range_id = pricing.id;

                        // Adds the cost to the costs array
                        costsArray.push(costObj);
                    }

                    // console.log('garment', garment);
                    
                    // extra costs!!! :

                    //  For now, only the the vest and the Jacket (consequently the suit) has extra costs
                    // vest (silk line)
                    if (garment.garment_id == '3') {
                        // get lining selectedValue and check if is a silk line
                        var lining_category = garment.categories.filter(function (el) { return el.id == "27" })[0];

                        var lining_option = lining_category.options.filter(function (el) { return el.id == "132" })[0];

                        // all silk lines have an alphanumeric code.
                        // standard lines are just numeric
                        if (lining_option.selectedValue() && lining_option.selectedValue().name.toLowerCase().match(/[a-z]/i)) {
                            console.log("extra cost for vest");

                            const newExtraCost = self.getRegularCostObj();
                            newExtraCost.description = `1x Upgrade Lining ${garment_type} ${lining_option.selectedValue().name}` ;
                            
                            newExtraCost.value = parseFloat(self.pricing.general.filter(function(el){return el.id == '38'})[0].value);

                            newExtraCost.garment_unique_id = garment.unique_id;
                            newExtraCost.price_range_id = 38; // Silk Lining


                            costsArray.push(newExtraCost);
                        }

                    // jacket & suit (silk line; kissing buttons; Satin Trimming; Top Stitch)
                    } else if (garment.garment_id == '4' || garment.garment_id == '6') {
                        // get lining selectedValue and check if is a silk line
                        var lining_category = garment.categories.filter(function (el) { return el.id == "46" || el.id == "63" })[0];

                        var lining_option = lining_category.options.filter(function (el) { return el.id == "124" || el.id == '203'})[0];

                        // all silk lines have an alphanumeric code.
                        // standard lines are just numeric
                        if (lining_option.selectedValue() && lining_option.selectedValue().name.toLowerCase().match(/[a-z]/i)) {
                            

                            const newExtraCost = self.getRegularCostObj();
                            newExtraCost.description = `1x Upgrade Lining ${garment_type} ${lining_option.selectedValue().name}` ;
                            newExtraCost.value = parseFloat(self.pricing.general.filter(function(el){return el.id == '38'})[0].value);

                            newExtraCost.garment_unique_id = garment.unique_id;
                            newExtraCost.price_range_id = 38; // Silk Lining

                            costsArray.push(newExtraCost);
                        }


                        // check kissing buttons
                        var sleeve_category = garment.categories.filter(function (el) { return el.id == "7" || el.id == '54' })[0];

                        var kissing_option = sleeve_category.options.filter(function (el) { return el.id == "30" || el.id == '170' })[0];

                        if (kissing_option.selectedValue()) {
                            

                            const newExtraCost = self.getRegularCostObj();
                            newExtraCost.description = `1x Kissing buttons ${garment_type}`;
                            newExtraCost.value = parseFloat(self.pricing.general.filter(function(el){return el.id == '39'})[0].value);

                            newExtraCost.garment_unique_id = garment.unique_id;
                            newExtraCost.price_range_id = 39; // Silk Lining

                            costsArray.push(newExtraCost);
                        }



                        // check top stitch
                        var lapel_details = garment.categories.filter(function (el) { return el.id == "11" || el.id == '53' })[0];

                        var stitchLapel = lapel_details.options.filter(function (el) { return el.id == "25" || el.id == '199' })[0];
                        var stitchPockets = lapel_details.options.filter(function (el) { return el.id == "26" || el.id == '200' })[0];

                        if (stitchLapel.selectedValue() || stitchPockets.selectedValue()) {
                            

                            const newExtraCost = self.getRegularCostObj();
                            newExtraCost.description = `1x Top Stitch ${garment_type}`;
                            newExtraCost.value = parseFloat(self.pricing.general.filter(function(el){return el.id == '37'})[0].value);

                            newExtraCost.garment_unique_id = garment.unique_id;
                            newExtraCost.price_range_id = 37; // Silk Lining

                            costsArray.push(newExtraCost);
                        }

                        // check stain trimming? (TODO)

                    }



                 
                }
            }



                console.log("costsArray", costsArray);

                
                return costsArray;
        },

        getComputedTotalOrderCosts : function() {

            var self = this;


            return ko.computed(function() {

                let total = 0.0;

                for(let cost of self.regularCosts())
                {
                    total += parseFloat(cost.value);
                };

                for(let extraCost of self.extraCosts())
                {
                    total += parseFloat(extraCost.value());
                };

                const discount = self.getDiscountCodeValue();

                const withDiscount = total - discount;

                // Update the deposit value automatically with 10% of GST / 2
                console.log('updating deposit...');
                self.depositValue( ( (withDiscount * 1.1) / 2.0).toFixed(2) );

                return withDiscount;
            });

        },



        addExtraCost : function() {
        	console.log("adding extra cost...");
        	this.extraCosts.push(this.getExtraCostObj());
        },

        removeExtraCost : function(extraCost) {
        	console.log("removing extra cost...");
        	this.extraCosts.remove(extraCost);
        },

        getTermsReviewObject : function() {

        	return {
        		visible : ko.observable(false),
        		accepted : ko.observable(false)
        	}

        },

        akldTerm : function(accepted) {
			this.terms.accepted(accepted);
			this.terms.visible(false);
		},

		prepareCanvas : function() {

			console.log("preparing canvas...");

            // clear canvas
            this.canvasBase64(false);

			const divId = "canvas-" + this.customer.unique_id;

			let canvasDiv = $("#"+divId).find("#canvasDiv");

			let canvas = document.createElement('canvas');

			canvas.setAttribute('width', 710);
			canvas.setAttribute('height', 350);
			canvas.setAttribute('id', 'canvasel-' + this.customer.unique_id);

			canvasDiv.append(canvas);
			if(typeof G_vmlCanvasManager != 'undefined') {
				canvas = G_vmlCanvasManager.initElement(canvas);
			}
			context = canvas.getContext("2d");
			
			clickX = new Array();
			clickY = new Array();
			clickDrag = new Array();
			context.clearRect(0, 0, canvas.width, canvas.height); // Clears the canvas

		    canvas.addEventListener('mousedown', _mousedown, false);
	        canvas.addEventListener('mousemove', _mousemove, false);
	        window.addEventListener('mouseup', _mouseup, false);
		
			canvas.addEventListener('touchstart', _touchstart, false);
	        canvas.addEventListener('touchmove', _touchmove, false);

	        $("#"+divId).show();

            // hide the BTN
            this.signing(true);
		},

		saveCanvas : function() {
			console.log("Saving canvas...");
			const canvas = document.getElementById('canvasel-' + this.customer.unique_id);
			this.canvasBase64(canvas.toDataURL("image/png", 0.6));

            //navigator.notification.alert("Signature Saved !");
            //alert("Signature Saved !");

            this.submitSignature()
		},

		clearCanvas : function() {
			console.log("clearing canvas...");
			const canvas = document.getElementById('canvasel-' + this.customer.unique_id).remove();

            this.prepareCanvas();
		},

		chooseCCPicSource : function() {
 
            var self = this;
 
            console.log("choosing Pic source...");
 
            navigator.notification.confirm(
                'CC Picture Source', // message
                function(btnIndex) {
                    self.takeCCPic(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );
 
        },

        removeCCimg : function() {

            var self = this;
 
            console.log("Removing CC img...");
 
            navigator.notification.confirm(
                'Do you really want to delete this image ?', // message
                function(btnIndex) 
                {
                    if(btnIndex == 2)
                    {
                        self.ccImage(false);
                    }
                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['No', 'Yes']         // buttonLabels
            );

        },

		takeCCPic : function(buttonIndex) {

			console.log("Taking CC image...");
 
            const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;
             
            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            }); 
 
            function onSuccess(imageURI) { 
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError); 
            }  
 
            function onFail(message) { 
                alert('Failed because: ' + message); 
            } 
 
            function resolveOnSuccess(entry) {
                 
                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';
                 
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist
                     
                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };
 
            function successMove(entry) {
                console.log('After move');
                 
                console.log("entry.toURL()", entry.toURL());
 	
 				orders.selectedCustomer().paymentsStep.ccImage(entry.toURL());
            };
 
            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };


		},

        // Code from old Order process
        getDiscountCodeValue : function () {

            var code = this.currentDiscountCode();

            if( !code )
            {
                return 0;
            }

            var discounts = this.discountCodesFromServer;

            for (discount_name in discounts) {
                if (code == discount_name) {
                    return parseInt(discounts[code]);
                }
            }

            if (code == "Freelining") {
                return 100;
            } else if (code == "FreeShirt") {
                return 190;
            } else if (code == "FreeLinShi") {
                return 290;
            } else if (code == null || code == undefined || code.length < 12) {
                return 0;
            } else {

                var amount = code.substr(0, 2) + code.substr(7, 2) + "." + code.substr(10, 2);

                var codeday = code.substr(2, 2);
                var codemonth = code.substr(5, 2);
                var sellerFirstNameFD = code.substr(4, 1).toLowerCase();
                var sellerLastNameFD = code.substr(9, 1).toLowerCase();
                var first_nameFD = authCtrl.userInfo.first_name.substr(0, 1).toLowerCase();
                var last_nameFD = authCtrl.userInfo.last_name.substr(0, 1).toLowerCase();
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                if (day < 10) {
                    day = "0" + day;
                } else {
                    day = "" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                } else {
                    month = "" + month;
                }
                if (sellerFirstNameFD == first_nameFD && sellerLastNameFD == last_nameFD && codeday == day && codemonth == month && code.length == 12) {

                    return (amount * 1.0).round(2);
                } else {
                    return 0;
                }
            }
        },


        /**
         * Sends the signature to the server
         * This should be async !!
         * @return {[type]} [description]
         */
        submitSignature : function() {

            console.log('Saving Signature...');
            // using old endpoint for signature image
            const base64 = this.canvasBase64().split(',')[1];
            const customer_id = this.customer.customer_id;
            const salesman = { "username" : authCtrl.username(), "password" : authCtrl.password(), "id" : authCtrl.userInfo.user_id };

            $.ajax({
                type: 'POST',
                url: BUrl + 'client_image_upload/upload_signature_image',
                dataType: 'json',
                async: true,
                data:{
                    salesman: JSON.stringify(salesman),
                    customer_id: customer_id,
                    data: base64
                },
                success: function(dataS) {
                    console.log(dataS);
                    customAlert("Signature saved successfully !");
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    $.jGrowl('Signature Saved Succesfully !!');             
                }
            }); 


        }








    });
});