define(['jquery', 'knockout', 'base'], function($, ko) {
    
    OrderProcessVM = SimpleControl.extend({


        init: function() {
            console.log("Init OrderProcessVM")
            self = this;
            
            // to search customer:
            this.custSelectVM = new CustomerSelectClass(dsRegistry);
			this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
            this.custSelectVM.setParentVM(this);
            
            this.selectedCustomer = ko.observable(false);

            this.creatingCustomer = ko.observable(false);

            // In case of creation of new customer
            this.newCustomer = ko.observable(this.getNewCustomerInstance());
                // Locations 
            this.cities_display = dsRegistry.getDatasource('citiesDS').getStore();
            this.states_display = dsRegistry.getDatasource('statesDS').getStore();
            this.countries = dsRegistry.getDatasource('countriesDS').getStore();

            this.referal_methods_list = ko.observableArray([{"name":"Google Search","id":"1"},{"name":"Bing Search","id":"8"},{"name":"Yahoo Search","id":"9"},{"name":"Print Advertisement","id":"2"},{"name":"TV Ad","id":"3"},{"name":"Radio Ad","id":"4"},{"name":"Billboard","id":"5"},{"name":"Other","id":"6"},{"name":"Referral","id":"7"}]);

            this.days = ko.observableArray([]);
            for (i = 1; i <= 31; i++) this.days.push(i.toString());
  
            this.months  = ko.observableArray([{"name":"January","id":"1"},{"name":"February","id":"2"},{"name":"March","id":"3"},{"name":"April","id":"4"},{"name":"May","id":"5"},{"name":"June","id":"6"},{"name":"July","id":"7"},{"name":"August","id":"8"},{"name":"September","id":"9"},{"name":"October","id":"10"},{"name":"November","id":"11"},{"name":"December","id":"12"}]);
            
            this.years = ko.observableArray([]);
            for (i = 2020; i >= 1930; i--) this.years.push(i.toString());
        },

        addNewCustomer : function() {

            // Unselect any customer
            this.selectedCustomer(false);

            // flag to change screen
            this.creatingCustomer(true);

            // brand new customer
            this.newCustomer(this.getNewCustomerInstance());

            // update datepicker ui
            $(".datepicker").datepicker();

        },

        backToCustomerSearch : function() {

            this.selectedCustomer(false);
            this.creatingCustomer(false);
            this.custSelectVM.customersAutocompleteList([]);
        },

        addCustomer : function() {

            if( !confirm('Proceed to order with this customer ?') )
            {
                return;
            }

            alert("TODO");
        },

        getNewCustomerInstance() {

            console.log("Gettgin new customer....");

            const customer = {
                      "customer_id"                   : ko.observable(null),
                      "customer_first_name"           : ko.observable(""),
                      "customer_last_name"            : ko.observable(""),
                      "customer_birth_day"            : ko.observable(""),
                      "customer_birth_month"          : ko.observable(""),
                      "customer_birth_year"           : ko.observable(""),
                    //   "customer_DOB": "",
                      "customer_occupation"           : ko.observable(""),
                      "customer_company_name"         : ko.observable(""),
                      "customer_gender"               : ko.observable("1"), // default
                      "customer_address1"             : ko.observable(""),
                      "customer_address2"             : ko.observable(""),
                      "customer_country"              : ko.observable(""),
                      "customer_state"                : ko.observable(""),
                      "customer_city"                 : ko.observable(""),
                      "customer_postal_code"          : ko.observable(""),
                      "customer_mobile_phone"         : ko.observable(""),
                      "customer_landline_phone"       : ko.observable(""),
                      "customer_email"                : ko.observable(""),
                      "customer_email_subscription"   : ko.observable(""),
                      "customer_best_time_to_contact" : ko.observable(null),
                      "customer_referal_method"       : ko.observable(""),
                      "customer_refered_by"           : ko.observable(""),
                      "customer_other_way"            : ko.observable(""),
                      "customer_referal_moreinfo"     : ko.observable(""),
                      "customer_price_range"          : ko.observable(""),
                      "customer_cat_id"               : ko.observable(""),
                      "customer_how_did_hear"         : ko.observable(""),
                      "customer_landline_code"        : ko.observable(""),
                      "receive_marketing"             : ko.observable("")
                    }

            // a computed date for birth
            customer.customer_DOB = ko.computed(function(){
                return `${customer.customer_birth_month()}/${customer.customer_birth_day()}/${customer.customer_birth_year()}`;
            });
            
            return customer;
        },

        selectCustomer : function(customer){
            console.log("appending client to list.", customer);

         
            this.selectedCustomer(customer);
            this.custSelectVM.customersAutocompleteList([]);

            $.jGrowl("Added successfully");
        },

        /**
         * Creates customer and proceed to order
         * @return {[type]} [description]
         */
        createNewCustomer : function() {
            
            var self = this;

            const customer = ko.mapping.toJS(self.newCustomer());

            console.log("PROCEDING WITH customer.....", customer);


            if( !confirm(`Create customer and proceed to order ?`) )
            {
                return;
            }  

            
            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/add_customer',
                dataType: 'json',
                data: { 
                    "user": authCtrl.userInfo,
                    "customer": JSON.stringify(customer)
                },
                success: function (dataS) {
                    
                    console.log("add_customer result", dataS);
                    
                    if(dataS.result)
                    {
                        // Sets the variables necessary in proceedToOrder
                        customer.server_id = dataS.customer_id;
                        // Selectes the recently created customer
                        self.selectedCustomer(customer);

                        // Call proceedToOrder
                        self.proceedToOrder(showConfirm = false);
                    }
                    else
                    {
                        customAlert("Problem creating the customer :("); 
                    }

                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("");
                    
                    // changeVisibilityDiv('loading_jp');

                    completedFittings = false;

                    customAlert("TIME OUT - there is a network issue. Please try again later"); 
                },
                async: false
            });



        },


        /**
         * Post to ERP. Send a list of Selected clients
         */
        proceedToOrder: function(showConfirm = true) {
            
            var self = this;

            if( showConfirm && !confirm(`Proceed to order with ${self.selectedCustomer().customer_first_name + ' ' + self.selectedCustomer().customer_last_name } ?`) )
            {
                return;
            }

            $('#loading_jp').show( () => {

                const customer = ko.mapping.toJS(self.selectedCustomer());

                // sets necessary flags for ERP
                customer.is_leader = true;
                customer.is_present = true;
                customer.is_paying = true;
                customer.is_locked = false;
                customer.server_id = parseInt(customer.customer_id) > 0 ? customer.customer_id : customer.server_id;

                $.ajax({
                    type: 'POST',
                    timeout: 60000, // sets timeout to 60 seconds
                    url: BUrl + 'orders_pos/prepare_order_customers',
                    dataType: 'json',
                    data: { 
                        "user": authCtrl.userInfo,
                        "customers": JSON.stringify([customer])
                    },
                    success: function (ret) {
                        
                        console.log(ret);

                        if(ret.result)
                        {
                            const groupOrdersData = {
                                    'customers_ids'  : ret.customers_ids,
                                    'leader_id'      : ret.leader_id,
                                    'customers_data' : [customer],
                                    'leaderPic'      : "",
                                    'group_name'     : "",
                                    'is_reorder'     : false
                                };
                            /**
                             * Writing the data in the local store, so we can retrieve it when we initiate the group orders 
                             * process
                             */
                            localStorage.setItem('groupOrdersData' , JSON.stringify(groupOrdersData) );
                            posChangePage('#orders');
                        }
                        else
                        {
                            alert("Something wrong... :( please try again");
                            //posChangePage('#main');
                        }
                   
                    },
                    error: function (error) {
                        console.log(JSON.stringify(error));
                        customAlert("TIME OUT - there is a network issue. Please try again later"); 
                        //posChangePage('#main');
                    },
                    async: false
                });

            });



            
        },


    });
});