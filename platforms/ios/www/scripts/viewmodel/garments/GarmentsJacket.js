define(['jquery', 'knockout', 'base'], function($, ko) {
GarmentsJacket = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;

		this.fabricsVM   = new FabricsCntClass(DsRegistry);
		this.fabricsVM.subscribeTo('fabricsDS');


		this.JacketData     = [];
		this.JacketDataAID  = 0;
		this.test = 0;
		
		this.mediaUpload = orderItem.MediaUploadVM;
		this.indexGarment = -1;
		this.indexOption = -1;
		this.isImage = false;
		this.customCount = ko.observable(0);
		this.isExternal = false;;
		
		this.galleryImages = function(){
			that.isExternal = true;
			$('#files').trigger('click');
		};
		
		this.isFabric = false;
		
		this.addCustomImage = function(indexGarment, indexOption, isFabric){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = true;
			that.isExternal = false;
			that.isFabric = isFabric;
			that.mediaUpload.choosePhoto();
		};
		
		this.addCustomVideo = function(indexGarment, indexOption){
			that.indexGarment = indexGarment;
			that.indexOption = indexOption;
			that.isImage = false;
			that.isExternal = false;
			that.mediaUpload.chooseVideo();
		};
		
		this.mediaCallback = function(data){
			if(that.isFabric){
				orderItem.FabricImage.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
				that.isFabric = false;
			}
			else if(that.isImage){
				that.JacketData[that.indexGarment].custom.getCustomImages(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
			else{
				that.JacketData[that.indexGarment].custom.getCustomVideos(that.indexOption).addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, !that.isExternal);
			}
		};
		
		this.previousStepEnabled = ko.observable(false);
	
		this.nextStepOLD = function() {
			var cs = that.currentStep().id;					
			if(cs == 0){
				if(that.JacketFabric().title != "none"){
					that.workflow()[cs].completed = true;
				}	
			}else{
				that.workflow()[cs].completed = true;	
			}
			var foundnotcompletedstep = false;
			for(var x = cs+1; x < that.workflow().length; x++){
				if(that.workflow()[x].completed == false){
					that.currentStep(that.workflow()[x] );
					foundnotcompletedstep = true;
					break;
				}
			}				
			if(foundnotcompletedstep == false){
				for(var x = 0; x <= cs ; x++){
					if(that.workflow()[x].completed == false){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}
			}
			if(foundnotcompletedstep == false){
				posChangePage('#orderItemSelectGarment');
			}
		}


		this.nextStep = function() {
			
			var cs = that.currentStep().id;
			for(var pos in that.workflow()){
				if(that.workflow()[pos].id == cs)
					break;
			}
				
			var notcompletedstep = false;						
			if(cs == 0){
				for(var x = 0; x < that.JacketData.length; x++){								
					if (that.JacketData[x].JacketFabric.title == 'none' && !that.JacketData[x].JacketHasCustomerFabric) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 1){
				for(var x = 0; x < that.JacketData.length; x++){
					if (that.JacketData[x].JacketJacketStructure.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 2){
				for(var x = 0; x < that.JacketData.length; x++){									
					if (that.JacketData[x].JacketBottomStyle.id == 0 ) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 3){
				for(var x = 0; x < that.JacketData.length; x++){
					if (that.JacketData[x].JacketVentStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 4){
				for(var x = 0; x < that.JacketData.length; x++){
					if (that.JacketData[x].JacketLapelStyle.id == 0) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 5){
				// nothing to check here
				;
			}else if(cs == 6){
				for(var x = 0; x < that.JacketData.length; x++){
					if (that.JacketData[x].JacketPocketStyle.id == 0 ) {
						notcompletedstep = true;
						break;
					}	
				}
			}else if(cs == 7){
				// nothing to check here
				;				
			}else if(cs == 8){
				// nothing to check here
				;
			}else if(cs == 9){
				/* 
				for(var x = 0; x < that.JacketData.length; x++){
					if (that.JacketData[x].JacketDefaultMonogramHeader == true && that.JacketData[x].JacketMonogram.trim() == "") {
						notcompletedstep = true;
						break;
					}		
					if (that.JacketData[x].JacketCustomMonogram == true && ( that.JacketData[x].JacketCustomMonogramImage == "" && that.JacketData[x].JacketCustomMonogramNotes.trim() == "") ) {
						notcompletedstep = true;
						break;
					}
					if (that.JacketData[x].JacketEmbroidery == true && ( that.JacketData[x].JacketEmbroideryImage == "" && that.JacketData[x].JacketEmbroideryNotes.trim() == "") ) {
						notcompletedstep = true;
						break;
					}		
				}
				*/
			}else if(cs == 'Custom'){
				for(var x = 0; x < that.JacketData.length; x++){
					if(!that.JacketData[x].custom.isFinished()){
						notcompletedstep = true;
						break;
					}
				}
			}	
			
			if(notcompletedstep == false){
				if(cs == 'Custom'){
					that.customWorkflow.completed = true;
				}
				else{
					that.workflow()[pos].completed = true;
				}
				var foundnotcompletedstep = false;
				for(var x = 0; x < that.workflow().length; x++){
					if(that.workflow()[x].completed == false && that.workflow()[x] !== that.customWorkflow){
						that.currentStep(that.workflow()[x] );
						foundnotcompletedstep = true;
						break;
					}
				}			
				if(notcompletedstep == false){
					for(var x = 0; x <= cs ; x++){
						if(that.workflow()[x].completed == false){
							that.currentStep(that.workflow()[x] );
							foundnotcompletedstep = true;
							break;
						}
					}
					if(foundnotcompletedstep == false && !that.customWorkflow.completed && that.customCount() > 0 ){
						that.currentStep(that.customWorkflow);
						foundnotcompletedstep = true;
					}
				}
				if(foundnotcompletedstep == false){
					posChangePage('#orderItemSelectGarment');
				}				
				
			}else{
				if(cs == 'Custom'){
					that.customWorkflow.completed = false;
					that.currentStep(that.customWorkflow);
				}
				else{
					that.workflow()[pos].completed = false;
					that.currentStep(that.workflow()[pos] );
				}
				
				customAlert("Please fill in all necessary fields");
			}
			
		}





		this.workflow    = ko.observableArray([
			{id: 0, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Fabric" },
			{id: 1, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Jacket Style" },
			{id: 2, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Bottom" },
			{id: 3, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Vent" },
			{id: 4, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Lapel" },
			{id: 10, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Lapel Details" },
			{id: 5, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Sleeves" },
			{id: 6, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Pockets" },
			{id: 7, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Breast Pockets" },
			{id: 8, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Lining & Piping" },
			/*{id: 9, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Monogram" }*/
			{id: 9, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Notes" },
			{id: 'Custom', target: "#garmentsJacket",  completed: false, caption: "Pant", title: "Custom Options" }
                       // {id: 0, target: "#garmentsJacket",  completed: false, caption: "Jacket", title: "Jacket structure",      icon: "gfx/workflow_icons/Jacket/icon1.png"},
					   
		]);
		this.customWorkflow = this.workflow()[this.workflow().length - 1];
		
		this.customStep = function(){
			that.currentStep(that.customWorkflow);
		};

		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			for(var pos in that.workflow()){
				if(that.workflow()[pos].id == data.id)
					break;
			}
			
			if(pos == 3){
				that.PreviewVisible('back'); 
			}else if(pos == 6){
				that.PreviewVisible('buttons');
			}else if(pos == 9){
				that.PreviewVisible('monogram');
			//}else if(data.id == 9){
			//	that.PreviewVisible('monogram');
			}else{
				that.PreviewVisible('front');
			}
			
			posChangePage(data.target);
		});

		this.isStepCompleted = ko.computed(function() {

			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					return that.workflow()[ind].completed;
				}
			}
			console.log('isStepCompleted bo');
			return false;
		});


		this.JacketStepACompletion = function() {
			/*
			for ( var ind in that.JacketData ) {
			if (that.JacketData[ind].JacketFabric.id == 0 ) {
				that.setAsCompleted(0, false);
					return;
				}
			}
			for ( var ind in that.JacketData ) {
			if (that.JacketData[ind].JacketBottomStyle.id == 0 ) {
				that.setAsCompleted(0, false);
					return;
				}
			}
			for (     ind in that.JacketData ) {
			if (that.JacketData[ind].JacketVentStyle.id == 0 ) {
				that.setAsCompleted(0, false);
					return;
				}
			}
			that.setAsCompleted(0, true);
			*/
			//Black magic
			//that.currentStep(that.currentStep());
		};

		this.JacketStepBCompletion = function() {
			/*
			for ( var ind in that.JacketData ) {
				if (that.JacketData[ind].JacketLapelStyle.id == 0 ) {
					that.setAsCompleted(1, false);
						return;
					}
			}
			that.setAsCompleted(1, true);
			*/
			//Black magic
			//that.currentStep(that.currentStep());
		};

		this.JacketStepCCompletion = function() {
			/*
			for ( var ind in that.JacketData ) {
				if (that.JacketData[ind].JacketPocketStyle.id == 0 ) {
					that.setAsCompleted(2, false);
						return;
					}
			}
			for ( var ind in that.JacketData ) {
				if (that.JacketData[ind].JacketSleeveButtonNumber.id == 0 ) {
					that.setAsCompleted(2, false);
						return;
					}
			}
			that.setAsCompleted(2, true);
			*/
			//Black magic
			//that.currentStep(that.currentStep());
		};

		this.JacketStepDCompletion = function() {
			/*
			for ( var ind in that.JacketData ) {
				if (that.JacketData[ind].JacketLining.id == 0 ) {
					that.setAsCompleted(3, false);
						return;
					}
			}
			for ( var ind in that.JacketData ) {
				if (that.JacketData[ind].JacketButtonColor.id == 0 ) {
					that.setAsCompleted(3, false);
						return;
					}
			}
			for ( var ind in that.JacketData ) {
				if (that.JacketData[ind].JacketThreadColor.id == 0 ) {
					that.setAsCompleted(3, false);
						return;
					}
			}
			that.setAsCompleted(3, true);
			*/
			//Black magic
			//that.currentStep(that.currentStep());

			
		};




		this.setAsCompleted =  function(id, status) {
		//	if (status) {console.log(id + ' setAdCompleted ' + status);}
			var tWork = that.workflow();
			tWork[id].completed = status;
			//that.workflow(tWork);
		};





		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});


		this.completion       = ko.observable(0);
		this.variantNameJacket  = ko.observableArray([{id: 0, title: "Jacket 1"}]);
		this.selectedVariantJacket  = ko.observable(this.variantNameJacket()[0]);
		this.selectedVariantJacket.subscribe(function(data) {
			$('.cloneDialog').remove();	
                        that.selectVariantJacket();
		});

		//Prev/Next computedz

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== true) {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return 'not avilable'
				//return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});



        	


/*
 
 function(){ 
                                        	if(id != 0){
                                            	return true;
                                             }else if( $parent.JacketJacketStructure().id === 'Double_Breasted' ||  $parent.JacketJacketStructure().id === 'Double_Breasted_One_to_close'||  $parent.JacketJacketStructure().id === 'Double_Breasted_six_Buttons' ){
                                             		if(id === 1 || id === 2){
                                                    	return false;
                                                    }else{
                                                    	return true;    
                                                	}
                                             }else{
                                             	return true;
                                             }         
                                        
                                        }
 
 
 */

		this.JacketNotes = ko.observable('');
		this.JacketNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});		

		this.JacketFabricNotes = ko.observable('');
		this.JacketFabricNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketFabricNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketStructureNotes = ko.observable('');
		this.JacketStructureNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketStructureNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketBottomNotes = ko.observable('');
		this.JacketBottomNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBottomNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketVentNotes = ko.observable('');
		this.JacketVentNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketVentNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketLapelStyleNotes = ko.observable('');
		this.JacketLapelStyleNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLapelStyleNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketButtonsNotes = ko.observable('');
		this.JacketButtonsNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketButtonsNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketPocketsNotes = ko.observable('');
		this.JacketPocketsNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketPocketsNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});		
		this.JacketLiningColoursNotes = ko.observable('');
		this.JacketLiningColoursNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLiningColoursNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketMonogramNotes = ko.observable('');
		this.JacketMonogramNotes.subscribe(function(data) {				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketMonogramNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});


		this.JacketCustomStructureNotes = ko.observable('');
		this.JacketCustomStructureNotes.subscribe(function(data) {
console.log("ADDING DATA TO JacketCustomStructureNotes");
console.log(data);				
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomStructureNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketCustomStructureImage = ko.observable('');
		this.JacketCustomStructureImage.subscribe(function(data) {
console.log("ADDING DATA TO JacketCustomStructureImage");
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomStructureImage = json;
			
			that.flushModel();
			that.JacketStepBCompletion();
		});
		
		
		this.JacketCustomVentNotes = ko.observable('');
		this.JacketCustomVentNotes.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomVentNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketCustomVentImage = ko.observable('');
		this.JacketCustomVentImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomVentImage = json;
			that.flushModel();
			that.JacketStepBCompletion();
		});		
		
		
		this.JacketCustomLapelNotes = ko.observable('');
		this.JacketCustomLapelNotes.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomLapelNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketCustomLapelImage = ko.observable('');
		this.JacketCustomLapelImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomLapelImage = json;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		
		this.JacketCustomBottomNotes = ko.observable('');
		this.JacketCustomBottomNotes.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomBottomNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketCustomBottomImage = ko.observable('');
		this.JacketCustomBottomImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomBottomImage = json;
			that.flushModel();
			that.JacketStepBCompletion();
		});

		//JacketFabric
		this.JacketFabric = ko.observable(that.dsRegistry.getDatasource('fabricsDS').getStore()[1]);
		this.JacketFabric.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketFabric = data;
			that.flushModel();
			that.JacketStepACompletion();
			
		//	that.Price(data.prices);
		//	that.PriceRange(data);
		});
		
		/*
		this.Price = ko.observable('');
		this.Price.subscribe(function(data) {
				//		that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].Price = that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketFabric.price;
				//		that.flushModel();
				//		that.JacketStepBCompletion();
				that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].Price = data.Jacket;
console.log("PRICE: " + data.Jacket);
				that.flushModel();
				that.JacketStepACompletion();
				
        	});
        	
		this.PriceRange = ko.observable('');
		this.PriceRange.subscribe(function(data) {
			 //	that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].PriceRange = that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketFabric.price_range;
			//	that.flushModel();
			//	that.JacketStepBCompletion();
				that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].PriceRange = data.price_range;
console.log("PRICERANGE: " + data.price_range);				
				that.flushModel();
				that.JacketStepACompletion();
			
        	});		
		*/
		
		


		//JacketBottom
		this.JacketBottomStyleList = ko.observable(that.dsRegistry.getDatasource('suitBottomStyleDS').getStore());
		this.JacketBottomStyleListDoubleBreasted = ko.observable(that.dsRegistry.getDatasource('suitBottomStyleDS').getStore().slice(2,4));
		this.JacketBottomStyle     = ko.observable(that.dsRegistry.getDatasource('suitBottomStyleDS').getStore()[0]);
		this.JacketBottomStyle.subscribe(function(data) {
console.log(data);			
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBottomStyle = data;
			that.flushModel();
			that.JacketStepACompletion();
		});


//suitJacketStructure
		this.JacketJacketStructureList = ko.observable(that.dsRegistry.getDatasource('suitJacketStructureDS').getStore());
		this.JacketJacketStructure     = ko.observable(that.dsRegistry.getDatasource('suitJacketStructureDS').getStore()[0]);
		this.JacketJacketStructure.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketJacketStructure = data;
			that.flushModel();
			that.JacketStepACompletion();
		});


		//JacketVent
		this.JacketVentStyleList = ko.observable(that.dsRegistry.getDatasource('suitVentStyleDS').getStore());
		this.JacketVentStyle     = ko.observable(that.dsRegistry.getDatasource('suitVentStyleDS').getStore()[0]);
		this.JacketVentStyle.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketVentStyle = data;
			that.flushModel();
			that.JacketStepACompletion();
		});

		//JacketLapelStyle 
		this.JacketLapelStyleList = ko.observable(that.dsRegistry.getDatasource('suitLapelStyleDS').getStore());
		this.JacketLapelStyle     = ko.observable(that.dsRegistry.getDatasource('suitLapelStyleDS').getStore()[0]);
		this.JacketLapelStyle.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLapelStyle = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});



		//this.JacketBreastPocketStyleList = ko.observableArray(["Standard Pocket","Patch Pocket","Welt Pocket"]);               
		this.JacketBreastPocketStyleList = ko.observableArray(that.dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore());
		this.JacketBreastPocketStyle     = ko.observable(that.dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore()[0]);
		this.JacketBreastPocketStyle.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBreastPocketStyle = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});

		this.JacketTicketPocketsDifferent = ko.observable(false);
		this.JacketTicketPocketsDifferent.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTicketPocketsDifferent = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		/*
		this.JacketTicketPocketStyleList = ko.observable(that.dsRegistry.getDatasource('tickPocketStyleDS').getStore());               
		this.JacketTicketPocketStyle     = ko.observable(that.dsRegistry.getDatasource('tickPocketStyleDS').getStore()[0]);
		this.JacketTicketPocketStyle.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTicketPocketStyle = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		*/
		//JacketLapelWidth
		this.JacketLapelWidth = ko.observable(0);
		this.JacketLapelWidth.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLapelWidth = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
/*
		//JacketLapelSatin
		this.JacketLapelSatin = ko.observable(false);
		this.JacketLapelSatin.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLapelSatin = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
*/
		//JacketPocketStyle 
		this.JacketPocketStyleList = ko.observable(that.dsRegistry.getDatasource('suitPocketStyleDS').getStore());
		this.JacketPocketStyle     = ko.observable(that.dsRegistry.getDatasource('suitPocketStyleDS').getStore()[0]);
		this.JacketPocketStyle.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketPocketStyle = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});

		//JacketSleeveButtonNumber 
		this.JacketSleeveButtonNumberList = ko.observable(that.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore());
		this.JacketSleeveButtonNumber     = ko.observable(that.dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore()[3]); 
		this.JacketSleeveButtonNumber.subscribe(function(data) {
console.log(JSON.stringify(data));
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonNumber = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});
		
		this.JacketKissingButtons = ko.observable(false);
		this.JacketKissingButtons.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketKissingButtons = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});


		this.JacketButtonholeOnLapel = ko.observable(true);
		this.JacketButtonholeOnLapel.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketButtonholeOnLapel = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});

		/*	// REMOVED
		this.JacketButtonholeOnLapelContrastCheck = ko.observable(false);
		this.JacketButtonholeOnLapelContrastCheck.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketButtonholeOnLapelContrastCheck = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});
		*/

		this.JacketButtonholeOnLapelColorList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.JacketButtonholeOnLapelColor = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketButtonholeOnLapelColor.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketButtonholeOnLapelColor = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});
		
		
		this.JacketSleeveButtonType     = ko.observable('Working');
		this.JacketSleeveButtonType.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonType = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});

		

        //Jacket Designer Vent 		
		this.JacketDesignerVent  = ko.observable(false);														 
		this.JacketDesignerVent.subscribe(function(data) {													 
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketDesignerVent = data;		 
			that.flushModel();																			 
			that.JacketStepCCompletion();																 
		});
		
        //Jacket Tail		
		this.JacketTail  = ko.observable(false);														 
		this.JacketTail.subscribe(function(data) {													 
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTail = data;		 
			that.flushModel();																			 
			that.JacketStepCCompletion();																 
		});		
		
/*
        //Jacket CustomVent 		
		this.JacketCustomVent = ko.observable(false);														 
		this.JacketCustomVent.subscribe(function(data) {													 
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomVent = data;		 
			that.flushModel();																			 
			that.JacketStepCCompletion();																 
		});
*/

        //Dinner suit 		
		this.JacketDinner = ko.observable(false);														 
		this.JacketDinner.subscribe(function(data) {													 
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketDinner = data;		 
			that.flushModel();																			 
			that.JacketStepCCompletion();																 
		});																								 

		this.JacketCollarAndLapelTrimming = ko.observable(false);													 
		this.JacketCollarAndLapelTrimming.subscribe(function(data) {													 
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCollarAndLapelTrimming = data;	 
			that.flushModel();																						 
			that.JacketStepCCompletion();																			 
		});																											 



		this.JacketContrastTrimming = ko.observable(false);
		this.JacketContrastTrimming.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketContrastTrimming = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});
		
		
		
		this.JacketCollarTrimming = ko.observable(false);														 
		this.JacketCollarTrimming.subscribe(function(data) {													
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCollarTrimming = data;		
			that.flushModel();																					
			that.JacketStepCCompletion();																		
		});																										
		
		this.JacketLapelTrimming = ko.observable(false);														 
		this.JacketLapelTrimming.subscribe(function(data) {														
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLapelTrimming = data;		
			that.flushModel();																					
			that.JacketStepCCompletion();																		
		});																										
		
		this.JacketCoverButtons = ko.observable(false);														 
		this.JacketCoverButtons.subscribe(function(data) {														
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCoverButtons = data;		
			that.flushModel();																					
			that.JacketStepCCompletion();																		
		});	

		this.JacketPocketTrimming = ko.observable(false);
		this.JacketPocketTrimming.subscribe(function(data) {														
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketPocketTrimming = data;		
			that.flushModel();																					
			that.JacketStepCCompletion();																		
		});
		
		this.JacketBreastPocketTrimming = ko.observable(false);														 
		this.JacketBreastPocketTrimming.subscribe(function(data) {														
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBreastPocketTrimming = data;		
			that.flushModel();																					
			that.JacketStepCCompletion();																		
		});		

//suitContrastPockets 		
		this.JacketContrastPockets = ko.observable(false);
		this.JacketContrastPockets.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketContrastPockets = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});


        /*
		this.JacketBreastPocket = ko.observable(false);
		this.JacketBreastPocket.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBreastPocket = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});
		*/

		this.JacketBreastPocketAngledStyle = ko.observable('Straight');
		this.JacketBreastPocketAngledStyle.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBreastPocketAngledStyle = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});


	//suitContrastLapelLower 		
		this.JacketContrastLapelLower = ko.observable(false);
		this.JacketContrastLapelLower.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketContrastLapelLower = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});

                //suitLapelSleeveButtonHole 		
		this.JacketLapelButtonHole = ko.observable(false);
		this.JacketLapelButtonHole.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLapelButtonHole = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});

                //suitLapelSleeveButtonHole 		
		this.JacketTicketPocket = ko.observable(false);
		this.JacketTicketPocket.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTicketPocket = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});

//suitContrastLapelUpper 		
		this.JacketContrastLapelUpper = ko.observable(false);
		this.JacketContrastLapelUpper.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketContrastLapelUpper = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});

                //suitContrastCheckPocket 		
		this.JacketContrastCheckPocket = ko.observable(false);
		this.JacketContrastCheckPocket.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketContrastCheckPocket = data;
			that.flushModel();
			that.JacketStepCCompletion();
		});



		//JacketTopStitch
		this.JacketTopStitch = ko.observable(false);
		this.JacketTopStitch.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitch = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		//JacketTopStitchPocket
		this.JacketTopStitchPocket = ko.observable(false);
		this.JacketTopStitchPocket.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchPocket = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		//JacketTopStitchLapel
		this.JacketTopStitchLapel = ko.observable(false);
		this.JacketTopStitchLapel.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchLapel = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
	/*	
		this.JacketTopStitchDarts = ko.observable(false);
		this.JacketTopStitchDarts.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchDarts = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		this.JacketTopStitchAroundBottom = ko.observable(false);
		this.JacketTopStitchAroundBottom.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchAroundBottom = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
	*/
	
		/*
		this.JacketTopStitchContrast = ko.observable(false);
		this.JacketTopStitchContrast.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchContrast = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		*/

		//JacketTopStitchPiping 
		this.JacketTopStitchPipingList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.JacketTopStitchPiping     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketTopStitchPiping.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchPiping = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		//JacketPiping 
		this.JacketPipingList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.JacketPiping     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketPiping.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketPiping = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		this.JacketInsidePocketsDifferentFromPiping = ko.observable(false);
		this.JacketInsidePocketsDifferentFromPiping.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketInsidePocketsDifferentFromPiping = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketInsidePocketsColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketInsidePocketsColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketInsidePocketsColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});


		//JacketLining 
		this.JacketLiningList = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore());
		this.JacketLining     = ko.observable(that.dsRegistry.getDatasource('liningFabricDS').getStore()[0]);
		this.JacketLining.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLining = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		//JacketButtonColor
		this.JacketButtonColorList = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore());
		this.JacketButtonColor     = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
		this.JacketButtonColor.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketButtonColor = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		//JacketThreadColor
		this.JacketThreadColorList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
		this.JacketThreadColor     = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketThreadColor.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketThreadColor = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});


//JacketMonogram 
		this.JacketMonogram = ko.observable('');
        this.JacketMonogram.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketMonogram = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		this.JacketMonogramExtraLine = ko.observable('');
        this.JacketMonogramExtraLine.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketMonogramExtraLine = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		this.JacketDefaultMonogramHeader = ko.observable(true);
		this.JacketDefaultMonogramHeader.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketDefaultMonogramHeader = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketCustomMonogramHeader = ko.observable('');
		this.JacketCustomMonogramHeader.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomMonogramHeader = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		this.JacketMonogramStitchDifferentFromPiping = ko.observable(false);
		this.JacketMonogramStitchDifferentFromPiping.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketMonogramStitchDifferentFromPiping = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		this.JacketMonogramStitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketMonogramStitchColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketMonogramStitchColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		this.JacketCustomMonogram = ko.observable(false);
		this.JacketCustomMonogram.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomMonogram = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		
		this.JacketCustomMonogramImage = ko.observable('');
		this.JacketCustomMonogramImage.subscribe(function(data) { 
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomMonogramImage = json;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketCustomMonogramNotes = ko.observable('');
		this.JacketCustomMonogramNotes.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketCustomMonogramNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		
		
		this.JacketEmbroidery = ko.observable(false);
		this.JacketEmbroidery.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketEmbroidery = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		
		this.JacketEmbroideryImage = ko.observable('');
		this.JacketEmbroideryImage.subscribe(function(data) {
			try{
				var json = eval("(" + data + ")");
			}catch(e){
				var json = data;
			}
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketEmbroideryImage = json;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.JacketEmbroideryNotes = ko.observable(false);
		this.JacketEmbroideryNotes.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketEmbroideryNotes = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});

/*
		//JacketLiningCode
		this.JacketLiningCode = ko.observable('');
		this.JacketLiningCode.subscribe(function(data) {
                        that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketLiningCode = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
*/	
		
/*
                //suitPipingCode
		this.JacketPipingCode = ko.observable('');
		this.JacketPipingCode.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketPipingCode = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
*/
		//suitPipingCode


              //suitTopStitchCode
		//this.JacketTopStitchCode = ko.observable('');
		this.JacketTopStitchCode = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
		this.JacketTopStitchCode.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketTopStitchCode = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});

		
		this.JacketContrastTrimmingColour = ko.observable('');//(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketContrastTrimmingColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketContrastTrimmingColour = data;			
			that.flushModel();
			that.JacketStepDCompletion();
		});
		

		this.JacketFitList = ko.observableArray(["Fitted", "Semi Fitted", "Standard Fit"]);//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore());
		this.JacketFit = ko.observable('Semi Fitted');//ko.observable(that.dsRegistry.getDatasource('fitDS').getStore()[1].title);
        this.JacketFit.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketFit = data;     		
			that.flushModel();
			that.JacketStepDCompletion();
		});


		this.urgent = ko.observable(0);
		this.urgent.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].urgent = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		/*
		this.urgentDate = ko.observable('');
		this.urgentDate.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].urgentDate = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		*/
		this.DOP_day = ko.observable('');
		this.DOP_day.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].DOP_day = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.DOP_month = ko.observable('');
		this.DOP_month.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].DOP_month = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		this.DOP_date = ko.observable('');
		this.DOP_date.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].DOP_date = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});		

//////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*
		this.JacketButtonContrast = ko.observable(false);
		this.JacketButtonContrast.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketButtonContrast = data;
			that.flushModel();
			that.JacketStepBCompletion();
		});
		*/
		
		this.JacketSleeveButton1Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.JacketSleeveButton1Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton1Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButton2Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.JacketSleeveButton2Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton2Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButton3Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.JacketSleeveButton3Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton3Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButton4Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.JacketSleeveButton4Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton4Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});				
		this.JacketSleeveButton5Colour = ko.observable(that.dsRegistry.getDatasource('vestButtonsDS').getStore()[0]);
        this.JacketSleeveButton5Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton5Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
			
		this.JacketbuttonholeList = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore());
	/*	this.Jacketbuttonhole     = ko.observable(that.dsRegistry.getDatasource('buttonholethreadDS').getStore()[0]);
		this.Jacketbuttonhole.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].Jacketbuttonhole = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
*/
		this.JacketSleeveButtonHole1Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButtonHole1Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonHole1Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButtonHole2Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButtonHole2Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonHole2Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButtonHole3Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButtonHole3Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonHole3Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButtonHole4Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButtonHole4Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonHole4Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});				
		this.JacketSleeveButtonHole5Colour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButtonHole5Colour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButtonHole5Colour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		this.JacketSleeveButton1StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButton1StitchColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton1StitchColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButton2StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButton2StitchColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton2StitchColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButton3StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButton3StitchColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton3StitchColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		this.JacketSleeveButton4StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButton4StitchColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton4StitchColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});				
		this.JacketSleeveButton5StitchColour = ko.observable(that.dsRegistry.getDatasource('pipingColorDS').getStore()[0]);
        this.JacketSleeveButton5StitchColour.subscribe(function(data) {
			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketSleeveButton5StitchColour = data;
			that.flushModel();
			that.JacketStepDCompletion();
		});
		
		this.checkVisibility = ko.observable(true);
		this.checkVisibility.subscribe(function(data) {
            if(that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBottomStyle.id == 0){
				that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].checkVisibility = false;
			}else if( that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketJacketStructure.id == 'Double_Breasted' ||  
					  that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketJacketStructure.id == 'Double_Breasted_One_to_close' ||  
					  that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketJacketStructure.id == 'Double_Breasted_six_Buttons' 
					  ){
                                             		if(that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBottomStyle.id === 1 || 
                                             			that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].JacketBottomStyle.id === 2
                                             			){
                                                    	that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].checkVisibility = false;
                                                    }else{
                                                    	that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].checkVisibility = true;
                                                	}
             }else{
             	that.JacketData[ that.getRow(that.selectedVariantJacket().id)  ].checkVisibility = true;
             }   
                                             
		});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		this.PreviewVisible = ko.observable('front');

		this.JacketFabricSelect = function() {
  //customAlert("11 select shuit" +  this.getRow(this.selectedVariantJacket().id)); 
			that.dsRegistry.setContext({
				targetModel:     'garmentsJacketDS',
				targetGarment:   'Jacket',
				targetDelta:      this.getRow(this.selectedVariantJacket().id),
				targetAttribute: 'JacketFabric',
				callbackPage:    '#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsJacketDS').getStore().Jacket[that.getRow(that.selectedVariantJacket().id)].JacketFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};

		this.JacketContrastFabricSelect = function() {
                         // customAlert("11 shuit");
			that.dsRegistry.setContext({
				targetModel:     'garmentsJacketDS',
				targetGarment:   'Jacket',
				targetDelta:      this.getRow(this.selectedVariantJacket().id),
				targetAttribute: 'JacketFabric',
				callbackPage:    '#garmentsVestFabric'
			});

			var fab = that.dsRegistry.getDatasource('garmentsJacketDS').getStore().Jacket[that.getRow(that.selectedVariantJacket().id)].JacketFabric;
			that.fabricsVM.setSelectedFabric(fab);
		};


	this.fnCloneJacket = function(e) {
				fe = this;
				var parentOffset = $('.content-wrapper').offset(); 
				//or $(this).offset(); if you really just want the current element's offset
				relX = e.pageX - parentOffset.left;
				relY = e.pageY - parentOffset.top;

				//need testing
				if (relY > 500) relY -= 200;
				targetAttr = $(this).parent().parent().parent().attr('data-target-attr');
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().parent().attr('data-target-attr');	
				}	
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().attr('data-target-attr');	
				}	

				currentVariant = that.selectedVariantJacket();
				var vnames = that.variantNameJacket(); 
				if (vnames.length == 1) return;

				var fa  = 'Select Jacket changes! ';
			    fa += '<div class="cloneDialogBtnClose" data-role="button"><img src="http://shirt-tailor.net/thepos/appimg/template/topmenu/close.png"/></div><div  data-role="fieldcontain"><fieldset data-role="controlgroup">';
				for (var ind in vnames) {
					//if (vnames[ind].id != currentVariant.id) {
						check = '';
						if (vnames[ind].id == currentVariant.id) check = ' checked="checked" disabled ';
						fa += '<input type="checkbox" '+ check +' data-id="' + vnames[ind].id + '" id="cSuit-'+ vnames[ind].id +'" name="cSuit-'+ vnames[ind].id +'"/>'
						fa += '<label for ="cSuit-'+ vnames[ind].id +'">' + vnames[ind].title + '</label>';
					//}
				}
				fa += "</fieldset></div><div class='cloneDialogBtn' data-role='button'>Apply</div>";
				$('.cloneDialog').remove();
				$('.content-wrapper').append("<div class='cloneDialog' id='cloneSuitDialog' style='left: " + relX + "px !important; top: " + relY + "px !important;'>" + fa + "</div>");
				
				$('#cloneSuitDialog input').checkboxradio();
				$('.cloneDialogBtn').on('click', function() {
					cI = [];
					$('#cloneSuitDialog :checked').each(function(thet,c) {cI.push($(c).attr('data-id'));});
					that.cloneJacketAttributes(currentVariant.id, targetAttr, cI);
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
					//$.mobile.sdCurrentDialog.close();
				});
				$('.cloneDialogBtnClose').on('click', function() {
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
				});
		}


	this.attachCloneJacketAttribute = function() {
	//		$('#garmentsSuit #textCloneBtn1').on('click', that.fnCloneSuitkk);
	//		$('#garmentsJacket .textCloneBtn').on('click', that.fnCloneJacket);
	//		$('#garmentsJacket .gAttr img').on('click', that.fnCloneJacket);
	//		$('#garmentsJacket .fbSelectBtn').on('click', that.fnCloneJacket);
	//		$('#garmentsJacket .gAttr .mycheck').on('click', that.fnCloneJacket);			
	//		$('#garmentsJacket .selectlist').on('change', that.fnCloneJacket);
		};


	this.fnCloneSuitkk = function(e) {
    
   // customAlert("yykkkky");
     //  customAlert("!!!!lll!!!!!!!!!!!!!!");
                        fe = this;
				var parentOffset = $('.content-wrapper').offset(); 
				//or $(this).offset(); if you really just want the current element's offset
				relX = e.pageX - parentOffset.left;
				relY = e.pageY - parentOffset.top;

				//need testing
				if (relY > 500) relY -= 200;
				targetAttr = $(this).parent().parent().parent().attr('data-target-attr');
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().parent().attr('data-target-attr');	
				}	
				if (typeof(targetAttr) == "undefined") {
					targetAttr = $(this).parent().attr('data-target-attr');	
				}	

				currentVariant = that.selectedVariantJacket();
				var vnames = that.variantNameJacket(); 
				if (vnames.length == 1) return;

				var fa  = 'Select to which Jacket to clone! ';
			    fa += '<div class="cloneDialogBtnClose" data-role="button"><img src="http://shirt-tailor.net/thepos/appimg/template/topmenu/close.png"/></div><div  data-role="fieldcontain"><fieldset data-role="controlgroup">';
				for (var ind in vnames) {
					//if (vnames[ind].id != currentVariant.id) {
						check = '';
						if (vnames[ind].id == currentVariant.id) check = ' checked="checked" ';
						fa += '<input type="checkbox" '+ check +' data-id="' + vnames[ind].id + '" id="cSuit-'+ vnames[ind].id +'" name="cSuit-'+ vnames[ind].id +'"/>'
						fa += '<label for ="cSuit-'+ vnames[ind].id +'">' + vnames[ind].title + '</label>';
					//}
				}
				fa += "</fieldset></div><div class='cloneDialogBtn' data-role='button'>Clone</div>";
				$('.cloneDialog').remove();
				$('.content-wrapper').append("<div class='cloneDialog' id='cloneSuitDialog' style='left: " + relX + "px !important; top: " + relY + "px !important;'>" + fa + "</div>");
				
				$('#cloneSuitDialog input').checkboxradio();
				$('.cloneDialogBtn').on('click', function() {
					cI = [];
					$('#cloneSuitDialog :checked').each(function(thet,c) {cI.push($(c).attr('data-id'));});
					that.cloneJacketAttributes(currentVariant.id, targetAttr, cI);
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
					//$.mobile.sdCurrentDialog.close();
				});
				$('.cloneDialogBtnClose').on('click', function() {
					$('#cloneSuitDialog').fadeOut('fast', function()  { $('.cloneDialog').remove(); });
				});
}


		this.cloneJacketAttributes = function( pvId, pvTA, cvI)
		{
			var tAD = that.getVariant(pvId)[pvTA];
			for (var ind in that.JacketData) {
				if ( cvI.indexOf(that.JacketData[ind].variantId.toString()) != -1) {
					//console.log('cloning to ' + that.suitData[ind].variantId);
					that.JacketData[ind][pvTA] = tAD; 
				}
			}
			that.flushModel();
		};



		this.deleteVariantJacket = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							var currentcount = orderItem.garmentsList()[0].count();
							orderItem.garmentsList()[0].count( currentcount - 1);
							
							var tJacketData = [];
							var tvarsData = [];
							for (var ind in that.variantNameJacket() ) {
								if ( that.variantNameJacket()[ind].id != data.id ) {
									tvarsData.push(that.variantNameJacket()[ind]);
									tJacketData.push(that.getVariant( that.variantNameJacket()[ind].id ));
								}
							}
							that.JacketData = tJacketData;
							that.flushModel();
							that.variantNameJacket(tvarsData);
							that.selectedVariantJacket(that.variantNameJacket()[0]);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};
		
		this.deleteVariantJacket2 = function(data) {
			$('<div>').simpledialog2({
				mode: 'button',
				headerText: 'Click One...',
				headerClose: true,
				buttonPrompt: 'Really delete ' + data.title,
				buttons : {
					'OK': {
						click: function () {
							var currentcount = orderItem.garmentsList()[0].count();
							orderItem.garmentsList()[0].count( currentcount - 1);
							
							var tJacketData = [];
							var tvarsData = [];
							for (var ind in that.variantNameJacket() ) {
								if ( that.variantNameJacket()[ind].id != data.id ) {
									tvarsData.push(that.variantNameJacket()[ind]);
									tJacketData.push(that.getVariant( that.variantNameJacket()[ind].id ));
								}
								else{
									orderItem.removeFabric('Jacket', 0, 'Jacket ' + (that.JacketData[ ind ].variantId + 1), that.JacketData[ ind ].JacketCustomerFabric, that.JacketData[ ind ].variantId, 2.5);
								}
							}
							that.JacketData = tJacketData;
							that.flushModel();
							that.variantNameJacket(tvarsData);
							that.selectedVariantJacket(that.variantNameJacket()[0]);
							
							var spinner = document.getElementById('loading_jp');
							spinner.style.display = "block";
							//posChangePage('#bodyshape');
							//posChangePage('#orderItemSelectGarment');
							orderItem.prepareGarments();
							orderItem.populateGarments();
							setTimeout(function() { var spinner = document.getElementById('loading_jp'); spinner.style.display = "none"; },1000);
						}
					},
					'Cancel': {
						click: function () {},
						icon: "delete",
						theme: "c"
					}
				}
			});
		};


		this.compareFunction = function(data1, data2) {
			if(JSON.stringify(data1) == JSON.stringify(data2)){
				return true;
			}else{
				return false;
			}
		};
		
		this.toggle_visibility_array = function (id, index) {		
			var e = document.getElementsByName(id); 
			for(var x = 0; x < e.length; x++){
				if(x != index){
					e[x].style.display = "none";	
				}
			}
			if(e[index].style.display == 'block'){
			   	e[index].style.display = 'none';
			}else{
				e[index].style.display = 'block';
			}	
		};
	
	/*
		this.jacketinitializedivs = function(){
			var mainlistelements = document.getElementsByClassName("MainListElement");
			var garmentelements = document.getElementsByClassName("triggerr");
			var checkboxes = document.getElementsByClassName("listsimpleclick");
			var garmentsnumber = garmentelements.length/mainlistelements.length;
			
console.log("mainlistelements FROM JS: " +mainlistelements.length);
console.log("garmentelements: " + garmentelements.length);
console.log("checkboxes: " + checkboxes.length);
console.log("garmentsnumber: " + garmentsnumber);
 
			for(var a = 0; a < mainlistelements.length; a++){
				var haschecked = false;
				for(var b = 0; b < garmentsnumber; b++){
					var index = a*garmentsnumber + b;		
					if(checkboxes[index].classList.contains('selecteds')){					
						mainlistelements[a].classList.add('activate');
						garmentelements[index].classList.add('showtrigger');
					}else{
						garmentelements[index].classList.add('offtrigger');
					}
				}
			}
			return false;		
		};
		*/
				
				
		
		ko.bindingHandlers.modifiedJacketListOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var wantedValue = value[1];	
					var list = document.getElementsByName(value[2]);
					var checkboxes = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;
					var elemdivs = document.getElementsByName(value[5]);

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
						if(wantedValue.id != undefined){
							checkValue = wantedValue.id; 
						}else{
							checkValue = wantedValue;
						}	
						if(spanvalue == checkValue){
							checkboxes[ index ].classList.add('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('showtrigger');
								elemdivs[ index ].classList.remove('offtrigger');
							}	
						}else{
							checkboxes[ index ].classList.remove('selecteds');
							if(elemdivs != undefined && elemdivs.length > 0){
								elemdivs[ index ].classList.add('offtrigger');
								elemdivs[ index ].classList.remove('showtrigger');
							}	
						}	
					}
					
					if(value[4] != 'none'){
						//var customdivs = document.getElementsByName(value[4]);
						//if(customdivs[value[0]] != undefined){
							if(wantedValue.id == '123'){
								that.JacketData[value[0]].custom.addCustomInfo(value[7]);
								that.JacketData[value[0]].custom.custom0 = 100;
								that.customCount(that.customCount() + 1);
								//customdivs[value[0]].style.display = 'block';
							}else{
								if(that.JacketData[value[0]].custom.removeCustomInfo(value[7])){
									that.customCount(that.customCount() - 1);
								}
								//customdivs[value[0]].style.display = 'none';
							}
						//}	
					}
					
					if(value[6] != undefined && value[6] != 'none'){
						var mainoptionsdivs = document.getElementsByName(value[6]);
						var mainoptionscount = mainoptionsdivs.length;
						
						for(var a = 0; a < mainoptionscount; a++){
							var haschecked = false;
							for(var b = 0; b < garmentscount; b++){
								var index = a*garmentscount + b;
								if(checkboxes[index].classList.contains('selecteds')){
									haschecked = true;
									break;
								}
							}
							if(haschecked == true){
								mainoptionsdivs[a].classList.add('activate');
							}else{
								mainoptionsdivs[a].classList.remove('activate');
							}
						}
					}
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
					
		        });
		    }
		};    

		ko.bindingHandlers.modifiedJacketListBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( !checkboxes[ index ].classList.contains(value[3]) ){
							checkboxes[ index ].classList.add(value[3]);
						}else{
							checkboxes[ index ].classList.remove(value[3]);						
						}	
					}
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);					
		        });
		    }
		};   


		ko.bindingHandlers.modifiedJacketBooleanOptions = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var checkboxes = document.getElementsByName(value[1]);
					var index = value[0];
					var cssclass = "" + value[2];

					if( !checkboxes[ index ].classList.contains(cssclass) ){
						checkboxes[ index ].classList.add(cssclass);
					}else{
						checkboxes[ index ].classList.remove(cssclass);
					}	
					
					if(value[1] == 'JacketCoverButtons'){
						if(that.JacketData[ index ].JacketCoverButtons == true && that.JacketData[ index ].JacketDinner == true){
							document.getElementsByName(value[3])[index].style.display = "block";
						}else{
							document.getElementsByName(value[3])[index].style.display = "none";
						}
					}
					
					if(value[1] == 'JacketKissingButtons'){
						if(that.JacketData[ index ].JacketKissingButtons == true){
							document.getElementsByName(value[3])[index].style.display = "block";
						}else{
							document.getElementsByName(value[3])[index].style.display = "none";
						}
					}
					
					if( value[1] != 'FabricCheck' ){
						that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
					}	
		        });
		    }
		};


		ko.bindingHandlers.switchpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);

					for(var x = 0; x < popups.length; x++){
						if(x != value[0]){
							popups[x].style.display = "none";	
						}
					}
					if(popups[value[0]].style.display == "block"){
						popups[value[0]].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}else{
						popups[value[0]].style.display = "block";		
					}

		        });
		    }
		}; 
		

		ko.bindingHandlers.jacketswitchcustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
					
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.JacketData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[1]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		}; 	
		
		ko.bindingHandlers.jacketcustomMonogramEmbroideryPopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					var tmp = that.selectedVariantJacket();
					that.selectedVariantJacket(tmp);
					try{		
						if(value[1] != undefined && value[2] != undefined){
							var link = "";		
							if(value[1].indexOf("Monogram") !=-1 ){						
								link = BUrl + that.JacketCustomMonogramImage().image;
							}else if(value[1].indexOf("Embroidery") !=-1 ){
								link = BUrl + that.JacketEmbroideryImage().image;
							}
							if(link != ""){
								document.getElementById(value[1]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
							}	
						}
					}catch(e){
						;
					}	
						
					var popup = document.getElementsByName(value[0]);
					if(popup[0].style.display == "block"){
						popup[0].style.display = "none";
					}else{
						popup[0].style.display = "block";		
					}

		        });
		    }
		};		
			
		
		ko.bindingHandlers.jacketopencustompopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
					
					try{		
						if(value[2] != undefined && value[3] != undefined){								
							var link = BUrl + that.JacketData[ value[0] ][value[3] ] .image;						
							document.getElementById(value[2]).innerHTML = ['<img src="', link ,'"  width="180"/>'].join('');
						}						
					}catch(e){
						;
					}
					
					var popup = document.getElementsByName(value[1]);
					popup[0].style.display = "block";		
		        });
		    }
		}; 		
		

		ko.bindingHandlers.openpopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[1]);			
					popups[value[0]].style.display = "block";		//.show(), .dialog( "open" ) : not working
		        });
		    }
		}; 

		ko.bindingHandlers.closepopup = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					var popups = document.getElementsByName(value[0]);
					for(var x = 0; x < popups.length; x++){			
						popups[x].style.display = "none";		//.show(), .dialog( "open" ) : not working
					}	
		        });
		    }
		}; 


		ko.bindingHandlers.modifiedJacketThreads = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var images = document.getElementsByName(value[3]);
					var texts = document.getElementsByName(value[3] + "Text");
					var options = document.getElementsByName(value[4]);
					images[value[0]].src = value[2].image;
					if(texts[value[0]] != undefined){
						texts[value[0]].innerHTML = value[2].title;
					}	

					var garmentssize = options.length/images.length;
					for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					
						// FOR LINING OPTIONS
						if(value[5] != undefined && value[6] == undefined /*&& value[7] != undefined && value[8] != undefined && value[9] != undefined && value[10] != undefined*/){
							document.getElementsByName(value[5])[x].classList.remove('selected');	
						}
					}
					options[garmentssize*value[0] + value[1]].classList.add('selected');
					
					// FOR LINING OPTIONS
					if(value[5] != undefined && value[6] == undefined /*&& value[7] != undefined && value[8] != undefined && value[9] != undefined && value[10] != undefined*/){
						document.getElementsByName(value[5])[garmentssize*value[0] + value[1]].classList.add('selected');	
					}
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
					
					if(value[5] != undefined && value[6] != undefined && value[7] != undefined && value[8] != undefined && value[9] != undefined && value[10] != undefined){
						if(that.JacketInsidePocketsDifferentFromPiping() == false){
							var images2 = document.getElementsByName(value[5]);
							var texts2 = document.getElementsByName(value[6]);
							var options2 = document.getElementsByName(value[7]);
							images2[value[0]].src = value[2].image;
							texts2[value[0]].innerHTML = value[2].title;
							var garmentssize = options2.length/images2.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options2[x].classList.remove('selected');	
							}
							options2[garmentssize*value[0] + value[1]].classList.add('selected');
						}/*
						if(that.JacketMonogramStitchDifferentFromPiping() == false){
							var images3 = document.getElementsByName(value[8]);
							var texts3 = document.getElementsByName(value[9]);
							var options3 = document.getElementsByName(value[10]);
							images3[value[0]].src = value[2].image;
							texts3[value[0]].innerHTML = value[2].title;
							var garmentssize = options3.length/images3.length;
							for(var x = garmentssize*value[0]; x < garmentssize*(value[0] + 1); x++ ){
								options3[x].classList.remove('selected');	
							}
							options3[garmentssize*value[0] + value[1]].classList.add('selected');
						}	*/	
					}
		        });
		    }
		}; 
		
		ko.bindingHandlers.modifiedJacketButtonNumber = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
					//	[$parentContext.$index(), $index(), $data, 'suitJacketSleeveButtonNumber', 'suitJacketSleeveButtonNumberList']
					var maindivs = document.getElementsByName(value[3]);
					var options = document.getElementsByName(value[4]);
					
					//var garmentssize = maindivs.length;
					var optionsnumber = options.length / maindivs.length; 
					for(var x = optionsnumber*value[0]; x < optionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[optionsnumber*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
					
		        });
		    }
		}; 			
		
		
		ko.bindingHandlers.modifiedJacketColors = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	

					var listdivs = document.getElementsByName(value[3]);					
					var listimages = document.getElementsByName(value[4]);
					var listtexts = document.getElementsByName(value[5]);
					var options = document.getElementsByName(value[6]);
					var garmentssize = document.getElementById(value[7]).innerHTML;
					var jacketoptionsnumber = listdivs.length/garmentssize;
					var coloroptionsnumber = options.length/garmentssize;
					
					for(var x = 0; x < jacketoptionsnumber; x++){
						var y = value[0] + x*garmentssize;
						if(listimages.length > 0){	 				
							listimages[y].src = value[2].image;
						}
						if(listtexts.length > 0){
							listtexts[y].innerHTML = value[2].title;
						}
					}					
					for(var x = coloroptionsnumber*value[0]; x < coloroptionsnumber*(value[0] + 1); x++ ){
						options[x].classList.remove('selected');	
					}
					options[coloroptionsnumber*value[0] + value[1]].classList.add('selected');
					
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);	
		        });
		        
		    }
		};		
		
		
		ko.bindingHandlers.modifiedJacketFit = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();
					for(var x = 0; x < that.JacketData.length; x++){
						that.JacketData[ x ].JacketFit = value;	
					}
					that.flushModel();
		        });
		    }
		}; 		
		

		ko.bindingHandlers.modifiedJacketDropdown = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  	   		        	      	
					var value = valueAccessor();	
					that.selectedVariantJacket(that.variantNameJacket()[value[0]]);
		        });
		    }
		}; 
		
		/*
		ko.bindingHandlers.jacketslider = {
		    init: function(element, valueAccessor, allBindingsAccessor) {
		        //initialize the control
		        var options = allBindingsAccessor().jacketslideroptions || {};
		        $(element).slider(options);
		
		        //handle the value changing in the UI
		        ko.utils.registerEventHandler(element, "slidechange", function() {
		            //would need to do some more work here, if you want to bind against non-observables
		            var values = valueAccessor(); 
		            var observable = values[0];
		            observable($(element).slider("value"));
		        });
		
		    },
		    //handle the model value changing
		    update: function(element, valueAccessor) {
		    	var values = valueAccessor();
		        var value = ko.utils.unwrapObservable(values[0]);
		       // $(element).slider("value", value);
				var maindivs = document.getElementsByName(values[1]);
				var alltexts = document.getElementsByName(values[2]);
				var garmentscount = alltexts.length/maindivs.length;
				var optionscount = maindivs.length;
				
				for(var a = 0; a < maindivs.length; a++){
					for(var b = 0; b < garmentscount; b++){
						var index = a*garmentscount + b;						
						alltexts[index].innerHTML = that.JacketData[ b ].JacketLapelWidth; 
					}	
				}
				
				try{
					$(element).val(value).slider('refresh');
				}catch(e){
					;
				} 

		    }
		};		*/
		
		
		ko.bindingHandlers.modifiedJacketLapelWidth = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var values = valueAccessor();	
					var maindivs = document.getElementsByName(values[1]);
					var alltexts = document.getElementsByName(values[2]);
					var garmentscount = alltexts.length/maindivs.length;
					var optionscount = maindivs.length;
					
					for(var a = 0; a < maindivs.length; a++){
						for(var b = 0; b < garmentscount; b++){
							var index = a*garmentscount + b;						
							alltexts[index].innerHTML = that.JacketData[ b ].JacketLapelWidth; 
						}	
					}
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.modifyClassOfDiv = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {	        	  		        	      	
					var value = valueAccessor();
					var index = value[0];
					var divs = document.getElementsByName(value[1]);
					var classname = 'activated'; //value[2];
					var activeclassname = 'activate';//value[3];
					//var notactiveclassname = value[4];
					
					var addtheclass = true;
					if( divs[ index ].classList.contains(classname) ){
						addtheclass = false;
					}
					for(var x = 0; x < divs.length; x++){
						divs[ x ].classList.remove(classname);
					}		
					if(addtheclass == true){			
						divs[ index ].classList.add(classname);
					}	
		        });
		    }
		}; 		
		
		
		ko.bindingHandlers.jacketFabricApplier = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
			//		for(var x = 0; x < value.length; x++){
			//			console.log("VALUE " + x + ": " + JSON.stringify(value[x]) );
			//		}
					
					var checkboxes = document.getElementsByName(value[0]);
					var texts = document.getElementsByName(value[3]);
					var images = document.getElementsByName(value[4]);
					var cssclass = "" + value[2]; 
					var tomakegreenelements = document.getElementsByClassName("tomakegreen");

					var garmentsnumber = checkboxes.length;
					var textsnumber = texts.length;
					var differenttypesnumber = textsnumber/(garmentsnumber*2);
					
					var anychecked = false;
					
					for(var x = 0; x < checkboxes.length; x++){
						if( checkboxes[x].classList.contains(cssclass)){
							orderItem.removeFabric('Jacket', 0, 'Jacket ' + (x + 1), that.JacketData[ x ].JacketCustomerFabric, that.JacketData[ x ].variantId, 2.5);
							
							
							if(!orderItem.ClientFabric() && document.getElementById("selectedfabric").innerHTML != "none"){
								checkboxes[x].classList.remove(cssclass);
								tomakegreenelements[x].classList.add('makegreen');
								anychecked = true;
								that.JacketData[ x ].JacketCustomerFabric = {};
								that.JacketData[ x ].JacketHasCustomerFabric = false;
								that.JacketData[ x ].JacketFabric = value[1];
								
								if(texts.length > 0){
								//texts[x].innerHTML = value[1].title;
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = value[1].title;
											texts[pos].style.display = "block";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = '';
											texts[pos+1].style.display = "none";
										}
									}
								}
								if(images.length > 0){
									images[x].src = value[1].fabricImage;
								}
							}
							else if(orderItem.ClientFabric()) {
								if(orderItem.addFabricToGarment(that.JacketData[ x ], 'Jacket', 0,'Jacket ' + (x + 1), 2.5)){
									checkboxes[x].classList.remove(cssclass);
									tomakegreenelements[x].classList.add('makegreen');
									anychecked = true;
									that.JacketData[ x ].JacketFabric = dsRegistry.getDatasource('fabricsDS').getStore()[1];
									that.JacketData[ x ].JacketCustomerFabric = orderItem.SelectedCustomerFabric();
									that.JacketData[ x ].JacketHasCustomerFabric = true;
									
									for(var y = 0; y < differenttypesnumber; y++){
										var pos = y*garmentsnumber*2 + x*2;
										if(texts[pos]){
											texts[pos].innerHTML = '';
											texts[pos].style.display = "none";
										}
										if(texts[pos+1]){
											texts[pos+1].innerHTML = orderItem.SelectedCustomerFabric().FabricCode;
											texts[pos+1].style.display = "block";
										}
									}
								}
								else if(that.JacketData[ x ].JacketCustomerFabric.FabricCode){
									var temp = orderItem.SelectedCustomerFabric();
									orderItem.SelectedCustomerFabric(that.JacketData[ x ].JacketCustomerFabric);
									orderItem.addFabricToGarment(that.JacketData[ x ], 'Jacket', 0,'Jacket ' + (x + 1), 2.5)
									orderItem.SelectedCustomerFabric(temp);
								}
							}
							
						}
					}
					if(anychecked == true){
						document.getElementById("selectedfabric").innerHTML = "none";
						document.getElementById("fabricrange").innerHTML = "0";
						document.getElementById("composition").innerHTML = "0";
						document.getElementById("fabricinput").value = "";
						document.getElementById("selectedfabricimage").src = "http://shirt-tailor.net/thepos/uploaded/fabrics/none.png";
						document.getElementById("fabricinfo").style.display = "none";
						orderItem.ClientFabric(false);
						orderItem.SelectedCustomerFabric({});
					}
		        });
		    }
		}; 
		
		
		
		ko.bindingHandlers.modifiedJacketListBooleanDependencies = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var value = valueAccessor();	
	
					var list = document.getElementsByName(value[1]);
					var checkboxes = document.getElementsByName(value[2]);
					var divs = document.getElementsByName(value[3]);
					var garmentscount = checkboxes.length/list.length;

					for(var x = 0 ; x < list.length; x++){
						var spanvalue = list[x].innerHTML;					
						var index = x*garmentscount + value[0];
						var checkValue = '';
							
						if( checkboxes[ index ].classList.contains(value[4]) ){
							divs[ index ].classList.remove(value[5]);
						}else{
							divs[ index ].classList.add(value[5]);						
						}	
					}
								
		        });
		    }
		};   		
		

		this.jacketDinnerTrimmingDependencies = function( value0, value1 ){
			var TrimmingMain = document.getElementsByName(value0);
			var TrimmingElement = document.getElementsByName(value1);
			var garmentsnumber = TrimmingElement.length;
			
			var anydinner = false;
			for(var x = 0; x < garmentsnumber; x++){
				if(that.JacketData[x].JacketDinner == true){							
					anydinner = true;
					TrimmingElement[x].style.display = "block";
				}else{
					TrimmingElement[x].style.display = "none";
				}
			}
			if(anydinner == true){
				TrimmingMain[0].style.display = "block";
			}else{
				TrimmingMain[0].style.display = "none";
			}
			return false;
		};
		
		
		this.JacketButtonsBottomDependencies = function( value0, value1 ){			
			var bottomsList = document.getElementsByName(value0);
			var BottomsEachGarment = document.getElementsByName(value1);
			var garmentsnumber = BottomsEachGarment.length/bottomsList.length;
					
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < bottomsList.length; y++){
					var pos = y*garmentsnumber + x;
					if(pos < garmentsnumber*2){
						if(	that.JacketData[ x ].JacketJacketStructure.title.indexOf("Double Breasted") !=-1 || that.JacketData[ x ].JacketJacketStructure.title.indexOf("Double breasted") !=-1 ){			
							BottomsEachGarment[pos].style.display = "none";
						}else{
							BottomsEachGarment[pos].style.display = "";
						}		 
					}	
				}
			}					
			return false;
		}; 	
		
		this.JacketButtonsLapelDependencies = function( value0, value1 ){			
			var lapelsList = document.getElementsByName(value0);
			var lapelsEachGarment = document.getElementsByName(value1);
			var garmentsnumber = lapelsEachGarment.length/lapelsList.length;
					
			for(var x = 0; x < garmentsnumber; x++){
				for(var y = 0; y < lapelsList.length; y++){
					var pos = y*garmentsnumber + x;		
					if(pos == garmentsnumber*1 + x || pos == garmentsnumber*3 + x){						
						if(	that.JacketData[ x ].JacketJacketStructure.title.indexOf("Double Breasted") !=-1 || that.JacketData[ x ].JacketJacketStructure.title.indexOf("Double breasted") !=-1 ){
							lapelsEachGarment[pos].style.display = "none";
						}else{
							lapelsEachGarment[pos].style.display = "";
						}		 
					}	
				}
			}					
			return false;
		}; 			


		ko.bindingHandlers.jacketCloneMonogram = {
		    update: function(element, valueAccessor) {
		        ko.utils.registerEventHandler(element, "click", function() {  		        	      	
					var values = valueAccessor();
					for(var x = 0; x < that.JacketData.length; x++){
						that.JacketData[ x ].JacketMonogram = values[0];
						that.JacketData[ x ].JacketDefaultMonogramHeader = values[1];
						that.JacketData[ x ].JacketCustomMonogramHeader = values[2];
						//that.JacketData[ x ].JacketMonogramNotes = values[3];
						that.JacketData[ x ].JacketMonogramExtraLine = values[4];
						//that.JacketData[ x ].JacketMonogramStitchColour = values[5]; 
					}
		        });
		    }
		}; 	


	},
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
/*
	nextStep: function() {
		var tWork = this.workflow();
		for (var ind in tWork ) {
				if (!tWork[ind].completed) {
				this.currentStep( tWork[ind] );
				return;
			}
		}
	},*/

	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"Jacket"  : this.JacketData
		}, true);
	},

	selectVariantJacket: function() {
		this.renderJacket();
	},

	getVariant: function(id) {
		var toreturn = this.JacketData[0];
		for (var ind in this.JacketData) {
			if ( this.JacketData[ind].variantId == id  ) {
				toreturn = this.JacketData[ind];
			}
		}
		return toreturn;

	},

	getRow: function(id) {
		for (var ind in this.JacketData) {
			if ( this.JacketData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},

	addVariantJacket: function() {
		//var vname = "Jacket " + (this.variantNameJacket().length + 1 );
		this.JacketDataAID += 1;
		var newid = this.JacketDataAID + 1;
		var vname = "Jacket " + newid;

		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantJacket().id )  ); //CLONE Object
		this.variantNameJacket.push({title: vname, id: this.JacketDataAID});
		
		tObj.custom = new CustomProperties();
		tObj.variantId = this.JacketDataAID;
		if(orderItem.jacketfirsttime == true){
			this.JacketData.push(tObj);                                                       //Push to internal
		}	
		this.flushModel();
	},

	addVariantJacket2: function() {

		var currentcount = orderItem.garmentsList()[0].count();
		orderItem.garmentsList()[0].count( currentcount + 1);
		//var vname = "Jacket " + (this.variantNameJacket().length + 1 );
		this.JacketDataAID += 1;
		var newid = this.JacketDataAID + 1;
		var vname = "Jacket " + newid;
		
		var tObj = jQuery.extend(true, {}, this.getVariant( this.selectedVariantJacket().id )  ); //CLONE Object
		this.variantNameJacket.push({title: vname, id: this.JacketDataAID});
        //        customAlert(this.variantNameJacket.length + " -- " +  this.JacketDataAID + " ---" + this.variantNameJacket().length);
		tObj.variantId = this.JacketDataAID;
		tObj.custom = new CustomProperties();
		this.JacketData.push(tObj);                                                       //Push to internal
		this.flushModel();
		this.currentStep(this.workflow()[0] );
		var lastelemindex = this.variantNameJacket().length -1;
		this.selectedVariantJacket( this.variantNameJacket()[lastelemindex] );
	},

	digestData: function(data) {
		this.JacketData  = data.Jacket;
		this.renderView();

	},

	renderView: function() {
		this.renderJacket();
	},

	renderJacket: function() {
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantJacket().id);
			if (tData != null) {
	
				//Update observables
				if (typeof(tData.checkVisibility)    != "undefined") {this.checkVisibility(tData.checkVisibility);}
				if (typeof(tData.JacketNotes)    != "undefined") {this.JacketNotes(tData.JacketNotes);}
		//		if (typeof(tData.Price)			    		!= "undefined") {this.Price(tData.Price);}
		//		if (typeof(tData.PriceRange)    					!= "undefined") {this.PriceRange(tData.PriceRange);}
				if (typeof(tData.JacketFabricNotes)    		!= "undefined") {this.JacketFabricNotes(tData.JacketFabricNotes);}
				if (typeof(tData.JacketStructureNotes)  != "undefined") {this.JacketStructureNotes(tData.JacketStructureNotes);}
				if (typeof(tData.JacketBottomNotes)    		!= "undefined") {this.JacketBottomNotes(tData.JacketBottomNotes);}
				if (typeof(tData.JacketVentNotes)  			!= "undefined") {this.JacketVentNotes(tData.JacketVentNotes);}
				if (typeof(tData.JacketLapelStyleNotes)     != "undefined") {this.JacketLapelStyleNotes(tData.JacketLapelStyleNotes);}
				if (typeof(tData.JacketButtonsNotes) 		!= "undefined") {this.JacketButtonsNotes(tData.JacketButtonsNotes);}
				if (typeof(tData.JacketPocketsNotes) 		!= "undefined") {this.JacketPocketsNotes(tData.JacketPocketsNotes);}
				if (typeof(tData.JacketLiningColoursNotes)  != "undefined") {this.JacketLiningColoursNotes(tData.JacketLiningColoursNotes);}
				if (typeof(tData.JacketMonogramNotes)    	!= "undefined") {this.JacketMonogramNotes(tData.JacketMonogramNotes);}
				
				if (typeof(tData.JacketCustomStructureNotes)    != "undefined") {this.JacketCustomStructureNotes(tData.JacketCustomStructureNotes);}
				if (typeof(tData.JacketCustomStructureImage)    != "undefined") {this.JacketCustomStructureImage(tData.JacketCustomStructureImage);}
				if (typeof(tData.JacketCustomVentNotes)       	!= "undefined") {this.JacketCustomVentNotes(tData.JacketCustomVentNotes);}
				if (typeof(tData.JacketCustomVentImage)        != "undefined") {this.JacketCustomVentImage(tData.JacketCustomVentImage);}
				if (typeof(tData.JacketCustomLapelNotes)       	!= "undefined") {this.JacketCustomLapelNotes(tData.JacketCustomLapelNotes);}
				if (typeof(tData.JacketCustomLapelImage)        != "undefined") {this.JacketCustomLapelImage(tData.JacketCustomLapelImage);}
				if (typeof(tData.JacketCustomBottomNotes)       != "undefined") {this.JacketCustomBottomNotes(tData.JacketCustomBottomNotes);}
				if (typeof(tData.JacketCustomBottomImage)       != "undefined") {this.JacketCustomBottomImage(tData.JacketCustomBottomImage);}
				//////////////
				if (typeof(tData.JacketMonogram)         		!= "undefined") {this.JacketMonogram(tData.JacketMonogram);}
				if (typeof(tData.JacketMonogramExtraLine)         		!= "undefined") {this.JacketMonogramExtraLine(tData.JacketMonogramExtraLine);}
				if (typeof(tData.JacketDefaultMonogramHeader)   != "undefined") {this.JacketDefaultMonogramHeader(tData.JacketDefaultMonogramHeader);}
				if (typeof(tData.JacketCustomMonogramHeader)    != "undefined") {this.JacketCustomMonogramHeader(tData.JacketCustomMonogramHeader);}
				if (typeof(tData.JacketMonogramStitchDifferentFromPiping) != "undefined") {this.JacketMonogramStitchDifferentFromPiping(tData.JacketMonogramStitchDifferentFromPiping);}
				if (typeof(tData.JacketMonogramStitchColour)	!= "undefined") {this.JacketMonogramStitchColour(tData.JacketMonogramStitchColour);}
				if (typeof(tData.JacketCustomMonogram)          != "undefined") {this.JacketCustomMonogram(tData.JacketCustomMonogram);}
				if (typeof(tData.JacketCustomMonogramImage)     != "undefined") {this.JacketCustomMonogramImage(tData.JacketCustomMonogramImage);}
				if (typeof(tData.JacketCustomMonogramNotes)     != "undefined") {this.JacketCustomMonogramNotes(tData.JacketCustomMonogramNotes);}
				if (typeof(tData.JacketEmbroidery)        		!= "undefined") {this.JacketEmbroidery(tData.JacketEmbroidery);}
				if (typeof(tData.JacketEmbroideryImage)        	!= "undefined") {this.JacketEmbroideryImage(tData.JacketEmbroideryImage);}
				if (typeof(tData.JacketEmbroideryNotes)        	!= "undefined") {this.JacketEmbroideryNotes(tData.JacketEmbroideryNotes);}
				//////////////
				if (typeof(tData.JacketFabric)          		!= "undefined") {this.JacketFabric(tData.JacketFabric);}
				if (typeof(tData.JacketBottomStyle)    			!= "undefined") {this.JacketBottomStyle(tData.JacketBottomStyle);}
				if (typeof(tData.JacketVentStyle)     			!= "undefined") {this.JacketVentStyle(tData.JacketVentStyle);}
	
				if (typeof(tData.JacketLapelStyle)      		!= "undefined") {this.JacketLapelStyle(tData.JacketLapelStyle);}
				
				if (typeof(tData.JacketBreastPocketStyle)       != "undefined") {this.JacketBreastPocketStyle(tData.JacketBreastPocketStyle);}	 
	//			if (typeof(tData.JacketTicketPocketStyle)       != "undefined") {this.JacketTicketPocketStyle(tData.JacketTicketPocketStyle);}	 
				if (typeof(tData.JacketLapelWidth)      		!= "undefined") {this.JacketLapelWidth(tData.JacketLapelWidth);}
		//		if (typeof(tData.JacketLapelSatin)    			!= "undefined") {this.JacketLapelSatin(tData.JacketLapelSatin);}
	
				if (typeof(tData.JacketPocketStyle)    		    != "undefined") {this.JacketPocketStyle(tData.JacketPocketStyle);}
				if (typeof(tData.JacketSleeveButtonNumber)      != "undefined") {this.JacketSleeveButtonNumber(tData.JacketSleeveButtonNumber);}
				if (typeof(tData.JacketSleeveButtonType)    	!= "undefined") {this.JacketSleeveButtonType(tData.JacketSleeveButtonType);}
				if (typeof(tData.JacketTopStitch)        		!= "undefined") {this.JacketTopStitch(tData.JacketTopStitch);}
				if (typeof(tData.JacketTopStitchPocket) 		!= "undefined") {this.JacketTopStitchPocket(tData.JacketTopStitchPocket);}
				if (typeof(tData.JacketTopStitchLapel) 		!= "undefined") {this.JacketTopStitchLapel(tData.JacketTopStitchLapel);}
		//		if (typeof(tData.JacketTopStitchDarts) 			!= "undefined") {this.JacketTopStitchDarts(tData.JacketTopStitchDarts);}
		//		if (typeof(tData.JacketTopStitchAroundBottom) 	!= "undefined") {this.JacketTopStitchAroundBottom(tData.JacketTopStitchAroundBottom);}
		//		if (typeof(tData.JacketTopStitchContrast)		!= "undefined") {this.JacketTopStitchContrast(tData.JacketTopStitchContrast);}
				if (typeof(tData.JacketTopStitchCode)			!= "undefined") {this.JacketTopStitchCode(tData.JacketTopStitchCode);}			 
				if (typeof(tData.JacketTopStitchPiping)  		!= "undefined") {this.JacketTopStitchPiping(tData.JacketTopStitchPiping);}
				if (typeof(tData.JacketLining)           		!= "undefined") {this.JacketLining(tData.JacketLining);}
				if (typeof(tData.JacketButtonColor)      		!= "undefined") {this.JacketButtonColor(tData.JacketButtonColor);}
				if (typeof(tData.JacketThreadColor)      		!= "undefined") {this.JacketThreadColor(tData.JacketThreadColor);}
				
				
	            if (typeof(tData.JacketJacketStructure) 		!= "undefined") {this.JacketJacketStructure(tData.JacketJacketStructure);}
	            if (typeof(tData.JacketDesignerVent)			!= "undefined") {this.JacketDesignerVent(tData.JacketDesignerVent);}				 
	            if (typeof(tData.JacketTail)					!= "undefined") {this.JacketTail(tData.JacketTail);}				 
	  //          if (typeof(tData.JacketCustomVent)			 		!= "undefined") {this.JacketCustomVent(tData.JacketCustomVent);}								 
	            if (typeof(tData.JacketDinner)			 		!= "undefined") {this.JacketDinner(tData.JacketDinner);}								 
	            if (typeof(tData.JacketCollarAndLapelTrimming)	!= "undefined") {this.JacketCollarAndLapelTrimming(tData.JacketCollarAndLapelTrimming);}	 
	            if (typeof(tData.JacketContrastTrimming)        != "undefined") {this.JacketContrastTrimming(tData.JacketContrastTrimming);}			
	            if (typeof(tData.JacketCollarTrimming)        	!= "undefined") {this.JacketCollarTrimming(tData.JacketCollarTrimming);}				 
	            if (typeof(tData.JacketLapelTrimming)        	!= "undefined") {this.JacketLapelTrimming(tData.JacketLapelTrimming);}					 
	            if (typeof(tData.JacketCoverButtons)        	!= "undefined") {this.JacketCoverButtons(tData.JacketCoverButtons);}					 
	            if (typeof(tData.JacketPocketTrimming)        	!= "undefined") {this.JacketPocketTrimming(tData.JacketPocketTrimming);}
	            if (typeof(tData.JacketBreastPocketTrimming)    != "undefined") {this.JacketBreastPocketTrimming(tData.JacketBreastPocketTrimming);}
				if (typeof(tData.JacketContrastPockets)         != "undefined") {this.JacketContrastPockets(tData.JacketContrastPockets);}//JacketContrastPockets
				if (typeof(tData.JacketContrastCheckPocket)     != "undefined") {this.JacketContrastCheckPocket(tData.JacketContrastCheckPocket);}
				if (typeof(tData.JacketContrastLapelLower)   	!= "undefined") {this.JacketContrastLapelLower(tData.JacketContrastLapelLower);}
				if (typeof(tData.JacketContrastLapelUpper)      != "undefined") {this.JacketContrastLapelUpper(tData.JacketContrastLapelUpper);}
				if (typeof(tData.JacketLapelButtonHole)         != "undefined") {this.JacketLapelButtonHole(tData.JacketLapelButtonHole);}
				if (typeof(tData.JacketTicketPocket)            != "undefined") {this.JacketTicketPocket(tData.JacketTicketPocket);}
				if (typeof(tData.JacketContrastTrimmingColour)     		!= "undefined") {this.JacketContrastTrimmingColour(tData.JacketContrastTrimmingColour);}
		//		if (typeof(tData.JacketBreastPocket)            != "undefined") {this.JacketBreastPocket(tData.JacketBreastPocket);}
				if (typeof(tData.JacketKissingButtons)          != "undefined") {this.JacketKissingButtons(tData.JacketKissingButtons);}
				if (typeof(tData.JacketButtonholeOnLapel)       != "undefined") {this.JacketButtonholeOnLapel(tData.JacketButtonholeOnLapel);}
			//	if (typeof(tData.JacketButtonholeOnLapelContrastCheck)       != "undefined") {this.JacketButtonholeOnLapelContrastCheck(tData.JacketButtonholeOnLapelContrastCheck);}		// REMOVED
				if (typeof(tData.JacketButtonholeOnLapelColor)  != "undefined") {this.JacketButtonholeOnLapelColor(tData.JacketButtonholeOnLapelColor);}
				if (typeof(tData.JacketBreastPocketAngledStyle)      != "undefined") {this.JacketBreastPocketAngledStyle(tData.JacketBreastPocketAngledStyle);}
				if (typeof(tData.JacketPiping)		            != "undefined") {this.JacketPiping(tData.JacketPiping);}
				if (typeof(tData.JacketInsidePocketsColour)		!= "undefined") {this.JacketInsidePocketsColour(tData.JacketInsidePocketsColour);}
				if (typeof(tData.JacketFit)              		!= "undefined") {this.JacketFit(tData.JacketFit);}
				
				if (typeof(tData.JacketInsidePocketsDifferentFromPiping)  != "undefined") {this.JacketInsidePocketsDifferentFromPiping(tData.JacketInsidePocketsDifferentFromPiping);}
				if (typeof(tData.JacketTicketPocketsDifferent)            != "undefined") {this.JacketTicketPocketsDifferent(tData.JacketTicketPocketsDifferent);}
			//	if (typeof(tData.JacketButtonContrast)         	!= "undefined") {this.JacketButtonContrast(tData.JacketButtonContrast);}
				if (typeof(tData.JacketSleeveButton1Colour)         	!= "undefined") {this.JacketSleeveButton1Colour(tData.JacketSleeveButton1Colour);}
				if (typeof(tData.JacketSleeveButton2Colour)           != "undefined") {this.JacketSleeveButton2Colour(tData.JacketSleeveButton2Colour);}
				if (typeof(tData.JacketSleeveButton3Colour)           != "undefined") {this.JacketSleeveButton3Colour(tData.JacketSleeveButton3Colour);}
				if (typeof(tData.JacketSleeveButton4Colour)           != "undefined") {this.JacketSleeveButton4Colour(tData.JacketSleeveButton4Colour);}
				if (typeof(tData.JacketSleeveButton5Colour)           != "undefined") {this.JacketSleeveButton5Colour(tData.JacketSleeveButton5Colour);}
				if (typeof(tData.JacketSleeveButtonHole1Colour)       != "undefined") {this.JacketSleeveButtonHole1Colour(tData.JacketSleeveButtonHole1Colour);}
				if (typeof(tData.JacketSleeveButtonHole2Colour)       != "undefined") {this.JacketSleeveButtonHole2Colour(tData.JacketSleeveButtonHole2Colour);}
				if (typeof(tData.JacketSleeveButtonHole3Colour)       != "undefined") {this.JacketSleeveButtonHole3Colour(tData.JacketSleeveButtonHole3Colour);}
				if (typeof(tData.JacketSleeveButtonHole4Colour)       != "undefined") {this.JacketSleeveButtonHole4Colour(tData.JacketSleeveButtonHole4Colour);}
				if (typeof(tData.JacketSleeveButtonHole5Colour)       != "undefined") {this.JacketSleeveButtonHole5Colour(tData.JacketSleeveButtonHole5Colour);}
				if (typeof(tData.JacketSleeveButton1StitchColour)     != "undefined") {this.JacketSleeveButton1StitchColour(tData.JacketSleeveButton1StitchColour);}
				if (typeof(tData.JacketSleeveButton2StitchColour)     != "undefined") {this.JacketSleeveButton2StitchColour(tData.JacketSleeveButton2StitchColour);}
				if (typeof(tData.JacketSleeveButton3StitchColour)     != "undefined") {this.JacketSleeveButton3StitchColour(tData.JacketSleeveButton3StitchColour);}
				if (typeof(tData.JacketSleeveButton4StitchColour)     != "undefined") {this.JacketSleeveButton4StitchColour(tData.JacketSleeveButton4StitchColour);}
				if (typeof(tData.JacketSleeveButton5StitchColour)     != "undefined") {this.JacketSleeveButton5StitchColour(tData.JacketSleeveButton5StitchColour);}
				
				if (typeof(tData.urgent)     != "undefined") {this.urgent(tData.urgent);}
				//if (typeof(tData.urgentDate)     != "undefined") {this.urgentDate(tData.urgentDate);}
				if (typeof(tData.DOP_day)		!= "undefined") {this.DOP_day(tData.DOP_day);}
				if (typeof(tData.DOP_month)		!= "undefined") {this.DOP_month(tData.DOP_month);}
				if (typeof(tData.DOP_date)		!= "undefined") {this.DOP_date(tData.DOP_date);}
				
		//		if (typeof(tData.JacketPipingCode)              != "undefined") {this.JacketPipingCode(tData.JacketPipingCode);}
	    //        if (typeof(tData.JacketLiningCode)              != "undefined") {this.JacketLiningCode(tData.JacketLiningCode);}
	            if ( this.currentStep().id == 0) this.JacketFabricSelect();
				if ( this.currentStep().id == 4) this.JacketContrastFabricSelect();//JacketContrastFabricSelect
			}
		//this.JacketFabricSelect();
		}catch(e){
			;
		}                      
	}

});

defJacket = SimpleDatasource.extend({
	init: function(name, dsRegistry, olddata, count) {
		if(olddata == null || olddata == undefined){
			
			var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
			var Jacket = {};
			Jacket.variantId = 0;
	
			Jacket.checkVisibility	= true;
			Jacket.JacketNotes	= '';
	//		Jacket.Price			= '0';
	//		Jacket.PriceRange			= '-';
			Jacket.JacketFabricNotes			= '';
			Jacket.JacketStructureNotes			= '';
			Jacket.JacketBottomNotes			= '';
			Jacket.JacketVentNotes				= '';
			Jacket.JacketLapelStyleNotes		= '';
			Jacket.JacketButtonsNotes			= '';
			Jacket.JacketPocketsNotes			= '';
			Jacket.JacketLiningColoursNotes		= '';
			Jacket.JacketMonogramNotes			= '';
			Jacket.JacketCustomStructureNotes	= '';
			Jacket.JacketCustomStructureImage	= '';
			Jacket.JacketCustomVentNotes		= '';
			Jacket.JacketCustomVentImage		= '';
			Jacket.JacketCustomLapelNotes		= '';
			Jacket.JacketCustomLapelImage		= '';
			Jacket.JacketCustomBottomNotes		= '';
			Jacket.JacketCustomBottomImage		= ''; 
			Jacket.JacketMonogram        = '';				// 9a
			Jacket.JacketMonogramExtraLine        = '';				
			Jacket.JacketDefaultMonogramHeader  = true;		// 9b
			Jacket.JacketCustomMonogramHeader  = '';	// 9b
			Jacket.JacketMonogramStitchDifferentFromPiping = false;	// 9d
			Jacket.JacketMonogramStitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';		// 9d
			Jacket.JacketCustomMonogram			= false;	// 9 optional A
			Jacket.JacketCustomMonogramImage = '';			// 9 optional A
			Jacket.JacketCustomMonogramNotes = '';			// 9 optional A
			Jacket.JacketEmbroidery	= false;			// 9 optional B
			Jacket.JacketEmbroideryImage = '';			// 9 optional B
			Jacket.JacketEmbroideryNotes = '';			// 9 optional B
			Jacket.JacketFabric 	 	 = df;
			Jacket.JacketBottomStyle	 = dsRegistry.getDatasource('suitBottomStyleDS').getStore()[0]; 
			Jacket.JacketVentStyle   	 = dsRegistry.getDatasource('suitVentStyleDS').getStore()[0];
	        Jacket.JacketJacketStructure = dsRegistry.getDatasource('suitJacketStructureDS').getStore()[0];	
			Jacket.JacketLapelStyle  	 = dsRegistry.getDatasource('suitLapelStyleDS').getStore()[0];
			Jacket.JacketBreastPocketStyle = dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore()[1]; //"Standard Pocket";
			Jacket.JacketTicketPocketsDifferent = false;
	//		Jacket.JacketTicketPocketStyle = dsRegistry.getDatasource('tickPocketStyleDS').getStore()[0];
			Jacket.JacketLapelWidth   	 = 0;
	//		Jacket.JacketLapelSatin  	 = false;
			Jacket.JacketPocketStyle  	 = dsRegistry.getDatasource('suitPocketStyleDS').getStore()[0];
			Jacket.JacketSleeveButtonNumber	= dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore()[3];
			Jacket.JacketSleeveButtonType	= 'Working';
			Jacket.JacketTopStitch         = false;
			Jacket.JacketTopStitchPocket   = false;
			Jacket.JacketTopStitchLapel   = false;
	//		Jacket.JacketTopStitchDarts   = false;
	//		Jacket.JacketTopStitchAroundBottom   = false;
	//		Jacket.JacketTopStitchContrast = false;
			Jacket.JacketTopStitchCode      = dsRegistry.getDatasource('pipingColorDS').getStore()[0];	
	 //       Jacket.JacketBreastPocket        = true;
	        Jacket.JacketKissingButtons      = false;
	        Jacket.JacketButtonholeOnLapel      = true;
	 //       Jacket.JacketButtonholeOnLapelContrastCheck      = false;		// REMOVED
	        Jacket.JacketButtonholeOnLapelColor = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
	        Jacket.JacketBreastPocketAngledStyle  = 'Straight'; 
	        Jacket.JacketLapelButtonHole	 = true;
	        Jacket.JacketTicketPocket		 = false;
		    Jacket.JacketContrastLapelUpper  = false;
	        Jacket.JacketContrastLapelLower  = false;
	        Jacket.JacketContrastPockets     = false; 
	        Jacket.JacketContrastCheckPocket = false;
	        Jacket.JacketCollarAndLapelTrimming	 = false;
	        Jacket.JacketContrastTrimming	 = false;		
	        Jacket.JacketCollarTrimming		 = false;
	        Jacket.JacketLapelTrimming	 	= false;
	        Jacket.JacketCoverButtons	 	= false;
	        Jacket.JacketPocketTrimming	 	= false;
	        Jacket.JacketBreastPocketTrimming	 	= false;
	        Jacket.JacketDesignerVent		= false;
	        Jacket.JacketTail				= false;   			
	  //      Jacket.JacketCustomVent				= false;   			
	        Jacket.JacketDinner				= false;   			
			Jacket.JacketTopStitchPiping   	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketPiping            	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketInsidePocketsColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0]; // ='';
			Jacket.JacketLining              = dsRegistry.getDatasource('liningFabricDS').getStore()[0];
			Jacket.JacketButtonColor         = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			Jacket.JacketThreadColor         = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[1].title;
			Jacket.JacketInsidePocketsDifferentFromPiping = false;
	//		Jacket.JacketLiningCode = dsRegistry.getDatasource('liningFabricDS').getStore()[0];//'';
			Jacket.JacketContrastTrimmingColour = '';//dsRegistry.getDatasource('pipingColorDS').getStore()[0];
	//		Jacket.JacketPipingCode = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';
	//		Jacket.JacketButtonContrast = false;
			Jacket.JacketSleeveButton1Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			Jacket.JacketSleeveButton2Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			Jacket.JacketSleeveButton3Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			Jacket.JacketSleeveButton4Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			Jacket.JacketSleeveButton5Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
			Jacket.JacketSleeveButtonHole1Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButtonHole2Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButtonHole3Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButtonHole4Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButtonHole5Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButton1StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButton2StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButton3StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButton4StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButton5StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.JacketSleeveButton5StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			Jacket.custom     =    new CustomProperties();
			Jacket.JacketCustomerFabric = {};
			Jacket.JacketHasCustomerFabric = false;
			Jacket.GarmentPrice = 0;
			
			Jacket.urgent = 0;
		//	Jacket.urgentDate = '';
			Jacket.DOP_day = '';
			Jacket.DOP_month = '';
			Jacket.DOP_date = '';
		
			var iJacket = {
				Jacket: []
			};

			iJacket.Jacket.push(Jacket);
			this._super(name, iJacket, dsRegistry);
			
		}else{
			
			var iJacket = {
				Jacket: []
			};
			
			if(olddata.length > count){
				for(var x = count; x < olddata.length; x++){
					orderItem.removeFabric('Jacket', 0, 'Jacket ' + (x + 1), olddata[ x ].JacketCustomerFabric, olddata[ x ].variantId, 2.5);
				}
			}
			for(var x = 0; x < count; x++){
				if(olddata[x] != undefined){
					
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var Jacket = {};
					Jacket.variantId = x;
			
					Jacket.checkVisibility	= true;
					Jacket.JacketNotes	= olddata[x].JacketNotes;
			//		Jacket.Price			= '0';
			//		Jacket.PriceRange			= '-';
					Jacket.JacketFabricNotes			= olddata[x].JacketFabricNotes;
					Jacket.JacketStructureNotes			= olddata[x].JacketStructureNotes;
					Jacket.JacketBottomNotes			= olddata[x].JacketBottomNotes;
					Jacket.JacketVentNotes				= olddata[x].JacketVentNotes;
					Jacket.JacketLapelStyleNotes		= olddata[x].JacketLapelStyleNotes;
					Jacket.JacketButtonsNotes			= olddata[x].JacketButtonsNotes;
					Jacket.JacketPocketsNotes			= olddata[x].JacketPocketsNotes;
					Jacket.JacketLiningColoursNotes		= olddata[x].JacketLiningColoursNotes;
					Jacket.JacketMonogramNotes			= olddata[x].JacketMonogramNotes;
					Jacket.JacketCustomStructureNotes	= olddata[x].JacketCustomStructureNotes;
					Jacket.JacketCustomStructureImage	= olddata[x].JacketCustomStructureImage;
					Jacket.JacketCustomVentNotes		= olddata[x].JacketCustomVentNotes;
					Jacket.JacketCustomVentImage		= olddata[x].JacketCustomVentImage;
					Jacket.JacketCustomLapelNotes		= olddata[x].JacketCustomLapelNotes;
					Jacket.JacketCustomLapelImage		= olddata[x].JacketCustomLapelImage;
					Jacket.JacketCustomBottomNotes		= olddata[x].JacketCustomBottomNotes;
					Jacket.JacketCustomBottomImage		= olddata[x].JacketCustomBottomImage; 
					Jacket.JacketMonogram        		= olddata[x].JacketMonogram;
					Jacket.JacketMonogramExtraLine      = olddata[x].JacketMonogramExtraLine;
					Jacket.JacketDefaultMonogramHeader  = olddata[x].JacketDefaultMonogramHeader;
					Jacket.JacketCustomMonogramHeader  	= olddata[x].JacketCustomMonogramHeader;
					Jacket.JacketMonogramStitchDifferentFromPiping = olddata[x].JacketMonogramStitchDifferentFromPiping;
					Jacket.JacketMonogramStitchColour 	= olddata[x].JacketMonogramStitchColour;
					Jacket.JacketCustomMonogram			= olddata[x].JacketCustomMonogram;
					Jacket.JacketCustomMonogramImage 	= olddata[x].JacketCustomMonogramImage;
					Jacket.JacketCustomMonogramNotes 	= olddata[x].JacketCustomMonogramNotes;
					Jacket.JacketEmbroidery				= olddata[x].JacketEmbroidery;
					Jacket.JacketEmbroideryImage 		= olddata[x].JacketEmbroideryImage;
					Jacket.JacketEmbroideryNotes 		= olddata[x].JacketEmbroideryNotes;
					Jacket.JacketFabric 	 	 		= olddata[x].JacketFabric;
					Jacket.JacketBottomStyle	 		= olddata[x].JacketBottomStyle;
					Jacket.JacketVentStyle   	 		= olddata[x].JacketVentStyle;
					Jacket.JacketJacketStructure 		= olddata[x].JacketJacketStructure;	
					Jacket.JacketLapelStyle  	 		= olddata[x].JacketLapelStyle;
					Jacket.JacketBreastPocketStyle 		= olddata[x].JacketBreastPocketStyle;
					Jacket.JacketTicketPocketsDifferent = olddata[x].JacketTicketPocketsDifferent;
			//		Jacket.JacketTicketPocketStyle = olddata[x].JacketTicketPocketStyle;
					Jacket.JacketLapelWidth   	 		= olddata[x].JacketLapelWidth;
			//		Jacket.JacketLapelSatin  	 		= olddata[x].JacketLapelSatin;
					Jacket.JacketPocketStyle  	 		= olddata[x].JacketPocketStyle;
					Jacket.JacketSleeveButtonNumber		= olddata[x].JacketSleeveButtonNumber;
					Jacket.JacketSleeveButtonType		= olddata[x].JacketSleeveButtonType;
					Jacket.JacketTopStitch         		= olddata[x].JacketTopStitch;
					Jacket.JacketTopStitchPocket   		= olddata[x].JacketTopStitchPocket;
					Jacket.JacketTopStitchLapel   		= olddata[x].JacketTopStitchLapel;
			//		Jacket.JacketTopStitchDarts   		= olddata[x].JacketTopStitchDarts;
			//		Jacket.JacketTopStitchAroundBottom  = olddata[x].JacketTopStitchAroundBottom;
			//		Jacket.JacketTopStitchContrast 		= olddata[x].JacketTopStitchContrast;
					Jacket.JacketTopStitchCode      	= olddata[x].JacketTopStitchCode;	
			//        Jacket.JacketBreastPocket        	= olddata[x].JacketBreastPocket;
					Jacket.JacketKissingButtons      	= olddata[x].JacketKissingButtons;
					Jacket.JacketButtonholeOnLapel      = olddata[x].JacketButtonholeOnLapel;
			 //       Jacket.JacketButtonholeOnLapelContrastCheck      = olddata[x].JacketButtonholeOnLapelContrastCheck;		// REMOVED
			        Jacket.JacketButtonholeOnLapelColor = olddata[x].JacketButtonholeOnLapelColor;
			        Jacket.JacketBreastPocketAngledStyle  = olddata[x].JacketBreastPocketAngledStyle; 
			        Jacket.JacketLapelButtonHole	 = olddata[x].JacketLapelButtonHole;
			        Jacket.JacketTicketPocket		 = olddata[x].JacketTicketPocket;
				    Jacket.JacketContrastLapelUpper  = olddata[x].JacketContrastLapelUpper;
			        Jacket.JacketContrastLapelLower  = olddata[x].JacketContrastLapelLower;
			        Jacket.JacketContrastPockets     = olddata[x].JacketContrastPockets;
			        Jacket.JacketContrastCheckPocket = olddata[x].JacketContrastCheckPocket;
			        Jacket.JacketCollarAndLapelTrimming	 = olddata[x].JacketCollarAndLapelTrimming;
			        Jacket.JacketContrastTrimming	 = olddata[x].JacketContrastTrimming;
			        Jacket.JacketCollarTrimming		 = olddata[x].JacketCollarTrimming;
			        Jacket.JacketLapelTrimming	 	= olddata[x].JacketLapelTrimming;
			        Jacket.JacketCoverButtons	 	= olddata[x].JacketCoverButtons;
			        Jacket.JacketPocketTrimming	 	= olddata[x].JacketPocketTrimming;
			        Jacket.JacketBreastPocketTrimming	 	= olddata[x].JacketBreastPocketTrimming;
			        Jacket.JacketDesignerVent		= olddata[x].JacketDesignerVent;
			        Jacket.JacketTail				= olddata[x].JacketTail;	
			  //    Jacket.JacketCustomVent			= olddata[x].JacketCustomVent;   			
			        Jacket.JacketDinner				= olddata[x].JacketDinner;   			
					Jacket.JacketTopStitchPiping   	 = olddata[x].JacketTopStitchPiping;
					Jacket.JacketPiping            	 = olddata[x].JacketPiping;
					Jacket.JacketInsidePocketsColour = olddata[x].JacketInsidePocketsColour;
					Jacket.JacketLining              = olddata[x].JacketLining;
					Jacket.JacketButtonColor         = olddata[x].JacketButtonColor;
					Jacket.JacketThreadColor         = olddata[x].JacketThreadColor;
					Jacket.JacketFit = olddata[x].JacketFit;
					Jacket.JacketInsidePocketsDifferentFromPiping = olddata[x].JacketInsidePocketsDifferentFromPiping;
			//		Jacket.JacketLiningCode = olddata[x].JacketLiningCode;
					Jacket.JacketContrastTrimmingColour = olddata[x].JacketContrastTrimmingColour;
			//		Jacket.JacketPipingCode = olddata[x].JacketPipingCode;
			//		Jacket.JacketButtonContrast = olddata[x].JacketButtonContrast;
					Jacket.JacketSleeveButton1Colour = olddata[x].JacketSleeveButton1Colour;
					Jacket.JacketSleeveButton2Colour = olddata[x].JacketSleeveButton2Colour;
					Jacket.JacketSleeveButton3Colour = olddata[x].JacketSleeveButton3Colour;
					Jacket.JacketSleeveButton4Colour = olddata[x].JacketSleeveButton4Colour;
					Jacket.JacketSleeveButton5Colour = olddata[x].JacketSleeveButton5Colour;
					Jacket.JacketSleeveButtonHole1Colour = olddata[x].JacketSleeveButtonHole1Colour;
					Jacket.JacketSleeveButtonHole2Colour = olddata[x].JacketSleeveButtonHole2Colour;
					Jacket.JacketSleeveButtonHole3Colour = olddata[x].JacketSleeveButtonHole3Colour;
					Jacket.JacketSleeveButtonHole4Colour = olddata[x].JacketSleeveButtonHole4Colour;
					Jacket.JacketSleeveButtonHole5Colour = olddata[x].JacketSleeveButtonHole5Colour;
					Jacket.JacketSleeveButton1StitchColour = olddata[x].JacketSleeveButton1StitchColour;
					Jacket.JacketSleeveButton2StitchColour = olddata[x].JacketSleeveButton2StitchColour;
					Jacket.JacketSleeveButton3StitchColour = olddata[x].JacketSleeveButton3StitchColour;
					Jacket.JacketSleeveButton4StitchColour = olddata[x].JacketSleeveButton4StitchColour;
					Jacket.JacketSleeveButton5StitchColour = olddata[x].JacketSleeveButton5StitchColour;
					if(olddata[x].custom){
						Jacket.custom      		= olddata[x].custom;
					}
					else{
						Jacket.custom     =    new CustomProperties();
					}
							
					Jacket.urgent = olddata[x].urgent;
				//	Jacket.urgentDate = olddata[x].urgentDate;
					Jacket.DOP_day = olddata[x].DOP_day;
					Jacket.DOP_month = olddata[x].DOP_month;		
					Jacket.DOP_date = olddata[x].DOP_date;
					Jacket.JacketCustomerFabric = olddata[x].JacketCustomerFabric;
					Jacket.JacketHasCustomerFabric = olddata[x].JacketHasCustomerFabric;
					Jacket.GarmentPrice = olddata[x].GarmentPrice;;					
							
					iJacket.Jacket.push(Jacket);							
					
				}else{
					
					var df = dsRegistry.getDatasource('fabricsDS').getStore()[1];
					var Jacket = {};
					Jacket.variantId = x;
			
					Jacket.checkVisibility	= true;
					Jacket.JacketNotes	= '';
			//		Jacket.Price			= '0';
			//		Jacket.PriceRange			= '-';
					Jacket.JacketFabricNotes			= '';
					Jacket.JacketStructureNotes			= '';
					Jacket.JacketBottomNotes			= '';
					Jacket.JacketVentNotes				= '';
					Jacket.JacketLapelStyleNotes		= '';
					Jacket.JacketButtonsNotes			= '';
					Jacket.JacketPocketsNotes			= '';
					Jacket.JacketLiningColoursNotes		= '';
					Jacket.JacketMonogramNotes			= '';
					Jacket.JacketCustomStructureNotes	= '';
					Jacket.JacketCustomStructureImage	= '';
					Jacket.JacketCustomVentNotes		= '';
					Jacket.JacketCustomVentImage		= '';
					Jacket.JacketCustomLapelNotes		= '';
					Jacket.JacketCustomLapelImage		= '';
					Jacket.JacketCustomBottomNotes		= '';
					Jacket.JacketCustomBottomImage		= ''; 
					Jacket.JacketMonogram        = '';				// 9a
					Jacket.JacketMonogramExtraLine        = '';
					Jacket.JacketDefaultMonogramHeader  = true;		// 9b
					Jacket.JacketCustomMonogramHeader  = '';	// 9b
					Jacket.JacketMonogramStitchDifferentFromPiping = false;	// 9d
					Jacket.JacketMonogramStitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';		// 9d
					Jacket.JacketCustomMonogram			= false;	// 9 optional A
					Jacket.JacketCustomMonogramImage = '';			// 9 optional A
					Jacket.JacketCustomMonogramNotes = '';			// 9 optional A
					Jacket.JacketEmbroidery	= false;			// 9 optional B
					Jacket.JacketEmbroideryImage = '';			// 9 optional B
					Jacket.JacketEmbroideryNotes = '';			// 9 optional B
					Jacket.JacketFabric 	 	 = df;
					Jacket.JacketBottomStyle	 = dsRegistry.getDatasource('suitBottomStyleDS').getStore()[0];
					Jacket.JacketVentStyle   	 = dsRegistry.getDatasource('suitVentStyleDS').getStore()[0];
					Jacket.JacketJacketStructure = dsRegistry.getDatasource('suitJacketStructureDS').getStore()[0];	
					Jacket.JacketLapelStyle  	 = dsRegistry.getDatasource('suitLapelStyleDS').getStore()[0];
					Jacket.JacketBreastPocketStyle = dsRegistry.getDatasource('JacketBreastPocketStyleDS').getStore()[1]; //"Standard Pocket";
					Jacket.JacketTicketPocketsDifferent = false;
			//		Jacket.JacketTicketPocketStyle = dsRegistry.getDatasource('tickPocketStyleDS').getStore()[0];
					Jacket.JacketLapelWidth   	 = 0;
			//		Jacket.JacketLapelSatin  	 = false;
					Jacket.JacketPocketStyle  	 = dsRegistry.getDatasource('suitPocketStyleDS').getStore()[0];
					Jacket.JacketSleeveButtonNumber	= dsRegistry.getDatasource('JacketSleeveButtonNumberDS').getStore()[3];
					Jacket.JacketSleeveButtonType	= 'Working';
					Jacket.JacketTopStitch         = false;
					Jacket.JacketTopStitchPocket   = false;
					Jacket.JacketTopStitchLapel   = false;
			//		Jacket.JacketTopStitchDarts   = false;
			//		Jacket.JacketTopStitchAroundBottom   = false;
			//		Jacket.JacketTopStitchContrast = false;
					Jacket.JacketTopStitchCode      = dsRegistry.getDatasource('pipingColorDS').getStore()[0];	
			//        Jacket.JacketBreastPocket        = true;
			        Jacket.JacketKissingButtons      = false;
			        Jacket.JacketButtonholeOnLapel      = true;
			//        Jacket.JacketButtonholeOnLapelContrastCheck      = false;		// REMOVED
			        Jacket.JacketButtonholeOnLapelColor = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			        Jacket.JacketBreastPocketAngledStyle  = 'Straight'; 
			        Jacket.JacketLapelButtonHole	 = true;
			        Jacket.JacketTicketPocket		 = false;
				    Jacket.JacketContrastLapelUpper  = false;
			        Jacket.JacketContrastLapelLower  = false;
			        Jacket.JacketContrastPockets     = false; 
			        Jacket.JacketContrastCheckPocket = false;
			        Jacket.JacketCollarAndLapelTrimming	 = false;
			        Jacket.JacketContrastTrimming	 = false;
			        Jacket.JacketCollarTrimming		 = false;
			        Jacket.JacketLapelTrimming	 	= false;
			        Jacket.JacketCoverButtons	 	= false;
			        Jacket.JacketPocketTrimming	 	= false;
			        Jacket.JacketBreastPocketTrimming	 	= false;
			        Jacket.JacketDesignerVent		= false;
			        Jacket.JacketTail				= false;   			
			  //      Jacket.JacketCustomVent				= false;   			
			        Jacket.JacketDinner				= false;   			
					Jacket.JacketTopStitchPiping   	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketPiping            	 = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketInsidePocketsColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0]; // ='';
					Jacket.JacketLining              = dsRegistry.getDatasource('liningFabricDS').getStore()[0];
					Jacket.JacketButtonColor         = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					Jacket.JacketThreadColor         = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketFit = 'Semi Fitted';//dsRegistry.getDatasource('fitDS').getStore()[1].title;
					Jacket.JacketInsidePocketsDifferentFromPiping = false;
			//		Jacket.JacketLiningCode = dsRegistry.getDatasource('liningFabricDS').getStore()[0];//'';
					Jacket.JacketContrastTrimmingColour = '';//dsRegistry.getDatasource('pipingColorDS').getStore()[0];
			//		Jacket.JacketPipingCode = dsRegistry.getDatasource('pipingColorDS').getStore()[0];//'';
			//		Jacket.JacketButtonContrast = false;
					Jacket.JacketSleeveButton1Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					Jacket.JacketSleeveButton2Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					Jacket.JacketSleeveButton3Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					Jacket.JacketSleeveButton4Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					Jacket.JacketSleeveButton5Colour = dsRegistry.getDatasource('vestButtonsDS').getStore()[0];
					Jacket.JacketSleeveButtonHole1Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButtonHole2Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButtonHole3Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButtonHole4Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButtonHole5Colour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButton1StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButton2StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButton3StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButton4StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketSleeveButton5StitchColour = dsRegistry.getDatasource('pipingColorDS').getStore()[0];
					Jacket.JacketCustomerFabric = {};
					Jacket.JacketHasCustomerFabric = false;
					Jacket.GarmentPrice = 0;
							
					Jacket.urgent = false;
				//	Jacket.urgentDate = '';
					Jacket.DOP_day = '';
					Jacket.DOP_month = '';
					Jacket.DOP_date = '';
					Jacket.custom     =    new CustomProperties();
					iJacket.Jacket.push(Jacket);		
				}	
			}
			this._super(name, iJacket, dsRegistry);			
		}	
	}
});


//END DEFINE CLOSURE
});
