
define(['jquery', 'knockout', 'base'], function($, ko) {


    UniformOrderBodyShapesStepVM = SimpleControl.extend({

        init: function (bodyshapes) {
            self = this;

            // Clone the object
            const cloneBodyshapes = JSON.parse(JSON.stringify(bodyshapes));
            
            this.bodyShapes = ko.observableArray(this.buildBodyshapes(cloneBodyshapes));
            
            let bodyNotes = "";


            this.bodyshapeNotes = ko.observable(bodyNotes);
        },

        getSubmissionData : function() {

            const bodyshapes = [];

            for( let bodyshapeCategory of this.bodyShapes() ) // checks if there is at least one garment per customer
            {   
                if (bodyshapeCategory.selectedValue())
                {
                    selectedShapeId = bodyshapeCategory.selectedValue().id;
                }

                const shapeObj = { 
                        key : bodyshapeCategory.cat_key,
                        selectedVal : selectedShapeId
                    };

                bodyshapes.push(shapeObj)
            }

            const notesObj = {
                key : "bodyshape_notes",
                selectedVal : this.bodyshapeNotes() ? this.bodyshapeNotes() : ""
            };

            bodyshapes.push(notesObj);

            return bodyshapes;
        },

        buildBodyshapes : function(bodyShapes) {

            for( let shape of bodyShapes)
            {
                let selectedVal = false;

                // If there is no selected val
                if( !selectedVal || selectedVal == 0)
                {
                    // Get default value initually
                    selectedVal = this.getDefaultValueForShape(shape.cat_key);
                }
                
                console.log("BODYSHAPE", selectedVal);

                const selectedBodyValObj = shape.values[0].filter( e => e.id == selectedVal)[0] || false;

                shape.selectedValue = ko.observable(selectedBodyValObj);
            }

            return bodyShapes;
        },  

        /** Change the selected bodyShape */
        selectBodyShape: function (bodyshapeCategory, selectedValue) {
            console.log('bodyshapeCategory', bodyshapeCategory);
            console.log('selectedValue', selectedValue);
            
            bodyshapeCategory.selectedValue(selectedValue);
            this.toggleStepComplete();
        },

        /**
         * Returns the 'Normal' value for each bodyshape
         * @param  {[type]} shapeKey [description]
         * @return {[type]}          [description]
         */
        getDefaultValueForShape : function(shapeKey) {

            let defaultValue = 0;

            switch(shapeKey)
            {
                case ("bodyshapeShoulders") : defaultValue = 3;break; // Normal
                case ("bodyshapeArms") : defaultValue = 2;break; // balanced
                case ("bodyshapeBack") : defaultValue = 3;break; // Normal
                case ("bodyshapeFront") : defaultValue = 2;break; // Normal
                case ("bodyshapeLeg") : defaultValue = 3;break; // Normal
                case ("bodyshapeNeckHeight") : defaultValue = 2;break; // Normal
                case ("bodyshapeStanding") : defaultValue = 2;break; // Neutral
                case ("bodyshapeStomach") : defaultValue = 3;break; // Normal
                case ("bodyshapeThigh") : defaultValue = 6;break; // Normal
                case ("bodyshape_notes") : defaultValue = "";break; // Normal
            }

            return defaultValue;
        },

        /**
         * if all body shapes of all categories are checked, set step as 'completed
         */
        toggleStepComplete: function () {
            

           
            
            console.log("todo : toogle step");
        },

        /**
         * append without duplicate element
         */
        pushObservableArray:function(array, el){
            // check if not in array first
            for (const iterator of array()) {
                if (iterator.cat_key == el.cat_key){
                    return;
                }
            }

            // if not in array, append
            array.push(el);
        }

    });
});
