define(['jquery', 'knockout', 'base'], function ($, ko) {
	Fitting = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var that = this;
			this.FittingData = [];
			this.FittingDataAID = 0;
			this.NumberShirts = 0;
			this.editFittingVideoNotes = ko.observable(-1);
			//this.shirtCloneMessage = ko.observable(true);
			this.changedVideo = ko.observable(false);

			this.previousStepEnabled = ko.observable(false);
			this.skipAppear = ko.observable(true);
			this.skipAppearVideo = ko.observable(true);
			this.skipAppearVideoNotes = ko.observable(false);

			this.mediaUpload = orderItem.MediaUploadVM;
			this.mediaID = this.mediaUpload.mediaID;
			this.tagText = ko.observable('');
			this.media = this.mediaUpload.media;
			this.control = 0;

			
			this.previewFrontImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.previewSideImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.previewBackImage = ko.observable({ media: ko.observable(''), tags: ko.observableArray([]) });
			this.previewCustomImage = ko.observableArray([]);
			this.previewCustomVideo = ko.observableArray([]);

			this.updateSkipButtons = function () {

				that.skipAppearVideo(!that.FittingVideo.hasMedia());
				console.log(that.FittingVideo.anyMediaHavetags());
				that.skipAppearVideoNotes(!that.FittingVideo.anyMediaHavetags() && that.FittingVideo.hasMedia());



			};

			this.mediaCallback = function (data) {

				console.log("media callback fitting-original", data);

				console.log(orderItem.MediaUploadVM.pictureTaken());
				if (data) {
					console.log('Fitting medias length after take picture');
					console.log(orderItem.MediaUploadVM.medias().length);
					if (that.currentStep().id == 0 || that.currentStep().id == 4) {
						that.frontImage.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true);
					} else if (that.currentStep().id == 1 || that.currentStep().id == 5) {
						that.sideImage.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true);
					} else if (that.currentStep().id == 2 || that.currentStep().id == 6) {
						that.backImage.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true);
					} else if (that.currentStep().id == 3) {
						that.customImages.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true);
					}
					if (that.currentStep().id == 7) {
						that.FittingVideo.addMedia(that.mediaUpload.imagPath, that.mediaUpload.upPath, true);
						that.updateSkipButtons();
						return;
					} else if (that.currentStep().id < 3) {
						customAlert('Your image was saved successfully! Proceed');
						that.nextStep();
					}

				}
			};

			this.mediaUpload.setMediaCallback(this.mediaCallback);

			this.setText = function (text, observable) {
				observable(text);
			};

			this.editFittingVideoNotes.subscribe(function (data) {
				if (data > -1 && that.FittingVideoNotes().length > data) {
					that.FittingVideoNotesSingle(that.FittingVideoNotes()[data]);
				} else if (data > -1) {
					that.editFittingVideoNotes(-1);
				}
			});

			this.correctDate = function (date) {
				if (date != undefined && date != null) {
					if (date == -1 || date == "-1") {
						return 0;
					} else {
						if (date.indexOf(" ") >= 0) {
							date = date.substring(0, date.indexOf(" "));
						}
						var day = date.substring(date.lastIndexOf("-") + 1);
						var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
						var year = date.substring(0, date.indexOf("-"));
						return day + "/" + month + "/" + year;
					}
				} else {
					return 0;
				}
			};

			this.mapperID = function (index) {
				return "mapper" + index;
			};

			this.dialog = function (mediaData, evt, imageIndex, id) {
				this.mediaData = mediaData;
				this.evt = evt;
				this.imageIndex = imageIndex;
				if (id >= 0) {
					this.idCustomImage = id;
				}
				else {
					this.idCustomImage = -1;
				}
				that.tagText('');
				var tag = this.mediaData.buildTagPicture('', this.evt, this.imageIndex);
				if (this.idCustomImage >= 0) {
					$("#mapper" + this.idCustomImage).css("left", tag.left + '%').css("top", tag.top + '%').css("width", "35px").css("height", "35px").show();
				}
				else {
					$('div.mapper').css("left", tag.left + '%').css("top", tag.top + '%').css("width", "35px").css("height", "35px").show();
				}
				orderItem.openDialog();
			};

			this.addTag = function (text) {
				$("div.form_panel").hide();
				$('div.mapper').hide();
				this.mediaData.addTag(this.mediaData.buildTagPicture(this.tagText(), this.evt, this.imageIndex), this.imageIndex);
				this.tagText('');
				this.skipAppear(false);
			};

			this.takePicture = function () {
				that.mediaUpload.choosePhoto();
				that.control = 1;
			};

			this.recordVideo = function () {
				that.mediaUpload.chooseVideo();
			}

			this.cloneShirts = function (data) {
				if (that.NumberShirts > 1) {
					if (!data) {
						data = that.selectedVariantFitting()
					}
					whichGarment = data.id;
					that.FittingData[whichGarment].FittingCloned = 0;
					if (!that.FittingData[whichGarment].SkippedFrontTags && !that.FittingData[whichGarment].SkippedSideTags && !that.FittingData[whichGarment].SkippedBackTags && !that.FittingData[whichGarment].SkippedVideo && !that.FittingData[whichGarment].SkippedVideoNotes) {
						for (var i in that.FittingData) {
							if (i != whichGarment && that.variantNameFitting()[i].title == "Shirt") {
								var tmp = localStorage.getItem('FittingFrontImageTags' + whichGarment);
								localStorage.setItem('FittingFrontImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingSideImageTags' + whichGarment);
								localStorage.setItem('FittingSideImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingBackImageTags' + whichGarment);
								localStorage.setItem('FittingBackImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingCustomImageTags' + whichGarment);
								localStorage.setItem('FittingCustomImageTags' + i, tmp);
								tmp = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);
								localStorage.setItem('FittingFrontImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
								localStorage.setItem('FittingCustomImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
								localStorage.setItem('FittingSideImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
								localStorage.setItem('FittingBackImagePhoto' + i, tmp);
								tmp = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
								localStorage.setItem('FittingCustomImagePhoto' + i, tmp);
								that.FittingData[i].AdditionalImagesStepComplete = that.FittingData[whichGarment].AdditionalImagesStepComplete;
								that.FittingData[i].FittingVideo = that.FittingData[whichGarment].FittingVideo;

								that.FittingData[i].NoFrontTags = that.FittingData[whichGarment].NoFrontTags;
								that.FittingData[i].NoSideTags = that.FittingData[whichGarment].NoSideTags;
								that.FittingData[i].NoBackTags = that.FittingData[whichGarment].NoBackTags;
								that.FittingData[i].NoVideo = that.FittingData[whichGarment].NoVideo;
								that.FittingData[i].FittingCloned = that.FittingData[whichGarment].FittingOrderProductId;
								that.FittingData[i].FittingVideoNotes = [];
								for (x in that.FittingData[whichGarment].FittingVideoNotes) {
									that.FittingData[i].FittingVideoNotes.push(that.FittingData[whichGarment].FittingVideoNotes[x]);
								}
							}
						}
						that.assignTailorText();
						that.renderFitting();
						that.checkSteps();
					}
					else {
						customAlert("Can't clone a garment with skipped steps. Please complete all before cloning.");
					}
				}
			}


			this.NoTagsConfirm = function () {
				text = "Are you sure you want to continue without adding Notes on the image?";
				var asd = document.getElementById("fittingsDialogText123");
				asd.innerHTML = text;

				$("#fittingsDialogText123").text(text);
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "block";
				return true;
			};

			this.NoVideoConfirm = function () {
				text = "Are you sure you want to continue without adding a Video?";
				document.getElementById("fittingsDialogText123").innerHTML = text;
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "block";
				return true;
			};

			this.NoVideoNotesConfirm = function () {
				text = "Are you sure you want to continue without adding Video Notes?";
				document.getElementById("fittingsDialogText123").innerHTML = text;
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "block";
				return true;
			};

			this.buttonNoCloneAccept = function () {
				dialog = document.getElementById('dialogClone');
				dialog.style.display = "none";
				this.goToFirstUncompletedStep();
			};

			this.buttonYesCloneAccept = function () {
				that.cloneShirts(that.selectedVariantFitting());
				dialog = document.getElementById('dialogClone');
				dialog.style.display = "none";
				this.goToFirstUncompletedStep();
			};

			this.buttonNoAccept = function () {
				var cs = that.currentStep().id;
				var whichGarment = that.selectedVariantFitting().id;
				if (cs == 3) {
					that.AdditionalImagesStepComplete(false);
					that.workflow()[cs].completed = "false";
				} else if (cs == 4) {
					that.NoFrontTags(false);
					that.SkippedFrontTags(false);
				} else if (cs == 5) {
					that.NoSideTags(false);
					that.SkippedSideTags(false);
				} else if (cs == 6) {
					that.NoBackTags(false);
					that.SkippedBackTags(false);
				} else if (cs == 7) {
					that.NoVideo(false);
					that.SkippedVideo(false);
				}

				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "none";
			}
			this.buttonYesAccept = function () {
				console.log("buttonYesAccept");
				var cs = that.currentStep().id;
				var whichGarment = that.selectedVariantFitting().id;
				if (cs == 3) {
					that.AdditionalImagesStepComplete(true);
					that.workflow()[cs].completed = "true";
				} else if (cs == 4) {
					that.NoFrontTags(true);
					that.SkippedFrontTags(false);
					that.workflow()[4].completed = "true";
				} else if (cs == 5) {
					that.NoSideTags(true);
					that.SkippedSideTags(false);
					that.workflow()[5].completed = "true";
				} else if (cs == 6) {
					that.NoBackTags(true);
					that.SkippedBackTags(false);
					that.workflow()[6].completed = "true";
				} else if (cs == 7) {
					that.NoVideo(true);
					that.SkippedVideo(false);
					that.SkippedVideoNotes(false);
					that.workflow()[7].completed = "true";
				}
				dialog = document.getElementById('fittingsDialog123');
				dialog.style.display = "none";

				this.goToFirstUncompletedStep();
			}

			this.nextStep = function () {

				var cs = that.currentStep().id;
				var whichGarment = that.selectedVariantFitting().id;

				img = document.getElementById('imagesize');

				console.log('height ' + img.height + ' width ' + img.width);

				if (cs == 0) {
					if (that.frontImage.hasMedia()) {
						that.workflow()[cs].completed = "true";
					}
					else {
						customAlert("Please upload a front image");
					}

				} else if (cs == 1) {
					if (that.sideImage.hasMedia()) {
						that.workflow()[cs].completed = "true";
					}
					else {
						customAlert("Please upload a front image");
					}
				} else if (cs == 2) {
					if (that.backImage.hasMedia()) {
						that.workflow()[cs].completed = "true";
					}
					else {
						customAlert("Please upload a front image");
					}
				} else if (cs == 3) {
					that.AdditionalImagesStepComplete(false);
					that.workflow()[cs].completed = "false";
					if (that.customImages.hasMedia()) {
						if (that.customImages.anyMediaHavetags()) {
							that.AdditionalImagesStepComplete(true);
							that.workflow()[cs].completed = "true";
						} else {
							this.NoTagsConfirm();
						}
					}
					else {
						that.AdditionalImagesStepComplete(true);
						that.workflow()[cs].completed = "true";
					}
				} else if (cs == 4) {
					if (that.frontImage.allMediaHavetags()) {
						that.SkippedFrontTags(false);
						that.workflow()[cs].completed = "true";
					} else {
						this.NoTagsConfirm();
					}
				} else if (cs == 5) {
					if (that.sideImage.allMediaHavetags()) {
						that.SkippedSideTags(false);
						that.workflow()[cs].completed = "true";
					} else {
						this.NoTagsConfirm();
					}
				} else if (cs == 6) {
					if (that.backImage.allMediaHavetags()) {
						that.SkippedBackTags(false);
						that.workflow()[cs].completed = "true";
					} else {
						this.NoTagsConfirm();
					}
				} else if (cs == 7) {

					that.SkippedVideo(false);
					that.workflow()[cs].completed = "false";
					if (that.FittingVideo.hasMedia()) {
						if (that.FittingVideo.allMediaHavetags()) {
							that.workflow()[cs].completed = "true";
							that.SkippedVideo(false);
							that.SkippedVideoNotes(false);
						}
						else {
							this.NoVideoNotesConfirm();
						}
					} else {
						this.NoVideoConfirm();
					}
				}

				if (that.workflow()[cs].completed == "true") {
					this.goToFirstUncompletedStep();

				} else {
					that.currentStep(that.workflow()[cs]);
				}
			}

			this.goToFirstUncompletedStep = function () {
				var cs = that.currentStep().id;
				var foundnotcompletedstep = false;
				for (var x = 0; x < that.workflow().length - 1; x++) {
					//for(var x = cs+1; x < that.workflow().length; x++){
					if (that.workflow()[x].completed == "false") {
						that.currentStep(that.workflow()[x]);
						foundnotcompletedstep = true;
						break;
					}
				}
				if (foundnotcompletedstep == false) {
					//that.currentStep(that.workflow()[cs + 1] );
					var variantIncompleted = false;
					that.selectedVariantFitting().completed(true);
					if (that.FittingId() != '') {
						if (that.SkippedFrontTags() && cs < 4) {
							that.currentStep(that.workflow()[4]);
						} else if (that.SkippedSideTags() && cs < 5) {
							that.currentStep(that.workflow()[5]);
						} else if (that.SkippedBackTags() && cs < 6) {
							that.currentStep(that.workflow()[6]);
						} else if (that.SkippedVideo() || that.SkippedVideoNotes()) {
							that.currentStep(that.workflow()[7]);
						}
						else {
							that.currentStep(that.workflow()[8]);
						}
					}
					else {
						for (var i = 0; i < that.variantNameFitting().length; i++) {
							if (that.variantNameFitting()[i].completed() === false) {
								variantIncompleted = true;
								break;
							}
						}
						if (variantIncompleted) {
							that.selectedVariantFitting(that.variantNameFitting()[i]);
							that.goToFirstUncompletedStep();
						}
						else {
							that.currentStep(that.workflow()[8]);
						}
					}
				}

			}

			this.checkSteps = function () {

				var whichGarment = that.selectedVariantFitting().id;
				console.log("checkSteps " + whichGarment);
				that.workflow()[0].completed = "false";
				if (that.frontImage.hasMedia()) {
					that.workflow()[0].completed = "true";
				}

				that.workflow()[1].completed = "false";
				if (that.sideImage.hasMedia()) {
					that.workflow()[1].completed = "true";
				}

				that.workflow()[2].completed = "false";
				if (that.backImage.hasMedia()) {
					that.workflow()[2].completed = "true";
				}

				that.workflow()[3].completed = "false";
				if (that.AdditionalImagesStepComplete() == true) {
					that.workflow()[3].completed = "true";
				}

				that.workflow()[4].completed = "false";
				if (that.NoFrontTags() == true) {
					that.workflow()[4].completed = "true";
				} else if (that.frontImage.allMediaHavetags()) {
					that.workflow()[4].completed = "true";
					that.NoFrontTags(false)
					that.SkippedFrontTags(false)
				}
				else if (that.SkippedFrontTags() == true) {
					that.workflow()[4].completed = "pending";
				}
				that.workflow()[5].completed = "false";
				if (that.NoSideTags() == true) {
					that.workflow()[5].completed = "true";
				} else if (that.sideImage.allMediaHavetags()) {
					that.workflow()[5].completed = "true";
					that.NoSideTags(false)
					that.SkippedSideTags(false)
				} else if (that.SkippedSideTags() == true) {
					that.workflow()[5].completed = "pending";
				}

				that.workflow()[6].completed = "false";
				if (that.NoBackTags() == true) {
					that.workflow()[6].completed = "true";
				} else if (that.backImage.allMediaHavetags()) {
					that.workflow()[6].completed = "true";
					that.NoBackTags(false)
					that.SkippedBackTags(false)
				} else if (that.SkippedBackTags() == true) {
					that.workflow()[6].completed = "pending";
				}
				that.workflow()[7].completed = "false";
				if (that.FittingId() != '' && that.SkippedVideo() == false) {
					that.workflow()[7].completed = "true";
				} else if (that.NoVideo() == true) {
					that.workflow()[7].completed = "true";
				} else if (that.SkippedVideo() == true || that.SkippedVideoNotes()) {
					that.workflow()[7].completed = "pending";
				}
				else if (this.FittingVideo.hasMedia()) {
					that.workflow()[7].completed = "true";
				}

				that.workflow()[8].completed = "false";
				if (that.FittingId() != '' && that.FittingTailor() != undefined && that.FittingDeliveryMonth() != undefined && that.FittingDeliveryMonth() != ""
					&& that.FittingDeliveryDay() != undefined && that.FittingDeliveryDay() != "") {
					that.workflow()[8].completed = "true";
				}

				return '';
			};

			this.skipVideoComments = function () {
				that.SkippedVideoNotes(true);
				this.goToFirstUncompletedStep();
			};

			this.skipStep = function () {
				var whichGarment = that.selectedVariantFitting().id;
				var cs = that.currentStep().id;
				that.workflow()[cs].completed = "true";
				console.log("skipStep " + cs);
				if (cs == 4) {
					localStorage.removeItem('FittingFrontImageTags' + whichGarment);
					that.NoFrontTags(false);
					that.SkippedFrontTags(true);
				} else if (cs == 5) {
					localStorage.removeItem('FittingSideImageTags' + whichGarment);
					that.NoSideTags(false);
					that.SkippedSideTags(true);
				} else if (cs == 6) {
					localStorage.removeItem('FittingBackImageTags' + whichGarment);
					that.NoBackTags(false);
					that.SkippedBackTags(true);
				} else if (cs == 7) {
					that.SkippedVideo(true);
				}
				this.goToFirstUncompletedStep();
			}

			this.goToNextNotSubmittedFitting = function () {
				console.log("that.FittingData.length() " + this.FittingData.length);
				var found = false;
				for (var x = 1; x < this.FittingData.length; x++) {
					console.log("garment " + x + "  FittingId: " + this.FittingData[x].FittingId);
					if (this.FittingData[x].FittingId == '') {
						found = true;
						this.selectedVariantFitting(this.variantNameFitting()[x]);
						this.goToFirstUncompletedStep();
						break;
					}
				}
				if (found == false) {
					posChangePage('#customerGarmentsFittingsList');
				}
			}

			this.workflow = ko.observableArray([
				{ id: 0, target: "#fittings", completed: "false", caption: "Fitting", title: "UPLOAD FRONT IMAGE", myclass: "fimage" },
				{ id: 1, target: "#fittings", completed: "false", caption: "Fitting", title: "UPLOAD SIDE IMAGE", myclass: "fimage" },
				{ id: 2, target: "#fittings", completed: "false", caption: "Fitting", title: "UPLOAD BACK IMAGE", myclass: "fimage" },
				{ id: 3, target: "#fittings", completed: "false", caption: "Fitting", title: "ADDITIONAL IMAGES", myclass: "fimage" },
				{ id: 4, target: "#fittings", completed: "false", caption: "Fitting", title: "NOTES FRONT IMAGE", myclass: "fnote" },
				{ id: 5, target: "#fittings", completed: "false", caption: "Fitting", title: "NOTES SIDE IMAGE", myclass: "fnote" },
				{ id: 6, target: "#fittings", completed: "false", caption: "Fitting", title: "NOTES BACK IMAGE", myclass: "fnote" },
				{ id: 7, target: "#fittings", completed: "false", caption: "Fitting", title: "UPLOAD VIDEO", myclass: "fvideo" },
				{ id: 8, target: "#fittings", completed: "false", caption: "Fitting", title: "ASSIGN TAILOR", myclass: "ftailor" },
			]);


			this.currentStep = ko.observable();
			this.currentStep(this.workflow()[0]);
			this.currentStep.subscribe(function (data) {
				var whichGarment = that.selectedVariantFitting().id;
				for (x in that.workflow()) {
					if (that.workflow()[x] == data) {
						break;
					}
				}
				if (x == 4) {
					if (that.frontImage.allMediaHavetags()) {
						that.skipAppear(false);
					} else {
						that.skipAppear(true);
					}
				} else if (x == 5) {
					if (that.sideImage.allMediaHavetags()) {
						that.skipAppear(false);
					} else {
						that.skipAppear(true);
					}
				} else if (x == 6) {
					if (that.backImage.allMediaHavetags()) {
						that.skipAppear(false);
					} else {
						that.skipAppear(true);
					}
				}
				console.log(JSON.stringify(data));
				console.log(JSON.stringify(data.target));
				posChangePage(data.target);
				$('.cloneDialog').remove();
				that.renderFitting();
			});

			this.isStepCompleted = ko.computed(function () {
				for (var ind in that.workflow()) {
					if (that.workflow()[ind].id == that.currentStep().id) {
						if (that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending") {
							return true;
						} else {
							return false;
						}
					}
				}
				return false;
			});


			this.stepCaption = ko.computed(function () {
				return that.currentStep().caption;
			});

			this.stepTitle = ko.computed(function () {
				return that.currentStep().title;
			});


			this.completion = ko.observable(0);
			this.variantNameFitting = ko.observableArray([{ id: 0, title: "Fitting 1", completed: ko.observable(false) }]);
			this.selectedVariantFitting = ko.observable(this.variantNameFitting()[0]);
			this.selectedVariantFitting.subscribe(function (data) {
				that.selectedVariantFitting();
				//	posChangePage("#fittings");
				//	$('.cloneDialog').remove();
				that.renderFitting();

				var cs = that.currentStep().id;
				that.currentStep(that.workflow()[cs]);
			});

			this.nextStepCaption = ko.computed(function () {
				var tWork = that.workflow();
				var tInd = -1;
				for (var ind in tWork) {
					if (tWork[ind].completed !== "true") {
						tInd = ind;
						break;
					}
				}
				if (tInd != -1) {
					return tWork[tInd].title;
				} else {
					return " Not available ";
				}
			});

			this.previousStepCaption = ko.computed(function () {
				if (that.currentStep().id !== 0) {
					return that.workflow()[that.currentStep().id - 1].title;
				} else {
					return " Not available ";
				}
			});


			this.FittingCustomer = ko.observable('');
			this.FittingCustomer.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingCustomer = data;
				that.flushModel();
			});

			this.FittingGarment = ko.observable('');
			this.FittingGarment.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarment = data;
				that.flushModel();
			});

			this.FittingGarmentFabric = ko.observable('');
			this.FittingGarmentFabric.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarmentFabric = data;
				that.flushModel();
			});


			this.FittingGarmentFabricImage = ko.observable('');
			this.FittingGarmentFabricImage.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarmentFabricImage = data;
				that.flushModel();
			});

			this.FittingGarmentBarcode = ko.observable('');
			this.FittingGarmentBarcode.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingGarmentBarcode = data;
				that.flushModel();
			});

			this.FittingId = ko.observable('');
			this.FittingId.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingId = data;
				that.flushModel();
			});

			this.FittingOrderId = ko.observable('');
			this.FittingOrderId.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingOrderId = data;
				that.flushModel();
			});

			this.FittingOrderProductId = ko.observable('');
			this.FittingOrderProductId.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingOrderProductId = data;
				that.flushModel();
			});

			this.FittingTailors = ko.observable(that.dsRegistry.getDatasource('tailorsDS').getStore());
			this.FittingTailor = ko.observable('');
			this.FittingTailor.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingTailor = data;
				}

				that.flushModel();
			});
			this.FittingDeliveryDay = ko.observable('');
			this.FittingDeliveryDay.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingDeliveryDay = data;
				}

				that.flushModel();
			});
			this.FittingDeliveryMonth = ko.observable('');
			this.FittingDeliveryMonth.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingDeliveryMonth = data;
				}

				that.flushModel();
			});

			this.FittingDeliveryDate = ko.observable('');
			this.FittingDeliveryDate.subscribe(function (data) {
				for (ind in that.FittingData) {
					that.FittingData[ind].FittingDeliveryDate = data;
				}
				that.flushModel();
			});

			this.FittingTailorNotes = ko.observable('');
			this.FittingTailorNotes.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailorNotes = data;
				that.flushModel();
			});

			this.FittingVideoPreview = ko.observable('');
			this.FittingVideoPreview.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingVideoPreview = data;
				console.log('Fitting video preview data ');
				console.log(that.FittingVideoPreview());
				console.log(data);
				that.flushModel();

			});

			this.SkippedVideo = ko.observable(false);
			this.SkippedVideo.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedVideo = data;
				that.flushModel();
			});

			this.SkippedVideoNotes = ko.observable(false);
			this.SkippedVideoNotes.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedVideoNotes = data;
				that.flushModel();
			});


			this.NoVideo = ko.observable(false);
			this.NoVideo.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoVideo = data;
				that.flushModel();
			});

			this.SkippedFrontTags = ko.observable(false);
			this.SkippedFrontTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedFrontTags = data;
				that.flushModel();
			});
			this.SkippedSideTags = ko.observable(false);
			this.SkippedSideTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedSideTags = data;
				that.flushModel();
			});
			this.SkippedBackTags = ko.observable(false);
			this.SkippedBackTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].SkippedBackTags = data;
				that.flushModel();
			});
			this.NoFrontTags = ko.observable(false);
			this.NoFrontTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoFrontTags = data;
				that.flushModel();
			});
			this.NoSideTags = ko.observable(false);
			this.NoSideTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoSideTags = data;
				that.flushModel();
			});
			this.NoBackTags = ko.observable(false);
			this.NoBackTags.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].NoBackTags = data;
				that.flushModel();
			});
			this.isActive = ko.observable(true);
			this.isActive.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].isActive = data;
				that.flushModel();
			});
			this.isCompleted = ko.observable(false);
			this.isCompleted.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].isCompleted = data;
				that.flushModel();
			});

			this.AdditionalImagesStepComplete = ko.observable(false);
			this.AdditionalImagesStepComplete.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].AdditionalImagesStepComplete = data;
				that.flushModel();
			});


			this.FittingVideoNotes = ko.observableArray();
			this.FittingVideoNotes.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingVideoNotes = data;
				that.flushModel();
			});

			this.FittingCloned = ko.observable(0);
			this.FittingCloned.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingCloned = data;
				that.flushModel();
			});

			this.CalendarDate = ko.observable(new Date());
			this.CalendarDate.subscribe(function (data) {
				that.FittingData[that.getRow(that.selectedVariantFitting().id)].CalendarDate = data;
				that.flushModel();
			});

			this.FittingVideoNotesSingle = ko.observable('');

			this.addComment = function () {
				var temp = [];
				temp = that.FittingVideoNotes();
				console.log(temp);
				if (that.FittingVideoNotesSingle()) {
					if (that.editFittingVideoNotes() < 0) {
						temp.push(that.FittingVideoNotesSingle());
						that.FittingVideoNotesSingle(null);
					}
					else {
						temp[that.editFittingVideoNotes()] = that.FittingVideoNotesSingle();
						that.FittingVideoNotesSingle(null);
						that.editFittingVideoNotes(-1);
					}
					that.FittingVideoNotes(temp);
				}
			}

			this.deleteComment = function (index) {
				if (confirm("Do you wish to delete delete this note?")) {
					for (x in that.FittingVideoNotes()) {
						if (that.FittingVideoNotes()[x] == index) {
							index = x;

						}

					}
					that.FittingVideoNotes.splice(index, 1);
					that.editFittingVideoNotes(-1);
				}
			}

			this.FittingVideoViewModel = function (thepath, thenotes) {
				var self = this;
				this.path = ko.observable(thepath);
				this.notes = ko.observable(thenotes);
			};

			this.assignTailorText = function () {
				document.getElementById("roundup").innerHTML = '';
				var a = '';


				document.getElementById("roundup").innerHTML = a;
				if ("createEvent" in document) {
					var evt = document.createEvent("HTMLEvents");
					evt.initEvent("change", false, true);
					document.getElementById("roundup").dispatchEvent(evt);
					//	document.getElementById("Test").dispatchEvent(evt);
				} else {
					document.getElementById("roundup").fireEvent("onchange");
					//	document.getElementById("Test").fireEvent("onchange"); 
				}

				return a;
			};

			this.ImageTagsText = function (id) {
				var a = "";
				//for(i in that.variantNameFitting()){
				var whichGarment = id;
				var fronttags = JSON.parse(localStorage.getItem('FittingFrontImageTags' + whichGarment));//  [{"image_id":"1400","left":73.994140625,"top":12.560059502720833,"text":"asasdasdasdasd"},{"image_id":"1400","left":69.6875,"top":34.89892669022083,"text":"bbbbbbbbbbbbbbbbbb"}]
				var sidetags = JSON.parse(localStorage.getItem('FittingSideImageTags' + whichGarment));
				var backtags = JSON.parse(localStorage.getItem('FittingBackImageTags' + whichGarment));
				var customtags = JSON.parse(localStorage.getItem('FittingCustomImageTags' + whichGarment));

				//a += that.variantNameFitting()[i].title;
				var count = 1;
				var temp = localStorage.getItem('FittingFrontImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				if (temp) {
					a += '<div class="grid_12"><div class="grid_4 centerme"><span>Front Image</span><img src="' + temp[0].thumbnail + '" width="95%"/><ul>';
					for (var x in fronttags) {
						a += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + fronttags[x].text + '</span></li>';
						count++;
					}
					a += '</ul></div>';
				}

				count = 1;
				temp = localStorage.getItem('FittingSideImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				if (temp) {
					a += '<div class="grid_4 centerme"><span>Side Image</span><img src="' + temp[0].thumbnail + '" width="95%"/><ul>';
					for (var x in sidetags) {
						a += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + sidetags[x].text + '</span></li>';
						count++;
					}
					a += '</ul></div>';
				}

				count = 1;
				temp = localStorage.getItem('FittingBackImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				if (temp) {
					a += '<div class="grid_4 centerme"><span>Back Image</span><img src="' + temp[0].thumbnail + '" width="95%"/><ul>';
					for (var x in backtags) {
						a += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + backtags[x].text + '</span></li>';
						count++;
					}
					a += '</ul></div></div>';
				}

				var temp = localStorage.getItem('FittingCustomImagePhoto' + whichGarment);
				temp = JSON.parse(temp);
				for (var i in temp) {
					var b = '';
					count = 1;
					for (var x in customtags) {
						if (customtags[x].image_id == temp[i].image_id) {
							b += '<li><span class="imagenotes_count"> ' + count + ' </span>' + '<span class="imagenotes_text">' + customtags[x].text + '</span></li>';
							count++;
						}
					}
					b += '</ul></div>';
					a += '<div class="grid_4 centerme"><span>Additional Image</span><img src="' + temp[i].thumbnail + '" width="95%"/><ul>';
					a += b;

				}

				return a;
			}



			this.modifyDropdowns = function () {

				var whichGarment = that.selectedVariantFitting().id;

				var tailors = this.dsRegistry.getDatasource('tailorsDS').getStore();
				var days = orderItem.days();
				var months = orderItem.months();
				//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingTailor);     	
				try {
					if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailor == undefined || that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailor == '') {
						document.getElementsByName("FittingTailor")[0].getElementsByTagName('option')[0].selected = true;
					} else {
						for (var a = 0; a < tailors.length; a++) {
							if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingTailor.last_name == tailors[a].last_name) {
								document.getElementsByName("FittingTailor")[0].getElementsByTagName('option')[a + 1].selected = true;
							}
						}
					}
				} catch (e) {
					;
				}
				//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryDay);			
				try {
					if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryDay == undefined || that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryDay == '') {
						document.getElementsByName("FittingDeliveryDay")[0].getElementsByTagName('option')[0].selected = true;
					} else {
						for (var a = 0; a < days.length; a++) {
							if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryDay == days[a]) {
								document.getElementsByName("FittingDeliveryDay")[0].getElementsByTagName('option')[a + 1].selected = true;
							}
						}
					}
				} catch (e) {
					//console.log("day error: " + e);
				}
				//console.log("that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth: " + that.FittingData[ that.getRow(that.selectedVariantFitting().id)  ].FittingDeliveryMonth);			
				try {
					if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryMonth == undefined || that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryMonth == '') {
						document.getElementsByName("FittingDeliveryMonth")[0].getElementsByTagName('option')[0].selected = true;
					} else {
						for (var a = 0; a < months.length; a++) {
							if (that.FittingData[that.getRow(that.selectedVariantFitting().id)].FittingDeliveryMonth == months[a].id) {
								document.getElementsByName("FittingDeliveryMonth")[0].getElementsByTagName('option')[a + 1].selected = true;
							}
						}
					}
				} catch (e) {
					//console.log("month error: " + e);
				}

				try {
					if ("createEvent" in document) {
						var evt = document.createEvent("HTMLEvents");
						evt.initEvent("change", true, true);
						document.getElementsByName("FittingTailor")[0].dispatchEvent(evt);
						document.getElementsByName("FittingDeliveryDay")[0].dispatchEvent(evt);
						document.getElementsByName("FittingDeliveryMonth")[0].dispatchEvent(evt);
					} else {
						document.getElementsByName("FittingTailor")[0].fireEvent("onchange");
						document.getElementsByName("FittingDeliveryDay")[0].fireEvent("onchange");
						document.getElementsByName("FittingDeliveryMonth")[0].fireEvent("onchange");
					}
				} catch (e) {
					console.log(e);
				}

				return false;
			};



			///////////////////////////		SYNC PART	///////////////////////////	
			//////////////////////////////////////////////////////////////////////

			this.sync = function () {

				var data = [];
				var completedFittings = true;
				var spinner = document.getElementById('loading_jp');
				var error = false;
				var customphotos = "";
				spinner.style.display = "block";
				var ind = '';
				var allComplete = true;
				that.checkSteps()
				for (test in that.variantNameFitting()) {
					console.log(that.variantNameFitting()[test]);
					if (that.variantNameFitting()[test].completed() == false && orderItem.fittingsVM.FittingData[that.variantNameFitting()[test].id].FittingId == '') {
						allComplete = false;
						break;
					}
				}
				for (ind in that.variantNameFitting()) {
					var whichGarment = that.variantNameFitting()[ind].id;

					console.log("getting " + 'FittingFrontImagePhoto' + whichGarment);

					var frontImage = that.FittingData[whichGarment].frontImage.getSyncInfo();
					var sideImage = that.FittingData[whichGarment].sideImage.getSyncInfo();
					var backImage = that.FittingData[whichGarment].backImage.getSyncInfo();
					var customImages = that.FittingData[whichGarment].customImages.getSyncInfo();

					var customerString = orderItem.custSelectVM.selectedCustomer();
					var fittingIdPost = orderItem.fittingsVM.FittingData[whichGarment].FittingId;

					try {
						var date = new Date();
						var yearnow = date.getFullYear();
						var monthnow = date.getMonth() + 1;
						var daynow = date.getDate();
						var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
						var day = this.FittingDeliveryDate().getDate();
						var month = this.FittingDeliveryDate().getMonth() + 1;
						var year = this.FittingDeliveryDate().getFullYear();
						var dateToCompare = new Date(month + "/" + day + "/" + year);
						var dateToPost = day + "-" + month + "-" + year;
					} catch (e) {
						var dateToPost = null;
						var dateToCompare = null;
					}
					var diffDays = -1;
					try {
						var timeDiff = dateToCompare.getTime() - dateNow.getTime();
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						console.log("diffDays: " + diffDays);
					} catch (e) { ; }
					var fittingvideos = ko.observableArray();
					fittingvideos.push(new orderItem.FittingVM.FittingVideoViewModel(orderItem.fittingsVM.FittingData[whichGarment].FittingVideo, JSON.stringify(orderItem.fittingsVM.FittingData[whichGarment].FittingVideoNotes)));

					var tailorIdPost = "";
					if (this.FittingTailor() != undefined) {
						if (this.FittingTailor().user_id != undefined) {
							tailorIdPost = this.FittingTailor().user_id;
						}
					}
					var foundnotcompletedstep = false;
					for (var x = 0; x < this.workflow().length - 1; x++) {
						if (this.workflow()[x].completed == "false") {
							foundnotcompletedstep = true;
							break;
						}
					}

					var pendingFrontTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == true) {
						pendingFrontTagsPost = "1";
					}
					var pendingSideTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == true) {
						pendingSideTagsPost = "1";
					}
					var pendingBackTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == true) {
						pendingBackTagsPost = "1";
					}

					var pendingVideoPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == true) {
						pendingVideoPost = "1";
					}

					var pendingVideoNotesPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideoNotes == true) {
						pendingVideoNotesPost = "1";
					}



					//console.log("this.FittingVideo(): " + this.FittingVideo());
					if (dateToPost == "") {
						customAlert("Please select a date!");
						error = true;
						break;
					} else if (tailorIdPost == "") {
						customAlert("Please select a tailor and a date!");
						error = true;
						break;
					} else if (diffDays < 0) {
						customAlert("There has been an error with the selected date. <br/> Try again or select another.");
						error = true;
						break;
					} else if (diffDays > 120) {
						customAlert("The selected date is more than 4 months away! <br/> Please select a closer date.");
						error = true;
						break;
					} else if (!that.FittingData[whichGarment].frontImage.hasMedia()) {
						customAlert("Please upload a front image");
						error = true;
						this.currentStep(this.workflow()[0]);
						break;
					} else if (!that.FittingData[whichGarment].sideImage.hasMedia()) {
						customAlert("Please upload a side image");
						error = true;
						this.currentStep(this.workflow()[1]);
						break;
					} else if (!that.FittingData[whichGarment].backImage.hasMedia()) {
						customAlert("Please upload a back image");
						error = true;
						this.currentStep(this.workflow()[2]);
						break;
					} else if (!that.FittingData[whichGarment].frontImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoFrontTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == false) {
						error = true;
						customAlert("Please enter front image notes");
						this.currentStep(this.workflow()[4]);
						break;
					} else if (!that.FittingData[whichGarment].sideImage.allMediaHavetags() && this.NoSideTags() == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == false) {
						error = true;
						customAlert("Please enter side image notes");
						this.currentStep(this.workflow()[5]);
						break;
					} else if (!that.FittingData[whichGarment].backImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoBackTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == false) {
						error = true;
						customAlert("Please enter back image notes");
						this.currentStep(this.workflow()[6]);
						break;
					} else if (!orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.hasMedia() && orderItem.fittingsVM.FittingData[whichGarment].NoVideo == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == false) {
						error = true;
						customAlert("Please enter the fitting video");
						this.currentStep(this.workflow()[7]);
						break;
					} else {
						var spinner = document.getElementById('loading_jp');
						spinner.style.display = "block";
						data[ind] = {
							device_id: typeof device != "undefined" ? device.uuid : "",
							user_id: authCtrl.userInfo.user_id,
							customer: customerString,
							fitting_id: fittingIdPost,
							order_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderId,//this.selected_order.order_id,
							orders_products_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderProductId,
							barcode_id: orderItem.fittingsVM.FittingData[whichGarment].FittingGarmentBarcode,
							garment: orderItem.fittingsVM.FittingData[whichGarment].FittingGarment,
							pendingFrontNotes: pendingFrontTagsPost,
							pendingSideNotes: pendingSideTagsPost,
							pendingBackNotes: pendingBackTagsPost,
							pendingVideo: pendingVideoPost,
							pendingVideoNotes: pendingVideoNotesPost,
							tailor: {
								user_id: tailorIdPost,
								delivery_date: dateToPost,
								notes: that.FittingTailorNotes()
							},
							front_image: frontImage,
							side_image: sideImage,
							back_image: backImage,
							custom_images: customImages,
							fitting_videos: orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.getSyncInfo(),
							cloned: orderItem.fittingsVM.FittingData[whichGarment].FittingCloned
							//fitting_id, order_id, garment_id, tailor(user_id, delivery_date, notes), fitting_images(id, name), fitting_tags(left, top, text), fitting_videos(path, notes).
						};


					}



				}
				console.log(data);
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");
				console.log("datatopost: " + datatopost);
				if (!error) {
					$.ajax({
						type: 'POST',
						timeout: 45000, // sets timeout to 45 seconds
						url: BUrl + 'client_fittings/fitting_endpoint5',
						data: {
							data: datatopost,
						},
						success: function (dataS) {
							//dataS = JSON.parse(dataS);
							console.log(dataS);
							if (dataS) {
								//if(orderItem.fittingsVM.FittingData[whichGarment].FittingId == ''){
								customAlert("Fitting succesfully added");
								//}else{
								//	customAlert("Fitting succesfully edited");
								//}
								//that.FittingId(dataS.fitting);
								//var cs = that.currentStep().id;	
								//that.workflow()[cs].completed = "true";

								//if(that.variantNameFitting().length == 1){
								//}else{
								//	that.goToNextNotSubmittedFitting();
								//}
							} else {
								console.log('fail');
								customAlert("there is a network issue. Please try again later");
								$.jGrowl(dataS.msg);
							}
							spinner.style.display = "none";
						},
						error: function (error) {
							console.log(JSON.stringify(error));
							customAlert("");
							spinner.style.display = "none";
							completedFittings = false;
						},
						async: false
					});
					if (completedFittings) {
						posChangePage('#customerGarmentsFittingsList');
					}
				}
				else {
					that.selectedVariantFitting(that.variantNameFitting()[ind]);
				}
				spinner.style.display = "none";
				// }
			}

			this.sync_edit = function () {

				var data = [];
				var completedFittings = true;
				var spinner = document.getElementById('loading_jp');
				var error = false;
				var customphotos = "";
				spinner.style.display = "block";
				that.checkSteps()
				var allComplete = true;
				var test = '';
				for (test in that.variantNameFitting()) {
					console.log(that.variantNameFitting()[test]);
					if (that.variantNameFitting()[test].completed() == false && orderItem.fittingsVM.FittingData[that.variantNameFitting()[test].id].FittingId == '') {
						allComplete = false;
						break;
					}
				}
				for (ind in that.variantNameFitting()) {
					var whichGarment = that.variantNameFitting()[ind].id;

					console.log("getting " + 'FittingFrontImagePhoto' + whichGarment);

					var frontImage = that.FittingData[whichGarment].frontImage.getSyncInfo();
					var sideImage = that.FittingData[whichGarment].sideImage.getSyncInfo();
					var backImage = that.FittingData[whichGarment].backImage.getSyncInfo();
					var customImages = that.FittingData[whichGarment].customImages.getSyncInfo();


					var customerString = orderItem.custSelectVM.selectedCustomer();
					var fittingIdPost = orderItem.fittingsVM.FittingData[whichGarment].FittingId;

					try {
						var date = new Date();
						var yearnow = date.getFullYear();
						var monthnow = date.getMonth() + 1;
						var daynow = date.getDate();
						var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
						var day = this.FittingDeliveryDate().getDate();
						var month = this.FittingDeliveryDate().getMonth() + 1;
						var year = this.FittingDeliveryDate().getFullYear();
						var dateToCompare = new Date(month + "/" + day + "/" + year);
						var dateToPost = day + "-" + month + "-" + year;
					} catch (e) {
						var dateToPost = null;
						var dateToCompare = null;
					}
					var diffDays = -1;
					try {
						var timeDiff = dateToCompare.getTime() - dateNow.getTime();
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						console.log("diffDays: " + diffDays);
					} catch (e) { ; }
					var fittingvideos = ko.observableArray();
					fittingvideos.push(new orderItem.FittingVM.FittingVideoViewModel(orderItem.fittingsVM.FittingData[whichGarment].FittingVideo, JSON.stringify(orderItem.fittingsVM.FittingData[whichGarment].FittingVideoNotes)));

					var tailorIdPost = "";
					if (this.FittingTailor() != undefined) {
						if (this.FittingTailor().user_id != undefined) {
							tailorIdPost = this.FittingTailor().user_id;
						}
					}
					var foundnotcompletedstep = false;
					for (var x = 0; x < this.workflow().length - 1; x++) {
						if (this.workflow()[x].completed == "false") {
							foundnotcompletedstep = true;
							break;
						}
					}

					var pendingFrontTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == true) {
						pendingFrontTagsPost = "1";
					}
					var pendingSideTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == true) {
						pendingSideTagsPost = "1";
					}
					var pendingBackTagsPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == true) {
						pendingBackTagsPost = "1";
					}

					var pendingVideoPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == true) {
						pendingVideoPost = "1";
					}

					var pendingVideoNotesPost = "0";
					if (orderItem.fittingsVM.FittingData[whichGarment].SkippedVideoNotes == true) {
						pendingVideoNotesPost = "1";
					}


					//console.log("this.FittingVideo(): " + this.FittingVideo());
					if (dateToPost == "") {
						customAlert("Please select a date!");
						error = true;
						break;
					} else if (tailorIdPost == "") {
						customAlert("Please select a tailor and a date!");
						error = true;
						break;
					} else if (diffDays < 0) {
						customAlert("There has been an error with the selected date. <br/> Try again or select another.");
						error = true;
						break;
					} else if (diffDays > 120) {
						customAlert("The selected date is more than 4 months away! <br/> Please select a closer date.");
						error = true;
						break;
					} else if (!that.FittingData[whichGarment].frontImage.hasMedia()) {
						customAlert("Please upload a front image");
						error = true;
						this.currentStep(this.workflow()[0]);
						break;
					} else if (!that.FittingData[whichGarment].sideImage.hasMedia()) {
						customAlert("Please upload a side image");
						error = true;
						this.currentStep(this.workflow()[1]);
						break;
					} else if (!that.FittingData[whichGarment].backImage.hasMedia()) {
						customAlert("Please upload a back image");
						error = true;
						this.currentStep(this.workflow()[2]);
						break;
					} else if (!that.FittingData[whichGarment].frontImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoFrontTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedFrontTags == false) {
						error = true;
						customAlert("Please enter front image notes");
						this.currentStep(this.workflow()[4]);
						break;
					} else if (!that.FittingData[whichGarment].sideImage.allMediaHavetags() && this.NoSideTags() == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedSideTags == false) {
						error = true;
						customAlert("Please enter side image notes");
						this.currentStep(this.workflow()[5]);
						break;
					} else if (!that.FittingData[whichGarment].backImage.allMediaHavetags() && orderItem.fittingsVM.FittingData[whichGarment].NoBackTags == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedBackTags == false) {
						error = true;
						customAlert("Please enter back image notes");
						this.currentStep(this.workflow()[6]);
						break;
					} else if (!orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.hasMedia() && orderItem.fittingsVM.FittingData[whichGarment].NoVideo == false && orderItem.fittingsVM.FittingData[whichGarment].SkippedVideo == false) {
						error = true;
						customAlert("Please enter the fitting video");
						this.currentStep(this.workflow()[7]);
						break;
					} else {
						var spinner = document.getElementById('loading_jp');
						spinner.style.display = "block";
						data[ind] = {
							device_id: typeof device != "undefined" ? device.uuid : "",
							user_id: authCtrl.userInfo.user_id,
							customer: customerString,
							fitting_id: fittingIdPost,
							order_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderId,//this.selected_order.order_id,
							orders_products_id: orderItem.fittingsVM.FittingData[whichGarment].FittingOrderProductId,
							barcode_id: orderItem.fittingsVM.FittingData[whichGarment].FittingGarmentBarcode,
							garment: orderItem.fittingsVM.FittingData[whichGarment].FittingGarment,
							pendingFrontNotes: pendingFrontTagsPost,
							pendingSideNotes: pendingSideTagsPost,
							pendingBackNotes: pendingBackTagsPost,
							pendingVideo: pendingVideoPost,
							pendingVideoNotes: pendingVideoNotesPost,
							tailor: {
								user_id: tailorIdPost,
								delivery_date: dateToPost,
								notes: that.FittingTailorNotes()
							},
							front_image: frontImage,
							side_image: sideImage,
							back_image: backImage,
							custom_images: customImages,
							fitting_videos: orderItem.fittingsVM.FittingData[whichGarment].FittingVideo.getSyncInfo(),
							cloned: orderItem.fittingsVM.FittingData[whichGarment].FittingCloned
							//fitting_id, order_id, garment_id, tailor(user_id, delivery_date, notes), fitting_images(id, name), fitting_tags(left, top, text), fitting_videos(path, notes).
						};


					}



				}
				console.log(data);
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");
				console.log("datatopost: " + datatopost);
				if (!error) {
					$.ajax({
						type: 'POST',
						timeout: 45000, // sets timeout to 45 seconds
						url: BUrl + 'client_fittings/fitting_edit_endpoint4',
						data: {
							data: datatopost,
						},
						success: function (dataS) {
							//dataS = JSON.parse(dataS);
							console.log(dataS);
							if (dataS) {
								//if(orderItem.fittingsVM.FittingData[whichGarment].FittingId == ''){
								customAlert("Fitting succesfully added");
								//}else{
								//	customAlert("Fitting succesfully edited");
								//}
								//that.FittingId(dataS.fitting);
								//var cs = that.currentStep().id;	
								//that.workflow()[cs].completed = "true";

								//if(that.variantNameFitting().length == 1){
								//}else{
								//	that.goToNextNotSubmittedFitting();
								//}
							} else {
								console.log('fail');
								customAlert("there is a network issue. Please try again later");
								$.jGrowl(dataS.msg);
							}
							spinner.style.display = "none";
						},
						error: function (error) {
							console.log(JSON.stringify(error));
							customAlert("");
							spinner.style.display = "none";
							completedFittings = false;
						},
						async: false
					});
					if (completedFittings) {
						posChangePage('#customerGarmentsFittingsList');
					}
				}

				spinner.style.display = "none";
				//}
			}

		},


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	


		previousStep: function () {
			if (this.currentStep().id !== 0) {
				this.previousStepEnabled(true);
				this.currentStep(this.workflow()[this.currentStep().id - 1]);
			}
		},

		flushModel: function () {
			this.dsRegistry.getDatasource(this.subscribed).setStore({
				"Fitting": this.FittingData
			}, true);
		},

		getVariant: function (id) {
			var toreturn = this.FittingData[0];
			for (var ind in this.FittingData) {
				if (this.FittingData[ind].variantId == id) {
					toreturn = this.FittingData[ind];
				}
			}
			return toreturn;
		},

		getRow: function (id) {
			for (var ind in this.FittingData) {
				if (this.FittingData[ind].variantId == id) {
					return ind;
				}
			}
			return -1;
		},

		addVariantFitting: function (name) {
			//console.log(this.variantNameFitting);
			if (name == "Shirt") {
				this.NumberShirts++;
			}
			if (this.variantNameFitting()[0].title != "Fitting 1") {
				this.FittingDataAID += 1;
				var newid = this.FittingDataAID + 1;
				var vname = name;
				var tObj = jQuery.extend(true, {}, this.getVariant(this.getRow(this.selectedVariantFitting().id))); //CLONE Object
				this.variantNameFitting.push({ title: vname, id: this.FittingDataAID, completed: ko.observable(false) });
				tObj.variantId = this.FittingDataAID;
				this.flushModel();
			} else {
				this.variantNameFitting()[0].title = name;
			}
		},

		addVariantsFittings: function () {
			for (var x in orderItem.fittingsVM.FittingData) {
				//	console.log("addVariantsFittings " + x);

				if (x != 0) {
					this.FittingDataAID += 1;
					var vname = orderItem.fittingsVM.FittingData[x].FittingGarment;
					if (vname == "Shirt") {
						this.NumberShirts++;
					}
					var tObj = jQuery.extend(true, {}, this.getVariant(this.getRow(this.selectedVariantFitting().id))); //CLONE Object
					this.variantNameFitting.push({ title: vname, id: this.FittingDataAID, completed: ko.observable(false) });
					tObj.variantId = this.FittingDataAID;
					this.flushModel();
				} else {
					var vname = orderItem.fittingsVM.FittingData[x].FittingGarment;
					if (vname == "Shirt") {
						this.NumberShirts++;
					}
					var tObj = jQuery.extend(true, {}, this.getVariant(this.getRow(this.selectedVariantFitting().id))); //CLONE Object
					this.variantNameFitting()[x].title = vname;
					tObj.variantId = this.FittingDataAID;
					this.flushModel();
				}

			}
		},

		openError: function () {
			var garment = {};
			garment.garment_id = this.FittingOrderProductId();

			switch (this.FittingGarment().toLowerCase()) {
				case 'pants':
					garment.garment_type = '1';
					break;
				case 'jacket':
					garment.garment_type = '4';
					break;
				case 'shirt':
					garment.garment_type = '2';
					break;
				case 'vest':
					garment.garment_type = '3';
					break;
				case 'jacket/s':
					garment.garment_type = '4';
					break;
				case 'pant/s':
					garment.garment_type = '1';
					break;
			}
			errorReportVM.buildError(this.FittingCustomer(), this.FittingOrderId(), garment);
		},

		digestData: function (data) {
			this.FittingData = data.Fitting;
			this.renderView();
		},

		renderView: function () {
			this.renderFitting();
		},

		renderFitting: function () {
			this.modifyDropdowns();

			var whichGarment = this.selectedVariantFitting().id;

			//Get selected Variant
			try {
				var tData = this.getVariant(this.selectedVariantFitting().id);
				if (tData != null) {
					//Update observables
					if (typeof (tData.frontImage) != "undefined") {
						this.frontImage = tData.frontImage;
						this.frontImage.setCustomPreview(this.previewFrontImage);
						this.frontImage.setCustomPreviewValue();

					}
					if (typeof (tData.sideImage) != "undefined") {
						this.sideImage = tData.sideImage;
						this.sideImage.setCustomPreview(this.previewSideImage);
						this.sideImage.setCustomPreviewValue();
					}
					if (typeof (tData.backImage) != "undefined") {
						this.backImage = tData.backImage;
						this.backImage.setCustomPreview(this.previewBackImage);
						this.backImage.setCustomPreviewValue();
					}
					if (typeof (tData.customImages) != "undefined") {
						this.customImages = tData.customImages;
						this.customImages.setCustomPreview(this.previewCustomImage);
						this.customImages.setCustomPreviewValue();
					}
					if (typeof (tData.FittingVideo) != "undefined") {
						this.FittingVideo = tData.FittingVideo;
						this.FittingVideo.setCustomPreview(this.previewCustomVideo);
						this.FittingVideo.setCustomPreviewValue();
					}
					this.updateSkipButtons();
					if (typeof (tData.FittingCustomer) != "undefined") { this.FittingCustomer(tData.FittingCustomer); }
					if (typeof (tData.FittingGarment) != "undefined") { this.FittingGarment(tData.FittingGarment); }
					if (typeof (tData.FittingGarmentFabric) != "undefined") { this.FittingGarmentFabric(tData.FittingGarmentFabric); }
					if (typeof (tData.FittingGarmentFabricImage) != "undefined") { this.FittingGarmentFabricImage(tData.FittingGarmentFabricImage); }
					if (typeof (tData.FittingGarmentBarcode) != "undefined") { this.FittingGarmentBarcode(tData.FittingGarmentBarcode); }
					if (typeof (tData.FittingId) != "undefined") { this.FittingId(tData.FittingId); }
					if (typeof (tData.FittingOrderId) != "undefined") { this.FittingOrderId(tData.FittingOrderId); }
					if (typeof (tData.FittingOrderProductId) != "undefined") { this.FittingOrderProductId(tData.FittingOrderProductId); }
					if (typeof (tData.FittingTailor) != "undefined") { this.FittingTailor(tData.FittingTailor); }
					if (typeof (tData.FittingDeliveryDay) != "undefined") { this.FittingDeliveryDay(tData.FittingDeliveryDay); }
					if (typeof (tData.FittingDeliveryMonth) != "undefined") { this.FittingDeliveryMonth(tData.FittingDeliveryMonth); }
					if (typeof (tData.FittingDeliveryDate) != "undefined") { this.FittingDeliveryDate(tData.FittingDeliveryDate); }
					if (typeof (tData.CalendarDate) != "undefined") { this.CalendarDate(tData.CalendarDate); }
					if (typeof (tData.FittingTailorNotes) != "undefined") { this.FittingTailorNotes(tData.FittingTailorNotes); }
					//if (typeof(tData.FittingVideo)    		!= "undefined") {this.FittingVideo(tData.FittingVideo);}
					//if (typeof(tData.FittingVideoPreview)    		!= "undefined") {this.FittingVideoPreview(tData.FittingVideoPreview);}
					if (typeof (tData.SkippedVideo) != "undefined") { this.SkippedVideo(tData.SkippedVideo); }
					if (typeof (tData.SkippedVideoNotes) != "undefined") { this.SkippedVideoNotes(tData.SkippedVideoNotes); }
					if (typeof (tData.NoVideo) != "undefined") { this.NoVideo(tData.NoVideo); }
					if (typeof (tData.SkippedFrontTags) != "undefined") { this.SkippedFrontTags(tData.SkippedFrontTags); }
					if (typeof (tData.SkippedSideTags) != "undefined") { this.SkippedSideTags(tData.SkippedSideTags); }
					if (typeof (tData.SkippedBackTags) != "undefined") { this.SkippedBackTags(tData.SkippedBackTags); }
					if (typeof (tData.NoFrontTags) != "undefined") { this.NoFrontTags(tData.NoFrontTags); }
					if (typeof (tData.NoSideTags) != "undefined") { this.NoSideTags(tData.NoSideTags); }
					if (typeof (tData.NoBackTags) != "undefined") { this.NoBackTags(tData.NoBackTags); }
					if (typeof (tData.isActive) != "undefined") { this.isActive(tData.isActive); }
					if (typeof (tData.isCompleted) != "undefined") { this.isCompleted(tData.isCompleted); }
					//if (typeof(tData.FittingVideoNotes)   != "undefined") {this.FittingVideoNotes(tData.FittingVideoNotes);}
					if (typeof (tData.AdditionalImagesStepComplete) != "undefined") { this.AdditionalImagesStepComplete(tData.AdditionalImagesStepComplete); }
					if (typeof (tData.FittingCloned) != "undefined") { this.FittingCloned(tData.FittingCloned); }

				}
			} catch (e) {
				;
			}

			this.checkSteps();
		}
	});

	defFitting = SimpleDatasource.extend({
		init: function (name, dsRegistry, data) {
			orderItem.MediaUploadVM.clear();
			console.log('Fitting medias length ini fitting page');
			console.log(orderItem.MediaUploadVM.medias().length);
			if (data == null || data == undefined) {
				//	orderItem.fittingsVM.addVariantFitting('new');

				console.log("data is null");
				var Fitting = {};
				Fitting.variantId = 0;

				Fitting.frontImage = new MediaType(1, null, false, '', true);
				Fitting.sideImage = new MediaType(1, null, false, '', true);
				Fitting.backImage = new MediaType(1, null, false, '', true);
				Fitting.customImages = new MediaType(1, null, true, '', true);
				Fitting.FittingCustomer = '';
				Fitting.FittingGarment = '';
				Fitting.FittingGarmentFabric = '';
				Fitting.FittingGarmentFabricImage = '';
				Fitting.FittingGarmentBarcode = '';
				Fitting.FittingId = ""//'';
				Fitting.FittingOrderId = '';
				Fitting.FittingOrderProductId = '';
				Fitting.FittingTailor = '';
				Fitting.FittingDeliveryDay = '';
				Fitting.FittingDeliveryMonth = '';
				Fitting.FittingTailorNotes = '';
				Fitting.FittingVideo = new MediaType(2, null, true, '', true);
				Fitting.CalendarDate = new Date();
				Fitting.CalendarDate.setDate(Fitting.CalendarDate.getDate() + 15);
				Fitting.FittingDeliveryDate = Fitting.CalendarDate;
				Fitting.CalendarDate = new Date();
				Fitting.SkippedVideo = false;
				Fitting.SkippedVideoNotes = false;
				Fitting.NoVideo = false;
				Fitting.SkippedFrontTags = false;
				Fitting.SkippedSideTags = false;
				Fitting.SkippedBackTags = false;
				Fitting.NoFrontTags = false;
				Fitting.NoSideTags = false;
				Fitting.NoBackTags = false;
				Fitting.isActive = true;
				Fitting.isCompleted = false;
				Fitting.AdditionalImagesStepComplete = false;
				Fitting.FittingCloned = 0;

				var iFitting = {
					Fitting: []
				};
				iFitting.Fitting.push(Fitting);
				this._super(name, iFitting, dsRegistry);

			}
			else {
				//console.log("data is not null");
				var iFitting = {
					Fitting: []
				};

				var x = 0;
				for (var a in data) {
					console.log(data);
					for (var b in data[a].products) {
						if (data[a].products[b].checked == true) {
							var c = localStorage.getItem("fittingindex");
							var Fitting = {};
							Fitting.variantId = x;

							Fitting.FittingCustomer = orderItem.custSelectVM.selectedCustomer();
							Fitting.FittingGarment = data[a].products[b].garment;
							Fitting.FittingGarmentFabric = data[a].products[b].fabric;
							Fitting.FittingGarmentFabricImage = data[a].products[b].fabricImage;
							Fitting.FittingGarmentBarcode = data[a].products[b].barcode;
							Fitting.FittingOrderId = data[a].order_id;
							Fitting.FittingOrderProductId = data[a].products[b].orders_products_id;

							if (c == -1) {
								Fitting.FittingId = '';
								Fitting.FittingTailor = '';
								Fitting.frontImage = new MediaType(1, null, false, '', true);
								Fitting.sideImage = new MediaType(1, null, false, '', true);
								Fitting.backImage = new MediaType(1, null, false, '', true);
								Fitting.customImages = new MediaType(1, null, true, '', true);
								Fitting.CalendarDate = new Date();
								Fitting.CalendarDate.setDate(Fitting.CalendarDate.getDate() + 15);
								Fitting.FittingDeliveryDate = Fitting.CalendarDate;
								Fitting.CalendarDate = new Date();
								Fitting.FittingDeliveryDay = '';
								Fitting.FittingDeliveryMonth = '';
								Fitting.FittingTailorNotes = '';
								Fitting.FittingVideo = new MediaType(2, null, true, '', true);
								Fitting.FittingVideoNotes = [];
								Fitting.SkippedVideo = false;
								Fitting.SkippedVideoNotes = false;
								Fitting.NoVideo = false;
								Fitting.SkippedFrontTags = false;
								Fitting.SkippedSideTags = false;
								Fitting.SkippedBackTags = false;
								Fitting.NoFrontTags = false;
								Fitting.NoSideTags = false;
								Fitting.NoBackTags = false;
								Fitting.isActive = true;
								Fitting.isCompleted = false;
								Fitting.AdditionalImagesStepComplete = false;
								Fitting.FittingCloned = 0;

							} else {
								Fitting.AdditionalImagesStepComplete = true;

								Fitting.FittingId = data[a].products[b].fittings[c].fitting.fitting_id;
								if (data[a].products[b].fittings[c].fitting.cloned) {
									Fitting.FittingCloned = data[a].products[b].fittings[c].fitting.cloned;
								}
								else {
									Fitting.FittingCloned = 0;
								}
								if (data[a].products[b].fittings[c].fitting.is_active == "1" && (!data[a].products[b].with_tailor || data[a].products[b].skipStep)) {
									Fitting.isActive = true;
								} else {
									Fitting.isActive = false;
								}
								if (data[a].products[b].completed == "1") {
									Fitting.isCompleted = true;
								} else {
									Fitting.isCompleted = false;
								}

								Fitting.frontImage = new MediaType(1, null, false, '', Fitting.isActive);
								Fitting.sideImage = new MediaType(1, null, false, '', Fitting.isActive);
								Fitting.backImage = new MediaType(1, null, false, '', Fitting.isActive);
								Fitting.customImages = new MediaType(1, null, true, '', Fitting.isActive);
								Fitting.FittingVideo = new MediaType(1, null, true, '', Fitting.isActive);

								console.log("data[a].products[b].fittings[c].tailor: " + JSON.stringify(data[a].products[b].fittings[c].tailor.tailor_id));
								console.log("data[a].products[b].fittings[c].tailor.tailor_id: " + data[a].products[b].fittings[c].tailor.tailor_id);
								var tailors = dsRegistry.getDatasource('tailorsDS').getStore();
								console.log("TAILORS: " + JSON.stringify(tailors));
								for (var ind = 0; ind < tailors.length; ind++) {
									if (tailors[ind].user_id == data[a].products[b].fittings[c].tailor.tailor_id) {
										Fitting.FittingTailor = tailors[ind];
										break;
									}
								}

								var date = data[a].products[b].fittings[c].tailor.delivery_date;
								var day = date.substring(date.lastIndexOf("-") + 1);
								var month = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
								var year = date.substring(0, date.indexOf("-"));
								Fitting.FittingDeliveryDay = day * 1.0;
								month = month * 1.0;
								Fitting.FittingDeliveryMonth = month * 1.0;
								var dateFormat = new Date(date);
								dateFormat.setDate(dateFormat.getDate());

								Fitting.FittingDeliveryDate = dateFormat;
								Fitting.CalendarDate = new Date();
								Fitting.FittingTailorNotes = data[a].products[b].fittings[c].tailor.tailor_notes;
								Fitting.SkippedVideo = false;
								if (data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0") {
									Fitting.SkippedFrontTags = false;
								} else {
									Fitting.SkippedFrontTags = true;
								}
								if (data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0") {
									Fitting.SkippedSideTags = false;
								} else {
									Fitting.SkippedSideTags = true;
								}
								if (data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0") {
									Fitting.SkippedBackTags = false;
								} else {
									Fitting.SkippedBackTags = true;
								}
								if (data[a].products[b].fittings[c].fitting.video_pending == "0") {
									Fitting.SkippedVideo = false;
								} else {
									Fitting.SkippedVideo = true;
								}
								if (data[a].products[b].fittings[c].fitting.video_notes_pending == "0") {
									Fitting.SkippedVideoNotes = false;
								} else {
									Fitting.SkippedVideoNotes = true;
								}
								Fitting.NoVideo = data[a].products[b].fittings[c].videos.length == 0;
								//if(data[a].products[b].fittings[c].videos.length == 0 && data[a].products[b].fittings[c].fitting.video_pending == "0"){
								for (var x in data[a].products[b].fittings[c].videos) {
									Fitting.FittingVideo.addMedia(data[a].products[b].fittings[c].videos[x].video_path, data[a].products[b].fittings[c].videos[x].video_path, data[a].products[b].fittings[c].videos[0].is_sync == 1);
									try {
										if (!data[a].products[b].fittings[c].videos[x].video_notes || data[a].products[b].fittings[c].videos[x].video_notes.length < 3) {
											console.log('Nothing to do');
										}
										else {
											var videoNotes = JSON.parse(data[a].products[b].fittings[c].videos[x].video_notes);
											for (var y in videoNotes) {
												Fitting.FittingVideo.addTag(videoNotes[y], 0);
											}
										}
									} catch (exception) {
										var videoNotes = [data[a].products[b].fittings[c].videos[x].video_notes];
										for (var y in videoNotes) {
											Fitting.FittingVideo.addTag(videoNotes[y], 0);
										}
									}

								}

								Fitting.NoFrontTags = false;
								Fitting.NoSideTags = false;
								Fitting.NoBackTags = false;

								for (var i = 0; i < data[a].products[b].fittings[c].images.length; i++) {
									try {
										var what = "";
										if (data[a].products[b].fittings[c].images[i].fitting_image_type == "1") {
											what = Fitting.frontImage;
										} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "2") {
											what = Fitting.sideImage;
										} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "3") {
											what = Fitting.backImage;
										} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "4") {
											what = Fitting.customImages;
										}



										var is_sync = data[a].products[b].fittings[c].images[i].is_sync;
										if (is_sync == '1') {
											var previewPath = data[a].products[b].fittings[c].images[i].image_local_path;
											var uploadPath = data[a].products[b].fittings[c].images[i].image_local_path;
										}
										else {
											var previewPath = data[a].products[b].fittings[c].images[i].fitting_image_path;
											var uploadPath = data[a].products[b].fittings[c].images[i].fitting_image_path;
										}
										what.addMedia(previewPath, uploadPath, is_sync == '1');
										var servertags = JSON.parse(data[a].products[b].fittings[c].images[i].fitting_image_tags);
										if (servertags != null && servertags.length > 0) {
											if (data[a].products[b].fittings[c].images[i].fitting_image_type == "4") {
												media_index = what.getMediaPreview()().length - 1;
											}
											else {
												media_index = 0;
											}
											what.addTags(servertags, 0);
										} else {
											if (data[a].products[b].fittings[c].images[i].fitting_image_type == "1") {
												if (data[a].products[b].fittings[c].fitting.front_image_notes_pending == "0") {
													Fitting.NoFrontTags = true;
												}
											} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "2") {
												if (data[a].products[b].fittings[c].fitting.side_image_notes_pending == "0") {
													Fitting.NoSideTags = true;
												}
											} else if (data[a].products[b].fittings[c].images[i].fitting_image_type == "3") {
												if (data[a].products[b].fittings[c].fitting.back_image_notes_pending == "0") {
													Fitting.NoBackTags = true;
												}
											}
										}
									} catch (e) { ; }
								}

							}
							Fitting.frontImage.updateChange();
							Fitting.sideImage.updateChange();
							Fitting.backImage.updateChange();
							Fitting.customImages.updateChange();
							x++;
							//console.log("Fitting: " + JSON.stringify(Fitting));
							iFitting.Fitting.push(Fitting);
						}
					}
				}
				this._super(name, iFitting, dsRegistry);
			}
		}
	});

	FittingList = SimpleControl.extend({
		init: function (DsRegistry) {
			this._super(DsRegistry);
			var that = this;
			this.burl = BUrl;
			this.items = ko.observable();
			this.ordersPerPage = ko.observable(10);
			this.FittingList = ko.observableArray([]);
			this.page = ko.observable(1);
			this.hasPreviousPage = ko.observable(false);
			this.hasNextPage = ko.observable(false);

			loadFitting = function (data) {
				var customers = orderItem.custSelectVM.customers();
				for (x in customers) {
					var temp = customers[x];
					if (temp.server_id == data.customer_id) {
						customersVM.systemMode = "fitting";
						orderItem.custSelectVM.selectedCustomer(temp);
						posChangePage('#customerGarmentsFittingsList');
					}
				}
			};

			this.nextPage = function () {

				if (that.items() > that.page() * that.ordersPerPage()) {
					that.hasNextPage(true);
				}
				else {
					that.hasNextPage(false);
				}
				//console.log(that.hasNextPage());
				if (that.hasNextPage()) {
					that.page(that.page() + 1);
				}

			};

			this.lastPage = function () {
				var maxPage = Math.ceil(that.items() / that.ordersPerPage());
				that.page(maxPage);
			};

			this.previousPage = function () {
				if (that.page() > 1) {
					that.hasPreviousPage(true);
				}
				else {
					that.hasPreviousPage(false);
				}
				if (that.hasPreviousPage())
					that.page(that.page() - 1);

			};

			this.firstPage = function () {
				that.page(1);
			};
			this.page.extend({ notify: 'always' });
			this.page.subscribe(function (dara) {
				that.loadFittingPage();
			});
		},

		loadFittingPage: function () {
			var that = this;
			var server = this.burl;
			server = server + 'client_fittings/get_order_fittings2';


			//collect the user data and prepare to send it.
			data = {
				items_per_page: that.ordersPerPage(),
				page_no: (that.page() - 1) * that.ordersPerPage()
			};

			salesman = {
				id: authCtrl.userInfo.user_id,
				username: authCtrl.username(),
				password: authCtrl.password()

			};

			//prepare the autentification data
			datapost = JSON.stringify(data);
			salesmanpost = JSON.stringify(salesman);
			timestamp = Math.round(new Date().getTime() / 1000);

			//send the information for the endpoint and wait or an answer 
			$.ajax({
				type: 'POST',
				url: server,
				data: {
					data: datapost,
					salesman: salesmanpost,
					timestamp: timestamp
				},
				success: function (dataS) {
					//store all the new info into the array
					data = JSON.parse(dataS);
					that.items(data.items_no);
					temp = data.items;
					that.FittingList.removeAll();
					for (x in temp) {

						var products = [];
						var prodStr = '';
						//console.log(temp[x].products);
						for (i in temp[x].products) {
							//console.log(temp[x].products[i]);
							var hasGarment = false;
							var status = '';
							var gt = '';
							var video = temp[x].products[i].video.length > 0 ? "Contains video" : '';
							if (temp[x].products[i].products_id == 6) {
								for (l in temp[x].products[i].suit) {
									console.log(temp[x].products[i].suit[l]);
									console.log(temp[x].products[i].suit[l].fitting_status.status_ID);
									if (temp[x].products[i].suit[l].fitting_status.status_ID > 0) {
										switch (temp[x].products[i].suit[l].fitting_status.status_ID) {
											case '1':
												status = "";
												break;
											case '2':
												status = "FittingStepSkipped";
												break;
											case '3':
												status = "FittingUnassigned";
												break;
											case '4':
												status = "WithTailor";
												break;
											case '5':
												status = "finished";
												break;
											default:
												status = "";
												break;
										}
									}
									else {
										if (temp[x].products[i].suit[l].product_completed == true) {
											status = 'finished';
										}
										else if (temp[x].products[i].suit[l].skipStep) {
											status = 'FittingStepSkipped';
										}
										else if (temp[x].products[i].suit[l].alteration.length > 0) {
											if (temp[x].products[i].suit[l].alteration[0].alteration_report_date == null || !temp[x].products[i].suit[l].alteration[0].alteration_report_date) {
												status = 'WithTailor';
											}
											else {
												status = '';
											}
										}
										else {
											if (temp[x].products[i].suit[l].fitting.fitting_id > 0) {
												status = 'FittingUnassigned';
											}
											else {
												status = '';
											}
										}
									}
									temp[x].products[i].suit[l].status = status;
									if (temp[x].products[i].suit[l].garment == '1') {
										gt = "Pant/S</span><span class='myfitquant'>";
									}
									else {
										gt = "Jacket/S</span><span class='myfitquant'>";
									}
									prodStr += "<span class='maketeamf " + temp[x].products[i].suit[l].status + "'><span class='fittings'>" + gt + "</span></span> ";
								}
							}
							else {
								console.log(temp[x].products[i]);
								console.log(temp[x].products[i].fitting_status.status_ID);
								if (temp[x].products[i].fitting_status.status_ID > 0) {
									switch (temp[x].products[i].fitting_status.status_ID) {
										case '1':
											status = "";
											break;
										case '2':
											status = "FittingStepSkipped";
											break;
										case '3':
											status = "FittingUnassigned";
											break;
										case '4':
											status = "WithTailor";
											break;
										case '5':
											status = "finished";
											break;
										default:
											status = "";
											break;
									}
								}
								else {
									if (temp[x].products[i].product_completed > 0) {
										status = 'finished';
									}
									else if (temp[x].products[i].skipStep) {
										status = 'FittingStepSkipped';
									}
									else if (temp[x].products[i].alteration.length > 0) {
										if (temp[x].products[i].alteration[0].alteration_report_date == null || !temp[x].products[i].alteration[0].alteration_report_date) {
											status = 'WithTailor';
										}
										else {
											status = '';
										}
									}
									else {
										if (temp[x].products[i].fitting.fitting_id > 0) {
											status = 'FittingUnassigned';
										}
										else {
											status = '';
										}
									}
								}

								temp[x].products[i].status = status;
								var gt;
								switch (temp[x].products[i].products_id) {
									case '1':
										gt = "Pant</span><span class='myfitquant'>";
										break;
									case '2':
										gt = "Shirt</span><span class='myfitquant'>";
										break;
									case '3':
										gt = "Vest</span><span class='myfitquant'>";
										break;
									case '4':
										gt = "Jacket</span><span class='myfitquant'>";
										break;
									case '6':
										gt = "Jacket/S</span><span class='myfitquant'></span> <span class='fittings'>Pant/S</span><span class='myfitquant'>";
										break;
								}
								prodStr += "<span class='maketeamf " + temp[x].products[i].status + "'><span class='fittings'>" + gt + "</span></span> ";
							}

						}

						//<span class='ihavideo'>"+products[j].video+"</span>
						temp[x].products = prodStr;
					}
					for (j in temp) {
						if (temp[j].last_fitting == "01/01/1970") {
							temp[j].last_fitting = "-";
						}
						that.FittingList.push(temp[j]);
					}

				},
				async: false
			});
		},


	});

	//END DEFINE CLOSURE
});
