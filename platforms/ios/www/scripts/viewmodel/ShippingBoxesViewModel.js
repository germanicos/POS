define(['jquery', 'knockout', 'base'], function($, ko) {
	
	// ██████╗  ██████╗ ██╗  ██╗    ██╗████████╗███████╗███╗   ███╗    ██╗     ██╗███████╗████████╗    ██╗   ██╗███╗   ███╗
	// ██╔══██╗██╔═══██╗╚██╗██╔╝    ██║╚══██╔══╝██╔════╝████╗ ████║    ██║     ██║██╔════╝╚══██╔══╝    ██║   ██║████╗ ████║
	// ██████╔╝██║   ██║ ╚███╔╝     ██║   ██║   █████╗  ██╔████╔██║    ██║     ██║███████╗   ██║       ██║   ██║██╔████╔██║
	// ██╔══██╗██║   ██║ ██╔██╗     ██║   ██║   ██╔══╝  ██║╚██╔╝██║    ██║     ██║╚════██║   ██║       ╚██╗ ██╔╝██║╚██╔╝██║
	// ██████╔╝╚██████╔╝██╔╝ ██╗    ██║   ██║   ███████╗██║ ╚═╝ ██║    ███████╗██║███████║   ██║        ╚████╔╝ ██║ ╚═╝ ██║
	// ╚═════╝  ╚═════╝ ╚═╝  ╚═╝    ╚═╝   ╚═╝   ╚══════╝╚═╝     ╚═╝    ╚══════╝╚═╝╚══════╝   ╚═╝         ╚═══╝  ╚═╝     ╚═╝
	//                                                                                                                 
	BoxItemListVM = Class.extend({
		init:function(){
			var that = this;
			this.Title = ko.observable();
			this.Items = ko.observableArray([]);
			this.Headers = [];
			this.addMode = ko.observable(false);
			this.BackPage = ko.observable("#shipping_main");
			this.BackMessage = ko.observable("Shippings");
			this.Missing = ko.observable(false);
			this.Locations = ko.observableArray([]);
			
			this.setBackOptions = function(message, page){
				that.BackMessage(message);
				that.BackPage(page);
			};
			
			this.setLocations = function(data){
				for(var x in data){
					that.Locations.push(data[x]);
				}
			};
			
			this.changeItemsLocation = function(){
				if(that.Missing()){
					var items = [];
					for(x in that.Items()){
						if(that.Items()[x].Item.location){
							items.push({location_id: that.Items()[x].Item.location.stock_location_id, item_id: that.Items()[x].Item.ID, barcode: that.Items()[x].Item.barcode, item_type: that.Items()[x].Item.ItemTypeID});
							
						}
					}
					if(items.length > 0){
						var data = { 
							items: items
						};	
						DataRequest(false, 'client_logistics/missing_items_change_location_endpoint', data, function(){customAlert('Locations changed');},function(){customAlert('An error happened whilst trying to update the location of the items. Please try again later or cantact the admins about the issue');});
					}
					else{
						customAlert('No new locations were selected.');
					}
				}
			};
			
			this.addList = function (ItemListObject){
				//ItemListObject = ItemListObject.new;
				var Item = {};
				for(var x in ItemListObject){
					Item = new BoxItem(ItemListObject[x]);
					that.addHeaders(Item.getDetailHeaders());
					that.Items.push({Item: Item, checked: false});
				}
			};
			this.removeFromList = function(items){
				var c = 0;
				var d = 0;
				while(c < that.Items().length){
					d = 0;
					for(d in items){
						if(that.Items()[c].Item.ID == items[d]){
							that.Items.splice(c,1);
							c--;
							break;
						}
					}
					c++;
				}
			};
			
			this.addHeaders = function(headers){
				for(var i in headers){
					if(!that.Headers.includes(headers[i]))
						that.Headers.push(headers[i]);
				}
			};
			
		},
		getLocations: function(){
			DataRequest(false, 'client_logistics/locations_data', null, this.setLocations );
		},
		getSelectedItemsID: function(){
			var items = [];
			for(var x in this.Items()){
				if(this.Items()[x].checked){
					items.push(this.Items()[x].Item.ID);
				}
			}
			return items;
		},
		getSelectedItems: function(){
			var items = [];
			for(var x in this.Items()){
				if(this.Items()[x].checked){
					items.push(this.Items()[x].Item);
				}
			}
			return items;
		},
		printLabels: function(){
			var email = $('#ShippingBoxVariousInput').val().trim();
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		var result =  re.test(email);
			var items = this.getSelectedItemsID();
			if(!items || items.length == 0){
				customAlert("Please select the items that you want to print the labels for");
			}else if(email == "" || result == false){
				customAlert("Please enter a valid email address");
			}else{
				var spinner = document.getElementById('loading_jp');	
				spinner.style.display = "block";
				var data = { 
					items: items,
					email: email
				};	
				DataRequest(false, 'client_logistics/shipping_labels_endpoint', data, function(){customAlert('Labels sent succesfully');ShippingBoxListVM.loadList(false, false, 'client_logistics/standby_boxes_endpoint');},function(){customAlert('Labels was not sent. Please try again later or cantact the admins about the issue');},function(){customAlert('Labels sent succesfully');});
				spinner.style.display = "none";
			}
		},
		createNewBox: function(){
			if(typeof ShippingBoxVM === 'undefined' || ShippingBoxVM == undefined){
				ShippingBoxVM = new BoxVM(true);
			}
			ShippingBoxVM.newBox();
			ShippingBoxVM.setBackOptions(this.Title(), "#shipping_items");
			this.addItemsToBox();
			
		},
		addItemsToBox: function(){
			var items = this.getSelectedItems();
			if(items.length > 0){
				this.addMode(false);
				ShippingBoxVM.addItems(items);
				//ShippingBoxesItemsListVM = null;
				posChangePage("#shipping_box");
				
			}
			else{
				customAlert("Please select the items that you want to add to a box");
			}
		},
		setTitle: function(title){
			this.Title(title);
		},
		
		loadList: function(endpoint){
			DataRequest(true, endpoint, '', this.addList);
		},
		
	});
	
	// ██████╗  ██████╗ ██╗  ██╗    ██╗   ██╗███╗   ███╗
	// ██╔══██╗██╔═══██╗╚██╗██╔╝    ██║   ██║████╗ ████║
	// ██████╔╝██║   ██║ ╚███╔╝     ██║   ██║██╔████╔██║
	// ██╔══██╗██║   ██║ ██╔██╗     ╚██╗ ██╔╝██║╚██╔╝██║
	// ██████╔╝╚██████╔╝██╔╝ ██╗     ╚████╔╝ ██║ ╚═╝ ██║
	// ╚═════╝  ╚═════╝ ╚═╝  ╚═╝      ╚═══╝  ╚═╝     ╚═╝
	//                                                  
	BoxVM = Class.extend({
		init: function(loadInitialInformation){
			var that = this;
			
			//controlling flags
			this.sent = ko.observable(false);
			this.received = ko.observable(false);
			this.receiving = ko.observable(false);
			this.ReceivingProblems = ko.observable(false);
			this.Headers = [];
			this.Title= ko.observable('New Box');
			this.BackMessage = ko.observable('Shippings');
			this.BackPage = ko.observable('#shipping_main');
			this.modified = false;
			this.LocationName = ko.observable();
			this.MethodName = ko.observable();
			
			//create location
			this.TitleNewLocation = ko.observable('');
			this.AddressNewLocation = ko.observable('');
			this.PostCodeNewLocation = ko.observable('');
			this.SuburbNewLocation = ko.observable('');
			this.CityNewLocation = ko.observable('');
			this.CountryNewLocation = ko.observable('');
			this.customers = new CustomerAutoCompleteVM(orderItem.DsRegistry, orderItem.custSelectVM);
			this.customers.selectedCustomer.subscribe(function(data){
				console.log(data);
				if(data)
					that.TitleNewLocation('Customer Location');
				else
					that.TitleNewLocation('');
				that.AddressNewLocation(data.customer_address1);
				that.PostCodeNewLocation(data.customer_postal_code);
				that.SuburbNewLocation(data.customer_address2);
				that.CityNewLocation(null);
				for(x in orderItem.cities_display()){
					if(data.customer_city == orderItem.cities_display()[x].locations_id)
						that.CityNewLocation(orderItem.cities_display()[x]);
				}
				that.CountryNewLocation(null);
				for(x in customersVM.countries()){
					if(data.customer_country == customersVM.countries()[x].locations_id)
						that.CountryNewLocation(customersVM.countries()[x]);
				}
				
				that.SaveNewLocation(false);
			});
			this.SaveNewLocation = ko.observable(false);
			
			this.saveNewLocation = function(id){
				if(id > 0){
					var test = {stock_location_id: id, stock_location_name: that.TitleNewLocation() ? that.TitleNewLocation():'Other Location'}
					that.stockLocations.push(test);
					that.Destination(test);
					that.clearNewLocation();
					customAlert('Location saved.')
				}
				else{
					customAlert('Location not saved. Please try again later or cantact the admins about the issue.')
				}
			};
			
			this.SaveLocation = function(){
				
				if(that.SaveNewLocation() && !that.TitleNewLocation()){
					customAlert('Please inform the title of the location');
				}else if(!that.AddressNewLocation()){
					customAlert('Inform the address of the location');
				}else if(!that.PostCodeNewLocation()){
					customAlert('Inform the Postcode of the location');
				}else if(!that.SuburbNewLocation()){
					customAlert('Inform the city of the location');
				}else if(!that.CityNewLocation()){
					customAlert('Inform the country of the location');
				}else if(!that.CountryNewLocation()){
					customAlert('Inform the address of the location');
				}
				else{
					var spinner = document.getElementById('loading_jp');	
					spinner.style.display = "block";
					var data = { 
						title: that.TitleNewLocation(),
						address: that.AddressNewLocation(),
						postcode: that.PostCodeNewLocation(),
						suburb: that.SuburbNewLocation(),
						city: that.CityNewLocation().locations_id,
						country: that.CountryNewLocation().locations_id,
						save: that.SaveNewLocation()
					};	
					DataRequest(false, 'client_logistics/create_loation', data, function(id){that.saveNewLocation(id);},function(){that.saveNewLocation(0);},function(){that.saveNewLocation(0);});
					spinner.style.display = "none";
				}
					
			};
			
			
			//data needed to creation of boxes
			this.stockLocations = ko.observableArray([]);
			this.userList = ko.observableArray([]);
			this.shippingMethods = ko.observableArray([]);
			this.userLocations = ko.observableArray([]);
			
			
			//datacontrolers
			this.ID = ko.observable('');
			this.Sender = ko.observable('');
			this.SenderLocation = ko.observable({});
			this.ShippingDate = ko.observable();
			this.ShippingDate.subscribe(function(){
				that.modified = true;
			});
			this.ExpectedDate = ko.observable();
			this.ExpectedDate.subscribe(function(){
				that.modified = true;
			});
			this.ReceivedDate = ko.observable();
			this.ReceivedDate.subscribe(function(){
				that.modified = true;
			});
			this.Cost = ko.observable('');
			this.Cost.subscribe(function(){
				that.modified = true;
			});
			this.ShippingReceipt = ko.observable({});
			this.ShippingReceipt.subscribe(function(){
				that.modified = true;
			});
			this.TrackingCode = ko.observable('');
			this.TrackingCode.subscribe(function(){
				that.modified = true;
			});
			this.ShippingMethod = ko.observable('');
			this.ShippingMethod.subscribe(function(data){
				that.modified = true;
				if(data){
					that.MethodName(data.shipping_method_name);
				}
				else{
					that.MethodName('');
				}
			});
			this.Destination = ko.observable('');
			this.Destination.subscribe(function(data){
				that.modified = true;
				if(data){
					that.LocationName(data.stock_location_name);
				}
				else{
					that.LocationName('');
				}
			});
			this.shippingComments = ko.observable('');
			this.shippingComments.subscribe(function(){
				that.modified = true;
			});
			this.shippingReceivingIssue = ko.observable('');
			this.items = ko.observableArray([]);
			this.items.subscribe(function(){
				that.modified = true;
			});
			this.barcodeScanner = ko.observable('');
			this.barcodeScanner.subscribe(function(data){
				if(data){
					that.readBarcode(data.trim());
				}
			});
			
			
			//gambs
			this.readBarcode = function(data){
				for(x in that.items()){
					if(that.items()[x].Item.barcode == data){
						that.items()[x].checked(true);
						that.barcodeScanner('');
					}
				}
			}
			this.SenderString = ko.observable()
			this.DestinationString = ko.observable()
			this.MethodString = ko.observable()
			this.selectAllItems = function(){
				for(var x in that.items()){
					that.items()[x].checked(true);
				}
			}
			this.setBackOptions = function(message, page){
				that.BackMessage(message);
				that.BackPage(page);
			};
			this.setInitialData = function(data){
				
				var locations = data.locations;
				var methods = data.methods;
				var users = data.users;
				
				for(var x in locations){
					that.stockLocations.push(locations[x]);
				}
				
				for(var x in methods){
					that.shippingMethods.push(methods[x]);
				}
				
				for(var x in users){
					that.userList.push(users[x]);
				}
				
			};
			this.setUserLocation = function(data){
				
				for(var x in data){
					for(var c in that.stockLocations()){
						if(that.stockLocations()[c].stock_location_id == data[x]){
							that.userLocations.push(that.stockLocations()[c]);
							break;
						}
					}
				}
				if(that.userLocations().length == 1){
					that.SenderLocation(that.userLocations()[0]);
				}
			};
			
			this.setBoxData = function(data){
				that.clearBox();
				for(var c in that.stockLocations()){
					if(that.stockLocations()[c].stock_location_id == data.sender_stock_location_id){
						that.Sender(that.stockLocations()[c]);
						break;
					}
				}
				for(var c in that.stockLocations()){
					if(that.stockLocations()[c].stock_location_id == data.recepient_stock_location_id){
						that.Destination(that.stockLocations()[c]);
						break;
					}
				}
				for(var c in that.shippingMethods()){
					if(that.shippingMethods()[c].shipping_method_id == data.shipping_method_id){
						that.ShippingMethod(that.shippingMethods()[c]);
						break;
					}
				}
				that.SenderString(that.Sender().stock_location_name? that.Sender().stock_location_name + "  "+ data.shipping_items_box_created_by: data.shipping_items_box_created_by);
				that.DestinationString(that.Destination().stock_location_name? that.Destination().stock_location_name:data.recepient_stock_location_name);
				that.MethodString(that.ShippingMethod().shipping_method_name?that.ShippingMethod().shipping_method_name:data.shipping_method_name);
				that.ID(data.shipping_items_box_id);
				that.ShippingDate(data.shipping_items_box_sent_date);
				that.ExpectedDate(data.shipping_items_box_expected_date);
				that.ReceivedDate(data.shipping_items_box_received_date);
				that.Cost(data.shipping_items_box_cost);
				that.ShippingReceipt({Image:data.receipt_image});
				that.TrackingCode(data.shipping_items_box_tracking_code);
				that.shippingComments(data.shipping_items_box_comments);
				var Item = '';
				for(var x in data.items){
					Item = new BoxItem(data.items[x]);
					that.addHeaders(Item.getDetailHeaders());
					that.items.push({Item:Item, checked: ko.observable(false)});
				}
				that.modified = false;
			};
			this.addHeaders = function(headers){
				for(var i in headers){
					if(!that.Headers.includes(headers[i]))
						that.Headers.push(headers[i]);
				}
			};
			this.addItems = function(items){
				for(var x in items){
					that.addHeaders(items[x].getDetailHeaders());
					that.items.push({Item:items[x], checked: ko.observable(false)});
				}
			};
			
			this.clearBox = function(){
				that.modified = false;
				that.Title('New Box');
				that.Headers = [];
				that.sent(false);
				that.received(false);
				that.receiving(false);
				that.ReceivingProblems(false);
				that.ID('');
				that.ReceivedDate(null);
				that.ShippingDate(null);
				that.ExpectedDate(null);
				that.Cost('');
				that.ShippingReceipt({});
				that.TrackingCode('');
				that.ShippingMethod({});
				that.Destination({});
				that.Sender({});
				that.shippingComments('');
				that.shippingReceivingIssue('');
				that.items.removeAll();
				that.clearNewLocation();
				that.SenderLocation({});
			};
			
			this.clearNewLocation = function(){
				that.customers.removeCustomer();
				that.TitleNewLocation('');
				that.AddressNewLocation('');
				that.PostCodeNewLocation('');
				that.SuburbNewLocation('');
				that.CityNewLocation(null);
				that.CountryNewLocation(null);
				that.SaveNewLocation(false);
			};
			
			if(loadInitialInformation){
				this.getInitialData();
			};
			
			this.buildBox = function(sent){
				var items = [];
				for(var x in that.items()){
					items.push(that.items()[x].Item);
				}
				//that.ShippingDate($('#ShippingDate').val());
				//that.ExpectedDate($('#ExpectedDate').val());
				var box = new Box();
				box.ID = that.ID();
				try{
					var day = $('#ShippingDate').val().split("/")[0];
					var month = $('#ShippingDate').val().split("/")[1];
					var year = $('#ShippingDate').val().split("/")[2];
					if(day)box.shippingDate = year + "-" + month + "-" + day;
					else{
						box.shippingDate = ""
					}
				}catch(e){
					box.shippingDate = "";
				}
				try{
					var day = $('#ExpectedDate').val().split("/")[0];
					var month = $('#ExpectedDate').val().split("/")[1];
					var year = $('#ExpectedDate').val().split("/")[2];
					if(day)box.expectedDate = year + "-" + month + "-" + day;
					else{
						box.expectedDate = ""
					}
				}catch(e){
					box.expectedDate = "";
				}
				box.cost = that.Cost();
				try{
					box.receipt = that.ShippingReceipt().Image?that.ShippingReceipt().Image:"";
				}catch(e){
					box.receipt = "";
				}
				box.trackingCode = that.TrackingCode();
				try{
					box.method = that.ShippingMethod().shipping_method_id?that.ShippingMethod().shipping_method_id:"";
				}catch(e){
					box.method = "";
				}
				try{
					box.destination = that.Destination().stock_location_id?that.Destination().stock_location_id:"";
				}catch(e){
					box.destination = "";
				}
				box.comments = that.shippingComments();
				box.items = items;
				box.sent = sent;
				box.SenderLocation = that.SenderLocation()?that.SenderLocation().stock_location_id:"";
				return box
			};
			
			
		},
		
		setTitle: function(){
			if(this.sent() && this.received()){
				this.Title('History Box');
			}else if(this.sent()){
				this.Title('Incoming Box');
			}else{
				this.Title('Stand By Box');
			}
		},
		
		newBox: function(){
			this.clearBox();
		},
		getSelectedItemsID: function(){
			var items = [];
			for(var x in this.items()){
				if(this.items()[x].checked){
					items.push(this.items()[x].Item.ID);
				}
			}
			return items;
		},
		printLabels: function(){
			var email = $('#ShippingBoxVariousInput').val().trim();
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		var result =  re.test(email);
			var items = [];
			for(var x in this.items()){
				items.push(this.items()[x].Item.ID);
			}
			
			if(!items || items.length == 0){
				customAlert("Please select the items that you want to print the labels for");
			}else if(email == "" || result == false){
				customAlert("Please enter a valid email address");
			}else{
				var spinner = document.getElementById('loading_jp');	
				spinner.style.display = "block";
				var data = { 
					items: items,
					email: email
				};	
				DataRequest(false, 'client_logistics/shipping_labels_endpoint', data, function(){customAlert('Labels sent succesfully');},function(){customAlert('Labels was not sent. Please try again later or cantact the admins about the issue');},function(){customAlert('Labels sent succesfully');});
				spinner.style.display = "none";
			}
		},
		getInitialData: function(){
			DataRequest(false, 'client_logistics/new_box_data', null, this.setInitialData );
			DataRequest(false, 'client_logistics/user_locations_data', null, this.setUserLocation );
		},
		
		loadBox: function(boxID){
			DataRequest(false, 'client_logistics/shipping_get_box', {box_id: boxID}, this.setBoxData );
		},
		addPendingItems: function(){
			var items = [];
			for(x in this.items()){
				items.push(this.items()[x].Item.ID);
			}
			ShippingBoxesItemsListVM =  new BoxItemListVM();
			ShippingBoxesItemsListVM.addMode(true);
			ShippingBoxesItemsListVM.setBackOptions("Box", "#shipping_box");
			ShippingBoxesItemsListVM.setTitle('Add Pending Items To Box');
			ShippingBoxesItemsListVM.loadList('client_logistics/pending_items_endpoint');
			ShippingBoxesItemsListVM.removeFromList(items);
			posChangePage('#shipping_items');
		},
		saveBox: function(redirect){
			var box = this.buildBox(false);
			var that = this;
			if(!box.shippingDate){
				customAlert('Choose when the box will be sent!');
			}
			else if(!box.destination){
				customAlert('Select a destination!');
			}
			else if(box.items.length == 0){
				customAlert('Select what items are in the box!');
			}
			else{
				if(redirect)
					DataRequest(false, 'client_logistics/shipping_box_endpoint', box, function(){customAlert("Box Saved Succesfully");posChangePage('#shipping_main');},function(){customAlert("An error happened whilst trying to save the data. Try again later or contact the admins");}, function(){customAlert("An error happened whilst trying to save the data. Try again later or contact the admins");} );
				else
					DataRequest(false, 'client_logistics/shipping_box_endpoint', box, function(){console.log('saved');that.modified = false;that.addPendingItems();},function(){customAlert("An error happened whilst trying to save the data. Try again later or contact the admins");}, function(){customAlert("An error happened whilst trying to save the data. Try again later or contact the admins");} );
			}
		},
		sendBox: function(){
			var box = this.buildBox(true);
			changeVisibilityDiv('loading_jp');
			if(!box.shippingDate){
				customAlert('Select when the box will be sent!');
				changeVisibilityDiv('loading_jp');
			}
			else if(!box.method){
				customAlert('Select a shipping method!');
				changeVisibilityDiv('loading_jp');
			}
			else if(!box.destination){
				customAlert('Select a destination!');
				changeVisibilityDiv('loading_jp');
			}
			else if(box.items.length == 0){
				customAlert('Select what items are in the box!');
				changeVisibilityDiv('loading_jp');
			}
			else if(!box.SenderLocation){
				customAlert('Select the location of the sender!');
				changeVisibilityDiv('loading_jp');
			}
			else{
				var day = box.expectedDate.split("-")[2];
				var month = box.expectedDate.split("-")[1];
				var year = box.expectedDate.split("-")[0];
				var ed = new Date(month + "/" + day + "/" + year);
				var day = box.shippingDate.split("-")[2];
				var month = box.shippingDate.split("-")[1];
				var year = box.shippingDate.split("-")[0];
				var sd = new Date(month + "/" + day + "/" + year);
				if(sd>ed){customAlert('The expected date is earlier than the sent date!');changeVisibilityDiv('loading_jp');return;}
				DataRequest(false, 'client_logistics/shipping_box_endpoint', box, function(){customAlert("Box Saved Succesfully");posChangePage('#shipping_main');},function(){customAlert("An error happened whilst trying to save the data. Try again later or contact the admins");}, function(){customAlert("An error happened whilst trying to save the data. Try again later or contact the admins");} );
				changeVisibilityDiv('loading_jp');
			}
		},
		receiveBox: function(){
			this.receiving(true);
			$('#BarcodeScanner').focus();
			customAlert("Plz select the items are in the box.");
		},
		finishReceive: function(){
			changeVisibilityDiv('loading_jp');
			var items_received = [];
			for(var x in this.items()){
				if(this.items()[x].checked()){
					items_received.push({id:this.items()[x].Item.ID,barcode:this.items()[x].Item.barcode, item_type:this.items()[x].Item.ItemTypeID});
				}
			}
			if(items_received.length < this.items().length && !this.shippingReceivingIssue()){
				customAlert('Not all items were received. Please check again or report a problem');
				this.ReceivingProblems(true);
				changeVisibilityDiv('loading_jp');
			}
			else if(!this.SenderLocation()){
				customAlert('Not all items were received. Please check again or report a problem');
				changeVisibilityDiv('loading_jp');
			}
			else{
				DataRequest(false, 'client_logistics/shipping_box_receive_endpoint', {recipient_location: this.SenderLocation().stock_location_id?this.SenderLocation().stock_location_id:"", shipping_items_box_id: this.ID(), items_received: items_received, shipping_items_box_issue_notes:this.shippingReceivingIssue()}, this.receiveSucces );
				changeVisibilityDiv('loading_jp','none');
			}
		},
		receiveSucces: function(){
			customAlert("Items received succesfully");
			posChangePage("#shipping_main");
			changeVisibilityDiv('loading_jp','none');
		}
	});
	
	
	// ██████╗  ██████╗ ██╗  ██╗    ██╗     ██╗███████╗████████╗    ██╗   ██╗███╗   ███╗
	// ██╔══██╗██╔═══██╗╚██╗██╔╝    ██║     ██║██╔════╝╚══██╔══╝    ██║   ██║████╗ ████║
	// ██████╔╝██║   ██║ ╚███╔╝     ██║     ██║███████╗   ██║       ██║   ██║██╔████╔██║
	// ██╔══██╗██║   ██║ ██╔██╗     ██║     ██║╚════██║   ██║       ╚██╗ ██╔╝██║╚██╔╝██║
	// ██████╔╝╚██████╔╝██╔╝ ██╗    ███████╗██║███████║   ██║        ╚████╔╝ ██║ ╚═╝ ██║
	// ╚═════╝  ╚═════╝ ╚═╝  ╚═╝    ╚══════╝╚═╝╚══════╝   ╚═╝         ╚═══╝  ╚═╝     ╚═╝
	//                                                                                  
	BoxListVM = Class.extend({
		init: function(pagination){
			var that = this;
			
			//controlling flags
			this.sent = ko.observable(false);
			this.received = ko.observable(false);
			this.Title= ko.observable('New Box');
			this.receiveBox = ko.observable(false);
			
			this.boxes = ko.observableArray([]);
			this.receiveEntireBox = function(id){
				var spinner = document.getElementById('loading_jp');	
				spinner.style.display = "block";
				var data = { 
					box_id: id,
				};	
				DataRequest(false, 'client_logistics/receive_box_endpoint', data, function(){customAlert('Box received!');},function(){customAlert('Box not received! Please try again later or contact the admins!');},function(){customAlert('Box received!');});
				spinner.style.display = "none";
			};
			this.addBoxes = function(boxes){
				for(var x in boxes){
					that.boxes.push(boxes[x]);
					if(!that.pagination()){
						that.itemsVisible.push(boxes[x]);
					}
				}
				if(that.pagination()){
					that.page(1);
				}
				else{
					that.itemsVisible
				}
			};
			this.parseBoxes = function(data){
				var boxesData = [];
				var box = {};
				for(var x in data){
					box = new Box(true);
					box.ID = data[x].shipping_items_box_id;
					box.Sender = data[x].sender_stock_location_name;
					box.Receipient = data[x].recepient_stock_location_name;
					box.shippingDate = data[x].shipping_items_box_sent_date;
					box.expectedDate = data[x].shipping_items_box_expected_date;
					box.receivedDate = data[x].shipping_items_box_received_date;
					box.trackingCode = data[x].shipping_items_box_tracking_code;
					box.method = data[x].shipping_method_name;
					box.creator = data[x].shipping_items_box_created_by;
					boxesData.push(box);
				}
				that.addBoxes(boxesData);
				that.page(1);
			};
			
			
			//pagination
			
			//pagination
			
			this.itemsPerPage = 9;
			this.itemsVisible = ko.observableArray();
			pagination = pagination ?  true: false;
			this.pagination = ko.observable(pagination);
			this.page = ko.observable(1);
			this.hasNextPage = ko.observable(false);
			this.hasPreviousPage = ko.observable(false);
			
			this.firstPage = function(){
				that.page(1);
			};
			
			this.nextPage = function(){
				if(Math.ceil(that.boxes().length/that.itemsPerPage) > that.page()){
					that.hasNextPage(true);
				}
				else{
					that.hasNextPage(false);
				}
				console.log(that.hasNextPage());
				if(that.hasNextPage()){
					that.page(that.page() + 1);
				}
				
			};
			
			this.lastPage = function(){
				var maxPage = Math.ceil(that.boxes().length/that.itemsPerPage);
				if(maxPage == 0){
					maxPage = 1;
				}
				that.page(maxPage);
				if(maxPage < that.page()){
					that.hasNextPage(true);
				}
			};
			
			this.previousPage = function(){
				if(that.page() > 1){
					that.hasPreviousPage(true);
				}
				else{
					that.hasPreviousPage(false);
				}
				if(that.hasPreviousPage())
					that.page(that.page() - 1);
				
				//console.log(that.hasPreviousPage());
								
			};
			
			this.firstPage = function(){
				that.page(1);
			};
			
			this.page.extend({ notify: 'always' });
			this.page.subscribe(function(data){
				that.itemsVisible.removeAll();
				var limit = data * that.itemsPerPage;
				//alert(i < that.ErrorData && i < limit);
				for(i = (data - 1) * that.itemsPerPage; i < that.boxes().length && i < limit; i++){
					that.itemsVisible.push(that.boxes()[i]);
				}
				//console.log(that.itemsVisible());
			});
			
		},
		showBox:function(BoxID){
			if(typeof ShippingBoxVM === 'undefined' || ShippingBoxVM == undefined){
				ShippingBoxVM = new BoxVM(true);
			}
			ShippingBoxVM.loadBox(BoxID);
			ShippingBoxVM.setBackOptions(this.Title(), "#shipping_box_list");
			ShippingBoxVM.sent(this.sent());
			ShippingBoxVM.received(this.received());
			ShippingBoxVM.setTitle();
			posChangePage("#shipping_box");
		},
		loadList: function(sent,received, endpoint){
			this.sent(sent);
			this.received(received);
			DataRequest(false, endpoint, null, this.parseBoxes);
		}, 
		setTitle: function(title){
			this.Title(title);
		},
		
	});
	
});