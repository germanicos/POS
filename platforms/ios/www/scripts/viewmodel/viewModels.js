define(['jquery', 'knockout'], function($, ko) {
AuthControl = function AuthController() {
			var self = this;
			self.username    =  ko.observable(""); 
			self.password    =  ko.observable("");
			self.login       =  function() {
				if (!self.showSpinner()) {
					console.log('click');
					self.showSpinner(true);
					setTimeout(function() {self.showSpinner(false); posChangePage('#main');} , 1000); 
				}
			} 
			self.showSpinner =  ko.observable(false); 
			self.showButton  =  ko.computed(function() {
				if (self.username() != "" && self.password() != "") return true; 
				return false;
			}, this);
		
}

SimpleControl = function SimpleController(DsRegistry) {
	var self = this; 
	self.subscribed = ""; 
	//self.dsRegistry = DsRegistry;
	
	self.subscribeTo = function(name) {
		self.subscribed = name;
		var ds = DsRegistry.getDatasource(name).getStore();
		if (typeof(ds) != 'undefined') {
			self.digestData(ds);  	
		}
	}
	
	self.digestData = function(data) {
		//console.log(data);
	}
	
	self.extend = function(obj) {
		return $.extend(self, obj); 
	}
	
	
	$(document).live('modelUpdate', function(event, data) {
console.log("modelUpdate AT VIEWMODELS");		
		if (data == self.subscribed) { 
			var ds = DsRegistry.getDatasource(data).getStore();
			if (typeof(ds) != 'undefined') {
				self.digestData(ds); 
			}
		}
	});
}

FabricSelectControl = function FabricSelectController(dsRegistry) {
				var self = this; 
				self.subscribed = ""; 
				self.fabric = ko.observableArray(); 
				
				
				self.subscribeTo = function(name, handler) {
					var ds = dsRegistry.getDatasource(name);
					self.subscribed = name; 	
					if (typeof(ds) != 'undefined') {
						self.digestData(ds.getStore()); 
					}
				}
					
				self.initIscroll = function() {
					if ( $('#iscroll-wrapper-fs').length != 0  ) {
						self.hm_iScroll = new iScroll('iscroll-wrapper-fs',  { 
							hScrollbar: false, vScrollbar: false, snap: 'li', 
							onScrollEnd: function () {
								console.log(this.currPageX); 
							}
						});
					}
				}
				
				self.digestData = function(data) {
					self.fabric(data);
					self.initIscroll();
				}
				
				
				self.selectFabric = function() {
					//console.log(this);
					
				}
	
				
				$(document).live('modelUpdate', function(event, data) {
					if (data == self.subscribed) {  
						var ds = dsRegistry.getDatasource(data);
						if (typeof(ds) != 'undefined') {
							self.digestData(ds.getStore()); 
						}
					}
				});

				$(document).live('pageshow', function() { self.initIscroll();});
}





});