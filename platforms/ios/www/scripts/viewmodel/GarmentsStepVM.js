define(['jquery', 'knockout', 'base'], function($, ko) {


    GarmentsStepVM = SimpleControl.extend({

        init: function(customer, orderVM) {
            console.log("Init GarmentsStepVM")
            var self = this;
            this.customer = customer;

            this.garments = ko.observableArray(this.buildGarments());
            this.orderVM = orderVM;



            // for UI purposes. if this counter of garments is > 0, consider step as complet
            
        },

        //  Builds and returns the array of garments
        buildGarments: function() {
            
            return [
                {
                    'name': 'jacket',
                    'garments': ko.observableArray([]),
                    'id': '4',
                    'image': 'img/order_process/wetransfer-b60a96/jacket.png'
                },
                {
                    'name': 'pants',
                    'garments': ko.observableArray([]),
                    'id': '1',
                    'image': 'img/order_process/wetransfer-b60a96/pant.png'
                },
                {
                    'name': 'suit',
                    'garments': ko.observableArray([]),
                    'id': '6',
                    'image': 'img/order_process/wetransfer-b60a96/suit.png'
                },
                {
                    'name': 'vest',
                    'garments': ko.observableArray([]),
                    'id': '3',
                    'image': 'img/order_process/wetransfer-b60a96/vest.png'
                },
                {
                    'name': 'shirt',
                    'garments': ko.observableArray([]),
                    'id': '2',
                    'image': 'img/order_process/wetransfer-b60a96/shirt.png'
                },
            ]

        },

        // Increases the count of garments
        incrementGarment(garmentId) {

            console.log("Incrementing " + garmentId + " for... ", this.customer.full_name);

            let createdGarment = false;

            if(this.customer.is_locked)
            {
                customAlert("Sorry, The customer IS LOCKED");
                return;
            }

            for(let garment of this.garments())
            {
                if( garment.id == garmentId)
                {
                    // gets the object from the design VM
                    createdGarment = this.customer.garmentsDesignStep.getGarmentObject(garmentId)                    
                    garment.garments.push(createdGarment);
                    
                    let garmentTypeName = '';

                    switch(garmentId)
                    {
                        case '1':garmentTypeName = 'pants'; break;
                        case '2':garmentTypeName = 'shirt'; break;
                        case '3':garmentTypeName = 'vest'; break;
                        case '4':garmentTypeName = 'jacket'; break;
                        case '6':garmentTypeName = 'suit'; break;
                    }


                    // Adds the garment to the global garments obj
                    const globalGarmentObj = {
                            "customer" : this.customer,
                            "garmentType" : garmentTypeName,
                            "garment" : createdGarment,
                        }

                    this.orderVM.globalGarments[garment.name].push(globalGarmentObj)

                    for( let i in this.orderVM.globalGarments[garment.name]() )
                    {
                        const garmentObj = this.orderVM.globalGarments[garment.name]()[i];
                    }

                    this.orderVM.globalGarments[garment.name].sort(function(a, b) {

                        
                        return a.customer.full_name > b.customer.full_name;


                    }); 

                    this.toggleStepComplete();
                    break;
                }
            }

            return createdGarment;
        },

        // Decreasese the number of garments
        decrementGarment(garmentId) {

            if(this.customer.is_locked)
            {
                customAlert("Sorry, The customer IS LOCKED");
                return;
            }

            for(let garment of this.garments())
            {
                if( garment.id == garmentId)
                {
                    if(garment.garments().length > 0)
                    {
                        const removedGarment = garment.garments.pop(); // removes last element
                            
                        // Remove from the global garment list
                        this.orderVM.globalGarments[garment.name].remove( function(gar) {
                            return removedGarment.unique_id == gar.garment.unique_id;  
                        });

                        // Unselect the garments to avoid pointing to a zombie garment
                        this.orderVM.currentDesigingGarmentType(false); 
                        this.orderVM.currentGarmentInstance(false);
                        this.orderVM.currentDesignStep(false);


                        this.toggleStepComplete();
                    }
                    else
                    {
                        console.log("Cannot go below 0");
                    }

                    break;
                }
            }



        },

        toggleStepComplete : function()
        {
            if(orders.orderStepRequirementsVM !== undefined)
            {
                if (orders.orderStepRequirementsVM.checkGarmentsStep() == true)
                {
                    orders.steps.filter(function(el){return el.id == '1'})[0].complete = true;
                }
                else
                {
                    orders.steps.filter(function(el){return el.id == '1'})[0].complete = false;
                }
                
            }

          
        }



    });
});