define(['jquery', 'knockout', 'base'], function($, ko) {
    
    UniformOrderPaymentsVM = SimpleControl.extend({

    	init: function(uniformVM, paymentMethods, discountCodesFromServer) {

            var self = this;

            console.log("Init UniformOrderPaymentsVM...");

            this.uniformVM = uniformVM;

            this.terms = this.getTermsReviewObject();

            this.orderDescription = ko.observable("");

            this.cities = dsRegistry.getDatasource('citiesDS').getStore();
    		this.states = dsRegistry.getDatasource('statesDS').getStore();
    		this.countries = dsRegistry.getDatasource('countriesDS').getStore();

            // City in which the order has been taken
            this.selectedOrderCity = ko.observable(false);
            this.selectedOrderCountry = ko.observable("1"); // set Australia as default country
            this.selectedOrderState = ko.observable(false);

            // City in which the customer wants the fitting to happen
            this.selectedFittingCity = ko.observable(false);
            this.selectedFittingCountry = ko.observable(false);
            this.selectedFittingState = ko.observable(false);


    		this.banks = ko.observableArray(['ANZ', 'COM', 'STG']);

            this.selectedBank = ko.observable(false);

    		this.mailCustomerInvoice = ko.observable(false);

    		this.depositValue = ko.observable(0.0);

    		this.paymentMethods = ko.observableArray(paymentMethods);
    		this.selectedPaymentMethod = ko.observable(false);

    		this.discountCodesFromServer = discountCodesFromServer;

    		this.depositComments = ko.observable("");

			this.canvasBase64 = ko.observable(false);
			this.signing = ko.observable(false);

			this.ccImage = ko.observable(false);

            this.currentDiscountCode = ko.observable(false);

            this.extraCosts = ko.observableArray([]);

            // Holds a computed array of the costs of GARMENTS (NO GST)
            this.regularCosts = this.getComputedRegularCosts();
            // End regular costs

            // Holds a computed array of TOTAL COSTS = garment costs + extra costs (NO GST)
            this.totalOrderCost = this.getComputedTotalOrderCosts();
        },

        // Retursn the computed regular costs function
        getComputedRegularCosts : function() {

            var self = this;

            return ko.computed(function() {

                let costsArray = [];

                for( let template of self.uniformVM.garmentsStep.templates() )
                {
                    // Continue if there is no instances selected for this template
                    if( template.instances().length == 0 ) { continue; }

                    // Gets a 'clean' cost obj 
                    const costObj = self.getRegularCostObj();

                    costObj.description = `${template.instances().length}x ${template.name}`;
                    costObj.value = parseFloat(template.price * template.instances().length);
                    costObj.fabric_id = template.fabric_id;

                    // Adds the cost to the costs array
                    costsArray.push(costObj);
                }
                
                return costsArray;
            });
        },

        getComputedTotalOrderCosts : function() {

            var self = this;

            return ko.computed(function() {

                let total = 0.0;

                for(let cost of self.regularCosts())
                {
                    total += parseFloat(cost.value);
                };

                for(let extraCost of self.extraCosts())
                {
                    total += parseFloat(extraCost.value());
                };

                const discount = self.getDiscountCodeValue();

                return total - discount;
            });

        },

        getExtraCostObj : function() {

        	return {
        		'description' : ko.observable(''),
        		'value' : ko.observable(0.0)
        	};
        },

        getRegularCostObj : function() {

            return {
                'description' : '',
                'value' : 0.0,
                'GST' : 10, // Default GST 10 % 
                'fabric_id' : "",
            };

        },

        addExtraCost : function() {
        	console.log("adding extra cost...");
        	this.extraCosts.push(this.getExtraCostObj());
        },

        removeExtraCost : function(extraCost) {
        	console.log("removing extra cost...");
        	this.extraCosts.remove(extraCost);
        },


        prepareCanvas : function() {

			console.log("preparing canvas...");

            // clear canvas
            this.canvasBase64(false);

			const divId = "canvas-wrapper";

			let canvasDiv = $("#"+divId).find("#canvasDiv");

			let canvas = document.createElement('canvas');

			canvas.setAttribute('width', 710);
			canvas.setAttribute('height', 350);
			canvas.setAttribute('id', 'canvasel');

			canvasDiv.append(canvas);
			if(typeof G_vmlCanvasManager != 'undefined') {
				canvas = G_vmlCanvasManager.initElement(canvas);
			}
			context = canvas.getContext("2d");
			
			clickX = new Array();
			clickY = new Array();
			clickDrag = new Array();
			context.clearRect(0, 0, canvas.width, canvas.height); // Clears the canvas

		    canvas.addEventListener('mousedown', _mousedown, false);
	        canvas.addEventListener('mousemove', _mousemove, false);
	        window.addEventListener('mouseup', _mouseup, false);
		
			canvas.addEventListener('touchstart', _touchstart, false);
	        canvas.addEventListener('touchmove', _touchmove, false);

	        $("#"+divId).show();

            // hide the BTN
            this.signing(true);
		},

		saveCanvas : function() {
			console.log("Saving canvas...");
			const canvas = document.getElementById('canvasel');
			this.canvasBase64(canvas.toDataURL("image/png", 0.6));

            //navigator.notification.alert("Signature Saved !");
            //alert("Signature Saved !");

            this.submitSignature()
		},

		clearCanvas : function() {
			console.log("clearing canvas...");
			document.getElementById('canvasel').remove();

            this.prepareCanvas();
		},

		/**
		 * TODO : create customer if the salesman created a new one before the order process.
		 * @return {[type]} [description]
		 */
		submitSignature : function() {

		    console.log('Saving Signature...');
		    // using old endpoint for signature image
		    const base64 = this.canvasBase64().split(',')[1];
		    const customer_id = this.uniformVM.contactPersonStep.selectedCustomer().customer_id;
		    const salesman = { "username" : authCtrl.username(), "password" : authCtrl.password(), "id" : authCtrl.userInfo.user_id };

		    $.ajax({
		        type: 'POST',
		        url: BUrl + 'client_image_upload/upload_signature_image',
		        dataType: 'json',
		        async: true,
		        data:{
		            salesman: JSON.stringify(salesman),
		            customer_id: customer_id,
		            data: base64
		        },
		        success: function(dataS) {
		            console.log(dataS);
		            customAlert("Signature saved successfully !");
		        },
		        error: function (error) {
		            console.log(JSON.stringify(error));
		            $.jGrowl('Signature Saved Succesfully !!');             
		        }
		    }); 


		},

        chooseCCPicSource : function() {
 
            var self = this;
 
            console.log("choosing Pic source...");
 
            navigator.notification.confirm(
                'CC Picture Source', // message
                function(btnIndex) {
                    self.takeCCPic(btnIndex);
                },            // callback to invoke with index of button pressed
                '',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );
 
        },

        removeCCimg : function() {

            var self = this;
 
            console.log("Removing CC img...");
 
            navigator.notification.confirm(
                'Do you really want to delete this image ?', // message
                function(btnIndex) 
                {
                    if(btnIndex == 2)
                    {
                        self.ccImage(false);
                    }
                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['No', 'Yes']         // buttonLabels
            );

        },

        
		takeCCPic : function(buttonIndex) {

			console.log("Taking CC image...");
 
            const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;
             
            navigator.camera.getPicture(onSuccess, onFail,
            {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: source,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                saveToPhotoAlbum: true,
                correctOrientation : true,
            }); 
 
            function onSuccess(imageURI) { 
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError); 
            }  
 
            function onFail(message) { 
                alert('Failed because: ' + message); 
            } 
 
            function resolveOnSuccess(entry) {
                 
                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';
                 
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist
                     
                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };
 
            function successMove(entry) {
                console.log('After move');
                 
                console.log("entry.toURL()", entry.toURL());
 	
 				uniformOrder.paymentsStep.ccImage(entry.toURL());
            };
 
            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };


		},

        getTermsReviewObject : function() {

            return {
                visible : ko.observable(false),
                accepted : ko.observable(false)
            }

        },

        akldTerm : function(accepted) {
            this.terms.accepted(accepted);
            this.terms.visible(false);
        },

        // Code from old Order process
        getDiscountCodeValue : function () {

            var code = this.currentDiscountCode();

            console.log("discount code", code);
            
            if( !code )
            {
                return 0;
            }

            var discounts = this.discountCodesFromServer;

            for (discount_name in discounts) {
                if (code == discount_name) {
                    return parseInt(discounts[code]);
                }
            }

            if (code == "Freelining") {
                return 100;
            } else if (code == "FreeShirt") {
                return 190;
            } else if (code == "FreeLinShi") {
                return 290;
            } else if (code == null || code == undefined || code.length < 12) {
                return 0;
            } else {

                var amount = code.substr(0, 2) + code.substr(7, 2) + "." + code.substr(10, 2);

                var codeday = code.substr(2, 2);
                var codemonth = code.substr(5, 2);
                var sellerFirstNameFD = code.substr(4, 1).toLowerCase();
                var sellerLastNameFD = code.substr(9, 1).toLowerCase();
                var first_nameFD = authCtrl.userInfo.first_name.substr(0, 1).toLowerCase();
                var last_nameFD = authCtrl.userInfo.last_name.substr(0, 1).toLowerCase();
                var day = new Date().getDate();
                var month = new Date().getMonth() + 1;
                if (day < 10) {
                    day = "0" + day;
                } else {
                    day = "" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                } else {
                    month = "" + month;
                }
                if (sellerFirstNameFD == first_nameFD && sellerLastNameFD == last_nameFD && codeday == day && codemonth == month && code.length == 12) {

                    return (amount * 1.0).round(2);
                } else {
                    return 0;
                }
            }
        },


         /**
         * This method will return an object containing payment data to send to ERP
         */
        getSubmissionData : function() {

            const paymentData = {
                'payment_method' : this.selectedPaymentMethod(),
                'cc_image' : this.ccImage(),
                'selected_bank' : this.selectedBank() ? this.selectedBank() : false,
                'deposit_value' : this.depositValue(),
                'deposit_comments' : this.depositComments(),
                'order_city' : this.selectedOrderCity(),
                'fitting_city' : this.selectedFittingCity(),
                'order_notes' : this.orderDescription(),
                'mail_customer_invoice' : this.mailCustomerInvoice(),
                'extra_costs' : ko.mapping.toJS(this.extraCosts()),
                'regular_costs' : ko.mapping.toJS(this.regularCosts()),
                'total_order_costs' : ko.mapping.toJS(this.totalOrderCost()),
                'discount_code'  : this.currentDiscountCode(),
                'discount_value' : this.getDiscountCodeValue(), 
            }

            return paymentData;
        }

    });

});