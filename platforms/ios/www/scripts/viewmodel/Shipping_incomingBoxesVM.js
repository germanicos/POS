define(['jquery', 'knockout', 'base'], function ($, ko) {
    ShippingIncomingBoxes = Class.extend({
        init: function () {
            self = this;
            this.boxes = ko.observableArray();
            this.historyChat = ko.observable(null);
            this.data = this.requestData();
        },

        /**
         * @description Request necessary data from ERP
         */
        requestData: function () {
            console.log("requesting data for incoming box...");

            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/get_incoming_boxes",
                dataType: 'json',
                data: { "user": authCtrl.userInfo },
                async: false,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    for (let i = 0; i < ret.length; i++) {
                        ret[i].showInfo = ko.observable(false); // add flag to hide/show info
                        ret[i].items = Object.keys(ret[i].items).map(e => ret[i].items[e]) // It is easier item be an array, not a map
                        ret[i].wasReceived = ko.observable(false);
                        for (item of ret[i].items) {
                            // change this info to be an KO object to change the view in real time. and set to boolean
                            item.item_received = ko.observable(item.item_received == '0' ? false : true);

                            // set chat as ko.array
                            item.chat = ko.observableArray(Object.keys(item.chat).map(e => item.chat[e]));

                            // show/hide messages
                            item.showMessages = ko.observable(false);
                            item.newChat = ko.observable("");
                        }
                        self.boxes.push(ret[i]);
                    }


                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main_new");


                }

            });
        },

        /** @description hide/show items of a box 
         * @param {number} position position of box in array
         */
        hideShowBoxInfo: function (position) {
            self.boxes()[position].showInfo(!self.boxes()[position].showInfo());
        },

        /**
         * Calculate total weight of a box
         */
        getBoxTotalWeight: function (boxIndex) {
            var total = 0;

            for (let item of self.boxes()[boxIndex].items) {
                total += parseFloat(item.weight);
            }

            return total;
        },

        toggleReceiveItem: function (data, event/* , boxIndex, itemIndex */) {
            data.item_received(event.target.checked);
        },

        /**
         * Set a box as 'received' or 'not received';
         * changes the UI;
         * send updates to ERP
         * @param {number} boxIndex We have a list of boxes (self.boxes())
         */
        toggleReceiveBox: function (boxIndex) {

            var confirmation = confirm("finish box receipt ? (The not received items will be marked as missing !)");

            if (confirmation == false) {
                return /* do nothing */
            }

            // toggle 'receive' status
            self.boxes()[boxIndex].wasReceived(!self.boxes()[boxIndex].wasReceived());

            // build items list
            let items_to_send = []

            for (item of self.boxes()[boxIndex].items) {

                items_to_send.push({
                    item_id: item.id,
                    received: item.item_received()
                });
            }


            // send updates to ERP
            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/receive_box",
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "box": {
                        box_id: self.boxes()[boxIndex].id,
                        received: self.boxes()[boxIndex].wasReceived(),
                        items: JSON.stringify(items_to_send)
                    }
                },

                async: false,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    // remove received box from UI
                    self.boxes.remove(self.boxes()[boxIndex]);
                    $.jGrowl("Box received");

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main_new");


                }

            });
        },

        openHistoryChat: function (boxIndex, itemIndex) {
            self.historyChat({
                item_name: self.boxes()[boxIndex].items[itemIndex].garment_name,
                item_chat: self.boxes()[boxIndex].items[itemIndex].chat,
                client_name: self.boxes()[boxIndex].items[itemIndex].customer_name
            });
        },

        closeHistoryChat: function () {
            self.historyChat(null);
        },

        sendNewMessage: function (boxIndex, itemIndex, message) {
            // send message via ajax
             console.log("sending new message...");

            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/add_new_item_log",
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "box_id": self.boxes()[boxIndex].id,
                    "item_id": self.boxes()[boxIndex].items[itemIndex].id,
                    "reason": message
                },

                async: true,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    // append locally
                    self.boxes()[boxIndex].items[itemIndex].chat.push({
                        "item_id": self.boxes()[boxIndex].items[itemIndex].id,
                        "reason": message,
                        "created_at": ret.timestamp,
                        "user_name": authCtrl.userInfo.username,
                    });

                    // clear input text
                    self.boxes()[boxIndex].items[itemIndex].newChat("");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.");


                }

            });
        }
    });
});