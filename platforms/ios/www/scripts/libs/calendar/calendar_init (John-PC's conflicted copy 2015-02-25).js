$(window).load(function() {
    $(create_calendar());
    $("#save_button").click(ajax_update_entry);
    
    $("#delete_event_button").click(delete_event);
    $("#delete_leave_button").click(delete_availability);
    
    $("#user_id").change(function() {
        //get_availability_on_date('');
		get_availability_salesman();
    });
    
    $("#user_id-edit").change(function() {
        get_availability_salesman_edit();
    });

    $("input[name='which_time_range']").live('click',select_time_range);

    $(".mws-datepicker-event-date").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
			get_availability_salesman();
           	check_availabilities_overlap();
            /*check_availability = dateText;
            $('input[name="event_date"]').val(dateText);
            /*get_availability_on_date('');*/
			/*get_availability_salesman();*/
        },
        showOtherMonths: true,        
		 altField: "#event_date",
		 altFormat: "yy-mm-dd"
    });
    
	$(".mws-datepicker-event-start").timepicker({
		hourMin: 8,
        hourMax: 19,       
        timeFormat: 'HH:mm',
        stepMinute: 15,
		showButtonPanel: false,
		showDuration: true,
		timeOnlyTitle: 'Event Start Time',
		altField: '#event_start_time',
		onSelect: function(dateText, inst) {
            //check_availability = dateText;
            //$('input[name="event_date"]').val(dateText);
            /*get_availability_on_date('');*/
			get_availability_salesman();
           	check_availabilities_overlap();
        }
	});
	

	$(".durationSlider").slider({
	    range: "min", 
	    min: 15,
	    max: 90,
	    value: 30, 
	    step: 15,
	    ticks: [15, '|', 30, '|', 45, '|', 60, '|', 75, '|', 90],
	  	change : function (event, ui) {
			get_availability_salesman();
		}
	});
	
	/*endTimePicker.timepicker({
		hourMin: 8,
        hourMax: 19,       
        timeFormat: 'HH:mm',
        stepMinute: 15,
		showButtonPanel: false,
		timeOnlyTitle: 'Event End Time',
		 onSelect: function(dateText, inst) { 
			var time = dateText.split(":");
			var hour = time[0];
	       	startTimePicker.timepicker('option', 'hourMax', hour );
		   	get_availability_salesman();
	    }
	});*/
	
	$(".mws-datepicker-event-start-edit").timepicker({
		 onSelect: function(dateText, inst) {
           /*$(".mws-datepicker-event-end").timepicker('option', 'hourMin', 3 );*/
		   get_availability_salesman_edit();
           check_availabilities_overlap_edit();
        },
		hourMin: 8,
        hourMax: 19,       
        timeFormat: 'HH:mm',
        stepMinute: 15,
		showButtonPanel: false,
		timeOnlyTitle: 'Event Start Time',
		altField: '#event_start_time-edit'	
	});
	

	$(".editDurationSlider").slider({
	    range: "min", 
	    min: 15,
	    max: 90,
	    value: 30, 
	    step: 15,
	    ticks: [15, '|', 30, '|', 45, '|', 60, '|', 75, '|', 90],
	  	change : function (event, ui) {
		   get_availability_salesman_edit();
		}
	});
	
    $(".mws-datepicker-av").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
            check_availability = dateText;
            $('input[name="event_date"]').val(dateText);
            check_availabilities_overlap('');
        },
        showOtherMonths: true,
        altField: '#availability_date'
    });
    
    $(".mws-datepicker-av-end").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
            check_availability = dateText;
            $('input[name="event_date-end"]').val(dateText);
            check_availabilities_overlap('');
        },
        showOtherMonths: true,
        altField: '#availability_date-end'
    });

    $(".mws-datepicker-av-edit").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
            check_availability = dateText;
            $('input[name="event_date-edit"]').val(dateText);
            check_availabilities_overlap('-edit');
        },
        showOtherMonths: true
    });
    
    $(".mws-datepicker-av-end-edit").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
            check_availability = dateText;
            $('input[name="event_date-end-edit"]').val(dateText);
            check_availabilities_overlap('-edit');
        },
        showOtherMonths: true
    });

    $(".mws-datepicker-edit").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
            check_availability = dateText;
            $('#event_date-edit').val(dateText);
            get_availability_on_date('-edit');
        },
        showOtherMonths: true,
    //altField: '#event_date-edit'
    });
    
	 $(".mws-datepicker-event-date-edit").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(dateText, inst) {
            /*check_availability = dateText;
            $('input[name="event_date"]').val(dateText);
            get_availability_on_date('');*/
			get_availability_salesman_edit();
        },
        showOtherMonths: true,        
		 altField: "#event_date-edit",
		 altFormat: "yy-mm-dd"
    });
	
    $("#datetimepicker-av-start").datetimepicker({
        hourMin: 8,
        hourMax: 19,
        numberOfMonths: 1,
        timeFormat: 'HH:mm',
        stepMinute: 60,
        altField: '#availability_date',
        altFieldTimeOnly: false,
        altFormat: "yy-mm-dd",
        altTimeFormat: "HH:mm:ss",
        altSeparator: " ",
        showButtonPanel: false,
        showMinute: false,
        onSelect: function () {
        $('#datetimepicker-av-end').datepicker('option', {
            minDate: $(this).datepicker('getDate')
        });
    }
    });
    
    $("#datetimepicker-av-end").datetimepicker({
        hourMin: 8,
        hourMax: 19,
        numberOfMonths: 1,
        timeFormat: 'HH:mm',
        stepMinute: 60,
        altField: '#availability_date-end',
        altFieldTimeOnly: false,
        altFormat: "yy-mm-dd",
        altTimeFormat: "HH:mm:ss",
        altSeparator: " ",
        showButtonPanel: false,
        showMinute: false
    });
    
    $("#datetimepicker-av-start-edit").datetimepicker({
        hourMin: 8,
        hourMax: 19,
        numberOfMonths: 1,
        timeFormat: 'HH:mm',
        stepMinute: 60,
        altField: '#availability_date-edit',
        altFieldTimeOnly: false,
        altFormat: "yy-mm-dd",
        altTimeFormat: "HH:mm:ss",
        altSeparator: " ",
        showButtonPanel: false,
        showMinute: false,
        onSelect: function () {
        $('#datetimepicker-av-end-edit').datepicker('option', {
            minDate: $(this).datepicker('getDate')
        });
	}
    });
    
    $("#datetimepicker-av-end-edit").datetimepicker({
        hourMin: 8,
        hourMax: 19,
        numberOfMonths: 1,
        timeFormat: 'HH:mm',
        stepMinute: 60,
        altField: '#availability_date-end-edit',
        altFieldTimeOnly: false,
        altFormat: "yy-mm-dd",
        altTimeFormat: "HH:mm:ss",
        altSeparator: " ",
        showButtonPanel: false,
        showMinute: false
    });

    $("#mws-form-dialog-av-edit").dialog({
        autoOpen: false,
        title: "Update Leave",
        modal: true,
        width: "600",
        buttons: [{
            text: "Submit",
            click: function () {
                $(this).find('form#mws-validate-av-edit').submit();
            }
        }]
    });
    $("#mws-form-dialog").dialog({
        autoOpen: false,
        title: "Add New Entry",
        modal: true,
        width: "900",
        buttons: [{
            text: "Submit",
            click: function () {
                $(this).find('form#mws-validate').submit();
            }
        }]
    });
    $("#mws-form-dialog-edit").dialog({
        autoOpen: false,
        title: "Edit Entry",
        modal: true,
        width: "900",
        buttons: [{
            text: "Submit",
            click: function () {
                $(this).find('form#mws-validate').submit();
            }
        }]
    });
    $("#mws-form-dialog-av").dialog({
        autoOpen: false,
        title: "Add New Leave",
        modal: true,
        width: "600",
        buttons: [{
            text: "Submit",
            click: function () {
                $(this).find('form#mws-validate-av').submit();
            }
        }]
    });
});