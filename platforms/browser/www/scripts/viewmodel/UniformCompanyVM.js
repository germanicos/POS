define(['jquery', 'knockout', 'base'], function($, ko) {
   

    /**
     * 1st step for uniform order:
     * 
     * This will be responsible for the creation of the company for
     * the uniform orders.
     */
    UniformCompanyVM = SimpleControl.extend({

        init: function() {

            var self = this;

            console.log("Init UniformCompanyVM...");

            this.company = this.getCompanyObject();
            this.mainCompanyContact = this.getCompanyContactObject(main = true);
            this.secondaryCompanyContacts = ko.observableArray([this.getCompanyContactObject(main = false)]);


            // Locations 
            this.cities = dsRegistry.getDatasource('citiesDS').getStore();
            this.states = dsRegistry.getDatasource('statesDS').getStore();
            this.countries = dsRegistry.getDatasource('countriesDS').getStore();

            this.referal_methods_list = ko.observableArray([{"name":"Google Search","id":"1"},{"name":"Bing Search","id":"8"},{"name":"Yahoo Search","id":"9"},{"name":"Print Advertisement","id":"2"},{"name":"TV Ad","id":"3"},{"name":"Radio Ad","id":"4"},{"name":"Billboard","id":"5"},{"name":"Other","id":"6"},{"name":"Referral","id":"7"}]);

            this.companyLogo = ko.observable("https://cdn4.iconfinder.com/data/icons/media-24/32/camera-512.png");
        },

        getCompanyObject : function() {

            // Todo : onsubmit copy address/postal_address city/state/country from main contact to company details
            const company = {
                    "name" : "",
                    "email" : "",
                    "ABN" : "",
                    "address" : "",
                    "country" : "1", // default => australia
                    "state" : "5", // default => vic
                    "city" : "6", // default => melbourne
                    "postal_address" : "",
                    "address_different_postal" : false

                };

            return ko.mapping.fromJS(company);
        },


         getCompanyContactObject : function(isMainContact = false) {

            const customer = {
                      "company_main_contact" : isMainContact,
                      "customer_first_name": "",
                      "customer_last_name": "",
                      "customer_DOB": "",
                      "customer_occupation": "",
                      "customer_company_name": "",
                      "customer_gender": "1", // default
                      "customer_address1": "",
                      "customer_address2": "",
                      "customer_country": "1",
                      "customer_state": "5", // vic
                      "customer_city": "6",
                      "customer_postal_code": "",
                      "customer_mobile_phone": "",
                      "customer_landline_phone": "",
                      "customer_email": "",
                      "customer_email_subscription": "",
                      "customer_best_time_to_contact": null,
                      "customer_referal_method": "",
                      "customer_refered_by" : "",
                      "customer_other_way" : "",
                      "customer_referal_moreinfo": "",
                      "customer_price_range": "",
                      "customer_cat_id": "",
                      "customer_how_did_hear": "",
                      "customer_landline_code": "",
                      "receive_marketing": true
                    }

            // makes all properties observable
            return ko.mapping.fromJS(customer);
        },


        submitNewCompany : function() {

            if(!confirm("Do you really want to submit the new company data ?")) { return;}

            const company = ko.mapping.toJS(this.company);
            
            company.logo = this.companyLogo();
            company.country = this.mainCompanyContact.customer_country();
            company.state = this.mainCompanyContact.customer_state();
            company.city = this.mainCompanyContact.customer_city();
            company.postal_address = company.address_different_postal ? company.postal_address : company.address;

            const mainContact = ko.mapping.toJSON(this.mainCompanyContact);
            const secondaryContacts = ko.mapping.toJSON(this.secondaryCompanyContacts);

            $.ajax({
                type: 'POST',
                timeout: 60000, // sets timeout to 60 seconds
                url: BUrl + 'orders_pos/create_new_company',
                dataType: 'json',
                data: { 
                    "user": authCtrl.userInfo,
                    "company" : JSON.stringify(company),
                    "mainContact" : mainContact,
                    "secondaryContacts" : secondaryContacts,
                },
                success: function (ret) {
                    
                    console.log(ret);

                    if( ret.result == 'success')
                    {   

                        $.jGrowl("Company Created !");

                        if( confirm(`Do you want to add the master design this company ?`) ) 
                        { 
                            // stores company id and call the router
                            localStorage.setItem('company_idUniformTemplate', ret.company.id);
                            posChangePage('#uniformTemplate');
                        }
                        else
                        {
                            customAlert("Going back to main..."); 
                            posChangePage('#main');
                        }

                    }
                    else
                    {
                        customAlert("Something wrong happned, ... try again please"); 
                        posChangePage('#main');
                    }
                   


                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                    customAlert("TIME OUT - there is a network issue. Please try again later"); 
                    posChangePage('#main');
                },
                async: false
            });


        },

        removeSecondaryContact: function(contact) {
            console.log("removing...", contact);
            this.secondaryCompanyContacts.remove(contact);
        },


        addSecondaryContact : function() {
            console.log("adding...");
            this.secondaryCompanyContacts.push(this.getCompanyContactObject(main = false));
        },


        takeLogoPic : function(option) {

            var self = this;
            
            console.log("taking logo pic");

            navigator.notification.confirm(
                'Choose Picture Source', // message
                function(btnIndex) {
        
                    // const source = buttonIndex === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY;
                    
                    takeImg(btnIndex);
                     
                },            // callback to invoke with index of button pressed
                'Confirmation',           // title
                ['Camera', 'Gallery']         // buttonLabels
            );

            function takeImg(source)
            {
                navigator.camera.getPicture(onSuccess, onFail,
                {
                    quality: 10,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: source,
                    encodingType: Camera.EncodingType.JPEG,
                    mediaType: Camera.MediaType.PICTURE,
                    saveToPhotoAlbum: true,
                    correctOrientation : true,
                }); 
            }
        
            function onSuccess(imageURI) { 
                console.log("onSuccess", imageURI);
                window.resolveLocalFileSystemURL(imageURI, resolveOnSuccess, resOnError); 
            }  
        
            function onFail(message) { 
                alert('Failed because: ' + message); 
            } 
        
            function resolveOnSuccess(entry) {
                 
                const folderName = "ClientMediaStorage";
                const imageName = (new Date()).getTime() + '.jpg';
                 
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
                    //The folder is created if doesn't exist
                     
                    fileSys.root.getDirectory(folderName,
                        { create: true, exclusive: false },
                        function (directory) {
                            entry.moveTo(directory, imageName, successMove, resOnError);
                        },
                        resOnError);
                    },

                resOnError);
            };
        
            function successMove(entry) {
                console.log('After move');
                 
                console.log("entry.toURL()", entry.toURL());
                self.companyLogo(entry.toURL());
        
            };
        
            function resOnError(error) {
                alert("Error Writting the file....");
                console.log("Failed... check console");
                console.log(error);
            };

        },
    });


});