define(['jquery', 'knockout', 'base'], function ($, ko) {
    Shipping_trackingVM = Class.extend({

        init: function () 
        {
            self = this;
            this.boxes = ko.observableArray();
            this.requestData();
        },

        /**
         * @description Request necessary data from ERP
         */
        requestData: function () {
            console.log("requesting data for tracking boxes ...");

            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/get_box_tracking",
                dataType: 'json',
                data: { "user": authCtrl.userInfo },
                async: true,
                success: function (ret) {

                    console.log("tracking data", ret);
                    changeVisibilityDiv('loading_jp');

                    for (box of ret.boxes) 
                    {
                        for(let i in box.tracking_data.checkpoints )
                        {
                            const point = box.tracking_data.checkpoints[i];

                            const planeImg = (i == 0 ? 'plane1.png' : 'plane2.png');
                            const pointClass = (i == 0 ? 'lower-plane' : 'upper-plane');

                            // if the box is with customs => change icon
                            if( point.message.indexOf('customs') != -1 )
                            {
                                planeImg = "customs.png";
                            }
                            else if( point.message.indexOf('delivered') != -1) // if the packet is deliveried
                            {
                                pointClass = 'lower-plane';
                            }

                            point.pointClass = pointClass;
                            point.planeImg = BUrl + 'misc/logistics_gfx/' + planeImg;
                        }

                        self.boxes.push(box);
                    }

                    self.bindSearchEvents()

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main");


                }

            });
        },

        /**
         * Binds the search events made with jquery -- reusing code from ERP
         * @return {[type]} [description]
         */
        bindSearchEvents : function() {

            $(".search-shipping-tracking").off().on("keyup", function() {

                const name = $(this).val().trim().toLowerCase();
                console.log("searching for", name);

                let showing = 0;
                const total = $(".box-wrapper").length;

                if( name === "")
                {
                    $(".box-wrapper").show();
                    $(".box-showing-results").html(`Showing ${total}/${total}`);
                    return;
                }

                $(".box-wrapper").hide();
               
                $(".box-details").each(function() {

                    const $html = $(this).html().trim().toLowerCase();

                    if( $html.includes(name) )
                    {
                        showing += 1;
                        $(this).closest('.box-wrapper').show();
                    }
                });


                $(".box-showing-results").html(`Showing ${showing}/${total}`);
            });

            $(".clear-search-btn").off().on("click", function() {

                const total = $(".box-wrapper").length;

                $(".box-wrapper").show();
                $(".box-showing-results").html(`Showing ${total}/${total}`);
                
                $(".search-shipping-tracking").val("");

                $.jGrowl("Search cleared !");
            });
        }

    });
});