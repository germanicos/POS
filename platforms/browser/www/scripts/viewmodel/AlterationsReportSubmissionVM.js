define(['jquery', 'knockout', 'base', 'slick'], function ($, ko) {

	ko.bindingHandlers.slick = {
		init: function (element, valueAccessor, allBindingsAccessor, data, bindingContext) {


			alert("slick binding...");

			return { controlsDescendantBindings: true };
		},
		update: function (element, valueAccessor, allBindingsAccessor, data, context) {

		}
	};


	// Alerts => $.jGrowl('Message')

	/*
		Represents the costs that the Tailor will provide for each alteration
		It can be either by COST (money) or by HOURS
	*/
	function costClass(description, cost, hours, type) {
		self = this;
		var that = this;

		self.description = ko.observable(description);
		self.cost = ko.observable(cost);
		self.hours = ko.observable(hours);
		self.minutes = ko.observable("");
		
		self.type = ko.observable(type);

		if (self.cost() > 0) {
			self.hours("");
		}
		else if (parseInt(self.hours()) <= 0) {
			self.cost("");
		}
	}


	AlterationsReportSubmissionVM = SimpleControl.extend({

		init: function (initData) {

			var self = this;
			console.log("Init alterations report submission VMs", initData)

			// fitting id for the alteration
			this.fitting_id = initData.fitting_id;

			// alteration data - Customer data, order, garment, ...
			this.alteration = ko.observable();

			// left panel measurements
			this.left_panel_history = ko.observableArray([]);

			// right panel measurements
			this.right_panel_history = ko.observableArray([]);

			// Left and righrt panel measurments
			this.all_panel_history = ko.observableArray([]);

			// panel images (to help the user fill up the panel measurments)
			this.panel_images = ko.observable();

			// panel images that will be under the drawing
			this.drawing_placeholders = ko.observable();

			// type of cost used by the tailor (by COST or HOURLY)
			this.typeCost = ko.observable();

			// Holds each costs
			this.alteration_costs = ko.observableArray([]);

			// set pop-up visible or not
			this.emailPopUpIsVisible = ko.observable(false);

			// If the type of cost changes => we change the UI representation
			this.typeCost.subscribe(function (type) {

				console.log("chaging type : ", type)

				ko.utils.arrayForEach(self.alteration_costs(), function (cost) {

					if (type === "cost") {
						cost.hours("");
						cost.minutes("");
					}
					else {
						cost.cost("");
					}

					cost.type(type);

				});

			});

			// the type of cost will be default to COST (money)
			this.typeCost("cost");

			// Image tags texting to put into the alteration description
			this.tags_text = ko.observableArray([]);

			this.tailors = ko.observableArray(dsRegistry.getDatasource('tailorsDS').getStore());

			// In case user gives up changing the tailor.
			this.previousTailor = ko.observable(false);

			this.selectedTailor = ko.observable(false);


			this.totalDescriptionTime = ko.computed(function() {

				let totalHours = 0;
				let totalMinutes = 0;

				for(let desc of self.alteration_costs() )
				{
					if( desc.minutes() > 0 )
					{
						totalMinutes += parseInt(desc.minutes());
					}

					if( desc.hours() > 0  )
					{
						totalHours += parseInt(desc.hours());
					}
				}

				// gets the extra hours from minutes
				totalHours += Math.floor(totalMinutes / 60);
				// the rest must be the minutes
				totalMinutes = totalMinutes % 60; 

				const returnString = totalHours + ':' + totalMinutes;

				console.log("returnString", returnString);
				return returnString;
			});


			this.totalDescriptionCost = ko.computed(function() {

				let totalCost = 0;

				for(let desc of self.alteration_costs() )
				{
					if( !isNaN(parseFloat(desc.cost())) )
					{
						totalCost += parseFloat(desc.cost());
					}
				}

				returnString = totalCost.toFixed(2) + ' $';

				return returnString;
			});


			this.blank_panel_imgs = ko.observableArray([]);


			// POST to the server to the get alteration details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/get_alteration_report_data",
				dataType: 'json',
				data: { "user": authCtrl.userInfo, "fitting_id": initData.fitting_id },
				async: false,
				timeout: 60000,
				success: function (ret) {

					console.log("Alt report list", ret);
					self.alteration(ret.alteration);

					const left_panel = [];
					const right_panel = [];

					for(let meas of ret.alteration.panel_history)
					{
						meas.currentAlteration = ko.observable(parseFloat(meas.latest_val).toFixed(2));

						if(meas.position.trim() === "left")
						{
							// Checks if exist panel type inside the left panel measurments
							
							const exists_panel = left_panel.filter( e => e.panel_id == meas.panel_image_id ).length > 0;

							// Create the structure
							if(!exists_panel)
							{
								left_panel.push({ 'panel_id' : meas.panel_image_id, 'panel_name' : meas.panel_image_name, 'measurements' : [] }); 
							}


							// adds the measurment
							left_panel.filter( e => e.panel_id == meas.panel_image_id )[0].measurements.push(meas);
						}
						else
						{
							const exists_panel = right_panel.filter( e => e.panel_id == meas.panel_image_id ).length > 0;

							// Create the structure
							if(!exists_panel)
							{
								right_panel.push({ 'panel_id' : meas.panel_image_id, 'panel_name' : meas.panel_image_name, 'measurements' : [] }); 
							}

							// adds the measurment
							right_panel.filter( e => e.panel_id == meas.panel_image_id )[0].measurements.push(meas);
							
						}
					}


					// Getting image tags from images
					for( let tag_text of ret.alteration.tags_text)
					{
						self.alteration_costs.push(new costClass(tag_text, "", "", "cost"));
					}

					// if there are not tags in the images => adds the first blank cost
					if( self.alteration_costs().length === 0)
					{
						self.alteration_costs.push(new costClass("", 0.0, 0, 'cost') );
					}
					

					for(let garment_type in ret.blank_panel_imgs)
					{
						// Only get the panel for this alteration garment type
						if(garment_type != ret.alteration.garment_name.toLowerCase() )
						{
							continue;
						}

						const data = ret.blank_panel_imgs[garment_type];

						for(let panel_name in data)
						{
							const path = data[panel_name];

							self.blank_panel_imgs.push({
									"panel_name" : panel_name.replace("_", " "),
									"path" : BUrl + path
							});
						}
					}


					console.log("left", left_panel);	
					console.log("right", right_panel);

					ko.utils.arrayPushAll(self.left_panel_history, left_panel);
					ko.utils.arrayPushAll(self.right_panel_history, right_panel);
					
					// Merge left and right panel measurments and put into only one list
					ko.utils.arrayPushAll(self.all_panel_history, ret.alteration.panel_history);

					self.panel_images(ret.panel_images);
					self.drawing_placeholders(ret.drawing_placeholders);

					self.slick_render();

					self.selectedTailor(self.tailors().filter( e => e.user_id == ret.alteration.alteration_tailor_id )[0] );
					self.previousTailor(self.selectedTailor());
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					console.log("fitting comments endpoint cannot be reached! ");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
					customAlert(XMLHttpRequest);
					customAlert(textStatus);
					customAlert(errorThrown);
				}
			});


			this.callslick = ko.observable(false);

			this.callslick.subscribe(function () {

				console.log("chaging type : ", type)
				this.slick_render();

			});

			// popup for measurements guide
			this.measurementsGuidePopUp = new self.MeasurementsGuidePopUp(self.panel_images());

		},

		/**
		 * Init a briefDetails with the corresponding fit and go to next page
		 */
		goToBriefDetails: function (nextPage, fitting_id) {
		    briefDetails = new BriefDetailsVM({"fitting_id" : fitting_id});
		    posChangePage(nextPage)
		},


		/** set pop-up visible or not  */
		toggleEmailPopUp() {
			console.log("toglelling...");
			this.emailPopUpIsVisible(!this.emailPopUpIsVisible());
		},

		/** Send an e-mail */
		sendEmail() {

			// make sure that email is not null or blank
			if (document.getElementById("emailTosend").value == "" || document.getElementById("emailTosend").value == null)
			{
				customAlert("E-mail can not be blank");
			}

			// Close the email pop up -- we dont need to wait the server to process
			this.toggleEmailPopUp();

			// POST to the server to the get brief details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/send_brief_email",
				dataType: 'json',
				data: {
					"user": authCtrl.userInfo,
					"fitting_id": this.fitting_id,
					"email": document.getElementById("emailTosend").value,
					"content": document.getElementById("contentToSend").value
				},
				async: true,

				success: function (ret) 
				{
					if (ret.result == "success")
					{
						$.jGrowl("Email sent");
					} 
					else 
					{
						customAlert("Something went wrong -- EMAIL NOT SENT (ERROR 1)");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) 
				{
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
					customAlert("Something went wrong -- EMAIL NOT SENT (ERROR 2)");
				}
			});
		},

		new_cost_row: function (type) {
			console.log("new cost...", this.typeCost());

			this.alteration_costs.push(new costClass("", "", "", this.typeCost()));
		},

		remove_cost_row: function (cost) {
			console.log("removing cost...", cost);

			if (this.alteration_costs().length > 1) 
			{
				this.alteration_costs.remove(cost);
			}
		},

		submit_alteration_report: function () {

			self = this;
			console.log("tags", tags);
			if (!confirm("Do you want to submit the alteration report ?")) {
				return;
			}

			// loader
			changeVisibilityDiv('loading_jp');

			let panel_meas = {};


			// gets the measurments
			$(".meas-input").each(function () {

				const value = $(this).val() || "0.0";
				const id = $(this).data("id");

				panel_meas[id] = value;
			});


			console.log("drawings...", drawings);
			console.log("meas", panel_meas);

			let costs = this.alteration_costs().map(function (el) {

				let hours = 0;

				if( el.hours() > 0  )
				{
					hours += parseInt(el.hours());
				}

				// convert to hours
				if( el.minutes() > 0 )
				{
					hours += parseInt(el.minutes()) / 60.0;
				}

				return { "desc": el.description(), "cost": el.cost(), "hours": hours };
			});

			console.log("costs", costs);

			// POST to the server to the get alteration details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/submit_alteration_report_ajax",
				dataType: 'json',
				data: {
					"user": authCtrl.userInfo,
					"alteration_id": self.alteration().alteration_id,
					"panel_meas": JSON.stringify(panel_meas),
					"drawings": JSON.stringify(drawings),
					"costs": JSON.stringify(costs)
				},
				async: false,
				success: function (ret) {

					console.log(ret);

					// remove loader
					changeVisibilityDiv('loading_jp');

					posChangePage("#alterationsDashBoard");
					$.jGrowl('Alteration Report SAVED !');

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {

					console.log("XMLHttpRequest", XMLHttpRequest);
					console.log("textStatus", textStatus);
					console.log("errorThrown", errorThrown);

					// remove loader
					changeVisibilityDiv('loading_jp');
					posChangePage("#alterationsDashBoard");
					//$.jGrowl('Something went wrong :((((((');
					alert("Network Error, Please try to submit again.")
				}
			});
		},

		uploadImage: function () {

			self = this;

			changeVisibilityDiv('loading_jp');

			// get base64
			const fullValue = $("#encodedIMG").val();

			// remove the HTML markup
			const base64 = fullValue.substring(fullValue.indexOf(",") + 1, fullValue.length);;

			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/upload_alteration_report_invoice_img",
				dataType: 'json',
				data: {
					"user": authCtrl.userInfo,
					"base64": base64,
					"alteration_id": self.alteration().alteration_id
				},

				async: true,
				success: function (ret) {

					console.log(ret);

					changeVisibilityDiv('loading_jp');

					if (ret.result == "success") {
						alert("Invoice image uploaded successfully, you can submit the alteration !");
					}
					else {
						alert("Invoice image upload crashed... Please try again");
					}

					console.log("Uploaded successfully", ret);


				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {

					console.log("POST error");
					console.log("XMLHttpRequest", XMLHttpRequest);
					console.log("textStatus", textStatus);
					console.log("errorThrown", errorThrown);

					changeVisibilityDiv('loading_jp');

					alert("Invoice image upload crashed..... Please try again");
				}
			});


		},

		/*

		  timeout => to give time for the screen to load and then call the libraries that affects UI.

		  TODO : needs a better fix.
	  */
		slick_render: function () {

			that = this;

			drawings = undefined;

			var timeout = setTimeout(function () {

				console.log("Trying visible....");

				// reset global variables

				// Hack to do the script only when the pane is fully loaded and visible
				if ($("#panel-carrousel").is(":visible")) {

					const $html = that.drawing_placeholders().map(function (e) {

						return `<div>
									<div class="panel-img-holder container-placeholder">
										<img width="150px" data-name="" data-src="${BUrl + e.path}" data-id="${e.id}" class="placeholder" src="${BUrl + e.path}"/>
									</div>
								</div>`;

					}).join('')

					$("#panel-carrousel").html($html);

					$('#panel-carrousel').slick({
						centerMode: true,
						slidesToShow: 3,
						centerMode: true,
						arrows: true,
						focusOnSelect: true
					});

					/////////// Start raphael Drawing logic ///////////////

					const $raphael = ` 

									<span style="position: absolute; top: 7px; right: 0;color: rgb(255, 255, 255); text-align: center; padding: 4px; font-size: 14px; font-weight: bold; cursor: pointer;"><span class="save_drawing" style="background: green none repeat scroll 0% 0%;padding:8px;">SAVE</span> <span class="discard_drawing" style="background: red none repeat scroll 0% 0%;padding:8px;">X</span></span>
									<div id="imgContainer" style="transform-origin: 0px 0px; -webkit-transform-origin:0px 0px;    -ms-transform-origin:0px 0px;">

										<img id='panel-img' data-id="" src="">
										<div id='raphael_drawing' onclick="openTagDialog(event);" ></div>                                        
					
									</div>`

					$("#raphael-wrapper").html($raphael);


					/* Call back when the user selects one panel to draw. 
						the code will retrive which img the user needs to draw
						and will put in the drawing placeholder.

						After it will check if there is already a drawing for that image.
							If yes => will recover the drawing and recreate the skecthpad
							if no => will create a completelty new sckechpad
					*/
					$(".panel-img-holder").on("click", function () {

						PANEL_WIDTH = 300;
						PANEL_HEIGHT = 492;

						const src = $(this).find("img").attr("src");
						const panel_id = $(this).find("img").data("id");

						$("#raphael_drawing").html(""); // clean the drawings

						sketchpad = Raphael.sketchpad("raphael_drawing", {
							width: PANEL_WIDTH,
							height: PANEL_HEIGHT,
							strokes: [],
							editing: true
						});

						// default functinality should be tagging
						sketchpad.editing(false);
						$("#drawings-tagging-select").val('Tagging').trigger("change");

						self.sketchpad = sketchpad;

						if (drawings[panel_id] != undefined) // if exists => recreate the strokes
						{
							sketchpad.json(drawings[panel_id].strokes)
						}

						console.log("clicking... src = ", src);

						$("#panel-img").attr("src", src);
						$("#panel-img").data("id", panel_id);

						$("#raphael-wrapper-father").show();

					}); // end click to draw call back



					/*
						This will save the drawing for the current draw
					*/
					$(".save_drawing").on("click", function () {

						// Before closing the pop up => we must set the mode for tagging again to prevent errors
						sketchpad.editing(false);
						$("#drawings-tagging-select").val('Tagging').trigger("change");


						const id = $("#panel-img").data("id");

						drawings[id] = { "strokes": sketchpad.json(), 'taggd': tags[id] };

						$("#raphael-wrapper-father").hide();

						// add marker to the panel crousell placeholder
						if( (sketchpad.strokes().length || tags[id].length ) && !$(lastClickedSlickDiv).find(".has-drawing-tag").length )
						{
							const marker = `<div class="has-drawing-tag"></div>`;

							$(lastClickedSlickDiv).append(marker);
						}
						

						$.jGrowl(`Drawing Saved !!!`);
					});


					// reset the drawing and tags
					$(".discard_drawing").on("click", function () {

						if (confirm(`Discard Drawing ?`)) {

							// Before closing the pop up => we must set the mode for tagging again to prevent errors
							sketchpad.editing(false);
							$("#drawings-tagging-select").val('Tagging').trigger("change");

							const id = $("#panel-img").data("id");

							drawings[id] = undefined;
							tags[id] = undefined;

							$("#raphael_drawing").html("");

							$("#raphael-wrapper-father").hide();

							// Remove the marker
							$(lastClickedSlickDiv).find(".has-drawing-tag").remove();
						
							$.jGrowl(`Drawing DISCARDED !!!`);
						}

					});


					console.log("clearing timeout...");
					window.clearTimeout(timeout);
				}
				else {
					console.log("not visible yet...")
				}

				// This hold the last clicked element in order to 
				// add or remove the indicator of drawings/tags in the panel (red dot)
				
				$(".panel-img-holder").click(function() {
					console.log("Selecting last clicked element...");
					lastClickedSlickDiv = $(this); // defined in the view...
				});

			}, 100); // end timing





		},


		undoDrawing : function() {

			console.log("undo Drawing...");

			if( sketchpad != undefined)
			{
				sketchpad.undo();
				$.jGrowl(`Undoing drawing...`);
			}
			else
			{
				console.log("Sketchpad variable not found...");
			}



		},

		redoDrawing : function() {
			
			console.log("redo Drawing...");
			
			if( sketchpad != undefined)
			{
				sketchpad.redo();
				$.jGrowl(`Redoing drawing...`);
			}
			else
			{
				console.log("Sketchpad variable not found...");
			}

		},


		changeTailor : function() {


			let that = this;

			if(!confirm("Do you really want to change the tailor ?"))
			{
				console.log("Putting the old tailor back...");
				that.selectedTailor(that.previousTailor());
				return;
			}

			// Updates the previous value if the user accepts the change
			that.previousTailor(that.selectedTailor());


			// POST to the server to the get brief details
			$.ajax({
				type: 'POST',
				url: BUrl + "alterations_pos/update_alteration_tailor",
				dataType: 'json',
				data: { "user": authCtrl.userInfo, "fitting_id": that.fitting_id, "tailor_id" : that.selectedTailor().user_id },
				async: true,
				success: function (ret) {

					$.jGrowl(`Tailor Changed !`);
					console.log("set_alteration_to_tailor success");

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) 
				{
					console.log("fitting comments endpoint cannot be reached! ");
					console.log(XMLHttpRequest + " " + textStatus + " " + errorThrown);
				}
			});


		},

		MeasurementsGuidePopUp: class {

			/** @param {array} imagesToShow
			 * ex: [{
			 *	"id": "7",
			 *	"garment_type": "2",
			 *	"name": "shirt front",
			 *	"path": "misc/panels/panel_images/shirt/shirt_front.jpg",
			 *	"display_order": "1"
			 * }]
			*/
			constructor (imagesToShow){
				this.imagesToShow = ko.observableArray(imagesToShow);
				this.fixImagesPath();
				this.isVisible = ko.observable(false);

			}

			/** hide/show popup */
			toggleVisible(){
				this.isVisible(!this.isVisible());
			}

			/** Edit path to match current server 
			 * current server == window.BUrl
			*/
			fixImagesPath() {
				
				// Resize the image in the server ---> Original is too big
				//const resizerPath = window.BUrl + "image.php/?width=980&amp;height=800&amp;&image=";

				for (let image of this.imagesToShow())
				{
					image.path = (window.BUrl + image.path);
				}
			}

		}


	});


});