define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingFaultyRemakeGarments = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		this.ShippingFaultyRemakeGarmentsData     = [];
		this.ShippingFaultyRemakeGarmentsDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);

		this.workflow    = ko.observableArray([
			{id: 0, target: "#shipping_faulty_remake_garments",  completed: "false", caption: "FaultyRemakeGarment", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
			$('.cloneDialog').remove(); 
			that.renderShippingFaultyRemakeGarments();
		});
		
		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});
		
		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});

		this.completion	= ko.observable(0);
		this.variantNameShippingFaultyRemakeGarments  = ko.observableArray([{id: 0, title: "ShippingFaultyRemakeGarments 1"}]);
		this.selectedVariantShippingFaultyRemakeGarments  = ko.observable(this.variantNameShippingFaultyRemakeGarments()[0]);	
		this.selectedVariantShippingFaultyRemakeGarments.subscribe(function(data) {
			that.selectedVariantShippingFaultyRemakeGarments();
			that.renderShippingFaultyRemakeGarments();			
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});


		this.WhoseFaultList = ko.observableArray(["Manufacturer", "Fabric supplier", "Salesman / measurer", "HQ error"]);
		this.WhoseFault = ko.observable('');
		this.WhoseFault.subscribe(function(data) {
			if(data != undefined){
				that.ShippingFaultyRemakeGarmentsData[ that.getRow(that.selectedVariantShippingFaultyRemakeGarments().id)  ].WhoseFault = data;
				that.flushModel();
			}		
		});
				
		this.FaultDescription = ko.observable('');
		this.FaultDescription.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyRemakeGarmentsData[ that.getRow(that.selectedVariantShippingFaultyRemakeGarments().id)  ].FaultDescription = data;
				that.flushModel();
			}	
		});
		
		this.VideoUrl = ko.observable('');
		this.VideoUrl.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyRemakeGarmentsData[ that.getRow(that.selectedVariantShippingFaultyRemakeGarments().id)  ].VideoUrl = data;
				that.flushModel();
			}	
		});

		this.VideoComments = ko.observable('');
		this.VideoComments.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyRemakeGarmentsData[ that.getRow(that.selectedVariantShippingFaultyRemakeGarments().id)  ].VideoComments = data;
				that.flushModel();
			}	
		});
		
		this.Garment = ko.observable(false);
		this.Garment.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingFaultyRemakeGarmentsData[ that.getRow(that.selectedVariantShippingFaultyRemakeGarments().id)  ].Garment = data;
				that.flushModel();
			//	that.updateView();
			}	
		});
		
		

		this.updateView = function(data){
console.log( "updateView: " + JSON.stringify(data) );
			that.Garment(data);
					
			if(customersVM.systemMode == "shipping_remake_garments"){
				
				document.getElementsByName("jacketmeasurements")[0].style.display = "none";
				document.getElementsByName("pantmeasurements")[0].style.display = "none";
				document.getElementsByName("vestmeasurements")[0].style.display = "none";
				document.getElementsByName("shirtmeasurements")[0].style.display = "none";
				
				if(data.checked == true){
					document.getElementsByName("mygarmentname")[0].style.display = "block";
					if(data.garment == 'Suit/J' || data.garment == 'Jacket' || data.garment == 'jacket'){
						document.getElementsByName("jacketmeasurements")[0].style.display = "block";		
					}else if(data.garment == 'Suit/P' || data.garment == 'Pants' || data.garment == 'pants' || data.garment == 'Pant' || data.garment == 'pant'){							
						document.getElementsByName("pantmeasurements")[0].style.display = "block";
					}else if(data.garment == 'Vest' ||data.garment == 'vest'){			
						document.getElementsByName("vestmeasurements")[0].style.display = "block";
					}else if(data.garment == 'Shirt' ||data.garment == 'shirt'){
						document.getElementsByName("shirtmeasurements")[0].style.display = "block";
					}									
				}else{
					document.getElementsByName("mygarmentname")[0].style.display = "none";				
				}


			}	

		}		
		

///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var photosarray = [];
			var images = localStorage.getItem('ShippingFaultyRemakeGarmentsPhotos');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('ShippingFaultyRemakeGarmentsPhotos') != null){
				if(localStorage.getItem('ShippingFaultyRemakeGarmentsPhotos').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			var tagsarray = [];
			var tags = localStorage.getItem('ShippingFaultyRemakeGarmentsTags');
			tags = (tags == null) ? [] : JSON.parse(tags);
			if(localStorage.getItem('ShippingFaultyRemakeGarmentsTags') != null){
				if(localStorage.getItem('ShippingFaultyRemakeGarmentsTags').length > 0){
					for (var j=0; j<tags.length; j++){
						tagsarray.push(tags[j]);
				 	}
				}  
			}
			
			var customerString = orderItem.custSelectVM.selectedCustomer();
	
			var garment = "";
			var orders_products_id = "";
			for (var a in orderItem.AlterationsVM.alterations() ) { 
				for (var b in orderItem.AlterationsVM.alterations()[a].products){
					if (orderItem.AlterationsVM.alterations()[a].products[b].checked == true){
		   				garment = orderItem.AlterationsVM.alterations()[a].products[b].garment;
		   				orders_products_id  = orderItem.AlterationsVM.alterations()[a].products[b].orders_products_id;
					}
				}
			}

console.log("this.WhoseFault(): " + this.WhoseFault());

			if(garment == ""){
				customAlert("Please select a garment");
			}else if(this.WhoseFault() == "" || this.WhoseFault() == undefined){
				customAlert("Please select the source of the fault");
			}else if(this.FaultDescription().trim() == ""){
				if(customersVM.systemMode == "shipping_faulty_garments"){
					customAlert("Please enter a description for the fault");
				}else{
					customAlert("Please enter a description");
				}	
		//	}else if( this.Garment().checked == false ||  this.Garment().checked == undefined){
		//		customAlert("Select a garment");
			}else if(images.length  == 0){
				customAlert("Please upload at least one image");
			}else if(this.VideoUrl().trim() == ""){
				customAlert("Please enter a video url");
			}else{
				var data = {};
				var endpoint_url = BUrl;
				var salesman = new Object();
		   		salesman.username = authCtrl.username();
		   		salesman.password  = authCtrl.password();
		   		salesman.id  = authCtrl.userInfo.user_id;
				
				if(customersVM.systemMode == "shipping_faulty_garments"){
					data = { 
						salesman: salesman,
						customer: customerString,
						garment: garment,
						garment_id: orders_products_id,
						fault: this.WhoseFault(),
						fault_description: this.FaultDescription(),
						video: this.VideoUrl(),
						video_comments: this.VideoComments(),
						images: photosarray,
						tags: tagsarray
					};	
					endpoint_url += 'client_logistics/faulty_garment_endpoint';
				}else{
					data = { 
						salesman: salesman,
						customer: customerString,
						garment: garment,
						garment_id: orders_products_id,
						fault: this.WhoseFault(),
						fault_description: this.FaultDescription(),
						video: this.VideoUrl(),
						video_comments: this.VideoComments(),
						images: photosarray,
						tags: tagsarray,
						measurements: orderItem.measurementsVM.flushGarment(garment)
					};	
					endpoint_url += 'client_logistics/remake_garment_endpoint';					
				}
					
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);

				$.post(endpoint_url, {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Succesfully added");
		                posChangePage('#shipping_main');
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingFaultyRemakeGarments"  : this.ShippingFaultyRemakeGarmentsData
		}, true);
		this.renderShippingFaultyRemakeGarments();
	},

	getVariant: function(id) {
		var toreturn = this.ShippingFaultyRemakeGarmentsData[0];
		for (var ind in this.ShippingFaultyRemakeGarmentsData) {
			if ( this.ShippingFaultyRemakeGarmentsData[ind].variantId == id  ) {
				toreturn = this.ShippingFaultyRemakeGarmentsData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingFaultyRemakeGarmentsData) {
			if ( this.ShippingFaultyRemakeGarmentsData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantShippingFaultyGarment: function(name) {
		if(this.variantNameShippingFaultyRemakeGarments()[0].title != "ShippingFaultyRemakeGarments 1"){
			this.ShippingFaultyRemakeGarmentsDataAID += 1;
			var newid = this.ShippingFaultyRemakeGarmentsDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFaultyRemakeGarments().id ) )  ); //CLONE Object
			this.variantNameShippingFaultyRemakeGarments.push({title: vname, id: this.ShippingFaultyRemakeGarmentsDataAID});
			tObj.variantId = this.ShippingFaultyRemakeGarmentsDataAID;		
			this.flushModel();
		}else{
			this.variantNameShippingFaultyRemakeGarments()[0].title = name;
		}
	},	
	
	addVariantsShippingFaultyRemakeGarments: function() {
		for(var x in orderItem.ShippingFaultyRemakeGarmentsVM.ShippingFaultyRemakeGarmentsData){
		//	console.log("addVariantsShippingFaultyRemakeGarments " + x);
			if( x != 0){
				this.ShippingFaultyRemakeGarmentsDataAID += 1;
				var vname = "ShippingFaultyRemakeGarments " + x; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFaultyRemakeGarments().id ) )  ); //CLONE Object
				this.variantNameShippingFaultyRemakeGarments.push({title: vname, id: this.ShippingFaultyRemakeGarmentsDataAID});
				tObj.variantId = this.ShippingFaultyRemakeGarmentsDataAID;		
				this.flushModel();
			}else{
				var vname = "ShippingFaultyRemakeGarments " + x; 
				//var vname = orderItem.ShippingFaultyRemakeGarmentsVM.ShippingFaultyRemakeGarmentsData[x].pant;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingFaultyRemakeGarments().id ) )  ); //CLONE Object
				this.variantNameShippingFaultyRemakeGarments()[x].title = vname;
				tObj.variantId = this.ShippingFaultyRemakeGarmentsDataAID;		
				this.flushModel();
			}
		}
	},

	digestData: function(data) {
		this.ShippingFaultyRemakeGarmentsData  = data.ShippingFaultyRemakeGarments;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingFaultyRemakeGarments();
	},

	renderShippingFaultyRemakeGarments: function() {	
		//Get selected Variant		
		try{
			var tData = this.getVariant(this.selectedVariantShippingFaultyRemakeGarments().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.Garment)			!= "undefined") {this.Garment(tData.Garment);}
				if (typeof(tData.WhoseFault)    	!= "undefined") {this.WhoseFault(tData.WhoseFault);}
				if (typeof(tData.FaultDescription)  != "undefined") {this.FaultDescription(tData.FaultDescription);}
				if (typeof(tData.VideoUrl)   		!= "undefined") {this.VideoUrl(tData.VideoUrl);}
				if (typeof(tData.VideoComments)    	!= "undefined") {this.VideoComments(tData.VideoComments);}
			}
		}catch(e){
			;
		}

	}
});

defShippingFaultyRemakeGarments = SimpleDatasource.extend({
	init: function( name, dsRegistry) {
	//	if(data == null || data == undefined){
			var ShippingFaultyRemakeGarments = {};
			ShippingFaultyRemakeGarments.variantId        = 0;
		
			ShippingFaultyRemakeGarments.Garment = false;
			ShippingFaultyRemakeGarments.WhoseFault = '';
			ShippingFaultyRemakeGarments.FaultDescription = '';
			ShippingFaultyRemakeGarments.VideoUrl = '';
			ShippingFaultyRemakeGarments.VideoComments = '';
			
			var iShippingFaultyRemakeGarments = {
				ShippingFaultyRemakeGarments: []
			};
			iShippingFaultyRemakeGarments.ShippingFaultyRemakeGarments.push(ShippingFaultyRemakeGarments);
			this._super(name, iShippingFaultyRemakeGarments, dsRegistry);
	//	}	
	}
});


//END DEFINE CLOSURE
});
