define(['jquery', 'knockout', 'base'], function($, ko) {
	FabricsCntClass = SimpleControl.extend({
		init: function(DsRegistry) {
			this._super( DsRegistry );
			var that = this;
			

			this.fabrics  = ko.observableArray([]);
			this.fabricsArray = {}; 
			this.fabricsTrie = new Trie();
			that.mode = true;
			
			this.selected_sug    = ko.observable(); 
			this.selected_sug.subscribe(function(data) {
				that.selected_fabric(that.fabricsArray[data.title]);
				// passing the data back to the fabricAutocomplete observable so that the input box will not be empty
				that.mode = false;
				that.fabricAutocomplete(that.fabricsArray[data.title].title);
				that.checkIsOnStock(data);
				that.mode = true;
				document.getElementById("fabricinfo").style.display = "block";
			});

			this.selected_fabric = ko.observable();
			this.selected_fabric.subscribe(function(data) {
				;	
			});
			
			
			this.fabricAutocompleteList = ko.observableArray();
			this.fabricAutocomplete = ko.observable();
			this.fabricAutocomplete.subscribe(function(data) {
				
				var context = that.dsRegistry.getContext();
				var whatGarmentFabric = context.targetAttribute;							
				that.fabricAutocompleteList.removeAll();
				if(data.length > 2 && that.mode == true){
					var sug = that.fabricsTrie.autoComplete(data.toLowerCase());
					for (var ind in sug) {				
//console.log("context.targetAttribute: " + whatGarmentFabric);
//console.log("that.fabricsArray[sug[ind]].for_shirt: " + that.fabricsArray[sug[ind]].for_shirt);
						if(whatGarmentFabric == "shirtFabric"){
							if( that.fabricsArray[sug[ind]].for_shirt == "1" || that.fabricsArray[sug[ind]].for_shirt == undefined){																
							//if( (that.fabricsArray[sug[ind]]).prices.Shirt != undefined){
								that.fabricAutocompleteList.push({
									title: sug[ind]
								});								
							}
						}else{
							if( that.fabricsArray[sug[ind]].for_shirt == "0" || that.fabricsArray[sug[ind]].for_shirt == undefined){
								that.fabricAutocompleteList.push({
									title: sug[ind]
								});								
							}
						}
						
					}
				}
			});
		
		},
		digestData: function(data) {
			this.fabrics(data);
			var that = this;
			var tfab = that.fabrics();
    		for (var ind in tfab ) {
    			if(tfab[ind] != null){
    				that.fabricsTrie.insert(tfab[ind].title.toLowerCase());
    				this.fabricsArray[tfab[ind].title.toLowerCase()] = tfab[ind];
    			}	
    		}


			that.selected_fabric(that.fabrics()[0]);
		},
		setSelectedFabric: function(data) {
			this.selected_fabric(data);
			this.fabricAutocomplete('');
		},

		retrieveContext: function() {
			var context = this.dsRegistry.getContext();
			if (typeof(context) != "undefined") {
				
			}
		},
		selectFabric: function() {
			var context = this.dsRegistry.getContext();
			if (context != "") {
				var tVar = this.dsRegistry.getDatasource(context.targetModel).getStore();
				tVar[context.targetGarment][context.targetDelta][context.targetAttribute] = this.selected_fabric();
				this.dsRegistry.getDatasource(context.targetModel).setStore(tVar);
			}
		},
		cancelFabric: function() {
			var context = this.dsRegistry.getContext();
			var tVar = this.dsRegistry.getDatasource(context.targetModel).getStore();
		
		},
		initIscroll: function() {
			var that = this;
			if ( $('#iscroll-wrapper-fs').length !== 0  ) {
					this.hm_iScroll = new iScroll('iscroll-wrapper-fs',  {
					hScrollbar: false, vScrollbar: false, snap: 'li',
					onScrollEnd: function () {
						
					}
				});
			}
		},
		checkIsOnStock :function(data) {
			$.ajax({
			  type: 'POST',
			  url: BUrl + 'client/check_fabric_stock_endpoint',
			  dataType: 'json',
			  data: {fabric_code:data.title},
			  async: true,
			  success: function(dataS) {

				try{
					//var json = JSON.parse(dataS);
					if(dataS.in_stock == "0"){
						customAlert('Fabric out of stock');
					}
				}catch(e){
					console.log(e);
				}
			  },
			  error: function(XMLHttpRequest, textStatus, errorThrown) {
				  console.log("fitting comments endpoint cannot be reached! ");
				  console.log(XMLHttpRequest +" "+textStatus+" "+errorThrown);
			  }
			});
		}
		
	});
});