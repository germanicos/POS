define(['jquery', 'knockout', 'base'], function($, ko) {
    
    UniformOrderContactPersonVM = SimpleControl.extend({

    	init: function(uniformOrderVM) {

            var self = this;
            console.log("Init UniformOrderContactPersonVM...");

            this.uniformOrderVM = uniformOrderVM;

            // Used in the customer search
    		this.custSelectVM = new CustomerSelectClass(dsRegistry);
			this.custSelectVM.subscribeTo('customersDS_' + authCtrl.userInfo.user_id);
            this.custSelectVM.setParentVM(this);

            // Stored the customers found by the search
            this.selectedCustomer = ko.observable(this.getNewCustomerInstance());

            // Subscribe to changes, thus we can get latest customer measurments from ERP
            this.selectedCustomer.subscribe(function(selectedCustomer) {
				
                // parse birth date
				const date_split = selectedCustomer.customer_DOB.split(new RegExp("[/-]"));

				selectedCustomer.customer_birth_day = String(parseInt(date_split[2])); // remove '0' in '02', '03' for example
				selectedCustomer.customer_birth_month = parseInt(date_split[1]);
				selectedCustomer.customer_birth_year = date_split[0];
				

                const customer_id = selectedCustomer.server_id;

                console.log("getting meas for ", customer_id);


                // If the customer is an existing customer => gets the measurments
                if( parseInt(customer_id) > 0 ) 
                {
                    self.uniformOrderVM.customerDetailsStep.getLatestCustomerMeasurments(customer_id);
                }
                else
                {
                    self.uniformOrderVM.customerDetailsStep.cleanCustomerMeasurments();
                }

            });

            // End customer search
        },

        getSubmissionData : function() {

        	const customer = ko.mapping.toJS(this.selectedCustomer());

        	/**
        	 * The server_id property is set when the customer IS NOT NEW
        	 * therefore we should put the property customer_id
        	 */
        	if( customer.server_id != undefined )
        	{
        		customer.customer_id = customer.server_id;
        		
        		// removing uncesessary fields
        		customer.server_id = null;
        		customer.id = null;
        	}


        	return customer;

        },

        getNewCustomerInstance : function() {

    		console.log("Gettgin new customer....");

    		const customer = {
					  "customer_id"                   : ko.observable(null),
					  "customer_first_name"           : ko.observable(""),
					  "customer_last_name"            : ko.observable(""),
					  "customer_birth_day"            : ko.observable(""),
					  "customer_birth_month"          : ko.observable(""),
					  "customer_birth_year"           : ko.observable(""),
					//   "customer_DOB"                  : ko.observable(""),
					  "customer_occupation"           : ko.observable(""),
					  "customer_company_name"         : ko.observable(""),
					  "customer_gender"               : ko.observable("1"), // default
					  "customer_address1"             : ko.observable(""),
					  "customer_address2"             : ko.observable(""),
					  "customer_country"              : ko.observable(""),
					  "customer_state"                : ko.observable(""),
					  "customer_city"                 : ko.observable(""),
					  "customer_postal_code"          : ko.observable(""),
					  "customer_mobile_phone"         : ko.observable(""),
					  "customer_landline_phone"       : ko.observable(""),
					  "customer_email"                : ko.observable(""),
					  "customer_email_subscription"   : ko.observable("1"),
					  "customer_best_time_to_contact" : ko.observable(null),
					  "customer_referal_method"       : ko.observable(""),
					  "customer_refered_by"           : ko.observable(""),
					  "customer_other_way"            : ko.observable(""),
					  "customer_referal_moreinfo"     : ko.observable(""),
					  "customer_price_range"          : ko.observable(""),
					  "customer_cat_id"               : ko.observable(""),
					  "customer_how_did_hear"         : ko.observable(""),
					  "customer_landline_code"        : ko.observable(""),
					  "receive_marketing"             : ko.observable("1")
					}

			customer.customer_DOB = ko.computed(function(){
				return `${customer.customer_birth_month()}/${customer.customer_birth_day()}/${customer.customer_birth_year()}`;
			})
			
			// makes all properties observable
			return customer;
    	},


    	selectCustomerFromSearch : function(customer) {

    		console.log("selecting...", customer);
    		this.selectedCustomer(customer);
    		this.custSelectVM.customersAutocompleteList([]);
    		$.jGrowl('Customer Selected !');

    		// sets the customer_DOB to the datepicker
    		$(".datepicker").val(customer.customer_DOB).trigger("change");

    	},

    	clearCustomer : function() {

    		this.selectedCustomer(this.getNewCustomerInstance());
    		this.custSelectVM.customerAutocomplete("");
    		$.jGrowl('Customer clear !');
    	}




    });

});