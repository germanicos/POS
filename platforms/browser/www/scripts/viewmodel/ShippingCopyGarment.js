define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingCopyGarment = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		this.ShippingCopyGarmentData     = [];
		this.ShippingCopyGarmentDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);

		this.workflow    = ko.observableArray([
			{id: 0, target: "#shipping_copy_garment",  completed: "false", caption: "CopyGarment", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
			$('.cloneDialog').remove(); 
			that.renderShippingCopyGarment();
		});
		
		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});
		
		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});

		this.completion	= ko.observable(0);
		this.variantNameShippingCopyGarment  = ko.observableArray([{id: 0, title: "ShippingCopyGarment 1"}]);
		this.selectedVariantShippingCopyGarment  = ko.observable(this.variantNameShippingCopyGarment()[0]);	
		this.selectedVariantShippingCopyGarment.subscribe(function(data) {
			that.selectedVariantShippingCopyGarment();
			that.renderShippingCopyGarment();			
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});



		this.ReturnDay = ko.observable('');
		this.ReturnDay.subscribe(function(data) {
			if(data != undefined){
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].ReturnDay = data;
				that.flushModel();
			}		
		});
		
		this.ReturnMonth = ko.observable('');
		this.ReturnMonth.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].ReturnMonth = data;
				that.flushModel();
			}	
		});
				
		this.GarmentComments = ko.observable('');
		this.GarmentComments.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].GarmentComments = data;
				that.flushModel();
			}	
		});
		
		this.VideoUrl = ko.observable('');
		this.VideoUrl.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].VideoUrl = data;
				that.flushModel();
			}	
		});

		this.VideoComments = ko.observable('');
		this.VideoComments.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].VideoComments = data;
				that.flushModel();
			}	
		});

		this.jacket = ko.observable(false);
		this.jacket.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].jacket = data;
				that.flushModel();
				that.updateViewValues();
			}	
		});

		this.pant = ko.observable(false);
		this.pant.subscribe(function(data) {		
			if(data != undefined){
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].pant = data;
				that.flushModel();
				that.updateViewValues();
			}	
		});
		
		this.suit = ko.observable(false);
		this.suit.subscribe(function(data) {	
			if(data != undefined){	
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].suit = data;
				that.flushModel();
				that.updateViewValues();
			}	
		});

		this.shirt = ko.observable(false);
		this.shirt.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].shirt = data;
				that.flushModel();
				that.updateViewValues();
			}	
		});

		this.vest = ko.observable(false);
		this.vest.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingCopyGarmentData[ that.getRow(that.selectedVariantShippingCopyGarment().id)  ].vest = data;
				that.flushModel();
				that.updateViewValues();
			}	
		});
		

		this.updateViewValues = function(){						

			if(that.jacket() == false && that.pant() == false && that.suit() == false && that.vest() == false && that.shirt() == false){
				document.getElementsByName("jacketdiv")[0].style.display = "block";
				document.getElementsByName("pantdiv")[0].style.display = "block";			
				document.getElementsByName("suitdiv")[0].style.display = "block";
				document.getElementsByName("vestdiv")[0].style.display = "block";
				document.getElementsByName("shirtdiv")[0].style.display = "block";
			}else{

				document.getElementsByName("jacketdiv")[0].style.display = "none";
				document.getElementsByName("pantdiv")[0].style.display = "none";			
				document.getElementsByName("suitdiv")[0].style.display = "none";
				document.getElementsByName("vestdiv")[0].style.display = "none";
				document.getElementsByName("shirtdiv")[0].style.display = "none";
	
				if(that.jacket() == true){
					document.getElementsByName("jacketdiv")[0].style.display = "block";
				}else if(that.pant() == true){
					document.getElementsByName("pantdiv")[0].style.display = "block";
				}else if(that.suit() == true){
					document.getElementsByName("suitdiv")[0].style.display = "block";
				}else if(that.vest() == true){
					document.getElementsByName("vestdiv")[0].style.display = "block";
				}else if(that.shirt() == true){
					document.getElementsByName("shirtdiv")[0].style.display = "block";
				}
			}	
		}



///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";

			var photosarray = [];
			var images = localStorage.getItem('ShippingCopyGarmentPhotos');			
			images = (images == null) ? [] : JSON.parse(images);
			if(localStorage.getItem('ShippingCopyGarmentPhotos') != null){
				if(localStorage.getItem('ShippingCopyGarmentPhotos').length > 0){				
					for(var i=0, len=images.length; i<len; i++){
						var obj = new Object();
						var name = images[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + images[i].image_id + "";
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			var tagsarray = [];
			var tags = localStorage.getItem('ShippingCopyGarmentTags');
			tags = (tags == null) ? [] : JSON.parse(tags);
			if(localStorage.getItem('ShippingCopyGarmentTags') != null){
				if(localStorage.getItem('ShippingCopyGarmentTags').length > 0){
					for (var j=0; j<tags.length; j++){
						tagsarray.push(tags[j]);
				 	}
				}  
			}

			var customerString = orderItem.custSelectVM.selectedCustomer();
	
			var dateToPost = '';		
			var date = new Date();
			var yearnow = date.getFullYear();
			var monthnow = date.getMonth() + 1;
			var daynow = date.getDate();
			var dateNow = new Date(monthnow + "/" + daynow + "/" + yearnow);
			var dateToCompare;
			if(this.ReturnMonth() != undefined && this.ReturnMonth() !="" && this.ReturnDay() != undefined && this.ReturnDay() != ""){ 
				if( this.ReturnMonth() < monthnow || (this.ReturnMonth() == monthnow && this.ReturnDay() < daynow) ){
					var yearnew = yearnow + 1;
					dateToPost = this.ReturnDay() + "-" + this.ReturnMonth() + "-" + yearnew;
					dateToCompare = new Date(this.ReturnMonth() + "/" + this.ReturnDay() + "/" + yearnew); 
				}else{
					dateToPost = this.ReturnDay() + "-" + this.ReturnMonth() + "-" + yearnow;
					dateToCompare = new Date(this.ReturnMonth() + "/" + this.ReturnDay() + "/" + yearnow);
				}
			}
			var diffDays = -1;
			try{
				var timeDiff = dateToCompare.getTime() - dateNow.getTime();
				diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				console.log("diffDays: " + diffDays);
			}catch(e){ ; }		
			
			var garmentToPost = "";
			if(this.jacket() == true){
				garmentToPost = "jacket";
			}else if(this.pant() == true){
				garmentToPost = "pant";
			}else if(this.suit() == true){
				garmentToPost = "suit";
			}else if(this.shirt() == true){
				garmentToPost = "shirt";
			}else if(this.vest() == true){
				garmentToPost = "vest";
			}
	
			if(this.GarmentComments().trim() == ""){
				customAlert("Please enter comments concerning the garment's style");
			}else if(diffDays < 0){
				customAlert("There has been an error with the selected date. <br/> Try again or select another.");
			}else if(diffDays > 120){
				customAlert("The selected date is more than 4 months away! <br/> Please select a closer date.");				
			}else if(images.length  == 0){
				customAlert("Please upload at least one image");
			}else if(this.VideoUrl().trim() == ""){
				customAlert("Please enter a video url");
			}else if( this.jacket() == false && this.pant() == false && this.suit() == false && this.shirt() == false && this.vest() == false){
				customAlert("Select a garment");
			}else{
				var salesman = new Object();
		   		salesman.username = authCtrl.username();
		   		salesman.password  = authCtrl.password();
		   		salesman.id  = authCtrl.userInfo.user_id;
				
				data = { 
					salesman: salesman,
					customer: customerString,
					date: dateToPost,
					garment: garmentToPost,
					garment_comments: this.GarmentComments(),
					video: this.VideoUrl(),
					video_comments: this.VideoComments(),
					images: photosarray,
					tags: tagsarray,
					measurements: orderItem.measurementsVM.flushGarment(garmentToPost)
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				$.post(BUrl + 'client_logistics/shipping_copy_garment_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Succesfully added");
		                posChangePage('#shipping_main');
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingCopyGarment"  : this.ShippingCopyGarmentData
		}, true);
		this.renderShippingCopyGarment();
	},

	getVariant: function(id) {
		var toreturn = this.ShippingCopyGarmentData[0];
		for (var ind in this.ShippingCopyGarmentData) {
			if ( this.ShippingCopyGarmentData[ind].variantId == id  ) {
				toreturn = this.ShippingCopyGarmentData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingCopyGarmentData) {
			if ( this.ShippingCopyGarmentData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantShippingCopyGarment: function(name) {
		if(this.variantNameShippingCopyGarment()[0].title != "ShippingCopyGarment 1"){
			this.ShippingCopyGarmentDataAID += 1;
			var newid = this.ShippingCopyGarmentDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingCopyGarment().id ) )  ); //CLONE Object
			this.variantNameShippingCopyGarment.push({title: vname, id: this.ShippingCopyGarmentDataAID});
			tObj.variantId = this.ShippingCopyGarmentDataAID;		
			this.flushModel();
		}else{
			this.variantNameShippingCopyGarment()[0].title = name;
		}
	},	
	
	addVariantsShippingCopyGarments: function() {
		for(var x in orderItem.ShippingCopyGarmentVM.ShippingCopyGarmentData){
		//	console.log("addVariantsShippingCopyGarments " + x);
			if( x != 0){
				this.ShippingCopyGarmentDataAID += 1;
				var vname = "ShippingCopyGarment " + x; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingCopyGarment().id ) )  ); //CLONE Object
				this.variantNameShippingCopyGarment.push({title: vname, id: this.ShippingCopyGarmentDataAID});
				tObj.variantId = this.ShippingCopyGarmentDataAID;		
				this.flushModel();
			}else{
				var vname = "ShippingCopyGarment " + x; 
				//var vname = orderItem.ShippingCopyGarmentVM.ShippingCopyGarmentData[x].pant;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingCopyGarment().id ) )  ); //CLONE Object
				this.variantNameShippingCopyGarment()[x].title = vname;
				tObj.variantId = this.ShippingCopyGarmentDataAID;		
				this.flushModel();
			}
		}
	},

	digestData: function(data) {
		this.ShippingCopyGarmentData  = data.ShippingCopyGarment;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingCopyGarment();
	},

	renderShippingCopyGarment: function() {	
		//Get selected Variant
console.log("renderShippingCopyGarment");		
		try{
			var tData = this.getVariant(this.selectedVariantShippingCopyGarment().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.jacket)			!= "undefined") {this.jacket(tData.jacket);}
				if (typeof(tData.pant)				!= "undefined") {this.pant(tData.pant);}
				if (typeof(tData.suit)				!= "undefined") {this.suit(tData.suit);}
				if (typeof(tData.shirt)    			!= "undefined") {this.shirt(tData.shirt);}
				if (typeof(tData.vest)    			!= "undefined") {this.vest(tData.vest);}
				if (typeof(tData.ReturnDay)    	!= "undefined") {this.ReturnDay(tData.ReturnDay);}
				if (typeof(tData.ReturnMonth)   	!= "undefined") {this.ReturnMonth(tData.ReturnMonth);}
				if (typeof(tData.GarmentComments)    != "undefined") {this.GarmentComments(tData.GarmentComments);}
				if (typeof(tData.VideoUrl)   	!= "undefined") {this.VideoUrl(tData.VideoUrl);}
				if (typeof(tData.VideoComments)    != "undefined") {this.VideoComments(tData.VideoComments);}
			}
		}catch(e){
			;
		}

	}
});

defShippingCopyGarment = SimpleDatasource.extend({
	init: function( name, dsRegistry) {
	//	if(data == null || data == undefined){
			var ShippingCopyGarment = {};
			ShippingCopyGarment.variantId        = 0;
			
			ShippingCopyGarment.jacket = false;
			ShippingCopyGarment.pant = false;
			ShippingCopyGarment.suit = false;
			ShippingCopyGarment.shirt = false;
			ShippingCopyGarment.vest = false;
			ShippingCopyGarment.ReturnDay = '';
			ShippingCopyGarment.ReturnMonth = '';
			ShippingCopyGarment.GarmentComments = '';
			ShippingCopyGarment.VideoUrl = '';
			ShippingCopyGarment.VideoComments = '';
			

			var iShippingCopyGarment = {
				ShippingCopyGarment: []
			};
			iShippingCopyGarment.ShippingCopyGarment.push(ShippingCopyGarment);
			this._super(name, iShippingCopyGarment, dsRegistry);
	//	}	
	}
});


//END DEFINE CLOSURE
});
