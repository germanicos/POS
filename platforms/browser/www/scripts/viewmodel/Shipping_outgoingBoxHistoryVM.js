define(['jquery', 'knockout', 'base'], function ($, ko) {
    Shipping_outgoingBoxHistoryVM = Class.extend({
        init: function () {
            self = this;
            this.boxes = ko.observableArray();
            this.boxesToShow = ko.observableArray();
            this.invicePopUp = ko.observable(null);
            this.historyChat = ko.observable(null);
            this.requestData();
        },

        /**
         * @description Request necessary data from ERP
         */
        requestData: function () {
            console.log("requesting data for outgoing box history...");

            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/get_box_history",
                dataType: 'json',
                data: { "user": authCtrl.userInfo, "type": "outgoing" },
                async: false,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                    for (box of ret) {
                        box.showItemTable = ko.observable(false); // var to show/hide table in view
                        // editable datas
                        box.original_tracking_code = box.tracking_code;
                        box.original_cost = box.cost;
                        self.boxes.push(box);
                    }

                    self.boxesToShow(self.boxes());

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main");


                }

            });
        },

        openHistoryChat: function (boxIndex, itemIndex) {
            self.historyChat({
                item_name: self.boxes()[boxIndex].items[itemIndex].garment_name,
                item_chat: self.boxes()[boxIndex].items[itemIndex].chats,
                client_name : self.boxes()[boxIndex].items[itemIndex].customer_name
            });
        },

        closeHistoryChat: function () {
            self.historyChat(null);
        },

        /**
         * send via ajax POST a new tracking code and/or price
         */
        updateBox : function(boxIndex){
            // console.log("cost", self.boxes()[boxIndex].cost);
            // console.log("tracking_code", self.boxes()[boxIndex].tracking_code);
            
            // only call update if respective data has changed
            var box = self.boxes()[boxIndex];
            if (box.original_cost != box.cost) {
                self.updateBox_cost(boxIndex);
            }
            
            if (box.original_tracking_code != box.tracking_code) {
                self.updateBox_trackingCode(boxIndex);
            }
        },


        updateBox_cost : function(boxIndex){
            console.log("updating cost");
            
            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/update_box_cost",
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "box_id": self.boxes()[boxIndex].id,
                    "cost": self.boxes()[boxIndex].cost
                },
                async: true,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main_new");


                }

            });
        },

        updateBox_trackingCode : function(boxIndex){
            console.log("updating trackingCode");
            
            $.ajax({
                type: 'POST',
                url: BUrl + "shippings_pos/update_tracking_code",
                dataType: 'json',
                data: {
                    "user": authCtrl.userInfo,
                    "box_id": self.boxes()[boxIndex].id,
                    "tracking_code": self.boxes()[boxIndex].tracking_code
                },
                async: true,
                success: function (ret) {

                    console.log(ret);

                    // changeVisibilityDiv('loading_jp');

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    // changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#shipping_main_new");


                }

            });
        },

        applyFilterRange: function() {
            
            var from = document.getElementById('from').value;
            var until = document.getElementById('until').value;
            
            var fromDate = new Date (from);
            var untilDate = new Date (until);

            self.boxesToShow([]);

            for (box of self.boxes()){
                currentDatePreFormated = box.shipping_date.split('-');
                var currentDate = new Date(currentDatePreFormated[2],currentDatePreFormated[1]-1, currentDatePreFormated[0],0,0,0,0);
                
                if ( currentDate >= fromDate && currentDate <= untilDate){
                    self.boxesToShow.push(box);
                }
            }

        },

        clearFilterRange: function() {
          self.boxesToShow(self.boxes());

        }




    });
});