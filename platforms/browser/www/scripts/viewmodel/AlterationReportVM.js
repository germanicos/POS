define(['jquery', 'knockout', 'base'], function($, ko) {
    AlterationReportVM = SimpleControl.extend({

        init: function() {
            var that = this;
            this.cost = ko.observableArray();
            this.measurements = [];
            this.measurementsValue = [];
            this.measurementsDefault = []
            this.measurementsHistory = [];
            this.PanelDrawings = [];
            this.PanelTags = [];
            this.PanelImages = [];
            this.callback = "";
            this.selectedPanel = -1;
            this.customerName = "";
            this.order_id = "";
            this.customer_id = "";
            this.garmentLabel = "";
            this.garment_id = "";
            this.tagEnabled = ko.observable(false);
            this.costType = ko.observable(0);
            this.width = ko.observable(2);

            this.setAlterationInfo = function(data) {
                that.garmentLabel = data.garment_name;
                that.order_id = data.order_id;
                that.customerName = data.customer_full_name;
                that.alteration_id = data.alteration_id;
                that.tailor_id = data.tailor_id;
                that.customer_id = data.customer_id;
                that.garment_id = data.garment_id;
                for (var x in data.panel_images) {
                    that.PanelImages.push(BUrl + data.panel_images[x].path);
                    that.PanelTags.push(ko.observableArray());
                    that.PanelDrawings.push("");
                }
                that.measurementsHistory = data.measurements_history;
                that.measurements = data.panel_measurements;
                console.log(data.panel_measurements);
                console.log(data.current_measurement);
                for (var x in that.measurements) {
                    that.measurementsValue.push(ko.observable(data.current_measurement[that.measurements[x].id]));
                    that.measurementsDefault.push(data.current_measurement[that.measurements[x].id])
                }
                that.cost.removeAll();
                that.addCost();
                that.costType(0);
            }

        },

        undoDraw: function(index) {
            this.sketchpads[index].undo();
        },

        redoDraw: function(index) {
            this.sketchpads[index].redo();
        },

        setWidth: function(index, width) {
            this.sketchpads[index].pen().width(width * 3);
            this.width(width)
        },


        initializeSketchpads: function() {
            CURRENT_ZOOM = 1.0;
            this.sketchpads = [];
            for (var y in this.PanelImages) {

                this.sketchpads.push(
                    new Raphael.sketchpad('PanelDrawing' + y, {
                        width: 300,
                        height: 462,
                        editing: true
                    })
                );
            }
        },

        addTag: function(index, event) {
            target = event.target;
            var image_left = $(target).offset().left;
            var click_left = event.pageX;
            var left_distance = click_left - image_left;
            var image_top = $(target).offset().top;
            var click_top = event.pageY;
            var top_distance = click_top - image_top;
            var imagemap_width = $(target).width();
            var imagemap_height = $(target).height();
            var relative_left = (left_distance / imagemap_width) * 100;
            var relative_top = (top_distance / imagemap_height) * 100;
            alert('here');
            this.PanelTags[index].push({ left: relative_left, top: relative_top, text: ko.observable(), leftp: (relative_left + '%'), topp: (relative_top + '%') });
        },

        addCost: function() {
            this.cost.push({ description: ko.observable(), cost: ko.observable(), hour: ko.observable(), image: ko.observable() });
        },

        removeCost: function(index) {
            this.cost.remove(index);
        },

        finishAddingInvoiceImage: function(image) {
            console.log(image, "imagem invoice")
            this.cost()[this.invoiceIndex].image(BUrl + image);
        },

        loadInfo: function(ID) {
            var data = {
                alteration_id: ID
            };


            // $.post(BUrl+"client_alterations_new/get_alteration_endpoint", {
            //                                                         timestamp: Math.round(new Date().getTime() / 1000),
            //                                                         salesman: salesman,
            //                                                         data: dataToPost,
            //                                                     }, function(ret) {

            //     console.log("RETORNO", ret);

            // });


            //DataRequest(true, 'client_alterations_new/get_alteration_endpoint', data, this.setAlterationInfo, function() { customAlert('Could not load the list. Please try again later or contact the admins!'); });
        },

        loadPannelDrawing: function(panel) {
            this.loadPannelDrawing(panel);
        },

        savePanelDrawing: function(index) {
            this.PanelDrawings[index] = this.sketchpads[index].json();
        },

        loadPage: function(callback) {
            posChangePage('#alterationReport');
            this.callback = callback;
        },
        addInvoiceImage: function(index) {
            this.invoiceIndex = index;
            $('#AlterationReportCostImageFiles').click();
        },

        submit: function() {
            var measurements = { alteration_id: this.alteration_id };

            for (var x in this.measurements) {
                measurements[this.measurements[x].id] = this.measurementsValue[x]();
            }

            var drawings = [];
            for (var x in this.PanelDrawings) {
                if ((this.PanelDrawings[x] && JSON.parse(this.PanelDrawings[x]).length > 0) || this.PanelTags[x]().length > 0) {
                    var drawing = {}
                    drawing.drawing = this.PanelDrawings[x];
                    drawing.tags = this.PanelTags[x]();
                    drawing.panel_image = this.PanelImages[x];
                    drawings.push(drawing);
                }
            }
            costs = [];
            for (var x in this.cost()) {
                costs.push({ desc: this.cost()[x].description, cost: this.cost()[x].cost, hour: this.cost()[x].hour, invoice: this.cost()[x].image.replace(BUrl, "") })
            }

            var data = {
                mesurements: measurements,
                drawings: drawings,
                alteration_id: this.alteration_id,
                garment_type: this.garment_id,
                customer_id: this.customer_id,
                order_id: this.order_id,
                tailor_id: this.tailor_id,
                cost: cost
            };

            DataRequest(true, 'client_alterations_new/save_alteration_report', data, function() { customAlert('Salvo com sucesso'); }, function() { customAlert('Could not load the list. Please try again later or contact the admins!'); });
        },

        backToLastStep: function() {
            if (this.callback)
                this.callback.loadPage();
        },


        selectImage: function(image) {
            this.selectedImage(image);
        },
    });
});