
define(['jquery', 'knockout', 'base'], function($, ko) {


    BodyShapesStepVM = SimpleControl.extend({

        init: function (bodyshapes, latestCustomerBodyshapes, garmentStep) {
            self = this;

            // Clone the object
            const cloneBodyshapes = JSON.parse(JSON.stringify(bodyshapes));
            this.latestCustomerBodyshapes = latestCustomerBodyshapes;
            this.bodyShapes = ko.observableArray(this.buildBodyshapes(cloneBodyshapes));
            this.garmentStep = garmentStep;
            
            let bodyNotes = "";

            try 
            {
                bodyNotes = latestCustomerBodyshapes.bodyshape_notes == null ? bodyNotes : latestCustomerBodyshapes.bodyshape_notes;
            }
            catch(e)
            {
                console.log("bodyshape commets not found");
                console.log("exception", e);
            }


            this.bodyshapeNotes = ko.observable(bodyNotes);

            this.bodyShapesToShow = ko.observableArray();
        },

        getSubmissionData : function() {

            const bodyshapes = [];

            for( let bodyshapeCategory of this.bodyShapes() ) // checks if there is at least one garment per customer
            {   
                if (bodyshapeCategory.selectedValue())
                {
                    selectedShapeId = bodyshapeCategory.selectedValue().id;
                }

                const shapeObj = { 
                        key : bodyshapeCategory.cat_key,
                        selectedVal : selectedShapeId
                    };

                bodyshapes.push(shapeObj)
            }

            const notesObj = {
                key : "bodyshape_notes",
                selectedVal : this.bodyshapeNotes() ? this.bodyshapeNotes() : ""
            };

            bodyshapes.push(notesObj);

            return bodyshapes;
        },

        buildBodyshapes : function(bodyShapes) {

            for( let shape of bodyShapes)
            {
                let selectedVal = false;

                // If there is bodyshape already for this customer
                if( this.latestCustomerBodyshapes[shape.cat_key] != 'undefined' )
                {
                    selectedVal = this.latestCustomerBodyshapes[shape.cat_key];
                }

                // If there is no selected val
                if( !selectedVal || selectedVal == 0)
                {
                    // Get default value initually
                    selectedVal = this.getDefaultValueForShape(shape.cat_key);
                }
                
                console.log("BODYSHAPE", selectedVal);

                const selectedBodyValObj = shape.values[0].filter( e => e.id == selectedVal)[0] || false;

                shape.selectedValue = ko.observable(selectedBodyValObj);
            }

            return bodyShapes;
        },  

        /** Change the selected bodyShape */
        selectBodyShape: function (bodyshapeCategory, selectedValue) {
            console.log('bodyshapeCategory', bodyshapeCategory);
            console.log('selectedValue', selectedValue);
            
            bodyshapeCategory.selectedValue(selectedValue);
            this.toggleStepComplete();
        },

        /**
         * Returns the 'Normal' value for each bodyshape
         * @param  {[type]} shapeKey [description]
         * @return {[type]}          [description]
         */
        getDefaultValueForShape : function(shapeKey) {

            let defaultValue = 0;

            switch(shapeKey)
            {
                case ("bodyshapeShoulders") : defaultValue = 3;break; // Normal
                case ("bodyshapeArms") : defaultValue = 2;break; // balanced
                case ("bodyshapeBack") : defaultValue = 3;break; // Normal
                case ("bodyshapeFront") : defaultValue = 2;break; // Normal
                case ("bodyshapeLeg") : defaultValue = 3;break; // Normal
                case ("bodyshapeNeckHeight") : defaultValue = 2;break; // Normal
                case ("bodyshapeStanding") : defaultValue = 2;break; // Neutral
                case ("bodyshapeStomach") : defaultValue = 3;break; // Normal
                case ("bodyshapeThigh") : defaultValue = 6;break; // Normal
                case ("bodyshape_notes") : defaultValue = "";break; // Normal
            }

            return defaultValue;
        },

        /**
         * if all body shapes of all categories are checked, set step as 'completed
         */
        toggleStepComplete: function () {
            for (let customer of orders.customers()) {
                if (!customer.is_present) { continue };
                var bodyshapes = customer.bodyShapesStep.bodyShapes()
                for (let shape of bodyshapes) {
                    if (shape.selectedValue() == false) {
                        orders.steps.filter(function (el) { return el.id == '3' })[0].complete = false;
                        return; // not  completed yet
                    }
                }
            }

            orders.steps.filter(function (el) { return el.id == '3' })[0].complete = true;
        },

        /**
         * return a list of garments that have at least one selected garment.
         * 
         * For example, in {pants: two pants, shirt: 0 shirt, jacket: 1 jacket} ==return==> [pants, jacket]
         * 
         * For tests (call by console): orders.selectedCustomer().bodyShapesStep.getActiveGarments()
         */
        getActiveGarments: function () {
            var justIdAndName = [];
            var activeGarments = this.garmentStep.garments().filter(function (el) {
                return el['garments']().length > 0
            });

            activeGarments.map(e => {
                justIdAndName.push({name: e.name, id:e.id});
            });

            return justIdAndName;
        },

        /**
         * Show bodyshape category based on selected garments
         * 
         * @returns bodyshapes to show
         * 
         * example: hide Shoulders when selected garment is pants
         *  For tests (call by console): orders.selectedCustomer().bodyShapesStep.showBodyShape()
         */
        showBodyShape: function () {
            var activeGarments = this.getActiveGarments();

            // if suit is a active garment
            if (activeGarments.filter(function (el) { return el.id == '6' }).length > 0) {
                // show all
                this.bodyShapesToShow(this.bodyShapes());
                return;
            }

            // clear array
            this.bodyShapesToShow([]);

            // if shirt, jacket or vest is active
            if (activeGarments.filter(function (el) { return ['2', '3', '4'].includes(el.id) }).length > 0) {
                // return top bodyshapes
                for (const shape of this.bodyShapes()) {
                    if (["bodyshapeShoulders", "bodyshapeArms", "bodyshapeNeckHeight", "bodyshapeStanding", "bodyshapeStomach", 'bodyshapeBack', 'bodyshapeFront'].includes(shape.cat_key)) {
                        this.pushObservableArray(this.bodyShapesToShow, shape);
                    }
                }

            }

            // if pants is active
            if (activeGarments.filter(function (el) { return el.id == '1' }).length > 0) {
                // return bottom bodyShapes
                for (const shape of this.bodyShapes()) {
                    if (['bodyshapeLeg', 'bodyshapeThigh', 'bodyshapeStanding'].includes(shape.cat_key)) {
                        this.pushObservableArray(this.bodyShapesToShow, shape);
                    }
                }
            }



        },

        /**
         * append without duplicate element
         */
        pushObservableArray:function(array, el){
            // check if not in array first
            for (const iterator of array()) {
                if (iterator.cat_key == el.cat_key){
                    return;
                }
            }

            // if not in array, append
            array.push(el);
        }

    });
});
