define(['jquery', 'knockout', 'base'], function ($, ko) {

    UniformGarmentsStepVM = SimpleControl.extend({

        init: function (uniformTemplateVM) {
            console.log("Init UniformGarmentsStepVM")
            var self = this;
            this.uniformTemplateVM = uniformTemplateVM;

            this.garments = ko.observableArray(this.buildGarments());

            // for UI purposes. if this counter of garments is > 0, consider step as complet
        },






        //  There will be no SUIT in the uniform processes.
        //      Cause: Sometimes the customers will only need on part of the uniform, therefore
        //              its best if we keep the suit uniform separetly.
        //              
        buildGarments: function () {

            return [
                {
                    'name': 'jacket',
                    'garments': ko.observableArray([]),
                    'id': '4',
                    'image': 'img/order_process/wetransfer-b60a96/jacket.png'
                },
                {
                    'name': 'pants',
                    'garments': ko.observableArray([]),
                    'id': '1',
                    'image': 'img/order_process/wetransfer-b60a96/pant.png'
                },
                {
                    'name': 'vest',
                    'garments': ko.observableArray([]),
                    'id': '3',
                    'image': 'img/order_process/wetransfer-b60a96/vest.png'
                },
                {
                    'name': 'shirt',
                    'garments': ko.observableArray([]),
                    'id': '2',
                    'image': 'img/order_process/wetransfer-b60a96/shirt.png'
                }
            ]

        },

        // Increases the count of garments
        incrementGarment(garmentId) {

            console.log("Incrementing " + garmentId + " for... ");

            let createdGarment = false;

            for (let garment of this.garments()) 
            {
                if (garment.id == garmentId) {
                    // gets the object from the design VM
                    createdGarment = this.uniformTemplateVM.garmentsDesignStep.getGarmentObject(garmentId)
                    garment.garments.push(createdGarment);

                    break;
                }
            }

            // If salesman adds more garments => we need to resend the base64 images to the server
            // thus, we mark the hasSentBase64Images as false
            this.uniformTemplateVM.garmentsPortfolioStep.hasSentBase64Images(false);


            return createdGarment;
        },

        // Decreasese the number of garments
        decrementGarment(garmentId) {

            for (let garment of this.garments()) {
                if (garment.id == garmentId) {
                    if (garment.garments().length > 0) {
                        garment.garments.pop(); // removes last element
                    }
                    else {
                        console.log("Cannot go below 0");
                    }

                    break;
                }
            }
        },



    });
});