define(['jquery', 'knockout', 'base'], function($, ko) {
AlterationReport = SimpleControl.extend({
	init: function(DsRegistry) {
		this._super( DsRegistry );
		var that = this;
		
		
		backAlterations = function (){
			if(confirm("All data will be lost! Do you want to go back to alteration?")){
				posChangePage('#alterationsList');
			}
		};
		
		this.AlterationReportData     = [];
		this.AlterationReportDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);
	

		this.workflow    = ko.observableArray([
			{id: 0, target: "#alterationReport",  completed: "false", caption: "AlterationReport", title: "Alteration report", myclass: "report" },
		]);

		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
			$('.cloneDialog').remove();
			that.renderAlterationReport(); 
		});

		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});


		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});


		this.completion       = ko.observable(0);
		this.variantNameAlterationReport  = ko.observableArray([{id: 0, title: "AlterationReport 1"}]);
		this.selectedVariantAlterationReport  = ko.observable(this.variantNameAlterationReport()[0]);	
		this.selectedVariantAlterationReport.subscribe(function(data) {
			that.selectedVariantAlterationReport();
		//	posChangePage("#alterationReports");
		//	$('.cloneDialog').remove();
			that.renderAlterationReport();
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});
		

		this.AlterationReportCustomer = ko.observable('');
		this.AlterationReportCustomer.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportCustomer = data;
			that.flushModel();
		});

		this.AlterationReportGarment = ko.observable('');
		this.AlterationReportGarment.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportGarment = data;
			that.flushModel();
		});
		
		this.AlterationReportGarmentFabric = ko.observable('');
		this.AlterationReportGarmentFabric.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportGarmentFabric = data;
			that.flushModel();
		});

		this.AlterationId = ko.observable('');
		this.AlterationId.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationId = data;
			that.flushModel();
		});

		this.AlterationReportId = ko.observable('');
		this.AlterationReportId.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportId = data;
			that.flushModel();
		});
		
		this.AlterationReportOrderId = ko.observable('');
		this.AlterationReportOrderId.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportOrderId = data;
			that.flushModel();
		});
		
		this.AlterationReportOrderProductId = ko.observable('');
		this.AlterationReportOrderProductId.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportOrderProductId = data;
			that.flushModel();
		});
		
		//this.AlterationReportTailors = ko.observable(that.dsRegistry.getDatasource('tailorsDS').getStore());
		this.AlterationReportTailor = ko.observable('');
		this.AlterationReportTailor.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportTailor = data;
			that.flushModel();
		});
		this.AlterationReportDate = ko.observable('');
		this.AlterationReportDate.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportDate = data;
			that.flushModel();
		});
		this.AlterationReportNotes = ko.observable('');
		this.AlterationReportNotes.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportNotes = data;
			that.flushModel();
		});
		this.AlterationReportSalesmanComments = ko.observable('');
		this.AlterationReportSalesmanComments.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].AlterationReportSalesmanComments = data;
			that.flushModel();
		});
		
		this.PanelMeasurements = ko.observable('');
		this.PanelMeasurements.subscribe(function(data) {		
			that.AlterationReportData[ that.getRow(that.selectedVariantAlterationReport().id)  ].PanelMeasurements = data;
			that.flushModel();
		});
		
		this.AlterationCosts = ko.observableArray();
		
		this.AlterationCostsViewModel = function(){
    		var self = this;
    		this.description = ko.observable('');
           	this.hours = ko.observable();
           	this.cost = ko.observable();
		};
        this.addAlterationCost = function(){
        	var checkfailed = false;
        	for(var pos = 0; pos < this.AlterationCosts().length; pos++){
console.log(this.AlterationCosts()[pos].description().length  + " " + (this.AlterationCosts()[pos].hours()*1.0) + " " + (this.AlterationCosts()[pos].cost()*1.0));
        		if(	this.AlterationCosts()[pos].description().length == 0 || 
					(this.AlterationCosts()[pos].hours()*1.0) <= 0 || (this.AlterationCosts()[pos].hours()*1.0) == NaN || this.AlterationCosts()[pos].hours() == undefined || this.AlterationCosts()[pos].hours() == "" ||
					(this.AlterationCosts()[pos].cost()*1.0) <= 0 || (this.AlterationCosts()[pos].cost()*1.0) == NaN || this.AlterationCosts()[pos].cost() == undefined || this.AlterationCosts()[pos].cost() == ""){
        			
       				checkfailed = true;
       				break;
       			}	
       		}
       		if(checkfailed == false){
       			this.AlterationCosts.push(new this.AlterationCostsViewModel());
       		}	
    	};
		this.removeAlterationCost = function(pos){
       		this.AlterationCosts.remove(that.AlterationCosts()[pos]);
    	};    				
		this.AlterationCostsTotal = function(){
        	var total = 0;
    		for(var pos = 0; pos < this.AlterationCosts().length; pos++){        		
        		if(this.AlterationCosts()[pos].description().length > 0 && (this.AlterationCosts()[pos].hours()*1.0) > 0 && (this.AlterationCosts()[pos].cost()*1.0) > 0){
       				total += this.AlterationCosts()[pos].cost()*1.0;
       			}	
       		}
       		return total;
		};
		this.calculateVat = function(){
			gstAmount = that.AlterationCostsTotal() * orderItem.getGST();
			return gstAmount.round(2);
		}	
		this.calculateTotalCost = function(){
			if(this.AlterationCosts().length == 0){
				this.AlterationCosts.push(new this.AlterationCostsViewModel());
			}
			var total = that.calculateVat() + that.AlterationCostsTotal();				
			return total;
		}

		this.missingPanelMeasurements = function(){
			found = false;	
			if(this.AlterationReportGarment() == 'Suit/J' || this.AlterationReportGarment() == 'Jacket' || this.AlterationReportGarment() == 'jacket'){
				if(	this.PanelMeasurements().jacket_panel_front_chest == "" || this.PanelMeasurements().jacket_panel_front_true_waist == "" || this.PanelMeasurements().jacket_panel_front_hip == "" || this.PanelMeasurements().jacket_panel_side_chest == "" || 
					this.PanelMeasurements().jacket_panel_side_true_waist == "" || this.PanelMeasurements().jacket_panel_side_hip == "" || this.PanelMeasurements().jacket_panel_side_bottom_edge == "" || this.PanelMeasurements().jacket_panel_back_x_back == "" || 
					this.PanelMeasurements().jacket_panel_back_chest == "" || this.PanelMeasurements().jacket_panel_back_true_waist == "" || this.PanelMeasurements().jacket_panel_back_hip == "" || this.PanelMeasurements().jacket_panel_back_bottom_edge == "" || 
					this.PanelMeasurements().jacket_panel_across_shoulder == "" || this.PanelMeasurements().jacket_panel_across_back == "" || this.PanelMeasurements().jacket_panel_front_length == "" || this.PanelMeasurements().jacket_panel_front_side_seam_length == "" || 
					this.PanelMeasurements().jacket_panel_center_back_length == "" || this.PanelMeasurements().jacket_panel_shoulder_seam == "" || this.PanelMeasurements().jacket_panel_bicep == "" || this.PanelMeasurements().jacket_panel_elbow == "" || 
					this.PanelMeasurements().jacket_panel_sleeve_cuff_buttoned == "" || this.PanelMeasurements().jacket_panel_sleeve_length_right == "" || this.PanelMeasurements().jacket_panel_sleeve_length_left == "" ){
						
					found = true;
				}
			}else if( this.AlterationReportGarment() == 'Suit/P' || this.AlterationReportGarment() == 'Pants' || this.AlterationReportGarment() == 'pants' || this.AlterationReportGarment() == 'Pant' || this.AlterationReportGarment() == 'pant' ){
				if( this.PanelMeasurements().pants_panel_total_waist == "" || this.PanelMeasurements().pants_panel_front_hip == "" || this.PanelMeasurements().pants_panel_back_hip == "" || this.PanelMeasurements().pants_panel_fly == "" || 
					this.PanelMeasurements().pants_panel_front_crotch == "" || this.PanelMeasurements().pants_panel_back_crotch == "" || this.PanelMeasurements().pants_panel_front_thigh == "" || this.PanelMeasurements().pants_panel_back_thigh == "" || 
					this.PanelMeasurements().pants_panel_front_knee == "" || this.PanelMeasurements().pants_panel_back_knee == "" || this.PanelMeasurements().pants_panel_cuffs == "" || this.PanelMeasurements().pants_panel_inseam == "" || 
					this.PanelMeasurements().pants_panel_outseam == "" ){
						
					found = true;
				}				
			}else if(this.AlterationReportGarment() == 'Vest' || this.AlterationReportGarment() == 'vest'){
				if(	this.PanelMeasurements().vest_panel_front_chest == "" || this.PanelMeasurements().vest_panel_front_waist == "" || this.PanelMeasurements().vest_panel_front_hip == "" || this.PanelMeasurements().vest_panel_back_chest == "" || 
					this.PanelMeasurements().vest_panel_back_waist == "" || this.PanelMeasurements().vest_panel_back_hip == "" || this.PanelMeasurements().vest_panel_full_shoulder == "" || this.PanelMeasurements().vest_panel_half_shoulder == "" || 
					this.PanelMeasurements().vest_panel_front_armhole == "" || this.PanelMeasurements().vest_panel_back_armhole == "" || this.PanelMeasurements().vest_panel_first_button == "" || this.PanelMeasurements().vest_panel_side == "" || 
					this.PanelMeasurements().vest_panel_back_length == "" || this.PanelMeasurements().vest_panel_front_length == "" ){
						
					found = true;
				}					
			}else if(this.AlterationReportGarment() == 'Shirt' || this.AlterationReportGarment() == 'shirt'){
				if(	this.PanelMeasurements().shirt_panel_front_length == "" || this.PanelMeasurements().shirt_panel_back_length == "" || this.PanelMeasurements().shirt_panel_front == "" || this.PanelMeasurements().shirt_panel_back == "" || 
					this.PanelMeasurements().shirt_panel_full_shoulder == "" || this.PanelMeasurements().shirt_panel_half_shoulder == "" || this.PanelMeasurements().shirt_panel_front_chest == "" || this.PanelMeasurements().shirt_panel_back_chest == "" || 
					this.PanelMeasurements().shirt_panel_front_waist == "" || this.PanelMeasurements().shirt_panel_back_waist == "" || this.PanelMeasurements().shirt_panel_front_hip == "" || this.PanelMeasurements().shirt_panel_back_hip == "" || 
					this.PanelMeasurements().shirt_panel_neck == "" || this.PanelMeasurements().shirt_panel_bicep == "" || this.PanelMeasurements().shirt_panel_elbow == "" || this.PanelMeasurements().shirt_panel_cuff == "" || 
					this.PanelMeasurements().shirt_panel_sleeve_length == ""  ){
						
					found = true;
				}
			}
			return found;
		}

///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";
		
			var whichGarment = that.selectedVariantAlterationReport().id;

console.log("getting " + 'AlterationReportImage' + whichGarment);

			var photosarray = [];
			var reportImages = localStorage.getItem('AlterationReportImage' + whichGarment);			
			reportImages = (reportImages == null) ? [] : JSON.parse(reportImages);
			if(localStorage.getItem('AlterationReportImage' + whichGarment) != null){
				if(localStorage.getItem('AlterationReportImage' + whichGarment).length > 0){				
					for(var i=0, len=reportImages.length; i<len; i++){
						var obj = new Object();
						var name = reportImages[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + reportImages[i].image_id + "";
		   				obj.image_type = "1"; 
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}
			var costImages = localStorage.getItem('AlterationReportCostImage' + whichGarment); 
			costImages = (costImages == null) ? [] : JSON.parse(costImages);
			if(localStorage.getItem('AlterationReportCostImage' + whichGarment) != null){
				if(localStorage.getItem('AlterationReportCostImage' + whichGarment).length > 0){
					for(var i=0, len=costImages.length; i<len; i++){
						var obj = new Object();
		   				var name = costImages[i].picture;
		   				obj.name = name;
		   				obj.id  = "" + costImages[i].image_id + "";
		   				obj.image_type = "2"; 
		   				var jsonString= JSON.stringify(obj);
						photosarray.push(obj);
				 	}
				}  
			}

			var alterationIdPost = this.AlterationId();
			try{
				var date = this.AlterationReportDate();
				var day = date.split("/")[0];
				var month = date.split("/")[1];
				var year = date.split("/")[2];
				var dateToPost = day + "-" + month + "-" + year;
			}catch(e){
				var dateToPost = "";
			}
				
			var tailorIdPost = "";			
			if(this.AlterationReportTailor() != undefined){				
				if(this.AlterationReportTailor().user_id != undefined){
					tailorIdPost = this.AlterationReportTailor().user_id;
				}
			}
		
			try{
				var alterationcostspost = that.AlterationCosts;
				var alterationcostserror = false;
				for(var x = 0; x < alterationcostspost().length; x++){		
	//console.log(alterationcostspost()[x].description().length  + " " + (alterationcostspost()[x].hours()*1.0) + " " + (alterationcostspost()[x].cost()*1.0));		
					if(	alterationcostspost()[x].description().length == 0 || 
						(alterationcostspost()[x].hours()*1.0) <= 0 || (alterationcostspost()[x].hours()*1.0) == NaN || alterationcostspost()[x].hours() == undefined || alterationcostspost()[x].hours() == "" ||
						(alterationcostspost()[x].cost()*1.0) <= 0 || (alterationcostspost()[x].cost()*1.0) == NaN || alterationcostspost()[x].cost() == undefined || alterationcostspost()[x].cost() == ""){
						alterationcostserror = true;
						break;
					}
				}
			}
			catch(e){
				var alterationcostserror = true;
			}
			
						
			if(dateToPost == ""){
				customAlert("Please enter a date!");
			}else if(alterationcostserror == true){
				customAlert("Please fill in all fields in the alteration costs");
			}else if(reportImages.length  == 0){
				customAlert("Please upload an alteration report image");
			}else if(costImages.length  == 0){
				customAlert("Please upload an alteration cost image");
			}else if(alterationcostspost().length == 0){
				customAlert("Please fill in the alteration description");
			}else if(this.missingPanelMeasurements() == true){
				customAlert("Please fill in all panel measurements");
			
			}else{
				data = { 
					user_id: authCtrl.userInfo.user_id,
					customer: this.AlterationReportCustomer(),
					alteration_id: alterationIdPost,
					order_id: this.AlterationReportOrderId(),//this.selected_order.order_id,
					orders_products_id: this.AlterationReportOrderProductId(),
					garment: this.AlterationReportGarment(),
					panels: ko.toJSON(this.PanelMeasurements()),
					notes: this.AlterationReportNotes(),
					salesman_comments: this.AlterationReportSalesmanComments(),
					gst: orderItem.getGST()*100, 
					alteration_report_date: dateToPost,
					alterationcosts: ko.toJSON(alterationcostspost()),
					alteration_report_images: photosarray
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				$.post(BUrl + 'client_alterations/alteration_report_endpoint', {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Alteration Report succesfully added");
		            			            	
		            	that.AlterationReportId(dataS.alterationReport);

		                var cs = that.currentStep().id;	
		                that.workflow()[cs].completed = "true";

		                if(that.variantNameAlterationReport().length == 1){
		                	posChangePage('#alterationsList');
		                }
					}else{
						console.log('fail');
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"AlterationReport"  : this.AlterationReportData
		}, true);
	},

	getVariant: function(id) {
		var toreturn = this.AlterationReportData[0];
		for (var ind in this.AlterationReportData) {
			if ( this.AlterationReportData[ind].variantId == id  ) {
				toreturn = this.AlterationReportData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.AlterationReportData) {
			if ( this.AlterationReportData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantAlterationReport: function(name) {
		if(this.variantNameAlterationReport()[0].title != "AlterationReport 1"){
			this.AlterationReportDataAID += 1;
			var newid = this.AlterationReportDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantAlterationReport().id ) )  ); //CLONE Object
			this.variantNameAlterationReport.push({title: vname, id: this.AlterationReportDataAID});
			tObj.variantId = this.AlterationReportDataAID;		
			this.flushModel();
		}else{
			this.variantNameAlterationReport()[0].title = name;
		}
	},	
	
	addVariantsAlterationReports: function() {
		for(var x in orderItem.alterationReportsVM.AlterationReportData){
		//	console.log("addVariantsAlterationReports " + x);
			if( x != 0){
				this.AlterationReportDataAID += 1;
				var vname = orderItem.alterationReportsVM.AlterationReportData[x].AlterationReportGarment; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantAlterationReport().id ) )  ); //CLONE Object
				this.variantNameAlterationReport.push({title: vname, id: this.AlterationReportDataAID});
				tObj.variantId = this.AlterationReportDataAID;		
				this.flushModel();
			}else{
				var vname = orderItem.alterationReportsVM.AlterationReportData[x].AlterationReportGarment;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantAlterationReport().id ) )  ); //CLONE Object
				this.variantNameAlterationReport()[x].title = vname;
				tObj.variantId = this.AlterationReportDataAID;		
				this.flushModel();
			}
			
		}
	},
	
	

	digestData: function(data) {
		this.AlterationReportData  = data.AlterationReport;
		this.renderView();

	},

	renderView: function() {
		this.renderAlterationReport();
	},

	renderAlterationReport: function() {	
		
		var whichGarment = this.selectedVariantAlterationReport().id;
		try{
			photo = JSON.parse( localStorage.getItem('AlterationReportImage' + whichGarment) );			
			document.getElementById("AlterationReportImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'"  width="120"/>'].join('');
		}catch(e){			
			try{
				document.getElementById("AlterationReportImageMedia").innerHTML = [''].join('');
			}catch(e2){ ; }
		}
		try{
			photo = JSON.parse( localStorage.getItem('AlterationReportCostImage' + whichGarment) );
			document.getElementById("AlterationReportCostImageMedia").innerHTML = ['<img src="', photo[0].thumbnail ,'"  width="120"/>'].join('');
		}catch(e){
			try{
				document.getElementById("AlterationReportCostImageMedia").innerHTML = [''].join('');
			}catch(e2){ ; }
		}
		
		//Get selected Variant
		try{
			var tData = this.getVariant(this.selectedVariantAlterationReport().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.AlterationReportCustomer)		!= "undefined") {this.AlterationReportCustomer(tData.AlterationReportCustomer);}
				if (typeof(tData.AlterationReportGarment)		!= "undefined") {this.AlterationReportGarment(tData.AlterationReportGarment);}
				if (typeof(tData.AlterationReportGarmentFabric)	!= "undefined") {this.AlterationReportGarmentFabric(tData.AlterationReportGarmentFabric);}
				if (typeof(tData.AlterationId)    			!= "undefined") {this.AlterationId(tData.AlterationId);}
				if (typeof(tData.AlterationReportId)    			!= "undefined") {this.AlterationReportId(tData.AlterationReportId);}
				if (typeof(tData.AlterationReportOrderId)    	!= "undefined") {this.AlterationReportOrderId(tData.AlterationReportOrderId);}
				if (typeof(tData.AlterationReportOrderProductId)    	!= "undefined") {this.AlterationReportOrderProductId(tData.AlterationReportOrderProductId);}
				if (typeof(tData.AlterationReportTailor)    		!= "undefined") {this.AlterationReportTailor(tData.AlterationReportTailor);}
				if (typeof(tData.AlterationReportDate)    != "undefined") {this.AlterationReportDate(tData.AlterationReportDate);}
				if (typeof(tData.AlterationReportNotes)    != "undefined") {this.AlterationReportNotes(tData.AlterationReportNotes);}
				if (typeof(tData.AlterationReportSalesmanComments)    != "undefined") {this.AlterationReportSalesmanComments(tData.AlterationReportSalesmanComments);}
				if (typeof(tData.PanelMeasurements)   != "undefined") {this.PanelMeasurements(tData.PanelMeasurements);}
				
			}
		}catch(e){
			;
		}

	}
});

defAlterationReport = SimpleDatasource.extend({
	init: function( name, dsRegistry, data ) {
		if(data == null || data == undefined){
		//	orderItem.alterationReportsVM.addVariantAlterationReport('new');
			
console.log("data is null");			
			var AlterationReport = {};
			AlterationReport.variantId        = 0;
			
			AlterationReport.AlterationReportCustomer = '';
			AlterationReport.AlterationReportGarment = '';
			AlterationReport.AlterationReportGarmentFabric = '';
			AlterationReport.AlterationId = '';
			AlterationReport.AlterationReportId = '';
			AlterationReport.AlterationReportOrderId = '';
			AlterationReport.AlterationReportOrderProductId = '';
			AlterationReport.AlterationReportTailor = '';
			AlterationReport.AlterationReportDate = '';
			AlterationReport.AlterationReportNotes = '';
			AlterationReport.AlterationReportSalesmanComments = '';
			AlterationReport.PanelMeasurements = '';

			var iAlterationReport = {
				AlterationReport: []
			};
			iAlterationReport.AlterationReport.push(AlterationReport);
			this._super(name, iAlterationReport, dsRegistry);
			
		}else{
//console.log("data is not null");
			var iAlterationReport = {
				AlterationReport: []
			};
			
			var alterationOrderIndex = localStorage.getItem("alterationOrderIndex");	
			var alterationProductIndex = localStorage.getItem("alterationProductIndex");		
			
			var x = 0;
						var AlterationReport = {};
						AlterationReport.variantId = x;
						
						AlterationReport.AlterationReportCustomer = data[alterationOrderIndex].customer;
						AlterationReport.AlterationReportGarment = data[alterationOrderIndex].products[alterationProductIndex].garment;
						AlterationReport.AlterationReportGarmentFabric = data[alterationOrderIndex].products[alterationProductIndex].fabric;
						AlterationReport.AlterationReportOrderId = data[alterationOrderIndex].order_id;
						AlterationReport.AlterationReportOrderProductId = data[alterationOrderIndex].products[alterationProductIndex].orders_products_id;
						AlterationReport.AlterationId = data[alterationOrderIndex].products[alterationProductIndex].fittings.alteration.alteration_id;
						AlterationReport.AlterationReportId = '';
						var tailors = ko.observable(dsRegistry.getDatasource('tailorsDS').getStore());					
						for (i in tailors()){				
							if(tailors()[i].user_id == data[alterationOrderIndex].products[alterationProductIndex].fittings.alteration.alteration_tailor_id){
								AlterationReport.AlterationReportTailor = tailors()[i];
							}
						}	
						AlterationReport.AlterationReportDate = ''; 
						AlterationReport.AlterationReportNotes = '';
						AlterationReport.AlterationReportSalesmanComments = '';
						
						if(data[alterationOrderIndex].products[alterationProductIndex].garment == 'Suit/J' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'Jacket' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'jacket'){
							AlterationReport.PanelMeasurements = JSON.parse('{"jacket_panel_front_chest":"","jacket_panel_front_true_waist":"","jacket_panel_front_hip":"","jacket_panel_side_chest":"","jacket_panel_side_true_waist":"","jacket_panel_side_hip":"","jacket_panel_side_bottom_edge":"","jacket_panel_back_x_back":"","jacket_panel_back_chest":"","jacket_panel_back_true_waist":"","jacket_panel_back_hip":"","jacket_panel_back_bottom_edge":"","jacket_panel_across_shoulder":"","jacket_panel_across_back":"","jacket_panel_front_length":"","jacket_panel_front_side_seam_length":"","jacket_panel_center_back_length":"","jacket_panel_shoulder_seam":"","jacket_panel_bicep":"","jacket_panel_elbow":"","jacket_panel_sleeve_cuff_buttoned":"","jacket_panel_sleeve_length_right":"","jacket_panel_sleeve_length_left":""}');
						}else if( data[alterationOrderIndex].products[alterationProductIndex].garment == 'Suit/P' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'Pants' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'pants' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'Pant' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'pant' ){									
							AlterationReport.PanelMeasurements = JSON.parse('{"pants_panel_total_waist":"","pants_panel_front_hip":"","pants_panel_back_hip":"","pants_panel_fly":"","pants_panel_front_crotch":"","pants_panel_back_crotch":"","pants_panel_front_thigh":"","pants_panel_back_thigh":"","pants_panel_front_knee":"","pants_panel_back_knee":"","pants_panel_cuffs":"","pants_panel_inseam":"","pants_panel_outseam":""}');
						}else if(data[alterationOrderIndex].products[alterationProductIndex].garment == 'Vest' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'vest'){			
							AlterationReport.PanelMeasurements = JSON.parse('{"vest_panel_front_chest":"","vest_panel_front_waist":"","vest_panel_front_hip":"","vest_panel_back_chest":"","vest_panel_back_waist":"","vest_panel_back_hip":"","vest_panel_full_shoulder":"","vest_panel_half_shoulder":"","vest_panel_front_armhole":"","vest_panel_back_armhole":"","vest_panel_first_button":"","vest_panel_side":"","vest_panel_back_length":"","vest_panel_front_length":""}');
						}else if(data[alterationOrderIndex].products[alterationProductIndex].garment == 'Shirt' || data[alterationOrderIndex].products[alterationProductIndex].garment == 'shirt'){
							AlterationReport.PanelMeasurements = JSON.parse('{"shirt_panel_front_length":"","shirt_panel_back_length":"","shirt_panel_front":"","shirt_panel_back":"","shirt_panel_full_shoulder":"","shirt_panel_half_shoulder":"","shirt_panel_front_chest":"","shirt_panel_back_chest":"","shirt_panel_front_waist":"","shirt_panel_back_waist":"","shirt_panel_front_hip":"","shirt_panel_back_hip":"","shirt_panel_neck":"","shirt_panel_bicep":"","shirt_panel_elbow":"","shirt_panel_cuff":"","shirt_panel_sleeve_length":"","shirt_panel_used_block":""}');
						}
						x++;
						iAlterationReport.AlterationReport.push(AlterationReport);	
		//			}
		//		}
		//	}
			this._super(name, iAlterationReport, dsRegistry);
		}	
	}
});


//END DEFINE CLOSURE
});
