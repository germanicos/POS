define(['jquery', 'knockout', 'base'], function($, ko) {
    
    AlterationsReportListVM = SimpleControl.extend({

        init: function($initData) {

            var self = this;
            console.log("Init alterations report list VMs")

            changeVisibilityDiv('loading_jp');

            this.alterationsList = ko.observableArray();
            this.currentCustomer = ko.observable("");


             // POST to the server to the get alteration details
            $.ajax({
                type: 'POST',
                url: BUrl + "alterations_pos/get_alteratons_report_list",
                dataType: 'json',
                data :  { "user" : authCtrl.userInfo },
                async: true,
                success: function(ret) {

                    console.log(ret);

                    changeVisibilityDiv('loading_jp');
                    
                    console.log("Alt report list", ret);

                    for (let i = 0; i < ret.length; i++) {
                        self.alterationsList.push(ret[i]);
                    }
                

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    changeVisibilityDiv('loading_jp');

                    alert("Something went wrong, please try again.")
                    posChangePage("#alterationsDashBoard");

                    
                }
            });


        },

        chooseCustomer : function(customer) {
            console.log("changing customer", customer);
            this.currentCustomer(customer)
        },

        chooseReport : function(fitting_id) {
            
            console.log("Report for fitting: ", fitting_id);
            alterationsReportSubmissionVM = new AlterationsReportSubmissionVM( { "fitting_id" : fitting_id });
            // seccond parameter => for the back button to work properly
            
            posChangePage("#AlterationReportSubmission");
        }

    });
});