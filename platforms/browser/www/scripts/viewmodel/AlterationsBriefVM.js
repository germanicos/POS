define(['jquery', 'knockout', 'base'], function($, ko) {
    
    AlterationsBriefVM = SimpleControl.extend({

        init: function($initData) {

            var self = this;

            this.briefList = ko.observableArray();
            this.currentCustomer = ko.observable("");
            
            changeVisibilityDiv('loading_jp');

            console.log("Init AlterationsBrifVM");

            // POST to the server to the get alteration details
            $.ajax({
                type: 'POST',
                url: BUrl + "alterations_pos/get_alteration_brief",
                dataType: 'json',
                data :  { "user" : authCtrl.userInfo },
                async: true,
                timeout: 60000,
                success: function(ret) {

                    console.log(ret);

                    changeVisibilityDiv('loading_jp');


                    for (let i = 0; i < ret.length; i++) {
                        self.briefList.push(ret[i]);
                    }
                

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                    console.log("XMLHttpRequest", XMLHttpRequest);
                    console.log("textStatus", textStatus);
                    console.log("errorThrown", errorThrown);

                    // remove loader
                    changeVisibilityDiv('loading_jp');

                    customAlert("TIME OUT - there is a network issue. Please try again later"); 
                    posChangePage("#alterationsDashBoard");

                    
                }
            });




        },

        chooseCustomer : function(customer) {
            console.log("changing customer", customer);
            this.currentCustomer(customer)
        },

        /**
         * Init a briefDetails with the corresponding fit and go to next page
         */
        goToBriefDetails: function (nextPage, data) {

            briefDetails = new BriefDetailsVM(data);
            posChangePage(nextPage)

        }

    });
});