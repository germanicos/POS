define(['jquery', 'knockout', 'base'], function($, ko) {
ShippingOther = SimpleControl.extend({
	init: function(DsRegistry, csvm, muVM) {
		this._super( DsRegistry );
		var that = this;
		
		this.ShippingOtherData     = [];
		this.ShippingOtherDataAID  = 0;
		
		this.previousStepEnabled = ko.observable(false);
		this.customers = new CustomerAutoCompleteVM(this.DsRegistry, csvm);
		this.otherImage = new MediaType(1, null, true, '',true);
		this.otherVideo = new MediaType(2, null, true, '',true);
		this.otherComments = ko.observable('');
		this.otherPreviewImage = ko.observableArray([]);
		this.otherPreviewVideo = ko.observableArray([]);
		this.orderImageIndex = '';
		this.orderImageEvent = '';
		this.orderImageControler = '';
		this.mediaUpload = muVM;
		this.buttonVisible = ko.observable(true);
		
		this.callbackMedia = function(data){
			that.mediaController.addMedia(that.mediaUpload.imagPath,that.mediaUpload.upPath, true);	
		};
		
		this.clickMedia = function(controller, mode){
			that.mediaController = controller;
			if(mode == 2){
				that.mediaUpload.choosePhoto2();
			}
			else{
				$('#ShippingOtherImageFiles').click();
				//that.mediaUpload.chooseVideo();
			}
		};
		
		this.dialog = function(controller, event, index){
			this.orderImageIndex = index;
			this.orderImageEvent = event;
			this.orderImageControler = controller;
			var tag = this.otherImage.buildTagPicture('', this.orderImageEvent, this.orderImageIndex);
			$("#mapper" + this.orderImageIndex).css("left", tag.left+ '%');
			$("#mapper" + this.orderImageIndex).css("top", tag.top + '%');
			$("#mapper" + this.orderImageIndex).css("width","35px");
			$("#mapper" + this.orderImageIndex).css("height","35px");
			$("#mapper" + this.orderImageIndex).show();
			document.getElementById("form_panel").style.display = "block";
		};
		
		this.closeDialog2 = function(){
			this.orderImageIndex = -1;
			this.orderImageEvent = null;
			this.orderImageControler = null;
			$("div.form_panel").hide();
			$('div.mapper').hide();
			$("textarea.title").val('');
		};
		
		this.addTag = function(){
			var text = $("textarea.title").val();
			$("div.form_panel").hide();
			$('div.mapper').hide();
			$("textarea.title").val('');
			var tag = this.orderImageControler.buildTagPicture(text, this.orderImageEvent, this.orderImageIndex);
			this.orderImageControler.addTag(tag, this.orderImageIndex);
			this.orderImageIndex = -1;
			this.orderImageEvent = null;
			this.orderImageControler = null;
		};

		this.workflow    = ko.observableArray([
			{id: 0, target: "#shipping_other",  completed: "false", caption: "ShippingOther", title: "UPLOAD IMAGES", myclass: "cimage" },
		]);
 
		this.currentStep = ko.observable();
		this.currentStep(this.workflow()[0]);
		this.currentStep.subscribe( function(data) {
			posChangePage(data.target);
			$('.cloneDialog').remove(); 
			that.renderShippingOther();
		});
		
		this.isStepCompleted = ko.computed(function() {
			for ( var ind in that.workflow() ) {
				if ( that.workflow()[ind].id == that.currentStep().id ) {
					if(that.workflow()[ind].completed == "true" || that.workflow()[ind].completed == "pending"){
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		});
		
		this.stepCaption = ko.computed(function() {
			return that.currentStep().caption;
		});

		this.stepTitle = ko.computed(function() {
			return that.currentStep().title;
		});

		this.completion	= ko.observable(0);
		this.variantNameShippingOther  = ko.observableArray([{id: 0, title: "ShippingOther 1"}]);
		this.selectedVariantShippingOther  = ko.observable(this.variantNameShippingOther()[0]);	
		this.selectedVariantShippingOther.subscribe(function(data) {
			that.selectedVariantShippingOther();
			that.renderShippingOther();			
			var cs = that.currentStep().id;
			that.currentStep(that.workflow()[cs] );
		});

		this.nextStepCaption = ko.computed(function() {
			var tWork = that.workflow();
			var tInd  = -1;
			for ( var ind in tWork ) {
					if (tWork[ind].completed !== "true") {
					tInd = ind;
					break;
				}
			}
			if (tInd != -1 ) {
				return tWork[tInd].title;
			} else {
				return " Not available ";
			}
		});

		this.previousStepCaption = ko.computed(function() {
			if ( that.currentStep().id !== 0 ) {
				return that.workflow()[that.currentStep().id  - 1].title;
			} else {
				return " Not available ";
			}
		});

				
		this.Description = ko.observable('');
		this.Description.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingOtherData[ that.getRow(that.selectedVariantShippingOther().id)  ].Description = data;
				that.flushModel();
			}	
		});
		
		this.VideoUrl = ko.observable('');
		this.VideoUrl.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingOtherData[ that.getRow(that.selectedVariantShippingOther().id)  ].VideoUrl = data;
				that.flushModel();
			}	
		});

		this.VideoComments = ko.observable('');
		this.VideoComments.subscribe(function(data) {
			if(data != undefined){		
				that.ShippingOtherData[ that.getRow(that.selectedVariantShippingOther().id)  ].VideoComments = data;
				that.flushModel();
			}	
		});
		
			

///////////////////////////		SYNC PART	///////////////////////////	
//////////////////////////////////////////////////////////////////////
		
		this.sync = function() {
		
			var spinner = document.getElementById('loading_jp');
		   	spinner.style.display = "block";
			that.buttonVisible(false);
			
			
			var customerString = that.customers.selectedCustomer() ? that.customers.selectedCustomer(): null;
	
			if(!this.otherImage.hasMedia() && !this.otherVideo.hasMedia()){
				customAlert("Please upload at least a media showing the item");
				that.buttonVisible(true);
			}else if(this.Description().trim() == ""){
				customAlert("Please enter a description");
				that.buttonVisible(true);
			}else{
				var salesman = new Object();
		   		salesman.username = authCtrl.username();
		   		salesman.password  = authCtrl.password();
		   		salesman.id  = authCtrl.userInfo.user_id;
				
				data = { 
					salesman: salesman,
					device_id: typeof device != "undefined"? device.uuid: "",
					customer: customerString,
					description: this.Description(),
					video: this.otherVideo.getSyncInfo(),
					images: this.otherImage.getSyncInfo()
				};		
				
				var datatopost = JSON.stringify(data);
				datatopost = datatopost.replace("\"isDirty\":true,", "");	
console.log("datatopost: " + datatopost);
				var endpoint_url = BUrl + 'client_logistics/shipping_other_endpoint';
					
				$.post(endpoint_url, {
					data: datatopost
				},
		        function(dataS){
		        	dataS = JSON.parse(dataS);
		            if (dataS.valid == '1') {
		            	customAlert("Succesfully sent");
						that.buttonVisible(true);
		                posChangePage('#shipping_main');
					}else{
						console.log('fail');
						that.buttonVisible(true);
		                $.jGrowl(dataS.msg);
					}
				});
	        }        		
			spinner.style.display = "none";
		}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		
	},
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	previousStep:  function() {
		if (this.currentStep().id !== 0) {
			this.previousStepEnabled(true);
			this.currentStep( this.workflow()[this.currentStep().id  - 1]);
		}
	},
 
	flushModel: function() {
		this.dsRegistry.getDatasource(this.subscribed).setStore({
			"ShippingOther"  : this.ShippingOtherData
		}, true);
		this.renderShippingOther();
	},

	getVariant: function(id) {
		var toreturn = this.ShippingOtherData[0];
		for (var ind in this.ShippingOtherData) {
			if ( this.ShippingOtherData[ind].variantId == id  ) {
				toreturn = this.ShippingOtherData[ind];
			}
		}
		return toreturn;
	},
	
	getRow: function(id) {
		for (var ind in this.ShippingOtherData) {
			if ( this.ShippingOtherData[ind].variantId == id  ) {
				return ind;
			}
		}
		return -1;
	},
	
	addVariantShippingOther: function(name) {
		if(this.variantNameShippingOther()[0].title != "ShippingOther 1"){
			this.ShippingOtherDataAID += 1;
			var newid = this.ShippingOtherDataAID + 1;
			var vname = name; 
			var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingOther().id ) )  ); //CLONE Object
			this.variantNameShippingOther.push({title: vname, id: this.ShippingOtherDataAID});
			tObj.variantId = this.ShippingOtherDataAID;		
			this.flushModel();
		}else{
			this.variantNameShippingOther()[0].title = name;
		}
	},	
	
	addVariantsShippingOther: function() {
		for(var x in orderItem.ShippingOtherVM.ShippingOtherData){
		//	console.log("addVariantsShippingOther " + x);
			if( x != 0){
				this.ShippingOtherDataAID += 1;
				var vname = "ShippingOther " + x; 
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingOther().id ) )  ); //CLONE Object
				this.variantNameShippingOther.push({title: vname, id: this.ShippingOtherDataAID});
				tObj.variantId = this.ShippingOtherDataAID;		
				this.flushModel();
			}else{
				var vname = "ShippingOther " + x; 
				//var vname = orderItem.ShippingOtherVM.ShippingOtherData[x].pant;
				var tObj = jQuery.extend(true, {}, this.getVariant( this.getRow(this.selectedVariantShippingOther().id ) )  ); //CLONE Object
				this.variantNameShippingOther()[x].title = vname;
				tObj.variantId = this.ShippingOtherDataAID;		
				this.flushModel();
			}
		}
	},

	digestData: function(data) {
		this.ShippingOtherData  = data.ShippingOther;
		this.renderView();

	},

	renderView: function() {
		this.renderShippingOther();
	},

	renderShippingOther: function() {	
		//Get selected Variant		
		try{
			var tData = this.getVariant(this.selectedVariantShippingOther().id);
			if (tData != null) {
				//Update observables
				if (typeof(tData.otherImage)		!= "undefined") {
					this.otherImage = tData.otherImage;
					this.otherImage.setCustomPreview(this.otherPreviewImage);
					this.otherImage.setCustomPreviewValue();
					
				}
				if (typeof(tData.otherVideo)		!= "undefined") {
					this.otherVideo = tData.otherVideo;
					this.otherVideo.setCustomPreview(this.otherPreviewVideo);
					this.otherVideo.setCustomPreviewValue();
				}
				if (typeof(tData.Description)  		!= "undefined") {this.Description(tData.Description);}
			}
		}catch(e){
			;
		}

	}
});

defShippingOther = SimpleDatasource.extend({
	init: function( name, dsRegistry) {
	//	if(data == null || data == undefined){
			var ShippingOther = {};
			ShippingOther.variantId        = 0;
			ShippingOther.customers = new CustomerAutoCompleteVM(null, orderItem.custSelectVM);
			ShippingOther.Description = '';
			ShippingOther.otherImage = new MediaType(1, null, true, '',Fitting.isActive);
			ShippingOther.otherVideo = new MediaType(1, null, true, '',Fitting.isActive);
			
			var iShippingOther = {
				ShippingOther: []
			};
			iShippingOther.ShippingOther.push(ShippingOther);
			this._super(name, iShippingOther, dsRegistry);
	//	}	
	}
});


//END DEFINE CLOSURE
});
