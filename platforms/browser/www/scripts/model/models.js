
define(['jquery', 'knockout'], function($, ko) {

	function cleanArray(actual){
		  var newArray = new Array();
		  for(var i = 0; i<actual.length; i++){
			  if (actual[i]){
				newArray.push(actual[i]);
			}
		  }
		  return newArray;
	}

	DatasourceRegistry = function DatasourceRegistry() {
		var self = this; 
		self.registry = []; 
	
		self.register = function(name, datasource) {
			self.registry[name] = datasource; 
		}
		
		self.getDatasource = function(name) {
			return self.registry[name]; 
		}
	}


	RemoteDatasource = function DataSource(url) {
		var self = this;
		self.firstName =  ko.observable("alex"); 
		self.articles =   ko.observableArray(); 
		self.url = url; 

		
		self.fetch = function(nUrl) {
			if (typeof(nUrl) != 'undefined') {self.url = nUrl;}
			console.log(nUrl);	
			
			$.read(self.url).then(function (data) {
				self.articles.removeAll();
				for (ind in data) {
					self.articles.push(data[ind]);
				}
			});
		}
	}
	
SyncableDatasource = function SyncableDataSource(name, dataRef, datasourceRegistry) {
		var self = this; 
		self.index = 0;
		
		self.upsert = function(item, triggerUpdate) {
console.log("RUNNING BASIC upsert");			
			if (typeof(item) != "undefined" && typeof(item.id) != "undefined") {
				item.isDirty = true; 
				self.store[item.id] = item; 
			} else {
				item.id = self.index; 
				self.store[self.index] = item; 
				self.index += 1; 
			}
			if (triggerUpdate) {self.modelUpdate();} 
		}
		
		self.deleteItem = function(id, triggerUpdate) {
			if (typeof(self.store[id]) != "undefined") {
				self.store[id].isDead = true; 
			}
			
			if (triggerUpdate) {self.modelUpdate();} 
		}
		
		self.modelUpdate = function() { 
console.log("modelUpdate AT MODELS");			
			if (self.store.length != 0) {
				var st = {}
				st.store =  self.store; 
				st.timestamp = Math.round(new Date().getTime() / 1000); 
				
//				self.lastUpdate = st.timestamp;
				if (typeof(localStorage.getItem(self.dataRef)) != 'undefined') {
					localStorage.removeItem(self.dataRef); 
				}
				
				localStorage[dataRef] = JSON.stringify(st);   
				$(document).trigger('modelUpdate', name); 
			}
		}
		
		self.sync = function() {
console.log("MODELS.JS SYNC");			
			var diffArray = []; 
			var newStore  = self.store.slice(0);   //CLONE  
			for (ind in self.store) {
				if (self.store[ind].isDirty || typeof(self.store[ind].serverId) == "undefined" ) {
					diffArray.push(self.store[ind]); 
					newStore[ind] = null; 
				} else if (store[ind].isDead) {
					diffArray.push( {isDead: true, server_id: store[ind].server_id} )
					newStore[ind] = null;
				}
			}

			$.post('api/customer_endpoint.php', {
				timestamp:  self.lastUpdate, 
				data: JSON.stringify(diffArray)}, 
				function(data) {
					var newItems = data['new'];  					
					for (ind in newStore) {
						if ( typeof(newStore[ind]) != "undefined" &&
						     typeof(newStore[ind].server_id) != "undefined" && 
							 $.inArray(newStore[ind].server_id, data['existing']) != -1) {
				     			newStore[ind] = null; 
						}
					}
					
					self.store = cleanArray(newStore); 
					for (ind in newItems) {
						self.upsert(newItems[ind]);
					}
					self.modelUpdate(); 
					
				});
		}
		
		self.getStore = function() {
			return self.store; 
		}
		
		self.clearStore = function() {
			self.store = []; 
			self.modelUpdate(); 
		}
		
		
		self.name  = name; 
		self.dataRef = dataRef;

console.log("HERE");		
		if ( localStorage.getItem(dataRef) != null) {
			var st = JSON.parse(localStorage.getItem(dataRef));
			self.store = st.store; 
			self.lastUpdate = st.timestamp;
		} else {
			self.lastUpdate = ""; 
			self.store = [];
		}			
		
		datasourceRegistry.register(name, self);
		self.modelUpdate();
}
	
LocalDatasource = function LocalDataSource(name, data, datasourceRegistry) {
		var self = this;
		self.store = data; 
		self.name  = name; 
		

		self.modelUpdate = function() { 
			$(document).trigger('modelUpdate', name); 
		}
		
		self.addItem = function(item) {
			self.store.push(item);
			self.modelUpdate();
			return self;
		}
		
		self.getStore = function() {
console.log("models.js getStore");			
			return self.store; 
		}
		
		self.setStore = function(data) {
			self.store = data; 
			self.modelUpdate(); 
		}
		
		self.deleteStore = function() {
			self.store = []; 
			self.modelUpdate(); 
		}
		
		datasourceRegistry.register(name, self);
		self.modelUpdate();
	}
	
	
	
});