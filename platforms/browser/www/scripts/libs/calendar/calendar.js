function delete_event() {
    if (confirm("This action is irreversible. Are you sure you want to delete this event?")) {
        event_id = $("#event_id-edit").val();
        var request = $.ajax({
            url: BUrl + "calendar/delete_event",
            type: "POST",
            data: {
                "event_id": event_id
            },
            cache: false,
            dataType: "json"
        });
        request.done(function(msg) {
            //customAlert("Changes in Calendar saved!")
            console.log(msg);
            if (msg == "1") {
                window.location.assign(location.href);
            }
        });
        request.fail(function(jqXHR, textStatus) {
            //customAlert("Request failed!");
            console.log("Request failed");
            console.log(jqXHR);
            console.log(textStatus );
            if(jqXHR.responseText == "1") {
                window.location(location.href);
            }
            else {
                window.location.assign(location.href);
            }
        });
    }
}

function delete_availability() {
    if (confirm("This action is irreversible. Are you sure you want to delete this leave?")) {
        availability_id = $("#availability_id").val();
        var request = $.ajax({
            url: BUrl + "calendar/delete_availability",
            type: "POST",
            data: {
                "availability_id": availability_id
            },
            cache: false,
            dataType: "json"
        });
        request.done(function(msg) {
            //customAlert("Changes in Calendar saved!")
            console.log(msg);
            if (msg == "1") {
                window.location.assign(location.href);
            }
        });
        request.fail(function(jqXHR, textStatus) {
            //customAlert("Request failed!");
            console.log("Request failed");
            console.log(jqXHR);
            console.log(textStatus );
            if(jqXHR.responseText == "1") {
                window.location(location.href);
            }
            else {
                window.location.assign(location.href);
            }
        });
    }
}


function check_availabilities_overlap() {
	var start_time = $('#event_start_time').val();
	var time = start_time.split(":");
	var start_hours = time[0];
	var start_minutes = time[1];
	var start_hours_to_mins = (parseInt(start_hours)*60)+parseInt(start_minutes);
	var duration = $('.durationSlider').slider("option", "value");
	var end_hours_to_mins = start_hours_to_mins + duration;
	var end_hours = Math.floor(end_hours_to_mins / 60);          
    var end_minutes = end_hours_to_mins % 60;
    
	var event_start = $('#event_date').val() + " " + start_time + ":00"; 
	var event_end = $('#event_date').val() + " " + ("0" + end_hours).slice(-2) + ":" + ("0" + end_minutes).slice(-2) + ":00"; 
    user_id = $('#user_id-av').val();
    var request = $.ajax({
        url: BUrl + "calendar/check_availabilities_overlap",
        type: "POST",
        data: {
            "user_id" : user_id, 
            "start": event_start, 
            "end": event_end
        },
        cache: false,
        dataType: "json"
    });

    request.done(function(msg) {
        console.log(msg);
		options1 = '<div class="mws-panel grid_8"><div class="mws-panel-header"><span></span></div><table class="mws-table">';
		if(msg.count == '1') {options1 += '<tr><td style="color:#FF1E3E">'+msg.username+' already has a leave for the specific date and time.</td></tr>'; } 
		else {options1 += '<tr><td>No Leave found in this date</td></tr>';}
		 options1 += '</table></div>';
		 $('#availability_msg_1').html(options1);
		 /*
        console.log(msg);
        if (msg == '1') $('#overlap_msg'+edit).text('Warning: There already is an availability for the specific date and time.');
        if (msg == '0') $('#overlap_msg'+edit).text('');*/
    });

    request.fail(function(jqXHR, textStatus) {
        customAlert("Request failed!");
        console.log("Request failed");
        console.log(jqXHR);
        console.log(textStatus );
    });
}

function check_availabilities_overlap_edit() {
	var start_time = $('#event_start_time-edit').val();
	var time = start_time.split(":");
	var start_hours = time[0];
	var start_minutes = time[1];
	var start_hours_to_mins = (parseInt(start_hours)*60)+parseInt(start_minutes);
	var duration = $('.editDurationSlider').slider("option", "value");
	var end_hours_to_mins = start_hours_to_mins + duration;
	var end_hours = Math.floor(end_hours_to_mins / 60);          
    var end_minutes = end_hours_to_mins % 60;
    
	var event_start = $('#event_date-edit').val() + " " + start_time + ":00"; 
	var event_end = $('#event_date-edit').val() + " " + ("0" + end_hours).slice(-2) + ":" + ("0" + end_minutes).slice(-2) + ":00"; 
    user_id = $('#user_id-av').val();
    var request = $.ajax({
        url: BUrl + "calendar/check_availabilities_overlap",
        type: "POST",
        data: {
            "user_id" : user_id, 
            "start": event_start, 
            "end": event_end
        },
        cache: false,
        dataType: "json"
    });

    request.done(function(msg) {
        console.log(msg);
		options1 = '<div class="mws-panel grid_8"><div class="mws-panel-header"><span></span></div><table class="mws-table">';
		if(msg.count == '1') {options1 += '<tr><td style="color:#FF1E3E">'+msg.username+' already has a leave for the specific date and time.</td></tr>'; } 
		else {options1 += '<tr><td>No Leave found in this date</td></tr>';}
		 options1 += '</table></div>';
		 $('#availability_msg_edit_1').html(options1);
		 /*
        console.log(msg);
        if (msg == '1') $('#overlap_msg'+edit).text('Warning: There already is an availability for the specific date and time.');
        if (msg == '0') $('#overlap_msg'+edit).text('');*/
    });

    request.fail(function(jqXHR, textStatus) {
        customAlert("Request failed!");
        console.log("Request failed");
        console.log(jqXHR);
        console.log(textStatus );
    });
}

/*function check_availabilities_overlap(edit) {
    user_id = $('#user_id-av'+edit).val();
    event_date = $('#availability_date'+edit).val();
    start = $('#availability_hour_select'+edit).val();
    end = $('#availability_duration'+edit).val();
  
    var request = $.ajax({
        url: BUrl + "calendar/check_availabilities_overlap",
        type: "POST",
        data: {
            "user_id" : user_id, 
            "event_date": event_date,
            "start": start, 
            "end": end
        },
        cache: false,
        dataType: "json"
    });

    request.done(function(msg) {
        console.log(msg);
        if (msg == '1') $('#overlap_msg'+edit).text('Warning: There already is an availability for the specific date and time.');
        if (msg == '0') $('#overlap_msg'+edit).text('');
    });

    request.fail(function(jqXHR, textStatus) {
        customAlert("Request failed!");
        console.log("Request failed");
        console.log(jqXHR);
        console.log(textStatus );
    });
}*/



function select_time_range() {
    var hours = $(this).parent().children('span').text(); 
    hours = hours.split(' ');
    start = parseInt(hours[0].split(':')[0]);
    end = parseInt(hours[2].split(':')[0]);

    $('#event_hour_select').remove();
    $('label[for="event_hour_select"]').remove();
    $(this).parent().append('<label for="event_hour_select">Event starts at:</label>');
    $(this).parent().append('<select name="start" id="event_hour_select"></select>');
    for (i=start;i<end;i++) {
        $('#event_hour_select').append('<option value="'+((i<10)?"0":"")+i+':00">'+((i<10)?"0":"")+i+':00</option>');
        $('#event_hour_select').append('<option value="'+((i<10)?"0":"")+i+':30">'+((i<10)?"0":"")+i+':30</option>');
    }

    $('#event_duration').remove();
    $('label[for="event_duration"]').remove();
    $(this).parent().append('<label for="event_duration">Event duration (minutes):</label>');
    $(this).parent().append('<select name="end" id="event_duration">'+
        '<option value="30">30</option>'+
        '<option value="45">45</option>'+
        '<option value="60">60</option>'+
        '<option value="90">90</option>'+
        '<option value="120">120</option></select>');
}


function populate_edit_event_form(event) {
    id = event.id.split('-');
    type_of_service = event.className[0];
    id = id[1];
    console.log(id);
    switch (type_of_service) {
        case 'meeting':
            email = meetings[id].email;
            message = meetings[id].message;
            telephone = meetings[id].telephone;
            customer_name = meetings[id].customer_name;
            start = meetings[id].start;
            end = meetings[id].end;
            tos_val = 2;
            location_name = meetings[id].location_name;
            user_id = meetings[id].user_id;
            break;
        case 'fitting':
            email = fittings[id].email;
            message = fittings[id].message;
            telephone = fittings[id].telephone;
            customer_name = fittings[id].customer_name;
            start = fittings[id].start;
            end = fittings[id].end;
            tos_val = 1;
            location_name = fittings[id].location_name;
            user_id = fittings[id].user_id;
            break;
        case 'other':
            email = others[id].email;
            message = others[id].message;
            telephone = others[id].telephone;
            customer_name = others[id].customer_name;
            start = others[id].start;
            end = others[id].end;
            tos_val = 0;
            location_name = others[id].location_name;
            user_id = others[id].user_id;
            break;
        case 'availability':
            start = availabilities[id].start;
            end = availabilities[id].end;
            $('#datetimepicker-av-start-edit').datetimepicker('setDate', toJSDateFormat(start) );
            $('#datetimepicker-av-end-edit').datetimepicker('setDate', toJSDateFormat(end) );
            $('#availability_id').val(id);
            $('#user_id-av-edit').val(availabilities[id].user_id);
            break;
        case 'shipping':
            break;
    }

    $('#user_id-edit').val(user_id);
    $('#event_id-edit').val(id);
    $('#email-edit').val(email); 
    $('#customer_name-edit').val(customer_name);
    $('#message-edit').val(message); 
    $('#type_of_service-edit').val(tos_val);	
	$("#mws-datepicker-event-start-edit").datetimepicker('setDate', toJSDateFormat(start) );
    $('#event_datepicker-edit').datepicker( "setDate", start.substring(0,10) );
    $('#event_date-edit').val(start.substring(0,10));
    $('#location_name-edit').val(location_name);
    $('#telephone-edit').val(telephone); 
    
    var difference = toJSDateFormat(end) - toJSDateFormat(start);
    var minutesDifference = Math.floor(difference/1000/60);
    
    $('.editDurationSlider').slider('value',minutesDifference);
    
    check_availability = start.substring(0,10);
    get_availability_on_date('-edit');
}


function ajax_update_entry() {
    events_str = JSON.stringify(events_json);
    orders_str = JSON.stringify(orders_json);
    shippings_str = JSON.stringify(shippings_json);
    availabilities_str = JSON.stringify(availabilities_json);

    var request = $.ajax({
        url: BUrl + "calendar/update_entry",
        type: "POST",
        data: {
            "events_str" : events_str, 
            "orders_str": orders_str,
            "shippings_str": shippings_str, 
            "availabilities_str": availabilities_str
        },
        cache: false,
        dataType: "json"
    });
    request.done(function(msg) {
        customAlert("Changes in Calendar saved!");
        console.log(msg);
    });
    request.fail(function(jqXHR, textStatus) {
        customAlert("Request failed!");
        console.log("Request failed");
        console.log(jqXHR);
        console.log(textStatus );
    });
}


function get_availability_on_date(edit) {
    user_id = $('#user_id').val();
    var request = $.ajax({
        url: BUrl + "calendar/get_availability_on_date",
        type: "POST",
        data: {
            "user_id":user_id, 
            "date":check_availability
        },
        cache: false,
        dataType: "json"
    });

    request.done(function(msg) {
        console.log(msg);

        $('#availability_msg'+edit).html('<strong>'+check_availability+'</strong><br/>');

        if (msg != null) {
            $('#availability_msg'+edit).html($('#availability_msg'+edit).html()+msg);
            $('.event_availability_hours').append(' <input type="radio" name="which_time_range">');
        }
        else {
            $('#availability_msg'+edit).html($('#availability_msg'+edit).html()+'No availibility was found for this date.');
        }
    });

    request.fail(function(jqXHR, textStatus) {
        console.log(jqXHR);
        console.log(textStatus );
        $('#availability_msg'+edit).html('<strong>'+check_availability+'</strong><br/>');

        if (jqXHR.responseText != "") {
            $('#availability_msg'+edit).html($('#availability_msg'+edit).html()+jqXHR.responseText);
            $('.event_availability_hours').append(' <input type="radio" name="which_time_range">');
        }
        else {
            $('#availability_msg'+edit).html($('#availability_msg'+edit).html()+'No availibility was found for this date.');
        }
    });
}


function toMysqlFormat(dt) {
    function twoDigits(d) {
        if(0 <= d && d < 10) return "0" + d.toString();
        if(-10 < d && d < 0) return "-0" + (-1*d).toString();
        return d.toString();
    }
    var d           = new Date();
    var userOffset  = d.getTimezoneOffset()*60*1000; 
    var dt          = new Date(dt.getTime()-userOffset);

    return dt.getUTCFullYear() + "-" + twoDigits(1 + dt.getUTCMonth()) + "-" + twoDigits(dt.getUTCDate()) + " " + twoDigits(dt.getUTCHours()) + ":" + twoDigits(dt.getUTCMinutes()) + ":" + twoDigits(dt.getUTCSeconds());
}

function toJSDateFormat(SQLTimestamp) {
    t        = SQLTimestamp.split(/[- :]/);
    new_date =  new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    return new_date;
}
function get_availability_salesman()
{
    var start_time = $('#event_start_time').val();
	var time = start_time.split(":");
	var start_hours = time[0];
	var start_minutes = time[1];
	var start_hours_to_mins = (parseInt(start_hours)*60)+parseInt(start_minutes);
	var duration = $('.durationSlider').slider("option", "value");
	var end_hours_to_mins = start_hours_to_mins + duration;
	var end_hours = Math.floor(end_hours_to_mins / 60);          
    var end_minutes = end_hours_to_mins % 60;
    
	var event_start = $('#event_date').val() + " " + start_time + ":00"; 
	var event_end = $('#event_date').val() + " " + ("0" + end_hours).slice(-2) + ":" + ("0" + end_minutes).slice(-2) + ":00"; 
	$('#endTime').attr('value', ("0" + end_hours).slice(-2) + ":" + ("0" + end_minutes).slice(-2) + ":00");
	$('#durTime').attr('value',duration);
	
	var user_id = $('#user_id').val();
	var event_date = $('#event_date').val();
    var request = $.ajax({
        url: BUrl + "calendar/get_availability_salesman",
        type: "POST",
        data: {
            "userid":user_id, 
            "start":event_start,
			"end":event_end,
			"event_date" : event_date
        },
        cache: false,
        dataType: "json"
    });

    request.done(function(msg) {
        console.log(msg);
		options1 = '<div class="mws-panel grid_8"><div class="mws-panel-header"><span>'+msg.select_date+'</span></div><table class="mws-table">';
		if(msg.count>0) {options1 += '<tr><td style="color:#FF1E3E">'+msg.user_name+' Not available following hours</td></tr>'; } else {options1 += '<tr><td>No Event found in this date</td></tr>';}
		
		$.each(msg.events, function(index, element) {
           options1 += '<tr><td>'+element.start+' until '+element.end+'</td></tr>';
        });  
		 options1 += '</table></div>';
		 $('#availability_msg').html(options1);
        
    });
}
function get_availability_salesman_edit()
{
	var start_time = $('#event_start_time-edit').val();
	var time = start_time.split(":");
	var start_hours = time[0];
	var start_minutes = time[1];
	var start_hours_to_mins = (parseInt(start_hours)*60)+parseInt(start_minutes);
	var duration = $('.editDurationSlider').slider("option", "value");
	var end_hours_to_mins = start_hours_to_mins + duration;
	var end_hours = Math.floor(end_hours_to_mins / 60);          
    var end_minutes = end_hours_to_mins % 60;
    
	var event_start = $('#event_date-edit').val() + " " + start_time + ":00"; 
	var event_end = $('#event_date-edit').val() + " " + ("0" + end_hours).slice(-2) + ":" + ("0" + end_minutes).slice(-2) + ":00"; 
	$('#endTimeEdit').attr('value', ("0" + end_hours).slice(-2) + ":" + ("0" + end_minutes).slice(-2) + ":00");
	$('#durTimeEdit').attr('value',duration);
	
	var user_id = $('#user_id-edit').val();
	var event_date = $('#event_date-edit').val();
	var event_id = $('#event_id-edit').val();
    var request = $.ajax({
        url: BUrl + "calendar/get_availability_salesman_edit",
        type: "POST",
        data: {
            "userid":user_id, 
            "start":event_start,
			"end":event_end,
			"event_date" : event_date,
			"event_id" : event_id
        },
        cache: false,
        dataType: "json"
    });

    request.done(function(msg) {
        console.log(msg);
		options1 = '<div class="mws-panel grid_8"><div class="mws-panel-header"><span>'+msg.select_date+'</span></div><table class="mws-table">';
		if(msg.count>0) {options1 += '<tr><td style="color:#FF1E3E">'+msg.user_name+' Not available following hours</td></tr>'; } else {options1 += '<tr><td>No Event found in this date</td></tr>';}
		
		$.each(msg.events, function(index, element) {
           options1 += '<tr><td>'+element.start+' until '+element.end+'</td></tr>';
        });  
		 options1 += '</table></div>';
		 $('#availability_msg_edit').html(options1);
        
    });
}

$(window).load(function() {
	$('#email_to_customer').change(function() {
		if(this.checked) {
			$('#email_to_customer_template').select2('enable', true);
			$("#email_to_customer_template").removeAttr( "disabled" );
		}
		else {
			$('#email_to_customer_template').select2('disable', true);
		}
	});
	
	
	$('#email_to_customer-edit').change(function() {
		if(this.checked) {
			$('#email_to_customer_template-edit').select2('enable', true);
			$("#email_to_customer_template-edit").removeAttr( "disabled" );
		}
		else {
			$('#email_to_customer_template-edit').select2('disable', true);
		}
	});
});