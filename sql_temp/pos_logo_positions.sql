use `germanicos`;

DROP TABLE IF EXISTS `uniform_logo_positions`;

CREATE TABLE `uniform_logo_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `garment_id` int(1) NOT NULL,
  `view` varchar(10) NOT NULL DEFAULT 'front' COMMENT 'can be front/back',
  `pos_x` float NOT NULL DEFAULT '0',
  `pos_y` float NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `uniform_logo_positions` (`id`, `name`, `garment_id`, `view`, `pos_x`, `pos_y`, `description`)
VALUES
	(1,'1',4,'front',39,3,''),
	(2,'2',4,'front',20,17,''),
	(3,'3',4,'front',52,18,''),
	(4,'4',4,'front',14,30,''),
	(5,'5',4,'front',57,24,''),
	(6,'6',4,'front',55,32,''),
	(7,'7',4,'front',21,53,''),
	(8,'8',4,'front',16,62,''),
	(9,'9',4,'front',60,62,''),
	(10,'10',4,'front',0,80,''),
	(11,'11',4,'front',80,84,''),
	(12,'12',4,'front',20,87,''),
	(13,'13',4,'front',50,88,''),
	(14,'14',4,'back',38,4,''),
	(15,'15',4,'back',5,50,''),
	(16,'16',4,'back',80,52,''),
	(17,'17',4,'back',5,67,''),
	(18,'18',4,'back',77,67,''),
	
	(19,'1',2,'front',40,03,''),
	(20,'2',2,'front',28,12,''),
	(21,'3',2,'front',54,12,''),
	(22,'4',2,'front',23,28,''),
	(23,'5',2,'front',52,28,''),
	(24,'6',2,'front',00,69,''),
	(25,'7',2,'front',50,61,''),
	(26,'8',2,'front',25,76,''),
	(27,'9',2,'front',47,78,''),
	(28,'10',2,'back',39,04,''),
	(29,'11',2,'back',38,10,''),

	(30,'1',1,'front',28,9,''),
	(31,'2',1,'front',58,9,''),
	(32,'3',1,'front',55,15,''),
	(33,'4',1,'front',30,14,''),
	(34,'5',1,'back',28,12,''),
	(35,'6',1,'back',48,12,''),
	
	(36,'1',3,'front',36,5,''),
	(37,'2',3,'front',11,27,''),
	(38,'3',3,'front',44,27,''),
	(39,'4',3,'front',15,64,''),
	(40,'5',3,'front',43,64,''),
	(41,'6',3,'front',33,82,''),
	(42,'7',3,'back',33,03,'');
